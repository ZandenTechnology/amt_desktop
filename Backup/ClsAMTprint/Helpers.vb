Imports System.IO
Imports System.Reflection
Imports System.Drawing
Imports System.Drawing.Text
Imports System.Runtime.InteropServices
' Embedding custom fonts into your application 
' http://www.vbforums.com/showthread.php?795109-Embedding-custom-fonts-into-your-application
Module Helpers
    Public Function LoadFont(ByVal Asm As Assembly, ByVal Name As String, ByVal Size As Integer, ByVal Style As FontStyle, ByVal Unit As GraphicsUnit) As Font
        Using Collection As New PrivateFontCollection
            Dim Bytes() As Byte = Helpers.FontData(Asm, Name)
            Dim Ptr As IntPtr = Marshal.AllocCoTaskMem(Bytes.Length)
            Marshal.Copy(Bytes, 0, Ptr, Bytes.Length)
            Collection.AddMemoryFont(Ptr, Bytes.Length)
            Marshal.FreeCoTaskMem(Ptr)
            Return New Font(Collection.Families(0), Size, Style)
        End Using
    End Function
    Private Function FontData(ByVal Asm As Assembly, ByVal Name As String) As Byte()
        Using Stream As Stream = Asm.GetManifestResourceStream(Name)
            If (Stream Is Nothing) Then Throw New Exception(String.Format("Unable to load font '{0}'", Name))
            Dim Buffer() As Byte = New Byte(CInt(Stream.Length - 1)) {}
            Stream.Read(Buffer, 0, CInt(Stream.Length))
            Return Buffer
        End Using
    End Function
End Module