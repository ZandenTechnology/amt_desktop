Imports System.IO
Imports System.Drawing.Printing
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports ClsAMTprint.Helpers

Public Class frmAMTprintpreview

    Public Target As PrintDocument
    Public ds As New DataSet
    Private HeaderLeft As Single
    Public jy As Integer = 0
    Dim CTI As Integer = 0
    Public PName As String

    Private Sub frmAMTprintpreview_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim pkSize As PaperSize
        For i As Integer = 0 To Target.PrinterSettings.PaperSizes.Count - 1
            pkSize = Target.PrinterSettings.PaperSizes.Item(i)
            If pkSize.PaperName = "A4" Then
                Target.PrinterSettings.DefaultPageSettings.PaperSize = pkSize
                Target.DefaultPageSettings.PaperSize = pkSize
                Exit For
            End If
        Next

        'Dim margins As New Margins(30, 50, 15, 50)
        'PDamt.DefaultPageSettings.Margins = margins
        'PDamt.DefaultPageSettings.Landscape = True
        'PDamt.PrinterSettings.DefaultPageSettings.Margins = margins
        'PDamt.PrinterSettings.DefaultPageSettings.Landscape = True

        Dim margins As New Margins(30, 50, 15, 50)
        PDamt.DefaultPageSettings.Margins = margins
        PDamt.DefaultPageSettings.Landscape = False
        PDamt.PrinterSettings.DefaultPageSettings.Margins = margins
        PDamt.PrinterSettings.DefaultPageSettings.Landscape = False

        HeaderLeft = margins.Left + 110
        Me.Height = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height

        AddHandler PDamt.PrintPage, AddressOf Me.printDOC

        ' Dim Printname As String = PName 'System.Configuration.ConfigurationManager.AppSettings("Printer")
        'PDamt.PrinterSettings.PrinterName = PName 'Printname
        PDamt.Print()
        Me.Close()
    End Sub

    Sub printDOC(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
        'printbackup(sender, e)
        Try
            'Font Style
            'Dim f8bar As New System.Drawing.Font("IDAutomationHC39M", 12, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
            Dim f8bar As System.Drawing.Font
            f8bar = Helpers.LoadFont(Me.GetType.Assembly, "ClsAMTprint.IDAutomationHC39M.ttf", 12, FontStyle.Bold, GraphicsUnit.Point)
            Dim f6b As New System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
            Dim f8b As New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
            Dim f8bu As New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold Or FontStyle.Underline, System.Drawing.GraphicsUnit.Point)
            Dim f10 As New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
            Dim f10b As New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
            Dim f10bar As New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
            Dim f10bu As New System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Bold Or FontStyle.Underline, System.Drawing.GraphicsUnit.Point)
            Dim f16b As New System.Drawing.Font("Arial", 14, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
            Dim f12b As New System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
            Dim f16bu As New System.Drawing.Font("Arial", 14, System.Drawing.FontStyle.Bold Or FontStyle.Underline, System.Drawing.GraphicsUnit.Point)

            'Text Align
            Dim lSF As New System.Drawing.StringFormat(System.Drawing.StringFormatFlags.LineLimit)
            lSF.Alignment = StringAlignment.Near
            Dim rSF As New System.Drawing.StringFormat(System.Drawing.StringFormatFlags.LineLimit)
            rSF.Alignment = StringAlignment.Far
            Dim cSF As New System.Drawing.StringFormat(System.Drawing.StringFormatFlags.LineLimit)
            cSF.Alignment = StringAlignment.Center

            'Pen Style
            Dim SP1 As New Pen(Color.Black, 0.5)
            Dim SP2 As New Pen(Color.Black, 1)
            Dim SP3 As New Pen(Color.Black, 2)

            Dim il, it, pw, ph, ct, ch As Single

            il = e.MarginBounds.X '30
            it = e.MarginBounds.Y '15
            pw = e.MarginBounds.Width '747
            ph = e.MarginBounds.Height '1104

            Dim lines, chars As Double
            Static bs As Integer = 0
            Static pg As Integer = 1
            Static curpg As Integer = 1
            Static lastpgprinted As Boolean = False

            Dim l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11 As Single

            l1 = il
            l2 = l1 + 30
            l3 = l2 + 100
            l4 = l3 + 50
            l5 = l4 + 50
            l6 = l5 + 50
            l7 = l6 + 50
            l8 = l7 + 50
            l9 = l8 + 50
            l10 = l9 + 50
            l11 = il + pw

            ct = it ' + (30)
            ch = ph - (20)

            Dim pa As New System.Drawing.RectangleF(l1, ct, pw, ch)
            e.Graphics.DrawString("ADVANCED MATERIALS TECHNOLOGIES PTE LTD", f16b, Brushes.Black, pa, cSF)

            il = 640 'e.MarginBounds.X
            it = e.MarginBounds.Y
            pw = e.MarginBounds.Width
            ph = e.MarginBounds.Height

            pa = New System.Drawing.RectangleF(il, it, pw, ph)
            Dim logoPath As String = System.Configuration.ConfigurationManager.AppSettings("Path")

            If logoPath = "E:\Projects\2008\AMT\Source_20101210\AMT\AMT\Resources" Then
                'Dim img As Image = Image.FromFile(Windows.Forms.Application.StartupPath & "\AMT_Logo.jpg")
                Dim img As Image = Image.FromFile(logoPath & "\AMT_Logo.jpg")

                e.Graphics.DrawImage(img, CSng(il), CSng(it))
            End If

            ct += 30
            pa = New System.Drawing.RectangleF(l1, ct, pw, ch)
            If Len(wkono) > 5 Then
                If Mid(wkono, 5, 1) = "9" Then
                    e.Graphics.DrawString("Engineering Work Order", f12b, Brushes.Black, pa, cSF)
                Else
                    e.Graphics.DrawString("WORK ORDER ROUTING", f12b, Brushes.Black, pa, cSF)
                End If
            Else
                e.Graphics.DrawString("WORK ORDER ROUTING", f12b, Brushes.Black, pa, cSF)
            End If

            ct += 20
            pa = New System.Drawing.RectangleF(l1, ct, pw, ch)
            e.Graphics.DrawString("Date: " & Format(Now.Date, "dd-MMM-yyyy"), f8b, Brushes.Black, pa, lSF)

            'pa = New System.Drawing.RectangleF(400, 1100, 200, ch)
            'e.Graphics.DrawString("LEGEND", f8bu, Brushes.Black, pa, lSF)
            'pa = New System.Drawing.RectangleF(480, 1120, 200, ch)
            'e.Graphics.DrawString("CP = Completed", f8b, Brushes.Black, pa, lSF)
            'pa = New System.Drawing.RectangleF(560, 1140, 200, ch)
            'e.Graphics.DrawString("RJ = Rejected", f8b, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(680, 1020, 80, ch)
            e.Graphics.DrawString("LEGEND", f8bu, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(680, 1040, 100, ch)
            e.Graphics.DrawString("CP = Completed", f8b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(680, 1060, 100, ch)
            e.Graphics.DrawString("RJ = Rejected", f8b, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(50, 1020, 250, ch)
            e.Graphics.DrawString("Approved By:_______________(Engrg Mgr)", f8b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(50, 1040, 500, ch)
            e.Graphics.DrawString("Approved By:_______________(QA Head/Mgr) Only products accepted for delivery", f8b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(50, 1060, 500, ch)
            e.Graphics.DrawString("Remarks:", f8b, Brushes.Black, pa, lSF)

            'two point define one line.
            e.Graphics.DrawLine(SP1, l1, ct + 15, pw + 50, ct + 15)

            'wkono = "50SI981117-B01-R"

            ct += 20
            pa = New System.Drawing.RectangleF(l1, ct, 80, ch)
            e.Graphics.DrawString("Work Order No.", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 80, ct, 10, ch)
            e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 90, ct, 330, ch)
            e.Graphics.DrawString("*" & wkono & "*", f8bar, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(l1 + 420, ct, 100, ch)
            e.Graphics.DrawString("FG Code", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 520, ct, 10, ch)
            e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 530, ct, 100, ch)
            e.Graphics.DrawString(wkofgcode, f10b, Brushes.Black, pa, lSF)

            ct += 20


            pa = New System.Drawing.RectangleF(l1 + 420, ct, 100, ch)
            e.Graphics.DrawString("DWG No.", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 520, ct, 10, ch)
            e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 530, ct, 250, ch)
            e.Graphics.DrawString(wkoptno, f10b, Brushes.Black, pa, lSF)

            ct += 20
            pa = New System.Drawing.RectangleF(l1 + 420, ct, 100, ch)
            e.Graphics.DrawString("QP Rev No.", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 520, ct, 10, ch)
            e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 530, ct, 100, ch)
            e.Graphics.DrawString(wkorevno, f10b, Brushes.Black, pa, lSF)

            ct += 20

            'pa = New System.Drawing.RectangleF(l1 + 420, ct, 100, ch)
            'e.Graphics.DrawString("Effective Date", f10b, Brushes.Black, pa, lSF)
            'pa = New System.Drawing.RectangleF(l1 + 520, ct, 10, ch)
            'e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            'pa = New System.Drawing.RectangleF(l1 + 530, ct, 100, ch)
            'e.Graphics.DrawString(wkoeftdate, f10b, Brushes.Black, pa, lSF)

            'pa = New System.Drawing.RectangleF(l1, ct, 80, ch)
            'e.Graphics.DrawString("Material", f10b, Brushes.Black, pa, lSF)
            'pa = New System.Drawing.RectangleF(l1 + 80, ct, 10, ch)
            'e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            'pa = New System.Drawing.RectangleF(l1 + 90, ct, 190, ch)
            'e.Graphics.DrawString(wkomtr, f10b, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(l1, ct, 80, ch)
            e.Graphics.DrawString("Total Qty.", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 80, ct, 10, ch)
            e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 90, ct, 190, ch)
            e.Graphics.DrawString(wkoqty, f10b, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(l1 + 420, ct, 100, ch)
            e.Graphics.DrawString("Effective Date", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 520, ct, 10, ch)
            e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 530, ct, 100, ch)
            e.Graphics.DrawString(wkoeftdate, f10b, Brushes.Black, pa, lSF)

            ct += 20


            'pa = New System.Drawing.RectangleF(l1, ct, 80, ch)
            'e.Graphics.DrawString("Total Qty.", f10b, Brushes.Black, pa, lSF)
            'pa = New System.Drawing.RectangleF(l1 + 80, ct, 10, ch)
            'e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            'pa = New System.Drawing.RectangleF(l1 + 90, ct, 190, ch)
            'e.Graphics.DrawString(wkoqty, f10b, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(l1, ct, 80, ch)
            e.Graphics.DrawString("Material", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 80, ct, 10, ch)
            e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 90, ct, 250, ch)
            e.Graphics.DrawString(wkomtr, f10b, Brushes.Black, pa, lSF)

            Dim intLine As Double
            If wkomtr = "" Then
                e.Graphics.MeasureString(Trim(":"), f10b, New SizeF(250, ch), lSF, chars, lines)
            Else
                e.Graphics.MeasureString(Trim(wkomtr), f10b, New SizeF(250, ch), lSF, chars, lines)
            End If
            intLine = lines

            pa = New System.Drawing.RectangleF(l1 + 420, ct, 100, ch)
            e.Graphics.DrawString("Part Name", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 520, ct, 10, ch)
            e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 530, ct, 250, ch)
            e.Graphics.DrawString(wkoprname, f10b, Brushes.Black, pa, lSF)

            'pa = New System.Drawing.RectangleF(l1 + 420, ct, 100, ch)
            'e.Graphics.DrawString("Effective Date", f10b, Brushes.Black, pa, lSF)
            'pa = New System.Drawing.RectangleF(l1 + 520, ct, 10, ch)
            'e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            'pa = New System.Drawing.RectangleF(l1 + 530, ct, 100, ch)
            'e.Graphics.DrawString(wkoeftdate, f10b, Brushes.Black, pa, lSF)

            ct += f10b.Height * lines

            'e.Graphics.DrawLine(SP1, l1, ct + 20, pw + 50, ct + 20)
            e.Graphics.DrawLine(SP1, l1, ct, pw + 50, ct)

            'ct += f10bu.Height * 2.4
            'ch -= f10bu.Height * 2.4

            ct += f10b.Height
            ch -= f10b.Height

            pa = New System.Drawing.RectangleF(l1, ct, l2 - l1, ch)
            e.Graphics.DrawString("OP", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l2, ct, l3 - l2, ch)
            e.Graphics.DrawString("Work Centre Name", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l3, ct, 150, ch)
            e.Graphics.DrawString("Work Description", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l4 + 100 + 30, ct, l5 - l4 + 60, ch)
            e.Graphics.DrawString("Start Date/Time", f10b, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(l5 + 150 + 30, ct, l6 - l5 + 60, ch)
            e.Graphics.DrawString("End Date/Time", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l8 + 30 + 100, ct, 0, ch)
            e.Graphics.DrawString("", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l8 + 30 + 100, ct, l8 - l7, ch)
            e.Graphics.DrawString("CP", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l9 + 30 + 100, ct, l9 - l8, ch)
            e.Graphics.DrawString("RJ", f10b, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(l10 + 30 + 100, ct, l10 - l9, ch)
            e.Graphics.DrawString("Emp ID", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l10 + 150, ct, l11 - l10, ch)
            e.Graphics.DrawString("", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l10 + 150 + 30, ct, 100, ch)
            e.Graphics.DrawString("Remark", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l10 + 250, ct, 0, ch)
            e.Graphics.DrawString("", f10b, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(il, it, pw + 50, ph)
            e.Graphics.DrawLine(SP1, l1, ct + 40, pw + 50, ct + 40)

            ct += f10bu.Height * 2.4 + 10
            ch -= f10bu.Height * 2.4 + 10

            Dim j As Integer
            Dim prtgry As Boolean = False

            Dim tlin As Integer
            For j = bs To ds.Tables(0).Rows.Count - 1
                prtgry = Not prtgry

                With ds.Tables(0).Rows(j)
                    ct += f10.Height * 0.5
                    ch -= f10.Height * 0.5
                    'measure the maximumn lines

                    Dim tempdbl As Double
                    e.Graphics.MeasureString(Trim(.Item("CtlPt")), f10, New SizeF(150, ch), lSF, chars, lines)
                    tempdbl = lines

                    'include measure the remarks
                    If .Item("Remark") = "" Then
                        pa = New System.Drawing.RectangleF(l1, ct - 5, pw + 25, f10.Height * (lines + 0.6))
                        If prtgry = True Then
                            e.Graphics.FillRectangle(Brushes.LightGray, pa)
                        End If

                        pa = New System.Drawing.RectangleF(l1, ct, l2 - l1, ch)
                    Else
                        e.Graphics.MeasureString(Trim(.Item("Remark")), f10, New SizeF(100, ch), lSF, chars, lines)
                        If lines < tempdbl Then
                            lines = tempdbl
                        End If

                        pa = New System.Drawing.RectangleF(l1, ct - 5, pw + 25, f10.Height * (lines + 0.6))
                        If prtgry = True Then
                            e.Graphics.FillRectangle(Brushes.LightGray, pa)
                        End If

                        pa = New System.Drawing.RectangleF(l1, ct, l2 - l1, ch)
                    End If

                    e.Graphics.DrawString(.Item("OP"), f10, Brushes.Black, pa, cSF)
                    pa = New System.Drawing.RectangleF(l2, ct, l3 - l2, ch)
                    e.Graphics.DrawString(.Item("Work Centre Name"), f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l3, ct, 150, ch)
                    e.Graphics.DrawString(.Item("CtlPt"), f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l4 + 100 + 30, ct, l5 - l4 + 60, ch)
                    e.Graphics.DrawString(.Item("Start Time"), f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l5 + 150 + 30, ct, l6 - l5 + 60, ch)
                    e.Graphics.DrawString(.Item("End Time"), f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l8 + 30 + 100, ct, 0, ch)
                    'e.Graphics.DrawString(.Item("Total Hrs"), f10, Brushes.Black, pa, cSF)
                    e.Graphics.DrawString("", f10, Brushes.Black, pa, lSF)

                    pa = New System.Drawing.RectangleF(l8 + 30 + 100, ct, l8 - l7, ch)
                    e.Graphics.DrawString(.Item("CP"), f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l9 + 30 + 100, ct, l9 - l8, ch)
                    e.Graphics.DrawString(.Item("RJ"), f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l10 + 30 + 100, ct, l10 - l9, ch)
                    e.Graphics.DrawString(.Item("Emp ID"), f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l10 + 150, ct, l11 - l10, ch)
                    'e.Graphics.DrawString(.Item("OP Completed"), f10, Brushes.Black, pa, lSF)
                    e.Graphics.DrawString("", f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l10 + 150 + 30, ct, 100, ch)
                    e.Graphics.DrawString(.Item("Remark"), f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l10 + 250, ct, 0, ch)
                    e.Graphics.DrawString("", f10, Brushes.Black, pa, lSF)

                    If lines = "0" Then
                        tlin = "1"
                        ct += f10.Height * (tlin + 0.5)
                        ch -= f10.Height * (tlin + 0.5)
                    Else
                        tlin = lines
                        ct += f10.Height * (tlin + 0.5)
                        ch -= f10.Height * (tlin + 0.5)
                    End If

                    'tlin = lines
                    'ct += f10.Height * (tlin + 0.4)
                    'ch -= f10.Height * (tlin + 0.4)
                    'If ct >= (it + ph) - f10.Height Then
                    '    Exit For
                    'End If

                    'If ct >= (it + ph) - f10.Height * (tlin + 0.5) Then
                    '    Exit For
                    'End If
                    If ct >= 1050 Then '(it + ph) - f10.Height * (tlin + 0.5) Then
                        Exit For
                    End If
                End With

                If ct >= 1050 Then '(it + ph) - f10.Height * (tlin + 0.5) Then
                    Exit For
                End If
            Next

            bs = j + 1
            If j >= ds.Tables(0).Rows.Count Then
                If ct >= 1050 Then '(it + ph) - f10.Height * (tlin + 0.5) Then
                    pg = 1
                End If
                If lastpgprinted = False Then
                    If ct >= 1050 Then '(it + ph) - f10.Height * 14 Then
                        pg = 1
                    Else
                        ' '' '' 'Remove the total
                        ' '' ''e.Graphics.DrawLine(SP1, l1, ct, l11 - l1, ct)
                        '' ''e.Graphics.DrawLine(SP1, l1, ct, 1150, ct)

                        ' ''ct += f10b.Height * 0.4
                        ' ''ch -= f10b.Height * 0.4
                        ' '' ''e.Graphics.DrawLine(SP2, l1, ct, l11 - l1, ct)
                        '' ''e.Graphics.DrawLine(SP2, l1, ct, 1150, ct)
                        ' ''ct += f10b.Height * 0.4
                        ' ''ch -= f10b.Height * 0.4

                        ' ''ct += f10b.Height * 2
                        ' ''ch -= f10b.Height * 2
                        ' '' ''e.Graphics.DrawLine(SP1, l1, ct, l11 - l1, ct)
                        '' ''e.Graphics.DrawLine(SP1, l1, ct, 1150, ct)
                        ' ''ct += f10b.Height * 0.4
                        ' ''ch -= f10b.Height * 0.4
                        '' ''e.Graphics.DrawLine(SP2, l1, ct, 1150, ct)
                        ' '' ''e.Graphics.DrawLine(SP2, l1, ct, l11 - l1, ct)
                        ' ''ct += f10b.Height * 4
                        ' ''ch -= f10b.Height * 4

                        lastpgprinted = True
                        pg = 3
                    End If
                End If
            End If

            'pa = New RectangleF(l1, (il + ph) - 50, l11 - l1, f10.Height * 2)
            pa = New RectangleF(l1, 1104, l11 - l1, f10.Height * 2)
            e.Graphics.DrawString("Page " & curpg, f10, Brushes.Black, pa, cSF)
            curpg += 1

            If pg = 1 Then
                PPC1.Rows += 1
                e.HasMorePages = True
            Else
                bs = 0
                pg = 1
                curpg = 1
                e.HasMorePages = False
                lastpgprinted = False
            End If
        Catch ex As Exception
            MsgBox("There was an error preparing the document." & vbCrLf & vbCrLf & "Error Message :" & vbCrLf & ex.Message, MsgBoxStyle.Exclamation, "Preparation Error")
            RetPrt = False
        End Try
    End Sub

    Dim TextToBePrinted As String

    Public Sub prt(ByVal text As String)
        TextToBePrinted = text
        Dim prn As New Printing.PrintDocument
        Using (prn)
            prn.PrinterSettings.PrinterName _
               = "Canon LBP-810"
            AddHandler prn.PrintPage, _
            AddressOf Me.PrintPageHandler
            prn.Print()
            RemoveHandler prn.PrintPage, _
               AddressOf Me.PrintPageHandler
        End Using
    End Sub

    Private Sub PrintPageHandler(ByVal sender As Object, _
       ByVal args As Printing.PrintPageEventArgs)
        Dim myFont As New Font("Microsoft San Serif", 10)
        args.Graphics.DrawString(TextToBePrinted, _
           New Font(myFont, FontStyle.Regular), _
           Brushes.Black, 50, 50)
    End Sub

    Private Sub printbackup(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
        Try
            'Font Style
            Dim f8bar As New System.Drawing.Font("IDAutomationHC39M", 8, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
            Dim f6b As New System.Drawing.Font("Arial", 7, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
            Dim f8b As New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
            Dim f8bu As New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold Or FontStyle.Underline, System.Drawing.GraphicsUnit.Point)
            Dim f10 As New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
            Dim f10b As New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
            Dim f10bar As New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
            Dim f10bu As New System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Bold Or FontStyle.Underline, System.Drawing.GraphicsUnit.Point)
            Dim f16b As New System.Drawing.Font("Arial", 14, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
            Dim f12b As New System.Drawing.Font("Arial", 12, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
            Dim f16bu As New System.Drawing.Font("Arial", 14, System.Drawing.FontStyle.Bold Or FontStyle.Underline, System.Drawing.GraphicsUnit.Point)

            'Text Align
            Dim lSF As New System.Drawing.StringFormat(System.Drawing.StringFormatFlags.LineLimit)
            lSF.Alignment = StringAlignment.Near
            Dim rSF As New System.Drawing.StringFormat(System.Drawing.StringFormatFlags.LineLimit)
            rSF.Alignment = StringAlignment.Far
            Dim cSF As New System.Drawing.StringFormat(System.Drawing.StringFormatFlags.LineLimit)
            cSF.Alignment = StringAlignment.Center

            'Pen Style
            Dim SP1 As New Pen(Color.Black, 0.5)
            Dim SP2 As New Pen(Color.Black, 1)
            Dim SP3 As New Pen(Color.Black, 2)

            Dim il, it, pw, ph, ct, ch As Single

            il = e.MarginBounds.X
            it = e.MarginBounds.Y
            pw = e.MarginBounds.Width
            ph = e.MarginBounds.Height

            Dim lines, chars As Double
            Static bs As Integer = 0
            Static pg As Integer = 1
            Static curpg As Integer = 1
            Static lastpgprinted As Boolean = False

            Dim l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11 As Single

            l1 = il
            l2 = l1 + 50
            l3 = l2 + 200
            l4 = l3 + 80
            l5 = l4 + 80
            l6 = l5 + 80
            l7 = l6 + 80
            l8 = l7 + 80
            l9 = l8 + 80
            l10 = l9 + 80
            l11 = il + pw

            ct = it ' + (30)
            ch = ph - (20)

            Dim pa As New System.Drawing.RectangleF(l1, ct, pw, ch)
            e.Graphics.DrawString("ADVANCED MATERIALS TECHNOLOGIES PTE LTD", f16b, Brushes.Black, pa, cSF)

            ct += 30
            pa = New System.Drawing.RectangleF(l1, ct, pw, ch)
            e.Graphics.DrawString("WORK ORDER ROUTING", f12b, Brushes.Black, pa, cSF)

            ct += 20
            pa = New System.Drawing.RectangleF(l1, ct, pw, ch)
            e.Graphics.DrawString("Date: " & Format(Now.Date, "dd-MMM-yyyy"), f8b, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(1000, 700, 1150, ch)
            e.Graphics.DrawString("LEGEND", f8bu, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(1000, 720, 1150, ch)
            e.Graphics.DrawString("CP = Completed", f8b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(1000, 740, 1150, ch)
            e.Graphics.DrawString("RJ = Rejected", f8b, Brushes.Black, pa, lSF)

            'two point define one line.
            e.Graphics.DrawLine(SP1, l1, ct + 15, 1150, ct + 15)

            ct += 20
            pa = New System.Drawing.RectangleF(l1, ct, 150, ch)
            e.Graphics.DrawString("Work Order No.", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 150, ct, 10, ch)
            e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 190, ct, 190, ch)
            e.Graphics.DrawString("*" & wkono & "*", f8bar, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(l1 + 600, ct, 100, ch)
            e.Graphics.DrawString("FG Code", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 700, ct, 10, ch)
            e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 710, ct, 150, ch)
            e.Graphics.DrawString(wkofgcode, f10b, Brushes.Black, pa, lSF)

            ct += 20


            pa = New System.Drawing.RectangleF(l1 + 600, ct, 100, ch)
            e.Graphics.DrawString("DWG No.", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 700, ct, 10, ch)
            e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 710, ct, 150, ch)
            e.Graphics.DrawString(wkoptno, f10b, Brushes.Black, pa, lSF)

            ct += 20


            e.Graphics.DrawString(wkoqty, f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1, ct, 150, ch)
            e.Graphics.DrawString("Material", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 150, ct, 10, ch)
            e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 160, ct, 150, ch)
            e.Graphics.DrawString(wkomtr, f10b, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(l1 + 600, ct, 100, ch)
            e.Graphics.DrawString("QP Rev No.", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 700, ct, 10, ch)
            e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 710, ct, 150, ch)
            e.Graphics.DrawString(wkorevno, f10b, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(l1 + 900, ct, 100, ch)
            e.Graphics.DrawString("Effective Date", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 950, ct, 10, ch)
            e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 960, ct, 150, ch)
            e.Graphics.DrawString(wkoeftdate, f10b, Brushes.Black, pa, lSF)

            ct += 20
            'pa = New System.Drawing.RectangleF(l1, ct, 150, ch)
            'e.Graphics.DrawString("", f10b, Brushes.Black, pa, lSF)
            'pa = New System.Drawing.RectangleF(l1, ct, 150, ch)
            'e.Graphics.DrawString("", f10b, Brushes.Black, pa, lSF)
            'pa = New System.Drawing.RectangleF(l1, ct, 150, ch)
            'e.Graphics.DrawString("", f10b, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(l1, ct, 150, ch)
            e.Graphics.DrawString("Total Qty.", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 150, ct, 10, ch)
            e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 160, ct, 150, ch)
            e.Graphics.DrawString(wkoqty, f10b, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(l1 + 600, ct, 100, ch)
            e.Graphics.DrawString("Part Name", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 700, ct, 10, ch)
            e.Graphics.DrawString(":", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l1 + 710, ct, 150, ch)
            e.Graphics.DrawString(wkoprname, f10b, Brushes.Black, pa, lSF)

            e.Graphics.DrawLine(SP1, l1, ct + 20, 1150, ct + 20)

            ct += f10bu.Height * 2.4
            ch -= f10bu.Height * 2.4

            pa = New System.Drawing.RectangleF(l1, ct, l2 - l1, ch)
            e.Graphics.DrawString("OP", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l2, ct, l3 - l2, ch)
            e.Graphics.DrawString("Work Centre Name", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l3 - 120, ct, 0, ch)
            e.Graphics.DrawString("", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l4 - 60, ct, l5 - l4 + 60, ch)
            e.Graphics.DrawString("Start Date/Time", f10b, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(l5 - 20, ct, l6 - l5 + 60, ch)
            e.Graphics.DrawString("End Date/Time", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l6, ct, l7 - l6, ch)
            e.Graphics.DrawString("Total Hrs", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l7, ct, l8 - l7, ch)
            e.Graphics.DrawString("CP", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l8, ct, l9 - l8, ch)
            e.Graphics.DrawString("RJ", f10b, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(l9, ct, l10 - l9, ch)
            e.Graphics.DrawString("Emp ID", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l10, ct, l11 - l10, ch)
            e.Graphics.DrawString("OP Completed(0/1)", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l10 + 150, ct, 100, ch)
            e.Graphics.DrawString("Remark", f10b, Brushes.Black, pa, lSF)
            pa = New System.Drawing.RectangleF(l10 + 250, ct, 100, ch)
            e.Graphics.DrawString("Ctl.Pt.", f10b, Brushes.Black, pa, lSF)

            pa = New System.Drawing.RectangleF(il, it, pw, ph)
            e.Graphics.DrawLine(SP1, l1, ct + 20, 1150, ct + 20)

            ct += f10bu.Height * 2.4
            ch -= f10bu.Height * 2.4

            Dim j As Integer
            Dim prtgry As Boolean = False

            For j = bs To ds.Tables(0).Rows.Count - 1
                Dim tlin As Integer
                prtgry = Not prtgry

                With ds.Tables(0).Rows(j)
                    'measure the maximumn lines
                    e.Graphics.MeasureString(.Item("Work Centre Name"), f10, New SizeF(l5 - l4, ch), lSF, chars, lines)

                    If lines = "0" Then
                        pa = New System.Drawing.RectangleF(l1, ct - 5, 1100, f16b.Height * 1)
                    Else
                        pa = New System.Drawing.RectangleF(l1, ct - 5, 1100, f16b.Height * lines)
                    End If

                    If prtgry = True Then
                        e.Graphics.FillRectangle(Brushes.LightGray, pa)
                    End If

                    pa = New System.Drawing.RectangleF(l1, ct, l2 - l1, ch)
                    e.Graphics.DrawString(.Item("OP"), f10, Brushes.Black, pa, cSF)
                    pa = New System.Drawing.RectangleF(l2, ct, l3 - l2, ch)
                    e.Graphics.DrawString(.Item("Work Centre Name"), f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l3 - 120, ct, 0, ch)
                    e.Graphics.DrawString("", f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l4 - 60, ct, l5 - l4 + 60, ch)
                    e.Graphics.DrawString(.Item("Start Time"), f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l5 - 20, ct, l6 - l5 + 60, ch)
                    e.Graphics.DrawString(.Item("End Time"), f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l6, ct, l7 - l6, ch)
                    e.Graphics.DrawString(.Item("Total Hrs"), f10, Brushes.Black, pa, cSF)

                    pa = New System.Drawing.RectangleF(l7, ct, l8 - l7, ch)
                    e.Graphics.DrawString(.Item("CP"), f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l8, ct, l9 - l8, ch)
                    e.Graphics.DrawString(.Item("RJ"), f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l9, ct, l10 - l9, ch)
                    e.Graphics.DrawString(.Item("Emp ID"), f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l10, ct, l11 - l10, ch)
                    e.Graphics.DrawString(.Item("OP Completed"), f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l10 + 150, ct, 100, ch)
                    e.Graphics.DrawString(.Item("Remark"), f10, Brushes.Black, pa, lSF)
                    pa = New System.Drawing.RectangleF(l10 + 250, ct, 100, ch)
                    e.Graphics.DrawString(.Item("CtlPt"), f10, Brushes.Black, pa, lSF)

                    If lines = "0" Then
                        tlin = "1"
                        ct += f10.Height * (tlin + 0.5)
                        ch -= f10.Height * (tlin + 0.5)
                    Else
                        tlin = lines
                        ct += f10.Height * (tlin + 0.5)
                        ch -= f10.Height * (tlin + 0.5)
                    End If
                End With

                If ct >= (it + ph) - f10.Height Then
                    Exit For
                End If

            Next

            bs = j + 1
            If j >= ds.Tables(0).Rows.Count Then
                If ct >= (it + ph) - (f10.Height) Then
                    pg = 1
                End If
                If lastpgprinted = False Then
                    If ct >= (it + ph) - (f10.Height * 14) Then
                        pg = 1
                    Else
                        '' 'Remove the total
                        ''e.Graphics.DrawLine(SP1, l1, ct, l11 - l1, ct)
                        'e.Graphics.DrawLine(SP1, l1, ct, 1150, ct)

                        ct += f10b.Height * 0.4
                        ch -= f10b.Height * 0.4
                        ''e.Graphics.DrawLine(SP2, l1, ct, l11 - l1, ct)
                        'e.Graphics.DrawLine(SP2, l1, ct, 1150, ct)
                        ct += f10b.Height * 0.4
                        ch -= f10b.Height * 0.4

                        ct += f10b.Height * 2
                        ch -= f10b.Height * 2
                        ''e.Graphics.DrawLine(SP1, l1, ct, l11 - l1, ct)
                        'e.Graphics.DrawLine(SP1, l1, ct, 1150, ct)
                        ct += f10b.Height * 0.4
                        ch -= f10b.Height * 0.4
                        'e.Graphics.DrawLine(SP2, l1, ct, 1150, ct)
                        ''e.Graphics.DrawLine(SP2, l1, ct, l11 - l1, ct)
                        ct += f10b.Height * 4
                        ch -= f10b.Height * 4

                        lastpgprinted = True
                        pg = 3
                    End If
                End If
            End If

            pa = New RectangleF(l1, (il + ph) - 50, l11 - l1, f10.Height * 2)
            e.Graphics.DrawString("Page " & curpg, f10, Brushes.Black, pa, cSF)
            curpg += 1

            If pg = 1 Then
                PPC1.Rows += 1
                e.HasMorePages = True
            Else
                bs = 0
                pg = 1
                curpg = 1
                e.HasMorePages = False
                lastpgprinted = False
            End If
        Catch ex As Exception
            MsgBox("There was an error preparing the document." & vbCrLf & vbCrLf & "Error Message :" & vbCrLf & ex.Message, MsgBoxStyle.Exclamation, "Preparation Error")
        End Try
    End Sub

End Class