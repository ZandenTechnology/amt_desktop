<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAMTprintpreview
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PPC1 = New System.Windows.Forms.PrintPreviewControl
        Me.PDamt = New System.Drawing.Printing.PrintDocument
        Me.SuspendLayout()
        '
        'PPC1
        '
        Me.PPC1.AutoZoom = False
        Me.PPC1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PPC1.Location = New System.Drawing.Point(0, 0)
        Me.PPC1.Name = "PPC1"
        Me.PPC1.Size = New System.Drawing.Size(314, 241)
        Me.PPC1.TabIndex = 2
        Me.PPC1.Zoom = 1
        '
        'PDamt
        '
        '
        'frmAMTprintpreview
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(297, 223)
        Me.ControlBox = False
        Me.Controls.Add(Me.PPC1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmAMTprintpreview"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "AMT PRINT"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PPC1 As System.Windows.Forms.PrintPreviewControl
    Friend WithEvents PDamt As System.Drawing.Printing.PrintDocument
End Class
