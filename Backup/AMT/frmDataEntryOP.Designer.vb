<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDataEntryOP
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDataEntryOP))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.lstv = New System.Windows.Forms.ListView
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtTansID = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.lbl1 = New System.Windows.Forms.Label
        Me.Label34 = New System.Windows.Forms.Label
        Me.txtEmpName = New System.Windows.Forms.TextBox
        Me.txtEmpid = New System.Windows.Forms.TextBox
        Me.Label29 = New System.Windows.Forms.Label
        Me.txtC = New System.Windows.Forms.TextBox
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.txtOPQTY1 = New System.Windows.Forms.TextBox
        Me.pic1 = New System.Windows.Forms.PictureBox
        Me.lbl7 = New System.Windows.Forms.Label
        Me.txtHandoverid = New System.Windows.Forms.TextBox
        Me.lbl6 = New System.Windows.Forms.Label
        Me.txtHandoverName = New System.Windows.Forms.TextBox
        Me.lbl5 = New System.Windows.Forms.Label
        Me.lbl4 = New System.Windows.Forms.Label
        Me.lbl3 = New System.Windows.Forms.Label
        Me.lbl2 = New System.Windows.Forms.Label
        Me.lblSplit = New System.Windows.Forms.Label
        Me.txtSubval = New System.Windows.Forms.TextBox
        Me.txtSpStatus = New System.Windows.Forms.TextBox
        Me.txtSufParent = New System.Windows.Forms.TextBox
        Me.txtsuf = New System.Windows.Forms.TextBox
        Me.PictureBox4 = New System.Windows.Forms.PictureBox
        Me.PictureBox3 = New System.Windows.Forms.PictureBox
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.txtWKSatation = New System.Windows.Forms.TextBox
        Me.txtMachineid = New System.Windows.Forms.TextBox
        Me.Label31 = New System.Windows.Forms.Label
        Me.lblTotOPQTY = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.lbltotRej = New System.Windows.Forms.Label
        Me.Label30 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.lbltotCom = New System.Windows.Forms.Label
        Me.txtOPQTY = New System.Windows.Forms.TextBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.txtItemNoTemp = New System.Windows.Forms.TextBox
        Me.lblMsgBox = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.PictureBox5 = New System.Windows.Forms.PictureBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.txtRejqty3 = New System.Windows.Forms.TextBox
        Me.txtRejDes3 = New System.Windows.Forms.TextBox
        Me.txtRejCode3 = New System.Windows.Forms.TextBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.PictureBox7 = New System.Windows.Forms.PictureBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.txtRejqty2 = New System.Windows.Forms.TextBox
        Me.txtRejDes2 = New System.Windows.Forms.TextBox
        Me.txtRejCode2 = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.PictureBox6 = New System.Windows.Forms.PictureBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtRejqty1 = New System.Windows.Forms.TextBox
        Me.txtRejDes1 = New System.Windows.Forms.TextBox
        Me.txtRejCode1 = New System.Windows.Forms.TextBox
        Me.txtRejectedQty = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtCompletedQty = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtReleasedqty = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtEndTime = New System.Windows.Forms.TextBox
        Me.txtStartTime = New System.Windows.Forms.TextBox
        Me.txtEndDate = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtStartDate = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtOperators = New System.Windows.Forms.TextBox
        Me.txtItemNo = New System.Windows.Forms.TextBox
        Me.txtJobNo = New System.Windows.Forms.TextBox
        Me.txtOperationNo = New System.Windows.Forms.TextBox
        Me.txtWKSatationID = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Location = New System.Drawing.Point(-4, -3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(801, 600)
        Me.Panel1.TabIndex = 6
        '
        'Panel2
        '
        Me.Panel2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.lstv)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Controls.Add(Me.Panel4)
        Me.Panel2.Location = New System.Drawing.Point(68, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(730, 597)
        Me.Panel2.TabIndex = 0
        '
        'lstv
        '
        Me.lstv.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(3, 351)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(723, 243)
        Me.lstv.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lstv.TabIndex = 7
        Me.lstv.TabStop = False
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'Panel3
        '
        Me.Panel3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel3.BackColor = System.Drawing.Color.Khaki
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.txtTansID)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Location = New System.Drawing.Point(3, 9)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(724, 31)
        Me.Panel3.TabIndex = 36
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.Desktop
        Me.Label1.Location = New System.Drawing.Point(3, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(162, 13)
        Me.Label1.TabIndex = 37
        Me.Label1.Text = "OPERATION DATA ENTRY"
        '
        'txtTansID
        '
        Me.txtTansID.BackColor = System.Drawing.Color.White
        Me.txtTansID.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTansID.Location = New System.Drawing.Point(466, 6)
        Me.txtTansID.MaxLength = 50
        Me.txtTansID.Name = "txtTansID"
        Me.txtTansID.ReadOnly = True
        Me.txtTansID.Size = New System.Drawing.Size(10, 26)
        Me.txtTansID.TabIndex = 43
        Me.txtTansID.TabStop = False
        Me.txtTansID.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(479, 6)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(101, 13)
        Me.Label2.TabIndex = 38
        Me.Label2.Text = "SCAN (JOB NO.)"
        Me.Label2.Visible = False
        '
        'Panel4
        '
        Me.Panel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel4.Controls.Add(Me.lbl1)
        Me.Panel4.Controls.Add(Me.Label34)
        Me.Panel4.Controls.Add(Me.txtEmpName)
        Me.Panel4.Controls.Add(Me.txtEmpid)
        Me.Panel4.Controls.Add(Me.Label29)
        Me.Panel4.Controls.Add(Me.txtC)
        Me.Panel4.Controls.Add(Me.Panel5)
        Me.Panel4.Location = New System.Drawing.Point(3, 32)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(724, 313)
        Me.Panel4.TabIndex = 37
        '
        'lbl1
        '
        Me.lbl1.AutoSize = True
        Me.lbl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl1.Location = New System.Drawing.Point(546, 14)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(22, 13)
        Me.lbl1.TabIndex = 44
        Me.lbl1.Text = "(1)"
        Me.lbl1.Visible = False
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(195, 15)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(92, 13)
        Me.Label34.TabIndex = 72
        Me.Label34.Text = "Operator Name"
        '
        'txtEmpName
        '
        Me.txtEmpName.BackColor = System.Drawing.Color.White
        Me.txtEmpName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpName.Location = New System.Drawing.Point(287, 8)
        Me.txtEmpName.MaxLength = 50
        Me.txtEmpName.Name = "txtEmpName"
        Me.txtEmpName.ReadOnly = True
        Me.txtEmpName.Size = New System.Drawing.Size(254, 26)
        Me.txtEmpName.TabIndex = 42
        Me.txtEmpName.TabStop = False
        '
        'txtEmpid
        '
        Me.txtEmpid.BackColor = System.Drawing.Color.White
        Me.txtEmpid.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpid.Location = New System.Drawing.Point(90, 8)
        Me.txtEmpid.MaxLength = 50
        Me.txtEmpid.Name = "txtEmpid"
        Me.txtEmpid.ReadOnly = True
        Me.txtEmpid.Size = New System.Drawing.Size(99, 26)
        Me.txtEmpid.TabIndex = 41
        Me.txtEmpid.TabStop = False
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(12, 15)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(73, 13)
        Me.Label29.TabIndex = 40
        Me.Label29.Text = "Operator ID"
        '
        'txtC
        '
        Me.txtC.Location = New System.Drawing.Point(642, 7)
        Me.txtC.Name = "txtC"
        Me.txtC.Size = New System.Drawing.Size(0, 20)
        Me.txtC.TabIndex = 1
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel5.Controls.Add(Me.txtOPQTY1)
        Me.Panel5.Controls.Add(Me.pic1)
        Me.Panel5.Controls.Add(Me.lbl7)
        Me.Panel5.Controls.Add(Me.txtHandoverid)
        Me.Panel5.Controls.Add(Me.lbl6)
        Me.Panel5.Controls.Add(Me.txtHandoverName)
        Me.Panel5.Controls.Add(Me.lbl5)
        Me.Panel5.Controls.Add(Me.lbl4)
        Me.Panel5.Controls.Add(Me.lbl3)
        Me.Panel5.Controls.Add(Me.lbl2)
        Me.Panel5.Controls.Add(Me.lblSplit)
        Me.Panel5.Controls.Add(Me.txtSubval)
        Me.Panel5.Controls.Add(Me.txtSpStatus)
        Me.Panel5.Controls.Add(Me.txtSufParent)
        Me.Panel5.Controls.Add(Me.txtsuf)
        Me.Panel5.Controls.Add(Me.PictureBox4)
        Me.Panel5.Controls.Add(Me.PictureBox3)
        Me.Panel5.Controls.Add(Me.PictureBox2)
        Me.Panel5.Controls.Add(Me.txtWKSatation)
        Me.Panel5.Controls.Add(Me.txtMachineid)
        Me.Panel5.Controls.Add(Me.Label31)
        Me.Panel5.Controls.Add(Me.lblTotOPQTY)
        Me.Panel5.Controls.Add(Me.Label13)
        Me.Panel5.Controls.Add(Me.lbltotRej)
        Me.Panel5.Controls.Add(Me.Label30)
        Me.Panel5.Controls.Add(Me.Label28)
        Me.Panel5.Controls.Add(Me.lbltotCom)
        Me.Panel5.Controls.Add(Me.txtOPQTY)
        Me.Panel5.Controls.Add(Me.Label26)
        Me.Panel5.Controls.Add(Me.Label27)
        Me.Panel5.Controls.Add(Me.Label25)
        Me.Panel5.Controls.Add(Me.Label24)
        Me.Panel5.Controls.Add(Me.Label23)
        Me.Panel5.Controls.Add(Me.txtItemNoTemp)
        Me.Panel5.Controls.Add(Me.lblMsgBox)
        Me.Panel5.Controls.Add(Me.GroupBox3)
        Me.Panel5.Controls.Add(Me.GroupBox2)
        Me.Panel5.Controls.Add(Me.GroupBox1)
        Me.Panel5.Controls.Add(Me.txtRejectedQty)
        Me.Panel5.Controls.Add(Me.Label12)
        Me.Panel5.Controls.Add(Me.txtCompletedQty)
        Me.Panel5.Controls.Add(Me.Label11)
        Me.Panel5.Controls.Add(Me.txtReleasedqty)
        Me.Panel5.Controls.Add(Me.Label10)
        Me.Panel5.Controls.Add(Me.txtEndTime)
        Me.Panel5.Controls.Add(Me.txtStartTime)
        Me.Panel5.Controls.Add(Me.txtEndDate)
        Me.Panel5.Controls.Add(Me.Label9)
        Me.Panel5.Controls.Add(Me.txtStartDate)
        Me.Panel5.Controls.Add(Me.Label8)
        Me.Panel5.Controls.Add(Me.txtOperators)
        Me.Panel5.Controls.Add(Me.txtItemNo)
        Me.Panel5.Controls.Add(Me.txtJobNo)
        Me.Panel5.Controls.Add(Me.txtOperationNo)
        Me.Panel5.Controls.Add(Me.txtWKSatationID)
        Me.Panel5.Controls.Add(Me.Label7)
        Me.Panel5.Controls.Add(Me.Label6)
        Me.Panel5.Controls.Add(Me.Label5)
        Me.Panel5.Controls.Add(Me.Label4)
        Me.Panel5.Controls.Add(Me.Label3)
        Me.Panel5.Location = New System.Drawing.Point(11, 35)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(708, 275)
        Me.Panel5.TabIndex = 39
        '
        'txtOPQTY1
        '
        Me.txtOPQTY1.Location = New System.Drawing.Point(586, 226)
        Me.txtOPQTY1.Name = "txtOPQTY1"
        Me.txtOPQTY1.Size = New System.Drawing.Size(11, 20)
        Me.txtOPQTY1.TabIndex = 142
        Me.txtOPQTY1.Visible = False
        '
        'pic1
        '
        '  Me.pic1.Image = Global.AMT.My.Resources.Resources.buddysite_00204
        Me.pic1.Location = New System.Drawing.Point(-4, 226)
        Me.pic1.Name = "pic1"
        Me.pic1.Size = New System.Drawing.Size(48, 43)
        Me.pic1.TabIndex = 141
        Me.pic1.TabStop = False
        Me.pic1.Visible = False
        '
        'lbl7
        '
        Me.lbl7.AutoSize = True
        Me.lbl7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl7.Location = New System.Drawing.Point(550, 108)
        Me.lbl7.Name = "lbl7"
        Me.lbl7.Size = New System.Drawing.Size(22, 13)
        Me.lbl7.TabIndex = 140
        Me.lbl7.Text = "(7)"
        Me.lbl7.Visible = False
        '
        'txtHandoverid
        '
        Me.txtHandoverid.BackColor = System.Drawing.Color.White
        Me.txtHandoverid.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHandoverid.Location = New System.Drawing.Point(642, 108)
        Me.txtHandoverid.MaxLength = 50
        Me.txtHandoverid.Name = "txtHandoverid"
        Me.txtHandoverid.ReadOnly = True
        Me.txtHandoverid.Size = New System.Drawing.Size(16, 26)
        Me.txtHandoverid.TabIndex = 73
        Me.txtHandoverid.TabStop = False
        Me.txtHandoverid.Visible = False
        '
        'lbl6
        '
        Me.lbl6.AutoSize = True
        Me.lbl6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl6.Location = New System.Drawing.Point(685, 82)
        Me.lbl6.Name = "lbl6"
        Me.lbl6.Size = New System.Drawing.Size(22, 13)
        Me.lbl6.TabIndex = 139
        Me.lbl6.Text = "(6)"
        Me.lbl6.Visible = False
        '
        'txtHandoverName
        '
        Me.txtHandoverName.BackColor = System.Drawing.Color.White
        Me.txtHandoverName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHandoverName.Location = New System.Drawing.Point(661, 111)
        Me.txtHandoverName.MaxLength = 50
        Me.txtHandoverName.Name = "txtHandoverName"
        Me.txtHandoverName.ReadOnly = True
        Me.txtHandoverName.Size = New System.Drawing.Size(20, 26)
        Me.txtHandoverName.TabIndex = 75
        Me.txtHandoverName.TabStop = False
        Me.txtHandoverName.Visible = False
        '
        'lbl5
        '
        Me.lbl5.AutoSize = True
        Me.lbl5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl5.Location = New System.Drawing.Point(157, 137)
        Me.lbl5.Name = "lbl5"
        Me.lbl5.Size = New System.Drawing.Size(22, 13)
        Me.lbl5.TabIndex = 138
        Me.lbl5.Text = "(5)"
        Me.lbl5.Visible = False
        '
        'lbl4
        '
        Me.lbl4.AutoSize = True
        Me.lbl4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl4.Location = New System.Drawing.Point(182, 111)
        Me.lbl4.Name = "lbl4"
        Me.lbl4.Size = New System.Drawing.Size(22, 13)
        Me.lbl4.TabIndex = 137
        Me.lbl4.Text = "(4)"
        Me.lbl4.Visible = False
        '
        'lbl3
        '
        Me.lbl3.AutoSize = True
        Me.lbl3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl3.Location = New System.Drawing.Point(221, 59)
        Me.lbl3.Name = "lbl3"
        Me.lbl3.Size = New System.Drawing.Size(22, 13)
        Me.lbl3.TabIndex = 136
        Me.lbl3.Text = "(3)"
        Me.lbl3.Visible = False
        '
        'lbl2
        '
        Me.lbl2.AutoSize = True
        Me.lbl2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl2.Location = New System.Drawing.Point(229, 6)
        Me.lbl2.Name = "lbl2"
        Me.lbl2.Size = New System.Drawing.Size(22, 13)
        Me.lbl2.TabIndex = 135
        Me.lbl2.Text = "(2)"
        Me.lbl2.Visible = False
        '
        'lblSplit
        '
        Me.lblSplit.AutoSize = True
        Me.lblSplit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSplit.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblSplit.Location = New System.Drawing.Point(316, 7)
        Me.lblSplit.Name = "lblSplit"
        Me.lblSplit.Size = New System.Drawing.Size(96, 13)
        Me.lblSplit.TabIndex = 134
        Me.lblSplit.Text = "SCAN SPLIT ID"
        Me.lblSplit.Visible = False
        '
        'txtSubval
        '
        Me.txtSubval.Location = New System.Drawing.Point(210, 108)
        Me.txtSubval.Name = "txtSubval"
        Me.txtSubval.Size = New System.Drawing.Size(100, 20)
        Me.txtSubval.TabIndex = 133
        Me.txtSubval.Visible = False
        '
        'txtSpStatus
        '
        Me.txtSpStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSpStatus.Location = New System.Drawing.Point(210, 130)
        Me.txtSpStatus.MaxLength = 50
        Me.txtSpStatus.Name = "txtSpStatus"
        Me.txtSpStatus.Size = New System.Drawing.Size(57, 20)
        Me.txtSpStatus.TabIndex = 132
        Me.txtSpStatus.Text = "True"
        Me.txtSpStatus.Visible = False
        '
        'txtSufParent
        '
        Me.txtSufParent.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSufParent.Location = New System.Drawing.Point(253, 3)
        Me.txtSufParent.MaxLength = 50
        Me.txtSufParent.Name = "txtSufParent"
        Me.txtSufParent.Size = New System.Drawing.Size(57, 20)
        Me.txtSufParent.TabIndex = 131
        '
        'txtsuf
        '
        Me.txtsuf.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsuf.Location = New System.Drawing.Point(159, 55)
        Me.txtsuf.MaxLength = 50
        Me.txtsuf.Name = "txtsuf"
        Me.txtsuf.Size = New System.Drawing.Size(57, 20)
        Me.txtsuf.TabIndex = 130
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(671, 78)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(22, 20)
        Me.PictureBox4.TabIndex = 129
        Me.PictureBox4.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(520, 104)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(22, 20)
        Me.PictureBox3.TabIndex = 128
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(517, 133)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(22, 20)
        Me.PictureBox2.TabIndex = 127
        Me.PictureBox2.TabStop = False
        '
        'txtWKSatation
        '
        Me.txtWKSatation.BackColor = System.Drawing.Color.White
        Me.txtWKSatation.Location = New System.Drawing.Point(111, 81)
        Me.txtWKSatation.Name = "txtWKSatation"
        Me.txtWKSatation.ReadOnly = True
        Me.txtWKSatation.Size = New System.Drawing.Size(119, 20)
        Me.txtWKSatation.TabIndex = 126
        Me.txtWKSatation.TabStop = False
        '
        'txtMachineid
        '
        Me.txtMachineid.BackColor = System.Drawing.Color.White
        Me.txtMachineid.Location = New System.Drawing.Point(111, 107)
        Me.txtMachineid.Name = "txtMachineid"
        Me.txtMachineid.ReadOnly = True
        Me.txtMachineid.Size = New System.Drawing.Size(68, 20)
        Me.txtMachineid.TabIndex = 111
        Me.txtMachineid.TabStop = False
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(3, 111)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(72, 13)
        Me.Label31.TabIndex = 110
        Me.Label31.Text = "Machine ID"
        '
        'lblTotOPQTY
        '
        Me.lblTotOPQTY.AutoSize = True
        Me.lblTotOPQTY.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotOPQTY.Location = New System.Drawing.Point(426, 259)
        Me.lblTotOPQTY.Name = "lblTotOPQTY"
        Me.lblTotOPQTY.Size = New System.Drawing.Size(25, 13)
        Me.lblTotOPQTY.TabIndex = 109
        Me.lblTotOPQTY.Text = "0.0"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(327, 259)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(92, 13)
        Me.Label13.TabIndex = 108
        Me.Label13.Text = "Received Qty :"
        '
        'lbltotRej
        '
        Me.lbltotRej.AutoSize = True
        Me.lbltotRej.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltotRej.Location = New System.Drawing.Point(672, 259)
        Me.lbltotRej.Name = "lbltotRej"
        Me.lbltotRej.Size = New System.Drawing.Size(25, 13)
        Me.lbltotRej.TabIndex = 107
        Me.lbltotRej.Text = "0.0"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(513, 85)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(84, 13)
        Me.Label30.TabIndex = 75
        Me.Label30.Text = "Received Qty"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(583, 259)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(89, 13)
        Me.Label28.TabIndex = 104
        Me.Label28.Text = "Rejected Qty :"
        '
        'lbltotCom
        '
        Me.lbltotCom.AutoSize = True
        Me.lbltotCom.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltotCom.Location = New System.Drawing.Point(550, 259)
        Me.lbltotCom.Name = "lbltotCom"
        Me.lbltotCom.Size = New System.Drawing.Size(25, 13)
        Me.lbltotCom.TabIndex = 106
        Me.lbltotCom.Text = "0.0"
        '
        'txtOPQTY
        '
        Me.txtOPQTY.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtOPQTY.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtOPQTY.Location = New System.Drawing.Point(599, 81)
        Me.txtOPQTY.Name = "txtOPQTY"
        Me.txtOPQTY.Size = New System.Drawing.Size(69, 20)
        Me.txtOPQTY.TabIndex = 74
        Me.txtOPQTY.TabStop = False
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Red
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label26.Location = New System.Drawing.Point(640, 33)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(41, 12)
        Me.Label26.TabIndex = 73
        Me.Label26.Text = "HHMM"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(457, 259)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(97, 13)
        Me.Label27.TabIndex = 103
        Me.Label27.Text = "Completed Qty :"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Red
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label25.Location = New System.Drawing.Point(640, 59)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(41, 12)
        Me.Label25.TabIndex = 72
        Me.Label25.Text = "HHMM"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Red
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label24.Location = New System.Drawing.Point(515, 59)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(73, 12)
        Me.Label24.TabIndex = 71
        Me.Label24.Text = "DD/MM/YYYY"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Red
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label23.Location = New System.Drawing.Point(515, 33)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(73, 12)
        Me.Label23.TabIndex = 70
        Me.Label23.Text = "DD/MM/YYYY"
        '
        'txtItemNoTemp
        '
        Me.txtItemNoTemp.Location = New System.Drawing.Point(271, 29)
        Me.txtItemNoTemp.Name = "txtItemNoTemp"
        Me.txtItemNoTemp.Size = New System.Drawing.Size(26, 20)
        Me.txtItemNoTemp.TabIndex = 69
        Me.txtItemNoTemp.Visible = False
        '
        'lblMsgBox
        '
        Me.lblMsgBox.AutoSize = True
        Me.lblMsgBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsgBox.ForeColor = System.Drawing.Color.DarkRed
        Me.lblMsgBox.Location = New System.Drawing.Point(50, 232)
        Me.lblMsgBox.Name = "lblMsgBox"
        Me.lblMsgBox.Size = New System.Drawing.Size(174, 13)
        Me.lblMsgBox.TabIndex = 68
        Me.lblMsgBox.Text = "TRANSACTION HISTORY FO"
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Ivory
        Me.GroupBox3.Controls.Add(Me.PictureBox5)
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Controls.Add(Me.Label21)
        Me.GroupBox3.Controls.Add(Me.Label22)
        Me.GroupBox3.Controls.Add(Me.txtRejqty3)
        Me.GroupBox3.Controls.Add(Me.txtRejDes3)
        Me.GroupBox3.Controls.Add(Me.txtRejCode3)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(473, 159)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(216, 67)
        Me.GroupBox3.TabIndex = 66
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "REJECTED #3"
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(194, 12)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(22, 20)
        Me.PictureBox5.TabIndex = 130
        Me.PictureBox5.TabStop = False
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(-1, 40)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(60, 13)
        Me.Label20.TabIndex = 57
        Me.Label20.Text = "Description"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(117, 19)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(23, 13)
        Me.Label21.TabIndex = 56
        Me.Label21.Text = "Qty"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(-1, 19)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(32, 13)
        Me.Label22.TabIndex = 55
        Me.Label22.Text = "Code"
        '
        'txtRejqty3
        '
        Me.txtRejqty3.Location = New System.Drawing.Point(142, 16)
        Me.txtRejqty3.Name = "txtRejqty3"
        Me.txtRejqty3.Size = New System.Drawing.Size(49, 18)
        Me.txtRejqty3.TabIndex = 54
        '
        'txtRejDes3
        '
        Me.txtRejDes3.BackColor = System.Drawing.Color.White
        Me.txtRejDes3.Location = New System.Drawing.Point(62, 37)
        Me.txtRejDes3.Name = "txtRejDes3"
        Me.txtRejDes3.ReadOnly = True
        Me.txtRejDes3.Size = New System.Drawing.Size(129, 18)
        Me.txtRejDes3.TabIndex = 53
        Me.txtRejDes3.TabStop = False
        '
        'txtRejCode3
        '
        Me.txtRejCode3.BackColor = System.Drawing.Color.White
        Me.txtRejCode3.Location = New System.Drawing.Point(62, 16)
        Me.txtRejCode3.Name = "txtRejCode3"
        Me.txtRejCode3.ReadOnly = True
        Me.txtRejCode3.Size = New System.Drawing.Size(51, 18)
        Me.txtRejCode3.TabIndex = 52
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Ivory
        Me.GroupBox2.Controls.Add(Me.PictureBox7)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.txtRejqty2)
        Me.GroupBox2.Controls.Add(Me.txtRejDes2)
        Me.GroupBox2.Controls.Add(Me.txtRejCode2)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(252, 159)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(216, 67)
        Me.GroupBox2.TabIndex = 65
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "REJECTED #2"
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(191, 12)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(22, 20)
        Me.PictureBox7.TabIndex = 132
        Me.PictureBox7.TabStop = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(0, 40)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(60, 13)
        Me.Label17.TabIndex = 57
        Me.Label17.Text = "Description"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(111, 19)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(26, 13)
        Me.Label18.TabIndex = 56
        Me.Label18.Text = " Qty"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(-1, 19)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(32, 13)
        Me.Label19.TabIndex = 55
        Me.Label19.Text = "Code"
        '
        'txtRejqty2
        '
        Me.txtRejqty2.Location = New System.Drawing.Point(141, 16)
        Me.txtRejqty2.Name = "txtRejqty2"
        Me.txtRejqty2.Size = New System.Drawing.Size(49, 18)
        Me.txtRejqty2.TabIndex = 54
        '
        'txtRejDes2
        '
        Me.txtRejDes2.BackColor = System.Drawing.Color.White
        Me.txtRejDes2.Location = New System.Drawing.Point(61, 37)
        Me.txtRejDes2.Name = "txtRejDes2"
        Me.txtRejDes2.ReadOnly = True
        Me.txtRejDes2.Size = New System.Drawing.Size(129, 18)
        Me.txtRejDes2.TabIndex = 53
        Me.txtRejDes2.TabStop = False
        '
        'txtRejCode2
        '
        Me.txtRejCode2.BackColor = System.Drawing.Color.White
        Me.txtRejCode2.Location = New System.Drawing.Point(61, 16)
        Me.txtRejCode2.Name = "txtRejCode2"
        Me.txtRejCode2.ReadOnly = True
        Me.txtRejCode2.Size = New System.Drawing.Size(51, 18)
        Me.txtRejCode2.TabIndex = 52
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Ivory
        Me.GroupBox1.Controls.Add(Me.PictureBox6)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.txtRejqty1)
        Me.GroupBox1.Controls.Add(Me.txtRejDes1)
        Me.GroupBox1.Controls.Add(Me.txtRejCode1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(30, 159)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(216, 67)
        Me.GroupBox1.TabIndex = 64
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "REJECTED #1"
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(194, 12)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(22, 20)
        Me.PictureBox6.TabIndex = 131
        Me.PictureBox6.TabStop = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(-1, 40)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(60, 13)
        Me.Label16.TabIndex = 51
        Me.Label16.Text = "Description"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(118, 19)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(23, 13)
        Me.Label15.TabIndex = 50
        Me.Label15.Text = "Qty"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(-1, 19)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(32, 13)
        Me.Label14.TabIndex = 49
        Me.Label14.Text = "Code"
        '
        'txtRejqty1
        '
        Me.txtRejqty1.Location = New System.Drawing.Point(141, 16)
        Me.txtRejqty1.Name = "txtRejqty1"
        Me.txtRejqty1.Size = New System.Drawing.Size(49, 18)
        Me.txtRejqty1.TabIndex = 48
        '
        'txtRejDes1
        '
        Me.txtRejDes1.BackColor = System.Drawing.Color.White
        Me.txtRejDes1.Location = New System.Drawing.Point(60, 37)
        Me.txtRejDes1.Name = "txtRejDes1"
        Me.txtRejDes1.ReadOnly = True
        Me.txtRejDes1.Size = New System.Drawing.Size(129, 18)
        Me.txtRejDes1.TabIndex = 47
        Me.txtRejDes1.TabStop = False
        '
        'txtRejCode1
        '
        Me.txtRejCode1.BackColor = System.Drawing.Color.White
        Me.txtRejCode1.Location = New System.Drawing.Point(60, 16)
        Me.txtRejCode1.Name = "txtRejCode1"
        Me.txtRejCode1.ReadOnly = True
        Me.txtRejCode1.Size = New System.Drawing.Size(51, 18)
        Me.txtRejCode1.TabIndex = 46
        '
        'txtRejectedQty
        '
        Me.txtRejectedQty.Location = New System.Drawing.Point(442, 133)
        Me.txtRejectedQty.Name = "txtRejectedQty"
        Me.txtRejectedQty.Size = New System.Drawing.Size(69, 20)
        Me.txtRejectedQty.TabIndex = 63
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(328, 137)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(81, 13)
        Me.Label12.TabIndex = 62
        Me.Label12.Text = "Rejected Qty"
        '
        'txtCompletedQty
        '
        Me.txtCompletedQty.Location = New System.Drawing.Point(442, 107)
        Me.txtCompletedQty.Name = "txtCompletedQty"
        Me.txtCompletedQty.Size = New System.Drawing.Size(69, 20)
        Me.txtCompletedQty.TabIndex = 61
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(328, 111)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(89, 13)
        Me.Label11.TabIndex = 60
        Me.Label11.Text = "Completed Qty"
        '
        'txtReleasedqty
        '
        Me.txtReleasedqty.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtReleasedqty.Location = New System.Drawing.Point(442, 81)
        Me.txtReleasedqty.Name = "txtReleasedqty"
        Me.txtReleasedqty.ReadOnly = True
        Me.txtReleasedqty.Size = New System.Drawing.Size(69, 20)
        Me.txtReleasedqty.TabIndex = 59
        Me.txtReleasedqty.TabStop = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(328, 85)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(83, 13)
        Me.Label10.TabIndex = 58
        Me.Label10.Text = "Released Qty"
        '
        'txtEndTime
        '
        Me.txtEndTime.BackColor = System.Drawing.Color.White
        Me.txtEndTime.Location = New System.Drawing.Point(599, 55)
        Me.txtEndTime.MaxLength = 4
        Me.txtEndTime.Name = "txtEndTime"
        Me.txtEndTime.ReadOnly = True
        Me.txtEndTime.Size = New System.Drawing.Size(37, 20)
        Me.txtEndTime.TabIndex = 55
        Me.txtEndTime.Text = " "
        '
        'txtStartTime
        '
        Me.txtStartTime.BackColor = System.Drawing.Color.White
        Me.txtStartTime.Location = New System.Drawing.Point(599, 29)
        Me.txtStartTime.MaxLength = 4
        Me.txtStartTime.Name = "txtStartTime"
        Me.txtStartTime.ReadOnly = True
        Me.txtStartTime.Size = New System.Drawing.Size(37, 20)
        Me.txtStartTime.TabIndex = 54
        '
        'txtEndDate
        '
        Me.txtEndDate.BackColor = System.Drawing.Color.White
        Me.txtEndDate.Location = New System.Drawing.Point(442, 55)
        Me.txtEndDate.MaxLength = 10
        Me.txtEndDate.Name = "txtEndDate"
        Me.txtEndDate.ReadOnly = True
        Me.txtEndDate.Size = New System.Drawing.Size(69, 20)
        Me.txtEndDate.TabIndex = 53
        Me.txtEndDate.Text = " "
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(328, 59)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(111, 13)
        Me.Label9.TabIndex = 52
        Me.Label9.Text = "Date/Time Endted"
        '
        'txtStartDate
        '
        Me.txtStartDate.BackColor = System.Drawing.Color.White
        Me.txtStartDate.Location = New System.Drawing.Point(442, 29)
        Me.txtStartDate.MaxLength = 10
        Me.txtStartDate.Name = "txtStartDate"
        Me.txtStartDate.ReadOnly = True
        Me.txtStartDate.Size = New System.Drawing.Size(69, 20)
        Me.txtStartDate.TabIndex = 51
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(328, 33)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(112, 13)
        Me.Label8.TabIndex = 50
        Me.Label8.Text = "Date/Time Started"
        '
        'txtOperators
        '
        Me.txtOperators.BackColor = System.Drawing.Color.White
        Me.txtOperators.Location = New System.Drawing.Point(111, 133)
        Me.txtOperators.Name = "txtOperators"
        Me.txtOperators.Size = New System.Drawing.Size(42, 20)
        Me.txtOperators.TabIndex = 48
        '
        'txtItemNo
        '
        Me.txtItemNo.BackColor = System.Drawing.Color.White
        Me.txtItemNo.Location = New System.Drawing.Point(111, 29)
        Me.txtItemNo.Name = "txtItemNo"
        Me.txtItemNo.ReadOnly = True
        Me.txtItemNo.Size = New System.Drawing.Size(156, 20)
        Me.txtItemNo.TabIndex = 47
        '
        'txtJobNo
        '
        Me.txtJobNo.BackColor = System.Drawing.Color.White
        Me.txtJobNo.Location = New System.Drawing.Point(111, 3)
        Me.txtJobNo.Name = "txtJobNo"
        Me.txtJobNo.ReadOnly = True
        Me.txtJobNo.Size = New System.Drawing.Size(117, 20)
        Me.txtJobNo.TabIndex = 46
        Me.txtJobNo.TabStop = False
        '
        'txtOperationNo
        '
        Me.txtOperationNo.BackColor = System.Drawing.Color.White
        Me.txtOperationNo.Location = New System.Drawing.Point(111, 55)
        Me.txtOperationNo.Name = "txtOperationNo"
        Me.txtOperationNo.ReadOnly = True
        Me.txtOperationNo.Size = New System.Drawing.Size(42, 20)
        Me.txtOperationNo.TabIndex = 45
        Me.txtOperationNo.TabStop = False
        '
        'txtWKSatationID
        '
        Me.txtWKSatationID.BackColor = System.Drawing.Color.White
        Me.txtWKSatationID.Location = New System.Drawing.Point(232, 81)
        Me.txtWKSatationID.Name = "txtWKSatationID"
        Me.txtWKSatationID.ReadOnly = True
        Me.txtWKSatationID.Size = New System.Drawing.Size(79, 20)
        Me.txtWKSatationID.TabIndex = 44
        Me.txtWKSatationID.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 59)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(86, 13)
        Me.Label7.TabIndex = 43
        Me.Label7.Text = "Operation No."
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 137)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(101, 13)
        Me.Label6.TabIndex = 42
        Me.Label6.Text = "No. of Operators"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 33)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 13)
        Me.Label5.TabIndex = 41
        Me.Label5.Text = "Item No."
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 85)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 13)
        Me.Label4.TabIndex = 40
        Me.Label4.Text = "Work Center"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(51, 13)
        Me.Label3.TabIndex = 39
        Me.Label3.Text = "Job No."
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = CType(resources.GetObject("PictureBox1.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(4, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(77, 597)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'Timer2
        '
        Me.Timer2.Enabled = True
        Me.Timer2.Interval = 1000
        '
        'frmDataEntryOP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Ivory
        Me.ClientSize = New System.Drawing.Size(794, 594)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmDataEntryOP"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lbltotRej As System.Windows.Forms.Label
    Friend WithEvents lbltotCom As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents lstv As System.Windows.Forms.ListView
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents txtEmpid As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txtC As System.Windows.Forms.TextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtItemNoTemp As System.Windows.Forms.TextBox
    Friend WithEvents lblMsgBox As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtRejqty3 As System.Windows.Forms.TextBox
    Friend WithEvents txtRejDes3 As System.Windows.Forms.TextBox
    Friend WithEvents txtRejCode3 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtRejqty2 As System.Windows.Forms.TextBox
    Friend WithEvents txtRejDes2 As System.Windows.Forms.TextBox
    Friend WithEvents txtRejCode2 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtRejqty1 As System.Windows.Forms.TextBox
    Friend WithEvents txtRejDes1 As System.Windows.Forms.TextBox
    Friend WithEvents txtRejCode1 As System.Windows.Forms.TextBox
    Friend WithEvents txtRejectedQty As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtCompletedQty As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtReleasedqty As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtEndTime As System.Windows.Forms.TextBox
    Friend WithEvents txtStartTime As System.Windows.Forms.TextBox
    Friend WithEvents txtEndDate As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtStartDate As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtOperators As System.Windows.Forms.TextBox
    Friend WithEvents txtItemNo As System.Windows.Forms.TextBox
    Friend WithEvents txtJobNo As System.Windows.Forms.TextBox
    Friend WithEvents txtOperationNo As System.Windows.Forms.TextBox
    Friend WithEvents txtWKSatationID As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtOPQTY As System.Windows.Forms.TextBox
    Friend WithEvents txtEmpName As System.Windows.Forms.TextBox
    Friend WithEvents txtTansID As System.Windows.Forms.TextBox
    Friend WithEvents lblTotOPQTY As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtMachineid As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txtHandoverid As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txtHandoverName As System.Windows.Forms.TextBox
    Friend WithEvents txtWKSatation As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents txtsuf As System.Windows.Forms.TextBox
    Friend WithEvents txtSufParent As System.Windows.Forms.TextBox
    Friend WithEvents txtSubval As System.Windows.Forms.TextBox
    Friend WithEvents txtSpStatus As System.Windows.Forms.TextBox
    Friend WithEvents lblSplit As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents lbl1 As System.Windows.Forms.Label
    Friend WithEvents lbl7 As System.Windows.Forms.Label
    Friend WithEvents lbl6 As System.Windows.Forms.Label
    Friend WithEvents lbl5 As System.Windows.Forms.Label
    Friend WithEvents lbl4 As System.Windows.Forms.Label
    Friend WithEvents lbl3 As System.Windows.Forms.Label
    Friend WithEvents lbl2 As System.Windows.Forms.Label
    Friend WithEvents pic1 As System.Windows.Forms.PictureBox
    Friend WithEvents txtOPQTY1 As System.Windows.Forms.TextBox
End Class
