Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.DateAndTime
Public Class frmSupJobEdit
    Dim parm As SqlParameter
    Dim i As Integer
    Dim intMaxroute As Integer
    Dim intMinroute As Integer
    Dim StrJobNo As String
    Dim ACJobNo As String
    Dim tansid As Integer
    Dim clsM As New clsMain
    Dim opst As Boolean = False
    Dim CheckSplitst As Boolean
    Dim PUJOBNo As String
    Dim RlQty As Double
    Dim Rlrej As Double
    Dim RlOP As Double
    Private Sub ButSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButSearch.Click
        txtClearAll()

        If Trim(txtJobNo.Text) = "" Then
            MsgBox("Please enter the job no.!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If

        txtJobNo.Text = txtJobNo.Text.Split("-")(0)

        Dim s() As String = Split(Trim(txtJobNo.Text), "-")
        ACJobNo = s(0)

        If GetJobData(ACJobNo) Then
        Else
            MsgBox("This job no. is not correct!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If


        If GetJobRoute(ACJobNo) Then
        Else
            MsgBox("This is no job route for this job no. Please check your admin!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If


    End Sub

    Private Sub frmSupJobEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ActionAdd("COMPLETED")
        GetOperator()
        cmbAction.SelectedIndex = 0
        txtClearAll()
        With lstv
            .Columns.Add("RJ-Code", 100, HorizontalAlignment.Left)
            .Columns.Add("Desc.", lstv.Width - 201, HorizontalAlignment.Left)
            .Columns.Add("QTY", 100, HorizontalAlignment.Left)
        End With
    End Sub
    Sub txtClearAll()
        butPrint.Visible = False
        ButPrintHis.Visible = False
        ActionAdd("COMPLETED")
        tansid = 0
        StrJobNo = ""
        txtsuf.Text = ""
        lstJobRoute.Items.Clear()
        lstJobNo.Items.Clear()
        lblItemcode.Text = ""
        lblACRlQ.Text = ""
        lblDesc.Text = ""
        intMinroute = 0
        intMaxroute = 0
        txtStart.Text = ""
        txtEnd.Text = ""
        txtOPNo.Text = ""
        txtWorkCenter.Text = ""
        txtOPRlq.Text = ""
        'cmbMachine.SelectedIndex = 0
        cmbOPerator.SelectedIndex = 0
        cmbName.SelectedIndex = 0
        txtOperatorName.Text = ""
        txtNoofOperator.Text = ""
        txtReceived.Text = ""
        txtCompleted.Text = ""
        txtRejected.Text = ""
        txtStart.Text = ""
        txtEnd.Text = ""
        cmbAction.Text = ""
        lblStatus.Text = ""
        txtIDNo.Text = ""
        txtParent.Text = ""
        txtTot.Text = ""
        txtRemarks.Text = ""
        lstv.Items.Clear()
        Newhash.Clear()
        lblExport.Visible = False
        txtExport.Visible = False
        lbReWork.Visible = False
        butAdd.Visible = False
    End Sub
    Sub ClearLstBox()
        butPrint.Visible = False
        tansid = 0
        lstJobNo.Items.Clear()
        lstv.Items.Clear()
        txtStart.Text = ""
        txtEnd.Text = ""
        txtOPNo.Text = ""
        txtWorkCenter.Text = ""
        txtOPRlq.Text = ""
        'cmbMachine.SelectedIndex = 0
        cmbOPerator.SelectedIndex = 0
        cmbName.SelectedIndex = 0
        txtOperatorName.Text = ""
        txtNoofOperator.Text = ""
        txtReceived.Text = ""
        txtCompleted.Text = ""
        txtRejected.Text = ""
        txtStart.Text = ""
        txtEnd.Text = ""
        cmbAction.Text = ""
        lblStatus.Text = ""
        txtIDNo.Text = ""
        txtRemarks.Text = ""
        txtParent.Text = ""
        cmbAction.Items.Clear()
        cmbAction.Items.Add("-Select-")
        cmbMachine.Items.Clear()
        cmbMachine.Items.Add("-")
        cmbMachine.SelectedIndex = 0
        cmbAction.SelectedIndex = 0
        lblExport.Visible = False
        txtExport.Visible = False
        lbReWork.Visible = False
        butAdd.Visible = False
        txtTot.Text = ""
        Newhash.Clear()
    End Sub
    Function GetJobData(ByVal st As String) As Boolean
        ' txtClearAll()
        GetJobData = False
        Try
            com.Connection = cn
            com.CommandType = CommandType.Text
            com.CommandText = "select * from tbJob Inner join tbItem on tbJob._item=tbItem._itemCode where tbJob._job='" & RmQuo(st) & "'"
            Dim Ad As New SqlDataAdapter
            Dim ds As New DataSet
            Ad.SelectCommand = com
            Ad.Fill(ds)

            If ds.Tables(0).Rows.Count > 0 Then
                With ds.Tables(0).Rows(0)
                    GetJobData = True
                    lblItemcode.Text = .Item("_item")
                    lblDesc.Text = .Item("_itemdescription")
                    lblACRlQ.Text = Format(.Item("_qtyReleased"), "00.00")
                    lblJobDate.Text = Format(DateTime.FromOADate(.Item("_jobDate")), "dd/MM/yyyy")
                    ButPrintHis.Visible = True
                End With
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        Return GetJobData
    End Function
    Function GetJobRoute(ByVal st As String) As Boolean
        '  txtClearAll()
        GetJobRoute = False
        Try
            com.Connection = cn
            com.CommandType = CommandType.Text
            com.CommandText = "select _operationNo,_wc from tbJobRoute where _job='" & RmQuo(st) & "'"
            Dim Ad As New SqlDataAdapter
            Dim ds As New DataSet
            Ad.SelectCommand = com
            Ad.Fill(ds)
            If ds.Tables(0).Rows.Count > 0 Then
                intMinroute = ds.Tables(0).Rows(0).Item("_operationNo")
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    With ds.Tables(0).Rows(i)
                        lstJobRoute.Items.Add(New DataItem(Trim(.Item("_wc")), Trim(.Item("_operationNo"))))
                        intMaxroute = .Item("_operationNo")
                        GetJobRoute = True
                    End With
                Next
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        Return GetJobRoute
    End Function
    Function RmQuo(ByVal st As String) As String
        Dim ststr As String = ""
        If Trim(st) <> "" Then
            ststr = Trim(Replace(st, "'", "''"))
        End If
        Return ststr
    End Function
    Function CheckJobTrans(ByVal st As String) As Boolean
        CheckJobTrans = False
        lstJobNo.Items.Clear()
        Dim Booltras As Boolean = False
        Try
            com.Connection = cn
            com.CommandType = CommandType.Text
            If ckbRework.Checked = False Then
                com.CommandText = "select * from tbJobTrans where _job='" & RmQuo(st) & "'"
            Else
                com.CommandText = "select * from tbJobTransHis where _job='" & RmQuo(st) & "'"
            End If
            Dim Ad As New SqlDataAdapter
            Dim ds As New DataSet
            Ad.SelectCommand = com
            Ad.Fill(ds)


            If ds.Tables(0).Rows.Count > 0 Then
                Dim DRs As DataRow() = ds.Tables(0).Select("_oper_num=" & Val(lstJobRoute.SelectedItems.Item(0).value))
                If DRs.Length > 0 Then
                    For i = 0 To DRs.Length - 1
                        If DRs(i).Item("_Reworkst") <> "R" Then
                            lstJobNo.Items.Add(DRs(i).Item("_job") & "-" & DRs(i).Item("_jobsuffix"))
                        Else
                            lstJobNo.Items.Add(DRs(i).Item("_job") & "-" & DRs(i).Item("_jobsuffix") & "-R")
                        End If

                        Booltras = True
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        If ckbRework.Checked = False Then
            If Booltras = False And intMinroute = Val(lstJobRoute.SelectedItems.Item(0).value) Then
                lstJobNo.Items.Add(st & "-M")
                CheckJobTrans = True
            End If
        End If
        Return CheckJobTrans
    End Function
    Private Sub dtPStart_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtPStart.ValueChanged
        txtStart.Text = Format(dtPStart.Value, "dd/MM/yyyy HH:mm")
        dtPStart.Checked = False
    End Sub
    Private Sub dtpEnd_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpEnd.ValueChanged
        txtEnd.Text = Format(dtpEnd.Value, "dd/MM/yyyy HH:mm")
        dtpEnd.Checked = False
    End Sub
    Private Sub lstJobRoute_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstJobRoute.SelectedIndexChanged
        ClearLstBox()
        If lstJobRoute.SelectedIndex < 0 Then
            Exit Sub
        End If
        CheckJobTrans(ACJobNo)
        txtOPNo.Text = Trim(lstJobRoute.SelectedItems.Item(0).value)
        txtWorkCenter.Text = Trim(lstJobRoute.SelectedItems.Item(0).id)
        CheckMachineID(Trim(lstJobRoute.SelectedItems.Item(0).id))
    End Sub
    Private Sub lstJobNo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstJobNo.SelectedIndexChanged
        lblExport.Visible = False
        txtExport.Visible = False
        txtExport.Text = ""
        butPrint.Visible = False
        Dim stuBool As Boolean = False
        Try
            If lstJobNo.SelectedIndex = -1 Then
                Exit Sub
            End If
            Dim st As String = lstJobNo.SelectedItem
            Dim s() As String = Split(lstJobNo.SelectedItem, "-")

            If s(1) = "" Then
                s(1) = "-"
                txtsuf.Text = "-"
            Else
                txtsuf.Text = s(1)
            End If
            com.Connection = cn
            com.CommandType = CommandType.Text

            CheckSplitst = False
            Dim boolrew As Boolean = False
            '  Dim sql As String = "select * from tbJobTrans where _job='" & RmQuo(s(0)) & "' and _jobSuffix='" & RmQuo(s(1)) & "' and _oper_num=" & lstJobRoute.SelectedItem
            If ckbRework.Checked = False Then
                com.CommandText = "select * from tbJobTrans where _job='" & RmQuo(s(0)) & "' and _jobSuffix='" & RmQuo(s(1)) & "' and _oper_num=" & Val(lstJobRoute.SelectedItems.Item(0).value)
                boolrew = True
            Else
                com.CommandText = "select * from tbJobTransHis where _job='" & RmQuo(s(0)) & "' and _jobSuffix='" & RmQuo(s(1)) & "' and _oper_num=" & Val(lstJobRoute.SelectedItems.Item(0).value)
            End If
            Dim Ad As New SqlDataAdapter
            Dim ds As New DataSet
            Ad.SelectCommand = com
            Ad.Fill(ds)

            Dim dsSplit As New DataSet
            If boolrew = True Then
                'Dim sql As String = "select _oper_num from tbJobTrans where _job='" & RmQuo(s(0)) & "' and _jobSuffix='" & RmQuo(s(1)) & "' and _oper_num<" & Val(lstJobRoute.SelectedItems.Item(0).value)
                dsSplit = clsM.GetDataset("select _oper_num from tbJobTrans where _job='" & RmQuo(s(0)) & "' and _jobSuffix='" & RmQuo(s(1)) & "' and _oper_num<" & Val(lstJobRoute.SelectedItems.Item(0).value), "tbJobTrans")
                If dsSplit.Tables(0).Rows.Count > 0 Then
                    CheckSplitst = True
                Else
                    CheckSplitst = False
                End If


            End If




            If ds.Tables(0).Rows.Count > 0 Then
                With ds.Tables(0).Rows(0)
                    butPrint.Visible = True
                    tansid = .Item("_tansnum")
                    txtParent.Text = .Item("_jobsuffixParent")
                    txtIDNo.Text = .Item("_tansnum")
                    txtOPNo.Text = .Item("_oper_num")
                    txtWorkCenter.Text = .Item("_wc")
                    txtOPRlq.Text = .Item("_qty_Rele_qty")
                    cmbMachine.Text = .Item("_machineid")
                    cmbOPerator.Text = .Item("_emp_num")
                    cmbName.Text = .Item("_emp_name")

                    txtOperatorName.Text = .Item("_emp_name")
                    txtNoofOperator.Text = .Item("_no_oper")
                    txtReceived.Text = .Item("_qty_op_qty")
                    txtCompleted.Text = .Item("_qty_complete")
                    txtRejected.Text = .Item("_qty_scrapped")
                    txtRemarks.Text = .Item("_remarks")
                    If .Item("_start_Date") = 0 Then
                        txtStart.Text = ""
                    Else
                        txtStart.Text = Format(DateTime.FromOADate(.Item("_start_Date")), "dd/MM/yyyy HH:mm")
                    End If
                    If .Item("_end_Date") = 0 Then
                        txtEnd.Text = ""
                    Else
                        txtEnd.Text = Format(DateTime.FromOADate(.Item("_end_Date")), "dd/MM/yyyy HH:mm")
                    End If
                    If .Item("_Reworkst") = "R" Then
                        lbReWork.Visible = True
                    Else
                        lbReWork.Visible = False
                    End If
                    lblStatus.Text = .Item("_Status")
                    If lblStatus.Text = "COMPLETED" Then
                        lblExport.Visible = True
                        txtExport.Visible = True
                        If .Item("_export") = "N" Then
                            txtExport.Text = "NO"
                        Else
                            txtExport.Text = "YES"
                        End If

                    End If
                    ActionAdd(.Item("_Status"))
                    If UCase(.Item("_Status")) = "NEW" Then
                        butAdd.Visible = False
                    Else
                        butAdd.Visible = True
                    End If
                    'txtEnd.Text = .Item("")
                    ' cmbAction.Text = .Item("")
                    stuBool = True
                End With
            End If


            If stuBool = False And intMinroute = Val(lstJobRoute.SelectedItem.value) Then
                txtOPNo.Text = Val(lstJobRoute.SelectedItem.value)
                Dim DSN As New DataSet
                Dim sql As String = "select * from tbJob where _job='" & RmQuo(s(0)) & "'"
                DSN = clsM.GetDataset(sql, "tbMachine")
                If DSN.Tables(0).Rows.Count > 0 Then
                    With DSN.Tables(0).Rows(0)
                        tansid = 0
                        txtOPRlq.Text = .Item("_qtyReleased")
                        txtReceived.Text = .Item("_qtyReleased")
                        txtParent.Text = "M"
                        lblStatus.Text = "NEW"
                        txtIDNo.Text = 0
                        ActionAdd("NEW")
                        butPrint.Visible = True
                    End With
                End If


            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try

        getRejectedTrans()
        'If Booltras = False And intMinroute = lstJobRoute.SelectedItem Then
        '    lstJobNo.Items.Add(st)
        'End If
    End Sub
    Function CheckMachineID(ByVal wrkid As String) As Boolean
        cmbMachine.Items.Clear()
        cmbMachine.Items.Add("-")
        Dim DSMachine As New DataSet
        Dim sql As String = "select * from tbMachine where _Mid in(select _Mid from tbMachineGroup where _wc='" & Trim(wrkid) & "')"
        DSMachine = clsM.GetDataset("select * from tbMachine where _Mid in(select _Mid from tbMachineGroup where _wc='" & Trim(wrkid) & "')", "tbMachine")
        If DSMachine.Tables(0).Rows.Count > 0 Then
            For i = 0 To DSMachine.Tables(0).Rows.Count - 1
                cmbMachine.Items.Add(DSMachine.Tables(0).Rows(i).Item("_machineID"))
            Next
        End If
        cmbMachine.SelectedIndex = 0
        Return True
    End Function
    Function GetOperator() As Boolean
        cmbOPerator.Items.Clear()
        cmbName.Items.Clear()
        cmbOPerator.Items.Add(New DataItem("-Select-", "-Select-"))
        cmbName.Items.Add(New DataItem("-Select-", "-Select-"))
        Dim DSOP As New DataSet
        Dim sql As String = "select * from tbOperator"
        DSOP = clsM.GetDataset(sql, "tbMachine")
        If DSOP.Tables(0).Rows.Count > 0 Then
            For i = 0 To DSOP.Tables(0).Rows.Count - 1
                With DSOP.Tables(0).Rows(i)
                    cmbOPerator.Items.Add(New DataItem(Trim(.Item("_operatorName")), Trim(.Item("_operatorID"))))
                    cmbName.Items.Add(New DataItem(Trim(.Item("_operatorID")), Trim(.Item("_operatorName"))))
                End With
            Next
        End If
        cmbOPerator.SelectedIndex = 0
        cmbName.SelectedIndex = 0
        opst = True
        Return True
    End Function

    Private Sub cmbOPerator_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbOPerator.SelectedIndexChanged
        If opst = True Then
            txtOperatorName.Text = Trim(cmbOPerator.SelectedItem.id)
            cmbName.Text = Trim(cmbOPerator.SelectedItem.id)
        End If
    End Sub

    Private Sub cmbName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbName.SelectedIndexChanged
        If opst = True Then
            txtOperatorName.Text = cmbName.Text
            cmbOPerator.Text = Trim(cmbName.SelectedItem.id)

        End If
    End Sub
    

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        txtJobNo.Text = ""
        ACJobNo = ""
        txtClearAll()
        txtJobNo.Focus()
    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If cmbAction.Text = "UPDATE" Then
            UpdateData()
            Exit Sub
        End If


        If cmbAction.Text = "PAUSE" Or cmbAction.Text = "RESUME" Then
            PAUSE_SAVE()
            Exit Sub
        End If
        Dim comdate As Double = 0
        Dim comTime As Integer = 0

        If Trim(txtJobNo.Text) = "" Then
            MsgBox("Please enter the job no.!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If

        If lstJobRoute.SelectedIndex < 0 Then
            MsgBox("Please select operation no.!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If

        If lstJobNo.SelectedIndex < 0 Then
            MsgBox("Please select job no.!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtOPNo.Text) = "" Then
            MsgBox("Please select operation no.!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If txtIDNo.Text = "" Then
            MsgBox("Please select the job no.!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If


        If Trim(txtOPRlq.Text) = "" Then
            MsgBox("Please select properly!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If

        If cmbMachine.SelectedIndex < -1 Then
            MsgBox("Please select machine id!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If

        If cmbOPerator.SelectedIndex <= 0 Then
            MsgBox("Please select opeartor id!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If


        If Trim(cmbAction.Text) = "" Or Trim(cmbAction.Text) = "-Select-" Then
            MsgBox("Please select action!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Dim chkbool As Boolean = False
        If Val(txtReceived.Text) = 0 And Val(txtOPRlq.Text) = 0 Then
            chkbool = True
        End If
        If chkbool = False Then
            If Val(txtReceived.Text) = 0 Then
                MsgBox("Please enter received quantity!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
        End If


        If Trim(cmbAction.Text) = "" Or Trim(cmbAction.Text) = "-Select-" Then
            MsgBox("Please select action!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If UCase(cmbAction.Text) = "REWORK" Then
            txtStart.Text = Format(Now, "dd/MM/yyyy HH:mm")
        End If

        If UCase(cmbAction.Text) <> "SKIP" Then
            If Trim(txtStart.Text) = "" Then
                MsgBox("Please enter start date!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
        Else
            txtStart.Text = Format(Now, "dd/MM/yyyy HH:mm")
        End If

        If UCase(cmbAction.Text) = "COMPLETED" Then
            If Val(txtNoofOperator.Text) = 0 Or Trim(txtNoofOperator.Text) = "" Then
                MsgBox("Please enter the no. of operator!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            If Trim(txtEnd.Text) = "" Then
                MsgBox("Please enter end date!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            If Val(txtCompleted.Text) = 0 And Val(txtRejected.Text) = 0 Then
                MsgBox("Please check completed and rejected quantity!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If


            Try

                Dim dt() As String = Split(txtEnd.Text, " ")
                Dim stdate() As String = Split(dt(0), "/")
                Dim eddate As Date = CDate(DateSerial(stdate(2), stdate(1), stdate(0)) & " " & dt(1))
                comdate = eddate.ToOADate
                comTime = Format(eddate, "HHmm")
            Catch ex As Exception
                MsgBox("Please check completed date!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End Try




        End If

        If UCase(cmbAction.Text) = "COMPLETED" Or UCase(cmbAction.Text) = "SPLIT" Then
            SAVE_SPLIT()
        ElseIf UCase(cmbAction.Text) = "START" Or UCase(cmbAction.Text) = "SKIP" Or UCase(cmbAction.Text) = "REWORK" Then
            START_SAVE()
        End If

    End Sub
    Function SAVE_SPLIT() As Boolean
        Try
            Dim comdate As Double = 0
            Dim comTime As Integer = 0

            If Trim(txtJobNo.Text) = "" Then
                MsgBox("Please enter the job no.!", MsgBoxStyle.Information, "eWIP")
                Exit Function
            End If

            If lstJobRoute.SelectedIndex < 0 Then
                MsgBox("Please select operation no.!", MsgBoxStyle.Information, "eWIP")
                Exit Function
            End If

            If lstJobNo.SelectedIndex < 0 Then
                MsgBox("Please select job no.!", MsgBoxStyle.Information, "eWIP")
                Exit Function
            End If
            If Trim(txtOPNo.Text) = "" Then
                MsgBox("Please select operation no.!", MsgBoxStyle.Information, "eWIP")
                Exit Function
            End If
            If txtIDNo.Text = "" Then
                MsgBox("Please select the job no.!", MsgBoxStyle.Information, "eWIP")
                Exit Function
            End If


            If Trim(txtOPRlq.Text) = "" Then
                MsgBox("Please select properly!", MsgBoxStyle.Information, "eWIP")
                Exit Function
            End If

            If cmbMachine.SelectedIndex < -1 Then
                MsgBox("Please select machine id!", MsgBoxStyle.Information, "eWIP")
                Exit Function
            End If

            If cmbOPerator.SelectedIndex < -1 Then
                MsgBox("Please select opeartor id!", MsgBoxStyle.Information, "eWIP")
                Me.Close()
                Exit Function
            End If


            If Trim(cmbAction.Text) = "" Or Trim(cmbAction.Text) = "-Select-" Then
                MsgBox("Please select action!", MsgBoxStyle.Information, "eWIP")
                Exit Function
            End If

            If Val(txtNoofOperator.Text) = 0 Or Trim(txtNoofOperator.Text) = "" Then
                MsgBox("Please enter the no. of operator!", MsgBoxStyle.Information, "eWIP")
                Exit Function
            End If
            If Val(txtReceived.Text) = 0 Then
                MsgBox("Please enter received quantity!", MsgBoxStyle.Information, "eWIP")
                Exit Function
            End If


            If Trim(cmbAction.Text) = "" Or Trim(cmbAction.Text) = "-Select-" Then
                MsgBox("Please select action!", MsgBoxStyle.Information, "eWIP")
                Exit Function
            End If


            If Trim(txtStart.Text) = "" Then
                MsgBox("Please enter start date!", MsgBoxStyle.Information, "eWIP")
                Exit Function
            End If

            If UCase(cmbAction.Text) = "COMPLETED" Or UCase(cmbAction.Text) = "SPLIT" Then
                If Trim(txtEnd.Text) = "" Then
                    MsgBox("Please enter end date!", MsgBoxStyle.Information, "eWIP")
                    Exit Function
                End If
                If Val(txtCompleted.Text) = 0 And Val(txtRejected.Text) = 0 Then
                    MsgBox("Please check completed and rejected quantity!", MsgBoxStyle.Information, "eWIP")
                    Exit Function
                End If


                Try

                    Dim dt() As String = Split(txtEnd.Text, " ")
                    Dim stdate() As String = Split(dt(0), "/")
                    Dim eddate As Date = CDate(DateSerial(stdate(2), stdate(1), stdate(0)) & " " & dt(1))
                    comdate = eddate.ToOADate
                    comTime = Format(eddate, "HHmm")
                Catch ex As Exception
                    MsgBox("Please check completed date!", MsgBoxStyle.Information, "eWIP")
                    Exit Function
                End Try



                If UCase(cmbAction.Text) = "COMPLETED" Then
                    If Val(txtReceived.Text) <> Val(txtCompleted.Text) + Val(txtRejected.Text) Then
                        MsgBox("Please check completed and rejected quantity!", MsgBoxStyle.Information, "eWIP")
                        Exit Function
                    End If
                Else
                    If Val(txtCompleted.Text) = 0 And Val(txtRejected.Text) = 0 Then
                        MsgBox("Please check completed and rejected quantity!", MsgBoxStyle.Information, "eWIP")
                        Exit Function
                    End If
                    If Val(txtReceived.Text) = Val(txtCompleted.Text) + Val(txtRejected.Text) Then
                        MsgBox("Please check completed and rejected quantity!", MsgBoxStyle.Information, "eWIP")
                        Exit Function
                    End If
                End If
                If Val(txtRejected.Text) <> Val(txtTot.Text) Then
                    MsgBox("Please check rejected quantity!", MsgBoxStyle.Information, "eWIP")
                    ' txtMain.Focus()
                    'CountI = 1
                    Exit Function
                End If



            End If


            Dim strspt As String = GetMaxSufID()
            If UCase(cmbAction.Text) = "SPLIT" Then
                If strspt = "" Then
                    MsgBox("Split only three level only,Please check your admin!", MsgBoxStyle.Information, "eWIP")
                    Exit Function
                Else
                    txtParent.Text = txtsuf.Text
                End If
            End If
            Dim splitst As Boolean = False
            If UCase(cmbAction.Text) <> "SPLIT" Then
                If CheckSplitCon() = True Then
                    If strspt = "" Then
                        MsgBox("Split only three level only,Please check your admin!", MsgBoxStyle.Information, "eWIP")
                        Exit Function
                    Else
                        splitst = True
                        txtParent.Text = txtsuf.Text
                        txtsuf.Text = strspt
                    End If

                End If
            End If

            Dim strZeroCom As Boolean = False

            Dim strSupNew As String

            With com
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                If UCase(cmbAction.Text) <> "SPLIT" Then
                    .CommandText = "sp_JOB_COMPLETED_UPDATED_new"
                Else
                    .CommandText = "sp_JOB_SPLIT_UPDATED_new"
                End If
                parm = .Parameters.Add("@tansnum", SqlDbType.Int)
                parm.Value = Val(txtIDNo.Text)
                parm = .Parameters.Add("@jobsuffixParent", SqlDbType.VarChar, 10)
                parm.Value = Trim(txtParent.Text)
                parm = .Parameters.Add("@jobsuffix", SqlDbType.VarChar, 10)
                If UCase(cmbAction.Text) <> "SPLIT" Then
                    parm.Value = Trim(txtsuf.Text)
                    strSupNew = txtsuf.Text
                Else
                    strSupNew = strspt
                    parm.Value = strspt
                End If
                parm = .Parameters.Add("@emp_num", SqlDbType.VarChar, 50)
                parm.Value = Trim(cmbOPerator.Text)
                parm = .Parameters.Add("@emp_name", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtOperatorName.Text)
                parm = .Parameters.Add("@end_Date", SqlDbType.Float)
                parm.Value = comdate
                parm = .Parameters.Add("@end_time", SqlDbType.Int)
                parm.Value = comTime
                parm = .Parameters.Add("@no_oper", SqlDbType.Int)
                If Val(txtNoofOperator.Text) = 0 Or Trim(txtNoofOperator.Text) = "" Then
                    parm.Value = 1
                Else
                    parm.Value = Val(txtNoofOperator.Text)
                End If
                parm = .Parameters.Add("@qty_complete", SqlDbType.Float)
                parm.Value = Val(txtCompleted.Text)
                parm = .Parameters.Add("@qty_scrapped", SqlDbType.Float)
                parm.Value = Val(txtRejected.Text)
                parm = .Parameters.Add("@status", SqlDbType.VarChar, 10)
                parm.Value = Trim(cmbAction.Text)
                parm = .Parameters.Add("@Remarks", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtRemarks.Text)

                parm = .Parameters.Add("@SplitAction", SqlDbType.Int)
                If splitst = False Then
                    parm.Value = 0
                Else
                    parm.Value = 1
                End If

                parm = .Parameters.Add("@ARelQty", SqlDbType.Float)
                If splitst = False Then
                    parm.Value = Val(txtOPRlq.Text)
                Else
                    parm.Value = Val(Val(txtCompleted.Text) + Val(txtRejected.Text))
                End If



                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()

                Delete_rejected(Val(txtIDNo.Text))
                '
                Dim clsR As clsRej
                For Each clsR In Newhash.Values
                    .Connection = cn
                    .CommandType = CommandType.StoredProcedure
                    .CommandText = "sp_JOB_REJECTED_UPDATED"
                    parm = .Parameters.Add("@refID", SqlDbType.Int)
                    parm.Value = Val(txtIDNo.Text)
                    parm = .Parameters.Add("@RejectedCode", SqlDbType.VarChar, 50)
                    parm.Value = Trim(clsR.Rcode)
                    parm = .Parameters.Add("@RejectedDesc", SqlDbType.VarChar, 256)
                    parm.Value = Trim(clsR.Desc)
                    parm = .Parameters.Add("@RejectedQty", SqlDbType.Float)
                    parm.Value = CDbl(clsR.rQty)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()

                Next

                'If Val(txtCompleted.Text) = 0 Then
                '    CloseAllCase(Split(lstJobNo.SelectedItem, "-")(0), strSupNew, Trim(txtParent.Text), Val(txtOPNo.Text))
                'End If


                MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")
                ClearLstBox()
                'If UCase(txtAction.Text) = "SPLIT" Then
                '    PrintReport(txtJobNo.Text, strspt)
                'End If
                'If UCase(txtAction.Text) = "COMPLETED" Then
                '    If splitst = True Then
                '        PrintReport(txtJobNo.Text, strspt)
                '    End If
                'End If

                'COMPLETED_SAVE = True







            End With






        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
    End Function
    Function PAUSE_SAVE() As Boolean
        If Trim(txtJobNo.Text) = "" Then
            MsgBox("Please enter the job no.!", MsgBoxStyle.Information, "eWIP")
            Exit Function
        End If

        If lstJobRoute.SelectedIndex < 0 Then
            MsgBox("Please select operation no.!", MsgBoxStyle.Information, "eWIP")
            Exit Function
        End If

        If lstJobNo.SelectedIndex < 0 Then
            MsgBox("Please select job no.!", MsgBoxStyle.Information, "eWIP")
            Exit Function
        End If
        If Trim(txtOPNo.Text) = "" Then
            MsgBox("Please select operation no.!", MsgBoxStyle.Information, "eWIP")
            Exit Function
        End If
        If txtIDNo.Text = "" Then
            MsgBox("Please select the job no.!", MsgBoxStyle.Information, "eWIP")
            Exit Function
        End If
        If cmbOPerator.SelectedIndex < -1 Then
            MsgBox("Please select opeartor id!", MsgBoxStyle.Information, "eWIP")
            Me.Close()
            Exit Function
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "sp_JOB_PAUSE_UPDATED"
                parm = .Parameters.Add("@tansnum", SqlDbType.Int)
                parm.Value = Val(txtIDNo.Text)
                parm = .Parameters.Add("@parm", SqlDbType.Int)
                If cmbAction.Text = "PAUSE" Then
                    parm.Value = 0
                Else
                    parm.Value = 1
                End If
                Dim strdbl As Double = get_ServerDate()
                parm = .Parameters.Add("@pstart", SqlDbType.Float)
                parm.Value = strdbl
                parm = .Parameters.Add("@pend", SqlDbType.Float)
                parm.Value = strdbl
                parm = .Parameters.Add("@Remarks", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtRemarks.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                PAUSE_SAVE = True
                MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")
                ClearLstBox()
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try


        Return PAUSE_SAVE



    End Function



    Function START_SAVE() As Boolean
        START_SAVE = False

        Dim strdate As Double
        Dim strTime As Integer
        Try

            Dim dt() As String = Split(txtStart.Text, " ")
            Dim stdate() As String = Split(dt(0), "/")
            Dim eddate As Date = CDate(DateSerial(stdate(2), stdate(1), stdate(0)) & " " & dt(1))
            strdate = eddate.ToOADate
            strTime = Format(eddate, "HHmm")
        Catch ex As Exception
            MsgBox("Please check start date!", MsgBoxStyle.Information, "eWIP")
            Exit Function
        End Try

        Try
            With com
                '(_job,_jobsuffix,_jobsuffixParent,_item,_jobDate,_wc,_trans_type,_trans_datefrm,_qty_Rele_qty,_qty_op_qty,_oper_num,_emp_num,_emp_name,_start_time,_CreatedBy,_status,_resourceid,_resourceDesc,_machineid
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                If Trim(cmbAction.Text) = "START" Then
                    .CommandText = "sp_JOB_START_ADD_new"
                ElseIf Trim(cmbAction.Text) = "REWORK" Then
                    .CommandText = "sp_JOB_REWORK_ADD_new"
                Else
                    .CommandText = "sp_JOB_SKIP_ADD_new"
                End If
                parm = .Parameters.Add("@tansnum", SqlDbType.Int)
                parm.Value = Val(txtIDNo.Text)
                parm = .Parameters.Add("@job", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtJobNo.Text)
                parm = .Parameters.Add("@jobsuffix", SqlDbType.VarChar, 50)
                parm.Value = txtsuf.Text
                parm = .Parameters.Add("@jobsuffixParent", SqlDbType.VarChar, 50)
                parm.Value = txtParent.Text
                parm = .Parameters.Add("@item", SqlDbType.VarChar, 256)
                parm.Value = Trim(lblItemcode.Text)
                parm = .Parameters.Add("@oper_num", SqlDbType.Int)
                parm.Value = Val(Trim(txtOPNo.Text))
                parm = .Parameters.Add("@wc", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtWorkCenter.Text)
                parm = .Parameters.Add("@machineid", SqlDbType.VarChar, 50)
                parm.Value = Trim(cmbMachine.Text)
                parm = .Parameters.Add("@emp_num", SqlDbType.VarChar, 50)
                parm.Value = Trim(cmbOPerator.Text)
                parm = .Parameters.Add("@emp_name", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtOperatorName.Text)
                parm = .Parameters.Add("@Start_date", SqlDbType.Float)
                parm.Value = strdate
                parm = .Parameters.Add("@start_time", SqlDbType.Int)
                parm.Value = strTime
                parm = .Parameters.Add("@qty_Rele_qty", SqlDbType.Float)
                parm.Value = Val(txtOPRlq.Text)
                parm = .Parameters.Add("@qty_op_qty", SqlDbType.Float)
                parm.Value = Val(txtReceived.Text)
                parm = .Parameters.Add("@status", SqlDbType.VarChar, 10)
                parm.Value = Trim(cmbAction.Text)
                parm = .Parameters.Add("@Createdid", SqlDbType.VarChar, 50)
                parm.Value = Trim(cmbOPerator.Text)
                parm = .Parameters.Add("@CreatedBy", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtOperatorName.Text)
                If Trim(cmbAction.Text) = "REWORK" Then
                    parm = .Parameters.Add("@minop", SqlDbType.Int)
                    parm.Value = intMinroute
                    parm = .Parameters.Add("@UserName", SqlDbType.VarChar, 255)
                    parm.Value = LogName
                End If
                parm = .Parameters.Add("@Remarks", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtRemarks.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                START_SAVE = True
                If Val(txtCompleted.Text) = 0 Then
                    'CloseAllCase(Split(lstJobNo.SelectedItem, "-")(0), Trim(txtsuf.Text), Trim(txtParent.Text), Val(txtOPNo.Text))
                End If


                MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")
                ClearLstBox()
            End With
        Catch EX As Exception
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try

        Return START_SAVE
    End Function

    Private Sub butAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAdd.Click
        If (txtWorkCenter.Text) <> "" Then
            Dim objSuprj As New frmsupRejected
            objSuprj.txtRecQty.Text = txtReceived.Text
            objSuprj.txtcomQty.Text = txtCompleted.Text
            objSuprj.txtcomQty.Text = txtRejected.Text

            objSuprj.WCId = txtWorkCenter.Text
            objSuprj.ShowDialog()
            loadView()
        Else
            MsgBox("Please select job no. and operation no.!", MsgBoxStyle.Information, "eWIP")
        End If
      
    End Sub
    Sub ActionAdd(ByVal st As String)
        cmbAction.Items.Clear()
        cmbAction.Items.Add("-Select-")
        cmbOPerator.Text = ""
        cmbName.Text = ""
        Dim s() As String
        If lstJobNo.SelectedIndex > -1 Then
            s = Split(lstJobNo.SelectedItems.Item(0), "-")
        Else
            s = Split("", "-")
        End If

        If st = "NEW" Then
            If UCase(LogPrivilege) = "REWORK" Then
                If Val(lstJobRoute.SelectedItems.Item(0).value) > intMinroute Then
                    If CheckSplitst = True Then
                        cmbAction.Items.Add("REWORK")
                        cmbOPerator.Text = LogUserID
                        cmbName.Text = LogName
                    End If
                End If
            Else
                If Val(lstJobRoute.SelectedItems.Item(0).value) > intMinroute Then
                    If CheckSplitst = True Then
                        cmbAction.Items.Add("REWORK")
                    End If
                End If
                cmbAction.Items.Add("SKIP")
                cmbAction.Items.Add("START")
                If Val(txtIDNo.Text) <> 0 Then
                    cmbAction.Items.Add("UPDATE")
                End If
            End If
        ElseIf st = "START" Then
            If UCase(LogPrivilege) = "REWORK" Then
                If Val(lstJobRoute.SelectedItems.Item(0).value) > intMinroute Then
                    If CheckSplitst = True Then
                        cmbAction.Items.Add("REWORK")
                        cmbOPerator.Text = LogUserID
                        cmbName.Text = LogName
                    End If
                End If
            Else



                If Val(lstJobRoute.SelectedItems.Item(0).value) > intMinroute Then
                    If CheckSplitst = True Then
                        cmbAction.Items.Add("REWORK")

                    End If
                End If
                cmbAction.Items.Add("COMPLETED")
                If s.Length <= 2 Then
                    cmbAction.Items.Add("SPLIT")
                End If

                cmbAction.Items.Add("PAUSE")
                If Val(txtIDNo.Text) <> 0 Then
                    cmbAction.Items.Add("UPDATE")
                End If
            End If
        ElseIf st = "COMPLETED" And UCase(LogPrivilege) <> "REWORK" Then

            If Val(txtIDNo.Text) <> 0 Then
                cmbAction.Items.Add("UPDATE")
            End If

        ElseIf st = "PAUSE" And UCase(LogPrivilege) <> "REWORK" Then
            cmbAction.Items.Add("RESUME")
            If Val(txtIDNo.Text) <> 0 Then
                cmbAction.Items.Add("UPDATE")
            End If

        End If
        cmbAction.SelectedIndex = 0

    End Sub

    Function GetMaxSufID() As String
        GetMaxSufID = ""
        Dim DSTrans As New DataSet
        Dim txtSubval As String = ""
        Dim AdTrans As New SqlDataAdapter
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text

                .CommandText = "select * from tbJobTrans where _job='" & Trim(txtJobNo.Text) & "'"
                AdTrans.SelectCommand = com
                AdTrans.Fill(DSTrans, "AdTrans")

            End With


            Dim DRCheckChiled() As DataRow = DSTrans.Tables(0).Select("_jobsuffixParent='" & Trim(txtsuf.Text) & "' and _oper_num=" & Val(txtOPNo.Text))
            If DRCheckChiled.Length > 0 Then
                Dim i As Integer
                For i = 0 To DRCheckChiled.Length - 1
                    If txtSubval = "" Then
                        txtSubval = "'" & Trim(DRCheckChiled(i).Item("_jobsuffix")) & "'"
                    Else
                        txtSubval = txtSubval & ",'" & Trim(DRCheckChiled(i).Item("_jobsuffix")) & "'"
                    End If
                Next
            End If
            '  Dim DRCheckAfter() As DataRow = DSTrans.Tables(0).Select("_jobsuffixParent='" & Trim(txtParent.Text) & "' and _oper_num>" & Val(txtOperationno.Text))
            ' If DRCheckAfter.Length > 0 Then
            'GetMaxSufID = ""
            'Return GetMaxSufID
            'Exit Function
            'End If
            Dim dr As SqlDataReader
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                If txtSubval = "" Then
                    txtSubval = "''"
                End If
                Dim sql As String = "select Min(_lel_1_Name) as _lel_1_Name from tbLevel1 where _lel_1_Parent='" & Trim(txtsuf.Text) & "' and _lel_1_Name not in (" & txtSubval & ")"
                .CommandText = "select Min(_lel_1_Name) as _lel_1_Name from tbLevel1 where _lel_1_Parent='" & Trim(txtsuf.Text) & "' and _lel_1_Name not in (" & txtSubval & ")"
                cn.Open()
                dr = .ExecuteReader
                If dr.Read Then
                    GetMaxSufID = dr.Item("_lel_1_Name")
                    cn.Close()
                End If
                cn.Close()
                Return GetMaxSufID
            End With

        Catch ex As Exception
        Finally
            cn.Close()
        End Try
    End Function
    Function CheckSplitCon() As Boolean
        Dim ds As New DataSet
        CheckSplitCon = False
        ds = clsM.GetDataset("select * from  tbJobTrans where _tansnum<>" & Val(txtIDNo.Text) & " and  _job='" & Trim(txtJobNo.Text) & "' and _oper_num=" & Val(txtOPNo.Text) & " and _jobsuffixParent='" & Trim(txtsuf.Text) & "'", "tbDate")
        If ds.Tables(0).Rows.Count > 0 Then
            CheckSplitCon = True
        End If
        Return CheckSplitCon
    End Function

    Private Sub butPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPrint.Click
        Dim prn As New ClsAMTprint.clsmain
        Dim restr As String = ""
        Dim s() As String = Split(lstJobNo.SelectedItems.Item(0), "-")
        If s.Length > 2 Then
            restr = "-R"
        End If
        If ckbRework.Checked = True Then
            If prn.printAMT(Trim(s(0)) & "-" & Trim(s(1)), SqlString, PrinterName, True) Then
                MsgBox(PrintMSG)
            End If
        Else
            If prn.printAMT(Trim(s(0)) & "-" & Trim(s(1) & restr), SqlString, PrinterName, False) = True Then
                MsgBox(PrintMSG)
            End If
        End If

    End Sub

    Private Sub ckbRework_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbRework.CheckedChanged
        lstJobRoute_SelectedIndexChanged(sender, e)
    End Sub
    Sub loadView()
        lstv.Items.Clear()
        txttot.Text = 0.0
        If Newhash.Count > 0 Then
            Dim irej As clsRej
            For Each irej In Newhash.Values
                Dim ls As New ListViewItem(Trim(irej.Rcode))    ' you can also use reader.GetSqlValue(0) 
                ls.SubItems.Add(Trim(irej.Desc))
                ls.SubItems.Add(Trim(irej.rQty))
                lstv.Items.Add(ls)
                txttot.Text = Val(txttot.Text) + Val(irej.rQty)
            Next
        End If


    End Sub
    Sub getRejectedTrans()
        lstv.Items.Clear()
        Newhash.Clear()
        Dim dsR As New DataSet
        dsR = clsM.GetDataset("select * from  tbRejectedTrans where _refID=" & Val(txtIDNo.Text), "tbDate")
        If dsR.Tables(0).Rows.Count > 0 Then
            Dim i As Integer
            For i = 0 To dsR.Tables(0).Rows.Count - 1
                With dsR.Tables(0).Rows(i)
                    If (Newhash.ContainsKey(.Item("_RejectedCode"))) = False Then
                        Newhash.Remove(.Item("_RejectedCode"))
                        Dim clsI As New clsRej
                        clsI.Rcode = .Item("_RejectedCode")
                        clsI.Desc = .Item("_RejectedDesc")
                        clsI.rQty = Val(.Item("_RejectedQty"))
                        Newhash.Add(.Item("_RejectedCode"), clsI)
                    End If
                End With
            Next
        End If
      

        loadView()
    End Sub
    Function Delete_rejected(ByVal idno As Integer) As Boolean
        With com
            Delete_rejected = False
            Try
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "delete from tbRejectedTrans where _refID=" & idno
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                Delete_rejected = True
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Finally
                cn.Close()
            End Try
            Return Delete_rejected
        End With


    End Function
    Function get_ServerDate() As Double
        Dim dsdate As DataSet
        dsdate = clsM.GetDataset("select getDate() as ToDate", "tbDate")
        get_ServerDate = CDate(dsdate.Tables(0).Rows(0).Item("ToDate")).ToOADate
        Return get_ServerDate
    End Function
    Sub UpdateData()
        Try
            Dim comdate As Double = 0
            Dim comTime As Integer = 0

            If Trim(txtJobNo.Text) = "" Then
                MsgBox("Please enter the job no.!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If

            If lstJobRoute.SelectedIndex < 0 Then
                MsgBox("Please select operation no.!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If

            If lstJobNo.SelectedIndex < 0 Then
                MsgBox("Please select job no.!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            If Trim(txtOPNo.Text) = "" Then
                MsgBox("Please select operation no.!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            If txtIDNo.Text = "" Then
                MsgBox("Please select the job no.!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If

            If cmbOPerator.SelectedIndex < -1 Then
                MsgBox("Please select opeartor id!", MsgBoxStyle.Information, "eWIP")
                Me.Close()
                Exit Sub
            End If
            Dim strdate As Double = 0
            Dim strTime As Integer = 0
            If Trim(txtStart.Text) <> "" Then
                Try

                    Dim dt() As String = Split(txtStart.Text, " ")
                    Dim stdate() As String = Split(dt(0), "/")
                    Dim eddate As Date = CDate(DateSerial(stdate(2), stdate(1), stdate(0)) & " " & dt(1))
                    strdate = eddate.ToOADate
                    strTime = Format(eddate, "HHmm")
                Catch ex As Exception
                    MsgBox("Please check start date!", MsgBoxStyle.Information, "eWIP")
                    Exit Sub
                End Try

            End If
            If Trim(txtEnd.Text) <> "" Then
                Try

                    Dim dt() As String = Split(txtEnd.Text, " ")
                    Dim stdate() As String = Split(dt(0), "/")
                    Dim eddate As Date = CDate(DateSerial(stdate(2), stdate(1), stdate(0)) & " " & dt(1))
                    comdate = eddate.ToOADate
                    comTime = Format(eddate, "HHmm")
                Catch ex As Exception
                    MsgBox("Please check completed date!", MsgBoxStyle.Information, "eWIP")
                    Exit Sub
                End Try
            End If
            With com
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "sp_JOB_JOB_UPDATED_new"
                parm = .Parameters.Add("@vari", SqlDbType.Int)
                If ckbRework.Checked = False Then
                    parm.Value = 0
                Else
                    parm.Value = 1
                End If
                parm = .Parameters.Add("@tansnum", SqlDbType.Int)
                parm.Value = Val(txtIDNo.Text)
                parm = .Parameters.Add("@machineid", SqlDbType.VarChar, 50)
                'If txtCompleted.Text = "-Select-" Then
                '    parm.Value = "-"
                'Else
                parm.Value = cmbMachine.Text
                ' End If

                parm = .Parameters.Add("@emp_num", SqlDbType.VarChar, 50)
                parm.Value = Trim(cmbOPerator.Text)
                parm = .Parameters.Add("@emp_name", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtOperatorName.Text)


                parm = .Parameters.Add("@no_oper", SqlDbType.Int)
                If Val(txtNoofOperator.Text) = 0 Or Trim(txtNoofOperator.Text) = "" Then
                    parm.Value = 1
                Else
                    parm.Value = Val(txtNoofOperator.Text)
                End If

                parm = .Parameters.Add("@qty_Rele_qty", SqlDbType.Float)
                parm.Value = Val(txtOPRlq.Text)
                parm = .Parameters.Add("@qty_op_qty", SqlDbType.Float)
                parm.Value = Val(txtReceived.Text)
                parm = .Parameters.Add("@qty_complete", SqlDbType.Float)
                parm.Value = Val(txtCompleted.Text)
                parm = .Parameters.Add("@qty_scrapped", SqlDbType.Float)
                parm.Value = Val(txtRejected.Text)

                parm = .Parameters.Add("@Start_date", SqlDbType.Float)
                parm.Value = strdate
                parm = .Parameters.Add("@start_time", SqlDbType.Int)
                parm.Value = strTime
                parm = .Parameters.Add("@end_Date", SqlDbType.Float)
                parm.Value = comdate
                parm = .Parameters.Add("@end_time", SqlDbType.Int)
                parm.Value = comTime
                parm = .Parameters.Add("@Remarks", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtRemarks.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()

                Delete_rejected(Val(txtIDNo.Text))
                '
                Dim clsR As clsRej
                For Each clsR In Newhash.Values
                    .Connection = cn
                    .CommandType = CommandType.StoredProcedure
                    .CommandText = "sp_JOB_REJECTED_UPDATED"
                    parm = .Parameters.Add("@refID", SqlDbType.Int)
                    parm.Value = Val(txtIDNo.Text)
                    parm = .Parameters.Add("@RejectedCode", SqlDbType.VarChar, 50)
                    parm.Value = Trim(clsR.Rcode)
                    parm = .Parameters.Add("@RejectedDesc", SqlDbType.VarChar, 256)
                    parm.Value = Trim(clsR.Desc)
                    parm = .Parameters.Add("@RejectedQty", SqlDbType.Float)
                    parm.Value = CDbl(clsR.rQty)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()

                Next



                AllTotalHisData(ACJobNo, Val(txtOPNo.Text))
                MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")
                ClearLstBox()
                'If UCase(txtAction.Text) = "SPLIT" Then
                '    PrintReport(txtJobNo.Text, strspt)
                'End If
                'If UCase(txtAction.Text) = "COMPLETED" Then
                '    If splitst = True Then
                '        PrintReport(txtJobNo.Text, strspt)
                '    End If
                'End If

                'COMPLETED_SAVE = True

            End With






        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            com.Parameters.Clear()
            cn.Close()
        End Try
    End Sub

    Private Sub txtJobNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtJobNo.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Trim(txtJobNo.Text) <> "" Then
                txtJobNo.Text = txtJobNo.Text.Split("-")(0)
            End If
            ButSearch_Click(sender, e)
        End If


    End Sub

    
    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub

    Private Sub pl_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles pl.Paint

    End Sub

    Private Sub butUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdate.Click

    End Sub

    
    Private Sub ButPrintHis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButPrintHis.Click
        Dim frm As New frmJobStatus
        If Trim(txtJobNo.Text) <> "" Then
            frm.StrJobno = Trim(txtJobNo.Text)
            frm.ShowDialog()
        End If

    End Sub


    'Sub AllTotalHis(ByVal HissrtJOBNo As String, ByVal HIsstrop As Integer)
    '    Dim dsTransData As New DataSet
    '    Dim dsTransHis As New DataSet
    '    Dim strSqlTrans As String = "select * from tbJobTrans where _Job='" & Trim(HissrtJOBNo) & "' and _oper_num=" & Val(HIsstrop) & " and _status='COMPLETED'   order by _reworkCount"
    '    dsTransData = clsM.GetDataset(strSqlTrans, "tbJobTrans")
    '    Dim strSqlHis As String = "select * from tbJobTransHis where_Job='" & Trim(HissrtJOBNo) & "' and _oper_num=" & Val(HIsstrop) & " and _status='COMPLETED' order by _reworkCount"
    '    dsTransHis = clsM.GetDataset(strSqlHis, "tbJobTrans")
    '    Dim i As Integer
    '    If dsTransHis.Tables(0).Rows.Count > 0 Then
    '        Dim TotQTYOP As Double
    '        Dim TotQTYCom As Double
    '        Dim TotQTYRej As Double
    '        Dim HASHHis As New Hashtable
    '        Dim HASHCUr As New Hashtable
    '        For i = 0 To dsTransHis.Tables(0).Rows.Count - 1
    '            Dim DRP As DataRow() = dsTransHis.Tables(0).Select("_jobsuffix='" & dsTransHis.Tables(0).Rows(i).Item("_jobsuffix") & "'", "_reworkCount")
    '            If DRP.Length > 0 Then
    '                Dim j As Integer
    '                For j = 0 To DRP.Length - 1
    '                    If HASHHis.ContainsKey(dsTransHis.Tables(0).Rows(i).Item("_tansnum")) = False Then
    '                        Dim DSMInOP As DataSet


    '                        strSqlHis = "select * from tbJobTrans where_Job='" & Trim(HissrtJOBNo) & "' and _qty_op_qty=" & Val(HIsstrop) & " and _jobsuffix='" & dsTransHis.Tables(0).Rows(i).Item("_jobsuffix") & " order by _reworkCount"
    '                        DSMInOP = clsM.GetDataset(strSqlHis, "tbJobTrans")

    '                        ' Dim RWS As DataRow() = dsTransHis.Tables(0).Select("_jobsuffix='" & dsTransHis.Tables(0).Rows(i).Item("_jobsuffix") & "' and _reworkCount>" & dsTransHis.Tables(0).Rows(i).Item("_reworkCount"), "_reworkCount")




    '                        HASHHis.Add(dsTransHis.Tables(0).Rows(i).Item("_tansnum"), dsTransHis.Tables(0).Rows(i).Item("_tansnum"))
    '                    End If
    '                Next
    '            End If
    '        Next
    '    Else
    '    End If
    'End Sub


    Sub AllTotalHis(ByVal HissrtJOBNo As String, ByVal HIsstrop As Integer)
        Dim dsTransData As New DataSet
        Dim dsTransHis As New DataSet
        Dim HashT As New Hashtable
        Dim strSqlTrans As String = "select * from tbJobTransHis  where _Job='" & Trim(HissrtJOBNo) & "' and _jobsuffix=" & Val(HIsstrop) & " and _reworkCount in(select Min(_reworkCount) from tbJobTransHis where _Job='UC01990104' and _jobsuffix='A1')"
        'select _job,_jobsuffix from tbJobTransHis  where _Job='UC01990104' and _oper_num=1100 Group by _job,_jobsuffix
        'select _job,_jobsuffix from tbJobTrans  where _Job='UC01990104' and _oper_num=1100 Group by _job,_jobsuffix
        Try
            Dim DSMH As New DataSet
            DSMH = clsM.GetDataset("select _job,_jobsuffix from tbJobTransHis  where _Job='UC01990104' and _oper_num=1100 Group by _job,_jobsuffix", "tbJobTransHis")
            Dim DSMAC As New DataSet
            DSMAC = clsM.GetDataset("select _job,_jobsuffix from tbJobTransHis  where _Job='UC01990104' and _oper_num=1100 Group by _job,_jobsuffix", "tbJobTransHis")
            Dim k As Integer = -1
            If DSMH.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For i = 0 To DSMH.Tables(0).Rows.Count - 1
                    If HashT.ContainsValue(DSMH.Tables(0).Rows(i).Item("_jobsuffix")) = False Then
                        HashT.Add(DSMH.Tables(0).Rows(i).Item("_jobsuffix"), k)
                        k = k + 1
                    End If
                Next
            End If
            If DSMAC.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For i = 0 To DSMAC.Tables(0).Rows.Count - 1
                    If HashT.ContainsValue(DSMAC.Tables(0).Rows(i).Item("_jobsuffix")) = False Then
                        HashT.Add(DSMAC.Tables(0).Rows(i).Item("_jobsuffix"), k)
                        k = k + 1
                    End If
                Next
            End If
            Dim j As Integer
            If k > -1 Then
                For j = 0 To k
                    Dim xval As String = HashT(j)


                Next
            End If

        Catch ex As Exception
        End Try
    End Sub


    Sub AllTotalHisData(ByVal HissrtJOBNo As String, ByVal HIsstrop As Integer)
        Dim dsTransData As New DataSet
        Dim dsTransHis As New DataSet
        Dim HashT As New Hashtable
        Dim TotalC_qtry As Double = 0
        Dim TotalR_qtry As Double = 0
        Dim TotalP_qtry As Double = 0
        Dim TotalSkip_qtry As Double = 0
        ' Dim strSqlTrans As String = "select * from tbJobTransHis  where _Job='" & Trim(HissrtJOBNo) & "' and _oper_num=" & Val(HIsstrop) & " and _reworkCount in(select Min(_reworkCount) from tbJobTransHis where _Job='UC01990104' and _jobsuffix='A1')"
        Dim clsM As New clsMain
        Try
            Dim DSMH As New DataSet
            DSMH = clsM.GetDataset("select *  from tbJobTransHis  where _Job='" & Trim(HissrtJOBNo) & "' and _oper_num=" & Val(HIsstrop) & " order by _reworkCount", "tbJobTransHis")
            If DSMH.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For i = 0 To DSMH.Tables(0).Rows.Count - 1
                    If HashT.ContainsValue(DSMH.Tables(0).Rows(i).Item("_jobsuffix")) = False Then

                        TotalP_qtry = TotalP_qtry + DSMH.Tables(0).Rows(i).Item("_qty_op_qty")
                        TotalC_qtry = TotalC_qtry + DSMH.Tables(0).Rows(i).Item("_qty_complete")
                        TotalR_qtry = TotalR_qtry + DSMH.Tables(0).Rows(i).Item("_qty_scrapped")

                        Dim DSResult As New DataSet
                        DSResult = clsM.GetDataset("select *  from tbJobTrans where _Job='" & Trim(HissrtJOBNo) & "' and _jobsuffix='" & DSMH.Tables(0).Rows(i).Item("_jobsuffix") & "' and _oper_num In(select max(_oper_num) from tbJobtrans where _Job='" & Trim(HissrtJOBNo) & "' and _jobsuffix='" & DSMH.Tables(0).Rows(i).Item("_jobsuffix") & "' and  _oper_num<=" & DSMH.Tables(0).Rows(i).Item("_oper_num") & ")", "tbJobTransHis")
                        If DSResult.Tables(0).Rows.Count > 0 Then
                            If DSResult.Tables(0).Rows(0).Item("_oper_num") = HIsstrop Then
                                If DSResult.Tables(0).Rows(0).Item("_status") <> "COMPLETED" Then
                                    TotalP_qtry = TotalP_qtry - DSResult.Tables(0).Rows(0).Item("_qty_op_qty")
                                    TotalC_qtry = TotalC_qtry - DSResult.Tables(0).Rows(0).Item("_qty_op_qty")
                                Else
                                    TotalP_qtry = TotalP_qtry + DSResult.Tables(0).Rows(0).Item("_qty_scrapped")
                                    TotalC_qtry = TotalC_qtry '- DSResult.Tables(0).Rows(0).Item("_qty_scrapped")
                                    TotalR_qtry = TotalR_qtry + DSResult.Tables(0).Rows(0).Item("_qty_scrapped")

                                End If
                            Else
                                TotalP_qtry = TotalP_qtry - DSResult.Tables(0).Rows(0).Item("_qty_op_qty")
                                TotalC_qtry = TotalC_qtry - DSResult.Tables(0).Rows(0).Item("_qty_op_qty")
                            End If
                        End If

                        Dim DSRej1 As New DataSet

                        DSRej1 = clsM.GetDataset("select Sum(_qty_scrapped) as _qty_scrapped  from tbJobTrans  where _Job='" & Trim(HissrtJOBNo) & "' and _oper_num<=" & Val(HIsstrop) & "  and _jobsuffix='" & DSMH.Tables(0).Rows(i).Item("_jobsuffix") & "' and _reworkCount>" & DSMH.Tables(0).Rows(i).Item("_reworkCount"), "tbJobTransHis")

                        If DSRej1.Tables(0).Rows.Count > 0 Then
                            If IsDBNull(DSRej1.Tables(0).Rows(0).Item("_qty_scrapped")) = False Then
                                TotalP_qtry = TotalP_qtry - DSRej1.Tables(0).Rows(0).Item("_qty_scrapped")
                                TotalC_qtry = TotalC_qtry - DSRej1.Tables(0).Rows(0).Item("_qty_scrapped")
                            End If
                            Dim DSRej2 As New DataSet
                            DSRej2 = clsM.GetDataset("select Sum(_qty_scrapped)as _qty_scrapped  from tbJobTransHis  where _Job='" & Trim(HissrtJOBNo) & "' and _oper_num<" & Val(HIsstrop) & "  and _jobsuffix='" & DSMH.Tables(0).Rows(i).Item("_jobsuffix") & "' and _reworkCount>" & DSMH.Tables(0).Rows(i).Item("_reworkCount"), "tbJobTransHis")
                            If DSRej2.Tables(0).Rows.Count > 0 Then
                                If IsDBNull(DSRej2.Tables(0).Rows(0).Item("_qty_scrapped")) = False Then
                                    TotalP_qtry = TotalP_qtry - DSRej2.Tables(0).Rows(0).Item("_qty_scrapped")
                                    TotalC_qtry = TotalC_qtry - DSRej2.Tables(0).Rows(0).Item("_qty_scrapped")
                                End If
                            End If

                        End If
                        HashT.Add(DSMH.Tables(0).Rows(i).Item("_jobsuffix"), DSMH.Tables(0).Rows(i).Item("_jobsuffix"))
                    End If

                Next

                Dim DSX As New DataSet
                DSMH = clsM.GetDataset("select *  from tbJobTrans  where _Job='" & Trim(HissrtJOBNo) & "' and _oper_num=" & Val(HIsstrop) & " and _status='COMPLETED' order by _reworkCount", "tbJobTransHis")
                If DSMH.Tables(0).Rows.Count > 0 Then
                    'Dim i As Integer
                    For i = 0 To DSMH.Tables(0).Rows.Count - 1
                        If HashT.ContainsValue(DSMH.Tables(0).Rows(i).Item("_jobsuffix")) = False Then
                            TotalC_qtry = TotalC_qtry + DSMH.Tables(0).Rows(i).Item("_qty_complete")
                            TotalR_qtry = TotalR_qtry + DSMH.Tables(0).Rows(i).Item("_qty_scrapped")
                            TotalP_qtry = TotalP_qtry + DSMH.Tables(0).Rows(i).Item("_qty_op_qty")
                            HashT.Add(DSMH.Tables(0).Rows(i).Item("_jobsuffix"), DSMH.Tables(0).Rows(i).Item("_jobsuffix"))
                        End If
                    Next

                End If



            Else
                Dim DSX As New DataSet
                DSMH = clsM.GetDataset("select *  from tbJobTrans  where _Job='" & Trim(HissrtJOBNo) & "' and _oper_num=" & Val(HIsstrop) & " and _status='COMPLETED' order by _reworkCount", "tbJobTransHis")
                If DSMH.Tables(0).Rows.Count > 0 Then
                    Dim i As Integer
                    For i = 0 To DSMH.Tables(0).Rows.Count - 1
                        TotalC_qtry = TotalC_qtry + DSMH.Tables(0).Rows(i).Item("_qty_complete")
                        TotalR_qtry = TotalR_qtry + DSMH.Tables(0).Rows(i).Item("_qty_scrapped")
                        TotalP_qtry = TotalP_qtry + DSMH.Tables(0).Rows(i).Item("_qty_op_qty")

                    Next

                End If
            End If


            'Dim DSMAC As New DataSet



            RlQty = TotalC_qtry
            Rlrej = TotalR_qtry
            RlOP = TotalP_qtry

            If RlQty <> 0 And Rlrej <> 0 Or RlOP <> 0 Then
                Try
                    com.Connection = cn
                    com.CommandType = CommandType.Text
                    com.CommandText = "Update tbJobTransMain set _qty_op_qty=" & RlOP & " ,_qty_complete=" & RlQty & ",_qty_scrapped=" & Rlrej & " where _Job='" & HissrtJOBNo & "' and _oper_num=" & HIsstrop
                    cn.Open()
                    com.ExecuteReader()
                    cn.Close()
                Catch ex As Exception

                End Try



            End If

            RlQty = 0
            Rlrej = 0
            RlOP = 0

            'DSMAC = clsM.GetDataset("select _job,_jobsuffix ,Min(_reworkCount) as _reworkCount from tbJobTrans  where _Job='" & Trim(HissrtJOBNo) & "' and _jobsuffix=" & Val(HIsstrop) & " Group by _job,_jobsuffix", "tbJobTransHis")
            'Dim k As Integer = -1
            'If DSMH.Tables(0).Rows.Count > 0 Then
            '    Dim i As Integer
            '    For i = 0 To DSMH.Tables(0).Rows.Count - 1
            '        If HashT.ContainsValue(DSMH.Tables(0).Rows(i).Item("_jobsuffix")) = False Then
            '            Dim DS1 As New DataSet
            '            DS1 = clsM.GetDataset("select _job,_jobsuffix ,Min(_reworkCount) as _reworkCount from tbJobTrans  where _Job='" & Trim(HissrtJOBNo) & "' and _jobsuffix=" & Val(HIsstrop) & " Group by _job,_jobsuffix", "tbJobTransHis")








            '            HashT.Add(k, DSMH.Tables(0).Rows(i).Item("_jobsuffix"))

            '        End If
            '    Next
            'End If






            'If DSMAC.Tables(0).Rows.Count > 0 Then
            '    Dim i As Integer
            '    For i = 0 To DSMAC.Tables(0).Rows.Count - 1
            '        If HashT.ContainsValue(DSMAC.Tables(0).Rows(i).Item("_jobsuffix")) = False Then
            '            HashT.Add(k, DSMAC.Tables(0).Rows(i).Item("_jobsuffix"))
            '            k = k + 1
            '        End If
            '    Next
            'End If
            'Dim j As Integer
            'If k > -1 Then
            '    '  For j = 0 To k
            '    ' Dim xval As String = HashT.ContainsValue(k)
            '    Dim xxx As Object      '
            '    For Each xxx In HashT
            '        MsgBox(xxx.value)
            '    Next


            '    '   Next
            'End If

        Catch ex As Exception
        End Try
    End Sub
    Function CloseAllCase(ByVal strJobNo As String, ByVal strSuf As String, ByVal strSufP As String, ByVal strOPNum As String) As Boolean
        CloseAllCase = False
        Dim DSOP As New DataSet
        DSOP = clsM.GetDataset("select _operationNo,_WC from tbJobRoute  where _job='" & strJobNo & "' and _operationNo>" & strOPNum & " order by _operationNo", "tbOP")
        If DSOP.Tables("tbOP").Rows.Count > 0 Then
            For i = 0 To DSOP.Tables("tbOP").Rows.Count - 1
                With com


                    .Connection = cn
                    .CommandType = CommandType.StoredProcedure

                    .CommandText = "sp_JOB_Close_Com_is_Zero"

                    parm = .Parameters.Add("@Job", SqlDbType.VarChar, 50)
                    parm.Value = Trim(strJobNo)
                    parm = .Parameters.Add("@jobsuffix", SqlDbType.VarChar, 10)
                    parm.Value = Trim(strSuf)
                    parm = .Parameters.Add("@jobsuffixParent", SqlDbType.VarChar, 10)
                    parm.Value = Trim(strSufP)

                    parm = .Parameters.Add("@OldOPNo", SqlDbType.Int)
                    parm.Value = strOPNum

                    parm = .Parameters.Add("@opNo", SqlDbType.Int)
                    parm.Value = DSOP.Tables("tbOP").Rows(i).Item("_operationNo")

                    parm = .Parameters.Add("WC", SqlDbType.VarChar, 256)
                    parm.Value = DSOP.Tables("tbOP").Rows(i).Item("_WC")

                    parm = .Parameters.Add("@NextOPNo", SqlDbType.Int)
                    If DSOP.Tables("tbOP").Rows.Count > i + 1 Then
                        parm.Value = DSOP.Tables("tbOP").Rows(i + 1).Item("_operationNo")
                    Else
                        parm.Value = 0
                    End If
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()
                    strOPNum = DSOP.Tables("tbOP").Rows(i).Item("_operationNo")
                End With

            Next
        End If
    End Function


End Class