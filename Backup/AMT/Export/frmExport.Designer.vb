<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExport))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label3 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.P1 = New System.Windows.Forms.Panel
        Me.lblTotal = New System.Windows.Forms.Label
        Me.lblRunning = New System.Windows.Forms.Label
        Me.PB1 = New System.Windows.Forms.ProgressBar
        Me.butBrows = New System.Windows.Forms.Button
        Me.txtFile = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtsearch = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.PlExport = New System.Windows.Forms.Panel
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.PlWait = New System.Windows.Forms.Panel
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.ckberp = New System.Windows.Forms.CheckBox
        Me.cmbStatus = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtto = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.pic1 = New System.Windows.Forms.PictureBox
        Me.ckbAll = New System.Windows.Forms.CheckBox
        Me.lstv = New System.Windows.Forms.ListView
        Me.lstvLoad = New System.Windows.Forms.ListView
        Me.butSearch = New System.Windows.Forms.Button
        Me.dtpFrom = New System.Windows.Forms.DateTimePicker
        Me.dtpTo = New System.Windows.Forms.DateTimePicker
        Me.txtfrom = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.sfdSave = New System.Windows.Forms.SaveFileDialog
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.P1.SuspendLayout()
        Me.PlExport.SuspendLayout()
        Me.PlWait.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(3, -7)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(772, 703)
        Me.Panel1.TabIndex = 6
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Location = New System.Drawing.Point(3, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(760, 692)
        Me.Panel2.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(13, 5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 15)
        Me.Label3.TabIndex = 35
        Me.Label3.Text = "EXPORT DATA"
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.P1)
        Me.Panel3.Controls.Add(Me.butBrows)
        Me.Panel3.Controls.Add(Me.txtFile)
        Me.Panel3.Controls.Add(Me.Label6)
        Me.Panel3.Controls.Add(Me.txtsearch)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.PlExport)
        Me.Panel3.Controls.Add(Me.PlWait)
        Me.Panel3.Controls.Add(Me.ckberp)
        Me.Panel3.Controls.Add(Me.cmbStatus)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.txtto)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Controls.Add(Me.butSearch)
        Me.Panel3.Controls.Add(Me.dtpFrom)
        Me.Panel3.Controls.Add(Me.dtpTo)
        Me.Panel3.Controls.Add(Me.txtfrom)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Location = New System.Drawing.Point(16, 22)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(734, 664)
        Me.Panel3.TabIndex = 0
        '
        'P1
        '
        Me.P1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.P1.BackColor = System.Drawing.Color.Gray
        Me.P1.Controls.Add(Me.lblTotal)
        Me.P1.Controls.Add(Me.lblRunning)
        Me.P1.Controls.Add(Me.PB1)
        Me.P1.Location = New System.Drawing.Point(9, 611)
        Me.P1.Name = "P1"
        Me.P1.Size = New System.Drawing.Size(719, 50)
        Me.P1.TabIndex = 64
        Me.P1.Visible = False
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(673, 11)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(14, 13)
        Me.lblTotal.TabIndex = 62
        Me.lblTotal.Text = "0"
        '
        'lblRunning
        '
        Me.lblRunning.AutoSize = True
        Me.lblRunning.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRunning.Location = New System.Drawing.Point(289, 33)
        Me.lblRunning.Name = "lblRunning"
        Me.lblRunning.Size = New System.Drawing.Size(14, 13)
        Me.lblRunning.TabIndex = 61
        Me.lblRunning.Text = "0"
        '
        'PB1
        '
        Me.PB1.Location = New System.Drawing.Point(8, 11)
        Me.PB1.Name = "PB1"
        Me.PB1.Size = New System.Drawing.Size(657, 19)
        Me.PB1.TabIndex = 0
        '
        'butBrows
        '
        Me.butBrows.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.butBrows.BackColor = System.Drawing.Color.DimGray
        Me.butBrows.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBrows.ForeColor = System.Drawing.Color.White
        Me.butBrows.Location = New System.Drawing.Point(274, 588)
        Me.butBrows.Name = "butBrows"
        Me.butBrows.Size = New System.Drawing.Size(17, 16)
        Me.butBrows.TabIndex = 8
        Me.butBrows.Text = "..."
        Me.butBrows.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butBrows.UseVisualStyleBackColor = False
        '
        'txtFile
        '
        Me.txtFile.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtFile.BackColor = System.Drawing.Color.White
        Me.txtFile.Location = New System.Drawing.Point(75, 588)
        Me.txtFile.Name = "txtFile"
        Me.txtFile.ReadOnly = True
        Me.txtFile.Size = New System.Drawing.Size(195, 20)
        Me.txtFile.TabIndex = 63
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 591)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(63, 13)
        Me.Label6.TabIndex = 62
        Me.Label6.Text = "File Name"
        '
        'txtsearch
        '
        Me.txtsearch.Location = New System.Drawing.Point(136, 40)
        Me.txtsearch.Name = "txtsearch"
        Me.txtsearch.Size = New System.Drawing.Size(155, 20)
        Me.txtsearch.TabIndex = 61
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(79, 41)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 13)
        Me.Label5.TabIndex = 60
        Me.Label5.Text = "Job no"
        '
        'PlExport
        '
        Me.PlExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PlExport.Controls.Add(Me.Button3)
        Me.PlExport.Controls.Add(Me.Button4)
        Me.PlExport.Location = New System.Drawing.Point(507, 578)
        Me.PlExport.Name = "PlExport"
        Me.PlExport.Size = New System.Drawing.Size(200, 37)
        Me.PlExport.TabIndex = 59
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DimGray
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(102, 8)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 1
        Me.Button3.Text = "Cancel"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DimGray
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(21, 8)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 0
        Me.Button4.Text = "Re-Export"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'PlWait
        '
        Me.PlWait.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PlWait.Controls.Add(Me.Button2)
        Me.PlWait.Controls.Add(Me.Button1)
        Me.PlWait.Location = New System.Drawing.Point(304, 578)
        Me.PlWait.Name = "PlWait"
        Me.PlWait.Size = New System.Drawing.Size(200, 37)
        Me.PlWait.TabIndex = 58
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DimGray
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(102, 8)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Cancel"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DimGray
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(21, 8)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Export"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ckberp
        '
        Me.ckberp.AutoSize = True
        Me.ckberp.Location = New System.Drawing.Point(420, 14)
        Me.ckberp.Name = "ckberp"
        Me.ckberp.Size = New System.Drawing.Size(77, 17)
        Me.ckberp.TabIndex = 57
        Me.ckberp.Text = "ERP Verify"
        Me.ckberp.UseVisualStyleBackColor = True
        '
        'cmbStatus
        '
        Me.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbStatus.FormattingEnabled = True
        Me.cmbStatus.Items.AddRange(New Object() {"Wait For Export", "Exported"})
        Me.cmbStatus.Location = New System.Drawing.Point(136, 64)
        Me.cmbStatus.Name = "cmbStatus"
        Me.cmbStatus.Size = New System.Drawing.Size(251, 21)
        Me.cmbStatus.TabIndex = 56
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(88, 67)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 15)
        Me.Label4.TabIndex = 55
        Me.Label4.Text = "Status"
        '
        'txtto
        '
        Me.txtto.Location = New System.Drawing.Point(269, 14)
        Me.txtto.Name = "txtto"
        Me.txtto.Size = New System.Drawing.Size(100, 20)
        Me.txtto.TabIndex = 42
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(251, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(21, 15)
        Me.Label2.TabIndex = 54
        Me.Label2.Text = "To"
        '
        'Panel5
        '
        Me.Panel5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Panel5.BackColor = System.Drawing.Color.LightGray
        Me.Panel5.Controls.Add(Me.pic1)
        Me.Panel5.Controls.Add(Me.ckbAll)
        Me.Panel5.Controls.Add(Me.lstv)
        Me.Panel5.Controls.Add(Me.lstvLoad)
        Me.Panel5.Location = New System.Drawing.Point(8, 89)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(723, 488)
        Me.Panel5.TabIndex = 53
        '
        'pic1
        '
        Me.pic1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pic1.BackColor = System.Drawing.Color.White
        Me.pic1.Location = New System.Drawing.Point(317, 206)
        Me.pic1.Name = "pic1"
        Me.pic1.Size = New System.Drawing.Size(54, 53)
        Me.pic1.TabIndex = 65
        Me.pic1.TabStop = False
        Me.pic1.Visible = False
        '
        'ckbAll
        '
        Me.ckbAll.AutoSize = True
        Me.ckbAll.Location = New System.Drawing.Point(5, 1)
        Me.ckbAll.Name = "ckbAll"
        Me.ckbAll.Size = New System.Drawing.Size(71, 17)
        Me.ckbAll.TabIndex = 64
        Me.ckbAll.Text = "Check All"
        Me.ckbAll.UseVisualStyleBackColor = True
        '
        'lstv
        '
        Me.lstv.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.lstv.CheckBoxes = True
        Me.lstv.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(5, 18)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(715, 454)
        Me.lstv.TabIndex = 7
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'lstvLoad
        '
        Me.lstvLoad.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.lstvLoad.CheckBoxes = True
        Me.lstvLoad.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstvLoad.FullRowSelect = True
        Me.lstvLoad.GridLines = True
        Me.lstvLoad.Location = New System.Drawing.Point(4, 17)
        Me.lstvLoad.Name = "lstvLoad"
        Me.lstvLoad.Size = New System.Drawing.Size(715, 454)
        Me.lstvLoad.TabIndex = 66
        Me.lstvLoad.UseCompatibleStateImageBehavior = False
        Me.lstvLoad.View = System.Windows.Forms.View.Details
        '
        'butSearch
        '
        Me.butSearch.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butSearch.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSearch.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butSearch.Image = CType(resources.GetObject("butSearch.Image"), System.Drawing.Image)
        Me.butSearch.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butSearch.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSearch.Location = New System.Drawing.Point(503, 3)
        Me.butSearch.Name = "butSearch"
        Me.butSearch.Size = New System.Drawing.Size(47, 56)
        Me.butSearch.TabIndex = 0
        Me.butSearch.Text = "Search"
        Me.butSearch.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butSearch.UseVisualStyleBackColor = False
        '
        'dtpFrom
        '
        Me.dtpFrom.Location = New System.Drawing.Point(231, 14)
        Me.dtpFrom.Name = "dtpFrom"
        Me.dtpFrom.Size = New System.Drawing.Size(19, 20)
        Me.dtpFrom.TabIndex = 44
        '
        'dtpTo
        '
        Me.dtpTo.Location = New System.Drawing.Point(368, 14)
        Me.dtpTo.Name = "dtpTo"
        Me.dtpTo.Size = New System.Drawing.Size(19, 20)
        Me.dtpTo.TabIndex = 43
        '
        'txtfrom
        '
        Me.txtfrom.Location = New System.Drawing.Point(136, 14)
        Me.txtfrom.Name = "txtfrom"
        Me.txtfrom.Size = New System.Drawing.Size(100, 20)
        Me.txtfrom.TabIndex = 40
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(5, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(127, 15)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = " Date between    from"
        '
        'sfdSave
        '
        Me.sfdSave.AddExtension = False
        Me.sfdSave.DefaultExt = "Microsoft Excel Worksheet"
        Me.sfdSave.Filter = "Planilhas (*.xls)|*.xls"
        Me.sfdSave.SupportMultiDottedExtensions = True
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "icon_check.jpg")
        '
        'frmExport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(238, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(779, 719)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MinimizeBox = False
        Me.Name = "frmExport"
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.P1.ResumeLayout(False)
        Me.P1.PerformLayout()
        Me.PlExport.ResumeLayout(False)
        Me.PlWait.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents P1 As System.Windows.Forms.Panel
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents lblRunning As System.Windows.Forms.Label
    Friend WithEvents PB1 As System.Windows.Forms.ProgressBar
    Friend WithEvents butBrows As System.Windows.Forms.Button
    Friend WithEvents txtFile As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtsearch As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents PlExport As System.Windows.Forms.Panel
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents PlWait As System.Windows.Forms.Panel
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ckberp As System.Windows.Forms.CheckBox
    Friend WithEvents cmbStatus As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtto As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents ckbAll As System.Windows.Forms.CheckBox
    Friend WithEvents lstv As System.Windows.Forms.ListView
    Friend WithEvents butSearch As System.Windows.Forms.Button
    Friend WithEvents dtpFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtfrom As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents sfdSave As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents pic1 As System.Windows.Forms.PictureBox
    Friend WithEvents lstvLoad As System.Windows.Forms.ListView
End Class
