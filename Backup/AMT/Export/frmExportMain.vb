Imports System.Data.SqlClient
Imports System.Runtime.InteropServices
'Imports Excel
Imports Microsoft.VisualBasic.DateAndTime

Public Class frmExportMain
    Dim parm As SqlParameter
    Private Sub frmExportMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cmbStatus.SelectedIndex = 0
        Dim sd As Date
        sd = DateSerial(Year(Now), Month(Now), Day(Now))
        dtpFrom.Value = sd
        txtfrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        'sd = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 1, sd))
        dtpTo.Value = sd
        txtto.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        With lstv
            .Columns.Add("Job", 100, HorizontalAlignment.Left)
            .Columns.Add("auto no.", 0, HorizontalAlignment.Left)
            .Columns.Add("Item", 100, HorizontalAlignment.Left)
            .Columns.Add("Operation No.", 100, HorizontalAlignment.Left)
            .Columns.Add("Parent Sufix", 50, HorizontalAlignment.Left)
            .Columns.Add("Child Sufix", 50, HorizontalAlignment.Left)

            .Columns.Add("Date From", 80, HorizontalAlignment.Left)
            .Columns.Add("Date To", 80, HorizontalAlignment.Left)
            .Columns.Add("Quantity Released", 60, HorizontalAlignment.Left)
            .Columns.Add("Quantity Completed", 60, HorizontalAlignment.Left)
            .Columns.Add("Quantity Scrapped", 60, HorizontalAlignment.Left)
            .Columns.Add("ID", 0, HorizontalAlignment.Left)
        End With


    End Sub
    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtfrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False
    End Sub

    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtto.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        dtpTo.Checked = False
    End Sub

    Private Sub cmbStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbStatus.SelectedIndexChanged
        lstv.Items.Clear()
        If cmbStatus.SelectedIndex = 0 Then
            PlExport.Visible = False
            PlWait.Visible = True
        Else
            PlExport.Visible = True
            PlWait.Visible = False
        End If
    End Sub
    'Private Sub butSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
    '    lblTotal.Text = 0
    '    lblRunning.Text = 0
    '    P1.Visible = False
    '    lstv.Items.Clear()
    '    Try
    '        Dim strsql As String
    '        Dim SD As Date
    '        Dim ED As Date
    '        If txtfrom.Text <> "" Then
    '            Dim s() As String = Split(txtfrom.Text, "/")
    '            SD = DateSerial(s(2), s(1), s(0))
    '        End If
    '        If txtto.Text <> "" Then
    '            Dim s1() As String = Split(txtto.Text, "/")
    '            ED = DateSerial(s1(2), s1(1), s1(0))
    '        End If

    '        Dim contsql As String
    '        If cmbStatus.Text = "Pending" Then
    '            contsql = " _export='N' and _job not in(select  distinct _job from tbjobTrans where _trans_dateto between " & SD.ToOADate & " and " & ED.ToOADate & ") "
    '        Else
    '            contsql = "  _export='Y' and _job  in(select  distinct _job from tbjobTrans where _trans_dateto between " & SD.ToOADate & " and " & ED.ToOADate & ") "
    '        End If


    '        Dim contsqldir As String
    '        If cmbStatus.Text = "Pending" Then
    '            contsqldir = "  _job ='" & fncstr(Trim(txtsearch.Text)) & "' and _export='N'"
    '        Else
    '            contsqldir = "  _job ='" & fncstr(Trim(txtsearch.Text)) & "'  and _export='Y'"
    '        End If



    '        If txtsearch.Text = "" Then
    '            strsql = "select * from tbjobTrans  WHERE " & contsql & " order by _job,_jobDate"
    '        Else
    '            strsql = "select * from tbJob WHERE " & contsqldir
    '        End If








    '        Dim DSERP As New DataSet
    '        Dim daERP As New SqlDataAdapter
    '        Dim SQLERP As String
    '        If ckberp.Checked = True Then
    '            If txtsearch.Text = "" Then
    '                With com
    '                    SQLERP = "select * from Job where job_Date between @SD and @ED"
    '                    .Connection = cnser
    '                    .CommandType = CommandType.Text
    '                    .CommandText = SQLERP
    '                    parm = .Parameters.Add("@sd", SqlDbType.SmallDateTime)
    '                    parm.Value = SD
    '                    parm = .Parameters.Add("@Ed", SqlDbType.SmallDateTime)
    '                    parm.Value = ED
    '                    daERP.SelectCommand = com
    '                    daERP.Fill(DSERP, "tbERP")
    '                    .Parameters.Clear()
    '                End With
    '            Else
    '                With com
    '                    SQLERP = "select * from Job where job=@job"
    '                    .Connection = cnser
    '                    .CommandType = CommandType.Text
    '                    .CommandText = SQLERP
    '                    parm = .Parameters.Add("@Job", SqlDbType.VarChar, 50)
    '                    parm.Value = Trim(txtsearch.Text)
    '                    daERP.SelectCommand = com
    '                    daERP.Fill(DSERP, "tbERP")
    '                    .Parameters.Clear()
    '                End With
    '            End If
    '        End If





    '        Dim ds As New DataSet
    '        Dim da As New SqlDataAdapter
    '        da = New SqlDataAdapter(strsql, cn)
    '        da.Fill(ds)
    '        da.Dispose()
    '        strsql = ""
    '        If txtsearch.Text = "" Then
    '            strsql = "select _job,_oper_num,Sum(_qty_scrapped)as _qty_scrapped,Sum(_qty_complete) as _qty_complete  from tbJobTrans WHERE  _job in(select  _job from tbjob where _jobDate between " & SD.ToOADate & " and " & ED.ToOADate & contsql & "  )  group by _job,_oper_num"
    '        Else
    '            strsql = "select  _job,_oper_num,Sum(_qty_scrapped)as _qty_scrapped,Sum(_qty_complete) as _qty_complete  from tbJobTrans WHERE   _job='" & fncstr(Trim(txtsearch.Text)) & " '" & contsqldir & "  group by _job,_oper_num "
    '        End If
    '        Dim daProg As New SqlDataAdapter
    '        Dim dsProg As New DataSet
    '        daProg = New SqlDataAdapter(strsql, cn)
    '        daProg.Fill(dsProg)
    '        dsProg.Dispose()



    '        strsql = ""
    '        If txtsearch.Text = "" Then
    '            strsql = "select _job,max(_operationNo) as _operationNo from tbJobRoute  WHERE  _job in(select  _job from tbjob where _jobDate between " & SD.ToOADate & " and " & ED.ToOADate & contsql & " ) group by _job"
    '        Else
    '            strsql = "select _job,max(_operationNo) as _operationNo from tbJobRoute  WHERE   _job='" & fncstr(Trim(txtsearch.Text)) & "' " & contsqldir & "  group by _job"
    '        End If
    '        Dim daRoute As New SqlDataAdapter
    '        Dim dsRoute As New DataSet
    '        daRoute = New SqlDataAdapter(strsql, cn)
    '        daRoute.Fill(dsRoute)
    '        daRoute.Dispose()






    '        If ds.Tables(0).Rows.Count > 0 Then
    '            Dim i As Integer
    '            For i = 0 To ds.Tables(0).Rows.Count - 1



    '                With ds.Tables(0).Rows(i)

    '                    Dim MaxOpno As Integer = 0
    '                    Dim DRRou As DataRow() = dsRoute.Tables(0).Select("_job='" & .Item("_job") & "'") ' get Job Route Data
    '                    If DRRou.Length > 0 Then
    '                        MaxOpno = DRRou(0).Item("_operationNo")
    '                    End If
    '                    Dim Reqtyst As Boolean = False
    '                    If .Item("_qtyReleased") <= .Item("_qtyCompleted") + .Item("_qtyScrapped") Then
    '                        Reqtyst = True
    '                    End If
    '                    Dim DRPr As DataRow() = dsProg.Tables(0).Select("_job='" & .Item("_job") & "'")  ' get Job Job Trans

    '                    Dim stus As Integer = 0
    '                    If DRPr.Length > 0 Then
    '                        Dim j As Integer
    '                        Dim TotRej As Double = 0
    '                        Dim TotCOM As Double = 0
    '                        Dim dists As Boolean = False
    '                        For j = 0 To DRPr.Length - 1
    '                            TotRej = TotRej + DRPr(j).Item("_qty_scrapped")
    '                            dists = True
    '                            stus = 1
    '                            If MaxOpno = DRPr(j).Item("_oper_num") Then
    '                                TotCOM = TotCOM + DRPr(j).Item("_qty_complete")
    '                            End If
    '                        Next


    '                        If (TotRej + TotCOM) = .Item("_qtyReleased") Then
    '                            Dim etpst As Boolean = False
    '                            If ckberp.Checked = True Then
    '                                Dim NRERP() As DataRow = DSERP.Tables(0).Select("job='" & .Item("_job") & "'")

    '                                If NRERP.Length > 0 Then
    '                                    If Val(NRERP(0).Item("qty_complete") + NRERP(0).Item("qty_scrapped")) <> 0 Then
    '                                        Dim ls As New ListViewItem(Trim(.Item("_job")), 0)
    '                                        ls.SubItems.Add(Trim(.Item("_jobsuffix")))
    '                                        ls.SubItems.Add(Format(Date.FromOADate(.Item("_jobDate")), "dd/MM/yyyy"))
    '                                        ls.SubItems.Add(Trim(.Item("_jobtype")))
    '                                        ls.SubItems.Add(Trim(.Item("_item")))
    '                                        ls.SubItems.Add(Trim(.Item("_qtyReleased")))

    '                                        If TotCOM = 0 Then
    '                                            ls.SubItems.Add("-")
    '                                        Else
    '                                            ls.SubItems.Add(TotCOM)
    '                                        End If

    '                                        If TotRej = 0 Then
    '                                            ls.SubItems.Add("-")
    '                                        Else
    '                                            ls.SubItems.Add(TotRej)
    '                                        End If
    '                                        lstv.Items.Add(ls).ForeColor = Color.Purple
    '                                        etpst = True
    '                                    End If
    '                                End If
    '                                If etpst = False Then
    '                                    Dim ls As New ListViewItem(Trim(.Item("_job")))
    '                                    ls.SubItems.Add(Trim(.Item("_jobsuffix")))
    '                                    ls.SubItems.Add(Format(Date.FromOADate(.Item("_jobDate")), "dd/MM/yyyy"))
    '                                    ls.SubItems.Add(Trim(.Item("_jobtype")))
    '                                    ls.SubItems.Add(Trim(.Item("_item")))
    '                                    ls.SubItems.Add(Trim(.Item("_qtyReleased")))

    '                                    If TotCOM = 0 Then
    '                                        ls.SubItems.Add("-")
    '                                    Else
    '                                        ls.SubItems.Add(TotCOM)
    '                                    End If

    '                                    If TotRej = 0 Then
    '                                        ls.SubItems.Add("-")
    '                                    Else
    '                                        ls.SubItems.Add(TotRej)
    '                                    End If
    '                                    lstv.Items.Add(ls).ForeColor = Color.Green
    '                                    etpst = False
    '                                End If




    '                            Else
    '                                Dim ls As New ListViewItem(Trim(.Item("_job")))
    '                                ls.SubItems.Add(Trim(.Item("_jobsuffix")))
    '                                ls.SubItems.Add(Format(Date.FromOADate(.Item("_jobDate")), "dd/MM/yyyy"))
    '                                ls.SubItems.Add(Trim(.Item("_jobtype")))
    '                                ls.SubItems.Add(Trim(.Item("_item")))
    '                                ls.SubItems.Add(Trim(.Item("_qtyReleased")))

    '                                If TotCOM = 0 Then
    '                                    ls.SubItems.Add("-")
    '                                Else
    '                                    ls.SubItems.Add(TotCOM)
    '                                End If

    '                                If TotRej = 0 Then
    '                                    ls.SubItems.Add("-")
    '                                Else
    '                                    ls.SubItems.Add(TotRej)
    '                                End If
    '                                lstv.Items.Add(ls).ForeColor = Color.Green
    '                            End If


    '                        End If
    '                    End If

    '                End With
    '            Next
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message)
    '    Finally
    '    End Try
    'End Sub
    Private Sub butSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
        lblTotal.Text = 0
        lblRunning.Text = 0
        P1.Visible = False
        lstv.Items.Clear()
        Try
            Dim strsql As String
            Dim SD As Date
            Dim ED As Date
            If txtfrom.Text <> "" Then
                Dim s() As String = Split(txtfrom.Text, "/")
                SD = DateSerial(s(2), s(1), s(0))
            End If
            If txtto.Text <> "" Then
                Dim s1() As String = Split(txtto.Text, "/")
                ED = CDate(DateSerial(s1(2), s1(1), s1(0)) & " 23:59:00")
            End If

            Dim contsql As String
            If cmbStatus.Text = "Pending" Then
                contsql = " _job  in(select  distinct _job from tbjobTrans where  _status='COMPLETED' and _end_Date between " & SD.ToOADate & " and " & ED.ToOADate & " and _export='N') "
            Else
                contsql = "   _job  in(select  distinct _job from tbjobTrans where _status='COMPLETED' and _end_Date between " & SD.ToOADate & " and " & ED.ToOADate & " and _export='Y') "
            End If


            Dim contsqldir As String
            If cmbStatus.Text = "Pending" Then
                contsqldir = "  _job ='" & fncstr(Trim(txtsearch.Text)) & "'"
            Else
                contsqldir = "  _job ='" & fncstr(Trim(txtsearch.Text)) & "'"
            End If


            Dim NewSQl As String
            If txtsearch.Text = "" Then
                NewSQl = "select * from tbjobTrans  WHERE " & contsql & " order by _job,_jobDate"
            Else
                NewSQl = "select * from tbjobTrans  WHERE " & contsqldir & " order by _job,_jobDate"
            End If













            'Dim DSERP As New DataSet
            'Dim daERP As New SqlDataAdapter
            'Dim SQLERP As String
            'If ckberp.Checked = True Then
            '    If txtsearch.Text = "" Then
            '        With com
            '            SQLERP = "select * from Job where job_Date between @SD and @ED"
            '            .Connection = cnser
            '            .CommandType = CommandType.Text
            '            .CommandText = SQLERP
            '            parm = .Parameters.Add("@sd", SqlDbType.SmallDateTime)
            '            parm.Value = SD
            '            parm = .Parameters.Add("@Ed", SqlDbType.SmallDateTime)
            '            parm.Value = ED
            '            daERP.SelectCommand = com
            '            daERP.Fill(DSERP, "tbERP")
            '            .Parameters.Clear()
            '        End With
            '    Else
            '        With com
            '            SQLERP = "select * from Job where job=@job"
            '            .Connection = cnser
            '            .CommandType = CommandType.Text
            '            .CommandText = SQLERP
            '            parm = .Parameters.Add("@Job", SqlDbType.VarChar, 50)
            '            parm.Value = Trim(txtsearch.Text)
            '            daERP.SelectCommand = com
            '            daERP.Fill(DSERP, "tbERP")
            '            .Parameters.Clear()
            '        End With
            '    End If
            'End If








            'strsql = ""
            'If txtsearch.Text = "" Then
            '    strsql = "select * from tbJobRoute where " & contsql & " order by _job,_operationNo DESC"

            'Else
            '    strsql = "select * from tbJobRoute where " & contsqldir & " order by _job,_operationNo DESC"
            'End If
            'Dim daRoute As New SqlDataAdapter
            'Dim dsRoute As New DataSet
            'daRoute = New SqlDataAdapter(strsql, cn)
            'daRoute.Fill(dsRoute)
            'daRoute.Dispose()







            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            da = New SqlDataAdapter(NewSQl, cn)
            da.Fill(ds)
            da.Dispose()
            strsql = ""





            If ds.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                Dim Jnid As String = ""

                For i = 0 To ds.Tables(0).Rows.Count - 1
                    With ds.Tables(0).Rows(i)
                        If .Item("_end_Date") = 0 Then
                        Else


                            Dim ls As New ListViewItem(Trim(.Item("_job")))
                            ls.SubItems.Add(Trim(.Item("_tansnum")))
                            ls.SubItems.Add(Trim(.Item("_item")))
                            ls.SubItems.Add(Trim(.Item("_oper_num")))
                            ls.SubItems.Add(Trim(.Item("_jobsuffixParent")))
                            ls.SubItems.Add(Trim(.Item("_jobsuffix")))
                            ls.SubItems.Add(Format(Date.FromOADate(.Item("_start_Date")), "dd/MM/yyyy HH:mm"))
                            ls.SubItems.Add(Format(Date.FromOADate(.Item("_end_Date")), "dd/MM/yyyy HH:mm"))
                            ls.SubItems.Add(Trim(.Item("_qty_Rele_qty")))
                            ls.SubItems.Add(Trim(.Item("_qty_op_qty")))
                            ls.SubItems.Add(Trim(.Item("_qty_complete")))
                            ls.SubItems.Add(Trim(.Item("_qty_scrapped")))
                            ls.SubItems.Add("0")
                            If cmbStatus.Text = "Pending" Then
                                If .Item("_export") = "N" Then
                                    lstv.Items.Add(ls).ForeColor = Color.Black
                                End If
                            Else
                                If .Item("_export") = "Y" Then
                                    lstv.Items.Add(ls).ForeColor = Color.Green
                                End If
                            End If



                        End If



                    End With

                Next
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
        End Try
    End Sub

    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click
        'Try
        '    MsgBox(lstv.SelectedItems(0).SubItems(2).Text)
        '    If lstv.SelectedItems(0).SubItems(11).Text = "1" Then
        '        lstv.SelectedItems(0).Checked = True
        '    Else
        '        lstv.SelectedItems(0).Checked = False
        '    End If
        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        'End Try
    End Sub
    Private Sub lstv_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.DoubleClick
        'Try
        '    If lstv.SelectedItems(0).SubItems(11).Text = "1" Then
        '        lstv.SelectedItems(0).Checked = True
        '    Else
        '        lstv.SelectedItems(0).Checked = False
        '    End If
        'Catch ex As Exception
        'End Try
        'SessionJobno = lstv.SelectedItems(0).Text
        Dim obj As New frmTans
        obj.JobNo = lstv.SelectedItems(0).Text
        obj.Jobdate = lstv.SelectedItems(0).SubItems(2).Text
        obj.JobItem = lstv.SelectedItems(0).SubItems(4).Text
        obj.JobQty = lstv.SelectedItems(0).SubItems(5).Text
        'obj.Width = Me.Width
        'obj.Height = Me.Height
        'obj.Left = Me.Left
        'obj.Top = Me.Top
        obj.ShowDialog()
    End Sub

   
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        lblRunning.Text = 0
        ' Dim at As Integer
        If txtFile.Text = "" Then
            MsgBox("Please enter the file name!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If

        Dim str, filename As String
        str = ""
        filename = ""
        Dim col, row As Integer
        col = 0
        row = 0
        Dim Excel As Object = CreateObject("Excel.Application")
        If Excel Is Nothing Then
            MsgBox("It appears that Excel is not installed on this machine. This operation requires MS Excel to be installed on this machine.", MsgBoxStyle.Critical, "eWIP")
            Exit Sub
        End If

        P1.Visible = True
        Try
            With Excel
                .SheetsInNewWorkbook = 1
                .Workbooks.Add()
                .Worksheets(1).Select()

                Dim i As Integer = 1

                .cells(1, 1).value = "Transaction"
                .cells(1, 1).EntireRow.Font.Bold = True
                .cells(1, 2).value = "Transaction Type"
                .cells(1, 2).EntireRow.Font.Bold = True
                .cells(1, 3).value = "Transaction Date"
                .cells(1, 3).EntireRow.Font.Bold = True

                .cells(1, 4).value = "Job"
                .cells(1, 4).EntireRow.Font.Bold = True
                .cells(1, 5).value = "Job Suffix"
                .cells(1, 5).EntireRow.Font.Bold = True
                .cells(1, 6).value = "Operation"
                .cells(1, 6).EntireRow.Font.Bold = True
                .cells(1, 7).value = "Item"
                .cells(1, 7).EntireRow.Font.Bold = True
                .cells(1, 8).value = "Item Description"
                .cells(1, 8).EntireRow.Font.Bold = True
                .cells(1, 9).value = "WC"
                .cells(1, 9).EntireRow.Font.Bold = True
                .cells(1, 10).value = "WC Description"
                .cells(1, 10).EntireRow.Font.Bold = True


                .cells(1, 11).value = "User Initials"
                .cells(1, 11).EntireRow.Font.Bold = True
                .cells(1, 12).value = "Employee"
                .cells(1, 12).EntireRow.Font.Bold = True
                .cells(1, 13).value = "Employee Name"
                .cells(1, 13).EntireRow.Font.Bold = True
                .cells(1, 14).value = "Shift"
                .cells(1, 14).EntireRow.Font.Bold = True
                .cells(1, 15).value = "Pay Type"
                .cells(1, 15).EntireRow.Font.Bold = True
                .cells(1, 16).value = "Pay Rate"
                .cells(1, 16).EntireRow.Font.Bold = True
                .cells(1, 17).value = "Job Cost Rate"
                .cells(1, 17).EntireRow.Font.Bold = True
                .cells(1, 18).value = "Indirect Code"
                .cells(1, 18).EntireRow.Font.Bold = True
                .cells(1, 19).value = "Description"
                .cells(1, 19).EntireRow.Font.Bold = True
                .cells(1, 20).value = "Completed"
                .cells(1, 20).EntireRow.Font.Bold = True

                .cells(1, 21).value = "Scrapped"
                .cells(1, 21).EntireRow.Font.Bold = True
                .cells(1, 22).value = "Moved"
                .cells(1, 22).EntireRow.Font.Bold = True
                .cells(1, 23).value = "U/M"
                .cells(1, 23).EntireRow.Font.Bold = True
                .cells(1, 24).value = "Warehouse"
                .cells(1, 24).EntireRow.Font.Bold = True
                .cells(1, 25).value = "Reason"
                .cells(1, 25).EntireRow.Font.Bold = True
                .cells(1, 26).value = "Reason Description"
                .cells(1, 26).EntireRow.Font.Bold = True
                .cells(1, 27).value = "Next Operation"
                .cells(1, 27).EntireRow.Font.Bold = True
                .cells(1, 28).value = "Work Center"
                .cells(1, 28).EntireRow.Font.Bold = True
                .cells(1, 29).value = "Work Center Description"
                .cells(1, 29).EntireRow.Font.Bold = True
                .cells(1, 30).value = "Move To Location"
                .cells(1, 30).EntireRow.Font.Bold = True
                .cells(1, 31).value = "Location Description"
                .cells(1, 31).EntireRow.Font.Bold = True
                .cells(1, 32).value = "Lot"
                .cells(1, 32).EntireRow.Font.Bold = True
                .cells(1, 33).value = "Start Time"
                .cells(1, 33).EntireRow.Font.Bold = True
                .cells(1, 34).value = "End Time"
                .cells(1, 34).EntireRow.Font.Bold = True
                .cells(1, 35).value = "Total Hours"
                .cells(1, 35).EntireRow.Font.Bold = True

                .cells(1, 36).value = "Cost Code"
                .cells(1, 36).EntireRow.Font.Bold = True
                .cells(1, 37).value = "Control Point"
                .cells(1, 37).EntireRow.Font.Bold = True
                .cells(1, 38).value = "Oper Complete"
                .cells(1, 38).EntireRow.Font.Bold = True
                .cells(1, 39).value = "Close Job"
                .cells(1, 39).EntireRow.Font.Bold = True
                .cells(1, 40).value = "Issue to Parent"
                .cells(1, 40).EntireRow.Font.Bold = True
                .cells(1, 41).value = "Remark 1"
                .cells(1, 41).EntireRow.Font.Bold = True
                .cells(1, 42).value = "Remark 2"
                .cells(1, 42).EntireRow.Font.Bold = True
                .cells(1, 43).value = "Remark 3"
                .cells(1, 43).EntireRow.Font.Bold = True
                ' .Cells(2, 1).Value = "Test"
                '.Cells(2, 2).Value = "Data"
                i += 1

            End With


            Dim k As Integer
            Dim exid As String = ""
            Dim joblstno As String = ""
            If lstv.CheckedItems.Count > 0 Then

                For k = 0 To lstv.CheckedItems.Count - 1
                    ' If Trim(lstv.CheckedItems(k).SubItems(12).Text) = "1" Then
                    If joblstno = "" Then
                        joblstno = "" & Trim(lstv.CheckedItems(k).SubItems(1).Text) & ""
                    Else
                        joblstno = joblstno & "," & Trim(lstv.CheckedItems(k).SubItems(1).Text) & ""
                    End If
                    'End If
                Next
            End If
            'Dim SQLJob As String
            'Dim ADJob As New SqlDataAdapter
            'Dim DSJob As New DataSet
            'With com
            '    SQLJob = "select * from tbJob where _job In(" & joblstno & ")"
            '    .Connection = cn
            '    .CommandType = CommandType.Text
            '    .CommandText = SQLJob
            '    ADJob.SelectCommand = com
            '    ADJob.Fill(DSJob, "tbERP")
            '    .Parameters.Clear()
            'End With

            'PB1.Value = 0

            Dim SQLWC As String
            Dim ADWC As New SqlDataAdapter
            Dim DSWC As New DataSet
            With com
                SQLWC = "select _wc,_description from tbWC"
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = SQLWC
                ADWC.SelectCommand = com
                ADWC.Fill(DSWC, "tbRoute")
                .Parameters.Clear()
            End With



            Dim SQLRoute As String
            Dim ADRoute As New SqlDataAdapter
            Dim DSRoute As New DataSet
            With com
                SQLRoute = "select * from tbJobroute where _job In(select distinct(_job) from tbJobTrans where _tansnum in(" & joblstno & "))  order by _job, _operationNo"
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = SQLRoute
                ADRoute.SelectCommand = com
                ADRoute.Fill(DSRoute, "tbRoute")
                .Parameters.Clear()
            End With

            Dim SQLReject As String
            Dim ADReject As New SqlDataAdapter
            Dim DSReject As New DataSet
            With com
                SQLReject = "select * from tbRejectedTrans where _refID In(" & joblstno & ")"
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = SQLReject
                ADReject.SelectCommand = com
                ADReject.Fill(DSReject, "tbRejected")

            End With


            Dim SQLTrans As String
            Dim ADTrans As New SqlDataAdapter
            Dim DSTrans As New DataSet
            With com
                SQLTrans = "select * from tbJobTrans where _tansnum In(" & joblstno & ") order by _job, _oper_num,_start_Date"
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = SQLTrans
                ADTrans.SelectCommand = com
                ADTrans.Fill(DSTrans, "tbERP")
                .Parameters.Clear()
            End With
            lblTotal.Text = DSTrans.Tables(0).Rows.Count
            PB1.Value = 0
            PB1.Minimum = 0
            PB1.Maximum = DSTrans.Tables(0).Rows.Count

            If DSRoute.Tables(0).Rows.Count > 0 Then
                Dim x As Integer = 0
                Dim xl As Integer = 1
                For x = 0 To DSRoute.Tables(0).Rows.Count - 1
                    Dim DRdata() As DataRow = DSTrans.Tables(0).Select("_job='" & DSRoute.Tables(0).Rows(x).Item("_job") & "' and _oper_num=" & DSRoute.Tables(0).Rows(x).Item("_operationNo"))

                    Dim Job As String = ""
                    Dim JobSuffix As String = ""
                    Dim Jdate As Double = 0
                    Dim JobRel As Double = 0
                    Dim ItemName As String = ""
                    Job = DSRoute.Tables(0).Rows(x).Item("_job")
                    JobSuffix = DSRoute.Tables(0).Rows(x).Item("_jobsuffix")
                    ItemName = ""
                    Jdate = 0
                    JobRel = 0
                    Dim Transdate As Double = 0
                    Dim Operation As Integer = 0
                    Dim WC As String = ""
                    Dim WCDes As String = ""
                    Dim StartTime As Integer = 0
                    Dim EndTime As Integer = 0
                    Dim remarks1 As String = ""
                    Dim remarks2 As String = ""
                    Dim remarks3 As String = ""
                    Dim exst As Boolean = False
                    Dim COQ As Double = 0
                    Dim RJQ As Double = 0
                    Dim NextOp As Integer = 0

                    If DRdata.Length > 0 Then
                        Dim j As Integer
                        For j = 0 To DRdata.Length - 1
                            If exid = "" Then
                                exid = DRdata(j).Item("_tansnum")
                            Else
                                exid = exid & "," & DRdata(j).Item("_tansnum")
                            End If
                            Operation = DRdata(j).Item("_oper_num")
                            ItemName = DRdata(j).Item("_item")
                            If WC = "" Then
                                Dim DRWC() As DataRow = DSWC.Tables(0).Select("_wc='" & DRdata(j).Item("_wc") & "'")
                                If DRWC.Length > 0 Then
                                    WC = DRWC(0).Item("_wc")
                                    WCDes = DRWC(0).Item("_description")
                                End If

                            End If
                            If Jdate = 0 Then
                                Jdate = DRdata(j).Item("_start_Date")
                            End If
                            If StartTime <> 0 Then
                                If StartTime > DRdata(j).Item("_start_Time") Then
                                    StartTime = DRdata(j).Item("_start_time")
                                End If
                            Else
                                StartTime = DRdata(j).Item("_start_time")
                            End If
                            If EndTime < DRdata(j).Item("_end_time") Then
                                EndTime = DRdata(j).Item("_end_time")
                            End If
                            COQ = COQ + DRdata(j).Item("_qty_complete")
                            RJQ = RJQ + DRdata(j).Item("_qty_scrapped")
                            If NextOp = 0 Then
                                If x = DSRoute.Tables(0).Rows.Count - 1 Then
                                Else
                                    NextOp = DSRoute.Tables(0).Rows(x + 1).Item("_operationNo")
                                End If
                            End If
                            Dim DRRJ() As DataRow = DSReject.Tables(0).Select("_refID=" & DRdata(j).Item("_tansnum"))
                            If DRRJ.Length > 0 Then
                                Dim intI As Integer
                                For intI = 0 To DRRJ.Length - 1
                                    If remarks1 = "" Then
                                        If DRRJ(intI).Item("_RejectedCode") <> "" Then
                                            remarks1 = DRRJ(intI).Item("_RejectedDesc")
                                        End If
                                    ElseIf remarks2 = "" Then
                                        If DRRJ(intI).Item("_RejectedCode") <> "" Then
                                            remarks2 = DRRJ(intI).Item("_RejectedDesc")
                                        End If
                                    ElseIf remarks3 = "" Then
                                        If DRRJ(intI).Item("_rejectedDesc1") <> "" Then
                                            remarks3 = DRRJ(intI).Item("_RejectedDesc")
                                        End If
                                    End If
                                Next
                            End If
                            exst = True
                        Next
                    End If



                    If exst = True Then
                        With Excel
                            xl = xl + 1
                            .cells(xl, 1).value = ""
                            .cells(xl, 2).value = "Run"
                            .cells(xl, 3).value = Format(DateTime.FromOADate(Jdate), "yyyy-MM-dd")
                            .cells(xl, 4).value = Job
                            .cells(xl, 5).value = JobSuffix
                            .cells(xl, 6).value = Operation
                            .cells(xl, 7).value = ItemName
                            .cells(xl, 8).value = ""
                            .cells(xl, 9).value = WC

                            .cells(xl, 10).value = WCDes



                            .cells(xl, 11).value = ""

                            .cells(xl, 12).value = ""
                            .cells(xl, 13).value = ""

                            .cells(xl, 14).value = "1"

                            .cells(xl, 15).value = "Regular"
                            .cells(xl, 16).value = 0

                            .cells(xl, 17).value = 0

                            .cells(xl, 18).value = 0

                            .cells(xl, 19).value = ""

                            .cells(xl, 20).value = COQ


                            .cells(xl, 21).value = RJQ

                            .cells(xl, 22).value = COQ

                            .cells(xl, 23).value = ""
                            .cells(xl, 24).value = "MAIN"

                            .cells(xl, 25).value = "PF"

                            .cells(xl, 26).value = ""

                            .cells(xl, 27).value = NextOp

                            .cells(xl, 28).value = ""

                            .cells(xl, 29).value = ""

                            .cells(xl, 30).value = "FS"

                            .cells(xl, 31).value = ""

                            .cells(xl, 32).value = "LOT"

                            .cells(xl, 33).value = StartTime
                            .cells(xl, 34).value = EndTime

                            .cells(xl, 35).value = 0


                            .cells(xl, 36).value = ""

                            .cells(xl, 37).value = ""

                            .cells(xl, 38).value = "1"

                            .cells(xl, 39).value = ""

                            .cells(xl, 40).value = ""

                            .cells(xl, 41).value = remarks1

                            .cells(xl, 42).value = remarks2

                            .cells(xl, 43).value = remarks3
                        End With
                        Application.DoEvents()
                    End If
                Next

            End If


            filename = txtFile.Text & ".xls"
            Excel.ActiveCell.Worksheet.SaveAs(filename)

            System.Runtime.InteropServices.Marshal.ReleaseComObject(Excel)
            Excel = Nothing
            MsgBox("Data's are exported to Excel Succesfully in '" & filename & "'", MsgBoxStyle.Information, "eWIP")

            P1.Visible = False
            txtFile.Text = ""
            ckbAll.Checked = False








            If exid <> "" Then


                Try
                    With com
                        .Connection = cn
                        .CommandType = CommandType.Text
                        .CommandText = "Update tbJobTrans set _export='Y' where _tansnum in(" & exid & ")"
                        cn.Open()
                        .ExecuteNonQuery()
                        cn.Close()
                    End With

                Catch ex As Exception
                Finally
                    cn.Close()
                    com.Parameters.Clear()
                End Try
            End If

            '                End With

            '    End If
            '            Application.DoEvents()
            '        Next
            '    End If
            '    at = at + 1
            '    lblRunning.Text = at
            '    If PB1.Value + 1 <= PB1.Maximum Then
            '        PB1.Value = at
            '    End If

            '    Application.DoEvents()

            '        Next




            '        filename = txtFile.Text & ".xls"
            '        Excel.ActiveCell.Worksheet.SaveAs(filename)

            '        System.Runtime.InteropServices.Marshal.ReleaseComObject(Excel)
            '        Excel = Nothing
            '        MsgBox("Data's are exported to Excel Succesfully in '" & filename & "'", MsgBoxStyle.Information, "eWIP")

            '        P1.Visible = False
            '        txtFile.Text = ""
            '        ckbAll.Checked = False

            '        Dim e1 As System.Object
            '        Dim e2 As System.EventArgs
            '        butSearch_Click(e1, e2)




            '    End If












            'Else
            '    MsgBox("Please select job no. in the list!", MsgBoxStyle.Information, "eWIP")
            '    System.Runtime.InteropServices.Marshal.ReleaseComObject(Excel)
            '    Excel = Nothing
            '    Exit Sub
            'End If








        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try

        ' The excel is created and opened for insert value. We most close this excel using this system
        Dim pro() As Process = System.Diagnostics.Process.GetProcessesByName("EXCEL")
      
        For Each i As Process In pro
            i.Kill()
        Next

        Dim e1 As System.Object
        Dim e2 As System.EventArgs

        butSearch_Click(e1, e2)
    End Sub

    Private Sub butBrows_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBrows.Click
        txtFile.Text = ""
        sfdSave.FileName = ""
        sfdSave.Filter = "Excel File|*.xls"
        sfdSave.Title = "Save an Excel File"
        If sfdSave.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtFile.Text = sfdSave.FileName
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        lblTotal.Text = 0
        lblRunning.Text = 0
        P1.Visible = False
        txtFile.Text = ""
        sfdSave.FileName = ""
        ckbAll.Checked = False
        lstv.Items.Clear()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        lblTotal.Text = 0
        lblRunning.Text = 0
        P1.Visible = False
        txtFile.Text = ""
        sfdSave.FileName = ""
        ckbAll.Checked = False
        lstv.Items.Clear()
    End Sub

   
    Private Sub ckbAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbAll.CheckedChanged
        If ckbAll.Checked = True Then
            Dim i As Integer

            For i = 0 To lstv.Items.Count - 1
                'If lstv.Items(i).SubItems(12).Text = "1" Then
                lstv.Items(i).Checked = True
                ' Else

                '  End If
            Next
        Else
            Dim i As Integer
            For i = 0 To lstv.Items.Count - 1
                lstv.Items(i).Checked = False
            Next
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Button1_Click(sender, e)
    End Sub
    Function CheckDataSt(ByVal ACDR() As DataRow, ByVal DS As DataSet, ByVal JN As String, ByVal opno As Integer, ByVal sufP As String, ByVal sufC As String, ByVal RelQty As Double, ByVal COMQty As Double) As Boolean
        CheckDataSt = False
        Dim RunsufP As String = ""
        Dim RunsufC As String = ""
        Dim RunRelQty As Double = 0
        Dim RunCOMQty As String = 0
        Dim i As Integer

        If ACDR.Length > 0 Then
            For i = 0 To ACDR.Length - 1
                If opno > ACDR(i).Item("_operationNo") Then
                    Dim REAc() As DataRow = DS.Tables(0).Select("_job='" & JN & "' and _oper_num=" & ACDR(i).Item("_operationNo"), "_oper_num desc")
                    Dim j As Integer
                    If REAc.Length > 0 Then
                        For j = 0 To REAc.Length - 1
                            If ACDR(i).Item("_operationNo") = REAc(j).Item("_oper_num") Then
                                If REAc(j).Item("_jobsuffixParent") = sufP Or REAc(j).Item("_jobsuffix") = sufP Then
                                    If RelQty = REAc(j).Item("_qty_complete") Then
                                        opno = ACDR(i).Item("_operationNo")
                                        RelQty = REAc(j).Item("_qty_Rele_qty")
                                        sufP = REAc(j).Item("_jobsuffixParent")
                                        j = REAc.Length
                                        CheckDataSt = True
                                    End If
                                End If
                            End If
                        Next
                    Else
                        CheckDataSt = False
                        Exit Function
                    End If
                End If
                'Dim DRAC() As DataRow = ds.Tables(0).Select("_job='" & .Item("_Job") & "' and _oper_num<" & MaxOpno & " and (_jobsuffixParent='" & .Item("_jobsuffixParent") & "' or _jobsuffix= '" & .Item("_jobsuffixParent") & "')")

            Next
        End If
        Return CheckDataSt
    End Function
    Function ExportDataAll() As DataSet
        Dim dsAll As New DSExportData
        Dim k As Integer
        Dim exid As String = ""
        Dim joblstno As String = ""
        If lstv.CheckedItems.Count > 0 Then

            For k = 0 To lstv.CheckedItems.Count - 1
                ' If Trim(lstv.CheckedItems(k).SubItems(12).Text) = "1" Then
                If joblstno = "" Then
                    joblstno = "" & Trim(lstv.CheckedItems(k).SubItems(1).Text) & ""
                Else
                    joblstno = joblstno & "," & Trim(lstv.CheckedItems(k).SubItems(1).Text) & ""
                End If
                'End If
            Next
        End If
     

        Dim SQLWC As String
        Dim ADWC As New SqlDataAdapter
        Dim DSWC As New DataSet
        With com
            SQLWC = "select _wc,_description from tbWC"
            .Connection = cn
            .CommandType = CommandType.Text
            .CommandText = SQLWC
            ADWC.SelectCommand = com
            ADWC.Fill(DSWC, "tbRoute")
            .Parameters.Clear()
        End With



        Dim SQLRoute As String
        Dim ADRoute As New SqlDataAdapter
        Dim DSRoute As New DataSet
        With com
            SQLRoute = "select * from tbJobroute where _job In(select distinct(_job) from tbJobTrans where _tansnum in(" & joblstno & "))  order by _job, _operationNo"
            .Connection = cn
            .CommandType = CommandType.Text
            .CommandText = SQLRoute
            ADRoute.SelectCommand = com
            ADRoute.Fill(DSRoute, "tbRoute")
            .Parameters.Clear()
        End With

        Dim SQLReject As String
        Dim ADReject As New SqlDataAdapter
        Dim DSReject As New DataSet
        With com
            SQLReject = "select * from tbRejectedTrans where _refID In(" & joblstno & ")"
            .Connection = cn
            .CommandType = CommandType.Text
            .CommandText = SQLReject
            ADReject.SelectCommand = com
            ADReject.Fill(DSReject, "tbRejected")

        End With


        Dim SQLTrans As String
        Dim ADTrans As New SqlDataAdapter
        Dim DSTrans As New DataSet
        With com
            SQLTrans = "select * from tbJobTrans where _tansnum In(" & joblstno & ") order by _job, _oper_num,_start_Date"
            .Connection = cn
            .CommandType = CommandType.Text
            .CommandText = SQLTrans
            ADTrans.SelectCommand = com
            ADTrans.Fill(DSTrans, "tbERP")
            .Parameters.Clear()
        End With
        Dim i As Integer

        If DSTrans.Tables(0).Rows.Count > 0 Then
            For i = 0 To DSTrans.Tables(0).Rows.Count - 1
                With DSTrans.Tables(0).Rows(i)
                    If .Item("_Rework") <> 0 Then








                    End If
                End With
            Next

        End If

    End Function
    Function GetOldData(ByRef ds As DataSet, ByRef DSRoute As DataSet, ByVal jobno As String, ByVal jobsuf As String) As Boolean
        Dim dsold As New DataSet
        Dim SQLTrans As String
        Dim ADTrans As New SqlDataAdapter
        Dim DSTrans As New DataSet
        With com
            SQLTrans = "select * from tbJobTransHis where _job='" & jobno & "' and _jobsuffix='" & jobsuf & "' _export='N'"
            .Connection = cn
            .CommandType = CommandType.Text
            .CommandText = SQLTrans
            ADTrans.SelectCommand = com
            ADTrans.Fill(DSTrans, "tbJobhis")
            If DSTrans.Tables(0).Rows.Count > 0 Then




            End If
        End With


    End Function


    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub

    
End Class