Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.DateAndTime
Public Class frmNewExport
    Dim parm As SqlParameter
    Dim DSSer As New DataSet
    Dim DSCur As New DataSet
    Private Sub butExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExport.Click

        If txtFile.Text = "" Then
            MsgBox("Please enter the file name!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        lblDataV.Text = "Data Verify to Export.."
        Dim DSSeverJob As New DataSet
        Dim DSSeverJobRoute As New DataSet
        Dim DSSeverJobTrans As New DataSet
        Dim SD As Date = DateSerial(Year(dtpFrom.Value), Month(dtpFrom.Value), Day(dtpFrom.Value))
        Dim ED As Date = DateSerial(Year(dtpTo.Value), Month(dtpTo.Value), Day(dtpTo.Value))
        Dim DSJobTrans As New DataSet
        Dim DSJobTransHis As New DataSet
        '//Modify Data

        'Dim strNewtes As String = ""
        'Dim DSNewTrans As New DataSet

        'With com
        '    Dim DA As New SqlDataAdapter
        '    .Connection = cn
        '    .CommandType = CommandType.Text
        '    .CommandText = "select distinct _job from tbJobtransMain where _start_date between @Sd and @ED"
        '    parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
        '    parm.Value = SD
        '    parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
        '    parm.Value = ED
        '    DA.SelectCommand = com
        '    DA.Fill(DSNewTrans, "tbJob")
        '    .Parameters.Clear()
        'End With
        'Try


        '    If DSNewTrans.Tables(0).Rows.Count > 0 Then
        '        strNewtes = ""
        '        Dim f As Integer
        '        For f = 0 To DSNewTrans.Tables(0).Rows.Count - 1
        '            If strNewtes = "" Then
        '                strNewtes = "'" & DSNewTrans.Tables(0).Rows(f).Item("_job") & "'"
        '            Else
        '                strNewtes = strNewtes & ",'" & DSNewTrans.Tables(0).Rows(f).Item("_job") & "'"
        '            End If


        '        Next
        '    End If
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        '    Exit Sub
        'End Try





        'With com
        '    Dim DA As New SqlDataAdapter
        '    .Connection = cnser
        '    .CommandType = CommandType.Text
        '    Dim strQ As String = "select * from job where  Job in(" & strNewtes & ")"
        '    .CommandText = "select * from job where  Job in(" & strNewtes & ")"
        '    parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
        '    parm.Value = SD
        '    parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
        '    parm.Value = ED
        '    DA.SelectCommand = com
        '    DA.Fill(DSSeverJob, "tbJob")
        '    .Parameters.Clear()
        'End With
        'With com
        '    Dim DA As New SqlDataAdapter
        '    .Connection = cnser
        '    .CommandType = CommandType.Text
        '    .CommandText = "select * from jobroute where Job in (" & strNewtes & ")" 'select job from job where Job_date between @Sd and @ED)"
        '    parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
        '    parm.Value = SD
        '    parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
        '    parm.Value = ED
        '    DA.SelectCommand = com
        '    DA.Fill(DSSeverJobRoute, "tbJob")
        '    .Parameters.Clear()
        'End With
        'With com
        '    Dim DA As New SqlDataAdapter
        '    .Connection = cnser
        '    .CommandType = CommandType.Text
        '    .CommandText = "select * from jobtran where Job in (" & strNewtes & ")" 'select job from job where Job_date between @Sd and @ED)"
        '    parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
        '    parm.Value = SD
        '    parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
        '    parm.Value = ED
        '    DA.SelectCommand = com
        '    DA.Fill(DSSeverJobTrans, "tbJob")
        '    .Parameters.Clear()
        'End With
        'Application.DoEvents()

        'With com
        '    Dim DA As New SqlDataAdapter
        '    .Connection = cn
        '    .CommandType = CommandType.Text
        '    .CommandText = "select * from tbjobtransMain where _Job in (" & strNewtes & ")" 'select _job from tbjob where _Jobdate between @Sd and @ED)"
        '    parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
        '    parm.Value = SD
        '    parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
        '    parm.Value = ED
        '    DA.SelectCommand = com
        '    DA.Fill(DSJobTrans, "tbJobTrans")
        '    .Parameters.Clear()
        'End With

        'Application.DoEvents()
























        '///










        With com
            Dim DA As New SqlDataAdapter
            .Connection = cnser
            .CommandType = CommandType.Text
            .CommandText = "select * from job_mst where job_date between @Sd and @ED"
            parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
            parm.Value = SD
            parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
            parm.Value = ED
            DA.SelectCommand = com
            DA.Fill(DSSeverJob, "job_mst")
            .Parameters.Clear()
        End With
        With com
            Dim DA As New SqlDataAdapter
            .Connection = cnser
            .CommandType = CommandType.Text
            .CommandText = "select * from jobroute_mst where job in (select job from job_mst where job_date between @Sd and @ED)"
            parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
            parm.Value = SD
            parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
            parm.Value = ED
            DA.SelectCommand = com
            DA.Fill(DSSeverJobRoute, "jobroute_mst")
            .Parameters.Clear()
        End With
        With com
            Dim DA As New SqlDataAdapter
            .Connection = cnser
            .CommandType = CommandType.Text
            .CommandText = "select * from jobtran_mst where job in (select job from job_mst where job_date between @Sd and @ED)"
            parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
            parm.Value = SD
            parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
            parm.Value = ED
            DA.SelectCommand = com
            DA.Fill(DSSeverJobTrans, "jobtran_mst")
            .Parameters.Clear()
        End With
        Application.DoEvents()

        With com
            Dim DA As New SqlDataAdapter
            .Connection = cn
            .CommandType = CommandType.Text
            .CommandText = "select * from tbJobTransMain where _job in (select _job from tbJob where _jobDate between @Sd and @ED) and _wc not in('STORER', 'MIX')"
            parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
            parm.Value = SD
            parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
            parm.Value = ED
            DA.SelectCommand = com
            DA.Fill(DSJobTrans, "tbJobTrans")
            .Parameters.Clear()
        End With
        Application.DoEvents()

        'With com
        '    Dim DA As New SqlDataAdapter
        '    .Connection = cn
        '    .CommandType = CommandType.Text
        '    .CommandText = "select * from tbJobTransHis where _job in (select _job from tbJob where _jobDate between @Sd and @ED)"
        '    parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
        '    parm.Value = SD
        '    parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
        '    parm.Value = ED
        '    DA.SelectCommand = com
        '    DA.Fill(DSJobTransHis, "tbJobTransHis")
        '    .Parameters.Clear()
        'End With
        'Application.DoEvents()

        Dim i As Integer
        Dim dsExport As New DSExportData
        Dim drExpRow As DataRow
        Dim m As Integer
        PB1.Value = 0
        PB1.Minimum = 0
        If DSSeverJob.Tables(0).Rows.Count > 0 Then
            PB1.Maximum = DSSeverJob.Tables(0).Rows.Count
            lblTotal.Text = DSSeverJob.Tables(0).Rows.Count
            For i = 0 To DSSeverJob.Tables(0).Rows.Count - 1 '// Server Job For Loop -Start

                If DSSeverJob.Tables(0).Rows(i).Item("stat") = "R" Then



                    Dim RWSerOP As DataRow() = DSSeverJobRoute.Tables(0).Select("job='" & DSSeverJob.Tables(0).Rows(i).Item("job") & "'", "oper_num")
                    Dim j As Integer
                    If RWSerOP.Length > 0 Then
                        For j = 0 To RWSerOP.Length - 1 '// Server JobRoute For Loop -Start
                            With RWSerOP(j)
                                Dim RWTransMain() As DataRow = DSJobTrans.Tables(0).Select("_job='" & DSSeverJob.Tables(0).Rows(i).Item("job") & "' and _oper_num=" & RWSerOP(j).Item("oper_num"))
                                If RWTransMain.Length > 0 Then
                                    Application.DoEvents()

                                    Dim ComSer As Double = 0
                                    Dim RejSer As Double = 0

                                    ComSer = ComSer + RWSerOP(j).Item("qty_complete")
                                    RejSer = RejSer + RWSerOP(j).Item("qty_scrapped")
                                    Dim ComCli As Double = 0
                                    Dim RejCli As Double = 0

                                    ComCli = RWTransMain(0).Item("_qty_complete")
                                    RejCli = RWTransMain(0).Item("_qty_scrapped")

                                    Dim FinCom As Double = 0
                                    Dim FinRej As Double = 0
                                    If (ComCli <> ComSer) Or (RejCli <> RejSer) Then
                                        Dim stUs As String = "N"
                                        If ComSer > ComCli Then
                                            FinCom = ComCli - ComSer
                                            stUs = "D"
                                        ElseIf ComSer < ComCli Then
                                            FinCom = ComCli - ComSer
                                        Else
                                            FinCom = 0
                                        End If


                                        If RejSer > RejCli Then
                                            FinRej = RejCli - RejSer
                                            stUs = "D"
                                        ElseIf RejSer < RejCli Then
                                            FinRej = RejCli - RejSer
                                        Else
                                            FinRej = 0
                                        End If

                                        If ckbP.Checked = True Then
                                            If FinCom < 0 Or FinRej < 0 Then
                                                drExpRow = dsExport.Tables("tbExport").NewRow
                                                m = m + 1
                                                drExpRow("idno") = m
                                                drExpRow("Job") = RWTransMain(0).Item("_job")
                                                drExpRow("JobSuffix") = "0"
                                                drExpRow("JobParent") = "0"
                                                drExpRow("Jobdate") = RWTransMain(0).Item("_jobDate")
                                                drExpRow("Operation") = RWTransMain(0).Item("_oper_num")
                                                drExpRow("ItemName") = RWTransMain(0).Item("_item")
                                                drExpRow("WC") = RWTransMain(0).Item("_wc")
                                                drExpRow("WCDes") = RWTransMain(0).Item("_wc")
                                                drExpRow("Received") = RWTransMain(0).Item("_qty_op_qty")
                                                drExpRow("Completed") = FinCom
                                                drExpRow("Rejected") = FinRej
                                                drExpRow("NextOp") = RWTransMain(0).Item("_nextoper_num")
                                                drExpRow("StartTime") = RWTransMain(0).Item("_start_Date")
                                                drExpRow("EndTime") = RWTransMain(0).Item("_end_Date")
                                                drExpRow("remarks1") = stUs
                                                drExpRow("remarks2") = ""
                                                drExpRow("remarks3") = ""
                                                drExpRow("TransDate") = RWTransMain(0).Item("_end_Date")
                                                dsExport.Tables("tbExport").Rows.Add(drExpRow)
                                            End If
                                        Else
                                            If FinCom >= 0 And FinRej >= 0 Then
                                                drExpRow = dsExport.Tables("tbExport").NewRow
                                                m = m + 1
                                                drExpRow("idno") = m
                                                drExpRow("Job") = RWTransMain(0).Item("_job")
                                                drExpRow("JobSuffix") = "0"
                                                drExpRow("JobParent") = "0"
                                                drExpRow("Jobdate") = RWTransMain(0).Item("_jobDate")
                                                drExpRow("Operation") = RWTransMain(0).Item("_oper_num")
                                                drExpRow("ItemName") = RWTransMain(0).Item("_item")
                                                drExpRow("WC") = RWTransMain(0).Item("_wc")
                                                drExpRow("WCDes") = RWTransMain(0).Item("_wc")
                                                drExpRow("Received") = RWTransMain(0).Item("_qty_op_qty")
                                                drExpRow("Completed") = FinCom
                                                drExpRow("Rejected") = FinRej
                                                drExpRow("NextOp") = RWTransMain(0).Item("_nextoper_num")
                                                drExpRow("StartTime") = RWTransMain(0).Item("_start_Date")
                                                drExpRow("EndTime") = RWTransMain(0).Item("_end_Date")
                                                drExpRow("remarks1") = stUs
                                                drExpRow("remarks2") = ""
                                                drExpRow("remarks3") = ""
                                                drExpRow("TransDate") = RWTransMain(0).Item("_end_Date")
                                                dsExport.Tables("tbExport").Rows.Add(drExpRow)
                                            End If
                                        End If

                                    End If
                                End If
                            End With

                        Next
                    End If
                    PB1.Value = i + 1
                    lblRunning.Text = i + 1
                End If
            Next



            Dim dsExtable As New DSExportData
            If dsExport.Tables("tbExport").Rows.Count > 0 Then
                i = 0
                Dim HasAc As New Hashtable

                Dim drExpTb As DataRow
                For i = 0 To dsExport.Tables("tbExport").Rows.Count - 1

                    With dsExport.Tables("tbExport").Rows(i)
                        If Not HasAc.ContainsKey(.Item("idno")) = True Then
                            Dim drAC As DataRow() = dsExport.Tables("tbExport").Select("Job='" & .Item("Job") & "' and JobSuffix='" & .Item("JobSuffix") & "' and JobParent='" & .Item("JobParent") & "' and Operation='" & .Item("Operation") & "'")
                            If drAC.Length > 0 Then
                                drExpTb = dsExtable.Tables("tbExportOrg").NewRow
                                drExpTb("idno") = i
                                drExpTb("Job") = .Item("Job")
                                drExpTb("JobSuffix") = "-"
                                drExpTb("JobDate") = .Item("JobDate")
                                drExpTb("Operation") = .Item("Operation")
                                drExpTb("WC") = .Item("WC")
                                drExpTb("ItemName") = .Item("ItemName")
                                drExpTb("WCDes") = .Item("WCDes")
                                drExpTb("TransDate") = .Item("TransDate")
                                Dim j As Integer
                                Dim intRecQty As Double = 0
                                Dim intComQty As Double = 0
                                Dim intRejQty As Double = 0
                                Dim strRm1 As String = ""
                                Dim strRm2 As String = ""
                                Dim strRm3 As String = ""
                                Dim dblSt As Double = 0
                                Dim dblEd As Double = 0
                                Dim ddlTrans As Double = 0
                                ' HasAc.Add(.Item("IDno"), .Item("IDno"))
                                For j = 0 To drAC.Length - 1
                                    intRecQty = intRecQty + drAC(j).Item("Received")
                                    intComQty = intComQty + drAC(j).Item("Completed")
                                    intRejQty = intRejQty + drAC(j).Item("Rejected")
                                    If Trim(strRm1) = "" Then
                                        strRm1 = drAC(j).Item("remarks1")
                                    End If
                                    If Trim(strRm2) = "" Then
                                        strRm1 = drAC(j).Item("remarks2")
                                    End If
                                    If Trim(strRm2) = "" Then
                                        strRm1 = drAC(j).Item("remarks3")
                                    End If
                                    If (dblSt > CDbl(drAC(j).Item("StartTime"))) Or dblSt = 0 Then
                                        dblSt = CDbl(drAC(j).Item("StartTime"))
                                    End If

                                    If dblEd < CDbl(drAC(j).Item("EndTime")) Then
                                        dblEd = CDbl(drAC(j).Item("EndTime"))
                                    End If



                                    HasAc.Add(drAC(j).Item("IDno"), drAC(j).Item("IDno"))


                                Next
                                drExpTb("Received") = intRecQty
                                drExpTb("Completed") = intComQty
                                drExpTb("Rejected") = intRejQty
                                drExpTb("NextOp") = .Item("JobSuffix")
                                drExpTb("StartTime") = dblSt
                                drExpTb("EndTime") = dblEd
                                drExpTb("remarks1") = strRm1
                                drExpTb("remarks2") = strRm2
                                drExpTb("remarks3") = strRm3
                                dsExtable.Tables("tbExportOrg").Rows.Add(drExpTb)
                            End If
                        End If
                    End With
                Next
            End If
            Dim HsCur As New Hashtable
            Dim HsHis As New Hashtable

            ExportDatatoExcel(dsExtable, dsExport, HsCur, HsHis)



        End If




    End Sub

    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtFrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False
    End Sub

    Private Sub frmNewExport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim SDate As Date = DateAdd(DateInterval.Year, -1, DateSerial(Year(Now), Month(Now), Day(Now)))
        dtpFrom.Value = SDate
        dtpTo.Value = DateSerial(Year(Now), Month(Now), Day(Now))

      


    End Sub

    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtTo.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        dtpTo.Checked = False
    End Sub

    Function ExportDatatoExcel(ByVal DSEx As DataSet, ByVal dsExport As DataSet, ByVal HsCur As Hashtable, ByVal HsHis As Hashtable) As Boolean
        Dim myTrans As System.Data.SqlClient.SqlTransaction
        lblDataV.Text = "Data Export in Processing.."
        Dim delFile As Boolean = False
        lblRunning.Text = 0
        ' Dim at As Integer
        ' txtFile.Text = "C:\vel123.xls"
        If txtFile.Text = "" Then
            MsgBox("Please enter the file name!", MsgBoxStyle.Information, "eWIP")
            Exit Function
        End If

        Dim str, filename As String
        str = ""
        filename = ""
        Dim col, row As Integer
        col = 0
        row = 0
        Dim strHisNo As String = ""
        Dim strAcrNo As String = ""
        Dim valHis As String
        Dim strHisVal As String = ""
        Dim strCurVal As String = ""
        If HsHis.Count > 0 Then
            For Each valHis In HsHis.Keys
                If strHisVal = "" Then
                    strHisVal = "" & valHis & ""
                Else
                    strHisVal = strHisVal & "," & valHis & ""
                End If
            Next
        End If

        If HsCur.Count > 0 Then
            For Each valHis In HsCur.Keys
                If strCurVal = "" Then
                    strCurVal = "" & valHis & ""
                Else
                    strCurVal = strCurVal & "," & valHis & ""
                End If
            Next
        End If
        Dim strJob As String = ""
        Dim HasNewJo As New Hashtable
        If dsExport.Tables("tbExport").Rows.Count > 0 Then
            Dim k As Integer
            For k = 0 To dsExport.Tables("tbExport").Rows.Count - 1
                With dsExport.Tables("tbExport").Rows(k)
                    If Not HasNewJo.ContainsKey(.Item("job")) = True Then
                        If Trim(strJob) = "" Then
                            strJob = "'" & .Item("JoB") & "'"
                        Else
                            strJob = strJob & ",'" & .Item("JoB") & "'"
                        End If
                        HasNewJo.Add(.Item("job"), .Item("job"))
                    End If
                End With
            Next

        End If
        Dim clsM As New clsMain
        Dim DSJob As New DataSet
        Dim DSJobRoute As New DataSet
        DSJob = clsM.GetDataset("select * from tbJob where _job in(" & strJob & ")", "tbJOB")
        DSJobRoute = clsM.GetDataset("select * from tbJobroute where _job in(" & strJob & ")", "tbJOBRoute")


        Dim localAll As Process()
        localAll = Process.GetProcesses()
        Dim EXhash As New Hashtable
        For i As Integer = 0 To localAll.Length - 1
            If localAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(localAll(i).Id) = False Then
                    EXhash.Add(localAll(i).Id, localAll(i).Id)
                End If
            End If
        Next




        Dim Excel As Object = CreateObject("Excel.Application")
        If Excel Is Nothing Then
            MsgBox("It appears that Excel is not installed on this machine. This operation requires MS Excel to be installed on this machine.", MsgBoxStyle.Critical, "eWIP")
            Exit Function
        End If
        Dim DSWC As New DataSet

        DSWC = clsM.GetDataset("select * from tbWC", "tbWC")
        P1.Visible = True
        Try
            With Excel
                .SheetsInNewWorkbook = 1
                .Workbooks.Add()
                .Worksheets(1).Select()
                Dim i As Integer = 1

                .cells(1, 1).value = "Transaction"
                .cells(1, 1).EntireRow.Font.Bold = True
                .cells(1, 2).value = "Transaction Type"
                .cells(1, 2).EntireRow.Font.Bold = True
                .cells(1, 3).value = "Transaction Date"
                .cells(1, 3).EntireRow.Font.Bold = True

                .cells(1, 4).value = "Job"
                .cells(1, 4).EntireRow.Font.Bold = True
                .cells(1, 5).value = "Job Suffix"
                .cells(1, 5).EntireRow.Font.Bold = True
                .cells(1, 6).value = "Operation"
                .cells(1, 6).EntireRow.Font.Bold = True
                .cells(1, 7).value = "Item"
                .cells(1, 7).EntireRow.Font.Bold = True
                .cells(1, 8).value = "Item Description"
                .cells(1, 8).EntireRow.Font.Bold = True
                .cells(1, 9).value = "WC"
                .cells(1, 9).EntireRow.Font.Bold = True
                .cells(1, 10).value = "WC Description"
                .cells(1, 10).EntireRow.Font.Bold = True


                .cells(1, 11).value = "User Initials"
                .cells(1, 11).EntireRow.Font.Bold = True
                .cells(1, 12).value = "Employee"
                .cells(1, 12).EntireRow.Font.Bold = True
                .cells(1, 13).value = "Employee Name"
                .cells(1, 13).EntireRow.Font.Bold = True
                .cells(1, 14).value = "Shift"
                .cells(1, 14).EntireRow.Font.Bold = True
                .cells(1, 15).value = "Pay Type"
                .cells(1, 15).EntireRow.Font.Bold = True
                .cells(1, 16).value = "Pay Rate"
                .cells(1, 16).EntireRow.Font.Bold = True
                .cells(1, 17).value = "Job Cost Rate"
                .cells(1, 17).EntireRow.Font.Bold = True
                .cells(1, 18).value = "Indirect Code"
                .cells(1, 18).EntireRow.Font.Bold = True
                .cells(1, 19).value = "Description"
                .cells(1, 19).EntireRow.Font.Bold = True
                .cells(1, 20).value = "Completed"
                .cells(1, 20).EntireRow.Font.Bold = True

                .cells(1, 21).value = "Scrapped"
                .cells(1, 21).EntireRow.Font.Bold = True
                .cells(1, 22).value = "Moved"
                .cells(1, 22).EntireRow.Font.Bold = True
                .cells(1, 23).value = "U/M"
                .cells(1, 23).EntireRow.Font.Bold = True
                .cells(1, 24).value = "Warehouse"
                .cells(1, 24).EntireRow.Font.Bold = True
                .cells(1, 25).value = "Reason"
                .cells(1, 25).EntireRow.Font.Bold = True
                .cells(1, 26).value = "Reason Description"
                .cells(1, 26).EntireRow.Font.Bold = True
                .cells(1, 27).value = "Next Operation"
                .cells(1, 27).EntireRow.Font.Bold = True
                .cells(1, 28).value = "Work Center"
                .cells(1, 28).EntireRow.Font.Bold = True
                .cells(1, 29).value = "Work Center Description"
                .cells(1, 29).EntireRow.Font.Bold = True
                .cells(1, 30).value = "Move To Location"
                .cells(1, 30).EntireRow.Font.Bold = True
                .cells(1, 31).value = "Location Description"
                .cells(1, 31).EntireRow.Font.Bold = True
                .cells(1, 32).value = "Lot"
                .cells(1, 32).EntireRow.Font.Bold = True
                .cells(1, 33).value = "Start Time"
                .cells(1, 33).EntireRow.Font.Bold = True
                .cells(1, 34).value = "End Time"
                .cells(1, 34).EntireRow.Font.Bold = True
                .cells(1, 35).value = "Total Hours"
                .cells(1, 35).EntireRow.Font.Bold = True

                .cells(1, 36).value = "Cost Code"
                .cells(1, 36).EntireRow.Font.Bold = True
                .cells(1, 37).value = "Control Point"
                .cells(1, 37).EntireRow.Font.Bold = True
                .cells(1, 38).value = "Oper Complete"
                .cells(1, 38).EntireRow.Font.Bold = True
                .cells(1, 39).value = "Close Job"
                .cells(1, 39).EntireRow.Font.Bold = True
                .cells(1, 40).value = "Issue to Parent"
                .cells(1, 40).EntireRow.Font.Bold = True
                ' .cells(1, 41).value = "Remark 1"
                ' .cells(1, 41).EntireRow.Font.Bold = True
                ' .cells(1, 42).value = "Remark 2"
                ' .cells(1, 42).EntireRow.Font.Bold = True
                ' .cells(1, 43).value = "Remark 3"
                ' .cells(1, 43).EntireRow.Font.Bold = True
                ' .Cells(2, 1).Value = "Test"
                '.Cells(2, 2).Value = "Data"
                i += 1

            End With
            Application.DoEvents()

            Dim xl As Integer = 1
            lblTotal.Text = 0
            lblRunning.Text = 0
            PB1.Value = 0
            PB1.Minimum = 0

            If DSEx.Tables("tbExportOrg").Rows.Count > 0 Then
                Dim j As Integer
                PB1.Maximum = DSEx.Tables("tbExportOrg").Rows.Count
                lblTotal.Text = DSEx.Tables("tbExportOrg").Rows.Count
                For j = 0 To DSEx.Tables("tbExportOrg").Rows.Count - 1

                    With DSEx.Tables("tbExportOrg").Rows(j)
                        xl = xl + 1
                        Dim drJob As DataRow() = DSJob.Tables(0).Select("_job='" & .Item("Job") & "'")
                        Dim dblJob As Double = 0

                        If drJob.Length > 0 Then
                            dblJob = drJob(0).Item("_jobDate")
                        End If


                        Dim DROP() As DataRow = DSJobRoute.Tables(0).Select("_Job='" & .Item("Job") & "' and _operationNo=" & .Item("Operation"))

                        Dim strOpnostatus As String
                        Dim dblLbrhr As Double = 0
                        Dim dbMChHr As Double = 0
                        If DROP.Length > 0 Then
                            dblLbrhr = DROP(0).Item("_runlbrhrs")
                            dbMChHr = DROP(0).Item("_runmchhrs")
                        End If
                        Dim RnRate As Double = 0

                        Dim DRWC As DataRow() = DSWC.Tables(0).Select("_wc='" & .Item("WC") & "'")
                        Dim strRun As String = "Run"
                        Dim strEMP As String = ""
                        If DRWC.Length > 0 Then
                            RnRate = DRWC(0).Item("_Run_Rate")
                            If DRWC(0).Item("_labour") = "L" Then
                                strRun = "Run"

                                strEMP = "PROD"
                            Else
                                strRun = "Machine"
                                strEMP = ""
                            End If
                        End If

                        Dim StrNext As String = ""
                        Dim DROPst() As DataRow = DSJobRoute.Tables(0).Select("_Job='" & .Item("Job") & "' and (_operationNo)>" & .Item("Operation"))
                        Dim l1 As Integer

                        If DROPst.Length > 0 Then
                            Dim dimcntOpno As Integer = 0
                            For l1 = 0 To DROPst.Length - 1
                                If dimcntOpno = 0 Then
                                    dimcntOpno = DROPst(l1).Item("_operationNo")
                                Else
                                    If dimcntOpno > DROPst(l1).Item("_operationNo") Then
                                        dimcntOpno = DROPst(l1).Item("_operationNo")
                                    End If
                                End If
                            Next


                            strOpnostatus = ""
                            StrNext = dimcntOpno
                        Else
                            StrNext = ""
                            'strOpnostatus = "LT"
                            strOpnostatus = "LOT"
                        End If
                        Dim strMoveLot As String = ""
                        If strOpnostatus = "LOT" Then
                            If .Item("ItemName") <> "" Then
                                If CStr(.Item("ItemName")).Length >= 2 Then
                                    Dim strss As String = UCase(Mid(.Item("ItemName"), 1, 2))
                                    If strss = "FG" Or strss = "AC" Then
                                        strMoveLot = "TL"
                                    Else
                                        strMoveLot = "FS"
                                    End If
                                Else
                                End If
                            End If
                        End If

                        'calculate Total Hour
                        Dim dblTotHr As Double
                        If strRun = "Run" Then
                            dblTotHr = .Item("Completed") * dblLbrhr
                        Else
                            dblTotHr = .Item("Completed") * dbMChHr
                        End If


                        Excel.cells(xl, 1).value = ""
                        Excel.cells(xl, 2).value = strRun
                        ' Excel.cells(xl, 3).value = "'" & Format(DateTime.FromOADate(.Item("StartTime")), "dd-MM-yyyy") 'HH:mm") 'Format(DateTime.FromOADate(dblJob), "yyyy-MM-dd")
                        Excel.cells(xl, 3).value = "'" & Format(DateTime.FromOADate(.Item("TransDate")), "dd/MM/yyyy") 'HH:mm") 'Format(DateTime.FromOADate(dblJob), "yyyy-MM-dd")
                        Excel.cells(xl, 4).value = .Item("Job")

                        Excel.cells(xl, 5).value = .Item("JobSuffix")
                        Excel.cells(xl, 6).value = .Item("Operation")
                        Excel.cells(xl, 7).value = .Item("ItemName")
                        Excel.cells(xl, 8).value = ""
                        Excel.cells(xl, 9).value = .Item("WC")
                        Excel.cells(xl, 10).value = .Item("WCDes")
                        Excel.cells(xl, 11).value = ""
                        Excel.cells(xl, 12).value = strEMP
                        Excel.cells(xl, 13).value = ""
                        Excel.cells(xl, 14).value = "1"
                        Excel.cells(xl, 15).value = "Regular"

                        Excel.cells(xl, 16) = CStr(Format(RnRate, "0.00"))

                        'Dim strRnt12 As String = CStr(Format(RnRate, "0.00"))
                        ' Excel.cells(xl, 16) = vbString & strRnt12
                        ' Excel.Colum(xl, 16).Select()
                        ' Excel.Selection.NumberFormat = "0.00"

                        ' Excel.cells(xl, 16).value = CStr(Format(RnRate, "0.00"))

                        Excel.cells(xl, 17).value = CStr(Format(RnRate, "0.00"))
                        Excel.cells(xl, 18).value = 0
                        Excel.cells(xl, 19).value = ""
                        Excel.cells(xl, 20).value = .Item("Completed")
                        Excel.cells(xl, 21).value = .Item("Rejected")
                        Excel.cells(xl, 22).value = .Item("Completed")
                        Excel.cells(xl, 23).value = ""
                        Excel.cells(xl, 24).value = "MAIN"
                        If .Item("Rejected") <> 0 Then
                            Excel.cells(xl, 25).value = "PF"
                        Else
                            Excel.cells(xl, 25).value = ""
                        End If
                        Excel.cells(xl, 26).value = ""
                        Excel.cells(xl, 27).value = StrNext
                        Excel.cells(xl, 28).value = ""
                        Excel.cells(xl, 29).value = ""
                        Excel.cells(xl, 30).value = strMoveLot
                        Excel.cells(xl, 31).value = ""
                        Excel.cells(xl, 32).value = strOpnostatus
                        Excel.cells(xl, 33).EntireColumn.NumberFormat = "Text"
                        Excel.cells(xl, 33).value = "'" & CStr(Format(DateTime.FromOADate(.Item("StartTime")), "HH:mm"))
                        Excel.cells(xl, 34).value = "'" & CStr(Format(DateTime.FromOADate(.Item("EndTime")), "HH:mm"))
                        Excel.cells(xl, 35).value = dblTotHr
                        Excel.cells(xl, 36).value = ""
                        Excel.cells(xl, 37).value = ""
                        Excel.cells(xl, 38).value = "1"
                        Excel.cells(xl, 39).value = ""
                        Excel.cells(xl, 40).value = ""
                        '   Excel.cells(xl, 41).value = .Item("remarks1")
                        ' Excel.cells(xl, 42).value = .Item("remarks2")
                        ' Excel.cells(xl, 43).value = .Item("remarks3")
                    End With
                    Application.DoEvents()
                    PB1.Value = j + 1
                    lblRunning.Text = j + 1
                    Application.DoEvents()
                Next

            End If
            filename = txtFile.Text
            Excel.ActiveCell.Worksheet.SaveAs(filename)
            System.Runtime.InteropServices.Marshal.ReleaseComObject(Excel)
            Excel = Nothing

            cn.Open()
            myTrans = cn.BeginTransaction
            Dim strDate As String = (Format(Now.ToOADate, "#.00000"))
            Dim dblExDate As Double = strDate
            'With com
            '    If Trim(strHisVal) <> "" Then
            '        .Connection = cn
            '        .Transaction = myTrans
            '        .CommandType = CommandType.Text
            '        .CommandText = " Update tbJobTransHis set _export='Y',_exportHis='Y',_ExportDate=" & dblExDate & "  where _tansnum in(" & strHisVal & ")"

            '        .ExecuteNonQuery()
            '        .Parameters.Clear()
            '    End If
            '    If Trim(strCurVal) <> "" Then
            '        .Connection = cn
            '        .Transaction = myTrans
            '        .CommandType = CommandType.Text
            '        .CommandText = " Update tbJobTrans set _export='Y',_ExportDate=" & dblExDate & " where _tansnum in(" & strCurVal & ")"
            '        .ExecuteNonQuery()
            '        .Parameters.Clear()
            '    End If

            '    If Trim(strCurVal) <> "" Or Trim(strHisVal) <> "" Then
            '        .Connection = cn
            '        .Transaction = myTrans
            '        .CommandType = CommandType.Text
            '        .CommandText = " Update tbExportDate set _exportDate=" & dblExDate
            '        .ExecuteNonQuery()
            '        .Parameters.Clear()
            '        txtRedoDate.Text = (Format(dblExDate, "#.00000"))
            '        butRedo.Enabled = True
            '    End If
            '    myTrans.Commit()
            'End With

            MsgBox("Data's are exported to Excel Succesfully in '" & filename & "'", MsgBoxStyle.Information, "eWIP")

            lblDataV.Text = "Data has been Successfully Exported"




        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "eWIP")
            delFile = True
            myTrans.Rollback()

        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try

        ' The excel is created and opened for insert value. We most close this excel using this system
        'Dim pro() As Process = System.Diagnostics.Process.GetProcessesByName("EXCEL")

        'For Each i As Process In pro
        '    i.Kill()
        'Next

        Dim DellocalAll As Process()

        DellocalAll = Process.GetProcesses()


        For i As Integer = 0 To DellocalAll.Length - 1
            If DellocalAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(DellocalAll(i).Id) = False Then
                    DellocalAll(i).Kill()
                End If
            End If
        Next


        If delFile = True Then
            Try
                If IO.File.Exists(filename) Then
                    IO.File.Delete(filename)
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End Try
        End If
        txtFile.Text = ""


    End Function

    Private Sub butBrows_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBrows.Click
        txtFile.Text = ""
        sfdSave.FileName = ""
        sfdSave.Filter = "Excel File|*.xls"
        sfdSave.Title = "Save an Excel File"
        If sfdSave.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtFile.Text = sfdSave.FileName
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        'MsgBox(Format(8989898.9909, "#.00"))
        txtFile.Text = ""
        lblDataV.Text = ""
        PB1.Value = 0
        PB1.Value = 0
        PB1.Minimum = 0
        lblTotal.Text = 0
        lblRunning.Text = 0
    End Sub
End Class