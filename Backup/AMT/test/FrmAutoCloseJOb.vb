Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.DateAndTime
Public Class FrmAutoCloseJOb
    Dim parm As SqlParameter
    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        If Trim(txtAC.Text) = "" Then
            MsgBox("Enter the Job order no.")
            Exit Sub
        End If
        Dim StrjobCom As String = ""
        Dim strJobPen As String = ""
        Dim stJob() As String = Split(txtAC.Text, vbCrLf)
        Dim strcheckJob As String = ""
        Dim DSERPJob As New DataSet
        Dim DSERPJobroute As New DataSet
        Dim DSERPJobTrans As New DataSet


        Dim DSJob As New DataSet
        Dim DSJobroute As New DataSet
        Dim DSJobTrans As New DataSet



        If stJob.Length > 0 Then

            Dim i As Integer
            For i = 0 To stJob.Length - 1
                If strcheckJob = "" Then
                    strcheckJob = "'" & stJob(i) & "'"
                Else
                    strcheckJob = strcheckJob & ",'" & stJob(i) & "'"
                End If
            Next
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cnser
                .CommandType = CommandType.Text
                .CommandText = "SELECT type,job,item,suffix,job_date, qty_released,qty_complete,qty_scrapped ,stat FROM job where job in(" & strcheckJob & ")"
                DA.SelectCommand = com
                DA.Fill(DSERPJob, "tbsch")
                .Parameters.Clear()

                Dim DA1 As New SqlDataAdapter
                .Connection = cnser
                .CommandType = CommandType.Text
                .CommandText = "select * from jobroute  where job in(" & strcheckJob & ")"
                DA1.SelectCommand = com
                DA1.Fill(DSERPJobroute, "tbsch")
                .Parameters.Clear()
                Dim DA2 As New SqlDataAdapter
                .Connection = cnser
                .CommandType = CommandType.Text
                .CommandText = "select * from jobtran  where job in(" & strcheckJob & ")"
                DA2.SelectCommand = com
                DA2.Fill(DSERPJobTrans, "tbsch")
                .Parameters.Clear()






                Dim DAJob As New SqlDataAdapter
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "SELECT * FROM tbjob where _job in(" & strcheckJob & ")"
                DAJob.SelectCommand = com
                DAJob.Fill(DSJob, "tbsch")
                .Parameters.Clear()


                Dim DAroute As New SqlDataAdapter
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "SELECT * FROM tbjobroute where _job in(" & strcheckJob & ")"
                DAroute.SelectCommand = com
                DAroute.Fill(DSJobroute, "tbsch")
                .Parameters.Clear()

                Dim DSReject As New DataSet
                Dim DAReject As New SqlDataAdapter
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select A._WC,B._RejectedCode,B._RejectedDesc from tbRejectedGroup A,tbRejected B where B._RejID=A._RejID"
                DAReject.SelectCommand = com
                DAReject.Fill(DSReject, "tbsch")
                .Parameters.Clear()



                Dim DATrans As New SqlDataAdapter
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "SELECT * FROM tbjobTrans where _job in(" & strcheckJob & ")"
                DATrans.SelectCommand = com
                DATrans.Fill(DSJobTrans, "tbsch")
                .Parameters.Clear()
                If DSERPJob.Tables(0).Rows.Count > 0 Then
                    For i = 0 To DSERPJob.Tables(0).Rows.Count - 1
                        Dim strItem As String = ""
                        Dim dblRel As Double = 0
                        Dim dblOpQty As Double = 0
                        Dim JobDate As Double = 0
                        strItem = DSERPJob.Tables(0).Rows(i).Item("Item")
                        dblRel = DSERPJob.Tables(0).Rows(i).Item("qty_released")
                        dblOpQty = DSERPJob.Tables(0).Rows(i).Item("qty_released")
                        JobDate = CDate(DSERPJob.Tables(0).Rows(i).Item("Job_date")).ToOADate
                        Dim DrJobTransCheck() As DataRow = DSJobTrans.Tables(0).Select("_job='" & DSERPJob.Tables(0).Rows(i).Item("Job") & "' and (_jobSuffix<>'M' or _reworkCount<>0)")
                        If DrJobTransCheck.Length <= 0 Then

                            Dim DRERBRoute As DataRow() = DSERPJobroute.Tables(0).Select("job='" & DSERPJob.Tables(0).Rows(i).Item("Job") & "'", "oper_num")
                            If DRERBRoute.Length > 0 Then
                                Dim j As Integer
                                Dim LastDate As Double = 0
                                Dim CurDate As Double = 0
                                For j = 0 To DRERBRoute.Length - 1

                                    Dim dblERB_rlqty As Double = 0
                                    Dim dblERB_Comty As Double = 0
                                    Dim dblERB_rejqty As Double = 0
                                    Dim dblERB_Date As DateTime

                                    Dim dbl_rlqty As Double = 0
                                    Dim dbl_Comty As Double = 0
                                    Dim dbl_rejqty As Double = 0
                                    Dim RejectID As String = "-"
                                    Dim RejectDesc As String = "-"
                                    Dim strWC As String = ""
                                    Dim NextOp As String = 0
                                    If DRERBRoute.Length > j + 1 Then
                                        NextOp = DRERBRoute(j + 1).Item("oper_num")
                                    End If
                                    Dim DRERPRoute() As DataRow = DSERPJobroute.Tables(0).Select("job='" & DSERPJob.Tables(0).Rows(i).Item("Job") & "' and oper_num=" & DRERBRoute(j).Item("oper_num"))
                                    If DRERPRoute.Length > 0 Then


                                        Dim drJobTtranData() As DataRow = DSJobTrans.Tables(0).Select("_job='" & DSERPJob.Tables(0).Rows(i).Item("Job") & "' and _oper_num=" & DRERBRoute(j).Item("oper_num"))

                                        If drJobTtranData.Length > 0 Then
                                            If LastDate = 0 Then
                                                LastDate = drJobTtranData(0).Item("_start_date")
                                                CurDate = drJobTtranData(0).Item("_start_date")
                                            Else
                                                If drJobTtranData(0).Item("_End_date") <> 0 Then
                                                    LastDate = drJobTtranData(0).Item("_start_date")
                                                    CurDate = drJobTtranData(0).Item("_start_date")
                                                ElseIf drJobTtranData(0).Item("_start_date") <> 0 Then
                                                    LastDate = drJobTtranData(0).Item("_start_date")
                                                    CurDate = drJobTtranData(0).Item("_start_date")
                                                End If
                                            End If
                                        End If

                                        dblERB_rejqty = DRERPRoute(0).Item("qty_scrapped")
                                        dblERB_Date = DRERPRoute(0).Item("RecordDate")
                                        strWC = DRERPRoute(0).Item("WC")
                                        Dim DRReject() As DataRow = DSReject.Tables(0).Select("_WC='" & strWC & "'")
                                        If DRReject.Length > 0 Then
                                            RejectID = DRReject(0).Item("_RejectedCode")
                                            RejectDesc = DRReject(0).Item("_RejectedDesc")
                                        End If


                                        
                                        With com
                                            .Connection = cn
                                            .CommandType = CommandType.StoredProcedure
                                            .CommandText = "Sp_edit_Close_Job_zanden"
                                            parm = .Parameters.Add("@job", SqlDbType.VarChar, 100)
                                            parm.Value = DRERPRoute(0).Item("Job")
                                            parm = .Parameters.Add("@item", SqlDbType.VarChar, 256)
                                            parm.Value = strItem
                                            parm = .Parameters.Add("@WC", SqlDbType.VarChar, 100)
                                            parm.Value = strWC
                                            parm = .Parameters.Add("@opno", SqlDbType.Int)
                                            parm.Value = DRERBRoute(j).Item("oper_num")
                                            parm = .Parameters.Add("@Next", SqlDbType.Int)
                                            parm.Value = NextOp

                                            parm = .Parameters.Add("@jobDate", SqlDbType.Float)
                                            parm.Value = JobDate
                                            parm = .Parameters.Add("@Rlqty", SqlDbType.Float)
                                            parm.Value = dblRel

                                            parm = .Parameters.Add("@Comqty", SqlDbType.Float)
                                            parm.Value = dblOpQty - DRERPRoute(0).Item("qty_scrapped")
                                            parm = .Parameters.Add("@Rjecqty", SqlDbType.Float)
                                            parm.Value = dblERB_rejqty

                                            parm = .Parameters.Add("@StartDate", SqlDbType.Float)
                                            If CDate(DRERPRoute(0).Item("RecordDate")).ToOADate > CurDate Then
                                                parm.Value = CDate(DRERPRoute(0).Item("RecordDate")).ToOADate
                                            Else
                                                parm.Value = CurDate
                                            End If

                                            parm = .Parameters.Add("@starttime", SqlDbType.Int)
                                            parm.Value = Val(Format(DRERPRoute(0).Item("RecordDate"), "HHmm"))

                                            parm = .Parameters.Add("@RejectID", SqlDbType.VarChar, 100)
                                            parm.Value = RejectID
                                            parm = .Parameters.Add("@RejectDesc", SqlDbType.VarChar, 256)
                                            parm.Value = RejectDesc
                                            cn.Open()
                                            .ExecuteNonQuery()
                                            .Parameters.Clear()
                                            cn.Close()
                                            dblOpQty = dblOpQty - dblERB_rejqty
                                        End With

                                    End If


                                    'If DRERPRoute.Length > 0 Then
                                    '    dblERB_Comty = DRERPRoute(0).Item("qty_complete")
                                    '    dblERB_rejqty = DRERPRoute(0).Item("qty_scrapped")
                                    '    dblERB_Date = DRERPRoute(0).Item("RecordDate")
                                    '    strWC = DRERPRoute(0).Item("WC")
                                    '    Dim DRReject() As DataRow = DSReject.Tables(0).Select("_WC='" & strWC & "'")
                                    '    If DRReject.Length > 0 Then
                                    '        RejectID = DRReject(0).Item("_RejectedCode")
                                    '        RejectDesc = DRReject(0).Item("_RejectedDesc")
                                    '    End If
                                    'End If
                                    'Dim drJobTtran() As DataRow = DSJobTrans.Tables(0).Select("_job='" & DSERPJob.Tables(0).Rows(i).Item("Job") & "' and _oper_num=" & DRERBRoute(i).Item("oper_num"))
                                    'If drJobTtran.Length > 0 Then
                                    '    dbl_Comty = drJobTtran(0).Item("_qty_complete")
                                    '    dbl_rejqty = drJobTtran(0).Item("_qty_scrapped")
                                    '    If dblERB_Comty = 0 And dblERB_rejqty = 0 Then
                                    '        'Insert
                                    '    ElseIf dblERB_Comty <> dbl_Comty Or dblERB_rejqty <> dbl_rejqty Then
                                    '        'Update
                                    '    End If
                                    'Else
                                    '    '  insert
                                    'End If
                                Next

                                If StrjobCom = "" Then
                                    StrjobCom = DSERPJob.Tables(0).Rows(i).Item("Job")
                                Else
                                    StrjobCom = StrjobCom & vbCrLf & DSERPJob.Tables(0).Rows(i).Item("Job")
                                End If






                                
                            End If


                        Else
                            If strJobPen = "" Then
                                strJobPen = DSERPJob.Tables(0).Rows(i).Item("Job")
                            Else
                                strJobPen = strJobPen & vbCrLf & DSERPJob.Tables(0).Rows(i).Item("Job")
                            End If



                        End If


                       
                    Next
                End If



                txtAC.Text = strJobPen
                txtClose.Text = StrjobCom





            End With
        End If
        
    End Sub

    Private Sub FrmAutoCloseJOb_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class