<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAutoCloseJOb
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.butClose = New System.Windows.Forms.Button
        Me.txtClose = New System.Windows.Forms.TextBox
        Me.txtAC = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'butClose
        '
        Me.butClose.Location = New System.Drawing.Point(276, 235)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(75, 23)
        Me.butClose.TabIndex = 3
        Me.butClose.Text = "Close Job"
        Me.butClose.UseVisualStyleBackColor = True
        '
        'txtClose
        '
        Me.txtClose.Location = New System.Drawing.Point(399, 40)
        Me.txtClose.Multiline = True
        Me.txtClose.Name = "txtClose"
        Me.txtClose.Size = New System.Drawing.Size(225, 429)
        Me.txtClose.TabIndex = 4
        '
        'txtAC
        '
        Me.txtAC.Location = New System.Drawing.Point(12, 40)
        Me.txtAC.Multiline = True
        Me.txtAC.Name = "txtAC"
        Me.txtAC.Size = New System.Drawing.Size(225, 429)
        Me.txtAC.TabIndex = 2
        '
        'FrmAutoCloseJOb
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(651, 481)
        Me.Controls.Add(Me.txtClose)
        Me.Controls.Add(Me.butClose)
        Me.Controls.Add(Me.txtAC)
        Me.Name = "FrmAutoCloseJOb"
        Me.Text = "FrmAutoCloseJOb"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents butClose As System.Windows.Forms.Button
    Friend WithEvents txtClose As System.Windows.Forms.TextBox
    Friend WithEvents txtAC As System.Windows.Forms.TextBox
End Class
