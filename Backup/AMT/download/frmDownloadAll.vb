Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.DateAndTime
Public Class frmDownloadAll
    Dim parm As SqlParameter
    Dim t1, t2, t3 As String
    Private Sub frmDownload_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Sd As Date
        Sd = DateSerial(Year(Now), Month(Now), 1)
        dtpFrom.Value = Sd
        txtFrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        Sd = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 1, Sd))
        dtpTo.Value = Sd
        txtTo.Text = Format(dtpTo.Value, "dd/MM/yyyy")

        cmbType.SelectedIndex = 2
    End Sub
    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtFrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False
    End Sub
    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtTo.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        dtpTo.Checked = False
    End Sub
    Private Sub cmbType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbType.SelectedIndexChanged

        PB1.Value = 0
        PB1.Minimum = 0
        PB1.Maximum = 0
        lblRunning.Text = 0
        lblTot.Text = 0
        If cmbType.Text = "Work Center" Then
            gb1.Visible = False
            lblJobNo.Visible = False
            txtJobNo.Visible = False
            txtJobNo.Text = ""
        ElseIf cmbType.Text = "Job-Between Date" Then
            gb1.Visible = True
            lblJobNo.Visible = False
            txtJobNo.Visible = False
            txtJobNo.Text = ""
        Else
            gb1.Visible = False
            lblJobNo.Visible = True
            txtJobNo.Visible = True
            txtJobNo.Text = ""

        End If
    End Sub
    Private Sub butDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDownload.Click
        '  If cmbType.Text = "Work Order" Then
        lblRunning.Text = "0"
        GetWorkOrderdata()
        UpdateDownloadLog()
        'Else
        ' GetResourceGroup()
        'End If
    End Sub
    Sub GetWorkOrderdata()

        Dim at As Integer = 0

        lblLogError.Text = ""
        Dim todw As Boolean = False
        Dim DSJob As New DataSet
        Dim DSRoute As New DataSet
        Dim DSJobAC As New DataSet
        Dim DSRouteAC As New DataSet
        Dim DSWS As New DataSet

        Dim DSwc As New DataSet        'By Yeo 20081110
        Dim DSTrans As New DataSet
        Dim tot As Integer
        Dim s() As String = Split(txtFrom.Text, "/")
        Dim SD As Date = DateSerial(s(2), s(1), s(0))
        Dim s1() As String = Split(txtTo.Text, "/")
        Dim ED As Date = DateSerial(s1(2), s1(1), s1(0))

        Try
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cnser
                .CommandType = CommandType.Text

                .CommandText = "SELECT type,job,item,suffix,job_date, qty_released,qty_complete,qty_scrapped ,stat FROM         job where job_date between @Sd and @ED"
                parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
                parm.Value = SD
                parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
                parm.Value = ED

                DA.SelectCommand = com
                DA.Fill(DSJob, "tbsch")
                .Parameters.Clear()
            End With
            tot = tot + DSJob.Tables(0).Rows.Count
            'lblTot.Text = tot
            Application.DoEvents()
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cnser
                .CommandType = CommandType.Text
                'job,suffix,opernum,wc

                .CommandText = "select jobroute.job as job,jobroute.suffix as suffix,jobroute.oper_num as opernum,jobroute.wc  as wc from jobroute  inner join job on jobroute.job=job.job and jobroute.suffix=job.suffix where job.job_date between @Sd and @ED"
                parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
                parm.Value = SD
                parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
                parm.Value = ED

                DA.SelectCommand = com
                DA.Fill(DSRoute, "tbsch")
                .Parameters.Clear()
            End With
            ' tot = tot + DSRoute.Tables(0).Rows.Count
            '    lblTot.Text = tot
            Application.DoEvents()




            With com
                Dim DA As New SqlDataAdapter
                .Connection = cnser
                .CommandType = CommandType.Text
                'job,suffix,opernum,wc

                '_Job,_jobsuffix,_trans_type,_trans_datefrm,_qty_complete,_qty_scrapped,_oper_num,_wc,_start_time,_end_time
                .CommandText = "select JobTran.job,JobTran.suffix,JobTran.trans_type,JobTran.trans_date,JobTran.qty_complete,JobTran.qty_scrapped,JobTran.oper_num,JobTran.wc,JobTran.start_time,JobTran.end_time,JobTran.user_code,JobTran.RecordDate,JobTran.oper_num from JobTran  inner join job on JobTran.job=job.job and JobTran.suffix=job.suffix where job.job_date between @Sd and @ED"
                parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
                parm.Value = SD
                parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
                parm.Value = ED

                DA.SelectCommand = com
                DA.Fill(DSTrans, "tbTans")
                .Parameters.Clear()
            End With
            'tot = tot + DSRoute.Tables(0).Rows.Count
            lblTot.Text = tot
            Application.DoEvents()







            '********************************************************************

        Catch ex As Exception
            lblLogError.Text = (ex.Message)
            Exit Sub
        End Try
        PB1.Value = 0
        PB1.Minimum = 0
        PB1.Maximum = tot
        Try
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cn
                .CommandType = CommandType.Text

                .CommandText = "SELECT * FROM tbjob where _jobDate between @Sd and @ED"
                parm = .Parameters.Add("@Sd", SqlDbType.Float)
                parm.Value = SD.ToOADate
                parm = .Parameters.Add("@Ed", SqlDbType.Float)
                parm.Value = ED.ToOADate

                DA.SelectCommand = com
                DA.Fill(DSJobAC, "tbsch")
                .Parameters.Clear()
            End With
        Catch ex As Exception
            lblLogError.Text = (ex.Message)
            Exit Sub
        End Try

        Try
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cn
                .CommandType = CommandType.Text

                .CommandText = "select tbJobRoute.* from tbJobRoute  inner join tbJob on tbJobRoute._job=tbjob._job and tbJobRoute._jobSuffix=tbjob._jobSuffix where tbjob._jobDate between @Sd  and @ED"

                parm = .Parameters.Add("@Sd", SqlDbType.Float)
                parm.Value = SD.ToOADate
                parm = .Parameters.Add("@Ed", SqlDbType.Float)
                parm.Value = ED.ToOADate

                DA.SelectCommand = com
                DA.Fill(DSRouteAC, "tbsch")
                .Parameters.Clear()
            End With
        Catch ex As Exception
            lblLogError.Text = (ex.Message)

            Exit Sub
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try

        Try
            If DSJob.Tables(0).Rows.Count > 0 Then

                Dim i As Integer
                For i = 0 To DSJob.Tables(0).Rows.Count - 1
                    Dim Stup As Boolean = False
                    With com
                        Try
                            .Connection = cn
                            .CommandType = CommandType.StoredProcedure
                            .CommandText = "sp_tbJob_Download_auto"

                            ' type,job,item,suffix,job_date, qty_released,qty_complete,qty_scrapped
                            parm = .Parameters.Add("@job_" & 1, SqlDbType.VarChar, 50)
                            parm.Value = Trim(DSJob.Tables(0).Rows(i).Item("job"))

                            parm = .Parameters.Add("@jobsuffix_" & 1, SqlDbType.Int)
                            parm.Value = DSJob.Tables(0).Rows(i).Item("suffix")

                            parm = .Parameters.Add("@jobDate_" & 1, SqlDbType.Float)
                            parm.Value = CDate(DSJob.Tables(0).Rows(i).Item("job_date")).ToOADate

                            parm = .Parameters.Add("@jobtype_" & 1, SqlDbType.VarChar, 50)
                            parm.Value = DSJob.Tables(0).Rows(i).Item("type")

                            parm = .Parameters.Add("@item_" & 1, SqlDbType.VarChar, 50)
                            parm.Value = DSJob.Tables(0).Rows(i).Item("item")

                            parm = .Parameters.Add("@qtyReleased_" & 1, SqlDbType.Float)
                            parm.Value = DSJob.Tables(0).Rows(i).Item("qty_released")

                            parm = .Parameters.Add("@qtyCompleted_" & 1, SqlDbType.Float)
                            parm.Value = DSJob.Tables(0).Rows(i).Item("qty_complete")

                            parm = .Parameters.Add("@qtyScrapped_" & 1, SqlDbType.Float)
                            parm.Value = DSJob.Tables(0).Rows(i).Item("qty_scrapped")
                            parm = .Parameters.Add("@stas_" & 1, SqlDbType.Char, 1)
                            parm.Value = DSJob.Tables(0).Rows(i).Item("stat")
                            Dim NR() As DataRow = DSJobAC.Tables(0).Select("_job='" & Trim(DSJob.Tables(0).Rows(i).Item("job")) & "' and _jobsuffix=" & DSJob.Tables(0).Rows(i).Item("suffix"))
                            parm = .Parameters.Add("@status_" & 1, SqlDbType.VarChar, 255)
                            If NR.Length > 0 Then
                                If NR(0).Item("_qtyReleased") = DSJob.Tables(0).Rows(i).Item("qty_released") Then
                                    parm.Value = "No"
                                Else
                                    parm.Value = "Edit"
                                End If
                            Else
                                parm.Value = "New"
                            End If
                            cn.Open()
                            .ExecuteNonQuery()
                            .Parameters.Clear()
                            cn.Close()
                            Stup = True
                            todw = True





                        Catch ex As Exception
                            Stup = False
                            Dim errtype As System.Diagnostics.EventLogEntryType
                            's = EventLogEntryType.Error
                            errtype = EventLogEntryType.Error
                            lblLogError.Text = ("Download error-" & DSJob.Tables(0).Rows(i).Item("job"))
                        Finally
                            cn.Close()
                            .Parameters.Clear()
                        End Try

                    End With
                    If Stup = True Then
                        Dim NEWRoute() As DataRow = DSRoute.Tables(0).Select("job='" & Trim(DSJob.Tables(0).Rows(i).Item("job")) & "'")
                        If NEWRoute.Length > 0 Then
                            Dim j As Integer = 0
                            For j = 0 To NEWRoute.Length - 1
                                With com
                                    .Connection = cn
                                    .CommandType = CommandType.StoredProcedure
                                    .CommandText = "sp_tbjobRoute_Download"
                                    Dim J1 As Integer
                                    For J1 = 0 To 14
                                        If j < NEWRoute.Length Then
                                            parm = .Parameters.Add("@job_" & J1 + 1, SqlDbType.VarChar, 50)
                                            parm.Value = Trim(NEWRoute(j).Item("job"))

                                            parm = .Parameters.Add("@jobsuffix_" & J1 + 1, SqlDbType.Int)
                                            parm.Value = NEWRoute(j).Item("suffix")

                                            parm = .Parameters.Add("@operationNo_" & J1 + 1, SqlDbType.Float)
                                            parm.Value = NEWRoute(j).Item("opernum")

                                            parm = .Parameters.Add("@wc_" & J1 + 1, SqlDbType.VarChar, 50)
                                            parm.Value = NEWRoute(j).Item("wc")
                                            Dim NR() As DataRow = DSRouteAC.Tables(0).Select("_job='" & Trim(NEWRoute(j).Item("job")) & "' and _jobsuffix=" & NEWRoute(j).Item("suffix") & " and  _operationNo=" & NEWRoute(j).Item("opernum"))
                                            parm = .Parameters.Add("@status_" & J1 + 1, SqlDbType.VarChar, 255)
                                            If NR.Length > 0 Then
                                                parm.Value = "Edit"
                                            Else
                                                parm.Value = "New"

                                            End If

                                        Else
                                            parm = .Parameters.Add("@job_" & J1 + 1, SqlDbType.VarChar, 50)
                                            parm.Value = ""
                                            parm = .Parameters.Add("@jobsuffix_" & J1 + 1, SqlDbType.Int)
                                            parm.Value = 0
                                            parm = .Parameters.Add("@operationNo_" & J1 + 1, SqlDbType.Float)
                                            parm.Value = 0
                                            parm = .Parameters.Add("@wc_" & J1 + 1, SqlDbType.VarChar, 50)
                                            parm.Value = ""
                                            parm = .Parameters.Add("@status_" & J1 + 1, SqlDbType.VarChar, 255)
                                            parm.Value = ""

                                        End If
                                        j = j + 1
                                    Next
                                    j = j - 1
                                    cn.Open()
                                    .ExecuteNonQuery()
                                    .Parameters.Clear()
                                    cn.Close()
                                End With
                            Next

                        End If

                    End If
                    Application.DoEvents()


                    '///JobTrans

                    If Stup = True Then

                        With com
                            .Connection = cn
                            .CommandType = CommandType.Text
                            .CommandText = "Delete from tbJobTransMain where _job='" & Trim(DSJob.Tables(0).Rows(i).Item("job")) & "'"
                            cn.Open()
                            .ExecuteNonQuery()
                            .Parameters.Clear()
                            cn.Close()
                        End With
                        com.Parameters.Clear()


                        With com
                            .Connection = cn
                            .CommandType = CommandType.Text
                            .CommandText = "Delete from tbJobTrans where _job='" & Trim(DSJob.Tables(0).Rows(i).Item("job")) & "'"
                            cn.Open()
                            .ExecuteNonQuery()
                            .Parameters.Clear()
                            cn.Close()
                        End With
                        com.Parameters.Clear()

                        ' job,suffix,rans_type,trans_date,qty_complete,qty_scrapped,oper_num,wc,start_time,end_time,user_code
                        Dim NEWTrans() As DataRow = DSTrans.Tables(0).Select("job='" & Trim(DSJob.Tables(0).Rows(i).Item("job")) & "'")
                        If NEWTrans.Length > 0 Then
                            Dim j As Integer = 0
                            Dim newHash As New Hashtable
                            For j = 0 To NEWTrans.Length - 1
                                With com
                                    .Connection = cn
                                    .CommandType = CommandType.StoredProcedure
                                    .CommandText = "sp_tbjobTrans_Download"
                                    Dim J1 As Integer
                                    For J1 = 0 To 14
                                        If j < NEWTrans.Length Then
                                            If newHash.ContainsKey(NEWTrans(j).Item("oper_num")) = False Then
                                                newHash.Add(NEWTrans(j).Item("oper_num"), NEWTrans(j).Item("oper_num"))
                                                parm = .Parameters.Add("@job_" & J1 + 1, SqlDbType.VarChar, 50)
                                                parm.Value = Trim(NEWTrans(j).Item("job"))
                                                parm = .Parameters.Add("@jobsuffix_" & J1 + 1, SqlDbType.VarChar, 10)
                                                parm.Value = "M"
                                                parm = .Parameters.Add("@jobsuffixParent_" & J1 + 1, SqlDbType.VarChar, 10)
                                                parm.Value = "M"

                                                parm = .Parameters.Add("@operationNo_" & J1 + 1, SqlDbType.Int)
                                                parm.Value = NEWTrans(j).Item("oper_num")

                                                parm = .Parameters.Add("@wc_" & J1 + 1, SqlDbType.VarChar, 50)
                                                If IsDBNull(NEWTrans(j).Item("wc")) = True Then
                                                    parm.Value = "0"
                                                Else
                                                    parm.Value = NEWTrans(j).Item("wc")
                                                End If


                                                Dim stTime As Integer = 0
                                                Dim stDate As Double = 0
                                                If IsDBNull(NEWTrans(j).Item("RecordDate")) = False Then
                                                    stDate = CDate(NEWTrans(j).Item("RecordDate")).ToOADate
                                                    stTime = CInt(Format(CDate(NEWTrans(j).Item("RecordDate")), "HHmm"))
                                                Else
                                                    stDate = CDate(Now).ToOADate
                                                    stTime = 0
                                                End If


                                                parm = .Parameters.Add("@start_Date_" & J1 + 1, SqlDbType.Float)
                                                parm.Value = stDate
                                                parm = .Parameters.Add("@start_time_" & J1 + 1, SqlDbType.Int)
                                                parm.Value = stTime
                                                '  parm = .Parameters.Add("@end_Date_" & J1 + 1, SqlDbType.Float)
                                                ' parm.Value = stDate
                                                ' parm = .Parameters.Add("@end_time_" & J1 + 1, SqlDbType.Int)
                                                ' parm.Value = stTime







                                                Dim COM_qty, SCP_qty, OPE_QTY As Double
                                                COM_qty = 0
                                                SCP_qty = 0
                                                OPE_QTY = 0

                                                parm = .Parameters.Add("@qty_Rele_qty_" & J1 + 1, SqlDbType.Float)
                                                parm.Value = DSJob.Tables(0).Rows(i).Item("qty_released")

                                                Dim RWCount() As DataRow = DSTrans.Tables(0).Select("job='" & Trim(DSJob.Tables(0).Rows(i).Item("job")) & "' and oper_num='" & NEWTrans(j).Item("oper_num") & "'")
                                                Dim x1 As Integer
                                                If RWCount.Length > 0 Then

                                                    For x1 = 0 To RWCount.Length - 1
                                                        If IsDBNull(RWCount(x1).Item("qty_complete")) = False Then
                                                            COM_qty = COM_qty + RWCount(x1).Item("qty_complete")
                                                        End If
                                                        If IsDBNull(RWCount(x1).Item("qty_scrapped")) = False Then
                                                            SCP_qty = SCP_qty + RWCount(x1).Item("qty_scrapped")
                                                        End If
                                                    Next
                                                End If
                                                OPE_QTY = COM_qty + SCP_qty

                                                parm = .Parameters.Add("@qty_complete_" & J1 + 1, SqlDbType.Float)
                                                parm.Value = COM_qty
                                                parm = .Parameters.Add("@qty_scrapped_" & J1 + 1, SqlDbType.Float)
                                                parm.Value = SCP_qty
                                                parm = .Parameters.Add("@qty_op_qty_" & J1 + 1, SqlDbType.Float)
                                                parm.Value = OPE_QTY
                                                Dim RWSt() As DataRow = DSTrans.Tables(0).Select("job='" & Trim(DSJob.Tables(0).Rows(i).Item("job")) & "' and oper_num>" & NEWTrans(j).Item("oper_num") & "")
                                                Dim DRALLRoute() As DataRow
                                                ' parm = .Parameters.Add("@status" & J1 + 1, SqlDbType.VarChar)
                                                ' parm.Value = "COMPLETED"

                                                parm = .Parameters.Add("@Nextstatus_" & J1 + 1, SqlDbType.Int)

                                                Dim strstNext As Boolean = False
                                                Dim strstNextOPNO As Integer
                                                If RWSt.Length > 0 Then
                                                    strstNext = False
                                                Else
                                                    DRALLRoute = DSRoute.Tables(0).Select("job='" & Trim(DSJob.Tables(0).Rows(i).Item("job")) & "' and opernum>" & NEWTrans(j).Item("oper_num") & "")
                                                    If DRALLRoute.Length > 0 Then
                                                        strstNextOPNO = DRALLRoute(0).Item("opernum")
                                                        strstNext = True
                                                    End If
                                                End If
                                                If strstNext = True Then
                                                    parm.Value = 1
                                                Else
                                                    parm.Value = 0

                                                End If
                                            Else
                                                parm = .Parameters.Add("@job_" & J1 + 1, SqlDbType.VarChar, 50)
                                                parm.Value = ""
                                                parm = .Parameters.Add("@jobsuffix_" & J1 + 1, SqlDbType.VarChar, 10)
                                                parm.Value = ""
                                                parm = .Parameters.Add("@jobsuffixParent_" & J1 + 1, SqlDbType.VarChar, 10)
                                                parm.Value = ""
                                                parm = .Parameters.Add("@operationNo_" & J1 + 1, SqlDbType.Int)
                                                parm.Value = 0
                                                parm = .Parameters.Add("@wc_" & J1 + 1, SqlDbType.VarChar, 50)
                                                parm.Value = ""
                                                parm = .Parameters.Add("@start_Date_" & J1 + 1, SqlDbType.Float)
                                                parm.Value = 0
                                                parm = .Parameters.Add("@start_time_" & J1 + 1, SqlDbType.Int)
                                                parm.Value = 0
                                                parm = .Parameters.Add("@qty_Rele_qty_" & J1 + 1, SqlDbType.Float)
                                                parm.Value = 0
                                                parm = .Parameters.Add("@qty_complete_" & J1 + 1, SqlDbType.Decimal)
                                                parm.Value = 0
                                                parm = .Parameters.Add("@qty_scrapped_" & J1 + 1, SqlDbType.Decimal)
                                                parm.Value = 0
                                                parm = .Parameters.Add("@qty_op_qty_" & J1 + 1, SqlDbType.Decimal)
                                                parm.Value = 0
                                                ' parm = .Parameters.Add("@status" & J1 + 1, SqlDbType.VarChar)
                                                ' parm.Value = ""
                                                parm = .Parameters.Add("@Nextstatus_" & J1 + 1, SqlDbType.Int)
                                                parm.Value = 0
                                            End If
                                        Else
                                            parm = .Parameters.Add("@job_" & J1 + 1, SqlDbType.VarChar, 50)
                                            parm.Value = ""
                                            parm = .Parameters.Add("@jobsuffix_" & J1 + 1, SqlDbType.VarChar, 10)
                                            parm.Value = ""
                                            parm = .Parameters.Add("@jobsuffixParent_" & J1 + 1, SqlDbType.VarChar, 10)
                                            parm.Value = ""
                                            parm = .Parameters.Add("@operationNo_" & J1 + 1, SqlDbType.Int)
                                            parm.Value = 0
                                            parm = .Parameters.Add("@wc_" & J1 + 1, SqlDbType.VarChar, 50)
                                            parm.Value = ""
                                            parm = .Parameters.Add("@start_Date_" & J1 + 1, SqlDbType.Float)
                                            parm.Value = 0
                                            parm = .Parameters.Add("@start_time_" & J1 + 1, SqlDbType.Int)
                                            parm.Value = 0
                                            parm = .Parameters.Add("@qty_Rele_qty_" & J1 + 1, SqlDbType.Float)
                                            parm.Value = 0
                                            parm = .Parameters.Add("@qty_complete_" & J1 + 1, SqlDbType.Float)
                                            parm.Value = 0
                                            parm = .Parameters.Add("@qty_scrapped_" & J1 + 1, SqlDbType.Float)
                                            parm.Value = 0
                                            parm = .Parameters.Add("@qty_op_qty_" & J1 + 1, SqlDbType.Float)
                                            parm.Value = 0
                                            ' parm = .Parameters.Add("@status" & J1 + 1, SqlDbType.VarChar)
                                            '  parm.Value = ""
                                            parm = .Parameters.Add("@Nextstatus_" & J1 + 1, SqlDbType.Int)
                                            parm.Value = 0
                                        End If
                                        j = j + 1
                                    Next
                                    cn.Open()
                                    .ExecuteNonQuery()
                                    .Parameters.Clear()
                                    cn.Close()
                                End With
                            Next

                        End If

                    End If
                    Application.DoEvents()
                    '///End Job Trans











                    at = at + 1
                    lblRunning.Text = at
                    If PB1.Value + 1 <= PB1.Maximum Then
                        PB1.Value = at
                    End If

                    Application.DoEvents()
















                Next
            End If
        Catch ex As SqlException
            lblLogError.Text = (ex.Message)
            Exit Sub
        Catch ex As Exception

            lblLogError.Text = (ex.Message)
            Exit Sub
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        If todw = True Then
            'lblTime.Text = Format(Now, "yyy-MM-dd HH:mm")
            'Dim errtype As System.Diagnostics.EventLogEntryType
            ''s = EventLogEntryType.Error
            'errtype = EventLogEntryType.Information
            'EventLog2.WriteEntry(passpar & " download(" & LogUserName & ") ", errtype)

        End If
        If lblLogError.Text = "" Then
            lblLogError.Text = "Data has successfully Download!"
        End If





















        '' '' '' '' ''Dim DSType As New DataSet
        ' '' '' '' ''Dim DSJob As New DataSet
        ' '' '' '' ''Dim DSRoute As New DataSet
        ' '' '' '' ''Dim DSTrans As New DataSet
        ' '' '' '' ''Dim DSJobAC As New DataSet
        ' '' '' '' ''Dim DSRouteAC As New DataSet
        ' '' '' '' ''Dim DSWS As New DataSet

        ' '' '' '' ''Dim DSwc As New DataSet        'By Yeo 20081110

        ' '' '' '' ''Dim tot As Integer
        ' '' '' '' ''Dim s() As String = Split(txtFrom.Text, "/")
        ' '' '' '' ''Dim SD As Date = DateSerial(s(2), s(1), s(0))
        ' '' '' '' ''Dim s1() As String = Split(txtTo.Text, "/")
        ' '' '' '' ''Dim ED As Date = DateSerial(s1(2), s1(1), s1(0))
        ' '' '' '' ''lblLoading.Visible = True
        ' '' '' '' ''lblLoading.Visible = False
        ' '' '' '' ''Dim at As Integer
        ' '' '' '' ''PB1.Value = 0
        ' '' '' '' ''PB1.Minimum = 0
        ' '' '' '' ''PB1.Maximum = tot
        ' '' '' '' ''Dim ct As Integer = 0
        ' '' '' '' ''If cmbType.Text <> "Work Center" Then
        ' '' '' '' ''    Try
        ' '' '' '' ''        With com
        ' '' '' '' ''            Dim DA As New SqlDataAdapter
        ' '' '' '' ''            .Connection = cnser
        ' '' '' '' ''            .CommandType = CommandType.Text
        ' '' '' '' ''            If cmbType.Text = "Between Date" Then
        ' '' '' '' ''                .CommandText = "SELECT type,job,item,suffix,job_date, qty_released,qty_complete,qty_scrapped ,stat FROM         job where job_date between @Sd and @ED"
        ' '' '' '' ''                parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
        ' '' '' '' ''                parm.Value = SD
        ' '' '' '' ''                parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
        ' '' '' '' ''                parm.Value = ED
        ' '' '' '' ''            Else
        ' '' '' '' ''                .CommandText = "SELECT type,job,item,suffix,job_date, qty_released,qty_complete,qty_scrapped ,stat FROM         job where job=@job"
        ' '' '' '' ''                parm = .Parameters.Add("@Job", SqlDbType.NVarChar, 20)
        ' '' '' '' ''                parm.Value = fncstr(txtJobNo.Text)
        ' '' '' '' ''            End If
        ' '' '' '' ''            DA.SelectCommand = com
        ' '' '' '' ''            DA.Fill(DSJob, "tbsch")
        ' '' '' '' ''            .Parameters.Clear()
        ' '' '' '' ''        End With
        ' '' '' '' ''        tot = tot + DSJob.Tables(0).Rows.Count
        ' '' '' '' ''        lblTot.Text = tot
        ' '' '' '' ''        Application.DoEvents()
        ' '' '' '' ''        With com
        ' '' '' '' ''            Dim DA As New SqlDataAdapter
        ' '' '' '' ''            .Connection = cnser
        ' '' '' '' ''            .CommandType = CommandType.Text
        ' '' '' '' ''            'job,suffix,opernum,wc
        ' '' '' '' ''            If cmbType.Text = "Between Date" Then
        ' '' '' '' ''                .CommandText = "select jobroute.job as job,jobroute.suffix as suffix,jobroute.oper_num as opernum,jobroute.wc  as wc from jobroute  inner join job on jobroute.job=job.job and jobroute.suffix=job.suffix where job.job_date between @Sd and @ED"
        ' '' '' '' ''                parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
        ' '' '' '' ''                parm.Value = SD
        ' '' '' '' ''                parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
        ' '' '' '' ''                parm.Value = ED
        ' '' '' '' ''            Else
        ' '' '' '' ''                .CommandText = "select jobroute.job as job,jobroute.suffix as suffix,jobroute.oper_num as opernum,jobroute.wc  as wc from jobroute  inner join job on jobroute.job=job.job and jobroute.suffix=job.suffix where job.job=@job"
        ' '' '' '' ''                parm = .Parameters.Add("@Job", SqlDbType.NVarChar, 20)
        ' '' '' '' ''                parm.Value = fncstr(txtJobNo.Text)
        ' '' '' '' ''            End If
        ' '' '' '' ''            DA.SelectCommand = com
        ' '' '' '' ''            DA.Fill(DSRoute, "tbsch")
        ' '' '' '' ''            .Parameters.Clear()
        ' '' '' '' ''        End With
        ' '' '' '' ''        tot = tot + DSRoute.Tables(0).Rows.Count
        ' '' '' '' ''        lblTot.Text = tot
        ' '' '' '' ''        Application.DoEvents()


        ' '' '' '' ''        With com
        ' '' '' '' ''            Dim DA As New SqlDataAdapter
        ' '' '' '' ''            .Connection = cnser
        ' '' '' '' ''            .CommandType = CommandType.Text
        ' '' '' '' ''            'job,suffix,opernum,wc
        ' '' '' '' ''            If cmbType.Text = "Between Date" Then
        ' '' '' '' ''                '_Job,_jobsuffix,_trans_type,_trans_datefrm,_qty_complete,_qty_scrapped,_oper_num,_wc,_start_time,_end_time
        ' '' '' '' ''                .CommandText = "select JobTran.job,JobTran.suffix,JobTran.rans_type,JobTran.trans_date,JobTran.qty_complete,JobTran.qty_scrapped,JobTran.oper_num,JobTran.wc,JobTran.start_time,JobTran.end_time,JobTran.user_code as wc from JobTran  inner join job on JobTran.job=job.job and JobTran.suffix=job.suffix where job.job_date between @Sd and @ED"
        ' '' '' '' ''                parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
        ' '' '' '' ''                parm.Value = SD
        ' '' '' '' ''                parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
        ' '' '' '' ''                parm.Value = ED
        ' '' '' '' ''            Else
        ' '' '' '' ''                .CommandText = "JobTran.job,JobTran.suffix,JobTran.rans_type,JobTran.trans_date,JobTran.qty_complete,JobTran.qty_scrapped,JobTran.oper_num,JobTran.wc,JobTran.start_time,JobTran.end_time,JobTran.user_code as wc from JobTran  inner join job on JobTran.job=job.job and JobTran.suffix=job.suffix  where job.job=@job"
        ' '' '' '' ''                parm = .Parameters.Add("@Job", SqlDbType.NVarChar, 20)
        ' '' '' '' ''                parm.Value = fncstr(txtJobNo.Text)
        ' '' '' '' ''            End If
        ' '' '' '' ''            DA.SelectCommand = com
        ' '' '' '' ''            DA.Fill(DSTrans, "tbTans")
        ' '' '' '' ''            .Parameters.Clear()
        ' '' '' '' ''        End With
        ' '' '' '' ''        tot = tot + DSRoute.Tables(0).Rows.Count
        ' '' '' '' ''        lblTot.Text = tot
        ' '' '' '' ''        Application.DoEvents()











        ' '' '' '' ''        '********************************************************************

        ' '' '' '' ''    Catch ex As Exception
        ' '' '' '' ''        MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        ' '' '' '' ''        lblLoading.Visible = False
        ' '' '' '' ''        Exit Sub
        ' '' '' '' ''    End Try

        ' '' '' '' ''    Try
        ' '' '' '' ''        With com
        ' '' '' '' ''            Dim DA As New SqlDataAdapter
        ' '' '' '' ''            .Connection = cn
        ' '' '' '' ''            .CommandType = CommandType.Text
        ' '' '' '' ''            If cmbType.Text = "Between Date" Then
        ' '' '' '' ''                .CommandText = "SELECT * FROM tbjob where _jobDate between @Sd and @ED"
        ' '' '' '' ''                parm = .Parameters.Add("@Sd", SqlDbType.Float)
        ' '' '' '' ''                parm.Value = SD.ToOADate
        ' '' '' '' ''                parm = .Parameters.Add("@Ed", SqlDbType.Float)
        ' '' '' '' ''                parm.Value = ED.ToOADate
        ' '' '' '' ''            Else
        ' '' '' '' ''                .CommandText = "SELECT * FROM tbjob where _job=@job"
        ' '' '' '' ''                parm = .Parameters.Add("@Job", SqlDbType.NVarChar, 20)
        ' '' '' '' ''                parm.Value = fncstr(txtJobNo.Text)
        ' '' '' '' ''            End If
        ' '' '' '' ''            DA.SelectCommand = com
        ' '' '' '' ''            DA.Fill(DSJobAC, "tbsch")
        ' '' '' '' ''            .Parameters.Clear()
        ' '' '' '' ''        End With
        ' '' '' '' ''    Catch ex As Exception
        ' '' '' '' ''        MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        ' '' '' '' ''        lblLoading.Visible = False
        ' '' '' '' ''        Exit Sub
        ' '' '' '' ''    End Try
        ' '' '' '' ''    Try
        ' '' '' '' ''        With com
        ' '' '' '' ''            Dim DA As New SqlDataAdapter
        ' '' '' '' ''            .Connection = cn
        ' '' '' '' ''            .CommandType = CommandType.Text
        ' '' '' '' ''            If cmbType.Text = "Between Date" Then
        ' '' '' '' ''                .CommandText = "select tbJobRoute.* from tbJobRoute  inner join tbJob on tbJobRoute._job=tbjob._job and tbJobRoute._jobSuffix=tbjob._jobSuffix where tbjob._jobDate between @Sd  and @ED"

        ' '' '' '' ''                parm = .Parameters.Add("@Sd", SqlDbType.Float)
        ' '' '' '' ''                parm.Value = SD.ToOADate
        ' '' '' '' ''                parm = .Parameters.Add("@Ed", SqlDbType.Float)
        ' '' '' '' ''                parm.Value = ED.ToOADate
        ' '' '' '' ''            Else
        ' '' '' '' ''                .CommandText = "select tbJobRoute.* from tbJobRoute  inner join tbJob on tbJobRoute._job=tbjob._job and tbJobRoute._jobSuffix=tbjob._jobSuffix where tbjob._job=@job"
        ' '' '' '' ''                parm = .Parameters.Add("@Job", SqlDbType.NVarChar, 20)
        ' '' '' '' ''                parm.Value = fncstr(txtJobNo.Text)
        ' '' '' '' ''            End If
        ' '' '' '' ''            DA.SelectCommand = com
        ' '' '' '' ''            DA.Fill(DSRouteAC, "tbsch")
        ' '' '' '' ''            .Parameters.Clear()
        ' '' '' '' ''        End With
        ' '' '' '' ''    Catch ex As Exception
        ' '' '' '' ''        MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        ' '' '' '' ''        lblLoading.Visible = False
        ' '' '' '' ''        Exit Sub
        ' '' '' '' ''    Finally
        ' '' '' '' ''        cn.Close()
        ' '' '' '' ''        com.Parameters.Clear()
        ' '' '' '' ''    End Try

        ' '' '' '' ''    Try
        ' '' '' '' ''        If DSJob.Tables(0).Rows.Count > 0 Then

        ' '' '' '' ''            Dim i As Integer
        ' '' '' '' ''            For i = 0 To DSJob.Tables(0).Rows.Count - 1
        ' '' '' '' ''                With com
        ' '' '' '' ''                    .Connection = cn
        ' '' '' '' ''                    .CommandType = CommandType.StoredProcedure
        ' '' '' '' ''                    .CommandText = "sp_tbJob_Download"
        ' '' '' '' ''                    Dim J1 As Integer
        ' '' '' '' ''                    ct = 0
        ' '' '' '' ''                    For J1 = 0 To 14
        ' '' '' '' ''                        If i < DSJob.Tables(0).Rows.Count Then

        ' '' '' '' ''                            ' type,job,item,suffix,job_date, qty_released,qty_complete,qty_scrapped
        ' '' '' '' ''                            parm = .Parameters.Add("@job_" & J1 + 1, SqlDbType.VarChar, 50)
        ' '' '' '' ''                            parm.Value = Trim(DSJob.Tables(0).Rows(i).Item("job"))

        ' '' '' '' ''                            parm = .Parameters.Add("@jobsuffix_" & J1 + 1, SqlDbType.Int)
        ' '' '' '' ''                            parm.Value = DSJob.Tables(0).Rows(i).Item("suffix")

        ' '' '' '' ''                            parm = .Parameters.Add("@jobDate_" & J1 + 1, SqlDbType.Float)
        ' '' '' '' ''                            parm.Value = CDate(DSJob.Tables(0).Rows(i).Item("job_date")).ToOADate

        ' '' '' '' ''                            parm = .Parameters.Add("@jobtype_" & J1 + 1, SqlDbType.VarChar, 50)
        ' '' '' '' ''                            parm.Value = DSJob.Tables(0).Rows(i).Item("type")

        ' '' '' '' ''                            parm = .Parameters.Add("@item_" & J1 + 1, SqlDbType.VarChar, 50)
        ' '' '' '' ''                            parm.Value = DSJob.Tables(0).Rows(i).Item("item")

        ' '' '' '' ''                            parm = .Parameters.Add("@qtyReleased_" & J1 + 1, SqlDbType.Float)
        ' '' '' '' ''                            parm.Value = DSJob.Tables(0).Rows(i).Item("qty_released")

        ' '' '' '' ''                            parm = .Parameters.Add("@qtyCompleted_" & J1 + 1, SqlDbType.Float)
        ' '' '' '' ''                            parm.Value = DSJob.Tables(0).Rows(i).Item("qty_complete")

        ' '' '' '' ''                            parm = .Parameters.Add("@qtyScrapped_" & J1 + 1, SqlDbType.Float)
        ' '' '' '' ''                            parm.Value = DSJob.Tables(0).Rows(i).Item("qty_scrapped")
        ' '' '' '' ''                            parm = .Parameters.Add("@stas_" & J1 + 1, SqlDbType.Char, 1)
        ' '' '' '' ''                            parm.Value = DSJob.Tables(0).Rows(i).Item("stat")


        ' '' '' '' ''                            Dim NR() As DataRow = DSJobAC.Tables(0).Select("_job='" & Trim(DSJob.Tables(0).Rows(i).Item("job")) & "' and _jobsuffix=" & DSJob.Tables(0).Rows(i).Item("suffix"))
        ' '' '' '' ''                            parm = .Parameters.Add("@status_" & J1 + 1, SqlDbType.VarChar, 255)
        ' '' '' '' ''                            If NR.Length > 0 Then
        ' '' '' '' ''                                If NR(0).Item("_qtyReleased") = DSJob.Tables(0).Rows(i).Item("qty_released") Then
        ' '' '' '' ''                                    parm.Value = "No"
        ' '' '' '' ''                                Else
        ' '' '' '' ''                                    parm.Value = "Edit"
        ' '' '' '' ''                                End If
        ' '' '' '' ''                            Else
        ' '' '' '' ''                                parm.Value = "New"
        ' '' '' '' ''                            End If

        ' '' '' '' ''                            ct = ct + 1

        ' '' '' '' ''                        Else




        ' '' '' '' ''                            parm = .Parameters.Add("@job_" & J1 + 1, SqlDbType.VarChar, 50)
        ' '' '' '' ''                            parm.Value = ""

        ' '' '' '' ''                            parm = .Parameters.Add("@jobsuffix_" & J1 + 1, SqlDbType.Int)
        ' '' '' '' ''                            parm.Value = 0

        ' '' '' '' ''                            parm = .Parameters.Add("@jobDate_" & J1 + 1, SqlDbType.Float)
        ' '' '' '' ''                            parm.Value = 0

        ' '' '' '' ''                            parm = .Parameters.Add("@jobtype_" & J1 + 1, SqlDbType.VarChar, 50)
        ' '' '' '' ''                            parm.Value = ""

        ' '' '' '' ''                            parm = .Parameters.Add("@item_" & J1 + 1, SqlDbType.VarChar, 50)
        ' '' '' '' ''                            parm.Value = ""

        ' '' '' '' ''                            parm = .Parameters.Add("@qtyReleased_" & J1 + 1, SqlDbType.Float)
        ' '' '' '' ''                            parm.Value = 0

        ' '' '' '' ''                            parm = .Parameters.Add("@qtyCompleted_" & J1 + 1, SqlDbType.Float)
        ' '' '' '' ''                            parm.Value = 0

        ' '' '' '' ''                            parm = .Parameters.Add("@qtyScrapped_" & J1 + 1, SqlDbType.Float)
        ' '' '' '' ''                            parm.Value = 0

        ' '' '' '' ''                            parm = .Parameters.Add("@status_" & J1 + 1, SqlDbType.VarChar, 255)
        ' '' '' '' ''                            parm.Value = ""
        ' '' '' '' ''                            parm = .Parameters.Add("@stas_" & J1 + 1, SqlDbType.Char, 1)
        ' '' '' '' ''                            parm.Value = ""
        ' '' '' '' ''                            'ct = ct + 1


        ' '' '' '' ''                        End If
        ' '' '' '' ''                        i = i + 1
        ' '' '' '' ''                    Next
        ' '' '' '' ''                    cn.Open()
        ' '' '' '' ''                    .ExecuteNonQuery()
        ' '' '' '' ''                    .Parameters.Clear()
        ' '' '' '' ''                    cn.Close()
        ' '' '' '' ''                End With
        ' '' '' '' ''                i = i - 1
        ' '' '' '' ''                at = at + ct
        ' '' '' '' ''                lblRunning.Text = at
        ' '' '' '' ''                If PB1.Value + ct <= PB1.Maximum Then
        ' '' '' '' ''                    PB1.Value = at
        ' '' '' '' ''                End If

        ' '' '' '' ''                Application.DoEvents()
        ' '' '' '' ''            Next
        ' '' '' '' ''        End If
        ' '' '' '' ''    Catch ex As Exception
        ' '' '' '' ''        MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        ' '' '' '' ''        Exit Sub
        ' '' '' '' ''    Finally
        ' '' '' '' ''        cn.Close()
        ' '' '' '' ''        com.Parameters.Clear()
        ' '' '' '' ''    End Try

        ' '' '' '' ''    ct = 0

        ' '' '' '' ''    Try
        ' '' '' '' ''        If DSRoute.Tables(0).Rows.Count > 0 Then

        ' '' '' '' ''            Dim i As Integer
        ' '' '' '' ''            For i = 0 To DSRoute.Tables(0).Rows.Count - 1
        ' '' '' '' ''                With com
        ' '' '' '' ''                    .Connection = cn
        ' '' '' '' ''                    .CommandType = CommandType.StoredProcedure
        ' '' '' '' ''                    .CommandText = "sp_tbjobRoute_Download"
        ' '' '' '' ''                    Dim J1 As Integer
        ' '' '' '' ''                    ct = 0
        ' '' '' '' ''                    t1 = ""
        ' '' '' '' ''                    For J1 = 0 To 14
        ' '' '' '' ''                        If i < DSRoute.Tables(0).Rows.Count Then
        ' '' '' '' ''                            'job,suffix,opernum,wc
        ' '' '' '' ''                            ' _job,_jobSuffix,_operationNo,_wc
        ' '' '' '' ''                            parm = .Parameters.Add("@job_" & J1 + 1, SqlDbType.VarChar, 50)
        ' '' '' '' ''                            parm.Value = Trim(DSRoute.Tables(0).Rows(i).Item("job"))

        ' '' '' '' ''                            parm = .Parameters.Add("@jobsuffix_" & J1 + 1, SqlDbType.Int)
        ' '' '' '' ''                            parm.Value = DSRoute.Tables(0).Rows(i).Item("suffix")

        ' '' '' '' ''                            parm = .Parameters.Add("@operationNo_" & J1 + 1, SqlDbType.Float)
        ' '' '' '' ''                            parm.Value = DSRoute.Tables(0).Rows(i).Item("opernum")

        ' '' '' '' ''                            parm = .Parameters.Add("@wc_" & J1 + 1, SqlDbType.VarChar, 50)
        ' '' '' '' ''                            parm.Value = DSRoute.Tables(0).Rows(i).Item("wc")


        ' '' '' '' ''                            Dim NR() As DataRow = DSRouteAC.Tables(0).Select("_job='" & Trim(DSRoute.Tables(0).Rows(i).Item("job")) & "' and _jobsuffix=" & DSRoute.Tables(0).Rows(i).Item("suffix") & " and  _operationNo=" & DSRoute.Tables(0).Rows(i).Item("opernum"))
        ' '' '' '' ''                            parm = .Parameters.Add("@status_" & J1 + 1, SqlDbType.VarChar, 255)
        ' '' '' '' ''                            If NR.Length > 0 Then
        ' '' '' '' ''                                parm.Value = "Edit"
        ' '' '' '' ''                            Else
        ' '' '' '' ''                                parm.Value = "New"
        ' '' '' '' ''                                't1 = Trim(DSRoute.Tables(0).Rows(i).Item("job"))
        ' '' '' '' ''                            End If



        ' '' '' '' ''                            ct = ct + 1
        ' '' '' '' ''                        Else
        ' '' '' '' ''                            parm = .Parameters.Add("@job_" & J1 + 1, SqlDbType.VarChar, 50)
        ' '' '' '' ''                            parm.Value = ""
        ' '' '' '' ''                            parm = .Parameters.Add("@jobsuffix_" & J1 + 1, SqlDbType.Int)
        ' '' '' '' ''                            parm.Value = 0
        ' '' '' '' ''                            parm = .Parameters.Add("@operationNo_" & J1 + 1, SqlDbType.Float)
        ' '' '' '' ''                            parm.Value = 0
        ' '' '' '' ''                            parm = .Parameters.Add("@wc_" & J1 + 1, SqlDbType.VarChar, 50)
        ' '' '' '' ''                            parm.Value = ""
        ' '' '' '' ''                            '  Dim NR() As DataRow = DSRouteAC.Tables(0).Select("_job='" & DSJob.Tables(0).Rows(i).Item("job") & "' and _jobsuffix=" & DSJob.Tables(0).Rows(i).Item("suffix") & " and _operationNo=" & DSJob.Tables(0).Rows(i).Item("opernum"))
        ' '' '' '' ''                            parm = .Parameters.Add("@status_" & J1 + 1, SqlDbType.VarChar, 255)
        ' '' '' '' ''                            ' If NR.Length > 0 Then
        ' '' '' '' ''                            parm.Value = ""
        ' '' '' '' ''                            ' Else

        ' '' '' '' ''                            'End If
        ' '' '' '' ''                            ' ct = ct + 1
        ' '' '' '' ''                        End If
        ' '' '' '' ''                        i = i + 1
        ' '' '' '' ''                    Next
        ' '' '' '' ''                    cn.Open()
        ' '' '' '' ''                    .ExecuteNonQuery()
        ' '' '' '' ''                    .Parameters.Clear()
        ' '' '' '' ''                    cn.Close()
        ' '' '' '' ''                End With
        ' '' '' '' ''                i = i - 1
        ' '' '' '' ''                at = at + ct
        ' '' '' '' ''                lblRunning.Text = at
        ' '' '' '' ''                If PB1.Value + ct <= PB1.Maximum Then
        ' '' '' '' ''                    PB1.Value = at
        ' '' '' '' ''                End If

        ' '' '' '' ''                Application.DoEvents()
        ' '' '' '' ''            Next

        ' '' '' '' ''        End If
        ' '' '' '' ''    Catch ex As Exception
        ' '' '' '' ''        MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        ' '' '' '' ''    Finally
        ' '' '' '' ''        cn.Close()
        ' '' '' '' ''        com.Parameters.Clear()
        ' '' '' '' ''    End Try




        ' '' '' '' ''End If
        ' '' '' '' ''MsgBox("Data download finished!", MsgBoxStyle.Information, "eWIP")

    End Sub
    Sub UpdateDownloadLog()
        Dim s() As String = Split(txtFrom.Text, "/")
        Dim SD As Date = DateSerial(s(2), s(1), s(0))
        Dim s1() As String = Split(txtTo.Text, "/")
        Dim ED As Date = DateSerial(s1(2), s1(1), s1(0))
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "sp_tbdownloadLog_add"

                parm = .Parameters.Add("@SD", SqlDbType.Float, 8)
                parm.Value = SD.ToOADate
                parm = .Parameters.Add("@ED", SqlDbType.Float, 8)
                parm.Value = ED.ToOADate
                parm = .Parameters.Add("@logby", SqlDbType.VarChar, 50)
                parm.Value = Trim(LogName)

                cn.Open()
                .ExecuteNonQuery()
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try

    End Sub
    Sub GetResourceGroup()
        Dim DSType As New DataSet
        Dim DSRS As New DataSet
        Dim DSWS As New DataSet
        Dim tot As Integer
        Try
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cnser
                .CommandType = CommandType.Text
                .CommandText = "select name from resourcetype"
                DA.SelectCommand = com
                DA.Fill(DSType, "tbsch")
            End With
            tot = tot + DSType.Tables(0).Rows.Count
            lblTot.Text = tot
            Application.DoEvents()
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cnser
                .CommandType = CommandType.Text
                .CommandText = "select RESTYPE,RESID,DESCR from RESRC000"
                DA.SelectCommand = com
                DA.Fill(DSRS, "tbsch")
            End With
            tot = tot + DSRS.Tables(0).Rows.Count
            lblTot.Text = tot
            Application.DoEvents()
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cnser
                .CommandType = CommandType.Text
                .CommandText = "SELECT    A.wc as wc ,B.description as description,A.rgid as rgid FROM wcresourcegroup A inner join wc B on A.wc=B.wc"
                DA.SelectCommand = com
                DA.Fill(DSWS, "tbsch")
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Exit Sub
        End Try
        tot = tot + DSWS.Tables(0).Rows.Count
        lblTot.Text = tot
        Application.DoEvents()
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "sp_delete_data"
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()

            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Exit Sub
        End Try
        PB1.Value = 0
        PB1.Minimum = 0
        PB1.Maximum = tot
        Dim at As Integer
        If DSType.Tables(0).Rows.Count > 0 Then

            Dim i As Integer
            For i = 0 To DSType.Tables(0).Rows.Count - 1
                With com
                    .Connection = cn
                    .CommandType = CommandType.StoredProcedure
                    .CommandText = "sp_tbresourcetype_Add"
                    Dim J1 As Integer
                    For J1 = 0 To 6
                        If i < DSType.Tables(0).Rows.Count Then
                            parm = .Parameters.Add("@Type_" & J1 + 1, SqlDbType.VarChar, 255)
                            parm.Value = DSType.Tables(0).Rows(i).Item("name")

                        Else
                            parm = .Parameters.Add("@Type_" & J1 + 1, SqlDbType.VarChar, 255)
                            parm.Value = ""
                        End If
                        i = i + 1

                    Next
                    cn.Open()
                    .ExecuteNonQuery()
                    .Parameters.Clear()
                    cn.Close()
                End With
                i = i - 1
                at = i
                PB1.Value = at
                Application.DoEvents()
            Next





        End If


        Dim ct As Integer = 0
        Try
            If DSRS.Tables(0).Rows.Count > 0 Then

                Dim i As Integer
                For i = 0 To DSRS.Tables(0).Rows.Count - 1
                    With com
                        .Connection = cn
                        .CommandType = CommandType.StoredProcedure
                        .CommandText = "sp_tbResourceGroup_Add"
                        Dim J1 As Integer
                        ct = 0
                        For J1 = 0 To 6
                            If i < DSRS.Tables(0).Rows.Count Then
                                parm = .Parameters.Add("@rGroupID_" & J1 + 1, SqlDbType.VarChar, 50)
                                parm.Value = DSRS.Tables(0).Rows(i).Item("RESID")
                                parm = .Parameters.Add("@rGroupName_" & J1 + 1, SqlDbType.VarChar, 255)
                                parm.Value = DSRS.Tables(0).Rows(i).Item("DESCR")
                                parm = .Parameters.Add("@Type_" & J1 + 1, SqlDbType.VarChar, 255)
                                parm.Value = DSRS.Tables(0).Rows(i).Item("RESTYPE")
                                ct = ct + 1
                            Else
                                parm = .Parameters.Add("@rGroupID_" & J1 + 1, SqlDbType.VarChar, 255)
                                parm.Value = ""
                                parm = .Parameters.Add("@rGroupName_" & J1 + 1, SqlDbType.VarChar, 255)
                                parm.Value = ""
                                parm = .Parameters.Add("@Type_" & J1 + 1, SqlDbType.VarChar, 255)
                                parm.Value = ""
                            End If
                            i = i + 1

                        Next
                        cn.Open()
                        .ExecuteNonQuery()
                        .Parameters.Clear()
                        cn.Close()
                    End With
                    i = i - 1
                    at = at + ct
                    If PB1.Value + ct <= PB1.Maximum Then
                        PB1.Value = at
                    End If

                    Application.DoEvents()
                Next
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Exit Sub
        End Try


        Try
            If DSWS.Tables(0).Rows.Count > 0 Then

                Dim i As Integer
                For i = 0 To DSWS.Tables(0).Rows.Count - 1
                    With com
                        .Connection = cn
                        .CommandType = CommandType.StoredProcedure
                        .CommandText = "sp_tbWorkStation_Add"
                        Dim J1 As Integer
                        ct = 0
                        For J1 = 0 To 6
                            If i < DSWS.Tables(0).Rows.Count Then
                                parm = .Parameters.Add("@stationidno_" & J1 + 1, SqlDbType.VarChar, 50)
                                parm.Value = DSWS.Tables(0).Rows(i).Item("wc")
                                parm = .Parameters.Add("@stationDescription_" & J1 + 1, SqlDbType.VarChar, 255)
                                parm.Value = DSWS.Tables(0).Rows(i).Item("description")
                                parm = .Parameters.Add("@rGroupid_" & J1 + 1, SqlDbType.VarChar, 50)
                                parm.Value = DSWS.Tables(0).Rows(i).Item("rgid")
                                ct = ct + 1
                            Else
                                parm = .Parameters.Add("@stationidno_" & J1 + 1, SqlDbType.VarChar, 50)
                                parm.Value = ""
                                parm = .Parameters.Add("@stationDescription_" & J1 + 1, SqlDbType.VarChar, 255)
                                parm.Value = ""
                                parm = .Parameters.Add("@rGroupid_" & J1 + 1, SqlDbType.VarChar, 50)
                                parm.Value = ""
                            End If
                            i = i + 1

                        Next
                        cn.Open()
                        .ExecuteNonQuery()
                        .Parameters.Clear()
                        cn.Close()
                    End With
                    i = i - 1
                    at = at + ct
                    If PB1.Value + ct <= PB1.Maximum Then
                        PB1.Value = at
                    End If

                    Application.DoEvents()
                Next
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Exit Sub
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Shell("C:\WINDOWS\system32\calc.exe", AppWinStyle.NormalFocus)
        'Shell("C:\WINDOWS\system32\notepad.exe", AppWinStyle.NormalFocus)
        'Shell("C:\Projects\5 Star\Source\ProReadExcel\bin\ProReadExcel.exe", AppWinStyle.NormalFocus)


    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class