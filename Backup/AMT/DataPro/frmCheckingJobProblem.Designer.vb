<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCheckingJobProblem
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.butFind = New System.Windows.Forms.Button
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtFindJob = New System.Windows.Forms.TextBox
        Me.txtProb = New System.Windows.Forms.TextBox
        Me.P1 = New System.Windows.Forms.Panel
        Me.PB1 = New System.Windows.Forms.ProgressBar
        Me.P1.SuspendLayout()
        Me.SuspendLayout()
        '
        'butFind
        '
        Me.butFind.BackColor = System.Drawing.Color.Gray
        Me.butFind.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butFind.ForeColor = System.Drawing.Color.White
        Me.butFind.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butFind.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butFind.Location = New System.Drawing.Point(73, 247)
        Me.butFind.Name = "butFind"
        Me.butFind.Size = New System.Drawing.Size(57, 24)
        Me.butFind.TabIndex = 129
        Me.butFind.Text = "Find"
        Me.butFind.UseVisualStyleBackColor = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(70, 56)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(152, 15)
        Me.Label7.TabIndex = 128
        Me.Label7.Text = "Endter The  Job Order No."
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(40, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(168, 28)
        Me.Label2.TabIndex = 136
        Me.Label2.Text = "Find Data Pro"
        '
        'txtFindJob
        '
        Me.txtFindJob.BackColor = System.Drawing.Color.White
        Me.txtFindJob.Location = New System.Drawing.Point(73, 84)
        Me.txtFindJob.Multiline = True
        Me.txtFindJob.Name = "txtFindJob"
        Me.txtFindJob.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.txtFindJob.Size = New System.Drawing.Size(610, 157)
        Me.txtFindJob.TabIndex = 137
        '
        'txtProb
        '
        Me.txtProb.BackColor = System.Drawing.Color.White
        Me.txtProb.Location = New System.Drawing.Point(73, 280)
        Me.txtProb.Multiline = True
        Me.txtProb.Name = "txtProb"
        Me.txtProb.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.txtProb.Size = New System.Drawing.Size(610, 141)
        Me.txtProb.TabIndex = 138
        '
        'P1
        '
        Me.P1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.P1.BackColor = System.Drawing.Color.Gray
        Me.P1.Controls.Add(Me.PB1)
        Me.P1.Location = New System.Drawing.Point(309, 245)
        Me.P1.Name = "P1"
        Me.P1.Size = New System.Drawing.Size(295, 32)
        Me.P1.TabIndex = 139
        '
        'PB1
        '
        Me.PB1.Location = New System.Drawing.Point(15, 7)
        Me.PB1.Name = "PB1"
        Me.PB1.Size = New System.Drawing.Size(266, 19)
        Me.PB1.TabIndex = 0
        '
        'frmCheckingJobProblem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(912, 606)
        Me.Controls.Add(Me.P1)
        Me.Controls.Add(Me.txtProb)
        Me.Controls.Add(Me.txtFindJob)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.butFind)
        Me.Controls.Add(Me.Label7)
        Me.Name = "frmCheckingJobProblem"
        Me.Text = "frmCheckingJobProblem"
        Me.P1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents butFind As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtFindJob As System.Windows.Forms.TextBox
    Friend WithEvents txtProb As System.Windows.Forms.TextBox
    Friend WithEvents P1 As System.Windows.Forms.Panel
    Friend WithEvents PB1 As System.Windows.Forms.ProgressBar
End Class
