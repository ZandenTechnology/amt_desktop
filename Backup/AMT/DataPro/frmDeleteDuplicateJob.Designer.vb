<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDeleteDuplicateJob
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtJob = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.lstv = New System.Windows.Forms.ListView
        Me.butFind = New System.Windows.Forms.Button
        Me.txtOPNo = New System.Windows.Forms.TextBox
        Me.txtSuf = New System.Windows.Forms.TextBox
        Me.txtOPQty = New System.Windows.Forms.TextBox
        Me.txtComqty = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.txtJobNo = New System.Windows.Forms.TextBox
        Me.txtTrans = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtRejected = New System.Windows.Forms.TextBox
        Me.txtStatus = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(9, 14)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(152, 15)
        Me.Label7.TabIndex = 109
        Me.Label7.Text = "Endter The  Job Order No."
        '
        'txtJob
        '
        Me.txtJob.BackColor = System.Drawing.Color.White
        Me.txtJob.Location = New System.Drawing.Point(167, 12)
        Me.txtJob.Name = "txtJob"
        Me.txtJob.Size = New System.Drawing.Size(155, 20)
        Me.txtJob.TabIndex = 108
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(397, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(402, 28)
        Me.Label1.TabIndex = 110
        Me.Label1.Text = "Delete  Duplicate Job Transaction "
        '
        'lstv
        '
        Me.lstv.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(5, 60)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(841, 162)
        Me.lstv.TabIndex = 111
        Me.lstv.TabStop = False
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'butFind
        '
        Me.butFind.BackColor = System.Drawing.Color.Gray
        Me.butFind.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butFind.ForeColor = System.Drawing.Color.White
        Me.butFind.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butFind.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butFind.Location = New System.Drawing.Point(328, 10)
        Me.butFind.Name = "butFind"
        Me.butFind.Size = New System.Drawing.Size(57, 24)
        Me.butFind.TabIndex = 126
        Me.butFind.Text = "Find"
        Me.butFind.UseVisualStyleBackColor = False
        '
        'txtOPNo
        '
        Me.txtOPNo.BackColor = System.Drawing.Color.White
        Me.txtOPNo.Location = New System.Drawing.Point(143, 245)
        Me.txtOPNo.Name = "txtOPNo"
        Me.txtOPNo.ReadOnly = True
        Me.txtOPNo.Size = New System.Drawing.Size(112, 20)
        Me.txtOPNo.TabIndex = 127
        '
        'txtSuf
        '
        Me.txtSuf.BackColor = System.Drawing.Color.White
        Me.txtSuf.Location = New System.Drawing.Point(143, 272)
        Me.txtSuf.Name = "txtSuf"
        Me.txtSuf.ReadOnly = True
        Me.txtSuf.Size = New System.Drawing.Size(156, 20)
        Me.txtSuf.TabIndex = 128
        '
        'txtOPQty
        '
        Me.txtOPQty.BackColor = System.Drawing.Color.White
        Me.txtOPQty.Location = New System.Drawing.Point(411, 245)
        Me.txtOPQty.Name = "txtOPQty"
        Me.txtOPQty.ReadOnly = True
        Me.txtOPQty.Size = New System.Drawing.Size(101, 20)
        Me.txtOPQty.TabIndex = 129
        '
        'txtComqty
        '
        Me.txtComqty.BackColor = System.Drawing.Color.White
        Me.txtComqty.Location = New System.Drawing.Point(411, 271)
        Me.txtComqty.Name = "txtComqty"
        Me.txtComqty.ReadOnly = True
        Me.txtComqty.Size = New System.Drawing.Size(133, 20)
        Me.txtComqty.TabIndex = 130
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(95, 247)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 15)
        Me.Label2.TabIndex = 131
        Me.Label2.Text = "OP No"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(95, 276)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 15)
        Me.Label3.TabIndex = 132
        Me.Label3.Text = "Suffix"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(356, 247)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 15)
        Me.Label4.TabIndex = 133
        Me.Label4.Text = "OP Qty"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(350, 274)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 15)
        Me.Label5.TabIndex = 134
        Me.Label5.Text = "Com Qty"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Gray
        Me.Button1.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Button1.Location = New System.Drawing.Point(143, 313)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(83, 24)
        Me.Button1.TabIndex = 135
        Me.Button1.Text = "Delete"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Gray
        Me.Button2.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Button2.Location = New System.Drawing.Point(239, 313)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(83, 24)
        Me.Button2.TabIndex = 136
        Me.Button2.Text = "Exit"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'txtJobNo
        '
        Me.txtJobNo.BackColor = System.Drawing.Color.White
        Me.txtJobNo.Location = New System.Drawing.Point(626, 359)
        Me.txtJobNo.Name = "txtJobNo"
        Me.txtJobNo.Size = New System.Drawing.Size(112, 20)
        Me.txtJobNo.TabIndex = 137
        Me.txtJobNo.Visible = False
        '
        'txtTrans
        '
        Me.txtTrans.BackColor = System.Drawing.Color.White
        Me.txtTrans.Location = New System.Drawing.Point(6, 242)
        Me.txtTrans.Name = "txtTrans"
        Me.txtTrans.ReadOnly = True
        Me.txtTrans.Size = New System.Drawing.Size(85, 20)
        Me.txtTrans.TabIndex = 138
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(579, 244)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 15)
        Me.Label6.TabIndex = 139
        Me.Label6.Text = "Rej Qty"
        '
        'txtRejected
        '
        Me.txtRejected.BackColor = System.Drawing.Color.White
        Me.txtRejected.Location = New System.Drawing.Point(637, 242)
        Me.txtRejected.Name = "txtRejected"
        Me.txtRejected.ReadOnly = True
        Me.txtRejected.Size = New System.Drawing.Size(101, 20)
        Me.txtRejected.TabIndex = 140
        '
        'txtStatus
        '
        Me.txtStatus.BackColor = System.Drawing.Color.White
        Me.txtStatus.Location = New System.Drawing.Point(637, 276)
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.ReadOnly = True
        Me.txtStatus.Size = New System.Drawing.Size(101, 20)
        Me.txtStatus.TabIndex = 141
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(579, 278)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(44, 15)
        Me.Label8.TabIndex = 142
        Me.Label8.Text = "Status"
        '
        'frmDeleteDuplicateJob
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(849, 431)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtStatus)
        Me.Controls.Add(Me.txtRejected)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtTrans)
        Me.Controls.Add(Me.txtJobNo)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtComqty)
        Me.Controls.Add(Me.txtOPQty)
        Me.Controls.Add(Me.txtSuf)
        Me.Controls.Add(Me.txtOPNo)
        Me.Controls.Add(Me.butFind)
        Me.Controls.Add(Me.lstv)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtJob)
        Me.Name = "frmDeleteDuplicateJob"
        Me.Text = "frmDeleteDuplicateJob"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtJob As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lstv As System.Windows.Forms.ListView
    Friend WithEvents butFind As System.Windows.Forms.Button
    Friend WithEvents txtOPNo As System.Windows.Forms.TextBox
    Friend WithEvents txtSuf As System.Windows.Forms.TextBox
    Friend WithEvents txtOPQty As System.Windows.Forms.TextBox
    Friend WithEvents txtComqty As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents txtJobNo As System.Windows.Forms.TextBox
    Friend WithEvents txtTrans As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtRejected As System.Windows.Forms.TextBox
    Friend WithEvents txtStatus As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
End Class
