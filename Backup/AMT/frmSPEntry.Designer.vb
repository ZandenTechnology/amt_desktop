<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSPEntry
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSPEntry))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.butPre = New System.Windows.Forms.Button
        Me.butNext = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtJonno = New System.Windows.Forms.TextBox
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.lblTot = New System.Windows.Forms.Label
        Me.txtTot = New System.Windows.Forms.Label
        Me.lstv = New System.Windows.Forms.ListView
        Me.lstvData = New System.Windows.Forms.ListView
        Me.pnlnoofOp = New System.Windows.Forms.Panel
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtKeynoofOp = New System.Windows.Forms.TextBox
        Me.pnlcom = New System.Windows.Forms.Panel
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtKeyRejqty = New System.Windows.Forms.TextBox
        Me.txtKeyComqty = New System.Windows.Forms.TextBox
        Me.txtOPid = New System.Windows.Forms.Label
        Me.txtID = New System.Windows.Forms.Label
        Me.fltStartDate = New System.Windows.Forms.Label
        Me.txtParent = New System.Windows.Forms.Label
        Me.txtsuf = New System.Windows.Forms.Label
        Me.lbl = New System.Windows.Forms.Label
        Me.txtJobno = New System.Windows.Forms.Label
        Me.txtAction = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.txtComQty = New System.Windows.Forms.Label
        Me.lblRlQ = New System.Windows.Forms.Label
        Me.txtEdate = New System.Windows.Forms.Label
        Me.txtReQ = New System.Windows.Forms.Label
        Me.txtRlQ = New System.Windows.Forms.Label
        Me.lblReQ = New System.Windows.Forms.Label
        Me.txtStartDate = New System.Windows.Forms.Label
        Me.txtRejQ = New System.Windows.Forms.Label
        Me.lblEdate = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.lblComqty = New System.Windows.Forms.Label
        Me.lblRejQ = New System.Windows.Forms.Label
        Me.lblMsgbox = New System.Windows.Forms.Label
        Me.lblAction = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.txtnoofOP = New System.Windows.Forms.Label
        Me.lblnoofOP = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.Label
        Me.txtOperatorid = New System.Windows.Forms.Label
        Me.txtMachineId = New System.Windows.Forms.Label
        Me.txtWorkcenter = New System.Windows.Forms.Label
        Me.txtOperationno = New System.Windows.Forms.Label
        Me.txtItem = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.lblOperatorid = New System.Windows.Forms.Label
        Me.lblMachineId = New System.Windows.Forms.Label
        Me.lblOperationno = New System.Windows.Forms.Label
        Me.lblWorkcenter = New System.Windows.Forms.Label
        Me.lblItem = New System.Windows.Forms.Label
        Me.lblJobno = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.pnlnoofOp.SuspendLayout()
        Me.pnlcom.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.Info
        Me.Panel1.Controls.Add(Me.Panel5)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Controls.Add(Me.lstvData)
        Me.Panel1.Controls.Add(Me.pnlnoofOp)
        Me.Panel1.Controls.Add(Me.pnlcom)
        Me.Panel1.Controls.Add(Me.txtOPid)
        Me.Panel1.Controls.Add(Me.txtID)
        Me.Panel1.Controls.Add(Me.fltStartDate)
        Me.Panel1.Controls.Add(Me.txtParent)
        Me.Panel1.Controls.Add(Me.txtsuf)
        Me.Panel1.Controls.Add(Me.lbl)
        Me.Panel1.Controls.Add(Me.txtJobno)
        Me.Panel1.Controls.Add(Me.txtAction)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.lblMsgbox)
        Me.Panel1.Controls.Add(Me.lblAction)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.lblJobno)
        Me.Panel1.Location = New System.Drawing.Point(49, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(771, 597)
        Me.Panel1.TabIndex = 1
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Beige
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.butPre)
        Me.Panel5.Controls.Add(Me.butNext)
        Me.Panel5.Controls.Add(Me.Label4)
        Me.Panel5.Controls.Add(Me.txtJonno)
        Me.Panel5.Location = New System.Drawing.Point(0, 3)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(785, 48)
        Me.Panel5.TabIndex = 98
        Me.Panel5.Visible = False
        '
        'butPre
        '
        Me.butPre.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.butPre.Image = CType(resources.GetObject("butPre.Image"), System.Drawing.Image)
        Me.butPre.Location = New System.Drawing.Point(280, 3)
        Me.butPre.Name = "butPre"
        Me.butPre.Size = New System.Drawing.Size(61, 41)
        Me.butPre.TabIndex = 59
        Me.butPre.UseVisualStyleBackColor = True
        '
        'butNext
        '
        Me.butNext.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.butNext.Image = CType(resources.GetObject("butNext.Image"), System.Drawing.Image)
        Me.butNext.Location = New System.Drawing.Point(357, 3)
        Me.butNext.Name = "butNext"
        Me.butNext.Size = New System.Drawing.Size(61, 41)
        Me.butNext.TabIndex = 60
        Me.butNext.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(13, 17)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(108, 13)
        Me.Label4.TabIndex = 58
        Me.Label4.Text = "REJECTED QTY :"
        '
        'txtJonno
        '
        Me.txtJonno.Location = New System.Drawing.Point(126, 13)
        Me.txtJonno.Name = "txtJonno"
        Me.txtJonno.Size = New System.Drawing.Size(126, 20)
        Me.txtJonno.TabIndex = 56
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Beige
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.lblTot)
        Me.Panel4.Controls.Add(Me.txtTot)
        Me.Panel4.Controls.Add(Me.lstv)
        Me.Panel4.Location = New System.Drawing.Point(608, 65)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(153, 299)
        Me.Panel4.TabIndex = 97
        '
        'lblTot
        '
        Me.lblTot.AutoSize = True
        Me.lblTot.Location = New System.Drawing.Point(3, 278)
        Me.lblTot.Name = "lblTot"
        Me.lblTot.Size = New System.Drawing.Size(37, 13)
        Me.lblTot.TabIndex = 95
        Me.lblTot.Text = "Total :"
        '
        'txtTot
        '
        Me.txtTot.AutoSize = True
        Me.txtTot.Location = New System.Drawing.Point(43, 278)
        Me.txtTot.Name = "txtTot"
        Me.txtTot.Size = New System.Drawing.Size(0, 13)
        Me.txtTot.TabIndex = 94
        '
        'lstv
        '
        Me.lstv.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.lstv.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(3, -1)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(145, 272)
        Me.lstv.TabIndex = 93
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'lstvData
        '
        Me.lstvData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.lstvData.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstvData.FullRowSelect = True
        Me.lstvData.GridLines = True
        Me.lstvData.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lstvData.Location = New System.Drawing.Point(0, 431)
        Me.lstvData.Name = "lstvData"
        Me.lstvData.Size = New System.Drawing.Size(768, 162)
        Me.lstvData.TabIndex = 96
        Me.lstvData.TabStop = False
        Me.lstvData.UseCompatibleStateImageBehavior = False
        Me.lstvData.View = System.Windows.Forms.View.Details
        '
        'pnlnoofOp
        '
        Me.pnlnoofOp.BackColor = System.Drawing.Color.DarkKhaki
        Me.pnlnoofOp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlnoofOp.Controls.Add(Me.Label3)
        Me.pnlnoofOp.Controls.Add(Me.txtKeynoofOp)
        Me.pnlnoofOp.Location = New System.Drawing.Point(18, 146)
        Me.pnlnoofOp.Name = "pnlnoofOp"
        Me.pnlnoofOp.Size = New System.Drawing.Size(148, 61)
        Me.pnlnoofOp.TabIndex = 95
        Me.pnlnoofOp.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(8, 11)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(121, 13)
        Me.Label3.TabIndex = 57
        Me.Label3.Text = "NO. OF OPERATOR"
        '
        'txtKeynoofOp
        '
        Me.txtKeynoofOp.Location = New System.Drawing.Point(11, 27)
        Me.txtKeynoofOp.Name = "txtKeynoofOp"
        Me.txtKeynoofOp.Size = New System.Drawing.Size(126, 20)
        Me.txtKeynoofOp.TabIndex = 56
        '
        'pnlcom
        '
        Me.pnlcom.BackColor = System.Drawing.Color.Beige
        Me.pnlcom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlcom.Controls.Add(Me.Label2)
        Me.pnlcom.Controls.Add(Me.Label1)
        Me.pnlcom.Controls.Add(Me.txtKeyRejqty)
        Me.pnlcom.Controls.Add(Me.txtKeyComqty)
        Me.pnlcom.Location = New System.Drawing.Point(18, 213)
        Me.pnlcom.Name = "pnlcom"
        Me.pnlcom.Size = New System.Drawing.Size(148, 117)
        Me.pnlcom.TabIndex = 91
        Me.pnlcom.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(10, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(108, 13)
        Me.Label2.TabIndex = 58
        Me.Label2.Text = "REJECTED QTY :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(119, 13)
        Me.Label1.TabIndex = 57
        Me.Label1.Text = "COMPLETED QTY :"
        '
        'txtKeyRejqty
        '
        Me.txtKeyRejqty.Location = New System.Drawing.Point(13, 71)
        Me.txtKeyRejqty.Name = "txtKeyRejqty"
        Me.txtKeyRejqty.Size = New System.Drawing.Size(126, 20)
        Me.txtKeyRejqty.TabIndex = 56
        '
        'txtKeyComqty
        '
        Me.txtKeyComqty.Location = New System.Drawing.Point(11, 27)
        Me.txtKeyComqty.Name = "txtKeyComqty"
        Me.txtKeyComqty.Size = New System.Drawing.Size(126, 20)
        Me.txtKeyComqty.TabIndex = 56
        '
        'txtOPid
        '
        Me.txtOPid.AutoSize = True
        Me.txtOPid.Location = New System.Drawing.Point(630, 325)
        Me.txtOPid.Name = "txtOPid"
        Me.txtOPid.Size = New System.Drawing.Size(0, 13)
        Me.txtOPid.TabIndex = 89
        Me.txtOPid.Visible = False
        '
        'txtID
        '
        Me.txtID.AutoSize = True
        Me.txtID.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtID.ForeColor = System.Drawing.Color.Black
        Me.txtID.Location = New System.Drawing.Point(701, 387)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(0, 19)
        Me.txtID.TabIndex = 90
        Me.txtID.Visible = False
        '
        'fltStartDate
        '
        Me.fltStartDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fltStartDate.Location = New System.Drawing.Point(418, 379)
        Me.fltStartDate.Name = "fltStartDate"
        Me.fltStartDate.Size = New System.Drawing.Size(149, 13)
        Me.fltStartDate.TabIndex = 87
        Me.fltStartDate.Text = " "
        Me.fltStartDate.Visible = False
        '
        'txtParent
        '
        Me.txtParent.AutoSize = True
        Me.txtParent.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtParent.ForeColor = System.Drawing.Color.Black
        Me.txtParent.Location = New System.Drawing.Point(454, 461)
        Me.txtParent.Name = "txtParent"
        Me.txtParent.Size = New System.Drawing.Size(0, 19)
        Me.txtParent.TabIndex = 86
        '
        'txtsuf
        '
        Me.txtsuf.AutoSize = True
        Me.txtsuf.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsuf.ForeColor = System.Drawing.Color.Black
        Me.txtsuf.Location = New System.Drawing.Point(276, 5)
        Me.txtsuf.Name = "txtsuf"
        Me.txtsuf.Size = New System.Drawing.Size(0, 19)
        Me.txtsuf.TabIndex = 84
        '
        'lbl
        '
        Me.lbl.AutoSize = True
        Me.lbl.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl.ForeColor = System.Drawing.Color.Black
        Me.lbl.Location = New System.Drawing.Point(261, 3)
        Me.lbl.Name = "lbl"
        Me.lbl.Size = New System.Drawing.Size(0, 22)
        Me.lbl.TabIndex = 83
        '
        'txtJobno
        '
        Me.txtJobno.AutoSize = True
        Me.txtJobno.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobno.ForeColor = System.Drawing.Color.Black
        Me.txtJobno.Location = New System.Drawing.Point(146, 5)
        Me.txtJobno.Name = "txtJobno"
        Me.txtJobno.Size = New System.Drawing.Size(0, 19)
        Me.txtJobno.TabIndex = 82
        '
        'txtAction
        '
        Me.txtAction.AutoSize = True
        Me.txtAction.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAction.ForeColor = System.Drawing.Color.Black
        Me.txtAction.Location = New System.Drawing.Point(158, 35)
        Me.txtAction.Name = "txtAction"
        Me.txtAction.Size = New System.Drawing.Size(12, 16)
        Me.txtAction.TabIndex = 70
        Me.txtAction.Text = " "
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Beige
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.txtComQty)
        Me.Panel3.Controls.Add(Me.lblRlQ)
        Me.Panel3.Controls.Add(Me.txtEdate)
        Me.Panel3.Controls.Add(Me.txtReQ)
        Me.Panel3.Controls.Add(Me.txtRlQ)
        Me.Panel3.Controls.Add(Me.lblReQ)
        Me.Panel3.Controls.Add(Me.txtStartDate)
        Me.Panel3.Controls.Add(Me.txtRejQ)
        Me.Panel3.Controls.Add(Me.lblEdate)
        Me.Panel3.Controls.Add(Me.lblStartDate)
        Me.Panel3.Controls.Add(Me.lblComqty)
        Me.Panel3.Controls.Add(Me.lblRejQ)
        Me.Panel3.Location = New System.Drawing.Point(303, 239)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(301, 125)
        Me.Panel3.TabIndex = 69
        '
        'txtComQty
        '
        Me.txtComQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComQty.Location = New System.Drawing.Point(149, 64)
        Me.txtComQty.Name = "txtComQty"
        Me.txtComQty.Size = New System.Drawing.Size(153, 13)
        Me.txtComQty.TabIndex = 69
        Me.txtComQty.Text = " "
        '
        'lblRlQ
        '
        Me.lblRlQ.AutoSize = True
        Me.lblRlQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRlQ.Location = New System.Drawing.Point(3, 4)
        Me.lblRlQ.Name = "lblRlQ"
        Me.lblRlQ.Size = New System.Drawing.Size(147, 13)
        Me.lblRlQ.TabIndex = 67
        Me.lblRlQ.Text = "RELEASED QUANTITY :"
        '
        'txtEdate
        '
        Me.txtEdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEdate.Location = New System.Drawing.Point(149, 104)
        Me.txtEdate.Name = "txtEdate"
        Me.txtEdate.Size = New System.Drawing.Size(151, 19)
        Me.txtEdate.TabIndex = 68
        '
        'txtReQ
        '
        Me.txtReQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReQ.Location = New System.Drawing.Point(149, 23)
        Me.txtReQ.Name = "txtReQ"
        Me.txtReQ.Size = New System.Drawing.Size(149, 13)
        Me.txtReQ.TabIndex = 65
        Me.txtReQ.Text = " "
        '
        'txtRlQ
        '
        Me.txtRlQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRlQ.Location = New System.Drawing.Point(149, 4)
        Me.txtRlQ.Name = "txtRlQ"
        Me.txtRlQ.Size = New System.Drawing.Size(149, 13)
        Me.txtRlQ.TabIndex = 61
        Me.txtRlQ.Text = " "
        '
        'lblReQ
        '
        Me.lblReQ.AutoSize = True
        Me.lblReQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReQ.Location = New System.Drawing.Point(3, 24)
        Me.lblReQ.Name = "lblReQ"
        Me.lblReQ.Size = New System.Drawing.Size(106, 13)
        Me.lblReQ.TabIndex = 46
        Me.lblReQ.Text = "RECEIVED QTY :"
        '
        'txtStartDate
        '
        Me.txtStartDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStartDate.Location = New System.Drawing.Point(149, 42)
        Me.txtStartDate.Name = "txtStartDate"
        Me.txtStartDate.Size = New System.Drawing.Size(149, 13)
        Me.txtStartDate.TabIndex = 66
        Me.txtStartDate.Text = " "
        '
        'txtRejQ
        '
        Me.txtRejQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRejQ.Location = New System.Drawing.Point(149, 84)
        Me.txtRejQ.Name = "txtRejQ"
        Me.txtRejQ.Size = New System.Drawing.Size(149, 13)
        Me.txtRejQ.TabIndex = 67
        Me.txtRejQ.Text = " "
        '
        'lblEdate
        '
        Me.lblEdate.AutoSize = True
        Me.lblEdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEdate.Location = New System.Drawing.Point(3, 104)
        Me.lblEdate.Name = "lblEdate"
        Me.lblEdate.Size = New System.Drawing.Size(78, 13)
        Me.lblEdate.TabIndex = 56
        Me.lblEdate.Text = "END DATE :"
        '
        'lblStartDate
        '
        Me.lblStartDate.AutoSize = True
        Me.lblStartDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(2, 44)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(93, 13)
        Me.lblStartDate.TabIndex = 54
        Me.lblStartDate.Text = "START DATE :"
        '
        'lblComqty
        '
        Me.lblComqty.AutoSize = True
        Me.lblComqty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComqty.Location = New System.Drawing.Point(3, 64)
        Me.lblComqty.Name = "lblComqty"
        Me.lblComqty.Size = New System.Drawing.Size(119, 13)
        Me.lblComqty.TabIndex = 55
        Me.lblComqty.Text = "COMPLETED QTY :"
        '
        'lblRejQ
        '
        Me.lblRejQ.AutoSize = True
        Me.lblRejQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRejQ.Location = New System.Drawing.Point(5, 84)
        Me.lblRejQ.Name = "lblRejQ"
        Me.lblRejQ.Size = New System.Drawing.Size(108, 13)
        Me.lblRejQ.TabIndex = 57
        Me.lblRejQ.Text = "REJECTED QTY :"
        '
        'lblMsgbox
        '
        Me.lblMsgbox.AutoSize = True
        Me.lblMsgbox.BackColor = System.Drawing.Color.FromArgb(CType(CType(11, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.lblMsgbox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsgbox.ForeColor = System.Drawing.Color.DarkRed
        Me.lblMsgbox.Location = New System.Drawing.Point(629, 394)
        Me.lblMsgbox.Name = "lblMsgbox"
        Me.lblMsgbox.Size = New System.Drawing.Size(110, 20)
        Me.lblMsgbox.TabIndex = 52
        Me.lblMsgbox.Text = "Machine ID :"
        '
        'lblAction
        '
        Me.lblAction.AutoSize = True
        Me.lblAction.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAction.ForeColor = System.Drawing.Color.Black
        Me.lblAction.Location = New System.Drawing.Point(6, 60)
        Me.lblAction.Name = "lblAction"
        Me.lblAction.Size = New System.Drawing.Size(65, 16)
        Me.lblAction.TabIndex = 47
        Me.lblAction.Text = "ACTION :"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Beige
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.txtnoofOP)
        Me.Panel2.Controls.Add(Me.lblnoofOP)
        Me.Panel2.Controls.Add(Me.txtName)
        Me.Panel2.Controls.Add(Me.txtOperatorid)
        Me.Panel2.Controls.Add(Me.txtMachineId)
        Me.Panel2.Controls.Add(Me.txtWorkcenter)
        Me.Panel2.Controls.Add(Me.txtOperationno)
        Me.Panel2.Controls.Add(Me.txtItem)
        Me.Panel2.Controls.Add(Me.lblName)
        Me.Panel2.Controls.Add(Me.lblOperatorid)
        Me.Panel2.Controls.Add(Me.lblMachineId)
        Me.Panel2.Controls.Add(Me.lblOperationno)
        Me.Panel2.Controls.Add(Me.lblWorkcenter)
        Me.Panel2.Controls.Add(Me.lblItem)
        Me.Panel2.Location = New System.Drawing.Point(303, 63)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(301, 170)
        Me.Panel2.TabIndex = 46
        '
        'txtnoofOP
        '
        Me.txtnoofOP.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnoofOP.Location = New System.Drawing.Point(128, 151)
        Me.txtnoofOP.Name = "txtnoofOP"
        Me.txtnoofOP.Size = New System.Drawing.Size(168, 13)
        Me.txtnoofOP.TabIndex = 66
        Me.txtnoofOP.Text = " "
        '
        'lblnoofOP
        '
        Me.lblnoofOP.AutoSize = True
        Me.lblnoofOP.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblnoofOP.Location = New System.Drawing.Point(1, 151)
        Me.lblnoofOP.Name = "lblnoofOP"
        Me.lblnoofOP.Size = New System.Drawing.Size(129, 13)
        Me.lblnoofOP.TabIndex = 65
        Me.lblnoofOP.Text = "NO. OF OPERATOR :"
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Location = New System.Drawing.Point(128, 131)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(168, 13)
        Me.txtName.TabIndex = 64
        Me.txtName.Text = " "
        '
        'txtOperatorid
        '
        Me.txtOperatorid.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOperatorid.Location = New System.Drawing.Point(128, 105)
        Me.txtOperatorid.Name = "txtOperatorid"
        Me.txtOperatorid.Size = New System.Drawing.Size(168, 13)
        Me.txtOperatorid.TabIndex = 63
        Me.txtOperatorid.Text = " "
        '
        'txtMachineId
        '
        Me.txtMachineId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMachineId.Location = New System.Drawing.Point(128, 79)
        Me.txtMachineId.Name = "txtMachineId"
        Me.txtMachineId.Size = New System.Drawing.Size(168, 13)
        Me.txtMachineId.TabIndex = 62
        Me.txtMachineId.Text = " "
        '
        'txtWorkcenter
        '
        Me.txtWorkcenter.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWorkcenter.Location = New System.Drawing.Point(128, 55)
        Me.txtWorkcenter.Name = "txtWorkcenter"
        Me.txtWorkcenter.Size = New System.Drawing.Size(168, 13)
        Me.txtWorkcenter.TabIndex = 60
        Me.txtWorkcenter.Text = " "
        '
        'txtOperationno
        '
        Me.txtOperationno.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOperationno.Location = New System.Drawing.Point(128, 29)
        Me.txtOperationno.Name = "txtOperationno"
        Me.txtOperationno.Size = New System.Drawing.Size(168, 13)
        Me.txtOperationno.TabIndex = 59
        Me.txtOperationno.Text = " "
        '
        'txtItem
        '
        Me.txtItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItem.Location = New System.Drawing.Point(128, 3)
        Me.txtItem.Name = "txtItem"
        Me.txtItem.Size = New System.Drawing.Size(168, 13)
        Me.txtItem.TabIndex = 58
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(2, 130)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(122, 13)
        Me.lblName.TabIndex = 53
        Me.lblName.Text = "OPERATOR NAME :"
        '
        'lblOperatorid
        '
        Me.lblOperatorid.AutoSize = True
        Me.lblOperatorid.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperatorid.Location = New System.Drawing.Point(2, 105)
        Me.lblOperatorid.Name = "lblOperatorid"
        Me.lblOperatorid.Size = New System.Drawing.Size(100, 13)
        Me.lblOperatorid.TabIndex = 52
        Me.lblOperatorid.Text = "OPERATOR ID :"
        '
        'lblMachineId
        '
        Me.lblMachineId.AutoSize = True
        Me.lblMachineId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMachineId.Location = New System.Drawing.Point(2, 79)
        Me.lblMachineId.Name = "lblMachineId"
        Me.lblMachineId.Size = New System.Drawing.Size(88, 13)
        Me.lblMachineId.TabIndex = 51
        Me.lblMachineId.Text = "MACHINE ID :"
        '
        'lblOperationno
        '
        Me.lblOperationno.AutoSize = True
        Me.lblOperationno.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperationno.Location = New System.Drawing.Point(3, 29)
        Me.lblOperationno.Name = "lblOperationno"
        Me.lblOperationno.Size = New System.Drawing.Size(113, 13)
        Me.lblOperationno.TabIndex = 44
        Me.lblOperationno.Text = "OPERATION NO. :"
        '
        'lblWorkcenter
        '
        Me.lblWorkcenter.AutoSize = True
        Me.lblWorkcenter.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkcenter.Location = New System.Drawing.Point(3, 55)
        Me.lblWorkcenter.Name = "lblWorkcenter"
        Me.lblWorkcenter.Size = New System.Drawing.Size(107, 13)
        Me.lblWorkcenter.TabIndex = 45
        Me.lblWorkcenter.Text = "WORK CENTER :"
        '
        'lblItem
        '
        Me.lblItem.AutoSize = True
        Me.lblItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItem.Location = New System.Drawing.Point(3, 3)
        Me.lblItem.Name = "lblItem"
        Me.lblItem.Size = New System.Drawing.Size(45, 13)
        Me.lblItem.TabIndex = 42
        Me.lblItem.Text = "ITEM :"
        '
        'lblJobno
        '
        Me.lblJobno.AutoSize = True
        Me.lblJobno.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobno.ForeColor = System.Drawing.Color.Black
        Me.lblJobno.Location = New System.Drawing.Point(82, 35)
        Me.lblJobno.Name = "lblJobno"
        Me.lblJobno.Size = New System.Drawing.Size(65, 16)
        Me.lblJobno.TabIndex = 13
        Me.lblJobno.Text = "JOB NO :"
        '
        'frmSPEntry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(238, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(903, 633)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmSPEntry"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.pnlnoofOp.ResumeLayout(False)
        Me.pnlnoofOp.PerformLayout()
        Me.pnlcom.ResumeLayout(False)
        Me.pnlcom.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lstvData As System.Windows.Forms.ListView
    Friend WithEvents pnlnoofOp As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtKeynoofOp As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtKeyRejqty As System.Windows.Forms.TextBox
    Friend WithEvents pnlcom As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtKeyComqty As System.Windows.Forms.TextBox
    Friend WithEvents txtOPid As System.Windows.Forms.Label
    Friend WithEvents txtID As System.Windows.Forms.Label
    Friend WithEvents fltStartDate As System.Windows.Forms.Label
    Friend WithEvents txtParent As System.Windows.Forms.Label
    Friend WithEvents txtsuf As System.Windows.Forms.Label
    Friend WithEvents lbl As System.Windows.Forms.Label
    Friend WithEvents txtJobno As System.Windows.Forms.Label
    Friend WithEvents txtAction As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtComQty As System.Windows.Forms.Label
    Friend WithEvents lblRlQ As System.Windows.Forms.Label
    Friend WithEvents txtEdate As System.Windows.Forms.Label
    Friend WithEvents txtReQ As System.Windows.Forms.Label
    Friend WithEvents txtRlQ As System.Windows.Forms.Label
    Friend WithEvents lblReQ As System.Windows.Forms.Label
    Friend WithEvents txtStartDate As System.Windows.Forms.Label
    Friend WithEvents txtRejQ As System.Windows.Forms.Label
    Friend WithEvents lblEdate As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents lblComqty As System.Windows.Forms.Label
    Friend WithEvents lblRejQ As System.Windows.Forms.Label
    Friend WithEvents lblMsgbox As System.Windows.Forms.Label
    Friend WithEvents lblAction As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtnoofOP As System.Windows.Forms.Label
    Friend WithEvents lblnoofOP As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.Label
    Friend WithEvents txtOperatorid As System.Windows.Forms.Label
    Friend WithEvents txtMachineId As System.Windows.Forms.Label
    Friend WithEvents txtWorkcenter As System.Windows.Forms.Label
    Friend WithEvents txtOperationno As System.Windows.Forms.Label
    Friend WithEvents txtItem As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblOperatorid As System.Windows.Forms.Label
    Friend WithEvents lblMachineId As System.Windows.Forms.Label
    Friend WithEvents lblOperationno As System.Windows.Forms.Label
    Friend WithEvents lblWorkcenter As System.Windows.Forms.Label
    Friend WithEvents lblItem As System.Windows.Forms.Label
    Friend WithEvents lblJobno As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents lblTot As System.Windows.Forms.Label
    Friend WithEvents txtTot As System.Windows.Forms.Label
    Friend WithEvents lstv As System.Windows.Forms.ListView
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtJonno As System.Windows.Forms.TextBox
    Friend WithEvents butPre As System.Windows.Forms.Button
    Friend WithEvents butNext As System.Windows.Forms.Button
End Class
