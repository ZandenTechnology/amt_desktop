<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmsupRejected
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmsupRejected))
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.txtRecQty = New System.Windows.Forms.Label
        Me.txtRejQty = New System.Windows.Forms.Label
        Me.txtcomQty = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.cmbRejectedCode = New System.Windows.Forms.ComboBox
        Me.butRemove = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.txtRejectedDesc = New System.Windows.Forms.TextBox
        Me.txtKeyReject = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.lstv = New System.Windows.Forms.ListView
        Me.txttot = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Panel7 = New System.Windows.Forms.Panel
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.lblWc = New System.Windows.Forms.Label
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.LemonChiffon
        Me.Panel2.Controls.Add(Me.txtRecQty)
        Me.Panel2.Controls.Add(Me.txtRejQty)
        Me.Panel2.Controls.Add(Me.txtcomQty)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Location = New System.Drawing.Point(10, 7)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(517, 51)
        Me.Panel2.TabIndex = 12
        '
        'txtRecQty
        '
        Me.txtRecQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRecQty.Location = New System.Drawing.Point(122, 8)
        Me.txtRecQty.Name = "txtRecQty"
        Me.txtRecQty.Size = New System.Drawing.Size(111, 13)
        Me.txtRecQty.TabIndex = 5
        '
        'txtRejQty
        '
        Me.txtRejQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRejQty.Location = New System.Drawing.Point(124, 30)
        Me.txtRejQty.Name = "txtRejQty"
        Me.txtRejQty.Size = New System.Drawing.Size(111, 13)
        Me.txtRejQty.TabIndex = 4
        '
        'txtcomQty
        '
        Me.txtcomQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcomQty.Location = New System.Drawing.Point(371, 8)
        Me.txtcomQty.Name = "txtcomQty"
        Me.txtcomQty.Size = New System.Drawing.Size(111, 13)
        Me.txtcomQty.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(10, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(106, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "RECEIVED QTY :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(10, 30)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(108, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "REJECTED QTY :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(249, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(123, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "COMPLETED QTY  :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.PaleGoldenrod
        Me.Panel1.Controls.Add(Me.cmbRejectedCode)
        Me.Panel1.Controls.Add(Me.butRemove)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.txtRejectedDesc)
        Me.Panel1.Controls.Add(Me.txtKeyReject)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(10, 64)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(517, 81)
        Me.Panel1.TabIndex = 13
        '
        'cmbRejectedCode
        '
        Me.cmbRejectedCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbRejectedCode.FormattingEnabled = True
        Me.cmbRejectedCode.Location = New System.Drawing.Point(127, 3)
        Me.cmbRejectedCode.Name = "cmbRejectedCode"
        Me.cmbRejectedCode.Size = New System.Drawing.Size(121, 21)
        Me.cmbRejectedCode.TabIndex = 0
        '
        'butRemove
        '
        Me.butRemove.BackColor = System.Drawing.Color.WhiteSmoke
        Me.butRemove.Location = New System.Drawing.Point(454, 28)
        Me.butRemove.Name = "butRemove"
        Me.butRemove.Size = New System.Drawing.Size(60, 21)
        Me.butRemove.TabIndex = 5
        Me.butRemove.Text = "Remove"
        Me.butRemove.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button1.Location = New System.Drawing.Point(412, 28)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(41, 21)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Add"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'txtRejectedDesc
        '
        Me.txtRejectedDesc.BackColor = System.Drawing.Color.White
        Me.txtRejectedDesc.Location = New System.Drawing.Point(127, 28)
        Me.txtRejectedDesc.MaxLength = 255
        Me.txtRejectedDesc.Multiline = True
        Me.txtRejectedDesc.Name = "txtRejectedDesc"
        Me.txtRejectedDesc.ReadOnly = True
        Me.txtRejectedDesc.Size = New System.Drawing.Size(281, 39)
        Me.txtRejectedDesc.TabIndex = 2
        '
        'txtKeyReject
        '
        Me.txtKeyReject.Location = New System.Drawing.Point(382, 3)
        Me.txtKeyReject.Name = "txtKeyReject"
        Me.txtKeyReject.Size = New System.Drawing.Size(100, 20)
        Me.txtKeyReject.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(276, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(108, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "REJECTED QTY :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(10, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(99, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "DESCRIPTION :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(10, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(117, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "REJECTED CODE :"
        '
        'lstv
        '
        Me.lstv.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(9, 151)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(518, 192)
        Me.lstv.TabIndex = 14
        Me.lstv.TabStop = False
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'txttot
        '
        Me.txttot.AutoSize = True
        Me.txttot.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txttot.Location = New System.Drawing.Point(400, 349)
        Me.txttot.Name = "txttot"
        Me.txttot.Size = New System.Drawing.Size(25, 13)
        Me.txttot.TabIndex = 16
        Me.txttot.Text = "0.0"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(242, 349)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(152, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "TOTAL REJECTED QTY :"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Transparent
        Me.Panel7.Controls.Add(Me.butCancel)
        Me.Panel7.Controls.Add(Me.butSave)
        Me.Panel7.Location = New System.Drawing.Point(23, 349)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(117, 67)
        Me.Panel7.TabIndex = 68
        '
        'butCancel
        '
        Me.butCancel.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butCancel.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCancel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butCancel.Image = CType(resources.GetObject("butCancel.Image"), System.Drawing.Image)
        Me.butCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(58, 5)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(48, 59)
        Me.butCancel.TabIndex = 7
        Me.butCancel.Text = "Cancel"
        Me.butCancel.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butCancel.UseVisualStyleBackColor = False
        '
        'butSave
        '
        Me.butSave.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butSave.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butSave.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSave.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butSave.Image = CType(resources.GetObject("butSave.Image"), System.Drawing.Image)
        Me.butSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(8, 5)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(48, 59)
        Me.butSave.TabIndex = 6
        Me.butSave.Text = "Exit"
        Me.butSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.butSave.UseVisualStyleBackColor = False
        '
        'lblWc
        '
        Me.lblWc.AutoSize = True
        Me.lblWc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWc.Location = New System.Drawing.Point(162, 395)
        Me.lblWc.Name = "lblWc"
        Me.lblWc.Size = New System.Drawing.Size(0, 13)
        Me.lblWc.TabIndex = 69
        Me.lblWc.Visible = False
        '
        'frmsupRejected
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(539, 426)
        Me.Controls.Add(Me.lblWc)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.txttot)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lstv)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmsupRejected"
        Me.Text = "Rejected Quantity"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtRecQty As System.Windows.Forms.Label
    Friend WithEvents txtRejQty As System.Windows.Forms.Label
    Friend WithEvents txtcomQty As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtKeyReject As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lstv As System.Windows.Forms.ListView
    Friend WithEvents txttot As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents txtRejectedDesc As System.Windows.Forms.TextBox
    Friend WithEvents lblWc As System.Windows.Forms.Label
    Friend WithEvents butRemove As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents cmbRejectedCode As System.Windows.Forms.ComboBox
End Class
