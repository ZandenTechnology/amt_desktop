<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MDINEW
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MDINEW))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.MMMaster = New System.Windows.Forms.ToolStripMenuItem
        Me.UMUser = New System.Windows.Forms.ToolStripMenuItem
        Me.UMOP = New System.Windows.Forms.ToolStripMenuItem
        Me.UMResourceGroup = New System.Windows.Forms.ToolStripMenuItem
        Me.UMWRK = New System.Windows.Forms.ToolStripMenuItem
        Me.UMItem = New System.Windows.Forms.ToolStripMenuItem
        Me.UMMacheine = New System.Windows.Forms.ToolStripMenuItem
        Me.UMRejDESC = New System.Windows.Forms.ToolStripMenuItem
        Me.UMACT = New System.Windows.Forms.ToolStripMenuItem
        Me.menuICustomer = New System.Windows.Forms.ToolStripMenuItem
        Me.MMGROUP = New System.Windows.Forms.ToolStripMenuItem
        Me.UCWRK_Group = New System.Windows.Forms.ToolStripMenuItem
        Me.UM_Machine_Group = New System.Windows.Forms.ToolStripMenuItem
        Me.UMREJECT_Group = New System.Windows.Forms.ToolStripMenuItem
        Me.DOWNLOADToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DownloadJToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ViewJobToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ExportExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ExportCompletedOperationsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.mnudAll = New System.Windows.Forms.ToolStripMenuItem
        Me.TestToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DataProblemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.JobCloseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.WIPDATAENTRYToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DataEntryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.JobVerificationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.JobProcessToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.InventoryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.toolInvenTrans = New System.Windows.Forms.ToolStripMenuItem
        Me.StockCheckToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DeliveryOrderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.REPORTToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuCompletedTransaction = New System.Windows.Forms.ToolStripMenuItem
        Me.mnIItemReport = New System.Windows.Forms.ToolStripMenuItem
        Me.WIPWorkCenterYieldToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.WIPFinalYieldToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.WIPFailureAnalysisToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ItemReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.WIPByToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.ActualByItemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SummaryByItemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SummaryByOperatorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ResourceGroupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SummaryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TransferReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EXITToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MnDataEdit = New System.Windows.Forms.ToolStripMenuItem
        Me.JobDeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.InsertOPToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DeleteDuplicateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Pwait = New System.Windows.Forms.Panel
        Me.Label5 = New System.Windows.Forms.Label
        Me.PictureBox5 = New System.Windows.Forms.PictureBox
        Me.P1 = New System.Windows.Forms.Panel
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Label3 = New System.Windows.Forms.Label
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ImportListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Pwait.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MMMaster, Me.MMGROUP, Me.DOWNLOADToolStripMenuItem, Me.WIPDATAENTRYToolStripMenuItem, Me.InventoryToolStripMenuItem, Me.REPORTToolStripMenuItem, Me.EXITToolStripMenuItem, Me.MnDataEdit})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.ShowItemToolTips = True
        Me.MenuStrip1.Size = New System.Drawing.Size(1232, 24)
        Me.MenuStrip1.TabIndex = 11
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'MMMaster
        '
        Me.MMMaster.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UMUser, Me.UMOP, Me.UMResourceGroup, Me.UMWRK, Me.UMItem, Me.UMMacheine, Me.UMRejDESC, Me.UMACT, Me.menuICustomer})
        Me.MMMaster.Name = "MMMaster"
        Me.MMMaster.ShortcutKeyDisplayString = "Alt+M"
        Me.MMMaster.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.M), System.Windows.Forms.Keys)
        Me.MMMaster.Size = New System.Drawing.Size(52, 20)
        Me.MMMaster.Text = "&Master"
        '
        'UMUser
        '
        Me.UMUser.Name = "UMUser"
        Me.UMUser.ShortcutKeyDisplayString = "Ctrl+Alt+U"
        Me.UMUser.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.U), System.Windows.Forms.Keys)
        Me.UMUser.Size = New System.Drawing.Size(227, 22)
        Me.UMUser.Text = "&User"
        '
        'UMOP
        '
        Me.UMOP.Name = "UMOP"
        Me.UMOP.ShortcutKeyDisplayString = "Ctrl+Alt+O"
        Me.UMOP.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.UMOP.Size = New System.Drawing.Size(227, 22)
        Me.UMOP.Text = "&Operator"
        '
        'UMResourceGroup
        '
        Me.UMResourceGroup.Name = "UMResourceGroup"
        Me.UMResourceGroup.ShortcutKeyDisplayString = "Ctrl+Alt+R"
        Me.UMResourceGroup.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.R), System.Windows.Forms.Keys)
        Me.UMResourceGroup.Size = New System.Drawing.Size(227, 22)
        Me.UMResourceGroup.Text = "&Resource Group"
        '
        'UMWRK
        '
        Me.UMWRK.Name = "UMWRK"
        Me.UMWRK.ShortcutKeyDisplayString = "Ctrl+Alt+W"
        Me.UMWRK.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.UMWRK.Size = New System.Drawing.Size(227, 22)
        Me.UMWRK.Text = "&Work Center"
        '
        'UMItem
        '
        Me.UMItem.Name = "UMItem"
        Me.UMItem.ShortcutKeyDisplayString = "Ctrl+Alt+I"
        Me.UMItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me.UMItem.Size = New System.Drawing.Size(227, 22)
        Me.UMItem.Text = "&Item"
        '
        'UMMacheine
        '
        Me.UMMacheine.Name = "UMMacheine"
        Me.UMMacheine.ShortcutKeyDisplayString = "Ctrl+Alt+M"
        Me.UMMacheine.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.M), System.Windows.Forms.Keys)
        Me.UMMacheine.Size = New System.Drawing.Size(227, 22)
        Me.UMMacheine.Text = "&Machine"
        '
        'UMRejDESC
        '
        Me.UMRejDESC.Name = "UMRejDESC"
        Me.UMRejDESC.ShortcutKeyDisplayString = "Ctrl+Alt+E"
        Me.UMRejDESC.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.UMRejDESC.Size = New System.Drawing.Size(227, 22)
        Me.UMRejDESC.Text = "R&ejected Description"
        '
        'UMACT
        '
        Me.UMACT.Name = "UMACT"
        Me.UMACT.ShortcutKeyDisplayString = "Ctrl+Alt+A"
        Me.UMACT.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.UMACT.Size = New System.Drawing.Size(227, 22)
        Me.UMACT.Text = "&Action Control"
        '
        'menuICustomer
        '
        Me.menuICustomer.Name = "menuICustomer"
        Me.menuICustomer.Size = New System.Drawing.Size(227, 22)
        Me.menuICustomer.Text = "Customer"
        '
        'MMGROUP
        '
        Me.MMGROUP.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UCWRK_Group, Me.UM_Machine_Group, Me.UMREJECT_Group})
        Me.MMGROUP.Name = "MMGROUP"
        Me.MMGROUP.ShortcutKeyDisplayString = "Alt+G"
        Me.MMGROUP.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.MMGROUP.Size = New System.Drawing.Size(63, 20)
        Me.MMGROUP.Text = "Gro&uping"
        '
        'UCWRK_Group
        '
        Me.UCWRK_Group.Name = "UCWRK_Group"
        Me.UCWRK_Group.ShortcutKeyDisplayString = "Ctrl+Alt+C"
        Me.UCWRK_Group.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.UCWRK_Group.Size = New System.Drawing.Size(236, 22)
        Me.UCWRK_Group.Text = "&Work Center Grouping"
        '
        'UM_Machine_Group
        '
        Me.UM_Machine_Group.Name = "UM_Machine_Group"
        Me.UM_Machine_Group.ShortcutKeyDisplayString = "Ctrl+Alt+N"
        Me.UM_Machine_Group.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.UM_Machine_Group.Size = New System.Drawing.Size(236, 22)
        Me.UM_Machine_Group.Text = "Machi&ne Grouping"
        '
        'UMREJECT_Group
        '
        Me.UMREJECT_Group.Name = "UMREJECT_Group"
        Me.UMREJECT_Group.ShortcutKeyDisplayString = "Ctrl+Alt+J"
        Me.UMREJECT_Group.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.J), System.Windows.Forms.Keys)
        Me.UMREJECT_Group.Size = New System.Drawing.Size(236, 22)
        Me.UMREJECT_Group.Text = "Re&jected Grouping"
        '
        'DOWNLOADToolStripMenuItem
        '
        Me.DOWNLOADToolStripMenuItem.CheckOnClick = True
        Me.DOWNLOADToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DownloadJToolStripMenuItem, Me.ViewJobToolStripMenuItem, Me.ExportExcelToolStripMenuItem, Me.ExportCompletedOperationsToolStripMenuItem, Me.mnudAll, Me.TestToolStripMenuItem, Me.DataProblemToolStripMenuItem, Me.JobCloseToolStripMenuItem})
        Me.DOWNLOADToolStripMenuItem.Name = "DOWNLOADToolStripMenuItem"
        Me.DOWNLOADToolStripMenuItem.ShortcutKeyDisplayString = "Alt+I"
        Me.DOWNLOADToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me.DOWNLOADToolStripMenuItem.Size = New System.Drawing.Size(38, 20)
        Me.DOWNLOADToolStripMenuItem.Text = "ERP"
        '
        'DownloadJToolStripMenuItem
        '
        Me.DownloadJToolStripMenuItem.Name = "DownloadJToolStripMenuItem"
        Me.DownloadJToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Alt+P"
        Me.DownloadJToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.DownloadJToolStripMenuItem.Size = New System.Drawing.Size(269, 22)
        Me.DownloadJToolStripMenuItem.Text = "Im&port Job Orders from ERP"
        '
        'ViewJobToolStripMenuItem
        '
        Me.ViewJobToolStripMenuItem.Name = "ViewJobToolStripMenuItem"
        Me.ViewJobToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Alt+V"
        Me.ViewJobToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.V), System.Windows.Forms.Keys)
        Me.ViewJobToolStripMenuItem.Size = New System.Drawing.Size(269, 22)
        Me.ViewJobToolStripMenuItem.Text = "&View Imported Job Orders"
        '
        'ExportExcelToolStripMenuItem
        '
        Me.ExportExcelToolStripMenuItem.Name = "ExportExcelToolStripMenuItem"
        Me.ExportExcelToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Alt+X"
        Me.ExportExcelToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.ExportExcelToolStripMenuItem.Size = New System.Drawing.Size(269, 22)
        Me.ExportExcelToolStripMenuItem.Text = "E&xport Completed Operations"
        '
        'ExportCompletedOperationsToolStripMenuItem
        '
        Me.ExportCompletedOperationsToolStripMenuItem.Name = "ExportCompletedOperationsToolStripMenuItem"
        Me.ExportCompletedOperationsToolStripMenuItem.Size = New System.Drawing.Size(269, 22)
        Me.ExportCompletedOperationsToolStripMenuItem.Text = "E&xport Completed Operations(Test)"
        Me.ExportCompletedOperationsToolStripMenuItem.Visible = False
        '
        'mnudAll
        '
        Me.mnudAll.Name = "mnudAll"
        Me.mnudAll.Size = New System.Drawing.Size(269, 22)
        Me.mnudAll.Text = "Download All"
        Me.mnudAll.Visible = False
        '
        'TestToolStripMenuItem
        '
        Me.TestToolStripMenuItem.Name = "TestToolStripMenuItem"
        Me.TestToolStripMenuItem.Size = New System.Drawing.Size(269, 22)
        Me.TestToolStripMenuItem.Text = "Test"
        '
        'DataProblemToolStripMenuItem
        '
        Me.DataProblemToolStripMenuItem.Name = "DataProblemToolStripMenuItem"
        Me.DataProblemToolStripMenuItem.Size = New System.Drawing.Size(269, 22)
        Me.DataProblemToolStripMenuItem.Text = "Data Problem"
        Me.DataProblemToolStripMenuItem.Visible = False
        '
        'JobCloseToolStripMenuItem
        '
        Me.JobCloseToolStripMenuItem.Name = "JobCloseToolStripMenuItem"
        Me.JobCloseToolStripMenuItem.Size = New System.Drawing.Size(269, 22)
        Me.JobCloseToolStripMenuItem.Text = "Job Close"
        Me.JobCloseToolStripMenuItem.Visible = False
        '
        'WIPDATAENTRYToolStripMenuItem
        '
        Me.WIPDATAENTRYToolStripMenuItem.CheckOnClick = True
        Me.WIPDATAENTRYToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DataEntryToolStripMenuItem, Me.JobVerificationToolStripMenuItem, Me.JobProcessToolStripMenuItem})
        Me.WIPDATAENTRYToolStripMenuItem.Name = "WIPDATAENTRYToolStripMenuItem"
        Me.WIPDATAENTRYToolStripMenuItem.ShortcutKeyDisplayString = "Alt+W"
        Me.WIPDATAENTRYToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.WIPDATAENTRYToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.WIPDATAENTRYToolStripMenuItem.Text = "&WIP"
        '
        'DataEntryToolStripMenuItem
        '
        Me.DataEntryToolStripMenuItem.Name = "DataEntryToolStripMenuItem"
        Me.DataEntryToolStripMenuItem.ShortcutKeyDisplayString = "Alt+D"
        Me.DataEntryToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.DataEntryToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.DataEntryToolStripMenuItem.Text = "&Data Entry"
        '
        'JobVerificationToolStripMenuItem
        '
        Me.JobVerificationToolStripMenuItem.Name = "JobVerificationToolStripMenuItem"
        Me.JobVerificationToolStripMenuItem.ShortcutKeyDisplayString = "Alt+B"
        Me.JobVerificationToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.B), System.Windows.Forms.Keys)
        Me.JobVerificationToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.JobVerificationToolStripMenuItem.Text = "Jo&b Verification"
        '
        'JobProcessToolStripMenuItem
        '
        Me.JobProcessToolStripMenuItem.Name = "JobProcessToolStripMenuItem"
        Me.JobProcessToolStripMenuItem.ShortcutKeyDisplayString = "Alt+S"
        Me.JobProcessToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.JobProcessToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.JobProcessToolStripMenuItem.Text = "Job Proce&ss"
        '
        'InventoryToolStripMenuItem
        '
        Me.InventoryToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolInvenTrans, Me.StockCheckToolStripMenuItem, Me.DeliveryOrderToolStripMenuItem, Me.ImportListToolStripMenuItem})
        Me.InventoryToolStripMenuItem.Name = "InventoryToolStripMenuItem"
        Me.InventoryToolStripMenuItem.Size = New System.Drawing.Size(64, 20)
        Me.InventoryToolStripMenuItem.Text = "Inventory"
        '
        'toolInvenTrans
        '
        Me.toolInvenTrans.Name = "toolInvenTrans"
        Me.toolInvenTrans.Size = New System.Drawing.Size(196, 22)
        Me.toolInvenTrans.Text = "Job Transfer to Invenory "
        '
        'StockCheckToolStripMenuItem
        '
        Me.StockCheckToolStripMenuItem.Name = "StockCheckToolStripMenuItem"
        Me.StockCheckToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.StockCheckToolStripMenuItem.Text = "Stock Check"
        '
        'DeliveryOrderToolStripMenuItem
        '
        Me.DeliveryOrderToolStripMenuItem.Name = "DeliveryOrderToolStripMenuItem"
        Me.DeliveryOrderToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.DeliveryOrderToolStripMenuItem.Text = "Delivery Order"
        '
        'REPORTToolStripMenuItem
        '
        Me.REPORTToolStripMenuItem.CheckOnClick = True
        Me.REPORTToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCompletedTransaction, Me.mnIItemReport, Me.WIPWorkCenterYieldToolStripMenuItem, Me.WIPFinalYieldToolStripMenuItem, Me.WIPFailureAnalysisToolStripMenuItem, Me.ItemReportToolStripMenuItem, Me.WIPByToolStripMenuItem, Me.ToolStripSeparator1, Me.ToolStripMenuItem1, Me.TransferReportToolStripMenuItem})
        Me.REPORTToolStripMenuItem.Name = "REPORTToolStripMenuItem"
        Me.REPORTToolStripMenuItem.ShortcutKeyDisplayString = "Alt+R"
        Me.REPORTToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.R), System.Windows.Forms.Keys)
        Me.REPORTToolStripMenuItem.Size = New System.Drawing.Size(51, 20)
        Me.REPORTToolStripMenuItem.Text = "&Report"
        '
        'mnuCompletedTransaction
        '
        Me.mnuCompletedTransaction.Name = "mnuCompletedTransaction"
        Me.mnuCompletedTransaction.ShortcutKeyDisplayString = "Ctrl+Alt+G"
        Me.mnuCompletedTransaction.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.mnuCompletedTransaction.Size = New System.Drawing.Size(245, 22)
        Me.mnuCompletedTransaction.Text = "CO by Resource &Group"
        '
        'mnIItemReport
        '
        Me.mnIItemReport.Name = "mnIItemReport"
        Me.mnIItemReport.ShortcutKeyDisplayString = "Ctrl+Alt+T"
        Me.mnIItemReport.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.T), System.Windows.Forms.Keys)
        Me.mnIItemReport.Size = New System.Drawing.Size(245, 22)
        Me.mnIItemReport.Text = "CO by I&tem Report"
        '
        'WIPWorkCenterYieldToolStripMenuItem
        '
        Me.WIPWorkCenterYieldToolStripMenuItem.Name = "WIPWorkCenterYieldToolStripMenuItem"
        Me.WIPWorkCenterYieldToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Alt+K"
        Me.WIPWorkCenterYieldToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.K), System.Windows.Forms.Keys)
        Me.WIPWorkCenterYieldToolStripMenuItem.Size = New System.Drawing.Size(245, 22)
        Me.WIPWorkCenterYieldToolStripMenuItem.Text = "Wor&k Center Yield"
        '
        'WIPFinalYieldToolStripMenuItem
        '
        Me.WIPFinalYieldToolStripMenuItem.Name = "WIPFinalYieldToolStripMenuItem"
        Me.WIPFinalYieldToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Alt+Y"
        Me.WIPFinalYieldToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.Y), System.Windows.Forms.Keys)
        Me.WIPFinalYieldToolStripMenuItem.Size = New System.Drawing.Size(245, 22)
        Me.WIPFinalYieldToolStripMenuItem.Text = "Final &Yield"
        '
        'WIPFailureAnalysisToolStripMenuItem
        '
        Me.WIPFailureAnalysisToolStripMenuItem.Name = "WIPFailureAnalysisToolStripMenuItem"
        Me.WIPFailureAnalysisToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Alt+L"
        Me.WIPFailureAnalysisToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.L), System.Windows.Forms.Keys)
        Me.WIPFailureAnalysisToolStripMenuItem.Size = New System.Drawing.Size(245, 22)
        Me.WIPFailureAnalysisToolStripMenuItem.Text = "Fai&lure Analysis"
        '
        'ItemReportToolStripMenuItem
        '
        Me.ItemReportToolStripMenuItem.Name = "ItemReportToolStripMenuItem"
        Me.ItemReportToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Alt+B"
        Me.ItemReportToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.B), System.Windows.Forms.Keys)
        Me.ItemReportToolStripMenuItem.Size = New System.Drawing.Size(245, 22)
        Me.ItemReportToolStripMenuItem.Text = "WIP by Item Report"
        '
        'WIPByToolStripMenuItem
        '
        Me.WIPByToolStripMenuItem.Name = "WIPByToolStripMenuItem"
        Me.WIPByToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Alt+S"
        Me.WIPByToolStripMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Alt) _
                    Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.WIPByToolStripMenuItem.Size = New System.Drawing.Size(245, 22)
        Me.WIPByToolStripMenuItem.Text = "WIP by Re&source Group"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(242, 6)
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ActualByItemToolStripMenuItem, Me.SummaryByItemToolStripMenuItem, Me.SummaryByOperatorToolStripMenuItem, Me.ResourceGroupToolStripMenuItem, Me.SummaryToolStripMenuItem})
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(245, 22)
        Me.ToolStripMenuItem1.Text = "Actual Cycle"
        '
        'ActualByItemToolStripMenuItem
        '
        Me.ActualByItemToolStripMenuItem.Name = "ActualByItemToolStripMenuItem"
        Me.ActualByItemToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.ActualByItemToolStripMenuItem.Text = "By Item - Details"
        '
        'SummaryByItemToolStripMenuItem
        '
        Me.SummaryByItemToolStripMenuItem.Name = "SummaryByItemToolStripMenuItem"
        Me.SummaryByItemToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.SummaryByItemToolStripMenuItem.Text = "By Item - Summary"
        '
        'SummaryByOperatorToolStripMenuItem
        '
        Me.SummaryByOperatorToolStripMenuItem.Name = "SummaryByOperatorToolStripMenuItem"
        Me.SummaryByOperatorToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.SummaryByOperatorToolStripMenuItem.Text = "By Operators"
        '
        'ResourceGroupToolStripMenuItem
        '
        Me.ResourceGroupToolStripMenuItem.Name = "ResourceGroupToolStripMenuItem"
        Me.ResourceGroupToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.ResourceGroupToolStripMenuItem.Text = "By Resource Group - Details (Work Centers / Machines)"
        '
        'SummaryToolStripMenuItem
        '
        Me.SummaryToolStripMenuItem.Name = "SummaryToolStripMenuItem"
        Me.SummaryToolStripMenuItem.Size = New System.Drawing.Size(344, 22)
        Me.SummaryToolStripMenuItem.Text = "By Resource Group - Summary by Items "
        '
        'TransferReportToolStripMenuItem
        '
        Me.TransferReportToolStripMenuItem.Name = "TransferReportToolStripMenuItem"
        Me.TransferReportToolStripMenuItem.Size = New System.Drawing.Size(245, 22)
        Me.TransferReportToolStripMenuItem.Text = "Exception Report"
        '
        'EXITToolStripMenuItem
        '
        Me.EXITToolStripMenuItem.CheckOnClick = True
        Me.EXITToolStripMenuItem.Name = "EXITToolStripMenuItem"
        Me.EXITToolStripMenuItem.ShortcutKeyDisplayString = "Alt+X"
        Me.EXITToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.EXITToolStripMenuItem.Size = New System.Drawing.Size(40, 20)
        Me.EXITToolStripMenuItem.Text = "E&XIT"
        '
        'MnDataEdit
        '
        Me.MnDataEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.JobDeleteToolStripMenuItem, Me.InsertOPToolStripMenuItem, Me.DeleteDuplicateToolStripMenuItem})
        Me.MnDataEdit.Name = "MnDataEdit"
        Me.MnDataEdit.Size = New System.Drawing.Size(61, 20)
        Me.MnDataEdit.Text = "Data Edit"
        '
        'JobDeleteToolStripMenuItem
        '
        Me.JobDeleteToolStripMenuItem.Name = "JobDeleteToolStripMenuItem"
        Me.JobDeleteToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.JobDeleteToolStripMenuItem.Text = "Delete Job Order"
        '
        'InsertOPToolStripMenuItem
        '
        Me.InsertOPToolStripMenuItem.Name = "InsertOPToolStripMenuItem"
        Me.InsertOPToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.InsertOPToolStripMenuItem.Text = "Insert Tans(OP)"
        '
        'DeleteDuplicateToolStripMenuItem
        '
        Me.DeleteDuplicateToolStripMenuItem.Name = "DeleteDuplicateToolStripMenuItem"
        Me.DeleteDuplicateToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.DeleteDuplicateToolStripMenuItem.Text = "Job Checking"
        Me.DeleteDuplicateToolStripMenuItem.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 24)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1232, 722)
        Me.Panel1.TabIndex = 12
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Pwait)
        Me.Panel2.Controls.Add(Me.P1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1232, 722)
        Me.Panel2.TabIndex = 1
        '
        'Pwait
        '
        Me.Pwait.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Pwait.BackColor = System.Drawing.Color.Gray
        Me.Pwait.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Pwait.Controls.Add(Me.Label5)
        Me.Pwait.Controls.Add(Me.PictureBox5)
        Me.Pwait.Location = New System.Drawing.Point(483, 269)
        Me.Pwait.Name = "Pwait"
        Me.Pwait.Size = New System.Drawing.Size(336, 60)
        Me.Pwait.TabIndex = 1
        Me.Pwait.Visible = False
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label5.BackColor = System.Drawing.Color.LightGray
        Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label5.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(72, 12)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(227, 31)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Wait few seconds....."
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(7, 3)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(65, 52)
        Me.PictureBox5.TabIndex = 2
        Me.PictureBox5.TabStop = False
        '
        'P1
        '
        Me.P1.BackColor = System.Drawing.Color.Ivory
        Me.P1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.P1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.P1.Location = New System.Drawing.Point(0, 0)
        Me.P1.Name = "P1"
        Me.P1.Size = New System.Drawing.Size(1232, 722)
        Me.P1.TabIndex = 2
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(0, 720)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1232, 26)
        Me.Panel3.TabIndex = 13
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(463, 4)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(358, 14)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Copyright � 2009 ZanDen Singapore Pte Ltd. All Rights Reserved"
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'ImportListToolStripMenuItem
        '
        Me.ImportListToolStripMenuItem.Name = "ImportListToolStripMenuItem"
        Me.ImportListToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.ImportListToolStripMenuItem.Text = "Import List"
        '
        'MDINEW
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1232, 746)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "MDINEW"
        Me.Text = "eWIP"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Pwait.ResumeLayout(False)
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Pwait As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents DOWNLOADToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WIPDATAENTRYToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents REPORTToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EXITToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents P1 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DownloadJToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewJobToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataEntryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JobVerificationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JobProcessToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExportExcelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCompletedTransaction As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnIItemReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WIPFinalYieldToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WIPWorkCenterYieldToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WIPFailureAnalysisToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents MMMaster As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MMGROUP As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UMUser As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UMOP As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UMResourceGroup As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UMWRK As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UMItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UMMacheine As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UMRejDESC As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UMACT As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UCWRK_Group As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UM_Machine_Group As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UMREJECT_Group As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnudAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TestToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ItemReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResourceGroupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SummaryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SummaryByOperatorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualByItemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SummaryByItemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataProblemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WIPByToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExportCompletedOperationsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JobCloseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MnDataEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JobDeleteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InsertOPToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteDuplicateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TransferReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InventoryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolInvenTrans As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockCheckToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuICustomer As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeliveryOrderToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImportListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
