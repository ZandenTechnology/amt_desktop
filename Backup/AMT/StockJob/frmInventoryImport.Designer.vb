<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInventoryImport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.butEXIT = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.ofdBox = New System.Windows.Forms.OpenFileDialog
        Me.txtFile = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.ButBrows = New System.Windows.Forms.Button
        Me.lstv = New System.Windows.Forms.ListView
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Location = New System.Drawing.Point(7, -2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(837, 493)
        Me.Panel1.TabIndex = 1
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BackColor = System.Drawing.Color.Khaki
        Me.Panel3.Controls.Add(Me.ButBrows)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.txtFile)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Location = New System.Drawing.Point(3, 2)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(834, 36)
        Me.Panel3.TabIndex = 38
        '
        'butEXIT
        '
        Me.butEXIT.BackColor = System.Drawing.Color.Gray
        Me.butEXIT.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butEXIT.ForeColor = System.Drawing.Color.White
        Me.butEXIT.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butEXIT.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butEXIT.Location = New System.Drawing.Point(129, 4)
        Me.butEXIT.Name = "butEXIT"
        Me.butEXIT.Size = New System.Drawing.Size(57, 24)
        Me.butEXIT.TabIndex = 50
        Me.butEXIT.Text = "Exit"
        Me.butEXIT.UseVisualStyleBackColor = False
        '
        'butCancel
        '
        Me.butCancel.BackColor = System.Drawing.Color.Gray
        Me.butCancel.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butCancel.ForeColor = System.Drawing.Color.White
        Me.butCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(66, 4)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(57, 24)
        Me.butCancel.TabIndex = 49
        Me.butCancel.Text = "Cancel"
        Me.butCancel.UseVisualStyleBackColor = False
        '
        'butSave
        '
        Me.butSave.BackColor = System.Drawing.Color.Gray
        Me.butSave.Enabled = False
        Me.butSave.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butSave.ForeColor = System.Drawing.Color.White
        Me.butSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(3, 4)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(57, 24)
        Me.butSave.TabIndex = 47
        Me.butSave.Text = "Save"
        Me.butSave.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(770, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 13)
        Me.Label1.TabIndex = 37
        Me.Label1.Text = "IMPORTS"
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.Khaki
        Me.Panel2.Controls.Add(Me.butEXIT)
        Me.Panel2.Controls.Add(Me.butSave)
        Me.Panel2.Controls.Add(Me.butCancel)
        Me.Panel2.Location = New System.Drawing.Point(1, 40)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(834, 36)
        Me.Panel2.TabIndex = 39
        '
        'ofdBox
        '
        Me.ofdBox.FileName = "OpenFileDialog1"
        Me.ofdBox.Filter = "Excel|*.xls"
        '
        'txtFile
        '
        Me.txtFile.Location = New System.Drawing.Point(64, 8)
        Me.txtFile.Name = "txtFile"
        Me.txtFile.ReadOnly = True
        Me.txtFile.Size = New System.Drawing.Size(498, 20)
        Me.txtFile.TabIndex = 38
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 13)
        Me.Label2.TabIndex = 39
        Me.Label2.Text = "File Path"
        '
        'ButBrows
        '
        Me.ButBrows.BackColor = System.Drawing.Color.Gray
        Me.ButBrows.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.ButBrows.ForeColor = System.Drawing.Color.White
        Me.ButBrows.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ButBrows.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ButBrows.Location = New System.Drawing.Point(565, 6)
        Me.ButBrows.Name = "ButBrows"
        Me.ButBrows.Size = New System.Drawing.Size(24, 24)
        Me.ButBrows.TabIndex = 51
        Me.ButBrows.Text = "...."
        Me.ButBrows.UseVisualStyleBackColor = False
        '
        'lstv
        '
        Me.lstv.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstv.CheckBoxes = True
        Me.lstv.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(5, 6)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(820, 391)
        Me.lstv.TabIndex = 40
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'Panel4
        '
        Me.Panel4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel4.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Panel4.Controls.Add(Me.lstv)
        Me.Panel4.Location = New System.Drawing.Point(4, 82)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(830, 408)
        Me.Panel4.TabIndex = 41
        '
        'frmInventoryImport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(854, 511)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmInventoryImport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Panel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents butEXIT As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ofdBox As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtFile As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ButBrows As System.Windows.Forms.Button
    Friend WithEvents lstv As System.Windows.Forms.ListView
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
End Class
