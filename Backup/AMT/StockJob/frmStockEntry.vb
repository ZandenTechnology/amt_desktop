Imports Microsoft.VisualBasic.DateAndTime
Imports System.Data.SqlClient
Public Class frmStockEntry
    Dim CountI As Integer
    Dim clsM As New clsMain
    Dim parm As SqlParameter
    Dim i As Integer
    Public TransNo As Integer
    Sub clearText()
        txtJob.Text = ""
        txtCom.Text = ""
        txtSuffix.Text = ""
        txtItem.Text = ""


        txtEndDate.Text = ""
        txtDate.Text = ""
        cmbName.Text = ""
        'txtJobNo.Text = ""
        'lstJobNo.Items.Clear()
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        clearText()
    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub

    Private Sub frmStockEntry_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' clearText()
        GetOperator()
        dtDate.Value = DateSerial(Year(Now), Month(Now), Day(Now))
    End Sub

    Function GetOperator() As Boolean
        cmbName.Items.Clear()
        cmbName.Items.Add(New DataItem("-Select-", "-Select-"))
        Dim DSOP As New DataSet
        Dim sql As String = "select * from tbOperator"
        DSOP = clsM.GetDataset(sql, "tbMachine")
        If DSOP.Tables(0).Rows.Count > 0 Then
            For i = 0 To DSOP.Tables(0).Rows.Count - 1
                With DSOP.Tables(0).Rows(i)
                    cmbName.Items.Add(New DataItem(Trim(.Item("_operatorID")), Trim(.Item("_operatorName"))))
                End With
            Next
        End If

        cmbName.SelectedIndex = 0
        Return True
    End Function

    Private Sub dtDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtDate.ValueChanged
        txtDate.Text = Format(dtDate.Value, "dd/MM/yyyy")
    End Sub

    Private Sub ButSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
      

    End Sub






    
    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(txtJob.Text) = "" Then
            MsgBox("Enter the job no.", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtJob.Text) = "" Then
            MsgBox("Enter the job suffix", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtItem.Text) = "" Then
            MsgBox("Enter the item code", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If

        If Trim(txtItem.Text) = "" Then
            MsgBox("Enter the item code", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Dim sd As Double
        sd = clsM.CheckDate(txtDate.Text)
        If sd = 0 Then
            MsgBox("Enter the date", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If cmbName.SelectedIndex <= 0 Then
            MsgBox("Select the operator name", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "sp_Stock_Add"
                parm = .Parameters.Add("@tansnum", SqlDbType.Int)
                parm.Value = Val(TransNo)
                parm = .Parameters.Add("@emp_num", SqlDbType.VarChar, 50)
                parm.Value = Trim(cmbName.SelectedItem.id)
                parm = .Parameters.Add("@emp_name", SqlDbType.VarChar, 256)
                parm.Value = cmbName.Text
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                strSaveStock = "SAVE"
                MsgBox("Successfully saved!", MsgBoxStyle.Information, "eWIP")
                Me.Close()
            End With












        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            com.Parameters.Clear()
            cn.Close()
        End Try



    End Sub
End Class