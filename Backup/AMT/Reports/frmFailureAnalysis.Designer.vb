<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFailureAnalysis
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFailureAnalysis))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblReportTit = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.butDelMulti = New System.Windows.Forms.Button
        Me.butDelSingle = New System.Windows.Forms.Button
        Me.butAssMulti = New System.Windows.Forms.Button
        Me.butAssSingle = New System.Windows.Forms.Button
        Me.lblSelect = New System.Windows.Forms.Label
        Me.lblCode = New System.Windows.Forms.Label
        Me.rdbItem = New System.Windows.Forms.RadioButton
        Me.rdbWC = New System.Windows.Forms.RadioButton
        Me.P1 = New System.Windows.Forms.Panel
        Me.PB1 = New System.Windows.Forms.ProgressBar
        Me.dtpTo = New System.Windows.Forms.DateTimePicker
        Me.dtpFrom = New System.Windows.Forms.DateTimePicker
        Me.txtTo = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtFrom = New System.Windows.Forms.TextBox
        Me.butEXIT = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.lstItem = New System.Windows.Forms.ListBox
        Me.lstSelItem = New System.Windows.Forms.ListBox
        Me.RViewer = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.butClose = New System.Windows.Forms.Button
        Me.butExport = New System.Windows.Forms.Button
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.PB2 = New System.Windows.Forms.ProgressBar
        Me.sfdSave = New System.Windows.Forms.SaveFileDialog
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.P1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.BackColor = System.Drawing.Color.Silver
        Me.Panel1.Controls.Add(Me.lblReportTit)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(16, 37)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(751, 526)
        Me.Panel1.TabIndex = 4
        '
        'lblReportTit
        '
        Me.lblReportTit.AutoSize = True
        Me.lblReportTit.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportTit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblReportTit.Location = New System.Drawing.Point(11, 9)
        Me.lblReportTit.Name = "lblReportTit"
        Me.lblReportTit.Size = New System.Drawing.Size(114, 15)
        Me.lblReportTit.TabIndex = 36
        Me.lblReportTit.Text = "FAILURE ANALYSIS"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.Controls.Add(Me.butDelMulti)
        Me.Panel2.Controls.Add(Me.butDelSingle)
        Me.Panel2.Controls.Add(Me.butAssMulti)
        Me.Panel2.Controls.Add(Me.butAssSingle)
        Me.Panel2.Controls.Add(Me.lblSelect)
        Me.Panel2.Controls.Add(Me.lblCode)
        Me.Panel2.Controls.Add(Me.rdbItem)
        Me.Panel2.Controls.Add(Me.rdbWC)
        Me.Panel2.Controls.Add(Me.P1)
        Me.Panel2.Controls.Add(Me.dtpTo)
        Me.Panel2.Controls.Add(Me.dtpFrom)
        Me.Panel2.Controls.Add(Me.txtTo)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.txtFrom)
        Me.Panel2.Controls.Add(Me.butEXIT)
        Me.Panel2.Controls.Add(Me.butSave)
        Me.Panel2.Controls.Add(Me.lstItem)
        Me.Panel2.Controls.Add(Me.lstSelItem)
        Me.Panel2.Location = New System.Drawing.Point(12, 27)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(716, 485)
        Me.Panel2.TabIndex = 0
        '
        'butDelMulti
        '
        Me.butDelMulti.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butDelMulti.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelMulti.Location = New System.Drawing.Point(340, 284)
        Me.butDelMulti.Name = "butDelMulti"
        Me.butDelMulti.Size = New System.Drawing.Size(39, 23)
        Me.butDelMulti.TabIndex = 142
        Me.butDelMulti.Text = "<<"
        Me.butDelMulti.UseVisualStyleBackColor = True
        '
        'butDelSingle
        '
        Me.butDelSingle.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butDelSingle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelSingle.Location = New System.Drawing.Point(340, 245)
        Me.butDelSingle.Name = "butDelSingle"
        Me.butDelSingle.Size = New System.Drawing.Size(39, 23)
        Me.butDelSingle.TabIndex = 141
        Me.butDelSingle.Text = "<"
        Me.butDelSingle.UseVisualStyleBackColor = True
        '
        'butAssMulti
        '
        Me.butAssMulti.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butAssMulti.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butAssMulti.Location = New System.Drawing.Point(340, 207)
        Me.butAssMulti.Name = "butAssMulti"
        Me.butAssMulti.Size = New System.Drawing.Size(39, 23)
        Me.butAssMulti.TabIndex = 140
        Me.butAssMulti.Text = ">>"
        Me.butAssMulti.UseVisualStyleBackColor = True
        '
        'butAssSingle
        '
        Me.butAssSingle.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butAssSingle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butAssSingle.Location = New System.Drawing.Point(340, 165)
        Me.butAssSingle.Name = "butAssSingle"
        Me.butAssSingle.Size = New System.Drawing.Size(39, 23)
        Me.butAssSingle.TabIndex = 139
        Me.butAssSingle.Text = ">"
        Me.butAssSingle.UseVisualStyleBackColor = True
        '
        'lblSelect
        '
        Me.lblSelect.AutoSize = True
        Me.lblSelect.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelect.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblSelect.Location = New System.Drawing.Point(383, 67)
        Me.lblSelect.Name = "lblSelect"
        Me.lblSelect.Size = New System.Drawing.Size(117, 15)
        Me.lblSelect.TabIndex = 136
        Me.lblSelect.Text = "Selected Item Code"
        '
        'lblCode
        '
        Me.lblCode.AutoSize = True
        Me.lblCode.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblCode.Location = New System.Drawing.Point(13, 70)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(64, 15)
        Me.lblCode.TabIndex = 135
        Me.lblCode.Text = "Item Code"
        '
        'rdbItem
        '
        Me.rdbItem.AutoSize = True
        Me.rdbItem.Checked = True
        Me.rdbItem.Location = New System.Drawing.Point(385, 49)
        Me.rdbItem.Name = "rdbItem"
        Me.rdbItem.Size = New System.Drawing.Size(45, 17)
        Me.rdbItem.TabIndex = 129
        Me.rdbItem.TabStop = True
        Me.rdbItem.Text = "Item"
        Me.rdbItem.UseVisualStyleBackColor = True
        '
        'rdbWC
        '
        Me.rdbWC.AutoSize = True
        Me.rdbWC.Location = New System.Drawing.Point(241, 49)
        Me.rdbWC.Name = "rdbWC"
        Me.rdbWC.Size = New System.Drawing.Size(85, 17)
        Me.rdbWC.TabIndex = 128
        Me.rdbWC.Text = "Work Center"
        Me.rdbWC.UseVisualStyleBackColor = True
        '
        'P1
        '
        Me.P1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.P1.BackColor = System.Drawing.Color.Gray
        Me.P1.Controls.Add(Me.PB1)
        Me.P1.Location = New System.Drawing.Point(210, 444)
        Me.P1.Name = "P1"
        Me.P1.Size = New System.Drawing.Size(295, 32)
        Me.P1.TabIndex = 127
        '
        'PB1
        '
        Me.PB1.Location = New System.Drawing.Point(15, 7)
        Me.PB1.Name = "PB1"
        Me.PB1.Size = New System.Drawing.Size(266, 19)
        Me.PB1.TabIndex = 0
        '
        'dtpTo
        '
        Me.dtpTo.Location = New System.Drawing.Point(460, 20)
        Me.dtpTo.Name = "dtpTo"
        Me.dtpTo.Size = New System.Drawing.Size(17, 20)
        Me.dtpTo.TabIndex = 126
        '
        'dtpFrom
        '
        Me.dtpFrom.Location = New System.Drawing.Point(317, 20)
        Me.dtpFrom.Name = "dtpFrom"
        Me.dtpFrom.Size = New System.Drawing.Size(17, 20)
        Me.dtpFrom.TabIndex = 125
        '
        'txtTo
        '
        Me.txtTo.BackColor = System.Drawing.Color.White
        Me.txtTo.Location = New System.Drawing.Point(370, 20)
        Me.txtTo.Name = "txtTo"
        Me.txtTo.Size = New System.Drawing.Size(91, 20)
        Me.txtTo.TabIndex = 118
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(348, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(21, 15)
        Me.Label1.TabIndex = 117
        Me.Label1.Text = "To"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(93, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(129, 15)
        Me.Label5.TabIndex = 116
        Me.Label5.Tag = ""
        Me.Label5.Text = "Completed Date From"
        '
        'txtFrom
        '
        Me.txtFrom.BackColor = System.Drawing.Color.White
        Me.txtFrom.Location = New System.Drawing.Point(228, 20)
        Me.txtFrom.Name = "txtFrom"
        Me.txtFrom.Size = New System.Drawing.Size(91, 20)
        Me.txtFrom.TabIndex = 115
        '
        'butEXIT
        '
        Me.butEXIT.BackColor = System.Drawing.Color.Gray
        Me.butEXIT.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butEXIT.ForeColor = System.Drawing.Color.White
        Me.butEXIT.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butEXIT.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butEXIT.Location = New System.Drawing.Point(378, 417)
        Me.butEXIT.Name = "butEXIT"
        Me.butEXIT.Size = New System.Drawing.Size(57, 24)
        Me.butEXIT.TabIndex = 114
        Me.butEXIT.Text = "Exit"
        Me.butEXIT.UseVisualStyleBackColor = False
        '
        'butSave
        '
        Me.butSave.BackColor = System.Drawing.Color.Gray
        Me.butSave.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butSave.ForeColor = System.Drawing.Color.White
        Me.butSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(270, 417)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(57, 24)
        Me.butSave.TabIndex = 112
        Me.butSave.Text = "Done"
        Me.butSave.UseVisualStyleBackColor = False
        '
        'lstItem
        '
        Me.lstItem.FormattingEnabled = True
        Me.lstItem.HorizontalScrollbar = True
        Me.lstItem.Location = New System.Drawing.Point(8, 90)
        Me.lstItem.Name = "lstItem"
        Me.lstItem.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lstItem.Size = New System.Drawing.Size(324, 316)
        Me.lstItem.Sorted = True
        Me.lstItem.TabIndex = 137
        '
        'lstSelItem
        '
        Me.lstSelItem.FormattingEnabled = True
        Me.lstSelItem.HorizontalScrollbar = True
        Me.lstSelItem.Location = New System.Drawing.Point(389, 86)
        Me.lstSelItem.Name = "lstSelItem"
        Me.lstSelItem.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lstSelItem.Size = New System.Drawing.Size(307, 316)
        Me.lstSelItem.Sorted = True
        Me.lstSelItem.TabIndex = 138
        '
        'RViewer
        '
        Me.RViewer.ActiveViewIndex = -1
        Me.RViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.RViewer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RViewer.Location = New System.Drawing.Point(0, 0)
        Me.RViewer.Name = "RViewer"
        ''Me.RViewer.ReuseParameterValuesOnRefresh = True
        Me.RViewer.ShowCloseButton = False
        Me.RViewer.Size = New System.Drawing.Size(779, 604)
        Me.RViewer.TabIndex = 5
        ''Me.RViewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.RViewer.Visible = False
        '
        'butClose
        '
        Me.butClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butClose.BackgroundImage = CType(resources.GetObject("butClose.BackgroundImage"), System.Drawing.Image)
        Me.butClose.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butClose.Location = New System.Drawing.Point(768, 0)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(11, 11)
        Me.butClose.TabIndex = 8
        Me.butClose.Text = "X"
        Me.butClose.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butClose.UseVisualStyleBackColor = True
        Me.butClose.Visible = False
        '
        'butExport
        '
        Me.butExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butExport.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butExport.Image = CType(resources.GetObject("butExport.Image"), System.Drawing.Image)
        Me.butExport.Location = New System.Drawing.Point(654, 5)
        Me.butExport.Name = "butExport"
        Me.butExport.Size = New System.Drawing.Size(30, 23)
        Me.butExport.TabIndex = 131
        Me.butExport.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butExport.UseVisualStyleBackColor = True
        Me.butExport.Visible = False
        '
        'Panel3
        '
        Me.Panel3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel3.BackColor = System.Drawing.Color.Gray
        Me.Panel3.Controls.Add(Me.PB2)
        Me.Panel3.Location = New System.Drawing.Point(33, 309)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(295, 32)
        Me.Panel3.TabIndex = 132
        Me.Panel3.Visible = False
        '
        'PB2
        '
        Me.PB2.Location = New System.Drawing.Point(15, 7)
        Me.PB2.Name = "PB2"
        Me.PB2.Size = New System.Drawing.Size(266, 19)
        Me.PB2.TabIndex = 0
        '
        'frmFailureAnalysis
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Info
        Me.ClientSize = New System.Drawing.Size(779, 604)
        Me.ControlBox = False
        Me.Controls.Add(Me.butExport)
        Me.Controls.Add(Me.butClose)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.RViewer)
        Me.Controls.Add(Me.Panel3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmFailureAnalysis"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.P1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblReportTit As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents rdbWC As System.Windows.Forms.RadioButton
    Friend WithEvents P1 As System.Windows.Forms.Panel
    Friend WithEvents PB1 As System.Windows.Forms.ProgressBar
    Friend WithEvents dtpTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtTo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtFrom As System.Windows.Forms.TextBox
    Friend WithEvents butEXIT As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents rdbItem As System.Windows.Forms.RadioButton
    Friend WithEvents RViewer As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents butClose As System.Windows.Forms.Button
    Friend WithEvents butExport As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents PB2 As System.Windows.Forms.ProgressBar
    Friend WithEvents sfdSave As System.Windows.Forms.SaveFileDialog
    Friend WithEvents lblSelect As System.Windows.Forms.Label
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents butDelMulti As System.Windows.Forms.Button
    Friend WithEvents butDelSingle As System.Windows.Forms.Button
    Friend WithEvents butAssMulti As System.Windows.Forms.Button
    Friend WithEvents butAssSingle As System.Windows.Forms.Button
    Friend WithEvents lstSelItem As System.Windows.Forms.ListBox
    Friend WithEvents lstItem As System.Windows.Forms.ListBox
End Class
