Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Microsoft.VisualBasic.DateAndTime
Public Class frmRptTimebyResource
    Dim clsM As New clsMain
    Public DSItem As New DataSet
    Public DSWC As New DataSet
    Dim dsrsg As DSRlCycleTime
    Dim ArrexWeek() As Integer
    Dim ArrexOP() As Integer
    Dim ArrexRes() As String
    Dim ArrexItem() As String
    Dim ArrHasAll() As String
    Dim strReportDate As String
    Private Sub frmRptTimebyResource_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'lblReportTit.Text = "CO BY RESOURCE GROUP"

        dtpFrom.Value = Now
        dtpTo.Value = Now
        txtFrom.Text = Format(Now, "dd/MM/yyyy")
        txtTo.Text = Format(Now, "dd/MM/yyyy")
        ResourceData()
        GetWC()
        GetItem()
    End Sub
    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtTo.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        dtpTo.Checked = False
    End Sub
    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtFrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False
    End Sub
    Sub GetWC()
        cmbWC.Items.Clear()
        DSWC.Tables.Clear()
        cmbWC.Items.Add(New DataItem("-Select-", "-Select-"))
        Dim selcon As String = ""
        Dim sql As String = ""
        If cmbResource.Text <> "-Select-" Then
            sql = "select * from tbWC where _wc in(select _WC from tbWorkStation where _rid in(select _rid from tbResourceGroup where _rGroupid='" & Trim(cmbResource.Text) & "')) order by _wc"

        Else
            sql = "select * from tbWC order by _wc"
        End If
        Try
            Dim ds As New DataSet
            ds = clsM.GetDataset(sql, "tbWC")
            DSWC = ds
            If IsDBNull(ds) = False Then
                Dim i As Integer
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            cmbWC.Items.Add(New DataItem(Trim(.Item("_wc")), Trim(.Item("_wc")) & "  :  " & Trim(.Item("_description")) & ""))
                        End With
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
        cmbWC.Text = "-Select-"
    End Sub
    Sub ResourceData()
        Dim ds As New DataSet
        cmbResource.Items.Clear()
        cmbResource.Items.Add(New DataItem("-Select-", "-Select-"))
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                'Dim DI As DataItem

                ds = clsM.GetDataset("select _rGroupid,_rGroupName,_rid from tbResourceGroup order by _rGroupid", "tbWC")
                If IsDBNull(ds) = False Then
                    Dim i As Integer
                    If ds.Tables(0).Rows.Count > 0 Then
                        For i = 0 To ds.Tables(0).Rows.Count - 1
                            With ds.Tables(0).Rows(i)
                                cmbResource.Items.Add(New DataItem(.Item("_rGroupName") & "||" & .Item("_rid"), .Item("_rGroupid")))
                            End With
                        Next
                    End If
                End If
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
        cmbResource.Text = "-Select-"
    End Sub
    Sub GetItem()
        DSItem.Tables.Clear()
        lstSelItem.Items.Clear()
        lstItem.Items.Clear()
        Try
            Dim ds As New DataSet
            'ds = clsM.GetDataset("select * from tbItem where  order by _itemCode", "tbItem")
            ds = clsM.GetDataset("select * from tbItem where _type<>'O' and( _itemCode like('F%') or _itemCode like('FG%') or _itemCode like('AC%') or  _itemCode like('SC%')) order by _itemCode", "tbItem")
            '
            DSItem = ds
            If IsDBNull(ds) = False Then
                Dim i As Integer
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            ' If UCase(Mid(.Item("_itemCode"), 1, 2)) <> "SC" Then
                            'Dim st As String() = Split(.Item("_itemCode"), "-")
                            'If st.Length = 1 Then
                            'lstItem.Items.Add(New DataItem(Trim(.Item("_itemCode")), Trim(.Item("_itemCode")) & "  :  " & Trim(.Item("_itemdescription")) & ""))
                            'End If
                            'Else
                            lstItem.Items.Add(New DataItem(Trim(.Item("_itemCode")), Trim(.Item("_itemCode")) & "  :  " & Trim(.Item("_itemdescription")) & ""))
                            'End If
                        End With
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try

    End Sub
    Private Sub cmbResource_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbResource.SelectedIndexChanged
        GetWC()
    End Sub
    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub
    Private Sub butAssSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssSingle.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstItem.SelectedItems.Count - 1
                lstSelItem.Items.Add(lstItem.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstItem.SelectedItems.Count - 1
                lstItem.Items.Remove(lstItem.SelectedItems.Item(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butAssMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssMulti.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstItem.Items.Count - 1
                lstSelItem.Items.Add(lstItem.Items(i))
            Next
            i = 0
            For i = 0 To lstItem.Items.Count - 1
                lstItem.Items.Remove(lstItem.Items(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butDelSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelSingle.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstSelItem.SelectedItems.Count - 1
                lstItem.Items.Add(lstSelItem.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstSelItem.SelectedItems.Count - 1
                lstSelItem.Items.Remove(lstSelItem.SelectedItems.Item(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub
    Private Sub butDelMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelMulti.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstSelItem.Items.Count - 1
                lstItem.Items.Add(lstSelItem.Items(i))
            Next
            i = 0
            For i = 0 To lstSelItem.Items.Count - 1
                lstSelItem.Items.Remove(lstSelItem.Items(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        dtpFrom.Value = Now
        dtpTo.Value = Now
        ResourceData()
        GetWC()
        GetItem()
    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If lblReportTit.Text = UCase("Actual Cycle Time by Resource Group(Summary by Items)") Then
            comletedByItem()
        Else
            comletedTransaction()
        End If

        TmVari = 2

        '  Parent.Focus()

    End Sub
    Sub comletedTransaction()
        Try
            If Trim(txtFrom.Text) = "" Or Trim(txtTo.Text) = "" Then
                MsgBox("Please enter the date!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim dblSd As Double = clsM.CheckDate(txtFrom.Text)
            If dblSd = 0 Then
                MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If

            Dim dblEd As Double = clsM.CheckDate(txtTo.Text)
            If dblEd = 0 Then
                MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim s() As String = Split(txtTo.Text, "/")
            dsrsg = New DSRlCycleTime
            Dim Dateto As Date = DateSerial(s(2), s(1), s(0))
            Dateto = DateAdd(DateInterval.Minute, -1, DateAdd(DateInterval.Day, 1, Dateto))
            dblEd = 0
            dblEd = Dateto.ToOADate

            Dim HASRe As New Hashtable
            Dim StItem As Boolean = False
            If lstSelItem.Items.Count > 0 Then
                Dim k As Integer
                For k = 0 To lstSelItem.Items.Count - 1
                    HASRe.Add(lstSelItem.Items(k).id, lstSelItem.Items(k).id)
                Next
            Else
                StItem = True
                HASRe.Add("ALL", "ALL")

            End If

            ' Dim strReportDate As String = txtFrom.Text & "-" & txtTo.Text

            strReportDate = Format(DateTime.FromOADate(dblSd), "dd MMM yyyy") & " - " & Format(DateTime.FromOADate(dblEd), "dd MMM yyyy")
            Dim CreatedDate As String = Format(Now, "dd MMM yyyy")

            Dim DSrep As New DataSet
            Dim DSrepHis As New DataSet
            Dim Strsql As String
            Dim strResource As String
            Dim DSRecGroup As New DataSet
            Dim DSItem As New DataSet
            DSItem = clsM.GetDataset("select * from tbItem", "tbItem")
            Dim DSRecWC As New DataSet
            DSRecWC = clsM.GetDataset("select * from tbWC", "tbWC")
            DSRecGroup = clsM.GetDataset("select A.*,B.* from tbWC A,tbResourceGroup B,tbWorkStation C where C._wc=A._wc and C._rid=B._rid", "tbRec")

            Dim DSMachine As New DataSet
            DSMachine = clsM.GetDataset("select * from tbMachine", "tbMachine")


            Dim hashFindUnique As New Hashtable
            Dim ArrResouce(0) As String
            Dim ArrMachine(0) As String
            Dim ArrResouceDesc(0) As String
            Dim ArrWC(0) As String
            Dim ArrWCDesc(0) As String
            Dim Arrhr(0) As Double
            Dim ArrMint(0) As Double
            Dim ArrtotMint(0) As Double
            Dim Arrtothr(0) As Double

            Dim ArrQty(0) As Double

            If cmbResource.Text <> "" And cmbResource.Text <> "-Select-" And cmbWC.Text = "-Select-" Then
                Dim Strx As String() = Split(cmbResource.SelectedItem.id, "||")
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped,A._Machineid  from tbjobtrans A,tbJobTransMain B where  A._Status='COMPLETED' and A._job=b._job and A._oper_num=B._oper_num   and  A._end_Date between " & dblSd & " and " & dblEd & " and B._wc in (select _wc from tbWorkStation where _rid=" & Val(Strx(1)) & ") order by A._Job"
            ElseIf cmbWC.Text <> "-Select-" Then
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped,A._Machineid  from tbjobtrans A,tbJobTransMain B where  A._Status='COMPLETED' and A._job=b._job and A._oper_num=B._oper_num   and  A._end_Date between " & dblSd & " and " & dblEd & " and B._wc ='" & Replace(cmbWC.SelectedItem.id, "'", "''") & "' order by A._Job"
            Else
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped,A._Machineid  from tbjobtrans A,tbJobTransMain B where  A._Status='COMPLETED' and A._job=b._job and A._oper_num=B._oper_num   and  A._end_Date between " & dblSd & " and " & dblEd & " order by A._Job"
            End If
            DSrep = clsM.GetDataset(Strsql, "tbJobTransMain")



            If cmbResource.Text <> "" And cmbResource.Text <> "-Select-" And cmbWC.Text = "-Select-" Then
                Dim Strx As String() = Split(cmbResource.SelectedItem.id, "||")
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped,A._Machineid  from tbjobtransHis A,tbJobTransMain B where  A._Status='COMPLETED' and A._job=b._job and A._oper_num=B._oper_num   and  A._end_Date between " & dblSd & " and " & dblEd & " and B._wc in (select _wc from tbWorkStation where _rid=" & Val(Strx(1)) & ") order by A._Job"
            ElseIf cmbWC.Text <> "-Select-" Then
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped,A._Machineid  from tbjobtransHis A,tbJobTransMain B where  A._Status='COMPLETED' and A._job=b._job and A._oper_num=B._oper_num   and  A._end_Date between " & dblSd & " and " & dblEd & " and B._wc ='" & Replace(cmbWC.SelectedItem.id, "'", "''") & "' order by A._Job"
            Else
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped,A._Machineid  from tbjobtransHis A,tbJobTransMain B where  A._Status='COMPLETED' and A._job=b._job and A._oper_num=B._oper_num   and  A._end_Date between " & dblSd & " and " & dblEd & " order by A._Job"
            End If
            DSrepHis = clsM.GetDataset(Strsql, "tbJobTransMain")





            Dim totqty As Double
            Dim drRp As DataRow
            Dim recount As Boolean = False
            PB1.Value = 0
            PB1.Minimum = 0
            PB1.Maximum = 0
            Dim IA As Integer = 0
            Dim j As Integer = 0
            If DSrep.Tables(0).Rows.Count > 0 Then
                Dim i As Integer

                PB1.Maximum = DSrep.Tables(0).Rows.Count + DSrepHis.Tables(0).Rows.Count

                For i = 0 To DSrep.Tables(0).Rows.Count - 1
                    With DSrep.Tables(0).Rows(i)
                        If .Item("_qty_complete") <> 0 Or .Item("_qty_scrapped") <> 0 Then
                            recount = True
                            Dim runt As Boolean = False
                            Dim strResCode As String = ""
                            Dim strResDesc As String = ""
                            Dim strWC As String = ""
                            Dim strWCdesc As String = ""
                            Dim ArrItem As String() = Split(.Item("_item"), "-")
                            Dim strCont As String = ArrItem(0)
                            If UCase(Mid(.Item("_item"), 1, 2)) = "SC" Then
                                strCont = .Item("_item")
                            End If

                            If HASRe.ContainsKey(strCont) = True Then
                                runt = True
                            Else
                                runt = False
                            End If


                            If runt = True Then


                                Dim strConst As String = ""
                                Dim DRResou() As DataRow = DSRecGroup.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                If DRResou.Length > 0 Then
                                    strResCode = DRResou(0).Item("_rGroupID")
                                    strResDesc = DRResou(0).Item("_rGroupName")
                                    strWC = DRResou(0).Item("_wc")
                                    strWCdesc = DRResou(0).Item("_description")
                                End If
                                Dim strMachine As String = .Item("_Machineid")
                                If Trim(strMachine) = "-" Or Trim(strMachine) = "" Then
                                Else
                                    Dim drM() As DataRow = DSMachine.Tables(0).Select("_machineID='" & Trim(strMachine) & "'")
                                    If drM.Length > 0 Then
                                        strMachine = drM(0).Item("_MachineDesc")
                                    End If
                                End If
                                Dim strFindid As String = strResCode & "||" & strWC & "||" & strMachine
                                If hashFindUnique.Contains(strFindid) = False Then

                                    ReDim Preserve ArrResouce(j)
                                    ReDim Preserve ArrResouceDesc(j)
                                    ReDim Preserve ArrWC(j)
                                    ReDim Preserve ArrWCDesc(j)
                                    ReDim Preserve Arrhr(j)
                                    ReDim Preserve ArrMint(j)
                                    ReDim Preserve ArrtotMint(j)
                                    ReDim Preserve ArrQty(j)
                                    ReDim Preserve ArrMachine(j)
                                    hashFindUnique.Add(strFindid, j)
                                    ArrResouce(j) = strResCode
                                    ArrResouceDesc(j) = strResCode
                                    ArrWC(j) = strWC
                                    ArrWCDesc(j) = strWCdesc
                                    Dim sd, Ed As Date
                                    ArrMachine(j) = strMachine
                                    sd = DateTime.FromOADate(.Item("_start_Date"))
                                    Ed = DateTime.FromOADate(.Item("_end_Date"))
                                    ArrQty(j) = .Item("_qty_complete") + .Item("_qty_scrapped")
                                    Dim dblTot As Double = DateDiff(DateInterval.Second, sd, Ed)
                                    ArrtotMint(j) = dblTot
                                    Dim dblMint As Double = ArrtotMint(j) / 60
                                    'dblTot = DateDiff(DateInterval.Minute, sd, Ed)
                                    Dim dblHrs As Double = (ArrtotMint(j) / 60) / 60
                                    Arrhr(j) = dblHrs
                                    ArrMint(j) = dblMint
                                    j = j + 1
                                Else
                                    Dim m As Integer = hashFindUnique(strFindid)

                                    Dim sd, Ed As Date
                                    sd = DateTime.FromOADate(.Item("_start_Date"))
                                    Ed = DateTime.FromOADate(.Item("_end_Date"))
                                    Dim dblTot As Double = DateDiff(DateInterval.Second, sd, Ed)
                                    ArrtotMint(m) = ArrtotMint(m) + dblTot
                                    Dim dblMint As Double = ArrtotMint(m) / 60
                                    ArrQty(m) = ArrQty(m) + .Item("_qty_complete") + .Item("_qty_scrapped")
                                    'dblTot = DateDiff(DateInterval.Minute, sd, Ed)
                                    Dim dblHrs As Double = (ArrtotMint(m) / 60) / 60
                                    Arrhr(m) = dblHrs
                                    ArrMint(m) = dblMint
                                End If
                            End If
                        End If
                    End With
                    PB1.Value = PB1.Value + 1
                Next
            End If




            If DSrepHis.Tables(0).Rows.Count > 0 Then
                Dim i As Integer

                ' Dim j As Integer = 0
                For i = 0 To DSrepHis.Tables(0).Rows.Count - 1
                    With DSrepHis.Tables(0).Rows(i)
                        If .Item("_qty_complete") <> 0 Or .Item("_qty_scrapped") <> 0 Then
                            recount = True
                            Dim runt As Boolean = False
                            Dim strResCode As String = ""
                            Dim strResDesc As String = ""
                            Dim strWC As String = ""
                            Dim strWCdesc As String = ""
                            Dim ArrItem As String() = Split(.Item("_item"), "-")
                            Dim strCont As String = ArrItem(0)
                            If UCase(Mid(.Item("_item"), 1, 2)) = "SC" Then
                                strCont = .Item("_item")
                            End If

                            If HASRe.ContainsKey(strCont) = True Then
                                runt = True
                            Else
                                runt = False
                            End If


                            If runt = True Then

                                Dim strConst As String = ""
                                Dim DRResou() As DataRow = DSRecGroup.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                If DRResou.Length > 0 Then
                                    strResCode = DRResou(0).Item("_rGroupID")
                                    strResDesc = DRResou(0).Item("_rGroupName")
                                    strWC = DRResou(0).Item("_wc")
                                    strWCdesc = DRResou(0).Item("_description")
                                End If
                                Dim strMachine As String = DRResou(0).Item("_description")
                                If Trim(strMachine) = "-" Or Trim(strMachine) = "" Then
                                Else
                                    Dim drM() As DataRow = DSMachine.Tables(0).Select("_machineID='" & Trim(strMachine) & "'")
                                    If drM.Length > 0 Then
                                        strMachine = drM(0).Item("_MachineDesc")
                                    End If
                                End If
                                Dim strFindid As String = strResCode & "||" & strWC & "||" & strMachine
                                If hashFindUnique.Contains(strFindid) = False Then
                                    'If strResCode = "FURNCE" And strWC = "DEGRSE" Then
                                    '    MsgBox("Hi")

                                    'End If
                                    ReDim Preserve ArrResouce(j)
                                    ReDim Preserve ArrResouceDesc(j)
                                    ReDim Preserve ArrWC(j)
                                    ReDim Preserve ArrWCDesc(j)
                                    ReDim Preserve Arrhr(j)
                                    ReDim Preserve ArrMint(j)
                                    ReDim Preserve ArrtotMint(j)
                                    ReDim Preserve ArrQty(j)
                                    ReDim Preserve ArrMachine(j)
                                    hashFindUnique.Add(strFindid, j)
                                    ArrResouce(j) = strResCode
                                    ArrResouceDesc(j) = strResCode
                                    ArrWC(j) = strWC
                                    ArrWCDesc(j) = strWCdesc
                                    ArrMachine(j) = strMachine
                                    Dim sd, Ed As Date
                                    sd = DateTime.FromOADate(.Item("_start_Date"))
                                    Ed = DateTime.FromOADate(.Item("_end_Date"))
                                    Dim dblTot As Double = DateDiff(DateInterval.Second, sd, Ed)
                                    ArrQty(j) = .Item("_qty_complete") + .Item("_qty_scrapped")
                                    ArrtotMint(j) = dblTot
                                    Dim dblMint As Double = ArrtotMint(j) / 60
                                    'dblTot = DateDiff(DateInterval.Minute, sd, Ed)
                                    Dim dblHrs As Double = (ArrtotMint(j) / 60) / 60
                                    Arrhr(j) = dblHrs
                                    ArrMint(j) = dblMint
                                    j = j + 1
                                Else
                                    Dim m As Integer = hashFindUnique(strFindid)

                                    Dim sd, Ed As Date
                                    sd = DateTime.FromOADate(.Item("_start_Date"))
                                    Ed = DateTime.FromOADate(.Item("_end_Date"))
                                    Dim dblTot As Double = DateDiff(DateInterval.Second, sd, Ed)
                                    ArrQty(m) = ArrQty(m) + .Item("_qty_complete") + .Item("_qty_scrapped")
                                    ArrtotMint(m) = ArrtotMint(m) + dblTot
                                    Dim dblMint As Double = ArrtotMint(m) / 60
                                    'dblTot = DateDiff(DateInterval.Minute, sd, Ed)
                                    Dim dblHrs As Double = (ArrtotMint(m) / 60) / 60
                                    Arrhr(m) = dblHrs
                                    ArrMint(m) = dblMint
                                End If
                            End If
                        End If
                    End With
                    PB1.Value = PB1.Value + 1
                Next
            End If










            If recount = True Then
                Dim i As Integer
                For i = 0 To ArrResouce.Length - 1
                    drRp = dsrsg.Tables("tbCTResource").NewRow
                    drRp("sn") = i + 1
                    drRp("Title") = strReportDate ' Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                    drRp("CreateDate") = CreatedDate
                    drRp("ReportDate") = strReportDate
                    drRp("FGCode") = ""
                    drRp("FGDesc") = ""
                    drRp("Job") = ""
                    drRp("Machine") = ArrMachine(i)
                    drRp("OP") = ""
                    drRp("WC") = ArrWC(i)
                    drRp("WCDesc") = ArrWCDesc(i)
                    drRp("Resource") = ArrResouce(i)
                    drRp("ResourceDesc") = ArrResouceDesc(i)
                    drRp("hrvs") = Arrhr(i)
                    drRp("mints") = ArrMint(i)
                    drRp("Qty") = ArrQty(i)
                    drRp("perHr") = 0
                    drRp("PerMin") = 0
                    dsrsg.Tables("tbCTResource").Rows.Add(drRp)
                Next
            Else
                drRp = dsrsg.Tables("tbCTResource").NewRow
                drRp("sn") = 0
                drRp("Title") = strReportDate 'Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                drRp("CreateDate") = CreatedDate
                drRp("ReportDate") = strReportDate
                drRp("FGCode") = ""
                drRp("FGDesc") = ""
                drRp("Job") = ""
                drRp("OP") = ""
                drRp("WC") = ""
                drRp("WCDesc") = ""
                drRp("Machine") = ""
                drRp("Resource") = ""
                drRp("ResourceDesc") = ""
                drRp("Qty") = 0
                drRp("hrvs") = 0
                drRp("mints") = 0
                drRp("perHr") = 0
                drRp("PerMin") = 0
                dsrsg.Tables("tbCTResource").Rows.Add(drRp)
            End If
            'If lblReportTit.Text = UCase("Actual Cycle Time by Resource Group(Summary)") Then
            '    ' Dim cry As New cryCyTimeResourceGroup
            '    Dim cry As New cryCyTimeSumResourceGroup
            '    cry.SetDataSource(dsrsg)
            '    RViewer.ReportSource = cry
            '    Panel1.Visible = False
            '    RViewer.Visible = True
            '    RViewer.Enabled = True
            '    Me.Activate()
            '    Application.DoEvents()
            '    RViewer.Refresh()
            '    butClose.Visible = True
            'Else
            Dim cry As New cryCyTimeResourceGroup
            'Dim cry As New cryCyTimeSumResourceGroup
            cry.SetDataSource(dsrsg)
            RViewer.ReportSource = cry
            Panel1.Visible = False
            RViewer.Visible = True
            RViewer.Enabled = True
            Me.Activate()
            Application.DoEvents()
            RViewer.Refresh()
            butClose.Visible = True
            ' End If


            ' butExport.Visible = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        RViewer.DataBindings.Clear()
        GC.GetGeneration(RViewer)
        RViewer.Visible = False
        butClose.Visible = False
        ' butExport.Visible = False
        PB1.Value = 0
        PB1.Minimum = 0
        PB1.Maximum = 0
        Panel1.Visible = True
    End Sub

    Sub comletedByItem()
        Try
            If Trim(txtFrom.Text) = "" Or Trim(txtTo.Text) = "" Then
                MsgBox("Please enter the date!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim dblSd As Double = clsM.CheckDate(txtFrom.Text)
            If dblSd = 0 Then
                MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If

            Dim dblEd As Double = clsM.CheckDate(txtTo.Text)
            If dblEd = 0 Then
                MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim s() As String = Split(txtTo.Text, "/")
            dsrsg = New DSRlCycleTime
            Dim Dateto As Date = DateSerial(s(2), s(1), s(0))
            Dateto = DateAdd(DateInterval.Minute, -1, DateAdd(DateInterval.Day, 1, Dateto))
            dblEd = 0
            dblEd = Dateto.ToOADate

            Dim HASRe As New Hashtable
            Dim StItem As Boolean = False
            If lstSelItem.Items.Count > 0 Then
                Dim k As Integer
                For k = 0 To lstSelItem.Items.Count - 1
                    HASRe.Add(lstSelItem.Items(k).id, lstSelItem.Items(k).id)
                Next
            Else
                StItem = True
                HASRe.Add("ALL", "ALL")

            End If

            ' Dim strReportDate As String = txtFrom.Text & "-" & txtTo.Text

            strReportDate = Format(DateTime.FromOADate(dblSd), "dd MMM yyyy") & " - " & Format(DateTime.FromOADate(dblEd), "dd MMM yyyy")
            Dim CreatedDate As String = Format(Now, "dd MMM yyyy")

            Dim DSrep As New DataSet
            Dim DSrepHis As New DataSet
            Dim Strsql As String
            Dim strResource As String
            Dim DSRecGroup As New DataSet
            Dim DSItem As New DataSet
            DSItem = clsM.GetDataset("select * from tbItem", "tbItem")
            Dim DSRecWC As New DataSet
            DSRecWC = clsM.GetDataset("select * from tbWC", "tbWC")
            DSRecGroup = clsM.GetDataset("select A.*,B.* from tbWC A,tbResourceGroup B,tbWorkStation C where C._wc=A._wc and C._rid=B._rid", "tbRec")

          


            Dim hashFindUnique As New Hashtable
            Dim ArrResouce(0) As String
            Dim ArrMachine(0) As String
            Dim ArrResouceDesc(0) As String
            Dim ArrWC(0) As String
            Dim ArrWCDesc(0) As String
            Dim Arrhr(0) As Double
            Dim ArrMint(0) As Double
            Dim ArrtotMint(0) As Double
            Dim Arrtothr(0) As Double
            Dim ArrItemName(0) As String
            Dim ArrItemDesc(0) As String

            Dim ArrQty(0) As Double

            If cmbResource.Text <> "" And cmbResource.Text <> "-Select-" And cmbWC.Text = "-Select-" Then
                Dim Strx As String() = Split(cmbResource.SelectedItem.id, "||")
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped,A._Machineid  from tbjobtrans A,tbJobTransMain B where  A._Status='COMPLETED' and A._job=b._job and A._oper_num=B._oper_num   and  A._end_Date between " & dblSd & " and " & dblEd & " and B._wc in (select _wc from tbWorkStation where _rid=" & Val(Strx(1)) & ") order by A._Job"
            ElseIf cmbWC.Text <> "-Select-" Then
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped,A._Machineid  from tbjobtrans A,tbJobTransMain B where  A._Status='COMPLETED' and A._job=b._job and A._oper_num=B._oper_num   and  A._end_Date between " & dblSd & " and " & dblEd & " and B._wc ='" & Replace(cmbWC.SelectedItem.id, "'", "''") & "' order by A._Job"
            Else
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped,A._Machineid  from tbjobtrans A,tbJobTransMain B where  A._Status='COMPLETED' and A._job=b._job and A._oper_num=B._oper_num   and  A._end_Date between " & dblSd & " and " & dblEd & " order by A._Job"
            End If
            DSrep = clsM.GetDataset(Strsql, "tbJobTransMain")



            If cmbResource.Text <> "" And cmbResource.Text <> "-Select-" And cmbWC.Text = "-Select-" Then
                Dim Strx As String() = Split(cmbResource.SelectedItem.id, "||")
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped,A._Machineid  from tbjobtransHis A,tbJobTransMain B where  A._Status='COMPLETED' and A._job=b._job and A._oper_num=B._oper_num   and  A._end_Date between " & dblSd & " and " & dblEd & " and B._wc in (select _wc from tbWorkStation where _rid=" & Val(Strx(1)) & ") order by A._Job"
            ElseIf cmbWC.Text <> "-Select-" Then
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped,A._Machineid  from tbjobtransHis A,tbJobTransMain B where  A._Status='COMPLETED' and A._job=b._job and A._oper_num=B._oper_num   and  A._end_Date between " & dblSd & " and " & dblEd & " and B._wc ='" & Replace(cmbWC.SelectedItem.id, "'", "''") & "' order by A._Job"
            Else
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped,A._Machineid  from tbjobtransHis A,tbJobTransMain B where  A._Status='COMPLETED' and A._job=b._job and A._oper_num=B._oper_num   and  A._end_Date between " & dblSd & " and " & dblEd & " order by A._Job"
            End If
            DSrepHis = clsM.GetDataset(Strsql, "tbJobTransMain")





            Dim totqty As Double
            Dim drRp As DataRow
            Dim recount As Boolean = False
            PB1.Value = 0
            PB1.Minimum = 0
            PB1.Maximum = 0
            Dim IA As Integer = 0
            Dim j As Integer = 0
            If DSrep.Tables(0).Rows.Count > 0 Then
                Dim i As Integer

                PB1.Maximum = DSrep.Tables(0).Rows.Count + DSrepHis.Tables(0).Rows.Count

                For i = 0 To DSrep.Tables(0).Rows.Count - 1
                    With DSrep.Tables(0).Rows(i)
                        If .Item("_qty_complete") <> 0 Or .Item("_qty_scrapped") <> 0 Then
                            recount = True
                            Dim runt As Boolean = False
                            Dim strResCode As String = ""
                            Dim strResDesc As String = ""
                            Dim strWC As String = ""
                            Dim strWCdesc As String = ""
                            Dim ArrItem As String() = Split(.Item("_item"), "-")
                            Dim strCont As String = ArrItem(0)
                            If UCase(Mid(.Item("_item"), 1, 2)) = "SC" Then
                                strCont = .Item("_item")
                            End If

                            If HASRe.ContainsKey(strCont) = True Then
                                runt = True
                            Else
                                runt = False
                            End If


                            If runt = True Then


                                Dim strConst As String = ""
                                Dim DRResou() As DataRow = DSRecGroup.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                If DRResou.Length > 0 Then
                                    strResCode = DRResou(0).Item("_rGroupID")
                                    strResDesc = DRResou(0).Item("_rGroupName")
                                    strWC = DRResou(0).Item("_wc")
                                    strWCdesc = DRResou(0).Item("_description")
                                End If
                                Dim strMachine As String = .Item("_Machineid")
                                Dim DRdesc As DataRow() = DSItem.Tables(0).Select("_itemCode='" & .Item("_item") & "'")
                                Dim StrItemdesc As String = ""
                                If DRdesc.Length > 0 Then
                                    StrItemdesc = DRdesc(0).Item("_itemdescription")
                                End If
                                Dim strFindid As String = strResCode & "||" & strCont
                                If hashFindUnique.Contains(strFindid) = False Then

                                    ReDim Preserve ArrResouce(j)
                                    ReDim Preserve ArrResouceDesc(j)
                                    ReDim Preserve ArrWC(j)
                                    ReDim Preserve ArrWCDesc(j)
                                    ReDim Preserve Arrhr(j)
                                    ReDim Preserve ArrMint(j)
                                    ReDim Preserve ArrtotMint(j)
                                    ReDim Preserve ArrQty(j)
                                    'ReDim Preserve ArrMachine(j)
                                    ReDim Preserve ArrItemName(j)
                                    ReDim Preserve ArrItemDesc(j)
                                    ArrItemName(j) = strCont
                                    ArrItemDesc(j) = StrItemdesc
                                    hashFindUnique.Add(strFindid, j)
                                    ArrResouce(j) = strResCode
                                    ArrResouceDesc(j) = strResCode
                                    ArrWC(j) = strWC
                                    ArrWCDesc(j) = strWCdesc
                                    Dim sd, Ed As Date
                                    ' ArrMachine(j) = strMachine
                                    sd = DateTime.FromOADate(.Item("_start_Date"))
                                    Ed = DateTime.FromOADate(.Item("_end_Date"))
                                    ArrQty(j) = .Item("_qty_complete") + .Item("_qty_scrapped")
                                    Dim dblTot As Double = DateDiff(DateInterval.Second, sd, Ed)
                                    ArrtotMint(j) = dblTot
                                    Dim dblMint As Double = ArrtotMint(j) / 60
                                    'dblTot = DateDiff(DateInterval.Minute, sd, Ed)
                                    Dim dblHrs As Double = (ArrtotMint(j) / 60) / 60
                                    Arrhr(j) = dblHrs
                                    ArrMint(j) = dblMint
                                    j = j + 1
                                Else
                                    Dim m As Integer = hashFindUnique(strFindid)

                                    Dim sd, Ed As Date
                                    sd = DateTime.FromOADate(.Item("_start_Date"))
                                    Ed = DateTime.FromOADate(.Item("_end_Date"))
                                    Dim dblTot As Double = DateDiff(DateInterval.Second, sd, Ed)
                                    ArrtotMint(m) = ArrtotMint(m) + dblTot
                                    Dim dblMint As Double = ArrtotMint(m) / 60
                                    ArrQty(m) = ArrQty(m) + .Item("_qty_complete") + .Item("_qty_scrapped")
                                    'dblTot = DateDiff(DateInterval.Minute, sd, Ed)
                                    Dim dblHrs As Double = (ArrtotMint(m) / 60) / 60
                                    Arrhr(m) = dblHrs
                                    ArrMint(m) = dblMint
                                End If
                            End If
                        End If
                    End With
                    PB1.Value = PB1.Value + 1
                Next
            End If




            If DSrepHis.Tables(0).Rows.Count > 0 Then
                Dim i As Integer

                ' Dim j As Integer = 0
                For i = 0 To DSrepHis.Tables(0).Rows.Count - 1
                    With DSrepHis.Tables(0).Rows(i)
                        If .Item("_qty_complete") <> 0 Or .Item("_qty_scrapped") <> 0 Then
                            recount = True
                            Dim runt As Boolean = False
                            Dim strResCode As String = ""
                            Dim strResDesc As String = ""
                            Dim strWC As String = ""
                            Dim strWCdesc As String = ""
                            Dim ArrItem As String() = Split(.Item("_item"), "-")
                            Dim strCont As String = ArrItem(0)
                            If UCase(Mid(.Item("_item"), 1, 2)) = "SC" Then
                                strCont = .Item("_item")
                            End If

                            If HASRe.ContainsKey(strCont) = True Then
                                runt = True
                            Else
                                runt = False
                            End If


                            If runt = True Then

                                Dim strConst As String = ""
                                Dim DRResou() As DataRow = DSRecGroup.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                If DRResou.Length > 0 Then
                                    strResCode = DRResou(0).Item("_rGroupID")
                                    strResDesc = DRResou(0).Item("_rGroupName")
                                    strWC = DRResou(0).Item("_wc")
                                    strWCdesc = DRResou(0).Item("_description")
                                End If
                                Dim strMachine As String = DRResou(0).Item("_description")
                                Dim StrItemdesc As String = ""
                                Dim DRdesc As DataRow() = DSItem.Tables(0).Select("_itemCode='" & .Item("_item") & "'")
                                If DRdesc.Length > 0 Then
                                    StrItemdesc = DRdesc(0).Item("_itemdescription")
                                End If
                                Dim strFindid As String = strResCode & "||" & strCont
                                If hashFindUnique.Contains(strFindid) = False Then
                                    'If strResCode = "FURNCE" And strWC = "DEGRSE" Then
                                    '    MsgBox("Hi")

                                    'End If
                                    ReDim Preserve ArrResouce(j)
                                    ReDim Preserve ArrResouceDesc(j)
                                    ReDim Preserve ArrWC(j)
                                    ReDim Preserve ArrWCDesc(j)
                                    ReDim Preserve Arrhr(j)
                                    ReDim Preserve ArrMint(j)
                                    ReDim Preserve ArrtotMint(j)
                                    ReDim Preserve ArrQty(j)
                                    ' ReDim Preserve ArrMachine(j)
                                    ReDim Preserve ArrItemName(j)
                                    ReDim Preserve ArrItemDesc(j)
                                    ArrItemName(j) = strCont
                                    ArrItemDesc(j) = StrItemdesc
                                    hashFindUnique.Add(strFindid, j)
                                    ArrResouce(j) = strResCode
                                    ArrResouceDesc(j) = strResCode
                                    ArrWC(j) = strWC
                                    ArrWCDesc(j) = strWCdesc
                                    '   ArrMachine(j) = strMachine
                                    Dim sd, Ed As Date
                                    sd = DateTime.FromOADate(.Item("_start_Date"))
                                    Ed = DateTime.FromOADate(.Item("_end_Date"))
                                    Dim dblTot As Double = DateDiff(DateInterval.Second, sd, Ed)
                                    ArrQty(j) = .Item("_qty_complete") + .Item("_qty_scrapped")
                                    ArrtotMint(j) = dblTot
                                    Dim dblMint As Double = ArrtotMint(j) / 60
                                    'dblTot = DateDiff(DateInterval.Minute, sd, Ed)
                                    Dim dblHrs As Double = (ArrtotMint(j) / 60) / 60
                                    Arrhr(j) = dblHrs
                                    ArrMint(j) = dblMint
                                    j = j + 1
                                Else
                                    Dim m As Integer = hashFindUnique(strFindid)

                                    Dim sd, Ed As Date
                                    sd = DateTime.FromOADate(.Item("_start_Date"))
                                    Ed = DateTime.FromOADate(.Item("_end_Date"))
                                    Dim dblTot As Double = DateDiff(DateInterval.Second, sd, Ed)
                                    ArrQty(m) = ArrQty(m) + .Item("_qty_complete") + .Item("_qty_scrapped")
                                    ArrtotMint(m) = ArrtotMint(m) + dblTot
                                    Dim dblMint As Double = ArrtotMint(m) / 60
                                    'dblTot = DateDiff(DateInterval.Minute, sd, Ed)
                                    Dim dblHrs As Double = (ArrtotMint(m) / 60) / 60
                                    Arrhr(m) = dblHrs
                                    ArrMint(m) = dblMint
                                End If
                            End If
                        End If
                    End With
                    PB1.Value = PB1.Value + 1
                Next
            End If










            If recount = True Then
                Dim i As Integer
                For i = 0 To ArrResouce.Length - 1
                    drRp = dsrsg.Tables("tbCTResource").NewRow
                    drRp("sn") = i + 1
                    drRp("Title") = strReportDate ' Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                    drRp("CreateDate") = CreatedDate
                    drRp("ReportDate") = strReportDate
                    drRp("FGCode") = ArrItemName(i)
                    drRp("FGDesc") = ArrItemDesc(i)
                    drRp("Job") = ""
                    drRp("Machine") = "" 'ArrMachine(i)
                    drRp("OP") = ""
                    drRp("WC") = ArrWC(i)
                    drRp("WCDesc") = ArrWCDesc(i)
                    drRp("Resource") = ArrResouce(i)
                    drRp("ResourceDesc") = ArrResouceDesc(i)
                    drRp("hrvs") = Arrhr(i)
                    drRp("mints") = ArrMint(i)
                    drRp("Qty") = ArrQty(i)
                    drRp("perHr") = 0
                    drRp("PerMin") = 0
                    dsrsg.Tables("tbCTResource").Rows.Add(drRp)
                Next
            Else
                drRp = dsrsg.Tables("tbCTResource").NewRow
                drRp("sn") = 0
                drRp("Title") = strReportDate 'Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                drRp("CreateDate") = CreatedDate
                drRp("ReportDate") = strReportDate
                drRp("FGCode") = ""
                drRp("FGDesc") = ""
                drRp("Job") = ""
                drRp("OP") = ""
                drRp("WC") = ""
                drRp("WCDesc") = ""
                drRp("Machine") = ""
                drRp("Resource") = ""
                drRp("ResourceDesc") = ""
                drRp("Qty") = 0
                drRp("hrvs") = 0
                drRp("mints") = 0
                drRp("perHr") = 0
                drRp("PerMin") = 0
                dsrsg.Tables("tbCTResource").Rows.Add(drRp)
            End If
            '   If lblReportTit.Text = UCase("Actual Cycle Time by Resource Group(Summary)") Then
            ' Dim cry As New cryCyTimeResourceGroup
            Dim cry As New cryCyTimeSumResourceGroup
            cry.SetDataSource(dsrsg)
            RViewer.ReportSource = cry
            Panel1.Visible = False
            RViewer.Visible = True
            RViewer.Enabled = True
            Me.Activate()
            Application.DoEvents()
            RViewer.Refresh()
            butClose.Visible = True
            'Else
            'Dim cry As New cryCyTimeResourceGroup
            ''Dim cry As New cryCyTimeSumResourceGroup
            'cry.SetDataSource(dsrsg)
            'RViewer.ReportSource = cry
            'Panel1.Visible = False
            'RViewer.Visible = True
            'RViewer.Enabled = True
            'Me.Activate()
            'Application.DoEvents()
            'RViewer.Refresh()
            'butClose.Visible = True
            'End If


            ' butExport.Visible = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub



End Class