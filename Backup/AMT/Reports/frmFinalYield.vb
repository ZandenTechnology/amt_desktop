Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Public Class frmFinalYield
    Dim clsM As New clsMain
    Public DSItem As New DataSet
    Public DSWC As New DataSet
    Dim dsrsg As DSReport
    Dim ArrexOP() As Integer
    Dim ArrexWC() As String
    Dim ArrexItem() As String
    Dim strReportDate As String
    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        Try
            If Trim(txtFrom.Text) = "" Or Trim(txtTo.Text) = "" Then
                MsgBox("Please enter the date!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim dblSd As Double = clsM.CheckDate(txtFrom.Text)
            If dblSd = 0 Then
                MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If

            Dim dblEd As Double = clsM.CheckDate(txtTo.Text)
            If dblEd = 0 Then
                MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim s() As String = Split(txtTo.Text, "/")

            Dim Dateto As Date = DateSerial(s(2), s(1), s(0))
            Dateto = DateAdd(DateInterval.Minute, -1, DateAdd(DateInterval.Day, 1, Dateto))
            dblEd = 0
            dblEd = Dateto.ToOADate

            Dim HASRe As New Hashtable
            Dim StItem As Boolean = False
            If lstSelItem.Items.Count > 0 Then
                Dim k As Integer
                For k = 0 To lstSelItem.Items.Count - 1
                    HASRe.Add(lstSelItem.Items(k).id, lstSelItem.Items(k).id)
                Next
            Else
                StItem = True
                HASRe.Add("ALL", "ALL")

            End If
            strReportDate = Format(DateTime.FromOADate(dblSd), "dd MMM yyyy") & " - " & Format(DateTime.FromOADate(dblEd), "dd MMM yyyy") 'txtFrom.Text & "-" & txtTo.Text
            ' Dim strReportDate As String = txtFrom.Text & "-" & txtTo.Text
            Dim CreatedDate As String = Format(Now, "dd MMM yyyy")

            Dim DSrep As New DataSet
            Dim Strsql As String
            Dim strResource As String
            Dim DSRecGroup As New DataSet
            Dim DSItem As New DataSet
            DSItem = clsM.GetDataset("select * from tbItem", "tbItem")
            DSRecGroup = clsM.GetDataset("select A.*,B.* from tbWC A,tbResourceGroup B,tbWorkStation C where C._wc=A._wc and C._rid=B._rid", "tbRec")

            Strsql = "select * from tbJobTransMain where _end_Date between " & dblSd & " and " & dblEd & " and _nextoper_num=0"

            DSrep = clsM.GetDataset(Strsql, "tbJobTransMain")
            dsrsg = New DSReport
            Dim totqty As Double
            Dim drRp As DataRow
            Dim recount As Boolean = False
            PB1.Value = 0
            PB1.Minimum = 0
            PB1.Maximum = 0

            Dim hashFindUnique As New Hashtable
            Dim ArrTot(0) As Double
            Dim ArrRej(0) As Double
            Dim ArrPer(0) As Double
            Dim ArrCount(0) As Double
            ArrexOP = Nothing
            ArrexItem = Nothing
            ArrexWC = Nothing


            Dim IA As Integer = 0
            If DSrep.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                PB1.Maximum = DSrep.Tables(0).Rows.Count
                For i = 0 To DSrep.Tables(0).Rows.Count - 1
                    With DSrep.Tables(0).Rows(i)
                        Dim runt As Boolean = False
                        If StItem = True Then
                            runt = True
                        ElseIf HASRe.ContainsKey(.Item("_item")) = True Then
                            runt = True
                        Else
                            runt = False
                        End If
                        Dim DSTrasAll As DataSet
                        Dim dblsumrej As Double = 0
                        Dim dblsumCom As Double = 0
                        Dim dblsumRel As Double = 0
                        Dim dblsumTot As Double = 0
                        dblsumrej = 0
                        DSTrasAll = clsM.GetDataset("select Sum(_qty_scrapped) as stqty from tbJobTransMain where _job='" & .Item("_job") & "'", "tbJobTransMain")
                        If DSTrasAll.Tables(0).Rows.Count > 0 Then
                            If DSTrasAll.Tables(0).Rows(0).Item("stqty") <> 0 Then
                                dblsumrej = DSTrasAll.Tables(0).Rows(0).Item("stqty")
                            End If
                        End If
                        dblsumRel = .Item("_qty_Rele_qty")
                        dblsumCom = .Item("_qty_complete")
                        dblsumTot = dblsumCom + dblsumrej
                        Dim Totst As Boolean = False
                        If dblsumRel = dblsumTot Then
                            Totst = True
                        End If
                        If runt = True And Totst = True Then
                            If .Item("_qty_complete") <> 0 Or .Item("_qty_Rele_qty") <> 0 Then
                                recount = True
                                Dim StrItemCode As String = ""
                                Dim StrItemName As String = ""
                                Dim StrRecName As String = ""
                                Dim StrWCCode As String = ""
                                Dim StrWCName As String = ""
                                Dim dblQty As Double
                                Dim StrOP As String = ""
                                StrOP = .Item("_oper_num")


                                StrWCCode = .Item("_WC")
                                Dim DRResou() As DataRow = DSRecGroup.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                If DRResou.Length > 0 Then
                                    StrRecName = DRResou(0).Item("_rGroupID")
                                    StrWCName = DRResou(0).Item("_description")
                                End If
                                Dim DRItem() As DataRow = DSItem.Tables(0).Select("_itemCode='" & .Item("_item") & "'")

                                StrItemCode = .Item("_item")
                                If DRItem.Length > 0 Then
                                    StrItemName = DRItem(0).Item("_itemdescription")
                                End If


                                Dim strID As String = StrItemCode
                                Dim ArrstrID As String = StrItemCode

                                If hashFindUnique.ContainsKey(strID) = False Then
                                    hashFindUnique.Add(strID, IA)
                                    ReDim Preserve ArrTot(IA)
                                    ReDim Preserve ArrRej(IA)
                                    ReDim Preserve ArrPer(IA)
                                    ReDim Preserve ArrCount(IA)
                                    'If StrItemCode = "AC0023" Then
                                    '    MsgBox("Hi")
                                    'End If
                                    ReDim Preserve ArrexOP(IA)
                                    ReDim Preserve ArrexItem(IA)
                                    ReDim Preserve ArrexWC(IA)
                                    ArrexOP(IA) = Val(StrOP)
                                    ArrexItem(IA) = .Item("_item")
                                    ArrexWC(IA) = StrWCName

                                    ArrTot(IA) = .Item("_qty_complete")
                                    ArrRej(IA) = dblsumrej '.Item("_qty_scrapped")

                                    Dim dblCom1 As Double = .Item("_qty_complete") + dblsumrej
                                    ArrPer(IA) = Format(((.Item("_qty_complete") * 100) / dblCom1), "00.00")
                                    ArrCount(IA) = ArrCount(IA) + 1
                                    IA = IA + 1
                                Else
                                    Dim strNV As Integer = hashFindUnique(strID)
                                    ArrTot(strNV) = ArrTot(strNV) + .Item("_qty_complete")
                                    ArrRej(strNV) = ArrRej(strNV) + dblsumrej

                                    Dim dblCom1 As Double = .Item("_qty_complete") + dblsumrej
                                    ArrPer(strNV) = ArrPer(strNV) + Format(((.Item("_qty_complete") * 100) / dblCom1), "00.00")
                                    ArrCount(strNV) = ArrCount(strNV) + 1


                                End If







                                drRp = dsrsg.Tables("tbYield").NewRow
                                drRp("sno") = i + 1
                                drRp("Title") = strReportDate 'Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                                drRp("CreateDate") = CreatedDate
                                drRp("ReportDate") = strReportDate
                                Dim StrArr() As String = Split(StrItemName, ",")
                                drRp("SelRSCGroup") = StrRecName
                                drRp("StockCard") = ""
                                drRp("RSCGroup") = StrRecName
                                drRp("FGCode") = StrItemCode
                                drRp("FGDesc") = StrArr(0)
                                drRp("Job") = UCase(.Item("_job"))
                                drRp("Author") = "NA"
                                drRp("PerOPDate") = "NA"
                                drRp("OP") = StrOP
                                drRp("WCDesc") = StrWCName
                                drRp("ComQty") = .Item("_qty_complete")
                                drRp("RejQty") = dblsumrej
                                drRp("RelQty") = .Item("_qty_Rele_qty")
                                totqty = totqty + .Item("_qty_complete")

                                Dim dblPer As Double = (.Item("_qty_complete") * 100) / .Item("_qty_Rele_qty")

                                drRp("Total") = totqty
                                drRp("per") = dblPer
                                dsrsg.Tables("tbYield").Rows.Add(drRp)

                            End If
                        End If
                    End With
                    PB1.Value = i + 1
                    Application.DoEvents()
                Next
            End If
            If recount = False Then
                drRp = dsrsg.Tables("tbYield").NewRow
                Dim StrItemCode As String = ""
                Dim StrItemName As String = ""
                Dim StrRecName As String = ""
                Dim StrWCCode As String = ""
                Dim StrWCName As String = ""
                Dim dblQty As Double
                Dim StrOP As String = ""
                StrOP = 0
                StrWCCode = ""
                drRp = dsrsg.Tables("tbYield").NewRow
                drRp("sno") = 0
                drRp("Title") = strReportDate 'Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                drRp("CreateDate") = CreatedDate
                drRp("ReportDate") = strReportDate
                drRp("SelRSCGroup") = ""
                drRp("StockCard") = ""
                drRp("RSCGroup") = StrRecName
                drRp("FGCode") = StrItemCode

                drRp("FGDesc") = StrItemName
                drRp("Job") = ""
                drRp("Author") = "NA"
                drRp("PerOPDate") = "NA"
                drRp("OP") = StrOP
                drRp("WCDesc") = ""
                totqty = 0
                drRp("ComQty") = 0
                drRp("RejQty") = 0
                drRp("RelQty") = 0
                totqty = 0



                drRp("Total") = totqty
                drRp("per") = 0
                dsrsg.Tables("tbYield").Rows.Add(drRp)
            End If
            Dim cry As New cryFinalYield

            If dsrsg.Tables("tbYield").Rows.Count > 0 Then
                Dim i As Integer
                For i = 0 To dsrsg.Tables("tbYield").Rows.Count - 1

                    Dim strFind As String
                    strFind = dsrsg.Tables("tbYield").Rows(i).Item("FGCode")
                    If (strFind = "AC0023") Then
                        MsgBox("Hi")
                    End If
                    Dim strNV As Integer = hashFindUnique(strFind)
                    Dim IntTot As Double
                    IntTot = ArrPer(strNV)
                    Dim IntCnt As Integer
                    IntCnt = ArrCount(strNV)
                    'If IntCnt > 1 Then
                    'MsgBox(IntCnt)
                    'End If
                    '//Edit 20100928
                    'If IntCnt > 0 Then
                    '    dsrsg.Tables("tbYield").Rows(i).Item("totPer") = IntTot / IntCnt
                    'Else
                    '    dsrsg.Tables("tbYield").Rows(i).Item("totPer") = IntTot
                    'End If
                    IntTot = 0
                    Dim dblRelqtytot As Double = ArrTot(strNV) + ArrRej(strNV)
                    Dim dblRejqtytot As Double = ArrRej(strNV)
                    Dim dblComqtytot As Double = ArrTot(strNV)
                    If dblRelqtytot > 0 Then
                        dsrsg.Tables("tbYield").Rows(i).Item("totPer") = (dblComqtytot * 100) / dblRelqtytot
                    Else
                        dsrsg.Tables("tbYield").Rows(i).Item("totPer") = IntTot
                    End If


                    dsrsg.Tables("tbYield").Rows(i).Item("Total") = totqty
                Next
            End If




            cry.SetDataSource(dsrsg)
            RViewer.ReportSource = cry
            butClose.Visible = True
            butExport.Visible = True
            RViewer.Refresh()
            Panel1.Visible = False
            RViewer.Visible = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
        TmVari = 2
    End Sub

    Private Sub frmFinalYield_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dtpFrom.Value = Now
        dtpTo.Value = Now
        txtFrom.Text = Format(Now, "dd/MM/yyyy")
        txtTo.Text = Format(Now, "dd/MM/yyyy")

        GetItem()
    End Sub

    Sub GetItem()
        DSItem.Tables.Clear()
        lstSelItem.Items.Clear()
        lstItem.Items.Clear()
        Try
            Dim ds As New DataSet
            'ds = clsM.GetDataset("select * from tbItem where  order by _itemCode", "tbItem")
            ds = clsM.GetDataset("select * from tbItem where _type<>'O' and (_itemCode like('F%') or _itemCode like('FG%') or _itemCode like('AC%') or  _itemCode like('SC%')) order by _itemCode", "tbItem")
            '
            DSItem = ds
            If IsDBNull(ds) = False Then
                Dim i As Integer
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            '  If UCase(Mid(.Item("_itemCode"), 1, 2)) <> "SC" Then
                            'Dim st As String() = Split(.Item("_itemCode"), "-")
                            'If st.Length = 1 Then
                            'lstItem.Items.Add(New DataItem(Trim(.Item("_itemCode")), Trim(.Item("_itemCode")) & "  :  " & Trim(.Item("_itemdescription")) & ""))
                            'End If
                            'Else
                            lstItem.Items.Add(New DataItem(Trim(.Item("_itemCode")), Trim(.Item("_itemCode")) & "  :  " & Trim(.Item("_itemdescription")) & ""))
                            'End If
                        End With
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try

    End Sub


    Private Sub butAssSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssSingle.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstItem.SelectedItems.Count - 1
                lstSelItem.Items.Add(lstItem.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstItem.SelectedItems.Count - 1
                lstItem.Items.Remove(lstItem.SelectedItems.Item(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butAssMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssMulti.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstItem.Items.Count - 1
                lstSelItem.Items.Add(lstItem.Items(i))
            Next
            i = 0
            For i = 0 To lstItem.Items.Count - 1
                lstItem.Items.Remove(lstItem.Items(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butDelSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelSingle.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstSelItem.SelectedItems.Count - 1
                lstItem.Items.Add(lstSelItem.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstSelItem.SelectedItems.Count - 1
                lstSelItem.Items.Remove(lstSelItem.SelectedItems.Item(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub
    Private Sub butDelMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelMulti.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstSelItem.Items.Count - 1
                lstItem.Items.Add(lstSelItem.Items(i))
            Next
            i = 0
            For i = 0 To lstSelItem.Items.Count - 1
                lstSelItem.Items.Remove(lstSelItem.Items(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        dtpFrom.Value = Now
        dtpTo.Value = Now
    End Sub
    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtTo.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        dtpTo.Checked = False
    End Sub
    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtFrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False
    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub
    'Private Sub RViewer_ClickPage(ByVal sender As Object, ByVal e As CrystalDecisions.Windows.Forms.PageMouseEventArgs) Handles RViewer.ClickPage
    '    TmVari = 2
    'End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        RViewer.DataBindings.Clear()
        GC.GetGeneration(RViewer)
        RViewer.Visible = False
        butClose.Visible = False
        butExport.Visible = False
        PB1.Value = 0
        PB1.Minimum = 0
        PB1.Maximum = 0
        Panel1.Visible = True
    End Sub



    Private Sub butExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExport.Click
        ExportDatatoExcel()
    End Sub
    Function ExportDatatoExcel() As Boolean

        Dim txtFile As String
        txtFile = ""
        sfdSave.FileName = ""
        sfdSave.Filter = "Excel File|*.xls"
        sfdSave.Title = "Save an Excel File"
        If sfdSave.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtFile = sfdSave.FileName
        Else
            Exit Function
        End If


        Panel3.Visible = True
        PB2.Value = 0
        PB2.Minimum = 0
        PB2.Maximum = 0
        Dim myTrans As System.Data.SqlClient.SqlTransaction

        Dim delFile As Boolean = False
        '= "C:\ZandenTemp\ComReport.xls"
        Try
            If IO.File.Exists(txtFile) Then
                IO.File.Delete(txtFile)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Function
        Finally
        End Try
        ' Dim at As Integer
        If txtFile = "" Then
            MsgBox("Please enter the file name!", MsgBoxStyle.Information, "eWIP")
            Exit Function
        End If

        Dim str, filename As String
        str = ""
        filename = ""
        Dim col, row As Integer
        col = 0
        row = 0
        Dim strHisNo As String = ""
        Dim strAcrNo As String = ""
        Dim valHis As String
        Dim strHisVal As String = ""
        Dim strCurVal As String = ""
        Dim localAll As Process()
        localAll = Process.GetProcesses()
        Dim EXhash As New Hashtable
        For i As Integer = 0 To localAll.Length - 1
            If localAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(localAll(i).Id) = False Then
                    EXhash.Add(localAll(i).Id, localAll(i).Id)
                End If
            End If
        Next

        Dim Excel As Object = CreateObject("Excel.Application")


        P1.Visible = True
        Try
            Dim i As Integer = 1
            With Excel
                .SheetsInNewWorkbook = 1
                .Workbooks.Add()
                .Worksheets(1).Select()

                'Dim chartRange As Microsoft.Office.Interop.Excel.Range

                'chartRange = .Range("A1", "G1")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "FINAL YIELD REPORT"
                'chartRange.HorizontalAlignment = 3
                'chartRange.VerticalAlignment = 3

                'chartRange = .Range("A2", "D2")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "Created Date :" & Format(Now, "dd MMM yyyy")
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1

                'chartRange = .Range("A3", "D3")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = ""
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1


                'chartRange = .Range("E2", "G2")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "Reported Date :" & strReportDate '& txtFrom.Text & "-" & txtTo.Text
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1

                'chartRange = .Range("E3", "G3")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "" '"Total :" & dsrsg.Tables(0).Rows.Count
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1


                '.cells(4, 1).MergeCells = True
                '.cells(4, 1).EntireRow.Font.Bold = True
                '.cells(4, 1).value = "FG Code"

                '.cells(4, 2).MergeCells = True
                '.cells(4, 2).EntireRow.Font.Bold = True
                '.cells(4, 2).value = "FG Description"


                '.cells(4, 3).MergeCells = True
                '.cells(4, 3).EntireRow.Font.Bold = True
                '.cells(4, 3).value = "Job"

                '.cells(4, 4).MergeCells = True
                '.cells(4, 4).EntireRow.Font.Bold = True
                '.cells(4, 4).value = "Release Qty"

                '.cells(4, 5).MergeCells = True
                '.cells(4, 5).EntireRow.Font.Bold = True
                '.cells(4, 5).value = "Completed Qty"

                '.cells(4, 6).MergeCells = True
                '.cells(4, 6).EntireRow.Font.Bold = True
                '.cells(4, 6).value = "Rejected Qty"

                '.cells(4, 7).MergeCells = True
                '.cells(4, 7).EntireRow.Font.Bold = True
                '.cells(4, 7).value = "%"




                i = 4

                i += 1

            End With
            Application.DoEvents()
            Dim AllTot As Double = 0
            If dsrsg.Tables("tbYield").Rows.Count > 0 Then
                PB2.Maximum = ArrexWC.Length
                Dim M1 As Integer
                Array.Sort(ArrexOP)
                Array.Sort(ArrexItem)
                Array.Sort(ArrexWC)
                Dim HashCheck As New Hashtable
                For M1 = 0 To ArrexWC.Length - 1
                    Dim N1 As Integer
                    For N1 = 0 To ArrexItem.Length - 1
                        Dim j1 As Integer
                        For j1 = 0 To ArrexOP.Length - 1

                            Dim strChV As String = ArrexItem(N1)
                            If HashCheck.ContainsKey(strChV) = False Then
                                Dim RWdata() As DataRow = dsrsg.Tables("tbYield").Select("FGCode='" & ArrexItem(N1) & "'", "FGCode,Job")
                                If RWdata.Length > 0 Then
                                    Dim k1 As Integer

                                    Dim strDe As String = ""
                                    Dim strComQ As Double = 0
                                    Dim strRejQ As Double = 0
                                    Dim strPerQ As Double = 0
                                    Dim strrelQ As Double = 0
                                    '  Dim strWC As String = ""
                                    Dim strFGCode As String = ""
                                    Dim strFGDesc As String = ""
                                    For k1 = 0 To RWdata.Length - 1
                                        With RWdata(k1)
                                            Excel.cells(i, 1).value = .Item("FGCode")
                                            strFGCode = .Item("FGCode")
                                            Excel.cells(i, 2).value = .Item("FGDesc") 'FGDesc
                                            strFGDesc = .Item("FGDesc")
                                            Excel.cells(i, 3).value = .Item("Job")
                                            Excel.cells(i, 4).value = .Item("RelQty")
                                            Excel.cells(i, 5).value = .Item("ComQty")
                                            Excel.cells(i, 6).value = .Item("RejQty")
                                            Excel.cells(i, 7).value = .Item("Per")
                                            strDe = .Item("WCDesc")
                                            strComQ = strComQ + .Item("ComQty")
                                            strRejQ = strRejQ + .Item("RejQty")
                                            strPerQ = Format(.Item("totPer"), "00.00")
                                            strrelQ = strrelQ + .Item("RelQty")
                                            i = i + 1
                                        End With
                                    Next
                                    'Dim chartRange As Excel.Range
                                    'Excel.Range("A" & i, "H" & i).Font.ColorIndex = 3
                                    ''chartRange.Interior.ColorIndex = 27
                                    'Excel.cells(i, 1).value = strFGCode
                                    'Excel.cells(i, 2).value = strFGDesc
                                    'chartRange = Excel.Range("C" & i, "C" & i)
                                    'chartRange.Merge()
                                    'chartRange.FormulaR1C1 = ""
                                    'chartRange.HorizontalAlignment = 3
                                    'chartRange.VerticalAlignment = 3
                                    Excel.cells(i, 4).value = strrelQ
                                    Excel.cells(i, 5).value = strComQ
                                    Excel.cells(i, 6).value = strRejQ
                                    Excel.cells(i, 7).value = Format(strPerQ, "00.00")
                                    AllTot = AllTot + strComQ

                                    strrelQ = 0
                                    strRejQ = 0
                                    strComQ = 0

                                    i = i + 1




                                End If
                                HashCheck.Add(strChV, strChV)

                            End If
                        Next
                    Next
                    PB2.Value = M1 + 1
                Next

            End If
            Excel.cells(3, 5) = "Total :" & AllTot
            filename = txtFile
            Excel.ActiveCell.Worksheet.SaveAs(filename)
            System.Runtime.InteropServices.Marshal.ReleaseComObject(Excel)
            Excel = Nothing



            MsgBox("Data's are exported to Excel Succesfully in '" & filename & "'", MsgBoxStyle.Information, "eWIP")

            '  lblDataV.Text = "Data has been Successfully Exported"




        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "eWIP")
            delFile = True


        Finally
            Panel3.Visible = False
            cn.Close()
            com.Parameters.Clear()
        End Try

        ' The excel is created and opened for insert value. We most close this excel using this system
        'Dim pro() As Process = System.Diagnostics.Process.GetProcessesByName("EXCEL")

        'For Each i As Process In pro
        '    i.Kill()
        'Next

        Dim DellocalAll As Process()

        DellocalAll = Process.GetProcesses()


        For i As Integer = 0 To DellocalAll.Length - 1
            If DellocalAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(DellocalAll(i).Id) = False Then
                    DellocalAll(i).Kill()
                End If
            End If
        Next



        If delFile = True Then
            Try
                If IO.File.Exists(filename) Then
                    IO.File.Delete(filename)
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End Try
        End If
        txtFile = ""


    End Function
End Class