Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Microsoft.VisualBasic.DateAndTime
Public Class frmRptCompletedTransaction
    Dim clsM As New clsMain
    Public DSItem As New DataSet
    Public DSWC As New DataSet
    Dim dsrsg As DSReport
    Dim ArrexWeek() As Integer
    Dim ArrexOP() As Integer
    Dim ArrexRes() As String
    Dim ArrexItem() As String
    Dim ArrHasAll() As String
    Dim strReportDate As String
    Private Sub frmRptCompletedTransaction_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      
        lblReportTit.Text = "CO BY RESOURCE GROUP"

        dtpFrom.Value = Now
        dtpTo.Value = Now
        txtFrom.Text = Format(Now, "dd/MM/yyyy")
        txtTo.Text = Format(Now, "dd/MM/yyyy")
        ResourceData()
        GetWC()
        GetItem()
    End Sub

    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtTo.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        dtpTo.Checked = False
    End Sub
    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtFrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False
    End Sub
    Sub GetWC()
        cmbWC.Items.Clear()
        DSWC.Tables.Clear()
        cmbWC.Items.Add(New DataItem("-Select-", "-Select-"))
        Dim selcon As String = ""
        Dim sql As String = ""
        If cmbResource.Text <> "-Select-" Then
            sql = "select * from tbWC where _wc in(select _WC from tbWorkStation where _rid in(select _rid from tbResourceGroup where _rGroupid='" & Trim(cmbResource.Text) & "')) order by _wc"

        Else
            sql = "select * from tbWC order by _wc"
        End If
        Try
            Dim ds As New DataSet
            ds = clsM.GetDataset(sql, "tbWC")
            DSWC = ds
            If IsDBNull(ds) = False Then
                Dim i As Integer
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            cmbWC.Items.Add(New DataItem(Trim(.Item("_wc")), Trim(.Item("_wc")) & "  :  " & Trim(.Item("_description")) & ""))
                        End With
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
        cmbWC.Text = "-Select-"
    End Sub
    Sub ResourceData()
        Dim ds As New DataSet
        cmbResource.Items.Clear()
        cmbResource.Items.Add(New DataItem("-Select-", "-Select-"))
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                'Dim DI As DataItem

                ds = clsM.GetDataset("select _rGroupid,_rGroupName,_rid from tbResourceGroup order by _rGroupid", "tbWC")
                If IsDBNull(ds) = False Then
                    Dim i As Integer
                    If ds.Tables(0).Rows.Count > 0 Then
                        For i = 0 To ds.Tables(0).Rows.Count - 1
                            With ds.Tables(0).Rows(i)
                                cmbResource.Items.Add(New DataItem(.Item("_rGroupName") & "||" & .Item("_rid"), .Item("_rGroupid")))
                            End With
                        Next
                    End If
                End If
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
        cmbResource.Text = "-Select-"
    End Sub
    Sub GetItem()
        DSItem.Tables.Clear()
        lstSelItem.Items.Clear()
        lstItem.Items.Clear()
        Try
            Dim ds As New DataSet
            'ds = clsM.GetDataset("select * from tbItem where  order by _itemCode", "tbItem")
            ds = clsM.GetDataset("select * from tbItem where _type<>'O' and( _itemCode like('F%') or _itemCode like('FG%') or _itemCode like('AC%') or  _itemCode like('SC%')) order by _itemCode", "tbItem")
            '
            DSItem = ds
            If IsDBNull(ds) = False Then
                Dim i As Integer
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            ' If UCase(Mid(.Item("_itemCode"), 1, 2)) <> "SC" Then
                            'Dim st As String() = Split(.Item("_itemCode"), "-")
                            'If st.Length = 1 Then
                            'lstItem.Items.Add(New DataItem(Trim(.Item("_itemCode")), Trim(.Item("_itemCode")) & "  :  " & Trim(.Item("_itemdescription")) & ""))
                            'End If
                            'Else
                            lstItem.Items.Add(New DataItem(Trim(.Item("_itemCode")), Trim(.Item("_itemCode")) & "  :  " & Trim(.Item("_itemdescription")) & ""))
                            'End If
                        End With
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try

    End Sub
    Private Sub cmbResource_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbResource.SelectedIndexChanged
        GetWC()
    End Sub
    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub
    Private Sub butAssSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssSingle.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstItem.SelectedItems.Count - 1
                lstSelItem.Items.Add(lstItem.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstItem.SelectedItems.Count - 1
                lstItem.Items.Remove(lstItem.SelectedItems.Item(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butAssMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssMulti.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstItem.Items.Count - 1
                lstSelItem.Items.Add(lstItem.Items(i))
            Next
            i = 0
            For i = 0 To lstItem.Items.Count - 1
                lstItem.Items.Remove(lstItem.Items(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butDelSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelSingle.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstSelItem.SelectedItems.Count - 1
                lstItem.Items.Add(lstSelItem.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstSelItem.SelectedItems.Count - 1
                lstSelItem.Items.Remove(lstSelItem.SelectedItems.Item(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub
    Private Sub butDelMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelMulti.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstSelItem.Items.Count - 1
                lstItem.Items.Add(lstSelItem.Items(i))
            Next
            i = 0
            For i = 0 To lstSelItem.Items.Count - 1
                lstSelItem.Items.Remove(lstSelItem.Items(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        dtpFrom.Value = Now
        dtpTo.Value = Now
        ResourceData()
        GetWC()
        GetItem()
    End Sub
    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
     
        comletedTransaction()
        TmVari = 2

        '  Parent.Focus()

    End Sub
    Sub comletedTransaction()
        Try
            If Trim(txtFrom.Text) = "" Or Trim(txtTo.Text) = "" Then
                MsgBox("Please enter the date!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim dblSd As Double = clsM.CheckDate(txtFrom.Text)
            If dblSd = 0 Then
                MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If

            Dim dblEd As Double = clsM.CheckDate(txtTo.Text)
            If dblEd = 0 Then
                MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim s() As String = Split(txtTo.Text, "/")
            dsrsg = New DSReport
            Dim Dateto As Date = DateSerial(s(2), s(1), s(0))
            Dateto = DateAdd(DateInterval.Minute, -1, DateAdd(DateInterval.Day, 1, Dateto))
            dblEd = 0
            dblEd = Dateto.ToOADate
            strReportDate = Format(DateTime.FromOADate(dblSd), "dd MMM yyyy") & " - " & Format(DateTime.FromOADate(dblEd), "dd MMM yyyy")
            Dim HASRe As New Hashtable
            Dim StItem As Boolean = False
            If lstSelItem.Items.Count > 0 Then
                Dim k As Integer
                For k = 0 To lstSelItem.Items.Count - 1
                    HASRe.Add(lstSelItem.Items(k).id, lstSelItem.Items(k).id)
                Next
            Else
                StItem = True
                HASRe.Add("ALL", "ALL")

            End If

            'Dim strReportDate As String = txtFrom.Text & "-" & txtTo.Text
            Dim CreatedDate As String = Format(Now, "dd MMM yyyy")

            Dim DSrep As New DataSet
            Dim Strsql As String
            Dim strResource As String
            Dim DSRecGroup As New DataSet
            Dim DSItem As New DataSet
            DSItem = clsM.GetDataset("select * from tbItem", "tbItem")
            Dim DSRecWC As New DataSet
            DSRecWC = clsM.GetDataset("select * from tbWC", "tbWC")
            DSRecGroup = clsM.GetDataset("select A.*,B.* from tbWC A,tbResourceGroup B,tbWorkStation C where C._wc=A._wc and C._rid=B._rid", "tbRec")

            Dim hashFindUnique As New Hashtable
            Dim ArrTot(0) As Double
            Dim ArrRej(0) As Double
            Dim ArrPer(0) As Double
            Dim ArrCount(0) As Double

            ArrexOP = Nothing
            ArrexItem = Nothing
            ArrexRes = Nothing
            ArrexWeek = Nothing
            ArrHasAll = Nothing

            If cmbResource.Text <> "" And cmbResource.Text <> "-Select-" And cmbWC.Text = "-Select-" Then
                Dim Strx As String() = Split(cmbResource.SelectedItem.id, "||")
                Strsql = "select * from tbJobTransMain where _end_Date between " & dblSd & " and " & dblEd & " and _wc in (select _wc from tbWorkStation where _rid=" & Val(Strx(1)) & ") order by _Job"
            ElseIf cmbWC.Text <> "-Select-" Then
                Strsql = "select * from tbJobTransMain where _end_Date between " & dblSd & " and " & dblEd & " and _wc ='" & Replace(cmbWC.SelectedItem.id, "'", "''") & "' order by _Job"
            Else
                Strsql = "select * from tbJobTransMain where _end_Date between " & dblSd & " and " & dblEd & " order by _Job"
            End If
            DSrep = clsM.GetDataset(Strsql, "tbJobTransMain")

            Dim totqty As Double
            Dim drRp As DataRow
            Dim recount As Boolean = False
            PB1.Value = 0
            PB1.Minimum = 0
            PB1.Maximum = 0
            Dim IA As Integer = 0
            If DSrep.Tables(0).Rows.Count > 0 Then
                Dim i As Integer

                PB1.Maximum = DSrep.Tables(0).Rows.Count
                For i = 0 To DSrep.Tables(0).Rows.Count - 1

                    With DSrep.Tables(0).Rows(i)
                        Dim runt As Boolean = False
                        Dim ArrItem As String() = Split(.Item("_item"), "-")
                        Dim strCont As String = ArrItem(0)
                        If UCase(Mid(.Item("_item"), 1, 2)) = "SC" Then
                            strCont = .Item("_item")
                        End If

                        'If ArrItem.Length > 1 Then
                        '    MsgBox(.Item("_item"))
                        'End If
                        If StItem = True Then
                            runt = True
                        ElseIf HASRe.ContainsKey(strCont) = True Then
                            runt = True
                        Else
                            runt = False
                        End If
                        If runt = True Then
                            If .Item("_qty_complete") <> 0 Then
                                recount = True
                                Dim StrItemCode As String = ""
                                Dim StrItemName As String = ""
                                Dim StrRecName As String = ""
                                Dim StrWCCode As String = ""
                                Dim StrWCName As String = ""
                                Dim dblQty As Double
                                Dim StrOP As String = ""
                                StrOP = .Item("_oper_num")
                                StrWCCode = .Item("_WC")
                                Dim DRResou() As DataRow = DSRecGroup.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                If DRResou.Length > 0 Then
                                    StrRecName = DRResou(0).Item("_rGroupID")
                                    StrWCName = DRResou(0).Item("_description")

                                Else
                                    Dim DRResouNew() As DataRow = DSRecWC.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                    If DRResouNew.Length > 0 Then
                                        StrWCName = DRResouNew(0).Item("_description")
                                    End If
                                    StrRecName = ""
                                End If
                                Dim DRItem() As DataRow = DSItem.Tables(0).Select("_itemCode='" & .Item("_item") & "'")

                                StrItemCode = .Item("_item")
                                If DRItem.Length > 0 Then
                                    StrItemName = DRItem(0).Item("_itemdescription")
                                End If
                                Dim WeelCount As Integer = GetWeek(DateTime.FromOADate(.Item("_end_Date")))
                                Dim strID As String = WeelCount & "||" & StrRecName & "||" & StrItemCode & "||" & CStr(StrOP)
                                Dim ArrstrID As String = StrRecName & "||" & StrItemCode & "||"

                                'check for Export
                                If hashFindUnique.ContainsKey(strID) = False Then
                                    ReDim Preserve ArrHasAll(IA)
                                    hashFindUnique.Add(strID, IA)
                                    ArrHasAll(IA) = strID
                                    ReDim Preserve ArrTot(IA)
                                    ReDim Preserve ArrRej(IA)
                                    ReDim Preserve ArrPer(IA)
                                    ReDim Preserve ArrCount(IA)

                                    ReDim Preserve ArrexOP(IA)
                                    ReDim Preserve ArrexItem(IA)
                                    ReDim Preserve ArrexRes(IA)
                                    ReDim Preserve ArrexWeek(IA)
                                    ArrexOP(IA) = Val(StrOP)
                                    ArrexItem(IA) = .Item("_item")
                                    ArrexRes(IA) = StrRecName
                                    ArrexWeek(IA) = WeelCount

                                    ArrTot(IA) = .Item("_qty_complete")
                                    ArrRej(IA) = .Item("_qty_scrapped")

                                    Dim dblCom1 As Double = .Item("_qty_complete") + .Item("_qty_scrapped")
                                    ArrPer(IA) = Format(((.Item("_qty_complete") * 100) / dblCom1), "00.00")
                                    ArrCount(IA) = ArrCount(IA) + 1
                                    IA = IA + 1
                                Else
                                    Dim strNV As Integer = hashFindUnique(strID)
                                    ArrTot(strNV) = .Item("_qty_complete")
                                    ArrRej(strNV) = .Item("_qty_scrapped")

                                    Dim dblCom1 As Double = .Item("_qty_complete") + .Item("_qty_scrapped")
                                    ArrPer(strNV) = ArrPer(strNV) + Format(((.Item("_qty_complete") * 100) / dblCom1), "00.00")
                                    ArrCount(strNV) = ArrCount(strNV) + 1


                                End If
                                'end Check

                                drRp = dsrsg.Tables("tbRSCGroupReport").NewRow
                                drRp("sno") = i + 1
                                drRp("Title") = strReportDate ' Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                                drRp("CreateDate") = CreatedDate
                                drRp("ReportDate") = strReportDate
                                Dim StrArr() As String = Split(StrItemName, ",")
                                drRp("SelRSCGroup") = StrRecName
                                drRp("StockCard") = ""
                                drRp("RSCGroup") = StrRecName
                                drRp("FGCode") = StrItemCode
                                drRp("FGDesc") = StrArr(0)
                                drRp("Job") = UCase(.Item("_job"))
                                drRp("Sdate") = Format(DateTime.FromOADate(.Item("_TransDate")), "d MMM yyyy HH:mm")
                                drRp("Edate") = Format(DateTime.FromOADate(.Item("_end_Date")), "d MMM yyyy HH:mm")
                                drRp("OP") = StrOP
                                drRp("WCDesc") = StrWCName
                                drRp("Qty") = .Item("_qty_complete")
                                drRp("Rej") = .Item("_qty_scrapped")
                                drRp("weekd") = WeelCount
                                Dim dblCom As Double = .Item("_qty_complete") + .Item("_qty_scrapped")
                                Dim dblPer As Double = 0
                                If dblCom <> 0 Then
                                    dblPer = Format(((.Item("_qty_complete") * 100) / dblCom), "00.00")
                                Else
                                    dblPer = 0.0
                                End If

                                drRp("Per") = Val(Format(dblPer, "00.00"))
                                totqty = totqty + .Item("_qty_complete")
                                drRp("Total") = totqty

                                dsrsg.Tables("tbRSCGroupReport").Rows.Add(drRp)

                            End If
                        End If
                    End With
                    PB1.Value = i + 1
                    Application.DoEvents()
                Next
            End If
            If recount = False Then
                drRp = dsrsg.Tables("tbRSCGroupReport").NewRow
                Dim StrItemCode As String = ""
                Dim StrItemName As String = ""
                Dim StrRecName As String = ""
                Dim StrWCCode As String = ""
                Dim StrWCName As String = ""
                Dim dblQty As Double
                Dim StrOP As String = ""
                StrOP = 0
                StrWCCode = ""
                drRp = dsrsg.Tables("tbRSCGroupReport").NewRow
                drRp("sno") = 0
                drRp("Title") = strReportDate ' Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                drRp("CreateDate") = CreatedDate
                drRp("ReportDate") = strReportDate
                drRp("SelRSCGroup") = ""
                drRp("StockCard") = ""
                drRp("RSCGroup") = StrRecName
                drRp("FGCode") = StrItemCode

                drRp("FGDesc") = StrItemName
                drRp("Job") = ""
                drRp("Sdate") = ""
                drRp("Edate") = ""
                drRp("OP") = val(StrOP)
                drRp("WCDesc") = ""
                drRp("Qty") = 0
                totqty = 0
                drRp("Per") = 0
                drRp("Total") = 0
                drRp("weekd") = 0
                hashFindUnique.Add("", 0)
                ReDim Preserve ArrTot(0)
                ReDim Preserve ArrRej(0)
                ReDim Preserve ArrPer(0)
                ReDim Preserve ArrCount(0)
                ArrTot(0) = 0
                ArrRej(0) = 0







                dsrsg.Tables("tbRSCGroupReport").Rows.Add(drRp)
            End If
            Dim cry As New cryRecGroupReport
            dsrsg.Tables(0).DefaultView.Sort = "SelRSCGroup,FGCode,OP ASC"

            If dsrsg.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For i = 0 To dsrsg.Tables("tbRSCGroupReport").Rows.Count - 1
                    Dim strFind As String
                    strFind = dsrsg.Tables("tbRSCGroupReport").Rows(i).Item("weekd") & "||" & dsrsg.Tables("tbRSCGroupReport").Rows(i).Item("SelRSCGroup") & "||" & dsrsg.Tables("tbRSCGroupReport").Rows(i).Item("FGCode") & "||" & CStr(dsrsg.Tables("tbRSCGroupReport").Rows(i).Item("OP"))
                    'If strFind = "46||MACHIN||FG0003C||600" Then
                    '    MsgBox(strFind)
                    'End If
                    Dim strNV As Integer = hashFindUnique(strFind)
                    Dim IntTot As Double
                    IntTot = ArrPer(strNV)
                    Dim IntCnt As Integer
                    IntCnt = ArrCount(strNV)
                    'If IntCnt > 1 Then
                    'MsgBox(IntCnt)
                    'End If
                    If IntCnt > 0 Then
                        dsrsg.Tables("tbRSCGroupReport").Rows(i).Item("totPer") = IntTot / IntCnt
                    Else
                        dsrsg.Tables("tbRSCGroupReport").Rows(i).Item("totPer") = IntTot
                    End If

                    dsrsg.Tables("tbRSCGroupReport").Rows(i).Item("Total") = totqty
                Next
            End If




            cry.SetDataSource(dsrsg)
            RViewer.ReportSource = cry
            Panel1.Visible = False
            RViewer.Visible = True
            RViewer.Enabled = True
            Me.Activate()
            Application.DoEvents()
            RViewer.Refresh()
            butClose.Visible = True
            butExport.Visible = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub



    Sub FinalYield()
        Try


            If Trim(txtFrom.Text) = "" Or Trim(txtTo.Text) = "" Then
                MsgBox("Please enter the date!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim dblSd As Double = clsM.CheckDate(txtFrom.Text)
            If dblSd = 0 Then
                MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If

            Dim dblEd As Double = clsM.CheckDate(txtTo.Text)
            If dblEd = 0 Then
                MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim s() As String = Split(txtTo.Text, "/")

            Dim Dateto As Date = DateSerial(s(2), s(1), s(0))
            Dateto = DateAdd(DateInterval.Minute, -1, DateAdd(DateInterval.Day, 1, Dateto))
            dblEd = 0
            dblEd = Dateto.ToOADate

            Dim HASRe As New Hashtable
            Dim StItem As Boolean = False
            If lstSelItem.Items.Count > 0 Then
                Dim k As Integer
                For k = 0 To lstSelItem.Items.Count - 1
                    HASRe.Add(lstSelItem.Items(k).id, lstSelItem.Items(k).id)
                Next
            Else
                StItem = True
                HASRe.Add("ALL", "ALL")

            End If

            strReportDate = Format(DateTime.FromOADate(dblSd), "dd MMM yyyy") & " - " & Format(DateTime.FromOADate(dblEd), "dd MMM yyyy")
            Dim CreatedDate As String = Format(Now, "dd MMM yyyy")

            Dim DSrep As New DataSet
            Dim Strsql As String
            Dim strResource As String
            Dim DSRecGroup As New DataSet
            Dim DSItem As New DataSet
            DSItem = clsM.GetDataset("select * from tbItem", "tbItem")
            DSRecGroup = clsM.GetDataset("select A.*,B.* from tbWC A,tbResourceGroup B,tbWorkStation C where C._wc=A._wc and C._rid=B._rid", "tbRec")
            If cmbResource.Text <> "" And cmbResource.Text <> "-Select-" And cmbWC.Text = "-Select-" Then
                Dim Strx As String() = Split(cmbResource.SelectedItem.id, "||")
                Strsql = "select * from tbJobTransMain where _end_Date between " & dblSd & " and " & dblEd & " and _wc in (select _wc from tbWorkStation where _rid=" & Val(Strx(1)) & ")"
            ElseIf cmbWC.Text <> "-Select-" Then
                Strsql = "select * from tbJobTransMain where _end_Date between " & dblSd & " and " & dblEd & " and _wc ='" & Replace(cmbWC.SelectedItem.id, "'", "''") & "')"
            Else
                Strsql = "select * from tbJobTransMain where _end_Date between " & dblSd & " and " & dblEd
            End If
            DSrep = clsM.GetDataset(Strsql, "tbJobTransMain")
            Dim dsrsg As New DSReport
            Dim totqty As Double
            Dim drRp As DataRow
            Dim recount As Boolean = False
            PB1.Value = 0
            PB1.Minimum = 0
            PB1.Maximum = 0
            If DSrep.Tables(0).Rows.Count > 0 Then
                Dim i As Integer

                PB1.Maximum = DSrep.Tables(0).Rows.Count
                For i = 0 To DSrep.Tables(0).Rows.Count - 1

                    With DSrep.Tables(0).Rows(i)
                        Dim runt As Boolean = False
                        If StItem = True Then
                            runt = True
                        ElseIf HASRe.ContainsKey(.Item("_item")) = True Then
                            runt = True
                        Else
                            runt = False
                        End If
                        If runt = True Then
                            If .Item("_qty_complete") <> 0 Then
                                recount = True
                                Dim StrItemCode As String = ""
                                Dim StrItemName As String = ""
                                Dim StrRecName As String = ""
                                Dim StrWCCode As String = ""
                                Dim StrWCName As String = ""
                                Dim dblQty As Double
                                Dim StrOP As String = ""
                                StrOP = .Item("_oper_num")
                                StrWCCode = .Item("_WC")
                                Dim DRResou() As DataRow = DSRecGroup.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                If DRResou.Length > 0 Then
                                    StrRecName = DRResou(0).Item("_rGroupID")
                                    StrWCName = DRResou(0).Item("_description")
                                End If
                                Dim DRItem() As DataRow = DSItem.Tables(0).Select("_itemCode='" & .Item("_item") & "'")

                                StrItemCode = .Item("_item")
                                If DRItem.Length > 0 Then
                                    StrItemName = DRItem(0).Item("_itemdescription")
                                End If

                                drRp = dsrsg.Tables("tbRSCGroupReport").NewRow
                                drRp("sno") = i + 1
                                drRp("Title") = Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                                drRp("CreateDate") = CreatedDate
                                drRp("ReportDate") = strReportDate
                                Dim StrArr() As String = Split(StrItemName, ",")
                                drRp("SelRSCGroup") = StrRecName
                                drRp("StockCard") = ""
                                drRp("RSCGroup") = StrRecName
                                drRp("FGCode") = StrItemCode
                                drRp("FGDesc") = StrArr(0)
                                drRp("Job") = UCase(.Item("_job"))
                                drRp("Author") = "NA"
                                drRp("PerOPDate") = "NA"
                                drRp("OP") = StrOP
                                drRp("WCDesc") = StrWCName
                                drRp("Qty") = .Item("_qty_complete")
                                totqty = totqty + .Item("_qty_complete")
                                drRp("Total") = totqty
                                dsrsg.Tables("tbRSCGroupReport").Rows.Add(drRp)

                            End If
                        End If
                    End With
                    PB1.Value = i + 1
                    Application.DoEvents()
                Next
            End If
            If recount = False Then
                drRp = dsrsg.Tables("tbRSCGroupReport").NewRow
                Dim StrItemCode As String = ""
                Dim StrItemName As String = ""
                Dim StrRecName As String = ""
                Dim StrWCCode As String = ""
                Dim StrWCName As String = ""
                Dim dblQty As Double
                Dim StrOP As String = ""
                StrOP = 0
                StrWCCode = ""
                drRp = dsrsg.Tables("tbRSCGroupReport").NewRow
                drRp("sno") = 0
                drRp("Title") = strReportDate ' Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                drRp("CreateDate") = CreatedDate
                drRp("ReportDate") = strReportDate
                drRp("SelRSCGroup") = ""
                drRp("StockCard") = ""
                drRp("RSCGroup") = StrRecName
                drRp("FGCode") = StrItemCode

                drRp("FGDesc") = StrItemName
                drRp("Job") = ""
                drRp("Author") = "NA"
                drRp("PerOPDate") = "NA"
                drRp("OP") = StrOP
                drRp("WCDesc") = ""
                drRp("Qty") = 0
                totqty = 0
                drRp("Total") = 0
                dsrsg.Tables("tbRSCGroupReport").Rows.Add(drRp)
            End If
            Dim cry As New cryRecGroupReport

            If dsrsg.Tables("tbRSCGroupReport").Rows.Count > 0 Then
                Dim i As Integer
                For i = 0 To dsrsg.Tables("tbRSCGroupReport").Rows.Count - 1
                    dsrsg.Tables("tbRSCGroupReport").Rows(i).Item("Total") = totqty
                Next
            End If




            cry.SetDataSource(dsrsg)
            RViewer.ReportSource = cry
            RViewer.Visible = True
            Panel1.Visible = False
            RViewer.FindForm.Focus()


            Application.DoEvents()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    'Private Sub RViewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RViewer.Click

    'End Sub

    'Private Sub RViewer_ClickPage(ByVal sender As Object, ByVal e As CrystalDecisions.Windows.Forms.PageMouseEventArgs) Handles RViewer.ClickPage
    '    '  Me.RViewer.TopLevelControl.Focus()
    '    ' Me.WindowState = FormWindowState.Maximized


    '    ' RViewer.m()
    '    TmVari = 2
    'End Sub

    'Private Sub RViewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RViewer.Click
    '    MessageBox.Show(e.ToString)
    'End Sub



    'Private Sub RViewer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RViewer.Load

    'End Sub

    
    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        RViewer.DataBindings.Clear()
        GC.GetGeneration(RViewer)
        RViewer.Visible = False
        butClose.Visible = False
        PB1.Value = 0
        PB1.Minimum = 0
        PB1.Maximum = 0
        butExport.Visible = False
        Panel1.Visible = True
    End Sub



    Function ExportDatatoExcel() As Boolean

        Dim txtFile As String
        txtFile = ""
        sfdSave.FileName = ""
        sfdSave.Filter = "Excel File|*.xls"
        sfdSave.Title = "Save an Excel File"
        If sfdSave.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtFile = sfdSave.FileName
        Else
            Exit Function
        End If


        Panel3.Visible = True
        PB2.Value = 0
        PB2.Minimum = 0
        PB2.Maximum = 0
        Dim myTrans As System.Data.SqlClient.SqlTransaction

        Dim delFile As Boolean = False
        '= "C:\ZandenTemp\ComReport.xls"
        Try
            If IO.File.Exists(txtFile) Then
                IO.File.Delete(txtFile)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Function
        Finally
        End Try
        ' Dim at As Integer
        If txtFile = "" Then
            MsgBox("Please enter the file name!", MsgBoxStyle.Information, "eWIP")
            Exit Function
        End If

        Dim str, filename As String
        str = ""
        filename = ""
        Dim col, row As Integer
        col = 0
        row = 0
        Dim strHisNo As String = ""
        Dim strAcrNo As String = ""
        Dim valHis As String
        Dim strHisVal As String = ""
        Dim strCurVal As String = ""
        Dim localAll As Process()

        localAll = Process.GetProcesses()
        Dim lstOldProcessID As New ListBox
        Dim EXhash As New Hashtable
        For i As Integer = 0 To localAll.Length - 1
            If localAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(localAll(i).Id) = False Then
                    EXhash.Add(localAll(i).Id, localAll(i).Id)
                End If
            End If
        Next



        Dim Excel As Object = CreateObject("Excel.Application")


        P1.Visible = True
        Try
            Dim i As Integer = 1
            With Excel
                .SheetsInNewWorkbook = 1
                .Workbooks.Add()
                .Worksheets(1).Select()

                'Dim chartRange As Excel.Range

                'chartRange = .Range("A1", "L1")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "COMPLETED OPERATION BY RESOURCE GROUP"
                'chartRange.HorizontalAlignment = 3
                'chartRange.VerticalAlignment = 3

                'chartRange = .Range("A2", "F2")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "Created Date :" & Format(Now, "dd MMM yyyy")
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1

                'chartRange = .Range("A3", "F3")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = ""
                'chartRange.HorizontalAlignment = 3
                'chartRange.VerticalAlignment = 3

                'chartRange = .Range("G2", "L2")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "Reported Date :" & strReportDate 'txtFrom.Text & "-" & txtTo.Text
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1

                'chartRange = .Range("G3", "L3")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "" '"Total :" & dsrsg.Tables(0).Rows.Count
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1


                ''   .cells(3, 1).MergeCells = True
                ''  .cells(3, 1).EntireRow.Font.Bold = True
                '' .cells(3, 1).value = "Week"
                'chartRange = .Range("A3", "F3")
                .cells(4, 1).MergeCells = True
                .cells(4, 1).EntireRow.Font.Bold = True
                .cells(4, 1).value = "Week"

                .cells(4, 2).MergeCells = True
                .cells(4, 2).EntireRow.Font.Bold = True
                .cells(4, 2).value = "Resource group"

                .cells(4, 3).MergeCells = True
                .cells(4, 3).EntireRow.Font.Bold = True
                .cells(4, 3).value = "FG Code"

                .cells(4, 4).MergeCells = True
                .cells(4, 4).EntireRow.Font.Bold = True
                .cells(4, 4).value = "FG Description"

                .cells(4, 5).MergeCells = True
                .cells(4, 5).EntireRow.Font.Bold = True
                .cells(4, 5).value = "Job"

                .cells(4, 6).MergeCells = True
                .cells(4, 6).EntireRow.Font.Bold = True
                .cells(4, 6).value = "Transfer Date"

                .cells(4, 7).MergeCells = True
                .cells(4, 7).EntireRow.Font.Bold = True
                .cells(4, 7).value = "Completed Date"

                .cells(4, 8).MergeCells = True
                .cells(4, 8).EntireRow.Font.Bold = True
                .cells(4, 8).value = "OP"

                .cells(4, 9).MergeCells = True
                .cells(4, 9).EntireRow.Font.Bold = True
                .cells(4, 9).value = "WC Description"

                .cells(4, 10).MergeCells = True
                .cells(4, 10).EntireRow.Font.Bold = True
                .cells(4, 10).value = "Com Qty"

                .cells(4, 11).MergeCells = True
                .cells(4, 11).EntireRow.Font.Bold = True
                .cells(4, 11).value = "Rej QTY"

                .cells(4, 12).MergeCells = True
                .cells(4, 12).EntireRow.Font.Bold = True
                .cells(4, 12).value = "%"




                i = 4

                i += 1

            End With
            Dim AllTot As Double = 0
            Application.DoEvents()
            If dsrsg.Tables(0).Rows.Count > 0 Then
                PB2.Maximum = ArrHasAll.Length
                Dim M1 As Integer
                Array.Sort(ArrexWeek)
                Array.Sort(ArrexOP)
                Array.Sort(ArrexItem)
                Array.Sort(ArrexRes)
                Dim HashCheck As New Hashtable

                Array.Sort(ArrHasAll)
                For M1 = 0 To ArrHasAll.Length - 1
                    Dim strChV As String = ArrHasAll(M1)
                    If HashCheck.ContainsKey(strChV) = False Then
                        Dim S() As String = Split(strChV, "||")
                        Dim RWdata() As DataRow = dsrsg.Tables(0).Select("weekd=" & Val(S(0)) & " and  RSCGroup='" & S(1) & "' and FGCode='" & S(2) & "' and OP=" & S(3), "weekd,RSCGroup,FGCode,OP,Job")


                        If RWdata.Length > 0 Then
                            Dim k1 As Integer

                            Dim strDe As String = ""
                            Dim strComQ As Double = 0
                            Dim strRejQ As Double = 0
                            Dim strPerQ As Double = 0
                            Dim stW As Integer = 0
                            Dim stRS As String = ""
                            Dim stFG As String = ""
                            Dim stFGD As String = ""
                            For k1 = 0 To RWdata.Length - 1
                                With RWdata(k1)
                                    stW = .Item("weekd")
                                    stRS = .Item("RSCGroup")
                                    stFG = .Item("FGCode")
                                    stFGD = .Item("FGDesc")
                                    Excel.cells(i, 1).value = .Item("weekd")
                                    Excel.cells(i, 2).value = .Item("RSCGroup")
                                    Excel.cells(i, 3).value = .Item("FGCode")
                                    Excel.cells(i, 4).value = .Item("FGDesc")
                                    Excel.cells(i, 5).value = .Item("Job")
                                    Excel.cells(i, 6).value = "'" & .Item("Sdate") 'DateTime.FromOADate(.Item("Sdate"))
                                    Excel.cells(i, 7).value = "'" & .Item("Edate") 'DateTime.FromOADate(.Item("Edate"))
                                    Excel.cells(i, 8).value = .Item("OP")
                                    Excel.cells(i, 9).value = .Item("WCDesc")
                                    Excel.cells(i, 10).value = .Item("Qty")
                                    Excel.cells(i, 11).value = .Item("Rej")
                                    ' Excel.cells(i, 10).value = .Item("Total")
                                    Excel.cells(i, 12).value = .Item("Per")
                                    strDe = .Item("WCDesc")
                                    strComQ = strComQ + .Item("Qty")
                                    strRejQ = strRejQ + .Item("Rej")
                                    strPerQ = .Item("totPer")

                                    i = i + 1
                                End With
                            Next


                            'Dim chartRange1 As Excel.Range
                            '' chartRange1 = Excel.Range("A" & i, "L" & i)
                            'Excel.Range("A" & i, "L" & i).Font.ColorIndex = 3


                            'Dim chartRange As Excel.Range
                            'Excel.cells(i, 1).value = stW

                            'Excel.cells(i, 2).value = stRS
                            'Excel.cells(i, 3).value = stFG
                            'chartRange = Excel.Range("D" & i, "G" & i)
                            'chartRange.HorizontalAlignment = 1
                            'chartRange.VerticalAlignment = 1
                            'chartRange.Merge()
                            'chartRange.FormulaR1C1 = stFGD


                            'chartRange.Interior.ColorIndex = 27
                            AllTot = AllTot + strComQ


                            'chartRange.FormulaR1C1 = ""

                            Excel.cells(i, 8).value = S(3)
                            'Excel.cells(i, 7).Interior.ColorIndex = 27
                            Excel.cells(i, 9).value = strDe
                            'Excel.cells(i, 8).Interior.ColorIndex = 27
                            Excel.cells(i, 10).value = strComQ
                            'Excel.cells(i, 9).Interior.ColorIndex = 27
                            Excel.cells(i, 11).value = strRejQ
                            'Excel.cells(i, 10).Interior.ColorIndex = 27
                            Excel.cells(i, 12).value = strPerQ
                            ' Excel.cells(i, 11).Interior.ColorIndex = 27



                            i = i + 1




                        End If
                        HashCheck.Add(strChV, strChV)






                    End If
                    PB2.Value = M1 + 1
                Next

                'For M1 = 0 To ArrexRes.Length - 1
                '    Dim N1 As Integer
                '    For N1 = 0 To ArrexItem.Length - 1
                '        Dim j1 As Integer
                '        For j1 = 0 To ArrexOP.Length - 1

                '            Dim strChV As String = ArrexWeek(M1) & "||" & ArrexRes(M1) & "||" & ArrexItem(N1) & "||" & ArrexOP(j1)
                '            If HashCheck.ContainsKey(strChV) = False Then
                '                Dim RWdata() As DataRow = dsrsg.Tables(0).Select("weekd=" & ArrexWeek(M1) & " and  RSCGroup='" & ArrexRes(M1) & "' and FGCode='" & ArrexItem(N1) & "' and OP=" & ArrexOP(j1), "RSCGroup,FGCode,OP")
                '                If RWdata.Length > 0 Then
                '                    Dim k1 As Integer

                '                    Dim strDe As String = ""
                '                    Dim strComQ As Double = 0
                '                    Dim strRejQ As Double = 0
                '                    Dim strPerQ As Double = 0
                '                    Dim stW As Integer = 0
                '                    Dim stRS As String = ""
                '                    Dim stFG As String = ""
                '                    Dim stFGD As String = ""
                '                    For k1 = 0 To RWdata.Length - 1
                '                        With RWdata(k1)
                '                            stW = .Item("weekd")
                '                            stRS = .Item("RSCGroup")
                '                            stFG = .Item("FGCode")
                '                            stFGD = .Item("FGDesc")
                '                            Excel.cells(i, 1).value = .Item("weekd")
                '                            Excel.cells(i, 2).value = .Item("RSCGroup")
                '                            Excel.cells(i, 3).value = .Item("FGCode")
                '                            Excel.cells(i, 4).value = .Item("FGDesc")
                '                            Excel.cells(i, 5).value = .Item("Job")
                '                            Excel.cells(i, 6).value = .Item("Sdate") 'DateTime.FromOADate(.Item("Sdate"))
                '                            Excel.cells(i, 7).value = .Item("Edate") 'DateTime.FromOADate(.Item("Edate"))
                '                            Excel.cells(i, 8).value = .Item("OP")
                '                            Excel.cells(i, 9).value = .Item("WCDesc")
                '                            Excel.cells(i, 10).value = .Item("Qty")
                '                            Excel.cells(i, 11).value = .Item("Rej")
                '                            ' Excel.cells(i, 10).value = .Item("Total")
                '                            Excel.cells(i, 12).value = .Item("Per")
                '                            strDe = .Item("WCDesc")
                '                            strComQ = strComQ + .Item("Qty")
                '                            strRejQ = strRejQ + .Item("Rej")
                '                            strPerQ = .Item("totPer")

                '                            i = i + 1
                '                        End With
                '                    Next


                '                    Dim chartRange1 As Excel.Range
                '                    ' chartRange1 = Excel.Range("A" & i, "L" & i)
                '                    Excel.Range("A" & i, "L" & i).Font.ColorIndex = 3


                '                    Dim chartRange As Excel.Range
                '                    Excel.cells(i, 1).value = stW

                '                    Excel.cells(i, 2).value = stRS
                '                    Excel.cells(i, 3).value = stFG
                '                    chartRange = Excel.Range("D" & i, "G" & i)
                '                    chartRange.HorizontalAlignment = 1
                '                    chartRange.VerticalAlignment = 1
                '                    chartRange.Merge()
                '                    chartRange.FormulaR1C1 = stFGD


                '                    'chartRange.Interior.ColorIndex = 27
                '                    AllTot = AllTot + strComQ


                '                    'chartRange.FormulaR1C1 = ""

                '                    Excel.cells(i, 8).value = ArrexOP(j1)
                '                    'Excel.cells(i, 7).Interior.ColorIndex = 27
                '                    Excel.cells(i, 9).value = strDe
                '                    'Excel.cells(i, 8).Interior.ColorIndex = 27
                '                    Excel.cells(i, 10).value = strComQ
                '                    'Excel.cells(i, 9).Interior.ColorIndex = 27
                '                    Excel.cells(i, 11).value = strRejQ
                '                    'Excel.cells(i, 10).Interior.ColorIndex = 27
                '                    Excel.cells(i, 12).value = strPerQ
                '                    ' Excel.cells(i, 11).Interior.ColorIndex = 27



                '                    i = i + 1




                '                End If
                '                HashCheck.Add(strChV, strChV)

                '            End If
                '        Next
                '    Next
                '    PB2.Value = M1 + 1
                'Next

            End If
            Excel.cells(3, 7) = "Total : " & AllTot

            filename = txtFile
            Excel.ActiveCell.Worksheet.SaveAs(filename)
            System.Runtime.InteropServices.Marshal.ReleaseComObject(Excel)
            Excel = Nothing



            MsgBox("Data's are exported to Excel Succesfully in '" & filename & "'", MsgBoxStyle.Information, "eWIP")

            '  lblDataV.Text = "Data has been Successfully Exported"




        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "eWIP")
            delFile = True


        Finally
            Panel3.Visible = False
            cn.Close()
            com.Parameters.Clear()
        End Try

        ' The excel is created and opened for insert value. We most close this excel using this system
        'Dim pro() As Process = System.Diagnostics.Process.GetProcessesByName("EXCEL")

        'For Each i As Process In pro
        '    i.Kill()
        'Next


        Dim DellocalAll As Process()

        DellocalAll = Process.GetProcesses()


        For i As Integer = 0 To DellocalAll.Length - 1
            If DellocalAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(DellocalAll(i).Id) = False Then
                    DellocalAll(i).Kill()
                End If
            End If
        Next



        If delFile = True Then
            Try
                If IO.File.Exists(filename) Then
                    IO.File.Delete(filename)
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End Try
        End If
        txtFile = ""


    End Function

    Private Sub butExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExport.Click

       

        ExportDatatoExcel()
    End Sub


    Function GetWeek(ByVal sd As Date) As Integer
        Dim allcont As Integer
        Try


            Dim GetD As Date = DateSerial(Year(Now), 1, 1)
            Dim FirstDate As String = UCase(Format(GetD, "ddd"))
            Dim FistWeek As Integer = 0
            Dim CalFirstDate As Date
            If FirstDate = "SUN" Then
                CalFirstDate = GetD
                FistWeek = 0
            ElseIf FirstDate = "MON" Then
                CalFirstDate = DateAdd(DateInterval.Day, 6, GetD)
                FistWeek = 1
            ElseIf FirstDate = "TUE" Then
                CalFirstDate = DateAdd(DateInterval.Day, 5, GetD)
                FistWeek = 1
            ElseIf FirstDate = "WED" Then
                CalFirstDate = DateAdd(DateInterval.Day, 4, GetD)
                FistWeek = 1
            ElseIf FirstDate = "THU" Then
                CalFirstDate = DateAdd(DateInterval.Day, 3, GetD)
                FistWeek = 1
            ElseIf FirstDate = "FRI" Then
                CalFirstDate = DateAdd(DateInterval.Day, 2, GetD)
                FistWeek = 1
            ElseIf FirstDate = "SAT" Then
                CalFirstDate = DateAdd(DateInterval.Day, 1, GetD)
                FistWeek = 1
            End If
            Dim GetD1 As Date = DateSerial(Year(sd), Month(sd), Day(sd))
            Dim intday As Integer = DateDiff(DateInterval.Day, CalFirstDate, GetD1) + 1
            Dim countWeek As Double
            If intday = 0 Then
                countWeek = 1
            Else
                countWeek = (intday / 7) + FistWeek
            End If
            Dim S() As String = Split(CStr(countWeek), ".")

            If S.Length > 1 Then
                If Val(S(1)) <> 0 Then
                    allcont = Val(S(0)) + 1
                Else
                    allcont = Val(S(0))
                End If
            Else
                allcont = Val(S(0))
            End If

        Catch ex As Exception
            allcont = 0
        End Try
        Return allcont
    End Function


  
End Class