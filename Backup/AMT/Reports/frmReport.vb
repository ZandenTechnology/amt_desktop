Imports System.Data.Sql
Public Class frmReport
    Dim clsM As New clsMain
    Public DSItem As New DataSet
    Public DSWC As New DataSet
    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtFrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False
    End Sub
    Private Sub frmReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dtpFrom.Value = Now
        dtpTo.Value = Now
        ResourceData()
        GetWC()
        GetItem()
        '  Me.RViewer.RefreshReport()


    End Sub
    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtTo.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        dtpTo.Checked = False
    End Sub
    Sub GetWC()
        cmbWC.Items.Clear()
        DSWC.Tables.Clear()
        cmbWC.Items.Add(New DataItem("-Select-", "-Select-"))
        Dim selcon As String = ""
        Dim sql As String = ""
        If cmbResource.Text <> "-Select-" Then
            sql = "select * from tbWC where _wc in(select _WC from tbWorkStation where _rid in(select _rid from tbResourceGroup where _rGroupid='" & Trim(cmbResource.Text) & "')) order by _wc"

        Else
            sql = "select * from tbWC order by _wc"
        End If
        Try
            Dim ds As New DataSet
            ds = clsM.GetDataset(sql, "tbWC")
            DSWC = ds
            If IsDBNull(ds) = False Then
                Dim i As Integer
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            cmbWC.Items.Add(New DataItem(Trim(.Item("_wc")), Trim(.Item("_wc")) & "{" & Trim(.Item("_description")) & "}"))
                        End With
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
        cmbWC.Text = "-Select-"
    End Sub
    Sub ResourceData()
        Dim ds As New DataSet
        cmbResource.Items.Clear()
        cmbResource.Items.Add(New DataItem("-Select-", "-Select-"))
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                'Dim DI As DataItem

                ds = clsM.GetDataset("select _rGroupid,_rGroupName,_rid from tbResourceGroup order by _rGroupName", "tbWC")
                If IsDBNull(ds) = False Then
                    Dim i As Integer
                    If ds.Tables(0).Rows.Count > 0 Then
                        For i = 0 To ds.Tables(0).Rows.Count - 1
                            With ds.Tables(0).Rows(i)
                                cmbResource.Items.Add(New DataItem(.Item("_rGroupName") & "||" & .Item("_rid"), .Item("_rGroupid")))
                            End With
                        Next
                    End If
                End If
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
        cmbResource.Text = "-Select-"
    End Sub
    Sub GetItem()
        DSItem.Tables.Clear()
        lstSelItem.Items.Clear()
        lstItem.Items.Clear()
        Try
            Dim ds As New DataSet
            ds = clsM.GetDataset("select * from tbItem order by _itemCode", "tbItem")
            DSItem = ds
            If IsDBNull(ds) = False Then
                Dim i As Integer
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            lstItem.Items.Add(New DataItem(Trim(.Item("_itemCode")), Trim(.Item("_itemCode")) & "{" & Trim(.Item("_itemdescription")) & "}"))

                        End With
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try

    End Sub
    Private Sub cmbResource_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbResource.SelectedIndexChanged
        GetWC()
    End Sub
    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub
    Private Sub butAssSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssSingle.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstItem.SelectedItems.Count - 1
                lstSelItem.Items.Add(lstItem.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstItem.SelectedItems.Count - 1
                lstItem.Items.Remove(lstItem.SelectedItems.Item(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butAssMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssMulti.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstItem.Items.Count - 1
                lstSelItem.Items.Add(lstItem.Items(i))
            Next
            i = 0
            For i = 0 To lstItem.Items.Count - 1
                lstItem.Items.Remove(lstItem.Items(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butDelSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelSingle.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstSelItem.SelectedItems.Count - 1
                lstItem.Items.Add(lstSelItem.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstSelItem.SelectedItems.Count - 1
                lstSelItem.Items.Remove(lstSelItem.SelectedItems.Item(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub
    Private Sub butDelMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelMulti.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstSelItem.Items.Count - 1
                lstItem.Items.Add(lstSelItem.Items(i))
            Next
            i = 0
            For i = 0 To lstSelItem.Items.Count - 1
                lstSelItem.Items.Remove(lstSelItem.Items(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        dtpFrom.Value = Now
        dtpTo.Value = Now
        ResourceData()
        GetWC()
        GetItem()
    End Sub
    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        Try
            ' Dim cls As New clsMain
            If Trim(txtFrom.Text) = "" Or (txtTo.Text) = "" Then
                MsgBox("Enter the date!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim sdate As Double
            Dim Edate As Double
            sdate = clsM.CheckDate(txtFrom.Text)
            Edate = clsM.CheckDate(txtTo.Text)
            If sdate = 0 Then
                MsgBox("please check from date!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            If Edate = 0 Then
                MsgBox("please check to date!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Edate = Edate + 1
            Dim DSRecGroup As New DataSet
            DSRecGroup = clsM.GetDataset("select A.*,B.* from tbWC A,tbResourceGroup B,tbWorkStation C where C._wc=A._wc and C._rid=B._rid", "tbRec")

            Dim contstr As String = " where _end_Date between " & sdate & " and " & Edate
            Dim DSJobRpt As New DataSet
            DSJobRpt = clsM.GetDataset("select _job,_oper_num from tbJobTrans where _job in (select distinct _job from tbJobtrans " & contstr & ") group by _job,_oper_num", "tbItem")
            Dim sql As String = "select * from tbJobtrans where _job in (select distinct _job from tbJobtrans " & contstr & ")"
            Dim DSJobAll As New DataSet
            DSJobAll = clsM.GetDataset("select * from tbJobtrans where _job in (select distinct _job from tbJobtrans " & contstr & ")", "tbItem")
            Dim DSJobHis As New DataSet
            DSJobHis = clsM.GetDataset("select * from DSJobHis where _job in (select distinct _job from tbJobtrans " & contstr & ")", "tbItem")
            Dim dsrsg As New DSReport
            Dim drRp As DataRow
            If DSJobRpt.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For i = 0 To DSJobRpt.Tables(0).Rows.Count - 1
                    Dim StrItemCode As String = ""
                    Dim StrItemName As String = ""
                    Dim StrRecName As String = ""
                    Dim StrWCCode As String = ""
                    Dim StrWCName As String = ""
                    Dim StrOP As String = ""
                    Dim DRAll As DataRow() = DSJobAll.Tables(0).Select("_Job='" & DSJobRpt.Tables(0).Rows(i).Item("_job") & "' and _oper_num=" & DSJobRpt.Tables(0).Rows(i).Item("_oper_num"))
                    ' Dim DRHis As DataRow() = DSJobHis.Tables(0).Select("_Job='" & DSJobRpt.Tables(0).Rows(i).Item("_job") & "' and _oper_num=" & DSJobRpt.Tables(0).Rows(i).Item("_oper_num"))
                    Dim jobtotqty As Double = 0
                    Dim jobComqty As Double = 0
                    Dim jobtotRjct As Double = 0

                    Dim jobRwkTot As Double = 0
                    Dim jobRwkcom As Double = 0
                    Dim jobRwkRej As Double = 0

                    Dim j As Integer
                    If DRAll.Length > 0 Then
                        For j = 0 To DRAll.Length - 1
                            With DRAll(j)
                                StrItemCode = .Item("_item")
                                StrOP = .Item("_oper_num")
                                StrWCCode = .Item("_wc")
                                If UCase(.Item("_reworkst")) = "R" Then
                                    jobRwkTot = jobRwkTot + .Item("_qty_op_qty")
                                    jobRwkcom = jobRwkcom + .Item("_qty_complete")
                                    jobRwkRej = jobRwkRej + .Item("_qty_scrapped")
                                Else
                                    jobRwkTot = jobRwkTot + .Item("_qty_op_qty")
                                    jobRwkcom = jobRwkcom + .Item("_qty_complete")
                                    jobRwkRej = jobRwkRej + .Item("_qty_scrapped")
                                End If
                            End With
                        Next
                    End If



                    If DSItem.Tables(0).Rows.Count > 0 Then
                        Dim DRItem As DataRow() = DSItem.Tables(0).Select("_itemCode='" & Trim(StrItemCode) & "'")
                        If DRItem.Length > 0 Then
                            StrItemName = DRItem(0).Item("_itemdescription")
                        End If
                    End If
                    If DSRecGroup.Tables(0).Rows.Count > 0 Then
                        Dim DRRec() As DataRow = DSRecGroup.Tables(0).Select("_wc='" & Trim(StrWCCode) & "'")
                        If DRRec.Length > 0 Then
                            StrRecName = DRRec(0).Item("_rGroupID")
                            StrWCName = DRRec(0).Item("_description")
                        End If
                    End If

                    If StrWCName = "" Then
                        If DSWC.Tables(0).Rows.Count > 0 Then
                            Dim DRWC() As DataRow = DSWC.Tables(0).Select("_wc='" & Trim(StrWCCode) & "'")
                            If DRWC.Length > 0 Then
                                StrWCName = DRWC(0).Item("_description")
                            End If
                        End If
                    End If


                    drRp = dsrsg.Tables("tbRSCGroupReport").NewRow
                    drRp("sno") = i + 1
                    drRp("Title") = Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                    drRp("CreateDate") = Now
                    drRp("ReportDate") = Now 'Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                    drRp("SelRSCGroup") = "dasd"
                    drRp("StockCard") = ""
                    drRp("RSCGroup") = StrRecName
                    drRp("FGCode") = StrItemCode
                    drRp("FGDesc") = StrItemName
                    drRp("Job") = DSJobRpt.Tables(0).Rows(i).Item("_job")
                    drRp("Author") = ""
                    drRp("PerOPDate") = Now
                    drRp("OP") = StrOP
                    drRp("WCDesc") = StrWCName
                    drRp("Qty") = 0
                    drRp("Total") = 0
                    dsrsg.Tables("tbRSCGroupReport").Rows.Add(drRp)







                Next



            End If
            Dim cry As New cryRecGroupReport
            cry.SetDataSource(dsrsg)
            RViewer.ReportSource = cry
            Panel1.Visible = False
            RViewer.Visible = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally

        End Try







    End Sub


    'Public Property DisplayStatusBar() As Boolean
    '    Get
    '        Dim obj As Object
    '        Dim tempStatusbar As New GroupBox
    '        RViewer.ShowGroupTree()
    '        For Each obj In Me.RViewer.Controls
    '            If obj.GetType Is tempStatusbar.GetType Then
    '                Return CType(obj, GroupBox).Visible
    '                Exit For
    '            End If
    '        Next

    '        tempStatusbar.Dispose()
    '        tempStatusbar = Nothing
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        Dim obj As Object
    '        Dim tempStatusbar As New GroupTre

    '        For Each obj In Me.CrystalReportViewer1.Controls
    '            If obj.GetType Is tempStatusbar.GetType Then
    '                CType(RViewer, RViewer.ShowGroupTree).Visible = Value
    '                Exit For
    '            End If
    '        Next

    '        tempStatusbar.Dispose()
    '        tempStatusbar = Nothing
    '    End Set
    'End Property


End Class