Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class frmRptException
    Dim clsM As New clsMain
    Public DSItem As New DataSet
    Public DSWC As New DataSet
    Dim strReportDate As String
    Dim parm As SqlParameter
    Dim dsrsg As DSNewException
    Private Sub frmRptException_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dtpFrom.Value = Now
        dtpTo.Value = Now
        txtFrom.Text = Format(Now, "dd/MM/yyyy")
        txtTo.Text = Format(Now, "dd/MM/yyyy")
    End Sub
    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(txtFrom.Text) = "" Or Trim(txtTo.Text) = "" Then
            MsgBox("Please enter the date!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Try
            PB1.Value = 0
            PB1.Minimum = 0
            PB1.Maximum = 0

            Dim s() As String = Split(txtFrom.Text, "/")
            Dim SD As Date = DateSerial(s(2), s(1), s(0))
            Dim s1() As String = Split(txtTo.Text, "/")
            Dim ED As Date = DateSerial(s1(2), s1(1), s1(0))

            Dim strTitle As String = txtFrom.Text & " - " & txtTo.Text
            Dim strCreated As String = Format(Now, "dd MMM yyyy")
            Dim DSServer As New DataSet
            Dim DSJob As New DataSet
            Dim DSClient As New DataSet
            Dim DSTrans As New DataSet
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cnser
                .CommandType = CommandType.Text
                '.CommandText = "select Job from job where stat in('R','S') and job_date between @Sd and @ED"
                .CommandText = "select Job from job where  job_date between @Sd and @ED"
                Dim sqlstr As String = "select Job from job where job_date between @Sd and @ED"
                parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
                parm.Value = SD
                parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
                parm.Value = ED
                DA.SelectCommand = com
                DA.Fill(DSJob, "tbsch")
                .Parameters.Clear()
            End With
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cnser
                .CommandType = CommandType.Text
                .CommandText = "select A.Job,A.Job_Date,A.item,B.oper_num,B.Wc from Job A inner join jobroute B on A.Job=B.Job where   A.job_date between @Sd and @ED"
                parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
                parm.Value = SD
                parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
                parm.Value = ED
                DA.SelectCommand = com
                DA.Fill(DSServer, "tbsch")
                .Parameters.Clear()
            End With


            With com
                Dim DA As New SqlDataAdapter
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select A._Job,A._Jobdate,A._item,B._operationNo,B._WC from tbJob A inner join tbJobRoute B on A._job=B._Job where A._jobdate between @Sd and @ED"
                parm = .Parameters.Add("@Sd", SqlDbType.Float)
                parm.Value = SD.ToOADate
                parm = .Parameters.Add("@Ed", SqlDbType.Float)
                parm.Value = ED.ToOADate
                DA.SelectCommand = com
                DA.Fill(DSClient, "tbsch")
                .Parameters.Clear()
            End With
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cn
                .CommandType = CommandType.Text
                '.CommandText = "select * from tbJobTrans where _job in(select _job from tbJob where _jobdate between @Sd and @ED)"
                .CommandText = "select _tansnum,_job,_jobsuffix,_jobsuffixParent ,_item, _jobDate ,_oper_num ,_wc from tbJobTrans where _job in(select _job from tbJob where _jobdate between @Sd and @ED)"
                parm = .Parameters.Add("@Sd", SqlDbType.Float)
                parm.Value = SD.ToOADate
                parm = .Parameters.Add("@Ed", SqlDbType.Float)
                parm.Value = ED.ToOADate
                DA.SelectCommand = com
                DA.Fill(DSTrans, "tbsch")
                .Parameters.Clear()
            End With


            With com
                Dim DA As New SqlDataAdapter
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select * from tbItem"
                DA.SelectCommand = com
                DA.Fill(DSItem, "tbsch")
                .Parameters.Clear()
            End With
            Dim Checkst As Boolean = False

            Dim drRp As DataRow
            dsrsg = New DSNewException
            If DSJob.Tables(0).Rows.Count > 0 Then
                PB1.Maximum = DSJob.Tables(0).Rows.Count
                Dim i As Integer
                For i = 0 To DSJob.Tables(0).Rows.Count - 1
                    If DSClient.Tables(0).Rows.Count > 0 Then

                        Dim DRServer() As DataRow = DSServer.Tables(0).Select("Job='" & DSJob.Tables(0).Rows(i).Item("Job") & "'", "oper_num")
                        Dim DRclient() As DataRow = DSClient.Tables(0).Select("_Job='" & Trim(DSJob.Tables(0).Rows(i).Item("Job")) & "'", "_operationNo")
                        If IsNothing(DRclient) = False Then


                            If DRServer.Length = DRclient.Length Then
                                If DSTrans.Tables(0).Rows.Count > 0 Then
                                    If DRclient.Length > 0 Then

                                        Dim j As Integer
                                        For j = 0 To DRclient.Length - 1
                                            Dim drTran As DataRow() = DSTrans.Tables(0).Select("_Job='" & DRclient(j).Item("_Job") & "' and _Oper_num=" & DRclient(j).Item("_operationNo"), "_Oper_num")
                                            If drTran.Length > 0 Then

                                            Else
                                                If DRclient.Length > j + 1 Then
                                                    Dim drTranNew As DataRow() = DSTrans.Tables(0).Select("_Job='" & DRclient(j + 1).Item("_Job") & "' and _Oper_num=" & DRclient(j + 1).Item("_operationNo"), "_Oper_num")
                                                    If drTranNew.Length Then
                                                        drRp = dsrsg.Tables("tbTransferReport").NewRow
                                                        drRp("sn") = "0"
                                                        drRp("Title") = strTitle
                                                        drRp("CreateDate") = strCreated
                                                        drRp("FGCode") = DRclient(j).Item("_item")
                                                        Dim drItem() As DataRow = DSItem.Tables(0).Select("_itemCode='" & DRclient(j).Item("_item") & "'")
                                                        If drItem.Length > 0 Then
                                                            drRp("FGDescription") = drItem(0).Item("_itemdescription")
                                                        Else
                                                            drRp("FGDescription") = ""
                                                        End If

                                                        drRp("Job") = DRclient(j).Item("_Job")
                                                        drRp("SP") = drTranNew(0).Item("_jobsuffix")
                                                        dsrsg.Tables("tbTransferReport").Rows.Add(drRp)
                                                        Checkst = True
                                                    End If
                                                End If
                                            End If
                                        Next
                                    End If

                                End If
                                'drRp = dsrsg.Tables("tbTransferReport").NewRow
                                'dsrsg.Tables("tbTransferReport").Rows.Add(drRp)
                            ElseIf DRServer.Length > DRclient.Length Then
                                drRp = dsrsg.Tables("tbTransferReport").NewRow
                                drRp("sn") = "0"
                                drRp("Title") = strTitle
                                drRp("CreateDate") = strCreated
                                drRp("FGCode") = DRServer(0).Item("item")

                                Dim drItem() As DataRow = DSItem.Tables(0).Select("_itemCode='" & DRServer(0).Item("item") & "'")
                                If drItem.Length > 0 Then
                                    drRp("FGDescription") = drItem(0).Item("_itemdescription")
                                Else
                                    drRp("FGDescription") = ""
                                End If

                                drRp("Job") = DRServer(0).Item("Job")
                                drRp("SP") = "M"
                                dsrsg.Tables("tbTransferReport").Rows.Add(drRp)
                                Checkst = True
                            Else
                                drRp = dsrsg.Tables("tbTransferReport").NewRow
                                drRp("sn") = "0"
                                drRp("Title") = strTitle
                                drRp("CreateDate") = strCreated
                                drRp("FGCode") = DRclient(0).Item("_item")
                                Dim drItem() As DataRow = DSItem.Tables(0).Select("_itemCode='" & DRclient(0).Item("_item") & "'")
                                If drItem.Length > 0 Then
                                    drRp("FGDescription") = drItem(0).Item("_itemdescription")
                                Else
                                    drRp("FGDescription") = ""
                                End If
                                drRp("Job") = DRclient(0).Item("_Job")
                                drRp("SP") = "M"
                                dsrsg.Tables("tbTransferReport").Rows.Add(drRp)
                                Checkst = True
                            End If

                        End If
                    End If
                    Application.DoEvents()
                    PB1.Value = PB1.Value + 1
                    Application.DoEvents()
                Next
            End If
            If Checkst = False Then
                drRp = dsrsg.Tables("tbTransferReport").NewRow
                drRp("sn") = "0"
                drRp("Title") = strTitle
                drRp("CreateDate") = strCreated
                drRp("FGCode") = ""
                drRp("FGDescription") = ""
                drRp("Job") = ""
                drRp("SP") = ""
                dsrsg.Tables("tbTransferReport").Rows.Add(drRp)
                Checkst = True
            End If

            Dim cry As New cryExceptionReport

            cry.SetDataSource(dsrsg)
            RViewer.ReportSource = cry
            butClose.Visible = True
            butExport.Visible = True
            RViewer.Refresh()
            Panel1.Visible = False
            RViewer.Visible = True

        Catch ex As Exception
            com.Parameters.Clear()
            MsgBox(ex.Message)
        End Try

        TmVari = 2
    End Sub


    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        dtpFrom.Value = Now
        dtpTo.Value = Now
    End Sub
    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtTo.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        dtpTo.Checked = False
    End Sub
    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtFrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False
    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub
    'Private Sub RViewer_ClickPage(ByVal sender As Object, ByVal e As CrystalDecisions.Windows.Forms.PageMouseEventArgs) Handles RViewer.ClickPage
    '    TmVari = 2
    'End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        RViewer.DataBindings.Clear()
        GC.GetGeneration(RViewer)
        RViewer.Visible = False
        butClose.Visible = False
        butExport.Visible = False
        PB1.Value = 0
        PB1.Minimum = 0
        PB1.Maximum = 0
        Panel1.Visible = True
    End Sub

    Private Sub butExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExport.Click
        ExportDatatoExcel()
    End Sub
    Function ExportDatatoExcel() As Boolean '//Modified date 2010-10-10
        Dim txtFile As String
        txtFile = ""
        sfdSave.FileName = ""
        sfdSave.Filter = "Excel File|*.xls"
        sfdSave.Title = "Save an Excel File"
        If sfdSave.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtFile = sfdSave.FileName
        Else
            Exit Function
        End If
        Panel3.Visible = True
        PB2.Value = 0
        PB2.Minimum = 0
        PB2.Maximum = 0
        Dim myTrans As System.Data.SqlClient.SqlTransaction

        Dim delFile As Boolean = False
        '= "C:\ZandenTemp\ComReport.xls"
        Try
            If IO.File.Exists(txtFile) Then
                IO.File.Delete(txtFile)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Function
        Finally
        End Try
        ' Dim at As Integer
        If txtFile = "" Then
            MsgBox("Please enter the file name!", MsgBoxStyle.Information, "eWIP")
            Exit Function
        End If

        Dim str, filename As String
        str = ""
        filename = ""
        Dim col, row As Integer
        col = 0
        row = 0
        Dim strHisNo As String = ""
        Dim strAcrNo As String = ""
        Dim valHis As String
        Dim strHisVal As String = ""
        Dim strCurVal As String = ""
        Dim localAll As Process()
        localAll = Process.GetProcesses()
        Dim EXhash As New Hashtable
        For i As Integer = 0 To localAll.Length - 1
            If localAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(localAll(i).Id) = False Then
                    EXhash.Add(localAll(i).Id, localAll(i).Id)
                End If
            End If
        Next

        Dim ExcelA As Object = CreateObject("Excel.Application")



        P1.Visible = True
        Try
            Dim i As Integer = 1
            With ExcelA
                .SheetsInNewWorkbook = 1
                .Workbooks.Add()
                .Worksheets(1).Select()

                'Dim chartRange As Excel

                'chartRange = .Range("A1", "D1")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "EXCEPTION REPORT"
                'chartRange.HorizontalAlignment = 3
                'chartRange.VerticalAlignment = 3

                'chartRange = .Range("A2", "B2")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "Created Date :" & Format(Now, "dd MMM yyyy")
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1

                'chartRange = .Range("C2", "D2")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "Reported Date  : " & txtFrom.Text & " - " & txtTo.Text
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1



                'i = i + 2
                '.cells(i, 1).MergeCells = True
                '.cells(i, 1).EntireRow.Font.Bold = True
                '.cells(i, 1).value = "FG Code"

                '.cells(i, 2).MergeCells = True
                '.cells(i, 2).EntireRow.Font.Bold = True
                '.cells(i, 2).value = "FG Description"

                '.cells(i, 3).MergeCells = True
                '.cells(i, 3).EntireRow.Font.Bold = True
                '.cells(i, 3).value = "Job"


                '.cells(i, 4).MergeCells = True
                '.cells(i, 4).EntireRow.Font.Bold = True
                '.cells(i, 4).value = "SP"



                'i += 1
                Dim stBoll As Boolean = False

                If dsrsg.Tables("tbTransferReport").Rows.Count > 0 Then

                    PB2.Maximum = dsrsg.Tables("tbTransferReport").Rows.Count

                    Dim M1 As Integer

                    For M1 = 0 To dsrsg.Tables("tbTransferReport").Rows.Count - 1

                        i += 1

                        .cells(i, 1).value = dsrsg.Tables("tbTransferReport").Rows(M1).Item("FGCode")
                        .cells(i, 2).value = dsrsg.Tables("tbTransferReport").Rows(M1).Item("FGDescription")
                        .cells(i, 3).value = dsrsg.Tables("tbTransferReport").Rows(M1).Item("JOB")
                        .cells(i, 4).value = dsrsg.Tables("tbTransferReport").Rows(M1).Item("SP")

                        stBoll = True



                        PB2.Value = M1 + 1
                    Next

                End If

                Application.DoEvents()





                filename = txtFile
                ExcelA.ActiveCell.Worksheet.SaveAs(filename)
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelA)
                ExcelA = Nothing



                MsgBox("Data's are exported to Excel Succesfully in '" & filename & "'", MsgBoxStyle.Information, "eWIP")

                '  lblDataV.Text = "Data has been Successfully Exported"



            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "eWIP")
            delFile = True


        Finally
            Panel3.Visible = False
            cn.Close()
            com.Parameters.Clear()
        End Try

        ' The excel is created and opened for insert value. We most close this excel using this system
        ' Dim pro() As Process = System.Diagnostics.Process.GetProcessesByName("Excel")

        Dim DellocalAll As Process()

        DellocalAll = Process.GetProcesses()


        For i As Integer = 0 To DellocalAll.Length - 1
            If DellocalAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(DellocalAll(i).Id) = False Then
                    DellocalAll(i).Kill()
                End If
            End If
        Next



        'For Each i As Process In pro
        '    If localAll(i).ProcessName.ToUpper = "EXCEL" Then
        '        lstOldProcessID.Items.Add(localAll(i).Id)
        '    End If
        '    i.Kill()
        'Next
        If delFile = True Then
            Try
                If IO.File.Exists(filename) Then
                    IO.File.Delete(filename)
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End Try
        End If
        txtFile = ""


    End Function

End Class