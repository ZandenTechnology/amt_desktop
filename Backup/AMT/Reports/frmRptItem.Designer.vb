<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRptItem
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRptItem))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblTitle = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Button1 = New System.Windows.Forms.Button
        Me.lstItem = New System.Windows.Forms.ListBox
        Me.P1 = New System.Windows.Forms.Panel
        Me.PB1 = New System.Windows.Forms.ProgressBar
        Me.dtpTo = New System.Windows.Forms.DateTimePicker
        Me.dtpFrom = New System.Windows.Forms.DateTimePicker
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtTo = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtFrom = New System.Windows.Forms.TextBox
        Me.butEXIT = New System.Windows.Forms.Button
        Me.butDone = New System.Windows.Forms.Button
        Me.RViewer = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.butClose = New System.Windows.Forms.Button
        Me.butExport = New System.Windows.Forms.Button
        Me.sfdSave = New System.Windows.Forms.SaveFileDialog
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.PB2 = New System.Windows.Forms.ProgressBar
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.P1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.BackColor = System.Drawing.Color.Silver
        Me.Panel1.Controls.Add(Me.lblTitle)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(33, 32)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(606, 520)
        Me.Panel1.TabIndex = 2
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblTitle.Location = New System.Drawing.Point(9, 9)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(122, 15)
        Me.lblTitle.TabIndex = 36
        Me.lblTitle.Text = "CO BY ITEM REPORT"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Controls.Add(Me.lstItem)
        Me.Panel2.Controls.Add(Me.P1)
        Me.Panel2.Controls.Add(Me.dtpTo)
        Me.Panel2.Controls.Add(Me.dtpFrom)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.txtTo)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.txtFrom)
        Me.Panel2.Controls.Add(Me.butEXIT)
        Me.Panel2.Controls.Add(Me.butDone)
        Me.Panel2.Location = New System.Drawing.Point(12, 27)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(581, 475)
        Me.Panel2.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(526, 316)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 129
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'lstItem
        '
        Me.lstItem.FormattingEnabled = True
        Me.lstItem.HorizontalScrollbar = True
        Me.lstItem.Location = New System.Drawing.Point(76, 63)
        Me.lstItem.Name = "lstItem"
        Me.lstItem.Size = New System.Drawing.Size(416, 277)
        Me.lstItem.Sorted = True
        Me.lstItem.TabIndex = 128
        '
        'P1
        '
        Me.P1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.P1.BackColor = System.Drawing.Color.Gray
        Me.P1.Controls.Add(Me.PB1)
        Me.P1.Location = New System.Drawing.Point(141, 391)
        Me.P1.Name = "P1"
        Me.P1.Size = New System.Drawing.Size(295, 32)
        Me.P1.TabIndex = 127
        '
        'PB1
        '
        Me.PB1.Location = New System.Drawing.Point(15, 7)
        Me.PB1.Name = "PB1"
        Me.PB1.Size = New System.Drawing.Size(266, 19)
        Me.PB1.TabIndex = 0
        '
        'dtpTo
        '
        Me.dtpTo.Location = New System.Drawing.Point(409, 18)
        Me.dtpTo.Name = "dtpTo"
        Me.dtpTo.Size = New System.Drawing.Size(17, 20)
        Me.dtpTo.TabIndex = 126
        '
        'dtpFrom
        '
        Me.dtpFrom.Location = New System.Drawing.Point(242, 18)
        Me.dtpFrom.Name = "dtpFrom"
        Me.dtpFrom.Size = New System.Drawing.Size(17, 20)
        Me.dtpFrom.TabIndex = 125
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(15, 45)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 15)
        Me.Label6.TabIndex = 121
        Me.Label6.Text = "Item Code"
        '
        'txtTo
        '
        Me.txtTo.BackColor = System.Drawing.Color.White
        Me.txtTo.Location = New System.Drawing.Point(319, 18)
        Me.txtTo.Name = "txtTo"
        Me.txtTo.Size = New System.Drawing.Size(91, 20)
        Me.txtTo.TabIndex = 118
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(292, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(21, 15)
        Me.Label1.TabIndex = 117
        Me.Label1.Text = "To"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(15, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(129, 15)
        Me.Label5.TabIndex = 116
        Me.Label5.Text = "Completed Date From"
        '
        'txtFrom
        '
        Me.txtFrom.BackColor = System.Drawing.Color.White
        Me.txtFrom.Location = New System.Drawing.Point(153, 18)
        Me.txtFrom.Name = "txtFrom"
        Me.txtFrom.Size = New System.Drawing.Size(91, 20)
        Me.txtFrom.TabIndex = 115
        '
        'butEXIT
        '
        Me.butEXIT.BackColor = System.Drawing.Color.Gray
        Me.butEXIT.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butEXIT.ForeColor = System.Drawing.Color.White
        Me.butEXIT.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butEXIT.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butEXIT.Location = New System.Drawing.Point(285, 354)
        Me.butEXIT.Name = "butEXIT"
        Me.butEXIT.Size = New System.Drawing.Size(57, 24)
        Me.butEXIT.TabIndex = 114
        Me.butEXIT.Text = "Exit"
        Me.butEXIT.UseVisualStyleBackColor = False
        '
        'butDone
        '
        Me.butDone.BackColor = System.Drawing.Color.Gray
        Me.butDone.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butDone.ForeColor = System.Drawing.Color.White
        Me.butDone.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butDone.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butDone.Location = New System.Drawing.Point(212, 354)
        Me.butDone.Name = "butDone"
        Me.butDone.Size = New System.Drawing.Size(57, 24)
        Me.butDone.TabIndex = 112
        Me.butDone.Text = "Done"
        Me.butDone.UseVisualStyleBackColor = False
        '
        'RViewer
        '
        Me.RViewer.ActiveViewIndex = -1
        Me.RViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.RViewer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RViewer.Location = New System.Drawing.Point(0, 0)
        Me.RViewer.Name = "RViewer"
        ''Me.RViewer.ReuseParameterValuesOnRefresh = True
        Me.RViewer.ShowCloseButton = False
        Me.RViewer.Size = New System.Drawing.Size(673, 585)
        Me.RViewer.TabIndex = 3
        ''Me.RViewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.RViewer.Visible = False
        '
        'butClose
        '
        Me.butClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butClose.BackgroundImage = CType(resources.GetObject("butClose.BackgroundImage"), System.Drawing.Image)
        Me.butClose.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butClose.Location = New System.Drawing.Point(661, 2)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(11, 11)
        Me.butClose.TabIndex = 8
        Me.butClose.Text = "X"
        Me.butClose.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butClose.UseVisualStyleBackColor = True
        Me.butClose.Visible = False
        '
        'butExport
        '
        Me.butExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butExport.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butExport.Image = CType(resources.GetObject("butExport.Image"), System.Drawing.Image)
        Me.butExport.Location = New System.Drawing.Point(545, 3)
        Me.butExport.Name = "butExport"
        Me.butExport.Size = New System.Drawing.Size(30, 23)
        Me.butExport.TabIndex = 130
        Me.butExport.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butExport.UseVisualStyleBackColor = True
        Me.butExport.Visible = False
        '
        'Panel3
        '
        Me.Panel3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel3.BackColor = System.Drawing.Color.Gray
        Me.Panel3.Controls.Add(Me.PB2)
        Me.Panel3.Location = New System.Drawing.Point(189, 276)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(295, 32)
        Me.Panel3.TabIndex = 131
        Me.Panel3.Visible = False
        '
        'PB2
        '
        Me.PB2.Location = New System.Drawing.Point(15, 7)
        Me.PB2.Name = "PB2"
        Me.PB2.Size = New System.Drawing.Size(266, 19)
        Me.PB2.TabIndex = 0
        '
        'frmRptItem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Info
        Me.ClientSize = New System.Drawing.Size(673, 585)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.butExport)
        Me.Controls.Add(Me.butClose)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.RViewer)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmRptItem"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.P1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lstItem As System.Windows.Forms.ListBox
    Friend WithEvents P1 As System.Windows.Forms.Panel
    Friend WithEvents PB1 As System.Windows.Forms.ProgressBar
    Friend WithEvents dtpTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtTo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtFrom As System.Windows.Forms.TextBox
    Friend WithEvents butEXIT As System.Windows.Forms.Button
    Friend WithEvents butDone As System.Windows.Forms.Button
    Friend WithEvents RViewer As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents butClose As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents butExport As System.Windows.Forms.Button
    Friend WithEvents sfdSave As System.Windows.Forms.SaveFileDialog
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents PB2 As System.Windows.Forms.ProgressBar
End Class
