Imports System.Data.SqlClient
Public Class frmSkipStage
    Dim dr As SqlDataReader
    Private Sub frmSkipStage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With lstv
            .Columns.Add(New ColHeader("WC Code", 230, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Description", lstv.Width - 330, HorizontalAlignment.Left, True))
            Getwc()
            list_Displaydata()
        End With
    End Sub
    Sub Getwc()
        cmbwkid.Items.Clear()
        cmbwkid.Items.Add("-Select-")
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                'Dim DI As DataItem
                .CommandText = "select distinct _description from tbwc order by _description"
                cn.Open()
                dr = .ExecuteReader(CommandBehavior.CloseConnection)
                While dr.Read
                    cmbwkid.Items.Add(dr.Item("_description"))
                End While
                cn.Close()
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
        cmbwkid.Text = "-Select-"
    End Sub

    Sub list_Displaydata()
        lstv.Items.Clear()
        Try
            com.Connection = cn
            com.CommandType = CommandType.Text
            com.CommandText = "select * from tbSkipStage"
            cn.Open()
            dr = com.ExecuteReader(CommandBehavior.CloseConnection)
            While dr.Read
                Dim ls As New ListViewItem(Trim(dr.Item("_idno")))    ' you can also use reader.GetSqlValue(0) 
                ls.SubItems.Add(Trim(dr.Item("_userID")))
                ls.SubItems.Add(Trim(dr.Item("_name")))
                ls.SubItems.Add(Trim(dr.Item("_userName")))
                ls.SubItems.Add(Trim(dr.Item("_password")))
                ls.SubItems.Add(Trim(dr.Item("_privilege")))
                lstv.Items.Add(ls)
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
    End Sub
End Class