Imports System.Data.SqlClient
Public Class frmRejectMaster
    Dim dr As SqlDataReader
    Dim parm As SqlParameter
    Dim strFoCus As Integer
    Dim strclsH As String
    Private Sub frmRejectMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With lstv
            .Columns.Add(New ColHeader("RNo", 0, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Reject Code", 100, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Description", lstv.Width - 120, HorizontalAlignment.Left, True))
            list_Displaydata()
        End With
    End Sub

    Sub list_Displaydata()
        lstv.Items.Clear()
        Try

            com.Connection = cn
            com.CommandType = CommandType.Text
            If strclsH = "" Then
                com.CommandText = "select _RejID,_RejectedCode,_RejectedDesc from tbRejected order by _RejectedCode"
            Else
                com.CommandText = "select _RejID,_RejectedCode,_RejectedDesc from tbRejected " & strclsH
            End If

            'parm = com.Parameters.Add("@Type", SqlDbType.VarChar, 256)
            'parm.Value = refTxt(cmbType.Text)
            cn.Open()
            dr = com.ExecuteReader(CommandBehavior.CloseConnection)
            Dim i As Integer = -1
            Dim rcount As Integer

            While dr.Read
                i = i + 1
                Dim ls As New ListViewItem(Trim(dr.Item("_RejID")))    ' you can also use reader.GetSqlValue(0) 
                ls.SubItems.Add(Trim(dr.Item("_RejectedCode")))
                ls.SubItems.Add(Trim(dr.Item("_RejectedDesc")))
                lstv.Items.Add(ls)
                If strFoCus = dr.Item("_RejID") Then
                    rcount = i
                End If
            End While
            lstv.Focus()
            lstv.Items(rcount).Selected = True
            strFoCus = -1
        Catch ex As Exception

            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(txtCode.Text) = "" Then
            MsgBox("Enter the rejected code!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtDesc.Text) = "" Then
            MsgBox("Enter the rejected description!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "insert into tbRejected( _RejectedCode,_RejectedDesc)values(@RejectedCode,@RejectedDesc)"
                parm = .Parameters.Add("@RejectedCode", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtCode.Text)
                parm = .Parameters.Add("@RejectedDesc", SqlDbType.VarChar, 256)
                parm.Value = refTxt(txtDesc.Text)
                'parm = .Parameters.Add("@Type", SqlDbType.VarChar, 256)
                'parm.Value = refTxt(cmbType.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully saved!", MsgBoxStyle.Information, "eWIP")
            End With
        Catch ex As SqlException
            If ex.Number = 2627 Then
                MsgBox("This rejected code already exist", MsgBoxStyle.Critical, "eWIP")
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub
    Function refTxt(ByVal txt As String) As String
        Return Trim(Replace(txt, "'", "''"))
    End Function
    Sub textReset()
        txtRNo.Clear()
        txtCode.Clear()
        txtDesc.Clear()
        butSave.Enabled = True
        butDelete.Enabled = False
        butUpdate.Enabled = False
    End Sub
    Private Sub butUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdate.Click
        If Trim(txtCode.Text) = "" Then
            MsgBox("Enter the rejected code!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtDesc.Text) = "" Then
            MsgBox("Enter the rejected description!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If

        Try
            With com
                strFoCus = Val(txtRNo.Text)
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "Update  tbRejected set _RejectedCode=@RejectedCode,_RejectedDesc=@RejectedDesc where _RejID =@RejID"
                parm = .Parameters.Add("@RejectedCode", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtCode.Text)
                parm = .Parameters.Add("@RejectedDesc", SqlDbType.VarChar, 256)
                parm.Value = refTxt(txtDesc.Text)
                parm = .Parameters.Add("@RejID ", SqlDbType.Int)
                parm.Value = Val(txtRNo.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")
            End With
        Catch ex As SqlException
            If ex.Number = 2627 Then
                MsgBox("This rejected code already exist", MsgBoxStyle.Critical, "eWIP")
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        Dim result As New DialogResult
        result = MessageBox.Show("Do you Want to delete this Records?", "Caption", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = Windows.Forms.DialogResult.Yes Then
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "delete from   tbRejected  where _RejID =@RejID "
                    parm = .Parameters.Add("@RejID ", SqlDbType.Int)
                    parm.Value = Val(txtRNo.Text)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()
                    textReset()
                    MsgBox("Successfully deleted!", MsgBoxStyle.Information, "eWIP")
                End With
            Catch ex As SqlException
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Finally
                cn.Close()
                com.Parameters.Clear()
            End Try
            list_Displaydata()
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        textReset()
    End Sub

    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click
        txtRNo.Text = lstv.SelectedItems(0).Text
        txtCode.Text = lstv.SelectedItems(0).SubItems(1).Text
        txtDesc.Text = lstv.SelectedItems(0).SubItems(2).Text
        butSave.Enabled = False
        butUpdate.Enabled = True
        butDelete.Enabled = True
        ' txtUserCode.ReadOnly = True
        'StbUser.Text = "You Can Edit And Delete This Records"
    End Sub


    Private Sub lstv_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lstv.ColumnClick
        Dim clickedCol As ColHeader = CType(Me.lstv.Columns(e.Column), ColHeader)
       
        clickedCol.ascending = Not clickedCol.ascending

        If e.Column = 2 Then
            If clickedCol.ascending = True Then
                strclsH = " Order by _RejectedDesc "
            Else
                strclsH = " Order by _RejectedDesc DESC"
            End If

        Else
            If clickedCol.ascending = True Then
                strclsH = " Order by _RejectedCode "
            Else
                strclsH = " Order by _RejectedCode DESC"
            End If
        End If


        Dim numItems As Integer = Me.lstv.Items.Count
        Me.lstv.BeginUpdate()
        Dim SortArray As New ArrayList
        Dim i As Integer
        For i = 0 To numItems - 1
            SortArray.Add(New SortWrapper(Me.lstv.Items(i), e.Column))
        Next i
        SortArray.Sort(0, SortArray.Count, New SortWrapper.SortComparer(clickedCol.ascending))
        Me.lstv.Items.Clear()
        Dim z As Integer
        For z = 0 To numItems - 1
            Me.lstv.Items.Add(CType(SortArray(z), SortWrapper).sortItem)
        Next z
        Me.lstv.EndUpdate()
    End Sub
    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub

    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub
End Class