Imports System.Data.SqlClient
Public Class frmOperationStatus
    Dim dr As SqlDataReader
    Sub list_Displaydata()
        Try
            com.Connection = cn
            com.CommandType = CommandType.Text
            com.CommandText = "select * from tbOperationStatus"
            'parm = com.Parameters.Add("@Type", SqlDbType.VarChar, 256)
            'parm.Value = refTxt(cmbType.Text)
            cn.Open()
            dr = com.ExecuteReader(CommandBehavior.CloseConnection)
            If dr.Read Then
                If dr.Item("_transfer") = 1 Then
                    ckbTrans.Checked = True
                Else
                    ckbTrans.Checked = False
                End If
                If dr.Item("_skip") = 1 Then
                    ckbSkip.Checked = True
                Else
                    ckbSkip.Checked = False
                End If
                If dr.Item("_Split") = 1 Then
                    ckbSplit.Checked = True
                Else
                    ckbSplit.Checked = False
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub butUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdate.Click
        Try
            Dim parm As SqlParameter
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "Update  tbOperationStatus set _transfer=@transfer,_Skip=@skip,_Split=@split"
                parm = .Parameters.Add("@transfer", SqlDbType.Int)
                If ckbTrans.Checked = True Then
                    parm.Value = 1
                Else
                    parm.Value = 0
                End If

                parm = .Parameters.Add("@skip", SqlDbType.Int)
                If ckbSkip.Checked = True Then
                    parm.Value = 1
                Else
                    parm.Value = 0
                End If

                parm = .Parameters.Add("@split", SqlDbType.Int)
                If ckbSplit.Checked = True Then
                    parm.Value = 1
                Else
                    parm.Value = 0
                End If
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")
            End With
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
    End Sub

    Private Sub frmOperationStatus_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        list_Displaydata()
    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub

    
End Class