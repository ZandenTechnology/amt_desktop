Imports System.Data.SqlClient
Public Class frmOperative
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtOperatorID As System.Windows.Forms.TextBox
    Friend WithEvents txtOperatorName As System.Windows.Forms.TextBox
    Friend WithEvents txtIDNo As System.Windows.Forms.TextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ckbSkip As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ckbSplit As System.Windows.Forms.CheckBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents ckbPrint As System.Windows.Forms.CheckBox
    Friend WithEvents txtopSerchby As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents butEXIT As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butDelete As System.Windows.Forms.Button
    Friend WithEvents butUpdate As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents lstv As System.Windows.Forms.ListView
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.butEXIT = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.butCancel = New System.Windows.Forms.Button
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtopSerchby = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.ckbPrint = New System.Windows.Forms.CheckBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.ckbSplit = New System.Windows.Forms.CheckBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.ckbSkip = New System.Windows.Forms.CheckBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtIDNo = New System.Windows.Forms.TextBox
        Me.txtOperatorName = New System.Windows.Forms.TextBox
        Me.txtOperatorID = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.lstv = New System.Windows.Forms.ListView
        Me.butDelete = New System.Windows.Forms.Button
        Me.butUpdate = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel2.BackColor = System.Drawing.Color.DarkGray
        Me.Panel2.Controls.Add(Me.butEXIT)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.butCancel)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Controls.Add(Me.butDelete)
        Me.Panel2.Controls.Add(Me.butUpdate)
        Me.Panel2.Controls.Add(Me.butSave)
        Me.Panel2.Location = New System.Drawing.Point(8, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(720, 546)
        Me.Panel2.TabIndex = 45
        Me.Panel2.TabStop = True
        '
        'butEXIT
        '
        Me.butEXIT.BackColor = System.Drawing.Color.Gray
        Me.butEXIT.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butEXIT.ForeColor = System.Drawing.Color.White
        Me.butEXIT.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butEXIT.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butEXIT.Location = New System.Drawing.Point(256, 4)
        Me.butEXIT.Name = "butEXIT"
        Me.butEXIT.Size = New System.Drawing.Size(57, 24)
        Me.butEXIT.TabIndex = 116
        Me.butEXIT.Text = "Close"
        Me.butEXIT.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(634, 11)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 15)
        Me.Label3.TabIndex = 35
        Me.Label3.Text = "OPERATOR"
        '
        'butCancel
        '
        Me.butCancel.BackColor = System.Drawing.Color.Gray
        Me.butCancel.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butCancel.ForeColor = System.Drawing.Color.White
        Me.butCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(195, 4)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(57, 24)
        Me.butCancel.TabIndex = 115
        Me.butCancel.Text = "Cancel"
        Me.butCancel.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.Label9)
        Me.Panel3.Controls.Add(Me.txtopSerchby)
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.ckbPrint)
        Me.Panel3.Controls.Add(Me.Label6)
        Me.Panel3.Controls.Add(Me.ckbSplit)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.ckbSkip)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.txtIDNo)
        Me.Panel3.Controls.Add(Me.txtOperatorName)
        Me.Panel3.Controls.Add(Me.txtOperatorID)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Location = New System.Drawing.Point(8, 33)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(704, 499)
        Me.Panel3.TabIndex = 23
        Me.Panel3.TabStop = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Red
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(195, 92)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(73, 15)
        Me.Label9.TabIndex = 111
        Me.Label9.Text = "(Search By)"
        '
        'txtopSerchby
        '
        Me.txtopSerchby.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.txtopSerchby.Location = New System.Drawing.Point(84, 89)
        Me.txtopSerchby.MaxLength = 6
        Me.txtopSerchby.Name = "txtopSerchby"
        Me.txtopSerchby.Size = New System.Drawing.Size(109, 20)
        Me.txtopSerchby.TabIndex = 110
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(13, 92)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(72, 15)
        Me.Label8.TabIndex = 109
        Me.Label8.Text = "Operator ID"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(257, 64)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 15)
        Me.Label7.TabIndex = 108
        Me.Label7.Text = "PRINT"
        '
        'ckbPrint
        '
        Me.ckbPrint.AutoSize = True
        Me.ckbPrint.Location = New System.Drawing.Point(240, 65)
        Me.ckbPrint.Name = "ckbPrint"
        Me.ckbPrint.Size = New System.Drawing.Size(15, 14)
        Me.ckbPrint.TabIndex = 107
        Me.ckbPrint.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(195, 63)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 15)
        Me.Label6.TabIndex = 106
        Me.Label6.Text = "SPLIT"
        '
        'ckbSplit
        '
        Me.ckbSplit.AutoSize = True
        Me.ckbSplit.Location = New System.Drawing.Point(178, 64)
        Me.ckbSplit.Name = "ckbSplit"
        Me.ckbSplit.Size = New System.Drawing.Size(15, 14)
        Me.ckbSplit.TabIndex = 105
        Me.ckbSplit.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(13, 63)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(69, 15)
        Me.Label5.TabIndex = 104
        Me.Label5.Text = "Authorised"
        '
        'ckbSkip
        '
        Me.ckbSkip.AutoSize = True
        Me.ckbSkip.Location = New System.Drawing.Point(116, 64)
        Me.ckbSkip.Name = "ckbSkip"
        Me.ckbSkip.Size = New System.Drawing.Size(15, 14)
        Me.ckbSkip.TabIndex = 103
        Me.ckbSkip.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(134, 63)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(34, 15)
        Me.Label4.TabIndex = 102
        Me.Label4.Text = "SKIP"
        '
        'txtIDNo
        '
        Me.txtIDNo.Location = New System.Drawing.Point(328, 15)
        Me.txtIDNo.MaxLength = 30
        Me.txtIDNo.Name = "txtIDNo"
        Me.txtIDNo.Size = New System.Drawing.Size(28, 20)
        Me.txtIDNo.TabIndex = 38
        Me.txtIDNo.TabStop = False
        Me.txtIDNo.Visible = False
        '
        'txtOperatorName
        '
        Me.txtOperatorName.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.txtOperatorName.Location = New System.Drawing.Point(116, 39)
        Me.txtOperatorName.MaxLength = 256
        Me.txtOperatorName.Name = "txtOperatorName"
        Me.txtOperatorName.Size = New System.Drawing.Size(488, 20)
        Me.txtOperatorName.TabIndex = 1
        '
        'txtOperatorID
        '
        Me.txtOperatorID.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.txtOperatorID.Location = New System.Drawing.Point(116, 15)
        Me.txtOperatorID.MaxLength = 10
        Me.txtOperatorID.Name = "txtOperatorID"
        Me.txtOperatorID.Size = New System.Drawing.Size(128, 20)
        Me.txtOperatorID.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(13, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 15)
        Me.Label2.TabIndex = 35
        Me.Label2.Text = "Operator Name"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(13, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 15)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "Operator ID"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.LightGray
        Me.Panel5.Controls.Add(Me.lstv)
        Me.Panel5.Location = New System.Drawing.Point(8, 116)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(688, 370)
        Me.Panel5.TabIndex = 101
        Me.Panel5.TabStop = True
        '
        'lstv
        '
        Me.lstv.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(8, 13)
        Me.lstv.MultiSelect = False
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(672, 352)
        Me.lstv.TabIndex = 7
        Me.lstv.TabStop = False
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'butDelete
        '
        Me.butDelete.BackColor = System.Drawing.Color.Gray
        Me.butDelete.Enabled = False
        Me.butDelete.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butDelete.ForeColor = System.Drawing.Color.White
        Me.butDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butDelete.Location = New System.Drawing.Point(134, 4)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(57, 24)
        Me.butDelete.TabIndex = 114
        Me.butDelete.Text = "Delete"
        Me.butDelete.UseVisualStyleBackColor = False
        '
        'butUpdate
        '
        Me.butUpdate.BackColor = System.Drawing.Color.Gray
        Me.butUpdate.Enabled = False
        Me.butUpdate.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butUpdate.ForeColor = System.Drawing.Color.White
        Me.butUpdate.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butUpdate.Location = New System.Drawing.Point(73, 4)
        Me.butUpdate.Name = "butUpdate"
        Me.butUpdate.Size = New System.Drawing.Size(57, 24)
        Me.butUpdate.TabIndex = 113
        Me.butUpdate.Text = "Update"
        Me.butUpdate.UseVisualStyleBackColor = False
        '
        'butSave
        '
        Me.butSave.BackColor = System.Drawing.Color.Gray
        Me.butSave.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butSave.ForeColor = System.Drawing.Color.White
        Me.butSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(12, 4)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(57, 24)
        Me.butSave.TabIndex = 112
        Me.butSave.Text = "Save"
        Me.butSave.UseVisualStyleBackColor = False
        '
        'frmOperative
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.Ivory
        Me.ClientSize = New System.Drawing.Size(736, 566)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmOperative"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim parm As SqlParameter
    Dim dr As SqlDataReader
    Dim strFoCus As Integer = 0
    Dim strclsH As String
    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(txtOperatorID.Text) = "" Then
            MsgBox("Enter the operator id!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtOperatorName.Text) = "" Then
            MsgBox("Enter the operator name!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "insert into tbOperator( _operatorID,_operatorName,_skip,_split,_print)values( @operatorID,@operatorName,@skip,@split,@print)"
                parm = .Parameters.Add("@operatorID", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtOperatorID.Text)
                parm = .Parameters.Add("@operatorName", SqlDbType.VarChar, 256)
                parm.Value = refTxt(txtOperatorName.Text)
                parm = .Parameters.Add("@skip", SqlDbType.Char, 1)
                If ckbSkip.Checked = True Then
                    parm.Value = "Y"
                Else
                    parm.Value = "N"
                End If
                parm = .Parameters.Add("@split", SqlDbType.Char, 1)
                If ckbSplit.Checked = True Then
                    parm.Value = "Y"
                Else
                    parm.Value = "N"
                End If
                parm = .Parameters.Add("@Print", SqlDbType.Char, 1)
                If ckbPrint.Checked = True Then
                    parm.Value = "Y"
                Else
                    parm.Value = "N"
                End If
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully saved!", MsgBoxStyle.Information, "eWIP")
            End With
        Catch ex As SqlException
            If ex.Number = 2627 Then
                MsgBox("This operator id already exist", MsgBoxStyle.Critical, "eWIP")
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub
    Sub list_Displaydata()
        lstv.Items.Clear()
        Try
            com.Connection = cn
            com.CommandType = CommandType.Text
            If Trim(txtopSerchby.Text) = "" Then
                com.CommandText = "select * from tbOperator " & strclsH
            Else
                Dim sql As String = Replace(Trim(txtopSerchby.Text), "'", "''")
                com.CommandText = "select * from tbOperator where _operatorID like ('" & sql & "%')" & strclsH
            End If
            cn.Open()
            dr = com.ExecuteReader(CommandBehavior.CloseConnection)
            Dim ss As String = ""
            Dim i As Integer = -1
            Dim rcount As Integer
            While dr.Read
                i = i + 1
                'Dim nr As DataRow()
                'nr.Item(0) = dr.Item("_oid")
                'DataGridView1.Rows.Add(nr)
                ss = dr.Item("_operatorID")
                Dim ls As New ListViewItem(Trim(dr.Item("_oid")))    ' you can also use reader.GetSqlValue(0) 
                ls.SubItems.Add(Trim(dr.Item("_operatorID")))
                ls.SubItems.Add(Trim(dr.Item("_operatorName")))
                ls.SubItems.Add(Trim(dr.Item("_skip")))
                ls.SubItems.Add(Trim(dr.Item("_split")))
                ls.SubItems.Add(Trim(dr.Item("_print")))
                ls.Selected = True
                lstv.Items.Add(ls)
                lstv.Focus()
                If strFoCus = dr.Item("_oid") Then
                    rcount = i
                End If


            End While
            If i <> -1 Then
                lstv.Items(rcount).Selected = True
            End If
            strFoCus = 0
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try

    End Sub
    Function refTxt(ByVal txt As String) As String
        Return Trim(Replace(txt, "'", "''"))
    End Function

    Private Sub frmOperative_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtOperatorID.TabIndex = 1

        ' Panel1.Focus()
        Panel2.Focus()
        Panel3.Focus()
        txtOperatorID.Focus()
        With lstv
            .Columns.Add(New ColHeader("IDNo", 0, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Operator ID", 130, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Operator Name", lstv.Width - 285, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Skip", 50, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Split", 50, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Print", 50, HorizontalAlignment.Left, True))
            list_Displaydata()
        End With
        txtOperatorID.Text = 1
        txtOperatorID.Focus()
        txtOperatorID.Text = ""
        '  lstv.Focus()
    End Sub



    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click




        txtIDNo.Text = lstv.SelectedItems(0).Text
        txtOperatorID.Text = lstv.SelectedItems(0).SubItems(1).Text
        txtOperatorName.Text = lstv.SelectedItems(0).SubItems(2).Text
        If lstv.SelectedItems(0).SubItems(3).Text = "Y" Then
            ckbSkip.Checked = True
        Else
            ckbSkip.Checked = False
        End If
        If lstv.SelectedItems(0).SubItems(4).Text = "Y" Then
            ckbSplit.Checked = True
        Else
            ckbSplit.Checked = False
        End If
        If lstv.SelectedItems(0).SubItems(5).Text = "Y" Then
            ckbPrint.Checked = True
        Else
            ckbPrint.Checked = False
        End If

        butSave.Enabled = False
        butUpdate.Enabled = True
        butDelete.Enabled = True
        ' txtUserCode.ReadOnly = True
        'StbUser.Text = "You Can Edit And Delete This Records"
    End Sub

    Private Sub butUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdate.Click
        If Trim(txtOperatorID.Text) = "" Then
            MsgBox("Enter the operator id!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtOperatorName.Text) = "" Then
            MsgBox("Enter the operator name!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Try
            With com
                strFoCus = Val(txtIDNo.Text)
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "Update  tbOperator set _operatorID=@operatorID,_operatorName=@operatorName,_Skip=@skip,_split=@split,_print=@print where _oid=@oid"
                parm = .Parameters.Add("@operatorID", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtOperatorID.Text)
                parm = .Parameters.Add("@operatorName", SqlDbType.VarChar, 256)
                parm.Value = refTxt(txtOperatorName.Text)
                parm = .Parameters.Add("@skip", SqlDbType.Char, 1)
                If ckbSkip.Checked = True Then
                    parm.Value = "Y"
                Else
                    parm.Value = "N"
                End If
                parm = .Parameters.Add("@split", SqlDbType.Char, 1)
                If ckbSplit.Checked = True Then
                    parm.Value = "Y"
                Else
                    parm.Value = "N"
                End If
                parm = .Parameters.Add("@Print", SqlDbType.Char, 1)
                If ckbPrint.Checked = True Then
                    parm.Value = "Y"
                Else
                    parm.Value = "N"
                End If
                parm = .Parameters.Add("@oid", SqlDbType.Int)
                parm.Value = Val(txtIDNo.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")
            End With
        Catch ex As SqlException
            If ex.Number = 2627 Then
                MsgBox("This operator id already exist", MsgBoxStyle.Critical, "eWIP")
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub
    Sub textReset()
        txtIDNo.Clear()
        txtOperatorID.Clear()
        txtOperatorName.Clear()
        ckbSkip.Checked = False
        ckbSplit.Checked = False
        butSave.Enabled = True
        butDelete.Enabled = False
        butUpdate.Enabled = False
        txtOperatorID.Focus()
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        textReset()
    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        'If Trim(txtOperatorID.Text) = "" Then
        '    MsgBox("Enter the operator id!", MsgBoxStyle.Information, "eWIP")
        '    Exit Sub
        'End If
        'If Trim(txtOperatorName.Text) = "" Then
        '    MsgBox("Enter the operator name!", MsgBoxStyle.Information, "eWIP")
        '    Exit Sub
        'End If
        Dim result As New DialogResult
        result = MessageBox.Show("Do you Want to delete this Records?", "Caption", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = Windows.Forms.DialogResult.Yes Then
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "delete from  tbOperator where _oid=@oid"
                    parm = .Parameters.Add("@oid", SqlDbType.Int)
                    parm.Value = Val(txtIDNo.Text)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()
                    textReset()
                    MsgBox("Successfully deleted!", MsgBoxStyle.Information, "eWIP")
                End With
            Catch ex As SqlException
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Finally

                com.Parameters.Clear()
            End Try
            list_Displaydata()
        End If
    End Sub

    Private Sub lstv_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lstv.ColumnClick


        Dim clickedCol As ColHeader = CType(Me.lstv.Columns(e.Column), ColHeader)


        clickedCol.ascending = Not clickedCol.ascending

        Dim strby As String = ""
        If clickedCol.ascending = False Then
            strby = " DESC"
        End If


     
        Select Case e.Column
            Case 0
                strclsH = " Order by _oid " & strby
            Case 1
                strclsH = " Order by _operatorID " & strby
            Case 2
                strclsH = " Order by _operatorName " & strby
            Case 3
                strclsH = " Order by _skip " & strby
            Case 4
                strclsH = " Order by _split " & strby
            Case 5
                strclsH = " Order by _print " & strby
        End Select



        Dim numItems As Integer = Me.lstv.Items.Count


        Me.lstv.BeginUpdate()


        Dim SortArray As New ArrayList
        Dim i As Integer
        For i = 0 To numItems - 1
            SortArray.Add(New SortWrapper(Me.lstv.Items(i), e.Column))
        Next i


        SortArray.Sort(0, SortArray.Count, New SortWrapper.SortComparer(clickedCol.ascending))


        Me.lstv.Items.Clear()
        Dim z As Integer
        For z = 0 To numItems - 1
            Me.lstv.Items.Add(CType(SortArray(z), SortWrapper).sortItem)
        Next z

        Me.lstv.EndUpdate()


    End Sub

    'Private Sub txtopSerchby_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtopSerchby.KeyDown
    '    list_Displaydata()
    'End Sub

    'Private Sub txtopSerchby_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtopSerchby.KeyPress
    '    If Asc(e.KeyChar) = 13 Then
    '        list_Displaydata()
    '    End If
    'End Sub





    Private Sub txtopSerchby_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtopSerchby.TextChanged
        list_Displaydata()
    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'lstv.Focus()
        'lstv.Items(0).Selected = True
    End Sub


    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub
End Class

