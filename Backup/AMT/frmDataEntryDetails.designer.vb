<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDataEntryDetails
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.lblqtyscrapped = New System.Windows.Forms.Label
        Me.lblqtycompleted = New System.Windows.Forms.Label
        Me.lblqtyreleased = New System.Windows.Forms.Label
        Me.lbljobno = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.lstv = New System.Windows.Forms.ListView
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Ivory
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(-2, -1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(741, 446)
        Me.Panel1.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.Controls.Add(Me.lblqtyscrapped)
        Me.Panel2.Controls.Add(Me.lblqtycompleted)
        Me.Panel2.Controls.Add(Me.lblqtyreleased)
        Me.Panel2.Controls.Add(Me.lbljobno)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Location = New System.Drawing.Point(14, 27)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(713, 405)
        Me.Panel2.TabIndex = 0
        '
        'lblqtyscrapped
        '
        Me.lblqtyscrapped.AutoSize = True
        Me.lblqtyscrapped.Location = New System.Drawing.Point(215, 91)
        Me.lblqtyscrapped.Name = "lblqtyscrapped"
        Me.lblqtyscrapped.Size = New System.Drawing.Size(39, 13)
        Me.lblqtyscrapped.TabIndex = 12
        Me.lblqtyscrapped.Text = "Label2"
        '
        'lblqtycompleted
        '
        Me.lblqtycompleted.AutoSize = True
        Me.lblqtycompleted.Location = New System.Drawing.Point(215, 67)
        Me.lblqtycompleted.Name = "lblqtycompleted"
        Me.lblqtycompleted.Size = New System.Drawing.Size(39, 13)
        Me.lblqtycompleted.TabIndex = 11
        Me.lblqtycompleted.Text = "Label2"
        '
        'lblqtyreleased
        '
        Me.lblqtyreleased.AutoSize = True
        Me.lblqtyreleased.Location = New System.Drawing.Point(215, 41)
        Me.lblqtyreleased.Name = "lblqtyreleased"
        Me.lblqtyreleased.Size = New System.Drawing.Size(39, 13)
        Me.lblqtyreleased.TabIndex = 10
        Me.lblqtyreleased.Text = "Label2"
        '
        'lbljobno
        '
        Me.lbljobno.AutoSize = True
        Me.lbljobno.Location = New System.Drawing.Point(215, 18)
        Me.lbljobno.Name = "lbljobno"
        Me.lbljobno.Size = New System.Drawing.Size(39, 13)
        Me.lbljobno.TabIndex = 9
        Me.lbljobno.Text = "Label2"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(9, 90)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 14)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Qty Rejected :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(9, 67)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(95, 14)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Qty Completed :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(10, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 14)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Qty Released :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(9, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Job No :"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.LightGray
        Me.Panel3.Controls.Add(Me.lstv)
        Me.Panel3.Location = New System.Drawing.Point(12, 125)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(685, 268)
        Me.Panel3.TabIndex = 0
        '
        'lstv
        '
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(12, 16)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(662, 238)
        Me.lstv.TabIndex = 0
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'frmDataEntryDetails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(737, 443)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmDataEntryDetails"
        Me.Text = "frmDataEntryDetails"
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lstv As System.Windows.Forms.ListView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblqtyscrapped As System.Windows.Forms.Label
    Friend WithEvents lblqtycompleted As System.Windows.Forms.Label
    Friend WithEvents lblqtyreleased As System.Windows.Forms.Label
    Friend WithEvents lbljobno As System.Windows.Forms.Label
End Class
