Imports System.Data.SqlClient
Public Class frmDataEntryDetails
    Dim dr As SqlDataReader

    Private Sub frmDataEntryDetails_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With lstv
            .Columns.Add("Job No", 100, HorizontalAlignment.Left)
            .Columns.Add("Suffix", 80, HorizontalAlignment.Left)
            .Columns.Add("Operation No", 100, HorizontalAlignment.Left)
            .Columns.Add("Item No", 100, HorizontalAlignment.Left)
            .Columns.Add("Date/Time Started", 150, HorizontalAlignment.Left)
            .Columns.Add("Date/Time End", 150, HorizontalAlignment.Left)
            .Columns.Add("Rejected Code 1", 100, HorizontalAlignment.Left)
            .Columns.Add("Rejected Qty 1", 100, HorizontalAlignment.Left)
            .Columns.Add("Description 1", 100, HorizontalAlignment.Left)
            .Columns.Add("Rejected Code 2", 100, HorizontalAlignment.Left)
            .Columns.Add("Rejected Qty 2", 100, HorizontalAlignment.Left)
            .Columns.Add("Description 2", 100, HorizontalAlignment.Left)
            .Columns.Add("Rejected Code 3", 100, HorizontalAlignment.Left)
            .Columns.Add("Rejected Qty 3", 100, HorizontalAlignment.Left)
            .Columns.Add("Description 3", 100, HorizontalAlignment.Left)
        End With
    End Sub

    Sub Load_ListView()
        lstv.Items.Clear()
        Dim dt As Date
        With com
            .Connection = cn
            .CommandType = CommandType.Text
            .CommandText = "select * from tbjobtrans"

            cn.Open()
            dr = .ExecuteReader(CommandBehavior.CloseConnection)
            While dr.Read
                Dim ls As New ListViewItem(Trim(dr.Item("_job")))
                ls.SubItems.Add(Trim(dr.Item("_jobsuffix")))
                ls.SubItems.Add(Trim(dr.Item("_oper_num")))
                ls.SubItems.Add(Trim(dr.Item("_item")))
                ls.SubItems.Add(Trim(dr.Item("_jobsuffix")))
                dt = Date.FromOADate(Trim(dr.Item("")))
                ls.SubItems.Add(Format(dt, "dd/MM/yyyy"))
                dt = Date.FromOADate(Trim(dr.Item("")))
                ls.SubItems.Add(Format(dt, "dd/MM/yyyy"))
                ls.SubItems.Add(Trim(dr.Item("_rejectedcode1")))
                ls.SubItems.Add(Trim(dr.Item("_rejectedqty1")))
                ls.SubItems.Add(Trim(dr.Item("_rejecteddesc1")))
                ls.SubItems.Add(Trim(dr.Item("_rejectedcode2")))
                ls.SubItems.Add(Trim(dr.Item("_rejectedqty2")))
                ls.SubItems.Add(Trim(dr.Item("_rejecteddesc2")))
                ls.SubItems.Add(Trim(dr.Item("_rejectedcode3")))
                ls.SubItems.Add(Trim(dr.Item("_rejectedqty3")))
                ls.SubItems.Add(Trim(dr.Item("_rejecteddesc3")))

                lstv.Items.Add(ls)
            End While
        End With
    End Sub

    Private Sub Label4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label4.Click

    End Sub

    Private Sub Label6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label6.Click

    End Sub

    Private Sub Label5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label5.Click

    End Sub
End Class