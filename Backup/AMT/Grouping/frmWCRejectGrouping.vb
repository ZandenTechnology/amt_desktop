Imports System.Data.SqlClient
Public Class frmWCRejectGrouping
    Dim clsM As New clsMain
    Dim parm As SqlParameter
    Dim DSItem As New DataSet
    Dim boolst As Boolean
    Dim strFoCus As Integer
    Dim strclsH As String
    Private Sub frmWCRejectGrouping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        boolst = False
        With lstv
            .Columns.Add(New ColHeader("rgid", 0, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("WC Code", 100, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Item Code", 100, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Rejected Code", 100, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Rejected Desc", lstv.Width - 300, HorizontalAlignment.Left, True))
            GetWC()
            GetItem()
            GetReject()
            ' Getwc()
        End With
    End Sub
    Sub GetWC()
        cmbWC.Items.Clear()
        cmbWC.Items.Add(New DataItem("-Select-", "-Select-"))
        Try
            Dim ds As New DataSet
            ds = clsM.GetDataset("select * from tbWC order by _wc", "tbWC")
            If IsDBNull(ds) = False Then
                Dim i As Integer
                Dim StSpcace = "  "
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            ' cmbWC.Items.Add(New DataItem(Trim(.Item("_wc")), Trim(.Item("_wc")) & "{" & Trim(.Item("_description")) & "}"))
                            cmbWC.Items.Add(New DataItem(Trim(.Item("_wc")), Trim(.Item("_wc")) & " : " & Trim(.Item("_description")) & ""))
                        End With
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
        cmbWC.Text = "-Select-"
    End Sub
    Sub GetItem()
        cmbItem.Items.Clear()
        cmbItem.Items.Add(New DataItem("-Select-", "-Select-"))
        Try
            Dim ds As New DataSet
            ds = clsM.GetDataset("select * from tbItem order by _itemCode", "tbItem")
            DSItem = ds
            If IsDBNull(ds) = False Then
                Dim i As Integer
                Dim StSpcace As String = "  "
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            ' cmbItem.Items.Add(New DataItem(Trim(.Item("_itemCode")), Trim(.Item("_itemCode")) & "{" & Trim(.Item("_itemdescription")) & "}"))
                            cmbItem.Items.Add(New DataItem(Trim(.Item("_itemCode")), Trim(.Item("_itemCode")) & " : " & Trim(.Item("_itemdescription")) & ""))
                        End With
                    Next
                End If
            End If



        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
        cmbItem.Text = "-Select-"
    End Sub

    Sub GetReject()
        cmbReject.Items.Clear()
        cmbReject.Items.Add(New DataItem("-Select-", "-Select-"))
        Try
            Dim ds As New DataSet
            ds = clsM.GetDataset("select * from tbRejected order by _RejectedCode,_RejectedDesc", "tbRejected")
            If IsDBNull(ds) = False Then
                Dim i As Integer
                Dim StSpcace As String = "  "
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            cmbReject.Items.Add(New DataItem(Trim(.Item("_RejID")), Trim(.Item("_RejectedCode")) & " : " & Trim(.Item("_RejectedDesc")) & ""))
                        End With
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
        cmbReject.Text = "-Select-"
    End Sub

    

  

    Private Sub cmbWC_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbWC.KeyPress
        txtWCDesc.Text = ""
    End Sub

    Private Sub cmbWC_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWC.SelectedIndexChanged
        txtWCDesc.Text = ""

        If cmbWC.SelectedIndex > 0 Then
            txtWCDesc.Text = Trim(Split(cmbWC.Text, ":")(1))
        End If
        If cmbWC.SelectedIndex > 0 Then
            strclsH = ""
            list_Displaydata()
        End If
    End Sub

    Private Sub cmbItem_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbItem.KeyPress
        txtItemDesc.Text = ""
    End Sub

    Private Sub cmbItem_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbItem.SelectedIndexChanged
        txtItemDesc.Text = ""
        If cmbItem.SelectedIndex > 0 Then
            ' txtItemDesc.Text = Split(Split(cmbItem.Text, "}")(0), "{")(1)
            txtItemDesc.Text = Trim(Split(cmbItem.Text, ":")(1))
        End If
        strclsH = ""
        list_Displaydata()
    End Sub

    Private Sub cmbReject_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbReject.KeyPress
        txtRejectDesc.Text = ""
    End Sub

    Private Sub cmbReject_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbReject.SelectedIndexChanged
        txtRejectDesc.Text = ""
        If cmbReject.SelectedIndex > 0 Then
            txtRejectDesc.Text = Trim(Split(cmbReject.Text, ":")(1))
            ' txtRejectDesc.Text = Split(Split(cmbReject.Text, "}")(0), "{")(1)
        End If
    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(cmbWC.Text) = "-Select-" Or Trim(cmbWC.Text) = "" Then
            MsgBox("Select work center code!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtWCDesc.Text) = "" Then
            MsgBox("Select work center code!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
     
        If Trim(cmbReject.Text) = "-Select-" Or Trim(cmbReject.Text) = "" Then
            MsgBox("Select rejected code!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If

        If Trim(txtRejectDesc.Text) = "" Then
            MsgBox("Select rejected code!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "insert into tbRejectedGroup(_wc,_itemCode,_RejID)values(@wc,@itemCode,@RejID)"
                parm = .Parameters.Add("@wc", SqlDbType.VarChar, 50)
                Dim DI As DataItem
                DI = cmbWC.Items(cmbWC.SelectedIndex)
                parm.Value = DI.ID
                parm = .Parameters.Add("@itemCode", SqlDbType.VarChar, 50)
                If (txtItemDesc.Text) <> "" Then
                    DI = cmbItem.Items(cmbItem.SelectedIndex)
                    parm.Value = DI.ID
                Else
                    parm.Value = ""
                End If
                parm = .Parameters.Add("@RejID", SqlDbType.Int)
                DI = cmbReject.Items(cmbReject.SelectedIndex)
                parm.Value = DI.ID

                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                'textReset()
                MsgBox("Successfully saved!", MsgBoxStyle.Information, "eWIP")
                txtClean()
            End With
        Catch ex As SqlException
            If ex.Number = 2627 Then
                MsgBox("This WC Id already in group!", MsgBoxStyle.Information, "eWIP")
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub
    Sub list_Displaydata()
        Try

      
            If boolst = False Then
                Dim ds As New DataSet
                If Trim(cmbItem.Text) = "-Select-" And Trim(cmbWC.Text) <> "-Select-" Then
                    Dim DI As DataItem
                    DI = cmbWC.Items(cmbWC.SelectedIndex)
                    ds = clsM.GetDataset("select * from tbRejected inner join  tbRejectedGroup on tbRejected._RejID=tbRejectedGroup._RejID where tbRejectedGroup._wc='" & fncstr(DI.ID) & "' " & strclsH, "tbRejected")
                ElseIf Trim(cmbWC.Text) <> "-Select-" And Trim(cmbItem.Text) <> "-Select-" Then
                    Dim DI As DataItem
                    DI = cmbWC.Items(cmbWC.SelectedIndex)
                    Dim strWC As String = DI.ID
                    DI = cmbItem.Items(cmbItem.SelectedIndex)
                    Dim strItem As String = DI.ID
                    ds = clsM.GetDataset("select * from tbRejected inner join  tbRejectedGroup on tbRejected._RejID=tbRejectedGroup._RejID where tbRejectedGroup._wc='" & fncstr(strWC) & "' and tbRejectedGroup._itemCode='" & fncstr(strItem) & "' " & strclsH, "tbRejected")
                Else
                    ds = clsM.GetDataset("select *  from tbRejectedGroup where _wc='' " & strclsH, "tbRejected")
                End If
                lstv.Items.Clear()
                Dim i As Integer
                Dim rcount As Integer = 0
                Dim j As Integer = -1
                If IsDBNull(ds) = False Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        For i = 0 To ds.Tables(0).Rows.Count - 1
                            With ds.Tables(0).Rows(i)
                                j = j + 1
                                Dim ls As New ListViewItem(Trim(.Item("_rgid")))    ' you can also use reader.GetSqlValue(0) 
                                ls.SubItems.Add(Trim(.Item("_wc")))
                                ls.SubItems.Add(Trim(.Item("_itemCode")))
                                ls.SubItems.Add(Trim(.Item("_RejectedCode")))
                                ls.SubItems.Add(Trim(.Item("_RejectedDesc")))
                                lstv.Items.Add(ls)
                                If strFoCus = .Item("_rgid") Then
                                    rcount = j
                                End If
                            End With
                        Next
                        If j <> -1 Then
                            lstv.Focus()
                            lstv.Items(rcount).Selected = True
                        End If
                        strFoCus = 0


                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click
        Try
            boolst = True
            strFoCus = Val(lstv.SelectedItems(0).Text)
            txtwcRid.Text = lstv.SelectedItems(0).Text
            If lstv.SelectedItems(0).SubItems(2).Text <> "" Then
                If DSItem.Tables(0).Rows.Count > 0 Then
                    Dim DR As DataRow() = DSItem.Tables(0).Select("_itemCode='" & Trim(lstv.SelectedItems(0).SubItems(2).Text) & "'")
                    If DR.Length > 0 Then
                        Dim strst As String = Trim(DR(0).Item("_itemCode")) & " : " & Trim(DR(0).Item("_itemdescription")) & ""
                        cmbItem.SelectedIndex = cmbItem.Items.IndexOf(strst)
                    End If
                End If

            Else
                cmbItem.Text = "-Select-"
            End If
            'if Trim(lstv.SelectedItems(0).SubItems(3).Text)<>
            Dim strReject As String = Trim(lstv.SelectedItems(0).SubItems(3).Text) & " : " & lstv.SelectedItems(0).SubItems(4).Text & ""
            cmbReject.Text = strReject
            butSave.Enabled = False
            butUpdate.Enabled = True
            butDelete.Enabled = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally

        End Try
    End Sub


    Private Sub butUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdate.Click
        If Trim(txtwcRid.Text) = "" Then
            MsgBox("Select the list!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(cmbWC.Text) = "-Select-" Or Trim(cmbWC.Text) = "" Then
            MsgBox("Select work center code!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(cmbReject.Text) = "-Select-" Or Trim(cmbReject.Text) = "" Then
            MsgBox("Select rejected code!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Try
            With com
                strFoCus = Val(txtwcRid.Text)
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "update tbRejectedGroup set _wc=@wc,_itemCode=@itemCode,_RejID=@RejID where _rgid=" & Val(txtwcRid.Text)
                parm = .Parameters.Add("@wc", SqlDbType.VarChar, 50)
                Dim DI As DataItem
                DI = cmbWC.Items(cmbWC.SelectedIndex)
                parm.Value = DI.ID
                parm = .Parameters.Add("@itemCode", SqlDbType.VarChar, 50)
                If cmbItem.SelectedIndex > 0 Then
                    DI = cmbItem.Items(cmbItem.SelectedIndex)
                    parm.Value = DI.ID
                Else
                    parm.Value = ""
                End If
                parm = .Parameters.Add("@RejID", SqlDbType.Int)
                DI = cmbReject.Items(cmbReject.SelectedIndex)
                parm.Value = DI.ID

                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                'textReset()
                MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")
                txtClean()
            End With
        Catch ex As SqlException
            If ex.Number = 2627 Then
                MsgBox("This WC Id already in group!", MsgBoxStyle.Information, "eWIP")
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub
    Sub txtClean()
        cmbItem.Text = "-Select-"
        cmbReject.Text = "-Select-"
        txtwcRid.Text = ""
        butSave.Enabled = True
        butUpdate.Enabled = False
        butDelete.Enabled = True
        boolst = False
    End Sub
    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        If Trim(txtwcRid.Text) = "" Then
            MsgBox("Select the list!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If

        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "delete from  tbRejectedGroup where _rgid=" & Val(txtwcRid.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                'textReset()
                MsgBox("Successfully deleted!", MsgBoxStyle.Information, "eWIP")
                txtClean()
            End With
        Catch ex As SqlException
            If ex.Number = 2627 Then
                MsgBox("This WC Id already in group!", MsgBoxStyle.Information, "eWIP")
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub
    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        txtClean()
    End Sub
    Private Sub lstv_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lstv.ColumnClick
        Dim clickedCol As ColHeader = CType(Me.lstv.Columns(e.Column), ColHeader)
        clickedCol.ascending = Not clickedCol.ascending

        Dim strby As String = ""
        If clickedCol.ascending = False Then
            strby = " DESC"
        End If

      

        Select Case e.Column
            Case 0
                strclsH = " Order by _rgid " & strby
            Case 1
                strclsH = " Order by _wc " & strby
            Case 2
                strclsH = " Order by _itemCode " & strby
            Case 3
                strclsH = " Order by _RejectedCode " & strby
            Case 4
                strclsH = " Order by _RejectedDesc " & strby
        
        End Select





        Dim numItems As Integer = Me.lstv.Items.Count
        Me.lstv.BeginUpdate()
        Dim SortArray As New ArrayList
        Dim i As Integer
        For i = 0 To numItems - 1
            SortArray.Add(New SortWrapper(Me.lstv.Items(i), e.Column))
        Next i
        SortArray.Sort(0, SortArray.Count, New SortWrapper.SortComparer(clickedCol.ascending))
        Me.lstv.Items.Clear()
        Dim z As Integer
        For z = 0 To numItems - 1
            Me.lstv.Items.Add(CType(SortArray(z), SortWrapper).sortItem)
        Next z
        Me.lstv.EndUpdate()
    End Sub
    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub

    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub
End Class