<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMachineWRK
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label3 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.butEXIT = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.butDelete = New System.Windows.Forms.Button
        Me.butUpdate = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtDesc = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtwcID = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.butDelMulti = New System.Windows.Forms.Button
        Me.butDelSingle = New System.Windows.Forms.Button
        Me.butAssMulti = New System.Windows.Forms.Button
        Me.butAssSingle = New System.Windows.Forms.Button
        Me.lstGroup = New System.Windows.Forms.ListBox
        Me.lstMachine = New System.Windows.Forms.ListBox
        Me.lstwrk = New System.Windows.Forms.ListBox
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel2.BackColor = System.Drawing.Color.DarkGray
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Location = New System.Drawing.Point(8, 6)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(720, 451)
        Me.Panel2.TabIndex = 45
        Me.Panel2.TabStop = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(10, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(103, 15)
        Me.Label3.TabIndex = 35
        Me.Label3.Text = "MACHINE GROUP"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.butEXIT)
        Me.Panel3.Controls.Add(Me.butCancel)
        Me.Panel3.Controls.Add(Me.butDelete)
        Me.Panel3.Controls.Add(Me.butUpdate)
        Me.Panel3.Controls.Add(Me.butSave)
        Me.Panel3.Controls.Add(Me.Label6)
        Me.Panel3.Controls.Add(Me.txtDesc)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.txtwcID)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.butDelMulti)
        Me.Panel3.Controls.Add(Me.butDelSingle)
        Me.Panel3.Controls.Add(Me.butAssMulti)
        Me.Panel3.Controls.Add(Me.butAssSingle)
        Me.Panel3.Controls.Add(Me.lstGroup)
        Me.Panel3.Controls.Add(Me.lstMachine)
        Me.Panel3.Controls.Add(Me.lstwrk)
        Me.Panel3.Location = New System.Drawing.Point(8, 32)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(704, 408)
        Me.Panel3.TabIndex = 23
        Me.Panel3.TabStop = True
        '
        'butEXIT
        '
        Me.butEXIT.BackColor = System.Drawing.Color.Gray
        Me.butEXIT.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butEXIT.ForeColor = System.Drawing.Color.White
        Me.butEXIT.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butEXIT.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butEXIT.Location = New System.Drawing.Point(386, 370)
        Me.butEXIT.Name = "butEXIT"
        Me.butEXIT.Size = New System.Drawing.Size(57, 24)
        Me.butEXIT.TabIndex = 111
        Me.butEXIT.Text = "Exit"
        Me.butEXIT.UseVisualStyleBackColor = False
        '
        'butCancel
        '
        Me.butCancel.BackColor = System.Drawing.Color.Gray
        Me.butCancel.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butCancel.ForeColor = System.Drawing.Color.White
        Me.butCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(323, 370)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(57, 24)
        Me.butCancel.TabIndex = 110
        Me.butCancel.Text = "Cancel"
        Me.butCancel.UseVisualStyleBackColor = False
        '
        'butDelete
        '
        Me.butDelete.BackColor = System.Drawing.Color.Gray
        Me.butDelete.Enabled = False
        Me.butDelete.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butDelete.ForeColor = System.Drawing.Color.White
        Me.butDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butDelete.Location = New System.Drawing.Point(590, 370)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(57, 24)
        Me.butDelete.TabIndex = 109
        Me.butDelete.Text = "Delete"
        Me.butDelete.UseVisualStyleBackColor = False
        Me.butDelete.Visible = False
        '
        'butUpdate
        '
        Me.butUpdate.BackColor = System.Drawing.Color.Gray
        Me.butUpdate.Enabled = False
        Me.butUpdate.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butUpdate.ForeColor = System.Drawing.Color.White
        Me.butUpdate.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butUpdate.Location = New System.Drawing.Point(590, 400)
        Me.butUpdate.Name = "butUpdate"
        Me.butUpdate.Size = New System.Drawing.Size(57, 24)
        Me.butUpdate.TabIndex = 108
        Me.butUpdate.Text = "Update"
        Me.butUpdate.UseVisualStyleBackColor = False
        Me.butUpdate.Visible = False
        '
        'butSave
        '
        Me.butSave.BackColor = System.Drawing.Color.Gray
        Me.butSave.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butSave.ForeColor = System.Drawing.Color.White
        Me.butSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(260, 370)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(57, 24)
        Me.butSave.TabIndex = 107
        Me.butSave.Text = "Save"
        Me.butSave.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(417, 13)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(111, 15)
        Me.Label6.TabIndex = 53
        Me.Label6.Text = "Work Center Desc"
        '
        'txtDesc
        '
        Me.txtDesc.BackColor = System.Drawing.Color.White
        Me.txtDesc.Location = New System.Drawing.Point(528, 10)
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ReadOnly = True
        Me.txtDesc.Size = New System.Drawing.Size(119, 20)
        Me.txtDesc.TabIndex = 52
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(204, 13)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(93, 15)
        Me.Label5.TabIndex = 51
        Me.Label5.Text = "Work Center ID"
        '
        'txtwcID
        '
        Me.txtwcID.BackColor = System.Drawing.Color.White
        Me.txtwcID.Location = New System.Drawing.Point(303, 10)
        Me.txtwcID.Name = "txtwcID"
        Me.txtwcID.ReadOnly = True
        Me.txtwcID.Size = New System.Drawing.Size(100, 20)
        Me.txtwcID.TabIndex = 50
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(491, 43)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(125, 15)
        Me.Label4.TabIndex = 49
        Me.Label4.Text = "Allocated Machine ID"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(257, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(123, 15)
        Me.Label2.TabIndex = 48
        Me.Label2.Text = "Available Machine ID"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(22, 43)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(93, 15)
        Me.Label1.TabIndex = 47
        Me.Label1.Text = "Work Center ID"
        '
        'butDelMulti
        '
        Me.butDelMulti.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butDelMulti.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelMulti.Location = New System.Drawing.Point(451, 272)
        Me.butDelMulti.Name = "butDelMulti"
        Me.butDelMulti.Size = New System.Drawing.Size(39, 23)
        Me.butDelMulti.TabIndex = 46
        Me.butDelMulti.Text = "<<"
        Me.butDelMulti.UseVisualStyleBackColor = True
        '
        'butDelSingle
        '
        Me.butDelSingle.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butDelSingle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelSingle.Location = New System.Drawing.Point(451, 223)
        Me.butDelSingle.Name = "butDelSingle"
        Me.butDelSingle.Size = New System.Drawing.Size(39, 23)
        Me.butDelSingle.TabIndex = 45
        Me.butDelSingle.Text = "<"
        Me.butDelSingle.UseVisualStyleBackColor = True
        '
        'butAssMulti
        '
        Me.butAssMulti.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butAssMulti.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butAssMulti.Location = New System.Drawing.Point(451, 169)
        Me.butAssMulti.Name = "butAssMulti"
        Me.butAssMulti.Size = New System.Drawing.Size(39, 23)
        Me.butAssMulti.TabIndex = 44
        Me.butAssMulti.Text = ">>"
        Me.butAssMulti.UseVisualStyleBackColor = True
        '
        'butAssSingle
        '
        Me.butAssSingle.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butAssSingle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butAssSingle.Location = New System.Drawing.Point(451, 116)
        Me.butAssSingle.Name = "butAssSingle"
        Me.butAssSingle.Size = New System.Drawing.Size(39, 23)
        Me.butAssSingle.TabIndex = 43
        Me.butAssSingle.Text = ">"
        Me.butAssSingle.UseVisualStyleBackColor = True
        '
        'lstGroup
        '
        Me.lstGroup.FormattingEnabled = True
        Me.lstGroup.Location = New System.Drawing.Point(494, 61)
        Me.lstGroup.Name = "lstGroup"
        Me.lstGroup.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lstGroup.Size = New System.Drawing.Size(186, 303)
        Me.lstGroup.TabIndex = 42
        '
        'lstMachine
        '
        Me.lstMachine.FormattingEnabled = True
        Me.lstMachine.Location = New System.Drawing.Point(260, 61)
        Me.lstMachine.Name = "lstMachine"
        Me.lstMachine.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lstMachine.Size = New System.Drawing.Size(186, 303)
        Me.lstMachine.TabIndex = 41
        '
        'lstwrk
        '
        Me.lstwrk.FormattingEnabled = True
        Me.lstwrk.Location = New System.Drawing.Point(24, 61)
        Me.lstwrk.Name = "lstwrk"
        Me.lstwrk.Size = New System.Drawing.Size(186, 303)
        Me.lstwrk.TabIndex = 40
        '
        'frmMachineWRK
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Ivory
        Me.ClientSize = New System.Drawing.Size(736, 465)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmMachineWRK"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents butDelMulti As System.Windows.Forms.Button
    Friend WithEvents butDelSingle As System.Windows.Forms.Button
    Friend WithEvents butAssMulti As System.Windows.Forms.Button
    Friend WithEvents butAssSingle As System.Windows.Forms.Button
    Friend WithEvents lstGroup As System.Windows.Forms.ListBox
    Friend WithEvents lstMachine As System.Windows.Forms.ListBox
    Friend WithEvents lstwrk As System.Windows.Forms.ListBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtwcID As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents butEXIT As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butDelete As System.Windows.Forms.Button
    Friend WithEvents butUpdate As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
End Class
