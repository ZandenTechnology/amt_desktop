<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmWCRejectGrouping
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.butEXIT = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.butCancel = New System.Windows.Forms.Button
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Panel8 = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmbReject = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtRejectDesc = New System.Windows.Forms.TextBox
        Me.Panel7 = New System.Windows.Forms.Panel
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtItemDesc = New System.Windows.Forms.TextBox
        Me.cmbItem = New System.Windows.Forms.ComboBox
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.Label4 = New System.Windows.Forms.Label
        Me.cmbWC = New System.Windows.Forms.ComboBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtWCDesc = New System.Windows.Forms.TextBox
        Me.txtwcRid = New System.Windows.Forms.TextBox
        Me.txtWSIDNo = New System.Windows.Forms.TextBox
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.lstv = New System.Windows.Forms.ListView
        Me.butDelete = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.butUpdate = New System.Windows.Forms.Button
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel2.BackColor = System.Drawing.Color.DarkGray
        Me.Panel2.Controls.Add(Me.butEXIT)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.butCancel)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Controls.Add(Me.butDelete)
        Me.Panel2.Controls.Add(Me.butSave)
        Me.Panel2.Controls.Add(Me.butUpdate)
        Me.Panel2.Location = New System.Drawing.Point(48, 5)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(743, 671)
        Me.Panel2.TabIndex = 0
        '
        'butEXIT
        '
        Me.butEXIT.BackColor = System.Drawing.Color.Gray
        Me.butEXIT.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butEXIT.ForeColor = System.Drawing.Color.White
        Me.butEXIT.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butEXIT.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butEXIT.Location = New System.Drawing.Point(245, 3)
        Me.butEXIT.Name = "butEXIT"
        Me.butEXIT.Size = New System.Drawing.Size(52, 24)
        Me.butEXIT.TabIndex = 55
        Me.butEXIT.Text = "Exit"
        Me.butEXIT.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(576, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(129, 15)
        Me.Label3.TabIndex = 35
        Me.Label3.Text = "REJECTED GROUPING"
        '
        'butCancel
        '
        Me.butCancel.BackColor = System.Drawing.Color.Gray
        Me.butCancel.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butCancel.ForeColor = System.Drawing.Color.White
        Me.butCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(187, 3)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(52, 24)
        Me.butCancel.TabIndex = 54
        Me.butCancel.Text = "Cancel"
        Me.butCancel.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.Panel8)
        Me.Panel3.Controls.Add(Me.Panel7)
        Me.Panel3.Controls.Add(Me.Panel6)
        Me.Panel3.Controls.Add(Me.txtwcRid)
        Me.Panel3.Controls.Add(Me.txtWSIDNo)
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Location = New System.Drawing.Point(8, 33)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(723, 621)
        Me.Panel3.TabIndex = 0
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Controls.Add(Me.Label1)
        Me.Panel8.Controls.Add(Me.cmbReject)
        Me.Panel8.Controls.Add(Me.Label5)
        Me.Panel8.Controls.Add(Me.txtRejectDesc)
        Me.Panel8.Location = New System.Drawing.Point(30, 168)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(655, 72)
        Me.Panel8.TabIndex = 60
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(27, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 15)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "Rejected Code"
        '
        'cmbReject
        '
        Me.cmbReject.Location = New System.Drawing.Point(137, 4)
        Me.cmbReject.MaxDropDownItems = 10
        Me.cmbReject.Name = "cmbReject"
        Me.cmbReject.Size = New System.Drawing.Size(502, 21)
        Me.cmbReject.TabIndex = 46
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(27, 37)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(36, 15)
        Me.Label5.TabIndex = 50
        Me.Label5.Text = "Desc"
        '
        'txtRejectDesc
        '
        Me.txtRejectDesc.BackColor = System.Drawing.Color.LightGray
        Me.txtRejectDesc.Location = New System.Drawing.Point(137, 33)
        Me.txtRejectDesc.MaxLength = 256
        Me.txtRejectDesc.Name = "txtRejectDesc"
        Me.txtRejectDesc.ReadOnly = True
        Me.txtRejectDesc.Size = New System.Drawing.Size(502, 20)
        Me.txtRejectDesc.TabIndex = 48
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.AliceBlue
        Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel7.Controls.Add(Me.Label7)
        Me.Panel7.Controls.Add(Me.Label2)
        Me.Panel7.Controls.Add(Me.txtItemDesc)
        Me.Panel7.Controls.Add(Me.cmbItem)
        Me.Panel7.Location = New System.Drawing.Point(30, 82)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(655, 77)
        Me.Panel7.TabIndex = 59
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(27, 39)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(36, 15)
        Me.Label7.TabIndex = 57
        Me.Label7.Text = "Desc"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(27, 6)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 15)
        Me.Label2.TabIndex = 54
        Me.Label2.Text = "Item Code"
        '
        'txtItemDesc
        '
        Me.txtItemDesc.BackColor = System.Drawing.Color.LightGray
        Me.txtItemDesc.Location = New System.Drawing.Point(137, 36)
        Me.txtItemDesc.MaxLength = 256
        Me.txtItemDesc.Name = "txtItemDesc"
        Me.txtItemDesc.ReadOnly = True
        Me.txtItemDesc.Size = New System.Drawing.Size(505, 20)
        Me.txtItemDesc.TabIndex = 56
        '
        'cmbItem
        '
        Me.cmbItem.DropDownHeight = 120
        Me.cmbItem.IntegralHeight = False
        Me.cmbItem.Location = New System.Drawing.Point(137, 4)
        Me.cmbItem.MaxDropDownItems = 10
        Me.cmbItem.Name = "cmbItem"
        Me.cmbItem.Size = New System.Drawing.Size(505, 21)
        Me.cmbItem.TabIndex = 55
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Beige
        Me.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel6.Controls.Add(Me.Label4)
        Me.Panel6.Controls.Add(Me.cmbWC)
        Me.Panel6.Controls.Add(Me.Label6)
        Me.Panel6.Controls.Add(Me.txtWCDesc)
        Me.Panel6.Location = New System.Drawing.Point(30, 5)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(655, 68)
        Me.Panel6.TabIndex = 58
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(27, 6)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 15)
        Me.Label4.TabIndex = 47
        Me.Label4.Text = "WC Code"
        '
        'cmbWC
        '
        Me.cmbWC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.cmbWC.DropDownHeight = 300
        Me.cmbWC.IntegralHeight = False
        Me.cmbWC.Location = New System.Drawing.Point(140, 3)
        Me.cmbWC.MaxDropDownItems = 10
        Me.cmbWC.Name = "cmbWC"
        Me.cmbWC.Size = New System.Drawing.Size(499, 21)
        Me.cmbWC.TabIndex = 49
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(27, 37)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(36, 15)
        Me.Label6.TabIndex = 52
        Me.Label6.Text = "Desc"
        '
        'txtWCDesc
        '
        Me.txtWCDesc.BackColor = System.Drawing.Color.LightGray
        Me.txtWCDesc.Location = New System.Drawing.Point(140, 34)
        Me.txtWCDesc.MaxLength = 256
        Me.txtWCDesc.Name = "txtWCDesc"
        Me.txtWCDesc.ReadOnly = True
        Me.txtWCDesc.Size = New System.Drawing.Size(499, 20)
        Me.txtWCDesc.TabIndex = 51
        '
        'txtwcRid
        '
        Me.txtwcRid.BackColor = System.Drawing.Color.White
        Me.txtwcRid.Location = New System.Drawing.Point(615, 181)
        Me.txtwcRid.MaxLength = 256
        Me.txtwcRid.Name = "txtwcRid"
        Me.txtwcRid.ReadOnly = True
        Me.txtwcRid.Size = New System.Drawing.Size(74, 20)
        Me.txtwcRid.TabIndex = 53
        Me.txtwcRid.Visible = False
        '
        'txtWSIDNo
        '
        Me.txtWSIDNo.Location = New System.Drawing.Point(611, 165)
        Me.txtWSIDNo.MaxLength = 30
        Me.txtWSIDNo.Name = "txtWSIDNo"
        Me.txtWSIDNo.Size = New System.Drawing.Size(28, 20)
        Me.txtWSIDNo.TabIndex = 38
        Me.txtWSIDNo.Visible = False
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.LightGray
        Me.Panel5.Controls.Add(Me.lstv)
        Me.Panel5.Location = New System.Drawing.Point(17, 258)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(691, 334)
        Me.Panel5.TabIndex = 1
        '
        'lstv
        '
        Me.lstv.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(9, 6)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(672, 310)
        Me.lstv.TabIndex = 7
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'butDelete
        '
        Me.butDelete.BackColor = System.Drawing.Color.Gray
        Me.butDelete.Enabled = False
        Me.butDelete.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butDelete.ForeColor = System.Drawing.Color.White
        Me.butDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butDelete.Location = New System.Drawing.Point(129, 3)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(52, 24)
        Me.butDelete.TabIndex = 53
        Me.butDelete.Text = "Delete"
        Me.butDelete.UseVisualStyleBackColor = False
        '
        'butSave
        '
        Me.butSave.BackColor = System.Drawing.Color.Gray
        Me.butSave.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butSave.ForeColor = System.Drawing.Color.White
        Me.butSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(13, 3)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(52, 24)
        Me.butSave.TabIndex = 51
        Me.butSave.Text = "Save"
        Me.butSave.UseVisualStyleBackColor = False
        '
        'butUpdate
        '
        Me.butUpdate.BackColor = System.Drawing.Color.Gray
        Me.butUpdate.Enabled = False
        Me.butUpdate.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butUpdate.ForeColor = System.Drawing.Color.White
        Me.butUpdate.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butUpdate.Location = New System.Drawing.Point(71, 3)
        Me.butUpdate.Name = "butUpdate"
        Me.butUpdate.Size = New System.Drawing.Size(52, 24)
        Me.butUpdate.TabIndex = 52
        Me.butUpdate.Text = "Update"
        Me.butUpdate.UseVisualStyleBackColor = False
        '
        'frmWCRejectGrouping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Ivory
        Me.ClientSize = New System.Drawing.Size(815, 671)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmWCRejectGrouping"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtwcRid As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmbWC As System.Windows.Forms.ComboBox
    Friend WithEvents txtRejectDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbReject As System.Windows.Forms.ComboBox
    Friend WithEvents txtWSIDNo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents lstv As System.Windows.Forms.ListView
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtItemDesc As System.Windows.Forms.TextBox
    Friend WithEvents cmbItem As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents butEXIT As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butDelete As System.Windows.Forms.Button
    Friend WithEvents butUpdate As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents txtWCDesc As System.Windows.Forms.TextBox
End Class
