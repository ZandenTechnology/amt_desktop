Imports System.Data.SqlClient

Public Class frmHandOver
    Public IDNo As Integer
    Dim Dr As SqlDataReader
    Dim parm As SqlParameter
    Private Sub txtHandoverid_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHandoverid.GotFocus
        txtC.Focus()
    End Sub

    Private Sub txtHandoverName_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHandoverName.GotFocus

        txtC.Focus()
    End Sub

    Private Sub plHand_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles plHand.GotFocus
        txtC.Focus()
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub frmHandOver_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtC.Select()
        txtC.Focus()
    End Sub

    Private Sub txtC_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtC.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If (txtC.Text) <> "" Then
                lblMsgBox.Text = "Incorrect scan code"
                If txtC.TextLength >= 2 Then
                    Dim idv As String = UCase(Mid(txtC.Text, 1, 2))
                    Dim ACV As String = UCase(Mid(txtC.Text, 3, txtC.TextLength))
                    txtC.Text = ""
                    If idv = "ID" Then
                        If lblEMPID.Text <> ACV And lblEMPID.Text <> "" Then
                            If Check_opIDhand(ACV) = True Then
                                txtC.Focus()
                            Else
                                lblMsgBox.Text = "Please check Handover id(" & txtHandoverid.Text & ")"
                                txtHandoverid.Text = ""
                                txtHandoverName.Text = ""

                                txtC.Focus()
                                Exit Sub
                            End If

                        Else
                            lblMsgBox.Text = "Please check Handover id(" & ACV & ")"
                            txtHandoverid.Text = ""
                            txtHandoverName.Text = ""
                            txtC.Focus()
                            Exit Sub
                        End If
                    ElseIf idv = "SA" Then
                        If SaveData() = True Then
                            ChHandOverID = txtHandoverid.Text
                            ChHandOverName = txtHandoverName.Text
                            Try
                                lblMsgBox.Text = ""
                                Me.Close()

                            Catch ex As Exception
                            End Try

                        End If
                    ElseIf idv = "CL" Then
                        txtHandoverid.Text = ""
                        txtHandoverName.Text = ""
                        txtC.Focus()
                        Exit Sub
                    ElseIf idv = "EX" Then
                        Try
                            lblMsgBox.Text = ""
                            Me.Close()

                        Catch ex As Exception
                        End Try

                    End If



                End If
            End If
        End If
    End Sub

    Function Check_opIDhand(ByVal idno As String) As Boolean
        Check_opIDhand = False
        txtHandoverid.Text = ""
        txtHandoverName.Text = ""
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "SELECT     * FROM  tbOperator where _operatorID='" & fncstr(idno) & "'"
                cn.Open()
                Dr = .ExecuteReader()
                If Dr.Read Then
                    txtHandoverid.Text = Dr.Item("_operatorID")
                    txtHandoverName.Text = Dr.Item("_operatorName")
                    Check_opIDhand = True
                Else
                    lblMsgBox.Text = "Please check your operator code(" & idno & " )!"
                    txtHandoverid.Text = ""
                    txtHandoverName.Text = ""
                    Check_opIDhand = False
                End If
            End With
        Catch ex As Exception
            lblMsgBox.Text = ex.Message
            Check_opIDhand = False
            txtHandoverid.Text = ""
            txtHandoverName.Text = ""
        Finally
            cn.Close()
        End Try

        'If Check_opID = False Then
        '    lblMsgBox.Text = "Please check your operator code(" & idno & " )!"
        'End If
        Return Check_opIDhand

    End Function

    Function SaveData() As Boolean
        SaveData = False
        If IDNo = 0 Then
            lblMachine.Text = "Please Check data!"
            txtC.Focus()
            Return SaveData
            Exit Function
        End If
        If Trim(txtHandoverid.Text) = "" Then
            lblMachine.Text = "Please enter handover id!"
            txtC.Focus()
            Return SaveData
            Exit Function

        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "sp_JobHandover_Add"
                parm = .Parameters.Add("@tansnum", SqlDbType.Int)
                parm.Value = Val(IDNo)
                parm = .Parameters.Add("@Handoverid", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtHandoverid.Text)
                parm = .Parameters.Add("@HandoverName", SqlDbType.VarChar, 255)
                parm.Value = Trim(txtHandoverName.Text)
                cn.Open()
                .ExecuteNonQuery()
                SaveData = True
                cn.Close()
                com.Parameters.Clear()
                '   txtWKID.Select()
                lblMsgBox.Text = "Successfully updated!"
            End With


        Catch ex As Exception
            lblMsgBox.Text = ex.Message
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        txtC.Focus()
        Return SaveData
    End Function

    
End Class