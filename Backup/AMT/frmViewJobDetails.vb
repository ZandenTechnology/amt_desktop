Imports System.Data.SqlClient

Public Class frmViewJobDetails

    Dim dr As SqlDataReader
    Dim parm As SqlParameter

    Public strjob As String
    Public strsuffix As String
    Public strdate As String
    Public stritem As String
    Public strqtyreleased As String
    Public strqtycompleted As String
    Public strqtyscrapped As String
    Private Sub frmViewJobDetails_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        With lstv
            .Columns.Add("Job", 100, HorizontalAlignment.Left)
            .Columns.Add("Suffix", 60, HorizontalAlignment.Left)
            .Columns.Add("Operation No", 100, HorizontalAlignment.Left)
            .Columns.Add("Work Center", 100, HorizontalAlignment.Left)
            .Columns.Add("Description", 150, HorizontalAlignment.Left)
            .Columns.Add("Department", 150, HorizontalAlignment.Left)
        End With
        lbljobno.Text = strjob
        lblsuffix.Text = strsuffix
        lbldate.Text = strdate
        lblitem.Text = stritem
        lblqtyreleased.Text = strqtyreleased
        lblqtycompleted.Text = strqtycompleted
        lblqtyscrapped.Text = strqtyscrapped
        LoadListview()
    End Sub

    Sub LoadListview()
        lstv.Items.Clear()
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select jr._job as job,jr._jobsuffix as suffix,jr._operationno as opno,jr._wc as wc,twc._description as wdesc,twc._dept as dept from tbjobroute jr join tbwc twc on(jr._wc=twc._wc)" & _
                "where _job=@job order by jr._job,jr._jobsuffix"
                parm = .Parameters.Add("@job", SqlDbType.NVarChar)
                parm.Value = strjob
                cn.Open()
                dr = .ExecuteReader

                While dr.Read
                    Dim ls As New ListViewItem(Trim(dr.Item("job")))
                    ls.SubItems.Add(Trim(dr.Item("suffix")))
                    ls.SubItems.Add(Trim(dr.Item("opno")))
                    ls.SubItems.Add(Trim(dr.Item("wc")))
                    ls.SubItems.Add(Trim(dr.Item("wdesc")))
                    ls.SubItems.Add(Trim(dr.Item("dept")))
                    lstv.Items.Add(ls)
                End While

            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        
    End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        Me.Close()
    End Sub

    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub
End Class