Imports System.Data.SqlClient
Public Class frmScanJob
    Dim parm As SqlParameter
    Dim CountI As Integer
    Dim clsM As New clsMain
    Dim strstage As String
    Dim strOPer As Integer
    Dim strWC As String
    Dim i As Integer
    Dim curOPno As Integer
    Dim curWC As String
    Dim Rl_qty As Double
    Dim OP_qty As Double

    Dim NexOPno As Integer
    Dim Nexwc As String
    Dim Intgetno As Integer
    Dim txtItem As String
    Dim curNO As String
    Dim curName As String
    Dim boolTrans As Boolean
    Dim trasopid As String
    Dim trasopName As String
    Dim StrPrint As String
    Dim WCDes As String
    Dim StrOldWC As String
    Private Sub frmScanJob_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtRework.Text = ""
        txtJobno.Text = ""
        lblJ.Text = ""
        txtAction.Text = ""
        CountI = 0
        strstage = ""
        txtsuf.Text = ""
        lbl.Text = ""
        strOPer = 0
        txtOpID.Text = ""
        txtOpID.Visible = False
        lblOpID.Visible = False
        boolTrans = False
    End Sub

    Sub txtClear()
        txtid.Text = ""
        txtRework.Text = ""
        lblJ.Text = ""
        txtJobno.Text = ""
        txtAction.Text = ""
        CountI = 0
        StrPrint = ""
        strstage = ""
        txtsuf.Text = ""
        lbl.Text = ""
        StrPrint = ""
        strOPer = 0
        boolTrans = False
        pic1.Top = 215
        pic1.Left = 236
    End Sub


    Private Sub txtMain_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMain.KeyPress
        If Asc(e.KeyChar) = 13 Then

            If txtMain.Text = "" Then
                Exit Sub
            End If
            Dim strRelV As String
            If UCase(txtMain.Text) = "START" Then

                MsgBox("Invalid action!", MsgBoxStyle.Information, "eWIP")
                txtMain.Text = ""
                CountI = 1
                Exit Sub
            ElseIf UCase(txtMain.Text) = "STRT" Then
                txtMain.Text = "START"
            ElseIf UCase(txtMain.Text) = "SPLIT" Then
                MsgBox("Invalid action!", MsgBoxStyle.Information, "eWIP")
                txtMain.Text = ""
                CountI = 1
                Exit Sub
            ElseIf UCase(txtMain.Text) = "SPLT" Then
                txtMain.Text = "SPLIT"

            ElseIf UCase(txtMain.Text) = "PRINT" Then
                MsgBox("Invalid action!", MsgBoxStyle.Information, "eWIP")
                txtMain.Text = ""
                CountI = 1
                Exit Sub
            ElseIf UCase(txtMain.Text) = "PRIN" Then
                txtMain.Text = "PRINT"
            ElseIf UCase(txtMain.Text) = "COMPLETED" Then
                MsgBox("Invalid action!", MsgBoxStyle.Information, "eWIP")
                txtMain.Text = ""
                CountI = 1
                Exit Sub
            ElseIf UCase(txtMain.Text) = "COMP" Then
                txtMain.Text = "COMPLETED"

            ElseIf UCase(txtMain.Text) = "CL" Then
                MsgBox("Invalid action!", MsgBoxStyle.Information, "eWIP")
                txtMain.Text = ""
                CountI = 1
                Exit Sub

            ElseIf UCase(txtMain.Text) = "CLER" Then
                txtMain.Text = "CL"
            ElseIf UCase(txtMain.Text) = "EX" Then
                MsgBox("Invalid action!", MsgBoxStyle.Information, "eWIP")
                txtMain.Text = ""
                CountI = 1
                Exit Sub

            ElseIf UCase(txtMain.Text) = "EXIT" Or UCase(txtMain.Text) = "CNCL" Then
                txtMain.Text = "EX"

            ElseIf UCase(txtMain.Text) = "PAUSE" Then
                MsgBox("Invalid action!", MsgBoxStyle.Information, "eWIP")
                txtMain.Text = ""
                CountI = 1
                Exit Sub
            ElseIf UCase(txtMain.Text) = "PAUS" Then
                txtMain.Text = "PAUSE"

            ElseIf UCase(txtMain.Text) = "RESUME" Then
                MsgBox("Invalid action!", MsgBoxStyle.Information, "eWIP")
                txtMain.Text = ""
                CountI = 1
                Exit Sub
            ElseIf UCase(txtMain.Text) = "RESU" Then
                txtMain.Text = "RESUME"
            ElseIf UCase(txtMain.Text) = "TRANSFER" Then
                MsgBox("Invalid action!", MsgBoxStyle.Information, "eWIP")
                txtMain.Text = ""
                CountI = 1
                Exit Sub
            ElseIf UCase(txtMain.Text) = "TRNF" Then
                txtMain.Text = "TRANSFER"



            End If



            strRelV = UCase(txtMain.Text)

            txtMain.Text = ""
            If UCase(strRelV) = "EX" Then
                Me.Close()
                Exit Sub
            End If
            If UCase(strRelV) = "CL" Then
                resetAll()
                Exit Sub
            End If

            If StrPrint = "PRINT" Then
                If funOpID(strRelV) = True Then
                    resetAll()
                    Exit Sub
                Else
                    Exit Sub
                End If
            End If


            If CountI = 0 Then

                Dim s() As String
                s = Split(strRelV, "-")
                Dim strJob As String = ""
                Dim strSuf As String = ""
                Dim strRework As String = ""
                If s.Length = 2 Then
                    strJob = s(0)
                    strSuf = s(1)
                    strRework = ""
                    lblJ.Text = s(0) & "-" & s(1)
                ElseIf s.Length > 2 Then
                    strJob = s(0)
                    strSuf = s(1)
                    strRework = s(2)
                    lblJ.Text = s(0) & "-" & s(1) & "-" & s(2)
                Else
                    strJob = s(0)
                    strSuf = "M"
                    strRework = ""
                    lblJ.Text = s(0) & "-M"
                End If

                If CheckJobNo(strJob, strSuf, strRework) = True Then
                    CountI = 1
                    pic1.Left = 236
                    pic1.Top = 278
                    lblMsgbox.Text = "Scan action!"
                Else
                    lblMsgbox.Text = "Scan job order no.!"
                    lblJ.Text = ""
                    txtMain.Text = ""
                    txtMain.Focus()
                    CountI = 0
                    pic1.Left = 236
                    pic1.Top = 213
                    Exit Sub
                End If

            ElseIf CountI = 1 Then
                If strstage = "COMPLETED" Then
                    If strRelV = "PRINT" Then
                        StrPrint = "PRINT"
                        txtAction.Text = "PRINT"
                        funPrintStatus()
                        lblMsgbox.Text = "Scan your ID!"
                        Exit Sub
                    End If
                End If
                If strstage = "NEW" Then
                    If boolTrans = False Then
                        If strRelV = "START" Or strRelV = "SKIP" Or strRelV = "PRINT" Then
                            If strRelV = "PRINT" Then
                                StrPrint = "PRINT"
                                txtAction.Text = "PRINT"
                                funPrintStatus()
                                lblMsgbox.Text = "Scan your ID!"
                                Exit Sub
                            End If
                            StrPrint = ""
                            txtAction.Text = strRelV
                            ShowNextForm()
                        Else
                            lblMsgbox.Text = "Invalid action!"
                            MsgBox("Invalid action!", MsgBoxStyle.Information, "eWIP")
                            CountI = 1
                            Exit Sub
                        End If
                    Else
                        If strRelV = "TRANSFER" Or strRelV = "PRINT" Then
                            If strRelV = "PRINT" Then
                                StrPrint = "PRINT"
                                txtAction.Text = "PRINT"
                                funPrintStatus()
                                lblMsgbox.Text = "Scan your ID!"
                                Exit Sub
                            End If
                            txtAction.Text = strRelV
                            ShowNextForm()
                        Else
                            lblMsgbox.Text = "Scan action!"
                            MsgBox("Invalid action!", MsgBoxStyle.Information, "eWIP")
                            CountI = 1
                            Exit Sub
                        End If
                    End If
                ElseIf strstage = "START" Then
                    If Trim(txtRework.Text) = "R" And strRelV = "SPLIT" Then
                        lblMsgbox.Text = "Invalid action!"
                        CountI = 1
                        Exit Sub
                    End If
                    If strRelV = "COMPLETED" Or strRelV = "PAUSE" Or strRelV = "SPLIT" Or strRelV = "PRINT" Then
                        If strRelV = "PRINT" Then
                            StrPrint = "PRINT"
                            txtAction.Text = "PRINT"
                            funPrintStatus()
                            lblMsgbox.Text = "Scan your ID!"
                            Exit Sub
                        End If
                        txtAction.Text = strRelV
                        ShowNextForm()
                    Else

                        lblMsgbox.Text = "Scan action!"
                        MsgBox("Invalid action!", MsgBoxStyle.Information, "eWIP")
                        CountI = 1
                        Exit Sub
                    End If
                ElseIf strstage = "PAUSE" Then
                    If strRelV = "RESUME" Or strRelV = "PRINT" Then
                        If strRelV = "PRINT" Then
                            StrPrint = "PRINT"
                            txtAction.Text = "PRINT"
                            funPrintStatus()
                            lblMsgbox.Text = "Scan your ID!"
                            Exit Sub
                        End If
                        txtAction.Text = strRelV
                        ShowNextForm()
                    Else
                        lblMsgbox.Text = "Scan action!"
                        MsgBox("Invalid action!", MsgBoxStyle.Information, "eWIP")
                        CountI = 1
                        Exit Sub
                    End If
                End If
            End If

        End If
    End Sub
    Sub resetAll()
        lblJ.Text = ""
        txtid.Text = ""
        txtRework.Text = ""
        txtJobno.Text = ""
        txtJobno.ForeColor = Color.Black
        lblJobno.ForeColor = Color.Black
        txtJobno.Text = ""
        lbl.Text = ""
        txtsuf.Text = ""
        txtAction.Text = ""
        StrPrint = ""
        ploptnew.Visible = False
        ploptStart.Visible = False
        ploptTrans.Visible = False
        ploptResume.Visible = False
        PPrint.Visible = False
        ploptStartrework.Visible = False
        pic1.Left = 236
        pic1.Top = 213
        trasopid = ""
        trasopName = ""
        lblOpID.Visible = False
        txtOpID.Text = ""
        txtOpID.Visible = False
        txtWC.Text = ""
        CountI = 0
        strstage = ""
        lblMsgbox.Text = "Scan job order no.!"
    End Sub
    Function CheckJobNo(ByVal strjobAC As String, ByVal strjobpre As String, ByVal strrw As String) As Boolean
        Try

            If strjobpre = "" Then
                strjobpre = "M"
            End If
            CheckJobNo = False
            Dim DS As New DataSet
            Dim DSroute As New DataSet
            Dim DSMAX As New DataSet
            Dim DSActual As New DataSet
            Dim DSActualCheck As New DataSet
            Dim strJob, strSuf As String
            strJob = strjobAC
            strSuf = strjobpre
            DS = clsM.GetDataset("select * from tbjob where _job='" & strJob & "'", "tbJob")
            curNO = ""
            curName = ""
            Dim MaxOPno As Integer = 0
            Dim minOPno As Integer = 0
            If DS.Tables(0).Rows.Count > 0 Then
                CheckJobNo = True
                Dim strst As Boolean = False
                MaxOPno = 0
                DSActual = clsM.GetDataset("select * from tbjobTrans where _job='" & strJob & "' and (_jobsuffix='" & strjobpre & "') order by _oper_num desc", "tbJob")
                DSActualCheck = clsM.GetDataset("select * from tbjobTrans where _job='" & strJob & "' and (_jobsuffixParent='" & strjobpre & "') order by _oper_num", "tbJob")
                DSroute = clsM.GetDataset("select * from tbjobroute where _job='" & strJob & "' order by _operationNo", "tbJob")
                DSMAX = clsM.GetDataset("select max(_operationno) as _operationno from tbjobroute where _job='" & strJob & "'", "tbJob")



                If DSActualCheck.Tables(0).Rows.Count > 0 Then
                    strst = True
                End If
                Dim IntMax As Integer = 0
                If DSMAX.Tables(0).Rows.Count > 0 Then
                    IntMax = DSMAX.Tables(0).Rows(0).Item("_operationno")
                End If
                If DSActual.Tables(0).Rows.Count > 0 Then
                    If strrw <> DSActual.Tables(0).Rows(0).Item("_Reworkst") Then
                        MsgBox("Job Order reworked.Please use new Job Order sheet.", MsgBoxStyle.Information, "eWIP")
                        txtMain.Text = ""
                        txtMain.Focus()
                        txtClear()
                        CheckJobNo = False
                        Exit Function
                    End If
                    '-------------delete---------------
                    '-------------delete---------------
                    '-------------delete---------------
                    If CheckPlitJob(DSActual, IntMax) = False Then
                        MsgBox("Job Order splitted.Please use new Job Order sheet.", MsgBoxStyle.Information, "eWIP")
                        txtMain.Text = ""
                        txtMain.Focus()
                        txtClear()
                        CheckJobNo = False
                        Exit Function
                    End If
                    '-------------done---------------
                    '-------------done---------------
                    '-------------done---------------
                    For i = 0 To DSroute.Tables(0).Rows.Count - 1
                        Dim DR() As DataRow = DSActual.Tables(0).Select("_oper_num=" & Val(DSroute.Tables(0).Rows(i).Item("_operationNo")), "_oper_num asc")
                        Dim j As Integer = 0
                        Dim Rejqty As Double = 0
                        Dim RelQty As Double = 0
                        Dim ComQty As Double = 0
                        If DR.Length > 0 Then
                            strOPer = 0
                            strWC = ""
                        End If
                        Dim rotMax As Integer = 0
                        For j = 0 To DR.Length - 1
                            With DR(j)
                                rotMax = .Item("_oper_num")



                                If .Item("_status") = "START" Or .Item("_status") = "PAUSE" Then
                                    txtJobno.Text = .Item("_job")
                                    txtRework.Text = .Item("_Reworkst")
                                    'If .Item("_jobsuffix") <> "-" Then
                                    lbl.Text = "-"
                                    txtsuf.Text = .Item("_jobsuffix")
                                    txtParent.Text = .Item("_jobsuffixParent")
                                    'End If
                                    curOPno = .Item("_tansnum")
                                    strstage = .Item("_status")
                                    strOPer = .Item("_oper_num")
                                    txtWC.Text = .Item("_wc") & " (" & .Item("_oper_num") & ")"
                                    strWC = .Item("_wc")
                                    curNO = .Item("_emp_num")
                                    curName = .Item("_emp_name")
                                    txtid.Text = .Item("_tansnum")
                                    If .Item("_status") = "START" And .Item("_Reworkst") = "" Then
                                        ploptStart.Visible = True
                                    ElseIf .Item("_status") = "START" And .Item("_Reworkst") = "R" Then
                                        ploptStartrework.Visible = True
                                    Else
                                        ploptResume.Visible = True
                                    End If

                                    Exit Function
                                End If
                                If UCase(.Item("_status")) = "NEW" Then
                                    txtJobno.Text = .Item("_job")
                                    lbl.Text = "-"
                                    txtsuf.Text = .Item("_jobsuffix")
                                    txtRework.Text = .Item("_Reworkst")
                                    txtItem = .Item("_item")
                                    txtParent.Text = .Item("_jobsuffixParent")
                                    Rl_qty = .Item("_qty_Rele_qty")
                                    OP_qty = .Item("_qty_op_qty")
                                    curOPno = .Item("_tansnum")
                                    strstage = "NEW"
                                    txtWC.Text = .Item("_wc") & " (" & .Item("_oper_num") & ")"
                                    rotMax = .Item("_oper_num")
                                    strOPer = .Item("_oper_num")
                                    strWC = .Item("_wc")
                                    txtid.Text = .Item("_tansnum")
                                    curNO = .Item("_emp_num")
                                    curName = .Item("_emp_name")
                                    ploptStart.Visible = False
                                    If Transfer_Status("") = 1 Then
                                        '  GetTransVal(.Item("_Wc"), .Item("_oper_num"))
                                        'If UCase(.Item("_trans")) = "YES" Then
                                        If GetTransVal(.Item("_Wc"), .Item("_job"), .Item("_oper_num"), StrOldWC) = True Then
                                            If Trim(.Item("_Transempid_to")) = "" Then
                                                boolTrans = True
                                                ploptnew.Visible = False
                                                ploptTrans.Visible = True
                                                trasopid = .Item("_Transempid")
                                                trasopName = .Item("_TransempName")
                                            Else
                                                ploptTrans.Visible = False
                                                boolTrans = False
                                                ploptnew.Visible = True
                                            End If
                                        Else
                                            ploptTrans.Visible = False
                                            boolTrans = False
                                            ploptnew.Visible = True
                                        End If
                                        Exit Function
                                    Else
                                        ploptTrans.Visible = False
                                        boolTrans = False
                                        ploptnew.Visible = True
                                    End If

                                End If



                                If .Item("_status") = "PAUSE" Then
                                    rotMax = .Item("_oper_num")
                                    curOPno = .Item("_tansnum")
                                    strstage = "PAUSE"
                                End If





                                'If .Item("_status") = "COMPLETED" Then
                                '    strstage = "COMPLETED"
                                '    txtid.Text = .Item("_tansnum")
                                '    Exit Function
                                '    'RelQty = .Item("_qty_Rele_qty")
                                '    'ComQty = ComQty + .Item("_qty_complete")
                                '    'Rejqty = Rejqty + .Item("_qty_scrapped")
                                '    'strOPer = .Item("_oper_num")
                                '    'strWC = .Item("_wc")
                                'End If
                                StrOldWC = .Item("_wc")
                            End With
                        Next

                        'If strst = True Then
                        '    MsgBox("This Job have Completed!", MsgBoxStyle.Information)
                        '    txtMain.Text = ""
                        '    txtMain.Focus()
                        '    CheckJobNo = False
                        '    Exit Function
                        'End If
                        If strstage = "" Then
                            If IntMax = rotMax Then
                                MsgBox("This job already completed!", MsgBoxStyle.Information, "eWIP")
                                strstage = "COMPLETED"
                                PPrint.Visible = True
                            End If
                        End If
                        If strjobpre = "-" Then
                            txtJobno.Text = strjobAC
                        Else
                            txtJobno.Text = strjobAC
                            lbl.Text = "-"
                            txtsuf.Text = strjobpre
                        End If
                    Next
                Else
                    If strst = True Then
                        MsgBox("Job Order splitted.Please use new Job Order sheet.", MsgBoxStyle.Information, "eWIP")
                        txtMain.Text = ""
                        txtMain.Focus()
                        txtClear()
                        CheckJobNo = False
                        Exit Function
                    End If
                    If strjobpre <> "M" Then
                        MsgBox("Please check your job no!", MsgBoxStyle.Information, "eWIP")
                        txtMain.Text = ""
                        txtMain.Focus()
                        txtClear()
                        CheckJobNo = False
                        Exit Function
                    End If
                    With DS.Tables(0).Rows(0)
                        txtJobno.Text = .Item("_job")
                        lbl.Text = "-"
                        txtsuf.Text = "M"
                        txtParent.Text = "M"
                        txtJobno.ForeColor = Color.Green
                        lblJobno.ForeColor = Color.Green
                        txtItem = .Item("_item")
                        Rl_qty = .Item("_qtyReleased")
                        OP_qty = .Item("_qtyReleased")
                        ploptnew.Visible = True
                        strstage = "NEW"
                        'txtChild.Text = "-"
                        txtParent.Text = "M"
                        If DSroute.Tables(0).Rows.Count > 0 Then
                            If UPDATE_INJECTION_SKIP(.Item("_job"), DSroute) = False Then
                                strOPer = DSroute.Tables(0).Rows(0).Item("_operationNo")
                                strWC = DSroute.Tables(0).Rows(0).Item("_wc")
                                txtWC.Text = DSroute.Tables(0).Rows(0).Item("_wc") & " (" & DSroute.Tables(0).Rows(0).Item("_operationNo") & ")"
                            End If
                        End If
                    End With

                End If
            Else
                MsgBox("Invalid job order no!", MsgBoxStyle.Information, "eWIP")
                lblMsgbox.Text = "Scan your job no.!"
                txtMain.Text = ""
                txtMain.Focus()
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Function

    Private Sub txtMain_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMain.TextChanged

    End Sub

    Private Sub TimerMachine_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerMachine.Tick
        If lblMsgbox.Visible = False Then
            lblMsgbox.Visible = True
        Else
            lblMsgbox.Visible = False
        End If
    End Sub
    Sub ShowNextForm()

        If UCase(txtAction.Text) = "TRANSFER" Then
            TimerMachine.Stop()
            ' pic2.Visible = False
            lblMsgbox.Visible = False
            lblMsgbox.Text = ""

            Dim objtran As New frmTransfer
            objtran.intidno = Val(txtid.Text)
            objtran.trasopid = trasopid
            objtran.trasopName = trasopName
            objtran.txtJobno.Text = Trim(txtJobno.Text) & "-" & Trim(txtsuf.Text)
            objtran.ShowDialog()
            ' pic2.Visible = True
            lblMsgbox.Text = ""
            boolTrans = False
            txtid.Text = 0
            TimerMachine.Start()
            lblMsgbox.Visible = True
            resetAll()
            Exit Sub
        End If

        Dim obj As New frmDataEntery
        obj.lblJAC.Text = lblJ.Text
        obj.txtJobno.Text = txtJobno.Text
        obj.lbl.Text = lbl.Text
        obj.txtsuf.Text = txtsuf.Text
        obj.txtParent.Text = txtParent.Text
        obj.lblJobno.ForeColor = Color.Green
        obj.txtJobno.ForeColor = Color.Green
        obj.lbl.ForeColor = Color.Green
        obj.txtsuf.ForeColor = Color.Green
        obj.txtItem.Text = txtItem
        obj.txtItem.ForeColor = Color.Green
        obj.lblItem.ForeColor = Color.Green
        obj.txtAction.Text = txtAction.Text
        obj.txtAction.ForeColor = Color.Green
        obj.lblAction.ForeColor = Color.Green
        If UCase(txtAction.Text) = "START" Or UCase(txtAction.Text) = "SKIP" Then
            obj.txtOperationno.ForeColor = Color.Green
            obj.txtWorkcenter.ForeColor = Color.Green
            obj.lblOperationno.ForeColor = Color.Green
            obj.lblWorkcenter.ForeColor = Color.Green
            obj.txtOperationno.Text = strOPer
            obj.txtWorkcenter.Text = strWC
            obj.curNO = curNO
            obj.curName = curName
            If Trim(txtParent.Text) = "" Then
                obj.txtParent.Text = "-"
            Else
                obj.txtParent.Text = txtParent.Text
            End If

            ' obj.txtChild.Text = txtChild.Text
            obj.txtRlQ.Text = Rl_qty
            obj.txtReQ.Text = OP_qty


            Dim strMachineid = clsM.CheckMachineID(strWC)
            obj.lblRlQ.ForeColor = Color.Green
            obj.txtRlQ.ForeColor = Color.Green
            obj.txtReQ.ForeColor = Color.Green
            obj.lblReQ.ForeColor = Color.Green
            If UCase(txtAction.Text) <> "SKIP" Then
                If strMachineid = "" Then
                    obj.pic1.Left = 278
                    obj.pic1.Top = 91
                    obj.txtMachineId.Text = strMachineid
                    obj.lblMsgbox.Text = "Scan machine id!"
                    obj.CountI = 0
                ElseIf strMachineid = "-" Then
                    obj.pic1.Left = 278
                    obj.pic1.Top = 119
                    obj.txtMachineId.Text = strMachineid
                    obj.txtMachineId.ForeColor = Color.Green
                    obj.lblMachineId.ForeColor = Color.Green
                    obj.lblMsgbox.Text = "Scan your id!"
                    obj.CountI = 1
                Else
                    obj.pic1.Left = 278
                    obj.pic1.Top = 119
                    obj.txtMachineId.ForeColor = Color.Green
                    obj.lblMachineId.ForeColor = Color.Green
                    obj.txtMachineId.Text = strMachineid
                    obj.lblMsgbox.Text = "Scan your id!"
                    obj.CountI = 1
                End If
            Else
                obj.pic1.Left = 278
                obj.pic1.Top = 119
                obj.txtMachineId.ForeColor = Color.Green
                obj.lblMachineId.ForeColor = Color.Green
                obj.txtMachineId.Text = "-"
                obj.lblMsgbox.Text = "Scan your id!"
                obj.CountI = 1
            End If
            obj.txtID.Text = txtid.Text


            obj.lblComqty.Visible = False
            obj.lblRejQ.Visible = False
            obj.lblEdate.Visible = False

        ElseIf UCase(txtAction.Text) = "COMPLETED" Or UCase(txtAction.Text) = "PAUSE" Or UCase(txtAction.Text) = "RESUME" Or UCase(txtAction.Text) = "SPLIT" Then
            Dim ds As DataSet = getProcessJob(txtid.Text)
            If ds.Tables(0).Rows.Count > 0 Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    With ds.Tables(0).Rows(i)
                        obj.lblMsgbox.Text = "Scan your ID!"
                        obj.txtItem.Text = .Item("_item")
                        obj.txtItem.ForeColor = Color.Green
                        obj.lblItem.ForeColor = Color.Green
                        obj.curNO = curNO
                        obj.curName = curName

                        obj.txtOperationno.Text = .Item("_oper_num")
                        obj.txtOperationno.ForeColor = Color.Green
                        obj.lblOperationno.ForeColor = Color.Green


                        obj.txtWorkcenter.Text = .Item("_wc")
                        obj.txtWorkcenter.ForeColor = Color.Green
                        obj.lblWorkcenter.ForeColor = Color.Green


                        obj.txtMachineId.Text = .Item("_machineid")
                        obj.txtMachineId.ForeColor = Color.Green
                        obj.lblMachineId.ForeColor = Color.Green

                        obj.txtOPid.Text = .Item("_emp_num")

                        obj.txtRlQ.Text = .Item("_qty_Rele_qty")
                        obj.txtRlQ.ForeColor = Color.Green
                        obj.lblRlQ.ForeColor = Color.Green

                        obj.txtReQ.Text = .Item("_qty_op_qty")
                        obj.txtReQ.ForeColor = Color.Green
                        obj.lblReQ.ForeColor = Color.Green

                        obj.txtStartDate.Text = Format(DateTime.FromOADate(.Item("_start_Date")), "dd/MM/yyyy HH:mm")
                        obj.txtRemarks.Text = .Item("_Remarks")
                        obj.txtOrgRemarks.Text = .Item("_Remarks")
                        obj.txtStartDate.ForeColor = Color.Green
                        obj.lblStartDate.ForeColor = Color.Green
                        obj.pic1.Left = 278
                        obj.pic1.Top = 119
                        obj.txtID.Text = txtid.Text
                        If UCase(txtAction.Text) <> "PAUSE" Then
                            obj.lblComqty.Visible = True
                            obj.lblRejQ.Visible = True
                            obj.lblEdate.Visible = True
                        End If



                    End With
                Next
            End If









        ElseIf UCase(txtAction.Text) = "SPLIT" Then
        ElseIf UCase(txtAction.Text) = "PAUSE" Then
        ElseIf UCase(txtAction.Text) = "VOID" Then
        ElseIf UCase(txtAction.Text) = "TRANSFER" Then
        ElseIf UCase(txtAction.Text) = "RESUME" Then
        End If


        TimerMachine.Stop()
        obj.ShowDialog()
        WCDes = ""
        txtid.Text = 0
        TimerMachine.Start()
        resetAll()
    End Sub
    Function getProcessJob(ByVal idno As Integer) As DataSet
        Dim DSP As New DataSet
        DSP = clsM.GetDataset("select * from tbJobTrans where _tansnum=" & idno, "tbJobTrans")
        Return DSP
    End Function


    Function GetMAXno(ByVal jn As String) As Integer
        'GetMAXno = 0
        'Dim DSP As New DataSet
        'DSP = clsM.GetDataset("select Max( from tbJobTrans where _tansnum=" & idno, "tbJobTrans")
        'Return DSP
    End Function

    Function Transfer_Status(ByVal jn As String) As Integer
        Transfer_Status = 0
        Dim DSP As New DataSet
        DSP = clsM.GetDataset("select * from tbOperationStatus", "tbOperationStatus")
        If DSP.Tables(0).Rows.Count > 0 Then
            Transfer_Status = DSP.Tables(0).Rows(0).Item("_transfer")
        End If
        Return Transfer_Status
    End Function

    Sub PrintReport(ByVal strjob As String, ByVal strsuf As String)
        Dim prn As New ClsAMTprint.clsmain
        Dim strRe As String = ""

        If txtRework.Text = "R" Then
            prn.printAMT(Trim(strjob) & "-" & Trim(strsuf) & "-R", SqlString, PrinterName, False)
        Else
            prn.printAMT(Trim(strjob) & "-" & Trim(strsuf), SqlString, PrinterName, False)
        End If

    End Sub
    Function funPrintStatus() As Boolean
        funPrintStatus = False
        Try
            txtOpID.Visible = True
            lblOpID.Visible = True
            txtOpID.Text = ""
            funPrintStatus = True
            pic1.Left = 236
            pic1.Top = 312
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
        End Try
        Return funPrintStatus
    End Function
    Function funOpID(ByVal opid As String) As Boolean
        Dim DSOP As New DataSet
        funOpID = False
        Dim strRe As String = ""
        Try
            DSOP = clsM.GetDataset("select * from tbOperator where _operatorID='" & Replace(opid, "'", "''") & "'", "tbJob")
            If DSOP.Tables(0).Rows.Count > 0 Then
                If DSOP.Tables(0).Rows(0).Item("_print") = "Y" Then
                    Dim prn As New ClsAMTprint.clsmain
                    If txtRework.Text = "R" Then
                        prn.printAMT(Trim(txtJobno.Text) & "-" & Trim(txtsuf.Text) & "-R", SqlString, PrinterName, False)
                    Else
                        prn.printAMT(Trim(txtJobno.Text) & "-" & Trim(txtsuf.Text), SqlString, PrinterName, False)
                    End If
                    funOpID = True
                    MsgBox(PrintMSG, MsgBoxStyle.Information, "eWIP")
                Else
                    MsgBox("Authorization failed !Please contact your supervisor.", MsgBoxStyle.Information, "eWIP")
                End If
            Else
                MsgBox("Invalid operator id! (" & opid & ").", MsgBoxStyle.Information, "eWIP")
            End If
        Catch ex As Exception
        End Try
    End Function

    Function CheckPlitJob(ByVal Ds As DataSet, ByVal Maxn As Integer) As String

        CheckPlitJob = True
        Dim drH As DataRow() = Ds.Tables(0).Select("_status<>'COMPLETED'", "_oper_num Asc")
        If drH.Length > 0 Then
            CheckPlitJob = True
        Else
            Dim drH1 As DataRow() = Ds.Tables(0).Select("_oper_num=" & Maxn, "_oper_num Asc")
            If drH1.Length > 0 Then
                CheckPlitJob = True
            Else
                CheckPlitJob = False
            End If
        End If
        Return CheckPlitJob
    End Function
    Function UPDATE_INJECTION_SKIP(ByVal InJob As String, ByVal DS_route As DataSet) As Boolean
        Try
            UPDATE_INJECTION_SKIP = False
            Dim DS_IN_Jobroute As New DataSet
            DS_IN_Jobroute = clsM.GetDataset("select max(_operationNo) as _operationNo from  tbJobroute where _Job='" & InJob & "' and _wc in(select _wc from tbWC where (_description='INJECTION' or _description='OVERMOLD' or _description ='INJECTION-CIM' ))", "tbJob")
            If DS_IN_Jobroute.Tables(0).Rows.Count > 0 Then
                If IsDBNull(DS_IN_Jobroute.Tables(0).Rows(0).Item("_operationNo")) = False Then
                    Dim DRInj As DataRow() = DS_route.Tables(0).Select("_operationNo<=" & DS_IN_Jobroute.Tables(0).Rows(0).Item("_operationNo"), "_operationNo")
                    If DRInj.Length > 0 Then
                        Dim i As Integer
                        For i = 0 To DRInj.Length - 1

                            With com
                                .Connection = cn
                                .CommandType = CommandType.StoredProcedure
                                .CommandText = "Sp_JOBSKIP_INJECTION_ADD"
                                parm = .Parameters.Add("@Job", SqlDbType.VarChar, 50)
                                parm.Value = Trim(InJob)
                                parm = .Parameters.Add("@WC", SqlDbType.VarChar, 50)
                                parm.Value = Trim(DRInj(i).Item("_WC"))
                                parm = .Parameters.Add("@oper_num", SqlDbType.Int)
                                parm.Value = Val(DRInj(i).Item("_operationNo"))
                                parm = .Parameters.Add("@start_Date", SqlDbType.Float)
                                parm.Value = Now.ToOADate
                                parm = .Parameters.Add("@start_time", SqlDbType.Int)
                                parm.Value = CInt(Format(Now, "HHMM"))
                                parm = .Parameters.Add("@Action", SqlDbType.Int)
                                If i = DRInj.Length - 1 Then
                                    parm.Value = 1
                                    strOPer = Val(DRInj(i).Item("_operationNo"))
                                    strWC = Trim(DRInj(i).Item("_WC"))
                                    txtWC.Text = Trim(DRInj(i).Item("_WC")) & " (" & DRInj(i).Item("_operationNo") & ")"
                                    UPDATE_INJECTION_SKIP = True
                                Else
                                    parm.Value = 0
                                End If
                                cn.Open()
                                Dim dr As SqlDataReader
                                dr = .ExecuteReader
                                If i = DRInj.Length - 1 Then
                                    If dr.Read Then
                                        txtid.Text = dr(0)
                                    End If
                                End If

                                cn.Close()
                                .Parameters.Clear()
                            End With
                        Next
                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        Return UPDATE_INJECTION_SKIP
    End Function
    Function GetTransVal(ByVal WCC As String, ByVal fnJobno As String, ByVal fnOpno As String, ByVal StrOldWC As String) As Boolean
        GetTransVal = False
        Dim strB As Boolean = False
        Dim DSTJob As New DataSet
        DSTJob = clsM.GetDataset("select * from tbJobRoute where _Job='" & fnJobno & "'  and _operationNo in(select min(_operationNo)from tbJobRoute where _Job='" & fnJobno & "' and _operationNo>" & Val(fnOpno) & ")", "tbCheck")
        If DSTJob.Tables(0).Rows.Count > 0 Then
            If DSTJob.Tables(0).Rows(0).Item("_WC") = WCC Then
                strB = True
            End If
        End If

        Dim strRCcode As String = ""
        Dim dsoldrc As DataSet
        dsoldrc = clsM.GetDataset("Select  _rGroupID from tbResourceGroup Where _rid in(select distinct _rid from tbWorkStation  where _wc='" & StrOldWC & "')", "tbJob")
        If dsoldrc.Tables(0).Rows.Count > 0 Then
            strRCcode = dsoldrc.Tables(0).Rows(0).Item("_rGroupID")
        End If

        If strB = False Then
            Try
                Dim DST As New DataSet
                DST = clsM.GetDataset("Select  _rGroupID,_Transfer from tbResourceGroup Where _rid in(select distinct _rid from tbWorkStation  where _wc='" & WCC & "')", "tbJob")
                'Select  _Transfer from tbResourceGroup Where _rid in(select distinct _rid from tbWorkStattion  where _wc='" & WCC & "'"
                If DST.Tables(0).Rows.Count > 0 Then
                    If strRCcode <> DST.Tables(0).Rows(0).Item("_rGroupID") Then
                        If UCase(DST.Tables(0).Rows(0).Item("_Transfer")) = "Y" Or UCase(DST.Tables(0).Rows(0).Item("_Transfer")) = "YES" Then
                            GetTransVal = True
                        End If
                    End If
                End If
            Catch ex As Exception
            End Try
        End If
        Return GetTransVal
    End Function



End Class