Imports System.Data.SqlClient
Public Class frmStockEntry
    Dim CountI As Integer
    Dim clsM As New clsMain
    Dim parm As SqlParameter
    Dim DSItem As New DataSet
    Sub clearText()
        CountI = 0
        pic1.Visible = True
        lblMsgbox.Text = "Please scan job no.!"
        txtJobno.Text = ""
        txtsuf.Text = ""
        lblJ.Text = ""
        lblJobDate.Text = ""
        lblItemCode.Text = ""
        lblDesc.Text = ""
        lblJobDate.Text = ""
        lblDate.Text = ""
        lblCom.Text = ""
        lblOpName.Text = ""
        lblOpID.Text = ""
        lblSn.Text = ""
        PicSave.Visible = False
        lblSave.Visible = False
        pMain.Visible = False
        txtMain.Focus()
    End Sub

    Function GetOperator(ByVal opid As String) As Boolean
        GetOperator = False
        Dim DSOP As New DataSet
        Dim sql As String = "select * from tbOperator where _operatorID='" & Replace(opid, "'", "''") & "'"
        DSOP = clsM.GetDataset(sql, "tbMachine")
        If DSOP.Tables(0).Rows.Count > 0 Then
            lblOpID.Text = DSOP.Tables(0).Rows(0).Item("_operatorID")
            lblOpName.Text = DSOP.Tables(0).Rows(0).Item("_operatorName")
            GetOperator = True
        End If


        Return GetOperator
    End Function

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub frmStockEntry_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DSItem = clsM.GetDataset("select * from tbItem", "tbItem")
        clearText()
    End Sub

    Private Sub txtMain_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMain.KeyPress
        Dim strRelV As String

        If Asc(e.KeyChar) = 13 Then
            If txtMain.Text = "" Then
                Exit Sub
            End If



            strRelV = UCase(txtMain.Text)
            txtMain.Text = ""
            If UCase(strRelV) = "CLER" Or UCase(strRelV) = "CNCL" Then
                clearText()
                CountI = 0
                Exit Sub
            End If

            If UCase(strRelV) = "EXIT" Then
                Me.Close()
                Exit Sub
            End If
            If CountI = 2 Then
                If strRelV = "SAVE" Then
                    If StockSave() = True Then
                        MsgBox("Action updated!", MsgBoxStyle.Information, "eWIP")
                        CountI = 0
                        clearText()
                        Exit Sub
                    End If
                Else
                    MsgBox("Invalid input!", MsgBoxStyle.Information, "eWIP")
                    ' mas("Invalid input!")
                    txtMain.Focus()
                End If


            End If

            If CountI = 1 Then
                If GetOperator(strRelV) = True Then

                    picJob.Visible = False
                    lblSave.Visible = True
                    PicSave.Visible = True
                    lblMsgbox.Text = "Scan 'SAVE' or 'CANCEL'!"
                    txtMain.Focus()
                    CountI = 2
                Else
                    CountI = 1
                    MsgBox("Invalid operator id!", MsgBoxStyle.Information, "eWIP")
                    txtMain.Text = ""
                    txtMain.Focus()

                End If
            End If

            If CountI = 0 Then

                Dim s() As String
                s = Split(strRelV, "-")
                Dim strJob As String = ""
                Dim strSuf As String = ""
                If s.Length = 2 Then
                    strJob = s(0)
                    strSuf = s(1)
                    lblJ.Text = s(0) & "-" & s(1)
                Else
                    strJob = s(0)
                    strSuf = "M"
                    lblJ.Text = s(0) & "-M"
                End If

                If CheckJobNo(strJob, strSuf) = True Then
                    CountI = 1
                    pic1.Visible = False
                    pMain.Visible = True
                    picJob.Visible = True
                    lblMsgbox.Text = "Scan your ID!"
                Else
                    lblMsgbox.Text = "Scan job order no.!"
                    lblJ.Text = ""
                    txtMain.Text = ""
                    pic1.Visible = True
                    txtMain.Focus()
                    CountI = 0
                    clearText()
                    Exit Sub
                End If


            End If


        End If

    End Sub



    Function CheckJobNo(ByVal strjobAC As String, ByVal strjobpre As String) As Boolean
        Try
            If strjobpre = "" Then
                strjobpre = "M"
            End If
            CheckJobNo = False
            Dim DSStock As New DataSet
            DSStock = clsM.GetDataset("select * from tb_st_Inventory where _job='" & strjobAC & "' and _jobsuffix='" & strjobpre & "'", "tbJob")
            If DSStock.Tables(0).Rows.Count > 0 Then
                MsgBox("This Job no. already in inventory list!", MsgBoxStyle.Information, "eWIP")
                Exit Function
            End If


            Dim DS As New DataSet
            DS = clsM.GetDataset("select * from tbvwJobTrans where _job='" & strjobAC & "' and _jobsuffix='" & strjobpre & "'", "tbJob")
            If DS.Tables(0).Rows.Count > 0 Then
                With DS.Tables(0).Rows(0)
                    lblJobDate.Text = Format(DateTime.FromOADate(.Item("_jobDate")), "dd/MM/yyyy")
                    lblItemCode.Text = .Item("_item")

                    If DSItem.Tables(0).Rows.Count > 0 Then
                        Dim drIt As DataRow() = DSItem.Tables(0).Select("_itemCode ='" & .Item("_item") & "'")
                        If drIt.Length > 0 Then
                            lblDesc.Text = drIt(0).Item("_itemdescription")
                        End If

                    End If
                    lblCom.Text = .Item("_qty_complete")
                    lblSn.Text = .Item("_tansnum")
                    lblDate.Text = Format(.Item("_toDate"), "dd/MM/yyyy")
                    CheckJobNo = True
                    Exit Function
                End With




            Else
                MsgBox("Invalid job order no!", MsgBoxStyle.Information, "eWIP")
                lblMsgbox.Text = "Scan your job no.!"
                clearText()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Function

    Function StockSave() As Boolean
        StockSave = False
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "sp_Stock_Add"
                parm = .Parameters.Add("@tansnum", SqlDbType.Int)
                parm.Value = Val(lblSn.Text)
                parm = .Parameters.Add("@emp_num", SqlDbType.VarChar, 50)
                parm.Value = Trim(lblOpID.Text)
                parm = .Parameters.Add("@emp_name", SqlDbType.VarChar, 256)
                parm.Value = Trim(lblOpName.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                StockSave = True
                lblMsgbox.Text = "Action updated!"
                'lblMsgbox.Text = "Successfully updated!"
            End With












        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            com.Parameters.Clear()
            cn.Close()
        End Try


    End Function
End Class