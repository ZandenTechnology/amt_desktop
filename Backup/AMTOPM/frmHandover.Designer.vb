<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHandover
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.plHand = New System.Windows.Forms.Panel
        Me.Label35 = New System.Windows.Forms.Label
        Me.txtHandoverName = New System.Windows.Forms.TextBox
        Me.Label33 = New System.Windows.Forms.Label
        Me.Label32 = New System.Windows.Forms.Label
        Me.txtHandoverid = New System.Windows.Forms.TextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblName = New System.Windows.Forms.Label
        Me.lblEMP = New System.Windows.Forms.Label
        Me.lblMSG = New System.Windows.Forms.Label
        Me.txtMain = New System.Windows.Forms.TextBox
        Me.txtid = New System.Windows.Forms.Label
        Me.txtsuf = New System.Windows.Forms.Label
        Me.lbl = New System.Windows.Forms.Label
        Me.txtJobno = New System.Windows.Forms.Label
        Me.lblJobno = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.plHand.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'plHand
        '
        Me.plHand.BackColor = System.Drawing.SystemColors.Desktop
        Me.plHand.Controls.Add(Me.Label35)
        Me.plHand.Controls.Add(Me.txtHandoverName)
        Me.plHand.Controls.Add(Me.Label33)
        Me.plHand.Controls.Add(Me.Label32)
        Me.plHand.Controls.Add(Me.txtHandoverid)
        Me.plHand.Location = New System.Drawing.Point(-9, 74)
        Me.plHand.Name = "plHand"
        Me.plHand.Size = New System.Drawing.Size(410, 72)
        Me.plHand.TabIndex = 145
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.Color.White
        Me.Label35.Location = New System.Drawing.Point(11, 38)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(92, 13)
        Me.Label35.TabIndex = 73
        Me.Label35.Text = "Operator Name"
        '
        'txtHandoverName
        '
        Me.txtHandoverName.BackColor = System.Drawing.Color.White
        Me.txtHandoverName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHandoverName.Location = New System.Drawing.Point(111, 37)
        Me.txtHandoverName.MaxLength = 50
        Me.txtHandoverName.Name = "txtHandoverName"
        Me.txtHandoverName.ReadOnly = True
        Me.txtHandoverName.Size = New System.Drawing.Size(264, 26)
        Me.txtHandoverName.TabIndex = 75
        Me.txtHandoverName.TabStop = False
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label33.Location = New System.Drawing.Point(216, 12)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(76, 13)
        Me.Label33.TabIndex = 74
        Me.Label33.Text = "(Hand Over)"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label32.Location = New System.Drawing.Point(11, 13)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(73, 13)
        Me.Label32.TabIndex = 72
        Me.Label32.Text = "Operator ID"
        '
        'txtHandoverid
        '
        Me.txtHandoverid.BackColor = System.Drawing.Color.White
        Me.txtHandoverid.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHandoverid.Location = New System.Drawing.Point(111, 5)
        Me.txtHandoverid.MaxLength = 50
        Me.txtHandoverid.Name = "txtHandoverid"
        Me.txtHandoverid.ReadOnly = True
        Me.txtHandoverid.Size = New System.Drawing.Size(99, 26)
        Me.txtHandoverid.TabIndex = 73
        Me.txtHandoverid.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.PaleGoldenrod
        Me.Panel1.Controls.Add(Me.lblName)
        Me.Panel1.Controls.Add(Me.lblEMP)
        Me.Panel1.Controls.Add(Me.lblMSG)
        Me.Panel1.Controls.Add(Me.txtMain)
        Me.Panel1.Controls.Add(Me.txtid)
        Me.Panel1.Controls.Add(Me.txtsuf)
        Me.Panel1.Controls.Add(Me.lbl)
        Me.Panel1.Controls.Add(Me.txtJobno)
        Me.Panel1.Controls.Add(Me.lblJobno)
        Me.Panel1.Location = New System.Drawing.Point(-9, 2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(410, 77)
        Me.Panel1.TabIndex = 146
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.ForeColor = System.Drawing.Color.Black
        Me.lblName.Location = New System.Drawing.Point(218, 39)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(39, 20)
        Me.lblName.TabIndex = 93
        Me.lblName.Text = "234"
        Me.lblName.Visible = False
        '
        'lblEMP
        '
        Me.lblEMP.AutoSize = True
        Me.lblEMP.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMP.ForeColor = System.Drawing.Color.Black
        Me.lblEMP.Location = New System.Drawing.Point(316, 49)
        Me.lblEMP.Name = "lblEMP"
        Me.lblEMP.Size = New System.Drawing.Size(39, 20)
        Me.lblEMP.TabIndex = 92
        Me.lblEMP.Text = "234"
        Me.lblEMP.Visible = False
        '
        'lblMSG
        '
        Me.lblMSG.AutoSize = True
        Me.lblMSG.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMSG.Location = New System.Drawing.Point(13, 56)
        Me.lblMSG.Name = "lblMSG"
        Me.lblMSG.Size = New System.Drawing.Size(0, 15)
        Me.lblMSG.TabIndex = 91
        '
        'txtMain
        '
        Me.txtMain.Location = New System.Drawing.Point(307, 9)
        Me.txtMain.Name = "txtMain"
        Me.txtMain.Size = New System.Drawing.Size(0, 20)
        Me.txtMain.TabIndex = 90
        '
        'txtid
        '
        Me.txtid.AutoSize = True
        Me.txtid.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtid.ForeColor = System.Drawing.Color.Black
        Me.txtid.Location = New System.Drawing.Point(378, 7)
        Me.txtid.Name = "txtid"
        Me.txtid.Size = New System.Drawing.Size(0, 20)
        Me.txtid.TabIndex = 89
        '
        'txtsuf
        '
        Me.txtsuf.AutoSize = True
        Me.txtsuf.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsuf.ForeColor = System.Drawing.Color.Black
        Me.txtsuf.Location = New System.Drawing.Point(218, 4)
        Me.txtsuf.Name = "txtsuf"
        Me.txtsuf.Size = New System.Drawing.Size(39, 20)
        Me.txtsuf.TabIndex = 88
        Me.txtsuf.Text = "234"
        '
        'lbl
        '
        Me.lbl.AutoSize = True
        Me.lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl.ForeColor = System.Drawing.Color.Black
        Me.lbl.Location = New System.Drawing.Point(203, 4)
        Me.lbl.Name = "lbl"
        Me.lbl.Size = New System.Drawing.Size(15, 20)
        Me.lbl.TabIndex = 87
        Me.lbl.Text = "-"
        '
        'txtJobno
        '
        Me.txtJobno.AutoSize = True
        Me.txtJobno.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobno.ForeColor = System.Drawing.Color.Black
        Me.txtJobno.Location = New System.Drawing.Point(94, 4)
        Me.txtJobno.Name = "txtJobno"
        Me.txtJobno.Size = New System.Drawing.Size(109, 20)
        Me.txtJobno.TabIndex = 86
        Me.txtJobno.Text = "9999999999"
        '
        'lblJobno
        '
        Me.lblJobno.AutoSize = True
        Me.lblJobno.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobno.ForeColor = System.Drawing.Color.Black
        Me.lblJobno.Location = New System.Drawing.Point(11, 4)
        Me.lblJobno.Name = "lblJobno"
        Me.lblJobno.Size = New System.Drawing.Size(83, 20)
        Me.lblJobno.TabIndex = 85
        Me.lblJobno.Text = "JOB NO :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkRed
        Me.Label3.Location = New System.Drawing.Point(4, 191)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(149, 13)
        Me.Label3.TabIndex = 149
        Me.Label3.Text = "EX -    Exit Without Save"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkRed
        Me.Label2.Location = New System.Drawing.Point(4, 171)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(143, 13)
        Me.Label2.TabIndex = 148
        Me.Label2.Text = "SAVE -   Exit With Save"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkRed
        Me.Label1.Location = New System.Drawing.Point(4, 149)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 13)
        Me.Label1.TabIndex = 147
        Me.Label1.Text = "CL  -  Clear"
        '
        'frmHandover
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(400, 224)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.plHand)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmHandover"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.plHand.ResumeLayout(False)
        Me.plHand.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents plHand As System.Windows.Forms.Panel
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents txtHandoverName As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtHandoverid As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtsuf As System.Windows.Forms.Label
    Friend WithEvents lbl As System.Windows.Forms.Label
    Friend WithEvents txtJobno As System.Windows.Forms.Label
    Friend WithEvents lblJobno As System.Windows.Forms.Label
    Friend WithEvents txtid As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtMain As System.Windows.Forms.TextBox
    Friend WithEvents lblMSG As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblEMP As System.Windows.Forms.Label
End Class
