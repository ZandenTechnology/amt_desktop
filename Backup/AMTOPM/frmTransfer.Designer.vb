<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransfer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTransfer))
        Me.plHand = New System.Windows.Forms.Panel
        Me.pic1 = New System.Windows.Forms.PictureBox
        Me.txtOPName = New System.Windows.Forms.Label
        Me.txtOPid = New System.Windows.Forms.Label
        Me.lblOPName = New System.Windows.Forms.Label
        Me.Label33 = New System.Windows.Forms.Label
        Me.lblOPid = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.pic2 = New System.Windows.Forms.PictureBox
        Me.txtTRName = New System.Windows.Forms.Label
        Me.txtTRID = New System.Windows.Forms.Label
        Me.lblTRName = New System.Windows.Forms.Label
        Me.lblTRID = New System.Windows.Forms.Label
        Me.lblAction = New System.Windows.Forms.Label
        Me.txtMain = New System.Windows.Forms.TextBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.PictureBox5 = New System.Windows.Forms.PictureBox
        Me.lblMsgbox = New System.Windows.Forms.Label
        Me.txtJobno = New System.Windows.Forms.Label
        Me.lblJobno = New System.Windows.Forms.Label
        Me.pic3 = New System.Windows.Forms.PictureBox
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Label2 = New System.Windows.Forms.Label
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.lblStatus = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.TimerMachine = New System.Windows.Forms.Timer(Me.components)
        Me.plHand.SuspendLayout()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.pic2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'plHand
        '
        Me.plHand.BackColor = System.Drawing.Color.PaleGoldenrod
        Me.plHand.Controls.Add(Me.pic1)
        Me.plHand.Controls.Add(Me.txtOPName)
        Me.plHand.Controls.Add(Me.txtOPid)
        Me.plHand.Controls.Add(Me.lblOPName)
        Me.plHand.Controls.Add(Me.Label33)
        Me.plHand.Controls.Add(Me.lblOPid)
        Me.plHand.Location = New System.Drawing.Point(212, 133)
        Me.plHand.Name = "plHand"
        Me.plHand.Size = New System.Drawing.Size(467, 99)
        Me.plHand.TabIndex = 115
        '
        'pic1
        '
        Me.pic1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.pic1.Image = Global.AMTOPM.My.Resources.Resources.Arrow1
        Me.pic1.Location = New System.Drawing.Point(6, 34)
        Me.pic1.Name = "pic1"
        Me.pic1.Size = New System.Drawing.Size(22, 22)
        Me.pic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic1.TabIndex = 119
        Me.pic1.TabStop = False
        '
        'txtOPName
        '
        Me.txtOPName.AutoSize = True
        Me.txtOPName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOPName.ForeColor = System.Drawing.Color.Black
        Me.txtOPName.Location = New System.Drawing.Point(169, 37)
        Me.txtOPName.Name = "txtOPName"
        Me.txtOPName.Size = New System.Drawing.Size(122, 16)
        Me.txtOPName.TabIndex = 76
        Me.txtOPName.Text = "Operator Name :"
        '
        'txtOPid
        '
        Me.txtOPid.AutoSize = True
        Me.txtOPid.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOPid.ForeColor = System.Drawing.Color.Black
        Me.txtOPid.Location = New System.Drawing.Point(328, 40)
        Me.txtOPid.Name = "txtOPid"
        Me.txtOPid.Size = New System.Drawing.Size(96, 16)
        Me.txtOPid.TabIndex = 75
        Me.txtOPid.Text = "Operator ID :"
        Me.txtOPid.Visible = False
        '
        'lblOPName
        '
        Me.lblOPName.AutoSize = True
        Me.lblOPName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOPName.ForeColor = System.Drawing.Color.Black
        Me.lblOPName.Location = New System.Drawing.Point(45, 37)
        Me.lblOPName.Name = "lblOPName"
        Me.lblOPName.Size = New System.Drawing.Size(122, 16)
        Me.lblOPName.TabIndex = 73
        Me.lblOPName.Text = "Operator Name :"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label33.Location = New System.Drawing.Point(361, 17)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(76, 13)
        Me.Label33.TabIndex = 74
        Me.Label33.Text = "(Hand Over)"
        Me.Label33.Visible = False
        '
        'lblOPid
        '
        Me.lblOPid.AutoSize = True
        Me.lblOPid.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOPid.ForeColor = System.Drawing.Color.Black
        Me.lblOPid.Location = New System.Drawing.Point(328, 72)
        Me.lblOPid.Name = "lblOPid"
        Me.lblOPid.Size = New System.Drawing.Size(96, 16)
        Me.lblOPid.TabIndex = 72
        Me.lblOPid.Text = "Operator ID :"
        Me.lblOPid.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.PaleGoldenrod
        Me.Panel1.Controls.Add(Me.pic2)
        Me.Panel1.Controls.Add(Me.txtTRName)
        Me.Panel1.Controls.Add(Me.txtTRID)
        Me.Panel1.Controls.Add(Me.lblTRName)
        Me.Panel1.Controls.Add(Me.lblTRID)
        Me.Panel1.Location = New System.Drawing.Point(212, 295)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(467, 108)
        Me.Panel1.TabIndex = 116
        '
        'pic2
        '
        Me.pic2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.pic2.Image = Global.AMTOPM.My.Resources.Resources.Arrow1
        Me.pic2.Location = New System.Drawing.Point(6, 36)
        Me.pic2.Name = "pic2"
        Me.pic2.Size = New System.Drawing.Size(22, 22)
        Me.pic2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic2.TabIndex = 120
        Me.pic2.TabStop = False
        Me.pic2.Visible = False
        '
        'txtTRName
        '
        Me.txtTRName.AutoSize = True
        Me.txtTRName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTRName.ForeColor = System.Drawing.Color.Black
        Me.txtTRName.Location = New System.Drawing.Point(169, 39)
        Me.txtTRName.Name = "txtTRName"
        Me.txtTRName.Size = New System.Drawing.Size(122, 16)
        Me.txtTRName.TabIndex = 78
        Me.txtTRName.Text = "Operator Name :"
        '
        'txtTRID
        '
        Me.txtTRID.AutoSize = True
        Me.txtTRID.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTRID.ForeColor = System.Drawing.Color.Black
        Me.txtTRID.Location = New System.Drawing.Point(289, 9)
        Me.txtTRID.Name = "txtTRID"
        Me.txtTRID.Size = New System.Drawing.Size(122, 16)
        Me.txtTRID.TabIndex = 77
        Me.txtTRID.Text = "Operator Name :"
        Me.txtTRID.Visible = False
        '
        'lblTRName
        '
        Me.lblTRName.AutoSize = True
        Me.lblTRName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTRName.ForeColor = System.Drawing.Color.Black
        Me.lblTRName.Location = New System.Drawing.Point(45, 39)
        Me.lblTRName.Name = "lblTRName"
        Me.lblTRName.Size = New System.Drawing.Size(119, 16)
        Me.lblTRName.TabIndex = 73
        Me.lblTRName.Text = "Transfer Name :"
        '
        'lblTRID
        '
        Me.lblTRID.AutoSize = True
        Me.lblTRID.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTRID.ForeColor = System.Drawing.Color.Black
        Me.lblTRID.Location = New System.Drawing.Point(163, 10)
        Me.lblTRID.Name = "lblTRID"
        Me.lblTRID.Size = New System.Drawing.Size(85, 16)
        Me.lblTRID.TabIndex = 72
        Me.lblTRID.Text = "Tranfer ID :"
        Me.lblTRID.Visible = False
        '
        'lblAction
        '
        Me.lblAction.AutoSize = True
        Me.lblAction.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAction.ForeColor = System.Drawing.Color.Black
        Me.lblAction.Location = New System.Drawing.Point(224, 432)
        Me.lblAction.Name = "lblAction"
        Me.lblAction.Size = New System.Drawing.Size(43, 13)
        Me.lblAction.TabIndex = 117
        Me.lblAction.Text = "SAVE "
        Me.lblAction.Visible = False
        '
        'txtMain
        '
        Me.txtMain.Location = New System.Drawing.Point(250, 161)
        Me.txtMain.Name = "txtMain"
        Me.txtMain.Size = New System.Drawing.Size(0, 20)
        Me.txtMain.TabIndex = 122
        '
        'Panel2
        '
        Me.Panel2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel2.BackColor = System.Drawing.SystemColors.Info
        Me.Panel2.Controls.Add(Me.PictureBox1)
        Me.Panel2.Controls.Add(Me.PictureBox5)
        Me.Panel2.Controls.Add(Me.lblMsgbox)
        Me.Panel2.Controls.Add(Me.txtJobno)
        Me.Panel2.Controls.Add(Me.lblJobno)
        Me.Panel2.Controls.Add(Me.plHand)
        Me.Panel2.Controls.Add(Me.pic3)
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Controls.Add(Me.lblAction)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Controls.Add(Me.Panel6)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Location = New System.Drawing.Point(32, 37)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(800, 600)
        Me.Panel2.TabIndex = 123
        '
        'PictureBox1
        '
        Me.PictureBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(206, 500)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(25, 26)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 149
        Me.PictureBox1.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(686, 3)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(111, 60)
        Me.PictureBox5.TabIndex = 143
        Me.PictureBox5.TabStop = False
        '
        'lblMsgbox
        '
        Me.lblMsgbox.BackColor = System.Drawing.Color.Khaki
        Me.lblMsgbox.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsgbox.ForeColor = System.Drawing.Color.DarkRed
        Me.lblMsgbox.Location = New System.Drawing.Point(206, 500)
        Me.lblMsgbox.Name = "lblMsgbox"
        Me.lblMsgbox.Size = New System.Drawing.Size(482, 49)
        Me.lblMsgbox.TabIndex = 140
        Me.lblMsgbox.Text = "Please scan job no.!"
        Me.lblMsgbox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtJobno
        '
        Me.txtJobno.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobno.ForeColor = System.Drawing.Color.Black
        Me.txtJobno.Location = New System.Drawing.Point(348, 66)
        Me.txtJobno.Name = "txtJobno"
        Me.txtJobno.Size = New System.Drawing.Size(247, 19)
        Me.txtJobno.TabIndex = 124
        '
        'lblJobno
        '
        Me.lblJobno.AutoSize = True
        Me.lblJobno.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobno.ForeColor = System.Drawing.Color.Black
        Me.lblJobno.Location = New System.Drawing.Point(285, 67)
        Me.lblJobno.Name = "lblJobno"
        Me.lblJobno.Size = New System.Drawing.Size(65, 16)
        Me.lblJobno.TabIndex = 123
        Me.lblJobno.Text = "JOB NO :"
        '
        'pic3
        '
        Me.pic3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.pic3.Image = Global.AMTOPM.My.Resources.Resources.Arrow1
        Me.pic3.Location = New System.Drawing.Point(194, 423)
        Me.pic3.Name = "pic3"
        Me.pic3.Size = New System.Drawing.Size(22, 22)
        Me.pic3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic3.TabIndex = 121
        Me.pic3.TabStop = False
        Me.pic3.Visible = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Olive
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Location = New System.Drawing.Point(206, 283)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(479, 127)
        Me.Panel3.TabIndex = 137
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(360, 253)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(0, 13)
        Me.Label2.TabIndex = 27
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Olive
        Me.Panel6.Controls.Add(Me.lblStatus)
        Me.Panel6.Location = New System.Drawing.Point(209, 122)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(479, 127)
        Me.Panel6.TabIndex = 136
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.Red
        Me.lblStatus.Location = New System.Drawing.Point(360, 253)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(0, 13)
        Me.lblStatus.TabIndex = 27
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Olive
        Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(207, 257)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(170, 36)
        Me.Label9.TabIndex = 138
        Me.Label9.Text = "TRANSFER TO"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Olive
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(209, 98)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(262, 36)
        Me.Label3.TabIndex = 139
        Me.Label3.Text = "TRANSFER FROM"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TimerMachine
        '
        Me.TimerMachine.Enabled = True
        Me.TimerMachine.Interval = 1000
        '
        'frmTransfer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.ClientSize = New System.Drawing.Size(866, 711)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtMain)
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmTransfer"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.plHand.ResumeLayout(False)
        Me.plHand.PerformLayout()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.pic2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents plHand As System.Windows.Forms.Panel
    Friend WithEvents lblOPName As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents lblOPid As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblTRName As System.Windows.Forms.Label
    Friend WithEvents lblTRID As System.Windows.Forms.Label
    Friend WithEvents txtOPName As System.Windows.Forms.Label
    Friend WithEvents txtOPid As System.Windows.Forms.Label
    Friend WithEvents txtTRName As System.Windows.Forms.Label
    Friend WithEvents txtTRID As System.Windows.Forms.Label
    Friend WithEvents lblAction As System.Windows.Forms.Label
    Friend WithEvents pic1 As System.Windows.Forms.PictureBox
    Friend WithEvents pic2 As System.Windows.Forms.PictureBox
    Friend WithEvents pic3 As System.Windows.Forms.PictureBox
    Friend WithEvents txtMain As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtJobno As System.Windows.Forms.Label
    Friend WithEvents lblJobno As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblMsgbox As System.Windows.Forms.Label
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents TimerMachine As System.Windows.Forms.Timer
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
End Class
