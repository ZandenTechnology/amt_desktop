<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAuto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAuto))
        Me.EventLog1 = New System.Diagnostics.EventLog
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.butClose = New System.Windows.Forms.Button
        Me.txtTime = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.EventLog2 = New System.Diagnostics.EventLog
        Me.Label3 = New System.Windows.Forms.Label
        Me.lblTime = New System.Windows.Forms.Label
        Me.lblRunTime = New System.Windows.Forms.Label
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.DownloadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.mn1 = New System.Windows.Forms.ToolStripMenuItem
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtUserName = New System.Windows.Forms.TextBox
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnSubmit = New System.Windows.Forms.Button
        Me.plDownload = New System.Windows.Forms.Panel
        Me.Button1 = New System.Windows.Forms.Button
        Me.dtpTo = New System.Windows.Forms.DateTimePicker
        Me.dtpFrom = New System.Windows.Forms.DateTimePicker
        Me.txtTo = New System.Windows.Forms.TextBox
        Me.txtFrom = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.butDownload = New System.Windows.Forms.Button
        Me.PlMain = New System.Windows.Forms.Panel
        Me.Button2 = New System.Windows.Forms.Button
        Me.lblLogError = New System.Windows.Forms.Label
        CType(Me.EventLog1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EventLog2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.plDownload.SuspendLayout()
        Me.PlMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'EventLog1
        '
        Me.EventLog1.Log = "Application"
        Me.EventLog1.SynchronizingObject = Me
        '
        'NotifyIcon1
        '
        resources.ApplyResources(Me.NotifyIcon1, "NotifyIcon1")
        '
        'butClose
        '
        Me.butClose.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.butClose, "butClose")
        Me.butClose.ForeColor = System.Drawing.Color.Transparent
        Me.butClose.Name = "butClose"
        Me.butClose.UseVisualStyleBackColor = False
        '
        'txtTime
        '
        Me.txtTime.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        resources.ApplyResources(Me.txtTime, "txtTime")
        Me.txtTime.Name = "txtTime"
        Me.txtTime.ReadOnly = True
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Name = "Label1"
        '
        'EventLog2
        '
        Me.EventLog2.Log = "Application"
        Me.EventLog2.Source = "AMT"
        Me.EventLog2.SynchronizingObject = Me
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Name = "Label3"
        '
        'lblTime
        '
        resources.ApplyResources(Me.lblTime, "lblTime")
        Me.lblTime.BackColor = System.Drawing.Color.Transparent
        Me.lblTime.Name = "lblTime"
        '
        'lblRunTime
        '
        resources.ApplyResources(Me.lblRunTime, "lblRunTime")
        Me.lblRunTime.BackColor = System.Drawing.Color.Transparent
        Me.lblRunTime.Name = "lblRunTime"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DownloadToolStripMenuItem, Me.mn1})
        resources.ApplyResources(Me.MenuStrip1, "MenuStrip1")
        Me.MenuStrip1.Name = "MenuStrip1"
        '
        'DownloadToolStripMenuItem
        '
        Me.DownloadToolStripMenuItem.Name = "DownloadToolStripMenuItem"
        resources.ApplyResources(Me.DownloadToolStripMenuItem, "DownloadToolStripMenuItem")
        '
        'mn1
        '
        Me.mn1.Name = "mn1"
        resources.ApplyResources(Me.mn1, "mn1")
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.txtUserName)
        Me.Panel1.Controls.Add(Me.txtPassword)
        Me.Panel1.Controls.Add(Me.btnCancel)
        Me.Panel1.Controls.Add(Me.btnSubmit)
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.Name = "Panel1"
        '
        'PictureBox1
        '
        resources.ApplyResources(Me.PictureBox1, "PictureBox1")
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.TabStop = False
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Name = "Label4"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Name = "Label5"
        '
        'txtUserName
        '
        Me.txtUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.txtUserName, "txtUserName")
        Me.txtUserName.Name = "txtUserName"
        '
        'txtPassword
        '
        Me.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.txtPassword, "txtPassword")
        Me.txtPassword.Name = "txtPassword"
        '
        'btnCancel
        '
        resources.ApplyResources(Me.btnCancel, "btnCancel")
        Me.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCancel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnCancel.Name = "btnCancel"
        '
        'btnSubmit
        '
        Me.btnSubmit.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.btnSubmit, "btnSubmit")
        Me.btnSubmit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSubmit.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnSubmit.Name = "btnSubmit"
        Me.btnSubmit.UseVisualStyleBackColor = False
        '
        'plDownload
        '
        Me.plDownload.BackColor = System.Drawing.Color.WhiteSmoke
        Me.plDownload.Controls.Add(Me.Button1)
        Me.plDownload.Controls.Add(Me.dtpTo)
        Me.plDownload.Controls.Add(Me.dtpFrom)
        Me.plDownload.Controls.Add(Me.txtTo)
        Me.plDownload.Controls.Add(Me.txtFrom)
        Me.plDownload.Controls.Add(Me.Label2)
        Me.plDownload.Controls.Add(Me.Label6)
        Me.plDownload.Controls.Add(Me.butDownload)
        resources.ApplyResources(Me.plDownload, "plDownload")
        Me.plDownload.Name = "plDownload"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.Button1, "Button1")
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Button1.Name = "Button1"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'dtpTo
        '
        resources.ApplyResources(Me.dtpTo, "dtpTo")
        Me.dtpTo.Name = "dtpTo"
        '
        'dtpFrom
        '
        resources.ApplyResources(Me.dtpFrom, "dtpFrom")
        Me.dtpFrom.Name = "dtpFrom"
        '
        'txtTo
        '
        resources.ApplyResources(Me.txtTo, "txtTo")
        Me.txtTo.Name = "txtTo"
        '
        'txtFrom
        '
        resources.ApplyResources(Me.txtFrom, "txtFrom")
        Me.txtFrom.Name = "txtFrom"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'butDownload
        '
        Me.butDownload.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.butDownload, "butDownload")
        Me.butDownload.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butDownload.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.butDownload.Name = "butDownload"
        Me.butDownload.UseVisualStyleBackColor = False
        '
        'PlMain
        '
        Me.PlMain.BackColor = System.Drawing.Color.Transparent
        Me.PlMain.Controls.Add(Me.Button2)
        Me.PlMain.Controls.Add(Me.lblRunTime)
        Me.PlMain.Controls.Add(Me.txtTime)
        Me.PlMain.Controls.Add(Me.Label1)
        Me.PlMain.Controls.Add(Me.butClose)
        Me.PlMain.Controls.Add(Me.lblTime)
        Me.PlMain.Controls.Add(Me.Label3)
        resources.ApplyResources(Me.PlMain, "PlMain")
        Me.PlMain.Name = "PlMain"
        '
        'Button2
        '
        resources.ApplyResources(Me.Button2, "Button2")
        Me.Button2.Name = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'lblLogError
        '
        resources.ApplyResources(Me.lblLogError, "lblLogError")
        Me.lblLogError.BackColor = System.Drawing.Color.Transparent
        Me.lblLogError.ForeColor = System.Drawing.Color.Red
        Me.lblLogError.Name = "lblLogError"
        '
        'frmAuto
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.Silver
        resources.ApplyResources(Me, "$this")
        Me.ControlBox = False
        Me.Controls.Add(Me.lblLogError)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.PlMain)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.plDownload)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "frmAuto"
        CType(Me.EventLog1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EventLog2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.plDownload.ResumeLayout(False)
        Me.plDownload.PerformLayout()
        Me.PlMain.ResumeLayout(False)
        Me.PlMain.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EventLog1 As System.Diagnostics.EventLog
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents butClose As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTime As System.Windows.Forms.TextBox
    Friend WithEvents EventLog2 As System.Diagnostics.EventLog
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents lblRunTime As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents DownloadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mn1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSubmit As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtUserName As System.Windows.Forms.TextBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents plDownload As System.Windows.Forms.Panel
    Friend WithEvents PlMain As System.Windows.Forms.Panel
    Friend WithEvents butDownload As System.Windows.Forms.Button
    Friend WithEvents dtpTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtTo As System.Windows.Forms.TextBox
    Friend WithEvents txtFrom As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lblLogError As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button

End Class
