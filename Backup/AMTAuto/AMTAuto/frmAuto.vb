Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.DateAndTime
Imports System.IO
Imports System.Web.Mail
Public Class frmAuto
    Dim dr As SqlDataReader
    Dim parm As SqlParameter
    Dim OpenAction As String
    Dim Dowboolean As String
    Dim passpar As String
    Dim downst As Boolean
    Dim SendMailSt As Boolean

    Private Sub NotifyIcon1_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick
        Me.WindowState = FormWindowState.Normal
        Me.NotifyIcon1.Visible = False
        Me.ShowInTaskbar = True
    End Sub
    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        Try
            '    Dim s As System.Diagnostics.EventLogEntryType
            's = EventLogEntryType.Error
            '   s = EventLogEntryType.Warning
            '  EventLog2.WriteEntry("hi", s)
            Me.NotifyIcon1.Visible = True
            Me.WindowState = FormWindowState.Minimized
            Me.ShowInTaskbar = False
            OpenAction = ""

        Catch ex As Exception
            lblLogError.Text = ex.Message
        End Try
    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        lblRunTime.Text = Format(Now, "HH:mm:ss")
        If RealDownload = Format(Now, "HHmm") And downst = True Then
            Dowboolean = Format(DateAdd(DateInterval.Minute, 3, Now), "HHmm")
            txtFrom.Text = Format(DateAdd(DateInterval.Year, -1, Now), "dd/MM/yyyy") 'Format(DateAdd(DateInterval.Day, -1, Now), "dd/MM/yyyy") 'As Request Shawn Sl9, He want auto import with time frame a year. Update code 12 Feb 2018
            txtTo.Text = Format(DateAdd(DateInterval.Day, 1, Now), "dd/MM/yyyy")
            passpar = "Auto"
            Timer1.Stop()
            butDownload_Click(sender, e)
            Timer1.Start()
            Dowboolean = "0"
            downst = False
        ElseIf RealDownload <> Format(Now, "HHmm") Then
            downst = True
        End If

        If SendMailSt = True And SendMailTime = Format(Now, "HHmm") Then
            SendMailSt = False
            SendAuoEmail()
        ElseIf SendMailTime <> Format(Now, "HHmm") Then
            SendMailSt = True
        End If



    End Sub
    Private Sub frmAuto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        OpenAction = ""
        lblTime.Text = ""
        lblRunTime.Text = ""
        Dowboolean = "0"
        passpar = "Manual"
        txtTime.Text = RealDownload
        downst = True
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        lblLogError.Text = ""
        PlMain.Visible = True
        Panel1.Visible = False
        OpenAction = ""
        txtUserName.Text = ""
        txtPassword.Text = ""
        MenuStrip1.Enabled = True
    End Sub
    Private Sub DownloadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DownloadToolStripMenuItem.Click
        dtpFrom.Value = Now
        dtpTo.Value = Now
        txtFrom.Text = Format(Now, "dd/MM/yyyy")
        txtTo.Text = Format(Now, "dd/MM/yyyy")
        MenuStrip1.Enabled = False
        PlMain.Visible = False
        Panel1.Visible = True
        OpenAction = "Download"
    End Sub
    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        lblLogError.Text = ""


        LogUserID = ""
        LogName = ""
        LogUserName = ""
        LogPrivilege = ""
        If Trim(txtUserName.Text) = "" Then

            lblLogError.Text = "Enter the username!"
            txtUserName.Focus()
        ElseIf Trim(txtPassword.Text) = "" Then
            lblLogError.Text = "Enter the password!"

            txtPassword.Focus()
        Else
            Try
                With com
                    com.Connection = cn
                    com.CommandType = CommandType.Text
                    com.CommandText = "select * from tbUser where _userName=@UserName"

                    parm = .Parameters.Add("@UserName", SqlDbType.NVarChar, 255)
                    parm.Value = Trim(txtUserName.Text)
                    cn.Open()
                    dr = com.ExecuteReader(CommandBehavior.CloseConnection)
                    If dr.Read Then
                        If dr.Item("_password") = Trim(txtPassword.Text) Then

                            LogUserID = dr.Item("_userID")
                            LogName = dr.Item("_name")
                            LogUserName = dr.Item("_userName")
                            LogPrivilege = dr.Item("_privilege")
                            cn.Close()
                            .Parameters.Clear()
                            If OpenAction = "Quit" Then
                                If APPCheck("Quit") = True Then
                                    lblTime.Text = Format(Now, "yyy-MM-dd HH:mm")
                                    Dim sw As System.Diagnostics.EventLogEntryType
                                    's = EventLogEntryType.Error
                                    sw = EventLogEntryType.Warning
                                    'EventLog2.WriteEntry("Quit(" & LogUserName & ") ", sw)
                                    Application.Exit()
                                End If
                            Else
                                plDownload.Visible = True
                                PlMain.Visible = False
                                Panel1.Visible = False
                            End If

                        Else
                            lblLogError.Text = "password is not Correct!"

                        End If
                    Else
                        lblLogError.Text = "Username not Correct!"

                    End If
                End With
            Catch exce As Exception
                lblLogError.Text = exce.Message

            Finally
                cn.Close()
                com.Parameters.Clear()
            End Try
        End If
    End Sub

    Function GetAllDataDownload() As Boolean
        lblLogError.Text = ""
        Dim todw As Boolean = False
        Dim DSJob As New DataSet
        Dim DSRoute As New DataSet
        Dim DSJobAC As New DataSet
        Dim DSRouteAC As New DataSet
        Dim DSTransAC As New DataSet
        Dim DSWS As New DataSet
        Dim DSJRT As New DataSet
        Dim DSwc As New DataSet        'By Yeo 20081110

        Dim tot As Integer
        Dim s() As String = Split(txtFrom.Text, "/")
        Dim SD As Date = DateSerial(s(2), s(1), s(0))
        Dim s1() As String = Split(txtTo.Text, "/")
        Dim ED As Date = DateSerial(s1(2), s1(1), s1(0))
        Try
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cnser
                .CommandType = CommandType.Text

                .CommandText = "SELECT type,job,item,suffix,job_date, qty_released,qty_complete,qty_scrapped ,stat FROM         job_mst where stat in('R','S') and job_date between @Sd and @ED"
                parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
                parm.Value = SD
                parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
                parm.Value = ED

                DA.SelectCommand = com
                DA.Fill(DSJob, "tbsch")
                .Parameters.Clear()
            End With
            tot = tot + DSJob.Tables(0).Rows.Count
            Application.DoEvents()
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cnser
                .CommandType = CommandType.Text
                'job,suffix,opernum,wc

                .CommandText = "select jobroute_mst.job as job, jobroute_mst.suffix as suffix, jobroute_mst.oper_num as opernum, jobroute_mst.wc as wc, jobroute_mst.RecordDate as start_Date, jobroute_mst.qty_received as qty_op_qty, jobroute_mst.qty_complete as qty_complete, jobroute_mst.qty_scrapped as qty_scrapped, jobroute_mst.qty_moved as qty_Rele_qty from jobroute_mst  inner join job_mst on jobroute_mst.job=job_mst.job and jobroute_mst.suffix=job_mst.suffix where job_mst.stat in('R','S') and job_mst.job_date between @Sd and @ED"
                parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
                parm.Value = SD
                parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
                parm.Value = ED

                DA.SelectCommand = com
                DA.Fill(DSRoute, "tbsch")
                .Parameters.Clear()
            End With


            'JRT SCH Table
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cnser
                .CommandType = CommandType.Text
                .CommandText = " select job,suffix,oper_num,run_lbr_hrs,run_mch_hrs from jrt_sch_mst where  job in(select job from job_mst where stat in('R','S') and job_date between @Sd and @ED)"
                parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
                parm.Value = SD
                parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
                parm.Value = ED

                DA.SelectCommand = com
                DA.Fill(DSJRT, "tbJRT")
                .Parameters.Clear()
            End With
            '********************************************************************

        Catch ex As Exception
            lblLogError.Text = (ex.Message)

            Exit Function
        End Try

        Dim DSMaterial As New DataSet
        With com
            Dim DA As New SqlDataAdapter
            .Connection = cnser
            .CommandType = CommandType.Text
            .CommandText = " select Job,item from jobmatl_mst where Job in (select job from job_mst where stat in('R','S') and Job_date between @Sd and @ED)"
            parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
            parm.Value = SD
            parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
            parm.Value = ED

            DA.SelectCommand = com
            DA.Fill(DSMaterial, "tbmat")
            .Parameters.Clear()
        End With
        Application.DoEvents()




        Try
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cn
                .CommandType = CommandType.Text

                .CommandText = "SELECT * FROM tbJob where _jobDate between @Sd and @ED"
                parm = .Parameters.Add("@Sd", SqlDbType.Float)
                parm.Value = SD.ToOADate
                parm = .Parameters.Add("@Ed", SqlDbType.Float)
                parm.Value = ED.ToOADate
                DA.SelectCommand = com
                DA.Fill(DSJobAC, "tbsch")
                .Parameters.Clear()
            End With
        Catch ex As Exception
            lblLogError.Text = (ex.Message)

            Exit Function
        End Try

        Try
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select tbJobRoute.* from tbJobRoute  inner join tbJob on tbJobRoute._job=tbJob._job and tbJobRoute._jobSuffix=tbJob._jobSuffix where tbJob._jobDate between @Sd  and @ED"
                parm = .Parameters.Add("@Sd", SqlDbType.Float)
                parm.Value = SD.ToOADate
                parm = .Parameters.Add("@Ed", SqlDbType.Float)
                parm.Value = ED.ToOADate
                DA.SelectCommand = com
                DA.Fill(DSRouteAC, "tbsch")
                .Parameters.Clear()
            End With
        Catch ex As Exception
            lblLogError.Text = (ex.Message)
            Exit Function
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try

        'Add codes at 26 Feb 2018
        'Delete Duplicate rows
        Dim DSTransACDuplicate As New DataSet
        Try
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select _job, _jobsuffix, _jobsuffixParent, _item, _oper_num, _wc, count(*) as duplicate_count from tbjobtrans group by _job, _jobsuffix, _jobsuffixParent, _item, _oper_num, _wc having count(*) > 1"
                DA.SelectCommand = com
                DA.Fill(DSTransACDuplicate, "tbsch")
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Function
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try

        Dim Duplicate() As DataRow = DSTransACDuplicate.Tables(0).Select()
        If Duplicate.Length > 0 Then
            For m As Integer = 0 To Duplicate.Length - 1
                Dim item As Object = Duplicate(m)
                With com
                    Try
                        .Connection = cn
                        .CommandText = CommandType.Text
                        ' Delete Top-N' Rows from a Table with some sorting(order by 'Column')
                        ' https://stackoverflow.com/questions/13508280/delete-top-n-rows-from-a-table-with-some-sortingorder-by-column
                        .CommandText = "with T as (select top " & item("duplicate_count") - 1 & " * from tbJobTrans where _job=@job and _jobsuffix=@jobsuffix and _jobsuffixParent=@jobsuffixParent and _item=@item and _oper_num=@oper_num and _wc=@wc order by _tansnum desc) delete from T"
                        parm = .Parameters.Add("@job", SqlDbType.VarChar, 50)
                        parm.Value = item("_job")
                        parm = .Parameters.Add("@jobsuffix", SqlDbType.VarChar, 10)
                        parm.Value = item("_jobsuffix")
                        parm = .Parameters.Add("@jobsuffixParent", SqlDbType.VarChar, 10)
                        parm.Value = item("_jobsuffixParent")
                        parm = .Parameters.Add("@item", SqlDbType.VarChar, 50)
                        parm.Value = item("_item")
                        parm = .Parameters.Add("@oper_num", SqlDbType.Int)
                        parm.Value = item("_oper_num")
                        parm = .Parameters.Add("@wc", SqlDbType.VarChar, 50)
                        parm.Value = item("_wc")
                        cn.Open()
                        .ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        com.Parameters.Clear()
                        cn.Close()
                    End Try
                End With
            Next
        End If

        Dim DSTransACDuplicateToo As New DataSet
        Try
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "declare @row int declare @job varchar(50) declare @jobsuffix varchar(50) declare @jobsuffixParent varchar(50) declare @item varchar(50) declare @oper_num int declare @wc varchar(50) declare @table_duplicate table(_job varchar(50), _jobsuffix varchar(50), _jobsuffixParent varchar(50), _item varchar(50), _oper_num int, _wc varchar(50)) (select row_number() over (order by _job, _oper_num desc) as _row, _job, _jobsuffixParent, _item, _oper_num, _wc into table_sampah from tbJobtrans group by _job, _jobsuffixParent, _item, _oper_num, _wc having count(*) > 1) while exists (select * from table_sampah) begin select top 1 @row=_row, @job=_job, @jobsuffixParent=_jobsuffixParent, @item=_item, @oper_num=_oper_num, @wc=_wc from table_sampah if (select sum(_qty_op_qty) from tbjobtrans where _job=@job and _jobsuffixParent=@jobsuffixParent and _item=@item and _oper_num=@oper_num and _wc=@wc) > (select sum(_qty_Complete) from tbjobtrans where _job=@job and _jobsuffixParent=@jobsuffixParent and _item=@item and _oper_num=(select max(_operationNo) from tbjobroute where _job=@job and _operationNo<@oper_num)) begin insert into @table_duplicate(_job, _jobsuffix, _jobsuffixParent, _item, _oper_num, _wc)values(@job, @jobsuffixParent, @jobsuffixParent, @item, @oper_num, @wc) end delete table_sampah where _row=@row end drop table table_sampah select * from @table_duplicate"
                DA.SelectCommand = com
                DA.Fill(DSTransACDuplicateToo, "tbsch")
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Function
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try

        Dim DuplicateToo() As DataRow = DSTransACDuplicateToo.Tables(0).Select()
        If DuplicateToo.Length > 0 Then
            Dim tes As String = DuplicateToo(0).Item("_job")
            For m As Integer = 0 To DuplicateToo.Length - 1
                Dim item As Object = DuplicateToo(m)
                With com
                    Try
                        .Connection = cn
                        .CommandText = CommandType.Text
                        ' Delete Top-N' Rows from a Table with some sorting(order by 'Column')
                        ' https://stackoverflow.com/questions/13508280/delete-top-n-rows-from-a-table-with-some-sortingorder-by-column
                        .CommandText = "with T as (select * from tbJobTrans where _job=@job and _jobsuffix=@jobsuffix and _jobsuffixParent=@jobsuffixParent and _item=@item and _oper_num=@oper_num and _wc=@wc) delete from T"
                        parm = .Parameters.Add("@job", SqlDbType.VarChar, 50)
                        parm.Value = item("_job")
                        parm = .Parameters.Add("@jobsuffix", SqlDbType.VarChar, 10)
                        parm.Value = item("_jobsuffix")
                        parm = .Parameters.Add("@jobsuffixParent", SqlDbType.VarChar, 10)
                        parm.Value = item("_jobsuffixParent")
                        parm = .Parameters.Add("@item", SqlDbType.VarChar, 50)
                        parm.Value = item("_item")
                        parm = .Parameters.Add("@oper_num", SqlDbType.Int)
                        parm.Value = item("_oper_num")
                        parm = .Parameters.Add("@wc", SqlDbType.VarChar, 50)
                        parm.Value = item("_wc")
                        cn.Open()
                        .ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    Finally
                        com.Parameters.Clear()
                        cn.Close()
                    End Try
                End With
            Next
        End If
        'End Add codes at 26 Feb 2018

        Try
            With com
                Dim DA As New SqlDataAdapter
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select tbJobTrans.* from tbJobTrans  inner join tbJob on tbJobTrans._job=tbJob._job and tbJobTrans._item=tbJob._item where tbJob._jobDate between @Sd  and @ED"
                parm = .Parameters.Add("@Sd", SqlDbType.Float)
                parm.Value = SD.ToOADate
                parm = .Parameters.Add("@Ed", SqlDbType.Float)
                parm.Value = ED.ToOADate
                DA.SelectCommand = com
                DA.Fill(DSTransAC, "tbsch")
                .Parameters.Clear()
            End With
        Catch ex As Exception
            lblLogError.Text = (ex.Message)
            Exit Function
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try

        Try
            If DSJob.Tables(0).Rows.Count > 0 Then



                Dim i As Integer
                For i = 0 To DSJob.Tables(0).Rows.Count - 1

                    Dim DRMat As DataRow() = DSMaterial.Tables(0).Select("Job='" & Trim(DSJob.Tables(0).Rows(i).Item("job")) & "'")

                    Dim strmat As String = ""
                    If DRMat.Length > 0 Then
                        Dim HAsMat As New Hashtable
                        Dim j2 As Integer
                        For j2 = 0 To DRMat.Length - 1
                            If strmat = "" Then
                                strmat = DRMat(j2).Item("item")
                            Else
                                If HAsMat.Contains(DRMat(j2).Item("item")) = False Then
                                    strmat = strmat & "||" & DRMat(j2).Item("item")
                                    HAsMat.Add(DRMat(j2).Item("item"), DRMat(j2).Item("item"))
                                End If
                            End If
                        Next
                    End If



                    Dim Stup As Boolean = False
                    With com
                        Try
                            .Connection = cn
                            .CommandType = CommandType.StoredProcedure
                            .CommandText = "sp_tbJob_Download_auto"

                            ' type,job,item,suffix,job_date, qty_released,qty_complete,qty_scrapped
                            parm = .Parameters.Add("@job_" & 1, SqlDbType.VarChar, 50)
                            parm.Value = Trim(DSJob.Tables(0).Rows(i).Item("job"))

                            parm = .Parameters.Add("@jobsuffix_" & 1, SqlDbType.Int)
                            parm.Value = DSJob.Tables(0).Rows(i).Item("suffix")

                            parm = .Parameters.Add("@jobDate_" & 1, SqlDbType.Float)
                            parm.Value = CDate(DSJob.Tables(0).Rows(i).Item("job_date")).ToOADate

                            parm = .Parameters.Add("@jobtype_" & 1, SqlDbType.VarChar, 50)
                            parm.Value = DSJob.Tables(0).Rows(i).Item("type")

                            parm = .Parameters.Add("@item_" & 1, SqlDbType.VarChar, 50)
                            parm.Value = DSJob.Tables(0).Rows(i).Item("item")

                            parm = .Parameters.Add("@qtyReleased_" & 1, SqlDbType.Float)
                            parm.Value = DSJob.Tables(0).Rows(i).Item("qty_released")

                            parm = .Parameters.Add("@qtyCompleted_" & 1, SqlDbType.Float)
                            parm.Value = DSJob.Tables(0).Rows(i).Item("qty_complete")

                            parm = .Parameters.Add("@qtyScrapped_" & 1, SqlDbType.Float)
                            parm.Value = DSJob.Tables(0).Rows(i).Item("qty_scrapped")
                            parm = .Parameters.Add("@stas_" & 1, SqlDbType.Char, 1)
                            parm.Value = DSJob.Tables(0).Rows(i).Item("stat")
                            parm = .Parameters.Add("@material_" & 1, SqlDbType.VarChar, 1000)
                            parm.Value = strmat

                            Dim NR() As DataRow = DSJobAC.Tables(0).Select("_job='" & Trim(DSJob.Tables(0).Rows(i).Item("job")) & "' and _jobsuffix=" & DSJob.Tables(0).Rows(i).Item("suffix"))
                            parm = .Parameters.Add("@status_" & 1, SqlDbType.VarChar, 255)
                            If NR.Length > 0 Then
                                If NR(0).Item("_qtyReleased") = DSJob.Tables(0).Rows(i).Item("qty_released") Then
                                    parm.Value = "No"
                                Else
                                    parm.Value = "Edit"
                                End If
                            Else
                                parm.Value = "New"
                            End If
                            cn.Open()
                            .ExecuteNonQuery()
                            .Parameters.Clear()
                            cn.Close()
                            Stup = True
                            'todw = True





                        Catch ex As Exception
                            Stup = False
                            Dim errtype As System.Diagnostics.EventLogEntryType
                            's = EventLogEntryType.Error
                            errtype = EventLogEntryType.Error
                            'MsgBox(("Download error-" & DSJob.Tables(0).Rows(i).Item("job")), MsgBoxStyle.Critical, "eWIP")
                        Finally
                            cn.Close()
                            .Parameters.Clear()
                        End Try

                    End With
                    Dim strAllJobroute As String = ""
                    If Stup = True Then
                        Dim NEWRoute() As DataRow = DSRoute.Tables(0).Select("job='" & Trim(DSJob.Tables(0).Rows(i).Item("job")) & "'")
                        If NEWRoute.Length > 0 Then
                            'Add codes at 26 Feb 2018
                            For n As Integer = 0 To NEWRoute.Length - 1
                                If (NEWRoute(n).Item("wc") = "STORER" Or NEWRoute(n).Item("wc") = "MIX") And NEWRoute(n).Item("qty_complete") = 0 Then
                                    NEWRoute(n).Item("qty_complete") = NEWRoute(n).Item("qty_op_qty")
                                    NEWRoute(n).Item("qty_Rele_qty") = NEWRoute(n).Item("qty_op_qty")

                                    Try
                                        If (NEWRoute(n + 1).Item("wc") <> "STORER" Or NEWRoute(n + 1).Item("wc") <> "MIX") And NEWRoute(n + 1).Item("qty_complete") = 0 Then
                                            NEWRoute(n + 1).Item("qty_op_qty") = NEWRoute(n).Item("qty_op_qty")
                                        End If
                                    Catch ex As Exception
                                        Exit For
                                    End Try
                                End If
                            Next
                            'End Add codes at 26 Feb 2018

                            Dim j As Integer = 0
                            For j = 0 To NEWRoute.Length - 1
                                With com
                                    .Connection = cn
                                    .CommandType = CommandType.StoredProcedure
                                    .CommandText = "sp_tbjobRoute_Download"
                                    Dim J1 As Integer
                                    For J1 = 0 To 14
                                        If j < NEWRoute.Length Then
                                            parm = .Parameters.Add("@job_" & J1 + 1, SqlDbType.VarChar, 50)
                                            parm.Value = Trim(NEWRoute(j).Item("job"))

                                            parm = .Parameters.Add("@jobsuffix_" & J1 + 1, SqlDbType.Int)
                                            parm.Value = NEWRoute(j).Item("suffix")

                                            parm = .Parameters.Add("@operationNo_" & J1 + 1, SqlDbType.Float)
                                            parm.Value = NEWRoute(j).Item("opernum")

                                            parm = .Parameters.Add("@wc_" & J1 + 1, SqlDbType.VarChar, 50)
                                            parm.Value = NEWRoute(j).Item("wc")

                                            'Get Labour or Machine
                                            Dim RWLBH() As DataRow = DSJRT.Tables(0).Select("job='" & Trim(NEWRoute(j).Item("job")) & "' and suffix='" & NEWRoute(j).Item("suffix") & "' and oper_num=" & NEWRoute(j).Item("opernum"))
                                            Dim lbrHur As Double = 0
                                            Dim MacHur As Double = 0
                                            If RWLBH.Length > 0 Then
                                                'run_lbr_hrs,run_mch_hrs
                                                lbrHur = RWLBH(0).Item("run_lbr_hrs")
                                                MacHur = RWLBH(0).Item("run_mch_hrs")
                                            End If

                                            parm = .Parameters.Add("@runlbrhrs_" & J1 + 1, SqlDbType.Decimal, 9)
                                            parm.Value = lbrHur
                                            parm = .Parameters.Add("@runmchhrs_" & J1 + 1, SqlDbType.Decimal, 9)
                                            parm.Value = MacHur




                                            Dim NR() As DataRow = DSRouteAC.Tables(0).Select("_job='" & Trim(NEWRoute(j).Item("job")) & "' and _jobsuffix=" & NEWRoute(j).Item("suffix") & " and  _operationNo=" & NEWRoute(j).Item("opernum"))
                                            parm = .Parameters.Add("@status_" & J1 + 1, SqlDbType.VarChar, 255)
                                            If NR.Length > 0 Then
                                                parm.Value = "Edit"
                                            Else
                                                parm.Value = "New"

                                            End If

                                            If strAllJobroute = "" Then
                                                strAllJobroute = NEWRoute(j).Item("opernum")
                                            Else
                                                strAllJobroute = strAllJobroute & "," & NEWRoute(j).Item("opernum")
                                            End If


                                        Else
                                            parm = .Parameters.Add("@job_" & J1 + 1, SqlDbType.VarChar, 50)
                                            parm.Value = ""
                                            parm = .Parameters.Add("@jobsuffix_" & J1 + 1, SqlDbType.Int)
                                            parm.Value = 0
                                            parm = .Parameters.Add("@operationNo_" & J1 + 1, SqlDbType.Float)
                                            parm.Value = 0
                                            parm = .Parameters.Add("@wc_" & J1 + 1, SqlDbType.VarChar, 50)
                                            parm.Value = ""
                                            parm = .Parameters.Add("@runlbrhrs_" & J1 + 1, SqlDbType.Decimal)
                                            parm.Value = 0
                                            parm = .Parameters.Add("@runmchhrs_" & J1 + 1, SqlDbType.Decimal)
                                            parm.Value = 0

                                            parm = .Parameters.Add("@status_" & J1 + 1, SqlDbType.VarChar, 255)
                                            parm.Value = ""

                                        End If
                                        j = j + 1
                                    Next
                                    j = j - 1
                                    cn.Open()
                                    .ExecuteNonQuery()
                                    .Parameters.Clear()
                                    cn.Close()
                                End With
                            Next

                            'Add codes at 5 Feb 2018
                            Dim is_k_last As Boolean = False
                            For k As Integer = 0 To NEWRoute.Length - 1
                                With com
                                    Try
                                        .Connection = cn
                                        .CommandType = CommandType.StoredProcedure
                                        .CommandText = "sp_tbjobTrans_Download_new"
                                        For J1 As Integer = 0 To 14
                                            parm = .Parameters.Add("@Nextstatus_" & J1 + 1, SqlDbType.Int)
                                            parm.Value = 0
                                            If k < NEWRoute.Length - 1 Then
                                                If NEWRoute(k).Item("qty_complete") <> 0 And NEWRoute(k + 1).Item("qty_complete") = 0 And NEWRoute(k + 1).Item("qty_op_qty") = 0 Then
                                                    parm.Value = 0
                                                End If
                                            End If

                                            If k < NEWRoute.Length And is_k_last = False Then
                                                Dim NR As DataRow() = DSTransAC.Tables(0).Select("_job='" & Trim(NEWRoute(k).Item("job")) & "' and _oper_num=" & NEWRoute(k).Item("opernum") & " and _wc='" & NEWRoute(k).Item("wc") & "' and _qty_op_qty=" & NEWRoute(k).Item("qty_op_qty"))
                                                Dim NRs As DataRow() = DSTransAC.Tables(0).Select("_job='" & Trim(NEWRoute(k).Item("job")) & "' and _oper_num=" & NEWRoute(k).Item("opernum") & " and _wc='" & NEWRoute(k).Item("wc") & "'")
                                                Dim sum_qty_op_qty As Double = 0
                                                For a As Integer = 0 To NRs.Length - 1
                                                    sum_qty_op_qty += NRs(a).Item("_qty_op_qty")
                                                Next

                                                parm = .Parameters.Add("@job_" & J1 + 1, SqlDbType.VarChar, 50)
                                                'parm.Value = Trim(NEWRoute(k).Item("job"))
                                                If (NEWRoute(k).Item("qty_complete") = 0 And NEWRoute(k).Item("qty_scrapped") = 0 And NEWRoute(k).Item("qty_op_qty") = 0) Or NR.Length > 0 Or sum_qty_op_qty >= NEWRoute(k).Item("qty_op_qty") Then
                                                    parm.Value = ""
                                                Else
                                                    parm.Value = Trim(NEWRoute(k).Item("job"))
                                                End If

                                                parm = .Parameters.Add("@jobsuffix_" & J1 + 1, SqlDbType.VarChar, 10)
                                                If NEWRoute(k).Item("suffix") = 0 Then
                                                    parm.Value = "M"
                                                Else
                                                    parm.Value = "A"
                                                End If

                                                parm = .Parameters.Add("@jobsuffixParent_" & J1 + 1, SqlDbType.VarChar, 10)
                                                parm.Value = "M"

                                                parm = .Parameters.Add("@operationNo_" & J1 + 1, SqlDbType.Int)
                                                parm.Value = NEWRoute(k).Item("opernum")

                                                parm = .Parameters.Add("@wc_" & J1 + 1, SqlDbType.VarChar, 50)
                                                parm.Value = NEWRoute(k).Item("wc")

                                                parm = .Parameters.Add("@start_Date_" & J1 + 1, SqlDbType.Float)
                                                If NEWRoute(k).Item("qty_op_qty") <> 0 And NEWRoute(k).Item("qty_complete") <> 0 Then
                                                    parm.Value = CDate(NEWRoute(k).Item("start_Date")).ToOADate
                                                Else
                                                    parm.Value = 0
                                                End If

                                                parm = .Parameters.Add("@start_time_" & J1 + 1, SqlDbType.Int)
                                                If NEWRoute(k).Item("qty_op_qty") <> 0 And NEWRoute(k).Item("qty_complete") <> 0 Then
                                                    parm.Value = parm.Value = Convert.ToInt32(NEWRoute(k).Item("start_Date").TimeOfDay.TotalSeconds)
                                                Else
                                                    parm.Value = 0
                                                End If

                                                parm = .Parameters.Add("@qty_scrapped_" & J1 + 1, SqlDbType.Float)
                                                If NEWRoute(k).Item("qty_op_qty") <> 0 And NEWRoute(k).Item("qty_complete") <> 0 Then
                                                    parm.Value = NEWRoute(k).Item("qty_scrapped")
                                                Else
                                                    parm.Value = 0
                                                End If

                                                parm = .Parameters.Add("@qty_Rele_qty_" & J1 + 1, SqlDbType.Float)
                                                If (NEWRoute(k).Item("qty_op_qty") <> 0 Or NEWRoute(k).Item("qty_Rele_qty") <> 0) And NEWRoute(k).Item("qty_complete") <> 0 Then
                                                    parm.Value = NEWRoute(k).Item("qty_Rele_qty")
                                                Else
                                                    parm.Value = 0
                                                End If

                                                parm = .Parameters.Add("@qty_complete_" & J1 + 1, SqlDbType.Float)
                                                If NEWRoute(k).Item("qty_op_qty") <> 0 Or NEWRoute(k).Item("qty_Rele_qty") <> 0 Then
                                                    parm.Value = NEWRoute(k).Item("qty_complete")
                                                Else
                                                    parm.Value = 0
                                                End If

                                                parm = .Parameters.Add("@qty_op_qty_" & J1 + 1, SqlDbType.Float)
                                                parm.Value = NEWRoute(k).Item("qty_op_qty")

                                                If k < NEWRoute.Length - 1 Then
                                                    k = k + 1
                                                Else
                                                    is_k_last = True
                                                End If
                                            Else
                                                parm = .Parameters.Add("@job_" & J1 + 1, SqlDbType.VarChar, 50)
                                                parm.Value = ""

                                                parm = .Parameters.Add("@jobsuffix_" & J1 + 1, SqlDbType.VarChar, 10)
                                                parm.Value = "-"

                                                parm = .Parameters.Add("@jobsuffixParent_" & J1 + 1, SqlDbType.VarChar, 10)
                                                parm.Value = "-"

                                                parm = .Parameters.Add("@operationNo_" & J1 + 1, SqlDbType.Int)
                                                parm.Value = 0

                                                parm = .Parameters.Add("@wc_" & J1 + 1, SqlDbType.VarChar, 50)
                                                parm.Value = ""

                                                parm = .Parameters.Add("@start_Date_" & J1 + 1, SqlDbType.Float)
                                                parm.Value = 0

                                                parm = .Parameters.Add("@start_time_" & J1 + 1, SqlDbType.Int)
                                                parm.Value = 0

                                                parm = .Parameters.Add("@qty_scrapped_" & J1 + 1, SqlDbType.Float)
                                                parm.Value = 0

                                                parm = .Parameters.Add("@qty_Rele_qty_" & J1 + 1, SqlDbType.Float)
                                                parm.Value = 0

                                                parm = .Parameters.Add("@qty_complete_" & J1 + 1, SqlDbType.Float)
                                                parm.Value = 0

                                                parm = .Parameters.Add("@qty_op_qty_" & J1 + 1, SqlDbType.Float)
                                                parm.Value = 0
                                            End If
                                        Next
                                        If (k Mod 14) = 1 And is_k_last = False Then
                                            k = k - 1
                                        End If
                                        cn.Open()
                                        .ExecuteNonQuery()
                                        .Parameters.Clear()
                                        cn.Close()
                                    Catch ex As Exception
                                        Dim errtype As System.Diagnostics.EventLogEntryType
                                        errtype = EventLogEntryType.Error
                                        MsgBox(ex.Message)
                                        'MsgBox(("Download error-" & DSRoute.Tables(0).Rows(i).Item("job") & ": " & ex.Message), MsgBoxStyle.Critical, "eWIP")
                                    End Try
                                End With
                            Next
                            'End add codes at 5 Feb 2018

                        End If

                    End If

                    If Trim(strAllJobroute) <> "" Then

                        With com
                            .Connection = cn
                            .CommandType = CommandType.Text
                            .CommandText = "delete from tbJobRoute where _Job='" & Trim(DSJob.Tables(0).Rows(i).Item("job")) & "' and _operationNo not in(" & strAllJobroute & ")"
                            cn.Open()
                            .ExecuteNonQuery()
                            cn.Close()
                        End With


                    End If


                Next






            End If

        Catch ex As Exception
            lblLogError.Text = (ex.Message)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try








    End Function
    Function GetWorkCenter() As Boolean
        Dim DSwc As New DataSet
        With com
            Dim DA As New SqlDataAdapter
            .Connection = cnser
            .CommandType = CommandType.Text
            .CommandText = "select wc,description,efficiency,dept,Run_Rate_Lbr,sched_drv from wc_mst"
            DA.SelectCommand = com
            DA.Fill(DSwc, "tbsch")
        End With
        Application.DoEvents()
        Try
            Dim DSAC As New DataSet

            If DSwc.Tables(0).Rows.Count > 0 Then
                Dim DA As New SqlDataAdapter
                With com


                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "select * from tbWC"
                    DA.SelectCommand = com
                    DA.Fill(DSAC, "tbWC")
                    .Parameters.Clear()
                End With
                Dim i As Integer
                If DSwc.Tables(0).Rows.Count > 0 Then
                    For i = 0 To DSwc.Tables(0).Rows.Count - 1
                        Dim DRAC() As DataRow
                        Dim stAC As Boolean = False
                        If DSAC.Tables(0).Rows.Count > 0 Then
                            DRAC = DSAC.Tables(0).Select("_wc='" & DSwc.Tables(0).Rows(i).Item("wc") & "'")
                            If DRAC.Length > 0 Then
                                If (DRAC(0).Item("_description") <> DSwc.Tables(0).Rows(i).Item("description")) Or (DRAC(0).Item("_labour") <> DSwc.Tables(0).Rows(i).Item("sched_drv")) Or (DRAC(0).Item("_Run_Rate") <> DSwc.Tables(0).Rows(i).Item("Run_Rate_Lbr")) Then
                                    stAC = False
                                Else
                                    stAC = True
                                End If
                            End If
                        End If

                        If stAC = False Then
                            With com
                                .Connection = cn
                                .CommandType = CommandType.StoredProcedure
                                .CommandText = "sp_tbwc_Download"

                                parm = .Parameters.Add("@_wc", SqlDbType.NVarChar, 6)
                                parm.Value = DSwc.Tables(0).Rows(i).Item("wc")

                                parm = .Parameters.Add("@_description", SqlDbType.NVarChar, 256)
                                parm.Value = DSwc.Tables(0).Rows(i).Item("description")

                                parm = .Parameters.Add("@_efficiency", SqlDbType.NVarChar, 40)
                                parm.Value = DSwc.Tables(0).Rows(i).Item("efficiency")

                                parm = .Parameters.Add("@_dept", SqlDbType.NVarChar, 40)
                                parm.Value = DSwc.Tables(0).Rows(i).Item("dept")

                                parm = .Parameters.Add("@_labour", SqlDbType.Char, 1)
                                parm.Value = DSwc.Tables(0).Rows(i).Item("sched_drv")
                                parm = .Parameters.Add("@_Run_Rate", SqlDbType.Float)
                                parm.Value = DSwc.Tables(0).Rows(i).Item("Run_Rate_Lbr")

                                cn.Open()
                                .ExecuteNonQuery()
                                .Parameters.Clear()
                                cn.Close()
                            End With
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            lblLogError.Text = (ex.Message)
        Finally
            cn.Close()
        End Try
    End Function

    Function GetAllItem() As Boolean
        Dim DSItem As New DataSet
        With com
            Dim DA As New SqlDataAdapter
            .Connection = cnser
            .CommandType = CommandType.Text
            .CommandText = "select item,description,Stat,Drawing_Nbr,Revision from item_mst"
            DA.SelectCommand = com
            DA.Fill(DSItem, "tbItem")
        End With


        Application.DoEvents()

        'By Yeo 20081110
        Try
            Dim DSAC As New DataSet

            If DSItem.Tables(0).Rows.Count > 0 Then
                With com
                    Dim DA As New SqlDataAdapter
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "select * from tbItem"
                    DA.SelectCommand = com
                    DA.Fill(DSAC, "tbItem")
                End With


                Dim i As Integer
                For i = 0 To DSItem.Tables(0).Rows.Count - 1
                    Dim DRAC() As DataRow
                    Dim stAC As Boolean = False
                    If DSAC.Tables(0).Rows.Count > 0 Then
                        DRAC = DSAC.Tables(0).Select("_itemCode='" & DSItem.Tables(0).Rows(i).Item("item") & "'")
                        If DRAC.Length > 0 Then
                            Dim strdrawing As String = ""
                            Dim strRevision As String = ""
                            If IsDBNull(DSItem.Tables(0).Rows(i).Item("Drawing_Nbr")) = False Then
                                strdrawing = DSItem.Tables(0).Rows(i).Item("Drawing_Nbr")
                            End If

                            If IsDBNull(DSItem.Tables(0).Rows(i).Item("Revision")) = False Then
                                strRevision = DSItem.Tables(0).Rows(i).Item("Revision")
                            End If

                            If IsDBNull(DSItem.Tables(0).Rows(i).Item("description")) = False Then
                                If (DRAC(0).Item("_itemdescription") <> DSItem.Tables(0).Rows(i).Item("description")) Or (DRAC(0).Item("_type") <> DSItem.Tables(0).Rows(i).Item("Stat") Or (DRAC(0).Item("_Drawing_Nbr") <> strdrawing) Or (DRAC(0).Item("_Revision") <> strRevision)) Then
                                    stAC = False
                                Else
                                    stAC = True
                                End If
                            End If
                        End If
                    End If

                    If stAC = False Then
                        With com
                            .Connection = cn
                            .CommandType = CommandType.StoredProcedure
                            .CommandText = "sp_tbItem_Download"

                            parm = .Parameters.Add("@itemCode", SqlDbType.NVarChar, 50)
                            parm.Value = DSItem.Tables(0).Rows(i).Item("item")

                            parm = .Parameters.Add("@itemdescription", SqlDbType.NVarChar, 256)
                            If IsDBNull(DSItem.Tables(0).Rows(i).Item("description")) = False Then
                                parm.Value = DSItem.Tables(0).Rows(i).Item("description")
                            Else
                                parm.Value = ""
                            End If
                            parm = .Parameters.Add("@type", SqlDbType.NVarChar, 5)
                            If IsDBNull(DSItem.Tables(0).Rows(i).Item("Stat")) = False Then
                                parm.Value = DSItem.Tables(0).Rows(i).Item("Stat")
                            Else
                                parm.Value = ""
                            End If
                            parm = .Parameters.Add("@Drawing_Nbr", SqlDbType.NVarChar, 256)
                            If IsDBNull(DSItem.Tables(0).Rows(i).Item("Drawing_Nbr")) = False Then
                                parm.Value = DSItem.Tables(0).Rows(i).Item("Drawing_Nbr")
                            Else
                                parm.Value = ""
                            End If
                            parm = .Parameters.Add("@Revision", SqlDbType.NVarChar, 256)
                            If IsDBNull(DSItem.Tables(0).Rows(i).Item("Revision")) = False Then
                                parm.Value = DSItem.Tables(0).Rows(i).Item("Revision")
                            Else
                                parm.Value = ""
                            End If



                            cn.Open()
                            .ExecuteNonQuery()
                            .Parameters.Clear()
                            cn.Close()
                        End With
                    End If




                Next

            End If
        Catch ex As Exception
            lblLogError.Text = (ex.Message)
        Finally
            cn.Close()
        End Try
    End Function
    Private Sub butDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDownload.Click
        lblLogError.Text = ""
        GetAllDataDownload()
        GetWorkCenter()
        GetAllItem()
        'Dim todw As Boolean = False
        'Dim DSJob As New DataSet
        'Dim DSRoute As New DataSet
        'Dim DSJobAC As New DataSet
        'Dim DSRouteAC As New DataSet
        'Dim DSWS As New DataSet

        'Dim DSwc As New DataSet        'By Yeo 20081110

        'Dim tot As Integer
        'Dim s() As String = Split(txtFrom.Text, "/")
        'Dim SD As Date = DateSerial(s(2), s(1), s(0))
        'Dim s1() As String = Split(txtTo.Text, "/")
        'Dim ED As Date = DateSerial(s1(2), s1(1), s1(0))

        'Try
        '    With com
        '        Dim DA As New SqlDataAdapter
        '        .Connection = cnser
        '        .CommandType = CommandType.Text

        '        .CommandText = "SELECT type,job,item,suffix,job_date, qty_released,qty_complete,qty_scrapped ,stat FROM         job where job_date between @Sd and @ED"
        '        parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
        '        parm.Value = SD
        '        parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
        '        parm.Value = ED

        '        DA.SelectCommand = com
        '        DA.Fill(DSJob, "tbsch")
        '        .Parameters.Clear()
        '    End With
        '    tot = tot + DSJob.Tables(0).Rows.Count
        '    'lblTot.Text = tot
        '    Application.DoEvents()
        '    With com
        '        Dim DA As New SqlDataAdapter
        '        .Connection = cnser
        '        .CommandType = CommandType.Text
        '        'job,suffix,opernum,wc

        '        .CommandText = "select jobroute.job as job,jobroute.suffix as suffix,jobroute.oper_num as opernum,jobroute.wc  as wc from jobroute  inner join job on jobroute.job=job.job and jobroute.suffix=job.suffix where job.job_date between @Sd and @ED"
        '        parm = .Parameters.Add("@Sd", SqlDbType.DateTime)
        '        parm.Value = SD
        '        parm = .Parameters.Add("@Ed", SqlDbType.DateTime)
        '        parm.Value = ED

        '        DA.SelectCommand = com
        '        DA.Fill(DSRoute, "tbsch")
        '        .Parameters.Clear()
        '    End With
        '    tot = tot + DSRoute.Tables(0).Rows.Count
        '    '    lblTot.Text = tot
        '    Application.DoEvents()

        '    '********************************************************************

        'Catch ex As Exception
        '    lblLogError.Text = (ex.Message)
        '    Exit Sub
        'End Try

        'Try
        '    With com
        '        Dim DA As New SqlDataAdapter
        '        .Connection = cn
        '        .CommandType = CommandType.Text

        '        .CommandText = "SELECT * FROM tbjob where _jobDate between @Sd and @ED"
        '        parm = .Parameters.Add("@Sd", SqlDbType.Float)
        '        parm.Value = SD.ToOADate
        '        parm = .Parameters.Add("@Ed", SqlDbType.Float)
        '        parm.Value = ED.ToOADate

        '        DA.SelectCommand = com
        '        DA.Fill(DSJobAC, "tbsch")
        '        .Parameters.Clear()
        '    End With
        'Catch ex As Exception
        '    lblLogError.Text = (ex.Message)
        '    Exit Sub
        'End Try

        'Try
        '    With com
        '        Dim DA As New SqlDataAdapter
        '        .Connection = cn
        '        .CommandType = CommandType.Text

        '        .CommandText = "select tbJobRoute.* from tbJobRoute  inner join tbJob on tbJobRoute._job=tbjob._job and tbJobRoute._jobSuffix=tbjob._jobSuffix where tbjob._jobDate between @Sd  and @ED"

        '        parm = .Parameters.Add("@Sd", SqlDbType.Float)
        '        parm.Value = SD.ToOADate
        '        parm = .Parameters.Add("@Ed", SqlDbType.Float)
        '        parm.Value = ED.ToOADate

        '        DA.SelectCommand = com
        '        DA.Fill(DSRouteAC, "tbsch")
        '        .Parameters.Clear()
        '    End With
        'Catch ex As Exception
        '    lblLogError.Text = (ex.Message)

        '    Exit Sub
        'Finally
        '    cn.Close()
        '    com.Parameters.Clear()
        'End Try

        'Try
        '    If DSJob.Tables(0).Rows.Count > 0 Then

        '        Dim i As Integer
        '        For i = 0 To DSJob.Tables(0).Rows.Count - 1
        '            Dim Stup As Boolean = False
        '            With com
        '                Try
        '                    .Connection = cn
        '                    .CommandType = CommandType.StoredProcedure
        '                    .CommandText = "sp_tbJob_Download_auto"

        '                    ' type,job,item,suffix,job_date, qty_released,qty_complete,qty_scrapped
        '                    parm = .Parameters.Add("@job_" & 1, SqlDbType.VarChar, 50)
        '                    parm.Value = Trim(DSJob.Tables(0).Rows(i).Item("job"))

        '                    parm = .Parameters.Add("@jobsuffix_" & 1, SqlDbType.Int)
        '                    parm.Value = DSJob.Tables(0).Rows(i).Item("suffix")

        '                    parm = .Parameters.Add("@jobDate_" & 1, SqlDbType.Float)
        '                    parm.Value = CDate(DSJob.Tables(0).Rows(i).Item("job_date")).ToOADate

        '                    parm = .Parameters.Add("@jobtype_" & 1, SqlDbType.VarChar, 50)
        '                    parm.Value = DSJob.Tables(0).Rows(i).Item("type")

        '                    parm = .Parameters.Add("@item_" & 1, SqlDbType.VarChar, 50)
        '                    parm.Value = DSJob.Tables(0).Rows(i).Item("item")

        '                    parm = .Parameters.Add("@qtyReleased_" & 1, SqlDbType.Float)
        '                    parm.Value = DSJob.Tables(0).Rows(i).Item("qty_released")

        '                    parm = .Parameters.Add("@qtyCompleted_" & 1, SqlDbType.Float)
        '                    parm.Value = DSJob.Tables(0).Rows(i).Item("qty_complete")

        '                    parm = .Parameters.Add("@qtyScrapped_" & 1, SqlDbType.Float)
        '                    parm.Value = DSJob.Tables(0).Rows(i).Item("qty_scrapped")
        '                    parm = .Parameters.Add("@stas_" & 1, SqlDbType.Char, 1)
        '                    parm.Value = DSJob.Tables(0).Rows(i).Item("stat")
        '                    Dim NR() As DataRow = DSJobAC.Tables(0).Select("_job='" & Trim(DSJob.Tables(0).Rows(i).Item("job")) & "' and _jobsuffix=" & DSJob.Tables(0).Rows(i).Item("suffix"))
        '                    parm = .Parameters.Add("@status_" & 1, SqlDbType.VarChar, 255)
        '                    If NR.Length > 0 Then
        '                        If NR(0).Item("_qtyReleased") = DSJob.Tables(0).Rows(i).Item("qty_released") Then
        '                            parm.Value = "No"
        '                        Else
        '                            parm.Value = "Edit"
        '                        End If
        '                    Else
        '                        parm.Value = "New"
        '                    End If
        '                    cn.Open()
        '                    .ExecuteNonQuery()
        '                    .Parameters.Clear()
        '                    cn.Close()
        '                    Stup = True
        '                    todw = True





        '                Catch ex As Exception
        '                    Stup = False
        '                    Dim errtype As System.Diagnostics.EventLogEntryType
        '                    's = EventLogEntryType.Error
        '                    errtype = EventLogEntryType.Error
        '                    EventLog2.WriteEntry("Download error-" & DSJob.Tables(0).Rows(i).Item("job"), errtype)
        '                Finally
        '                    cn.Close()
        '                    .Parameters.Clear()
        '                End Try

        '            End With
        '            If Stup = True Then
        '                Dim NEWRoute() As DataRow = DSRoute.Tables(0).Select("job='" & Trim(DSJob.Tables(0).Rows(i).Item("job")) & "'")
        '                If NEWRoute.Length > 0 Then
        '                    Dim j As Integer = 0
        '                    For j = 0 To NEWRoute.Length - 1
        '                        With com
        '                            .Connection = cn
        '                            .CommandType = CommandType.StoredProcedure
        '                            .CommandText = "sp_tbjobRoute_Download"
        '                            Dim J1 As Integer
        '                            For J1 = 0 To 14
        '                                If J1 < NEWRoute.Length Then
        '                                    parm = .Parameters.Add("@job_" & J1 + 1, SqlDbType.VarChar, 50)
        '                                    parm.Value = Trim(NEWRoute(j).Item("job"))

        '                                    parm = .Parameters.Add("@jobsuffix_" & J1 + 1, SqlDbType.Int)
        '                                    parm.Value = NEWRoute(j).Item("suffix")

        '                                    parm = .Parameters.Add("@operationNo_" & J1 + 1, SqlDbType.Float)
        '                                    parm.Value = NEWRoute(j).Item("opernum")

        '                                    parm = .Parameters.Add("@wc_" & J1 + 1, SqlDbType.VarChar, 50)
        '                                    parm.Value = NEWRoute(j).Item("wc")
        '                                    Dim NR() As DataRow = DSRouteAC.Tables(0).Select("_job='" & Trim(NEWRoute(j).Item("job")) & "' and _jobsuffix=" & NEWRoute(j).Item("suffix") & " and  _operationNo=" & NEWRoute(j).Item("opernum"))
        '                                    parm = .Parameters.Add("@status_" & J1 + 1, SqlDbType.VarChar, 255)
        '                                    If NR.Length > 0 Then
        '                                        parm.Value = "Edit"
        '                                    Else
        '                                        parm.Value = "New"

        '                                    End If

        '                                Else
        '                                    parm = .Parameters.Add("@job_" & J1 + 1, SqlDbType.VarChar, 50)
        '                                    parm.Value = ""
        '                                    parm = .Parameters.Add("@jobsuffix_" & J1 + 1, SqlDbType.Int)
        '                                    parm.Value = 0
        '                                    parm = .Parameters.Add("@operationNo_" & J1 + 1, SqlDbType.Float)
        '                                    parm.Value = 0
        '                                    parm = .Parameters.Add("@wc_" & J1 + 1, SqlDbType.VarChar, 50)
        '                                    parm.Value = ""
        '                                    parm = .Parameters.Add("@status_" & J1 + 1, SqlDbType.VarChar, 255)
        '                                    parm.Value = ""

        '                                End If
        '                                j = j + 1
        '                            Next
        '                            cn.Open()
        '                            .ExecuteNonQuery()
        '                            .Parameters.Clear()
        '                            cn.Close()
        '                        End With
        '                    Next

        '                End If

        '            End If
        '            Application.DoEvents()
        '        Next
        '    End If
        'Catch ex As Exception

        '    lblLogError.Text = (ex.Message)
        '    Exit Sub
        'Finally
        '    cn.Close()
        '    com.Parameters.Clear()
        'End Try
        'If todw = True Then
        '    lblTime.Text = Format(Now, "yyy-MM-dd HH:mm")
        '    Dim errtype As System.Diagnostics.EventLogEntryType
        '    's = EventLogEntryType.Error
        '    errtype = EventLogEntryType.Information
        '    EventLog2.WriteEntry(passpar & " download(" & LogUserName & ") ", errtype)

        'End If




        If lblLogError.Text = "" Then
            lblLogError.Text = "Data has successfully Download!"
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        dtpFrom.Value = Now
        dtpTo.Value = Now
        txtFrom.Text = Format(Now, "dd/MM/yyyy")
        txtTo.Text = Format(Now, "dd/MM/yyyy")
        PlMain.Visible = True
        Panel1.Visible = False
        plDownload.Visible = False
        txtUserName.Text = ""
        txtPassword.Text = ""
        OpenAction = ""
        MenuStrip1.Enabled = True
        lblLogError.Text = ""

    End Sub

    Private Sub mn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mn1.Click
        MenuStrip1.Enabled = False
        PlMain.Visible = False
        Panel1.Visible = True
        OpenAction = "Quit"
    End Sub


    Function APPCheck(ByVal Action As String) As Boolean
        APPCheck = False
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "Insert into tbAutoUser(_userid,_username,_action)values(@userid,@username,@action)"
                parm = .Parameters.Add("@userid", SqlDbType.VarChar, 50)
                parm.Value = LogUserID
                parm = .Parameters.Add("@username", SqlDbType.VarChar, 50)
                parm.Value = LogName
                parm = .Parameters.Add("@action", SqlDbType.VarChar, 50)
                parm.Value = Action
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                APPCheck = True
            End With
        Catch ex As Exception
            lblLogError.Text = ex.Message
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        Return APPCheck
    End Function

    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtFrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False
    End Sub

    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtTo.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        dtpTo.Checked = False

    End Sub
    Function SendAuoEmail() As Boolean

        '  If SendMailper <> Nothing And Val(SendMailper) <> 0 Then
        Dim clsM As New clsMain
        Dim dsReGEm As New DataSet
        dsReGEm = clsM.GetDataset("select * from tbResourceGroup", "tbRec")
        'Dim lcboolCheck As Boolean = False

        Dim dblSd As Double = CDate(DateSerial(Year(Now), Month(Now), Day(Now))).ToOADate
        Dim dblEd As Double = DateAdd(DateInterval.Minute, 1, DateAdd(DateInterval.Day, 1, CDate(DateSerial(Year(Now), Month(Now), Day(Now))))).ToOADate
        Dim recount As Boolean = False
        Try
            Dim strReportDate As String
            Dim HASRe As New Hashtable
            Dim StItem As Boolean = False
            StItem = True
            HASRe.Add("ALL", "ALL")
            Dim DSEmail As New DataSet
            DSEmail = clsM.GetDataset("select * from tbResourceGroup where _to<>''", "tbRec")

            strReportDate = Format(DateTime.FromOADate(dblSd), "dd MMM yyyy") & "-" & Format(DateTime.FromOADate(dblEd), "dd MMM yyyy")
            Dim CreatedDate As String = Format(Now, "dd MMM yyyy")

            Dim DSrep As New DataSet
            Dim Strsql As String
            Dim strResource As String
            Dim DSRecGroup As New DataSet
            Dim DSItem As New DataSet
            DSItem = clsM.GetDataset("select * from tbItem", "tbItem")
            DSRecGroup = clsM.GetDataset("select A.*,B.* from tbWC A,tbResourceGroup B,tbWorkStation C where C._wc=A._wc and C._rid=B._rid", "tbRec")

            Strsql = "select * from tbJobTransMain where _end_Date between " & dblSd & " and " & dblEd '& " and _nextoper_num=0"

            DSrep = clsM.GetDataset(Strsql, "tbJobTransMain")

            Dim totqty As Double
            ' Dim drRp As DataRow

            Dim ArrexOP() As Integer
            Dim ArrexWC() As String
            Dim ArrexItem() As String
            Dim hashFindUnique As New Hashtable
            Dim ArrTot(0) As Double
            Dim ArrRej(0) As Double
            Dim ArrPer(0) As Double
            Dim ArrCount(0) As Double
            ArrexOP = Nothing
            ArrexItem = Nothing
            ArrexWC = Nothing

            Dim drRp As DataRow
            Dim IA As Integer = 0
            Dim dsrsg As New DSReport
            Dim ArrResou() As String
            Dim re As Integer = 0
            Dim hashTResou As New Hashtable
            If DSrep.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For i = 0 To DSrep.Tables(0).Rows.Count - 1
                    With DSrep.Tables(0).Rows(i)
                        Dim runt As Boolean = False
                        If StItem = True Then
                            runt = True
                        ElseIf HASRe.ContainsKey(.Item("_item")) = True Then
                            runt = True
                        Else
                            runt = False
                        End If
                        Dim DSTrasAll As DataSet
                        Dim dblsumrej As Double = 0
                        Dim dblsumCom As Double = 0
                        Dim dblsumRel As Double = 0
                        Dim dblsumTot As Double = 0
                        dblsumrej = 0
                        DSTrasAll = clsM.GetDataset("select Sum(_qty_scrapped) as stqty from tbJobTransMain where _job='" & .Item("_job") & "'", "tbJobTransMain")
                        If DSTrasAll.Tables(0).Rows.Count > 0 Then
                            If DSTrasAll.Tables(0).Rows(0).Item("stqty") <> 0 Then
                                dblsumrej = DSTrasAll.Tables(0).Rows(0).Item("stqty")
                            End If
                        End If
                        dblsumRel = .Item("_qty_Rele_qty")
                        dblsumCom = .Item("_qty_complete")
                        dblsumTot = dblsumCom + dblsumrej
                        Dim Totst As Boolean = False
                        If dblsumRel = dblsumTot Then
                            Totst = True
                        End If
                        If runt = True And Totst = True Then
                            If .Item("_qty_complete") <> 0 Or .Item("_qty_Rele_qty") <> 0 Then

                                Dim StrItemCode As String = ""
                                Dim StrItemName As String = ""
                                Dim StrRecName As String = ""
                                Dim StrWCCode As String = ""
                                Dim StrWCName As String = ""
                                Dim dblQty As Double
                                Dim StrOP As String = ""
                                StrOP = .Item("_oper_num")


                                StrWCCode = .Item("_WC")
                                Dim DRResou() As DataRow = DSRecGroup.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                If DRResou.Length > 0 Then
                                    StrRecName = DRResou(0).Item("_rGroupID")
                                    StrWCName = DRResou(0).Item("_description")
                                End If
                                Dim DRItem() As DataRow = DSItem.Tables(0).Select("_itemCode='" & .Item("_item") & "'")

                                StrItemCode = .Item("_item")
                                If DRItem.Length > 0 Then
                                    StrItemName = DRItem(0).Item("_itemdescription")
                                End If


                                Dim strID As String = StrItemCode
                                Dim ArrstrID As String = StrItemCode

                                If hashFindUnique.ContainsKey(strID) = False Then
                                    hashFindUnique.Add(strID, IA)
                                    ReDim Preserve ArrTot(IA)
                                    ReDim Preserve ArrRej(IA)
                                    ReDim Preserve ArrPer(IA)
                                    ReDim Preserve ArrCount(IA)

                                    ReDim Preserve ArrexOP(IA)
                                    ReDim Preserve ArrexItem(IA)
                                    ReDim Preserve ArrexWC(IA)
                                    ArrexOP(IA) = Val(StrOP)
                                    ArrexItem(IA) = .Item("_item")
                                    ArrexWC(IA) = StrWCName

                                    ArrTot(IA) = .Item("_qty_complete")
                                    ArrRej(IA) = .Item("_qty_scrapped")

                                    Dim dblCom1 As Double = .Item("_qty_complete") + dblsumrej
                                    ArrPer(IA) = Format(((.Item("_qty_complete") * 100) / dblCom1), "00.00")
                                    ArrCount(IA) = ArrCount(IA) + 1
                                    IA = IA + 1
                                Else
                                    Dim strNV As Integer = hashFindUnique(strID)
                                    ArrTot(strNV) = ArrTot(strNV) + .Item("_qty_complete")
                                    ArrRej(strNV) = ArrRej(strNV) + dblsumrej

                                    Dim dblCom1 As Double = .Item("_qty_complete") + dblsumrej
                                    ArrPer(strNV) = ArrPer(strNV) + Format(((.Item("_qty_complete") * 100) / dblCom1), "00.00")
                                    ArrCount(strNV) = ArrCount(strNV) + 1


                                End If

                                ' lcboolCheck = True




                                drRp = dsrsg.Tables("tbYield").NewRow
                                drRp("sno") = i + 1
                                drRp("Title") = strReportDate 'Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                                drRp("CreateDate") = CreatedDate
                                drRp("ReportDate") = strReportDate
                                Dim StrArr() As String = Split(StrItemName, ",")
                                drRp("SelRSCGroup") = StrRecName
                                drRp("StockCard") = ""
                                drRp("RSCGroup") = StrRecName
                                drRp("FGCode") = StrItemCode
                                drRp("FGDesc") = StrArr(0)
                                drRp("Job") = .Item("_job")
                                drRp("Author") = "NA"
                                drRp("PerOPDate") = "NA"
                                drRp("OP") = StrOP
                                drRp("WCDesc") = StrWCName
                                drRp("ComQty") = .Item("_qty_complete")
                                drRp("RejQty") = dblsumrej
                                drRp("RelQty") = .Item("_qty_Rele_qty")
                                totqty = totqty + .Item("_qty_complete")

                                Dim dblPer As Double = (.Item("_qty_complete") * 100) / .Item("_qty_Rele_qty")
                                If hashTResou.ContainsKey(StrRecName) = False Then
                                    Dim drRG As DataRow() = dsReGEm.Tables(0).Select("_rgroupID='" & StrRecName & "'")
                                    If drRG.Length > 0 Then
                                        If drRG(0).Item("_yieldper") >= dblPer Then
                                            recount = True
                                            ReDim Preserve ArrResou(re)
                                            ArrResou(re) = StrRecName
                                            re = re + 1
                                            hashTResou.Add(StrRecName, StrRecName)
                                        End If
                                    End If
                                End If
                                drRp("Total") = totqty
                                drRp("per") = dblPer
                                dsrsg.Tables("tbYield").Rows.Add(drRp)




                            End If
                        End If
                    End With

                    Application.DoEvents()
                Next
            End If

            If recount = True Then
                Dim today As String = Format(Now, "yyyyMMdd")


                If IO.Directory.Exists(strMailPath & today) = False Then
                    Directory.CreateDirectory(strMailPath & today)
                Else
                    '/  If IO.File.Exists(strMailPath & today) Then
                    Dim s As String
                    For Each s In System.IO.Directory.GetFiles(strMailPath & today)
                        System.IO.File.Delete(s)
                    Next s

                    'End If
                End If
                Dim strEmailSend As Boolean = False


                If ArrResou.Length > 0 Then
                    Dim i As Integer = 0
                    For i = 0 To ArrResou.Length - 1
                        Dim drl() As DataRow = dsrsg.Tables("tbYield").Select("RSCGroup='" & ArrResou(i) & "'")
                        Dim j As Integer
                        Dim oFile As System.IO.File
                        Dim oWrite As System.IO.StreamWriter

                        oWrite = oFile.CreateText(strMailPath & today & "\" & ArrResou(i) & ".csv")
                        Dim drRG As DataRow() = dsReGEm.Tables(0).Select("_rgroupID='" & ArrResou(i) & "'")
                        Dim perN As Double = 0
                        If drRG.Length > 0 Then
                            perN = drRG(0).Item("_yieldper")
                        End If
                        If drl.Length > 0 Then
                            If ArrResou(i) = "SEC-OP" Then
                                ' MsgBox("Hi")
                            End If
                            strEmailSend = True
                            Dim strText As String
                            strText = "Resource Group,FG Code, FG Description,Job No.,Release Qty,Completed Qty,Rejected Qty,Percentage" & vbCrLf
                            For j = 0 To drl.Length - 1
                                If drl(j).Item("per") <= Val(perN) Then
                                    strText = strText & drl(j).Item("RSCGroup") & "," & drl(j).Item("FGCode") & "," & drl(j).Item("FGDesc") & "," & drl(j).Item("Job") & "," & Format(drl(j).Item("RelQty"), "00.00") & "," & Format(drl(j).Item("ComQty"), "00.00") & "," & Format(drl(j).Item("RejQty"), "00.00") & "," & Format(drl(j).Item("per"), "00.00") & vbCrLf
                                End If
                            Next
                            If strText <> "" Then

                                oWrite.WriteLine(strText)

                            End If
                            oWrite.Close()
                        End If
                    Next
                End If
                If strEmailSend = True Then
                    If ArrResou.Length > 0 Then
                        If DSEmail.Tables(0).Rows.Count > 0 Then
                            Dim i As Integer
                            For i = 0 To ArrResou.Length - 1
                                Dim drl() As DataRow = DSEmail.Tables(0).Select("_rGroupID='" & ArrResou(i) & "'")
                                If drl.Length > 0 Then

                                    Try

                                        ' Dim strArgs() As String = Command.Split(",")
                                        Dim blnSMTP As Boolean = False
                                        Dim blnCC As Boolean = False
                                        Dim blnAttachments As Boolean = True
                                        Dim insMail As New MailMessage
                                        With insMail
                                            .From = strFromEmail
                                            .To = Trim(drl(0).Item("_to"))
                                            .Subject = "eWIP-Yield Alert"
                                            .Body = "Yield Alert(" & ArrResou(i) & ")"
                                            If Trim(drl(0).Item("_cc")) <> "" Then
                                                .Cc = Trim(drl(0).Item("_cc"))
                                            End If

                                            If blnAttachments Then
                                                Dim strFile As String
                                                Dim str As String = strMailPath & "/" & today & "/" & ArrResou(i) & ".csv"
                                                Dim strAttach() As String = Split(str, ";")
                                                For Each strFile In strAttach
                                                    .Attachments.Add(New MailAttachment(Trim(strFile)))
                                                Next
                                            End If
                                        End With
                                        If blnSMTP Then SmtpMail.SmtpServer = strMailServer

                                        SmtpMail.Send(insMail)

                                        Console.WriteLine("Successfully sent email message" + vbCrLf)

                                    Catch err As Exception
                                        MsgBox("EXCEPTION " + err.Message + vbCrLf)
                                    End Try







                                End If
                            Next
                        End If
                    End If
                End If
            End If
            'Else

            '    If ArrResou.Length > 0 Then
            '        If DSEmail.Tables(0).Rows.Count > 0 Then
            '            Dim i As Integer
            '            For i = 0 To ArrResou.Length - 1
            '                Dim drl() As DataRow = DSEmail.Tables(0).Select("_rGroupID='" & ArrResou(i) & "'")
            '                If drl.Length > 0 Then

            '                    Try

            '                        ' Dim strArgs() As String = Command.Split(",")
            '                        Dim blnSMTP As Boolean = False
            '                        Dim blnCC As Boolean = False
            '                        Dim blnAttachments As Boolean = True
            '                        Dim insMail As New MailMessage
            '                        With insMail
            '                            .From = strFromEmail
            '                            .To = Trim(drl(0).Item("_to"))
            '                            .Subject = "eWIP-Yield Alert"
            '                            .Body = "Yield Alert(" & ArrResou(i) & ")"
            '                            If Trim(drl(0).Item("_cc")) <> "" Then
            '                                .Cc = Trim(drl(0).Item("_cc"))
            '                            End If
            '                        End With
            '                        If blnSMTP Then SmtpMail.SmtpServer = strMailServer

            '                        SmtpMail.Send(insMail)

            '                        Console.WriteLine("Successfully sent email message" + vbCrLf)

            '                    Catch err As Exception
            '                        MsgBox("EXCEPTION " + err.Message + vbCrLf)
            '                    End Try




            '                End If
            '            Next
            '        End If
            '    End If
            'End If






            'If dsrsg.Tables("tbYield").Rows.Count > 0 Then
            '    Dim i As Integer
            '    For i = 0 To dsrsg.Tables("tbYield").Rows.Count - 1

            '        Dim strFind As String
            '        strFind = dsrsg.Tables("tbYield").Rows(i).Item("FGCode")

            '        Dim strNV As Integer = hashFindUnique(strFind)
            '        Dim IntTot As Double
            '        IntTot = ArrPer(strNV)
            '        Dim IntCnt As Integer
            '        IntCnt = ArrCount(strNV)
            '        'If IntCnt > 1 Then
            '        'MsgBox(IntCnt)
            '        'End If
            '        If IntCnt > 0 Then
            '            dsrsg.Tables("tbYield").Rows(i).Item("totPer") = IntTot / IntCnt
            '        Else
            '            dsrsg.Tables("tbYield").Rows(i).Item("totPer") = IntTot
            '        End If
            '        dsrsg.Tables("tbYield").Rows(i).Item("Total") = totqty
            '    Next
            'End If








        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
        Return recount
    End Function
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim str As Boolean = SendAuoEmail()
        If str = False Then
            MsgBox("No eWIP-Yield Alert data")
        Else
            MsgBox("Done")
        End If
    End Sub
End Class
