Imports System.Data.SqlClient
Public Class frmTransDetails
    Public JobNo As String
    Public Jobdate As String
    Public JobItem As String
    Public JobQty As String
    Public JobOP As Integer
    Public JobWC As String

    Private Sub frmTransDetails_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With lstv
            .Columns.Add("Operator ID", 80, HorizontalAlignment.Left)
            .Columns.Add("Machine ID", 100, HorizontalAlignment.Left)
            .Columns.Add("Suffix", 80, HorizontalAlignment.Left)
            .Columns.Add("No. of Operators", 80, HorizontalAlignment.Left)
            .Columns.Add("Received Qty", 100, HorizontalAlignment.Left)
            .Columns.Add("Completed Qty", 80, HorizontalAlignment.Left)
            .Columns.Add("Rejected Qty", 80, HorizontalAlignment.Left)
            .Columns.Add("Date/Time Started", 150, HorizontalAlignment.Left)
            .Columns.Add("Date/Time End", 150, HorizontalAlignment.Left)
            .Columns.Add("Date/Time Transfer", 150, HorizontalAlignment.Left)
        End With
        Load_ListView()
    End Sub


    Sub Load_ListView()
        lstv.Items.Clear()
        Try
            lbljobno.Text = JobNo
            lblJobdate.Text = Jobdate
            lblItemName.Text = JobItem
            lblreleased.Text = JobQty
            lblOP.Text = JobOP
            lblWC.Text = JobWC

            With com
                .Connection = cn
                .CommandType = CommandType.Text
                'Dim sql As String = "select *   from tbJobTrans where  _job='" & JobNo & "' and _oper_num=" & JobOP & " order by _start_time"
                .CommandText = "select *   from tbJobTrans where  _job='" & JobNo & "' and _oper_num=" & JobOP & " order by _start_time"
                Dim ADNew As New SqlDataAdapter
                ADNew.SelectCommand = com
                Dim DSTans As New DataSet
                ADNew.Fill(DSTans, "tbJobTrans")
                Dim LCQty As Double = 0.0
                Dim LrecQty As Double = 0.0
                Dim LRejQty As Double = 0.0
                If DSTans.Tables(0).Rows.Count > 0 Then
                  

                    Dim i As Integer = 0
                    Dim stopno As Integer = 0
                    For i = 0 To DSTans.Tables(0).Rows.Count - 1
                        With DSTans.Tables(0).Rows(i)
                            'Dim Sd As Double = 0
                            'Dim Ed As Double = 0
                            'Dim StarDT As String = "-"
                            'Dim EndDT As String = "-"
                            'Dim RecQTY As Double = 0.0
                            'Dim ComQTY As Double = 0.0
                            'Dim RejQTY As Double = 0.0

                            Dim ls As New ListViewItem(Trim(.Item("_emp_num")))
                            ls.SubItems.Add(.Item("_machineid"))
                            ls.SubItems.Add(.Item("_jobsuffix"))
                            ls.SubItems.Add(.Item("_no_oper"))
                            ls.SubItems.Add(.Item("_qty_op_qty"))
                            ls.SubItems.Add(.Item("_qty_complete"))
                            ls.SubItems.Add(.Item("_qty_scrapped"))
                            If .Item("_start_Date") > 0 Then
                                Dim sd1 As Double = CDbl((.Item("_start_Date")))
                                ls.SubItems.Add(Format(DateTime.FromOADate(sd1), "dd/MM/yyyy HH:mm"))
                            Else
                                ls.SubItems.Add("-")
                            End If
                            If .Item("_end_Date") > 0 Then
                                Dim ed1 As Double = CDbl((.Item("_end_Date")))
                                ls.SubItems.Add(Format(DateTime.FromOADate(ed1), "dd/MM/yyyy HH:mm"))
                            Else
                                ls.SubItems.Add("-")
                            End If

                            If UCase(.Item("_trans")) = "YES" Then
                                Dim ed1 As Double = CDbl((.Item("_createDate")))
                                ls.SubItems.Add(Format(DateTime.FromOADate(ed1), "dd/MM/yyyy HH:mm"))
                            Else
                                ls.SubItems.Add("-")
                            End If



                            LrecQty = LrecQty + .Item("_qty_op_qty")
                            LCQty = LCQty + .Item("_qty_complete")
                            LRejQty = LRejQty + .Item("_qty_scrapped")
                            If .Item("_qty_op_qty") = .Item("_qty_complete") + .Item("_qty_scrapped") Then
                                lstv.Items.Add(ls).ForeColor = Color.Green
                            ElseIf .Item("_qty_op_qty") > .Item("_qty_complete") + .Item("_qty_scrapped") Then
                                lstv.Items.Add(ls).ForeColor = Color.Orange
                            ElseIf .Item("_qty_op_qty") < .Item("_qty_complete") + .Item("_qty_scrapped") Then
                                lstv.Items.Add(ls).ForeColor = Color.Red
                            Else
                                lstv.Items.Add(ls).ForeColor = Color.Black
                            End If

                        End With


                    Next

                  
                End If

                lblReceived.Text = LrecQty
                lblqtycompleted.Text = LCQty
                lblqtyscrapped.Text = LRejQty

            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
        End Try
    End Sub
End Class