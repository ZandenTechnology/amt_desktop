Imports System.Data.SqlClient
Public Class frmTransDis
    Private Sub frmTransDis_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sd As Date
        sd = DateSerial(Year(Now), Month(Now), 1)
        dtpFrom.Value = sd
        txtfrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        sd = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 1, sd))
        dtpTo.Value = sd
        txtto.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        cmbStatus.SelectedIndex = 0
        With lstv
            .Columns.Add("Job", 100, HorizontalAlignment.Left)
            .Columns.Add("Suff", 50, HorizontalAlignment.Left)
            .Columns.Add("Rel Date", 80, HorizontalAlignment.Left)
            .Columns.Add("Job Type", 100, HorizontalAlignment.Left)
            .Columns.Add("Item", 100, HorizontalAlignment.Left)
            .Columns.Add("Rel Qty", 60, HorizontalAlignment.Left)
            .Columns.Add("Comp Qty", 60, HorizontalAlignment.Left)
            .Columns.Add("Scrp Qty", 60, HorizontalAlignment.Left)
        End With
    End Sub

    Private Sub butSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
        lstv.Items.Clear()
        Try
            Dim strsql As String
            Dim SD As Date
            Dim ED As Date
            If txtfrom.Text <> "" Then
                Dim s() As String = Split(txtfrom.Text, "/")
                SD = DateSerial(s(2), s(1), s(0))
            End If
            If txtto.Text <> "" Then
                Dim s1() As String = Split(txtto.Text, "/")
                ED = DateSerial(s1(2), s1(1), s1(0))
            End If
            If Trim(txtsearch.Text) = "" Then

                strsql = "select * from tbJob WHERE _jobDate between " & SD.ToOADate & " and " & ED.ToOADate & " order by _job,_jobDate"
            Else
                Dim strid() As String = Split(Trim(txtsearch.Text), "-")
                strsql = "select * from tbJob WHERE _job='" & fncstr(Trim(strid(0))) & "'"
            End If
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            da = New SqlDataAdapter(strsql, cn)
            da.Fill(ds)
            da.Dispose()
            strsql = ""
            If Trim(txtsearch.Text) = "" Then
                strsql = "select _job,_oper_num,Sum(_qty_scrapped)as _qty_scrapped,Sum(_qty_complete) as _qty_complete  from tbJobTrans WHERE  _job in(select  _job from tbjob where _jobDate between " & SD.ToOADate & " and " & ED.ToOADate & " )  group by _job,_oper_num"
            Else
                Dim strid() As String = Split(Trim(txtsearch.Text), "-")
                strsql = "select  _job,_oper_num,Sum(_qty_scrapped)as _qty_scrapped,Sum(_qty_complete) as _qty_complete  from tbJobTrans WHERE   _job='" & fncstr(Trim(strid(0))) & "'  group by _job,_oper_num "
            End If
            Dim daProg As New SqlDataAdapter
            Dim dsProg As New DataSet
            daProg = New SqlDataAdapter(strsql, cn)
            daProg.Fill(dsProg)
            dsProg.Dispose()



            strsql = ""
            If txtsearch.Text = "" Then
                strsql = "select _job,max(_operationNo) as _operationNo from tbJobRoute  WHERE  _job in(select  _job from tbjob where _jobDate between " & SD.ToOADate & " and " & ED.ToOADate & " ) group by _job"
            Else
                Dim strid() As String = Split(Trim(txtsearch.Text), "-")
                strsql = "select _job,max(_operationNo) as _operationNo from tbJobRoute  WHERE   _job='" & fncstr(Trim(strid(0))) & "'  group by _job"
            End If
            Dim daRoute As New SqlDataAdapter
            Dim dsRoute As New DataSet
            daRoute = New SqlDataAdapter(strsql, cn)
            daRoute.Fill(dsRoute)
            daRoute.Dispose()






            If ds.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For i = 0 To ds.Tables(0).Rows.Count - 1



                    With ds.Tables(0).Rows(i)

                        Dim MaxOpno As Integer = 0
                        Dim DRRou As DataRow() = dsRoute.Tables(0).Select("_job='" & .Item("_job") & "'") ' get Job Route Data
                        If DRRou.Length > 0 Then
                            MaxOpno = DRRou(0).Item("_operationNo")
                        End If
                        Dim Reqtyst As Boolean = False
                        If .Item("_qtyReleased") <= .Item("_qtyCompleted") + .Item("_qtyScrapped") Then
                            Reqtyst = True
                        End If
                        Dim DRPr As DataRow() = dsProg.Tables(0).Select("_job='" & .Item("_job") & "'")  ' get Job Job Trans
                        Dim ls As New ListViewItem(Trim(.Item("_job")))
                        ls.SubItems.Add(Trim(.Item("_jobsuffix")))
                        ls.SubItems.Add(Format(Date.FromOADate(.Item("_jobDate")), "dd/MM/yyyy"))
                        ls.SubItems.Add(Trim(.Item("_jobtype")))
                        ls.SubItems.Add(Trim(.Item("_item")))
                        ls.SubItems.Add(Trim(.Item("_qtyReleased")))
                        Dim stus As Integer = 0
                        If DRPr.Length > 0 Then
                            Dim j As Integer
                            Dim TotRej As Double = 0
                            Dim TotCOM As Double = 0
                            Dim dists As Boolean = False
                            For j = 0 To DRPr.Length - 1
                                TotRej = TotRej + DRPr(j).Item("_qty_scrapped")
                                dists = True
                                stus = 1
                                If MaxOpno = DRPr(j).Item("_oper_num") Then
                                    TotCOM = TotCOM + DRPr(j).Item("_qty_complete")
                                End If
                            Next

                            If TotCOM = 0 Then
                                ls.SubItems.Add("-")
                            Else
                                ls.SubItems.Add(TotCOM)
                            End If

                            If TotRej = 0 Then
                                ls.SubItems.Add("-")
                            Else
                                ls.SubItems.Add(TotRej)
                            End If

                            If (TotRej + TotCOM) = .Item("_qtyReleased") Or Reqtyst = True Then
                                If cmbStatus.Text = "All" Or cmbStatus.Text = "Completed" Then
                                    lstv.Items.Add(ls).ForeColor = Color.Green
                                End If
                            ElseIf (TotRej + TotCOM) > .Item("_qtyReleased") Then
                                If cmbStatus.Text = "All" Or cmbStatus.Text = "Miscalculation" Then
                                    lstv.Items.Add(ls).ForeColor = Color.Red
                                End If
                            ElseIf stus = 1 Then
                                If cmbStatus.Text = "All" Or cmbStatus.Text = "Progress" Then
                                    lstv.Items.Add(ls).ForeColor = Color.Orange
                                End If
                            Else
                                lstv.Items.Add(ls).ForeColor = Color.Black
                            End If

                            'If (TotRej + TotCOM) > .Item("_qtyReleased") Then

                            'End If

                        Else

                            'If Reqtyst = True Then
                            '    ls.SubItems.Add(.Item("_qtyCompleted"))
                            '    ls.SubItems.Add(.Item("_qtyScrapped"))
                            '    lstv.Items.Add(ls).ForeColor = Color.Green
                            'Else
                            ls.SubItems.Add("-")
                            ls.SubItems.Add("-")
                            If UCase(cmbStatus.Text) = "ALL" Or UCase(cmbStatus.Text) = "NEW" Then
                                lstv.Items.Add(ls).ForeColor = Color.Black
                            End If
                            ' End If

                        End If


                    End With
                Next
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
        End Try
    End Sub
    Private Sub lstv_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.DoubleClick

        Try

    
            'SessionJobno = lstv.SelectedItems(0).Text
            Dim obj As New frmTans
            obj.JobNo = lstv.SelectedItems(0).Text
            obj.Jobdate = lstv.SelectedItems(0).SubItems(2).Text
            obj.JobItem = lstv.SelectedItems(0).SubItems(4).Text
            obj.JobQty = lstv.SelectedItems(0).SubItems(5).Text
            'obj.Width = Me.Width
            'obj.Height = Me.Height
            'obj.Left = Me.Left
            'obj.Top = Me.Top
            obj.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub

    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtfrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False
    End Sub

    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtto.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        dtpTo.Checked = False
    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub
End Class