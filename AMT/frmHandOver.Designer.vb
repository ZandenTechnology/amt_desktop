<[Global].Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
[Partial] Class frmHandOver
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components [IsNot] Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.plHand = New System.Windows.Forms.Panel
        Me.Label35 = New System.Windows.Forms.Label
        Me.txtHandoverName = New System.Windows.Forms.TextBox
        Me.Label33 = New System.Windows.Forms.Label
        Me.Label32 = New System.Windows.Forms.Label
        Me.txtHandoverid = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblSplit = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label31 = New System.Windows.Forms.Label
        Me.lblJob = New System.Windows.Forms.Label
        Me.lblOperation = New System.Windows.Forms.Label
        Me.lblSplitid = New System.Windows.Forms.Label
        Me.lblMachine = New System.Windows.Forms.Label
        Me.lblEMPID = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblMsgBox = New System.Windows.Forms.Label
        Me.txtC = New System.Windows.Forms.TextBox
        Me.plHand.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'plHand
        '
        Me.plHand.BackColor = System.Drawing.SystemColors.Desktop
        Me.plHand.Controls.Add(Me.Label35)
        Me.plHand.Controls.Add(Me.txtHandoverName)
        Me.plHand.Controls.Add(Me.Label33)
        Me.plHand.Controls.Add(Me.Label32)
        Me.plHand.Controls.Add(Me.txtHandoverid)
        Me.plHand.Location = New System.Drawing.Point(-2, 68)
        Me.plHand.Name = "plHand"
        Me.plHand.Size = New System.Drawing.Size(400, 72)
        Me.plHand.TabIndex = 114
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.Color.White
        Me.Label35.Location = New System.Drawing.Point(11, 38)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(92, 13)
        Me.Label35.TabIndex = 73
        Me.Label35.Text = "Operator Name"
        '
        'txtHandoverName
        '
        Me.txtHandoverName.BackColor = System.Drawing.Color.White
        Me.txtHandoverName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHandoverName.Location = New System.Drawing.Point(111, 37)
        Me.txtHandoverName.MaxLength = 50
        Me.txtHandoverName.Name = "txtHandoverName"
        Me.txtHandoverName.ReadOnly = True
        Me.txtHandoverName.Size = New System.Drawing.Size(264, 26)
        Me.txtHandoverName.TabIndex = 75
        Me.txtHandoverName.TabStop = False
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label33.Location = New System.Drawing.Point(216, 12)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(76, 13)
        Me.Label33.TabIndex = 74
        Me.Label33.Text = "(Hand Over)"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label32.Location = New System.Drawing.Point(11, 13)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(73, 13)
        Me.Label32.TabIndex = 72
        Me.Label32.Text = "Operator ID"
        '
        'txtHandoverid
        '
        Me.txtHandoverid.BackColor = System.Drawing.Color.White
        Me.txtHandoverid.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHandoverid.Location = New System.Drawing.Point(111, 5)
        Me.txtHandoverid.MaxLength = 50
        Me.txtHandoverid.Name = "txtHandoverid"
        Me.txtHandoverid.ReadOnly = True
        Me.txtHandoverid.Size = New System.Drawing.Size(99, 26)
        Me.txtHandoverid.TabIndex = 73
        Me.txtHandoverid.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkRed
        Me.Label1.Location = New System.Drawing.Point(10, 151)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 13)
        Me.Label1.TabIndex = 115
        Me.Label1.Text = "CL  -  Clear"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkRed
        Me.Label2.Location = New System.Drawing.Point(10, 173)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(127, 13)
        Me.Label2.TabIndex = 116
        Me.Label2.Text = "SA -   Exit With Save"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkRed
        Me.Label3.Location = New System.Drawing.Point(10, 193)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(147, 13)
        Me.Label3.TabIndex = 117
        Me.Label3.Text = "Ex -    Exit Without Save"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(8, 13)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 13)
        Me.Label5.TabIndex = 119
        Me.Label5.Text = "Job No.:"
        '
        'lblSplit
        '
        Me.lblSplit.AutoSize = True
        Me.lblSplit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSplit.ForeColor = System.Drawing.Color.Black
        Me.lblSplit.Location = New System.Drawing.Point(233, 8)
        Me.lblSplit.Name = "lblSplit"
        Me.lblSplit.Size = New System.Drawing.Size(53, 13)
        Me.lblSplit.TabIndex = 135
        Me.lblSplit.Text = "Split ID:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(8, 36)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(90, 13)
        Me.Label7.TabIndex = 136
        Me.Label7.Text = "Operation No.:"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(233, 36)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(76, 13)
        Me.Label31.TabIndex = 137
        Me.Label31.Text = "Machine ID:"
        '
        'lblJob
        '
        Me.lblJob.AutoSize = True
        Me.lblJob.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.ForeColor = System.Drawing.Color.Black
        Me.lblJob.Location = New System.Drawing.Point(100, 8)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(0, 13)
        Me.lblJob.TabIndex = 138
        '
        'lblOperation
        '
        Me.lblOperation.AutoSize = True
        Me.lblOperation.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperation.ForeColor = System.Drawing.Color.Black
        Me.lblOperation.Location = New System.Drawing.Point(100, 36)
        Me.lblOperation.Name = "lblOperation"
        Me.lblOperation.Size = New System.Drawing.Size(0, 13)
        Me.lblOperation.TabIndex = 139
        '
        'lblSplitid
        '
        Me.lblSplitid.AutoSize = True
        Me.lblSplitid.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSplitid.ForeColor = System.Drawing.Color.Black
        Me.lblSplitid.Location = New System.Drawing.Point(315, 8)
        Me.lblSplitid.Name = "lblSplitid"
        Me.lblSplitid.Size = New System.Drawing.Size(0, 13)
        Me.lblSplitid.TabIndex = 140
        '
        'lblMachine
        '
        Me.lblMachine.AutoSize = True
        Me.lblMachine.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMachine.ForeColor = System.Drawing.Color.Black
        Me.lblMachine.Location = New System.Drawing.Point(315, 36)
        Me.lblMachine.Name = "lblMachine"
        Me.lblMachine.Size = New System.Drawing.Size(0, 13)
        Me.lblMachine.TabIndex = 141
        '
        'lblEMPID
        '
        Me.lblEMPID.AutoSize = True
        Me.lblEMPID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMPID.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblEMPID.Location = New System.Drawing.Point(290, 151)
        Me.lblEMPID.Name = "lblEMPID"
        Me.lblEMPID.Size = New System.Drawing.Size(73, 13)
        Me.lblEMPID.TabIndex = 143
        Me.lblEMPID.Text = "Operator ID"
        Me.lblEMPID.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.PaleGoldenrod
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.lblOperation)
        Me.Panel1.Controls.Add(Me.Label31)
        Me.Panel1.Controls.Add(Me.lblMachine)
        Me.Panel1.Controls.Add(Me.lblJob)
        Me.Panel1.Controls.Add(Me.lblSplitid)
        Me.Panel1.Controls.Add(Me.lblSplit)
        Me.Panel1.Location = New System.Drawing.Point(-2, 1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(400, 77)
        Me.Panel1.TabIndex = 144
        '
        'lblMsgBox
        '
        Me.lblMsgBox.AutoSize = True
        Me.lblMsgBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsgBox.ForeColor = System.Drawing.SystemColors.Desktop
        Me.lblMsgBox.Location = New System.Drawing.Point(3, 220)
        Me.lblMsgBox.Name = "lblMsgBox"
        Me.lblMsgBox.Size = New System.Drawing.Size(0, 13)
        Me.lblMsgBox.TabIndex = 145
        '
        'txtC
        '
        Me.txtC.Location = New System.Drawing.Point(201, 166)
        Me.txtC.Name = "txtC"
        Me.txtC.Size = New System.Drawing.Size(0, 20)
        Me.txtC.TabIndex = 146
        '
        'frmHandOver
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(396, 237)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtC)
        Me.Controls.Add(Me.lblMsgBox)
        Me.Controls.Add(Me.lblEMPID)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.plHand)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmHandOver"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "JOB HANDOVER"
        Me.plHand.ResumeLayout(False)
        Me.plHand.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents plHand As System.Windows.Forms.Panel
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents txtHandoverName As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtHandoverid As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblSplit As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents lblOperation As System.Windows.Forms.Label
    Friend WithEvents lblSplitid As System.Windows.Forms.Label
    Friend WithEvents lblMachine As System.Windows.Forms.Label
    Friend WithEvents lblEMPID As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblMsgBox As System.Windows.Forms.Label
    Friend WithEvents txtC As System.Windows.Forms.TextBox
End Class
