<[Global].Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
[Partial] Class frmTransDetails
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components [IsNot] Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTransDetails))
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.lblWC = New System.Windows.Forms.Label
        Me.lblOP = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.lblItemName = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.lblReceived = New System.Windows.Forms.Label
        Me.lblJobdate = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblqtyscrapped = New System.Windows.Forms.Label
        Me.lblqtycompleted = New System.Windows.Forms.Label
        Me.lblreleased = New System.Windows.Forms.Label
        Me.lbljobno = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.lstv = New System.Windows.Forms.ListView
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.Controls.Add(Me.lblWC)
        Me.Panel2.Controls.Add(Me.lblOP)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.lblItemName)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.lblReceived)
        Me.Panel2.Controls.Add(Me.lblJobdate)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.lblqtyscrapped)
        Me.Panel2.Controls.Add(Me.lblqtycompleted)
        Me.Panel2.Controls.Add(Me.lblreleased)
        Me.Panel2.Controls.Add(Me.lbljobno)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Location = New System.Drawing.Point(6, 18)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(693, 401)
        Me.Panel2.TabIndex = 2
        '
        'lblWC
        '
        Me.lblWC.AutoSize = True
        Me.lblWC.Location = New System.Drawing.Point(109, 111)
        Me.lblWC.Name = "lblWC"
        Me.lblWC.Size = New System.Drawing.Size(39, 13)
        Me.lblWC.TabIndex = 22
        Me.lblWC.Text = "Label2"
        '
        'lblOP
        '
        Me.lblOP.AutoSize = True
        Me.lblOP.Location = New System.Drawing.Point(109, 92)
        Me.lblOP.Name = "lblOP"
        Me.lblOP.Size = New System.Drawing.Size(39, 13)
        Me.lblOP.TabIndex = 21
        Me.lblOP.Text = "Label2"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(15, 110)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(83, 14)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Work Center :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(15, 91)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(80, 14)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Opration No. :"
        '
        'lblItemName
        '
        Me.lblItemName.AutoSize = True
        Me.lblItemName.Location = New System.Drawing.Point(109, 34)
        Me.lblItemName.Name = "lblItemName"
        Me.lblItemName.Size = New System.Drawing.Size(39, 13)
        Me.lblItemName.TabIndex = 18
        Me.lblItemName.Text = "Label2"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(15, 34)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 14)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Item No. :"
        '
        'lblReceived
        '
        Me.lblReceived.AutoSize = True
        Me.lblReceived.Location = New System.Drawing.Point(550, 16)
        Me.lblReceived.Name = "lblReceived"
        Me.lblReceived.Size = New System.Drawing.Size(39, 13)
        Me.lblReceived.TabIndex = 16
        Me.lblReceived.Text = "Label2"
        '
        'lblJobdate
        '
        Me.lblJobdate.AutoSize = True
        Me.lblJobdate.Location = New System.Drawing.Point(109, 73)
        Me.lblJobdate.Name = "lblJobdate"
        Me.lblJobdate.Size = New System.Drawing.Size(39, 13)
        Me.lblJobdate.TabIndex = 15
        Me.lblJobdate.Text = "Label2"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(450, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 14)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "Received :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(15, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 14)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Job Date :"
        '
        'lblqtyscrapped
        '
        Me.lblqtyscrapped.AutoSize = True
        Me.lblqtyscrapped.Location = New System.Drawing.Point(550, 59)
        Me.lblqtyscrapped.Name = "lblqtyscrapped"
        Me.lblqtyscrapped.Size = New System.Drawing.Size(39, 13)
        Me.lblqtyscrapped.TabIndex = 12
        Me.lblqtyscrapped.Text = "Label2"
        '
        'lblqtycompleted
        '
        Me.lblqtycompleted.AutoSize = True
        Me.lblqtycompleted.Location = New System.Drawing.Point(550, 38)
        Me.lblqtycompleted.Name = "lblqtycompleted"
        Me.lblqtycompleted.Size = New System.Drawing.Size(39, 13)
        Me.lblqtycompleted.TabIndex = 11
        Me.lblqtycompleted.Text = "Label2"
        '
        'lblreleased
        '
        Me.lblreleased.AutoSize = True
        Me.lblreleased.Location = New System.Drawing.Point(109, 52)
        Me.lblreleased.Name = "lblreleased"
        Me.lblreleased.Size = New System.Drawing.Size(39, 13)
        Me.lblreleased.TabIndex = 10
        Me.lblreleased.Text = "Label2"
        '
        'lbljobno
        '
        Me.lbljobno.AutoSize = True
        Me.lbljobno.Location = New System.Drawing.Point(109, 15)
        Me.lbljobno.Name = "lbljobno"
        Me.lbljobno.Size = New System.Drawing.Size(39, 13)
        Me.lbljobno.TabIndex = 9
        Me.lbljobno.Text = "Label2"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(450, 58)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 14)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Rejected :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(450, 37)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(74, 14)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Completed :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(15, 53)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 14)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Qty Released :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(15, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Job No :"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.LightGray
        Me.Panel3.Controls.Add(Me.lstv)
        Me.Panel3.Location = New System.Drawing.Point(12, 140)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(670, 254)
        Me.Panel3.TabIndex = 0
        '
        'lstv
        '
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(12, 16)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(650, 224)
        Me.lstv.TabIndex = 0
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'frmTransDetails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(705, 431)
        Me.Controls.Add(Me.Panel2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTransDetails"
        Me.Text = "Transaction Details"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblItemName As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblReceived As System.Windows.Forms.Label
    Friend WithEvents lblJobdate As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblqtyscrapped As System.Windows.Forms.Label
    Friend WithEvents lblqtycompleted As System.Windows.Forms.Label
    Friend WithEvents lblreleased As System.Windows.Forms.Label
    Friend WithEvents lbljobno As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lstv As System.Windows.Forms.ListView
    Friend WithEvents lblWC As System.Windows.Forms.Label
    Friend WithEvents lblOP As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
