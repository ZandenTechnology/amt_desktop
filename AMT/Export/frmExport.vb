Imports System.Data.SqlClient
Imports System.Runtime.InteropServices
'Imports Excel
Imports Microsoft.VisualBasic.DateAndTime
Public Class frmExport
    Dim parm As SqlParameter
    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint
        cmbStatus.SelectedIndex = 0
        Dim sd As Date
        sd = DateSerial(Year(Now), Month(Now), Day(Now))
        dtpFrom.Value = sd
        txtfrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        'sd = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 1, sd))
        dtpTo.Value = sd
        txtto.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        With lstv
            .Columns.Add("Trans ID", 0, HorizontalAlignment.Left)
            .Columns.Add("Job", 100, HorizontalAlignment.Left)
            .Columns.Add("Suffix", 50, HorizontalAlignment.Left)
            .Columns.Add("Item", 100, HorizontalAlignment.Left)
            .Columns.Add("Operation No.", 50, HorizontalAlignment.Left)
            .Columns.Add("Work Center", 50, HorizontalAlignment.Left)
            .Columns.Add("Operator ID", 50, HorizontalAlignment.Left)
            .Columns.Add("Trans Date", 80, HorizontalAlignment.Left)
            .Columns.Add("Quantity Received", 60, HorizontalAlignment.Left)
            .Columns.Add("Quantity Completed", 60, HorizontalAlignment.Left)
            .Columns.Add("Quantity Scrapped", 60, HorizontalAlignment.Left)
        End With
        With lstvLoad
            .Columns.Add("Trans ID", 0, HorizontalAlignment.Left)
            .Columns.Add("Job", 100, HorizontalAlignment.Left)
            .Columns.Add("Suffix", 50, HorizontalAlignment.Left)
            .Columns.Add("Item", 100, HorizontalAlignment.Left)
            .Columns.Add("Operation No.", 50, HorizontalAlignment.Left)
            .Columns.Add("Work Center", 50, HorizontalAlignment.Left)
            .Columns.Add("Operator ID", 50, HorizontalAlignment.Left)
            .Columns.Add("Trans Date", 80, HorizontalAlignment.Left)
            .Columns.Add("Quantity Received", 60, HorizontalAlignment.Left)
            .Columns.Add("Quantity Completed", 60, HorizontalAlignment.Left)
            .Columns.Add("Quantity Scrapped", 60, HorizontalAlignment.Left)
        End With

    End Sub
    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtfrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False
    End Sub

    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtto.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        dtpTo.Checked = False
    End Sub
    Private Sub cmbStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbStatus.SelectedIndexChanged
        lstv.Items.Clear()
        If cmbStatus.SelectedIndex = 0 Then
            PlExport.Visible = False
            PlWait.Visible = True
        Else
            PlExport.Visible = True
            PlWait.Visible = False
        End If
    End Sub
    Private Sub butBrows_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBrows.Click
        txtFile.Text = ""
        sfdSave.FileName = ""
        sfdSave.Filter = "Excel File|*.xls"
        sfdSave.Title = "Save an Excel File"


        sfdSave.CheckPathExists = True
        sfdSave.CreatePrompt = True
        sfdSave.FilterIndex = 1
        sfdSave.OverwritePrompt = True
        sfdSave.ValidateNames = True
        sfdSave.AddExtension = True
      
    
        If sfdSave.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtFile.Text = sfdSave.FileName
        End If
    End Sub

    Private Sub butSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
        lstv.Visible = False
        lstvLoad.Visible = True
        pic1.Visible = True
        Application.DoEvents()
        lblTotal.Text = 0
        lblRunning.Text = 0
        P1.Visible = False
        lstv.Items.Clear()
        Try
            Dim strsql As String
            Dim SD As Date
            Dim ED As Date
            If txtfrom.Text <> "" Then
                Dim s() As String = Split(txtfrom.Text, "/")
                SD = DateSerial(s(2), s(1), s(0))
            End If
            If txtto.Text <> "" Then
                Dim s1() As String = Split(txtto.Text, "/")
                ED = DateSerial(s1(2), s1(1), s1(0))
            End If

            Dim contsql As String
            If cmbStatus.Text = "Wait For Export" Then
                contsql = " and _tansnum not in(select _tansnum from tbExport where _tansDate between " & SD.ToOADate & " and " & ED.ToOADate & ") "
            Else
                contsql = " and _tansnum  in(select _tansnum from tbExport where _tansDate between " & SD.ToOADate & " and " & ED.ToOADate & ") "
            End If


            Dim contsqldir As String
            If cmbStatus.Text = "Wait For Export" Then
                contsqldir = " and _tansnum not in(select _tansnum from tbExport where _job='" & fncstr(Trim(txtsearch.Text)) & "')"
            Else
                contsqldir = " and _tansnum  in(select _tansnum from tbExport where _job='" & fncstr(Trim(txtsearch.Text)) & "')"
            End If



            If txtsearch.Text = "" Then
                strsql = "select * from tbJobTrans WHERE _trans_dateto between " & SD.ToOADate & " and " & ED.ToOADate & contsql & " order by _job,_trans_dateto,_oper_num"
            Else
                strsql = "select * from tbJobTrans WHERE _job='" & fncstr(Trim(txtsearch.Text)) & "' " & contsqldir & " order by _job,_oper_num,_trans_dateto,_oper_num"
            End If
            Dim dsTrans As New DataSet
            Dim da As New SqlDataAdapter
            da = New SqlDataAdapter(strsql, cn)
            da.Fill(dsTrans)
            da.Dispose()







            Dim DSERP As New DataSet
            Dim daERP As New SqlDataAdapter
            Dim SQLERP As String
            If ckberp.Checked = True Then
                If txtsearch.Text = "" Then
                    With com
                        SQLERP = "select * from Job where job_Date between @SD and @ED"
                        .Connection = cnser
                        .CommandType = CommandType.Text
                        .CommandText = SQLERP
                        parm = .Parameters.Add("@sd", SqlDbType.SmallDateTime)
                        parm.Value = SD
                        parm = .Parameters.Add("@Ed", SqlDbType.SmallDateTime)
                        parm.Value = ED
                        daERP.SelectCommand = com
                        daERP.Fill(DSERP, "tbERP")
                        .Parameters.Clear()
                    End With
                Else
                    With com
                        SQLERP = "select * from Job where job=@job"
                        .Connection = cnser
                        .CommandType = CommandType.Text
                        .CommandText = SQLERP
                        parm = .Parameters.Add("@Job", SqlDbType.VarChar, 50)
                        parm.Value = Trim(txtsearch.Text)
                        daERP.SelectCommand = com
                        daERP.Fill(DSERP, "tbERP")
                        .Parameters.Clear()
                    End With
                End If
            End If





           
            strsql = ""
            If txtsearch.Text = "" Then
                strsql = "select _job,_oper_num,Sum(_qty_scrapped)as _qty_scrapped,Sum(_qty_complete) as _qty_complete  from tbJobTrans WHERE  _job in(select  distinct _job from tbjob where _trans_dateto between " & SD.ToOADate & " and " & ED.ToOADate & contsql & "  )  group by _job,_oper_num"
            Else
                strsql = "select  _job,_oper_num,Sum(_qty_scrapped)as _qty_scrapped,Sum(_qty_complete) as _qty_complete  from tbJobTrans WHERE   _job='" & fncstr(Trim(txtsearch.Text)) & " '" ' & contsqldir & "  group by _job,_oper_num "
            End If
            Dim daProg As New SqlDataAdapter
            Dim dsProg As New DataSet
            daProg = New SqlDataAdapter(strsql, cn)
            daProg.Fill(dsProg)
            dsProg.Dispose()



            strsql = ""
            If txtsearch.Text = "" Then
                strsql = "select _job,_operationNo as _operationNo  from tbJobRoute  WHERE  _job in(select  distinct _job from tbjobTrans where _trans_dateto between " & SD.ToOADate & " and " & ED.ToOADate & contsql & " ) order by  _job"
            Else
                strsql = "select _job,_operationNo as _operationNo from tbJobRoute  WHERE   distinct _job='" & fncstr(Trim(txtsearch.Text)) & "'" ' " & contsqldir & "  group by _job"
            End If
            Dim daRoute As New SqlDataAdapter
            Dim dsRoute As New DataSet
            daRoute = New SqlDataAdapter(strsql, cn)
            daRoute.Fill(dsRoute)
            daRoute.Dispose()






            If dsTrans.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For i = 0 To dsTrans.Tables(0).Rows.Count - 1
                    With dsTrans.Tables(0).Rows(i)
                        Dim NRT() As DataRow = dsRoute.Tables(0).Select("max(_operationNo)<" & .Item("_oper_num") & " and _job='" & Trim(.Item("_job")) & "'")
                        Dim ls As New ListViewItem(Trim(.Item("_tansnum")), 0)
                        ls.SubItems.Add(Trim(.Item("_job")))
                        ls.SubItems.Add(Trim(.Item("_jobsuffix")))
                        ls.SubItems.Add(Trim(.Item("_item")))
                        ls.SubItems.Add(Trim(.Item("_oper_num")))
                        ls.SubItems.Add(Trim(.Item("_wc")))
                        ls.SubItems.Add(Trim(.Item("_emp_num")))
                        ls.SubItems.Add(Format(Date.FromOADate(.Item("_trans_dateto")), "dd/MM/yyyy"))
                        ls.SubItems.Add(Trim(.Item("_qty_op_qty")))
                        ls.SubItems.Add(Trim(.Item("_qty_complete")))
                        ls.SubItems.Add(Trim(.Item("_qty_scrapped")))
                        lstv.Items.Add(ls).ForeColor = Color.Green
                        Application.DoEvents()
                    End With
                Next
            End If







            'Dim MaxOpno As Integer = 0
            'Dim DRRou As DataRow() = dsRoute.Tables(0).Select("_job='" & .Item("_job") & "'") ' get Job Route Data
            'If DRRou.Length > 0 Then
            '    MaxOpno = DRRou(0).Item("_operationNo")
            'End If
            'Dim Reqtyst As Boolean = False
            'If .Item("_qtyReleased") <= .Item("_qtyCompleted") + .Item("_qtyScrapped") Then
            '    Reqtyst = True
            'End If
            'Dim DRPr As DataRow() = dsProg.Tables(0).Select("_job='" & .Item("_job") & "'")  ' get Job Job Trans

            'Dim stus As Integer = 0
            'If DRPr.Length > 0 Then
            '    Dim j As Integer
            '    Dim TotRej As Double = 0
            '    Dim TotCOM As Double = 0
            '    Dim dists As Boolean = False
            '    For j = 0 To DRPr.Length - 1
            '        TotRej = TotRej + DRPr(j).Item("_qty_scrapped")
            '        dists = True
            '        stus = 1
            '        If MaxOpno = DRPr(j).Item("_oper_num") Then
            '            TotCOM = TotCOM + DRPr(j).Item("_qty_complete")
            '        End If
            '    Next


            '    If (TotRej + TotCOM) = .Item("_qtyReleased") Then
            '        Dim etpst As Boolean = False
            '        If ckberp.Checked = True Then
            '            Dim NRERP() As DataRow = DSERP.Tables(0).Select("job='" & .Item("_job") & "'")

            '            If NRERP.Length > 0 Then
            '                If Val(NRERP(0).Item("qty_complete") + NRERP(0).Item("qty_scrapped")) <> 0 Then
            '                    Dim ls As New ListViewItem(Trim(.Item("_job")), 0)
            '                    ls.SubItems.Add(Trim(.Item("_jobsuffix")))
            '                    ls.SubItems.Add(Format(Date.FromOADate(.Item("_jobDate")), "dd/MM/yyyy"))
            '                    ls.SubItems.Add(Trim(.Item("_jobtype")))
            '                    ls.SubItems.Add(Trim(.Item("_item")))
            '                    ls.SubItems.Add(Trim(.Item("_qtyReleased")))

            '                    If TotCOM = 0 Then
            '                        ls.SubItems.Add("-")
            '                    Else
            '                        ls.SubItems.Add(TotCOM)
            '                    End If

            '                    If TotRej = 0 Then
            '                        ls.SubItems.Add("-")
            '                    Else
            '                        ls.SubItems.Add(TotRej)
            '                    End If
            '                    lstv.Items.Add(ls).ForeColor = Color.Purple
            '                    etpst = True
            '                End If
            '            End If
            '            If etpst = False Then
            '                Dim ls As New ListViewItem(Trim(.Item("_job")))
            '                ls.SubItems.Add(Trim(.Item("_jobsuffix")))
            '                ls.SubItems.Add(Format(Date.FromOADate(.Item("_jobDate")), "dd/MM/yyyy"))
            '                ls.SubItems.Add(Trim(.Item("_jobtype")))
            '                ls.SubItems.Add(Trim(.Item("_item")))
            '                ls.SubItems.Add(Trim(.Item("_qtyReleased")))

            '                If TotCOM = 0 Then
            '                    ls.SubItems.Add("-")
            '                Else
            '                    ls.SubItems.Add(TotCOM)
            '                End If

            '                If TotRej = 0 Then
            '                    ls.SubItems.Add("-")
            '                Else
            '                    ls.SubItems.Add(TotRej)
            '                End If
            '                lstv.Items.Add(ls).ForeColor = Color.Green
            '                etpst = False
            '            End If




            '        Else
            '            Dim ls As New ListViewItem(Trim(.Item("_job")))
            '            ls.SubItems.Add(Trim(.Item("_jobsuffix")))
            '            ls.SubItems.Add(Format(Date.FromOADate(.Item("_jobDate")), "dd/MM/yyyy"))
            '            ls.SubItems.Add(Trim(.Item("_jobtype")))
            '            ls.SubItems.Add(Trim(.Item("_item")))
            '            ls.SubItems.Add(Trim(.Item("_qtyReleased")))

            '            If TotCOM = 0 Then
            '                ls.SubItems.Add("-")
            '            Else
            '                ls.SubItems.Add(TotCOM)
            '            End If

            '            If TotRej = 0 Then
            '                ls.SubItems.Add("-")
            '            Else
            '                ls.SubItems.Add(TotRej)
            '            End If
            '            lstv.Items.Add(ls).ForeColor = Color.Green
            '        End If


            '    End If
            'End If

            '        End With
            '    Next
            'End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
           
        Finally
            lstvLoad.Visible = False
            lstv.Visible = True
            pic1.Visible = False
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

    End Sub
End Class