Imports System.Data.SqlClient
Public Class frmWCRCGroup
    Dim dr As SqlDataReader
    Dim parm As SqlParameter

    Private Sub butAssSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssSingle.Click
        Dim i As Integer
        Try
            lstWC.Sorted = True
            For i = 0 To lstWC.SelectedItems.Count - 1
                lstGroup.Items.Add(lstWC.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstWC.SelectedItems.Count - 1
                lstWC.Items.Remove(lstWC.SelectedItems.Item(0))
            Next
            lstWC.Refresh()
            lstGroup.Refresh()
        Catch exce As Exception
            MsgBox(exce.ToString)
        End Try
    End Sub

    Private Sub butAssMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssMulti.Click
        Dim i As Integer
        Try
            lstWC.Sorted = True
            For i = 0 To lstWC.Items.Count - 1
                lstGroup.Items.Add(lstWC.Items(i))
            Next
            i = 0
            For i = 0 To lstWC.Items.Count - 1
                lstWC.Items.Remove(lstWC.Items(0))
            Next
            lstWC.Refresh()
            lstGroup.Refresh()
        Catch exce As Exception
            MsgBox(exce.ToString)
        End Try
    End Sub

    Private Sub butDelSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelSingle.Click
        Dim i As Integer
        Try
            lstWC.Sorted = True
            For i = 0 To lstGroup.SelectedItems.Count - 1
                Dim S() As String = Split(txtID.Text, "||")
                If S.Length > 0 Then
                    Dim j As Integer
                    Dim id As DataItem = lstGroup.SelectedItem
                    For j = 0 To S.Length - 1
                        If S(j) = refTxt(Split(id.ID, "||")(0)) Then
                            If CheckFaultgrp(S(j)) = True Then
                                MsgBox("This resource-WC group with rejected table")
                                Exit Sub
                            End If
                        End If
                    Next
                End If

                lstWC.Items.Add(lstGroup.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstGroup.SelectedItems.Count - 1
                lstGroup.Items.Remove(lstGroup.SelectedItems.Item(0))
            Next
            lstWC.Refresh()
            lstGroup.Refresh()
        Catch exce As Exception
            MsgBox(exce.ToString)
        End Try
    End Sub
    Private Sub butDelMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelMulti.Click
        Dim i As Integer
        Try
            lstWC.Sorted = True
            For i = 0 To lstGroup.Items.Count - 1
                lstWC.Items.Add(lstGroup.Items(i))
            Next
            i = 0
            For i = 0 To lstGroup.Items.Count - 1
                lstGroup.Items.Remove(lstGroup.Items(0))
            Next
            lstWC.Refresh()
            lstGroup.Refresh()
        Catch exce As Exception
            MsgBox(exce.ToString)
        End Try
    End Sub
    Function CheckFaultgrp(ByVal idv As String) As Boolean
        CheckFaultgrp = False
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select * from tbFailure where _sIDno in (select _sIDno from tbWorkStation where _wc=@WC and _rid=@rid)"
                parm = .Parameters.Add("@wc", SqlDbType.VarChar, 50)
                parm.Value = refTxt(idv)
                parm = .Parameters.Add("@rid", SqlDbType.Int)
                parm.Value = Val(txtReID.Text)
                cn.Open()
                dr = .ExecuteReader(CommandBehavior.CloseConnection)
                If dr.Read Then
                    CheckFaultgrp = True
                Else
                    CheckFaultgrp = False
                End If

                cn.Close()
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            com.Parameters.Clear()
            cn.Close()
        End Try
    End Function
    Private Sub frmWCRCGroup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadRG()
        CallWC()
    End Sub
    Sub LoadRG()
        lstwrk.Items.Clear()
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                'Dim DI As DataItem
                .CommandText = "select * from tbResourceGroup order by _rGroupID"
                cn.Open()
                dr = .ExecuteReader(CommandBehavior.CloseConnection)
                While dr.Read
                    lstwrk.Items.Add(New DataItem(dr.Item("_rGroupID") & " || " & dr.Item("_rGroupName") & " || " & dr.Item("_rid"), dr.Item("_rGroupID") & " - " & dr.Item("_rGroupName")))
                    ' lstwrk.Items.Add(dr.Item("_wc") & " - " & dr.Item("_description"))
                End While
                cn.Close()
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cn.Close()
        End Try

    End Sub
    Sub CallWC()
        lstWC.Items.Clear()
        lstGroup.Items.Clear()
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                'Dim DI As DataItem
                .CommandText = "select * from tbWC order by _description"
                cn.Open()
                dr = .ExecuteReader(CommandBehavior.CloseConnection)
                While dr.Read
                    lstWC.Items.Add(New DataItem(dr.Item("_wc") & "||" & dr.Item("_description"), dr.Item("_wc") & "  -  " & dr.Item("_description")))
                    '/lstwrk.Items.Add(dr.Item("_wc") & " - " & dr.Item("_description"))
                End While
                cn.Close()
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cn.Close()
        End Try


    End Sub

    Private Sub lstwrk_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstwrk.SelectedIndexChanged
        txtRGID.Text = ""
        txtDesc.Text = ""
        txtReID.Text = ""
        lstWC.Items.Clear()
        lstGroup.Items.Clear()
        txtID.Text = ""
        Try
            If lstwrk.SelectedIndex >= 0 Then
                Dim id As DataItem = lstwrk.SelectedItem
                Dim S() As String = Split(id.ID, "||")
                txtRGID.Text = S(0)
                txtDesc.Text = S(1)
                txtReID.Text = S(2)

                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    'Dim DI As DataItem
                    .CommandText = "select * from tbWC where _wc  not in(select _wc  from tbWorkStation)"
                    cn.Open()
                    dr = .ExecuteReader(CommandBehavior.CloseConnection)
                    While dr.Read
                        lstWC.Items.Add(New DataItem(dr.Item("_wc") & "||" & dr.Item("_description"), dr.Item("_wc") & "  -  " & dr.Item("_description")))
                        '/lstwrk.Items.Add(dr.Item("_wc") & " - " & dr.Item("_description"))
                    End While
                    cn.Close()
                End With
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cn.Close()
        End Try

        Try
            If lstwrk.SelectedIndex >= 0 Then
                Dim id As DataItem = lstwrk.SelectedItem
                Dim S() As String = Split(id.ID, "||")
                txtRGID.Text = S(0)
                txtDesc.Text = S(1)

                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    'Dim DI As DataItem
                    .CommandText = "select * from tbWC where _wc  in(select _wc  from tbWorkStation where _rid in(select _rid  from tbResourceGroup where _rGroupID='" & txtRGID.Text & "'))"
                    '.CommandText = "select * from tbMachine where _mid  in(select _mid from tbMachineGroup where _wc='" & refTxt(txtRGID.Text) & "') order by _machineID "
                    cn.Open()
                    dr = .ExecuteReader(CommandBehavior.CloseConnection)
                    While dr.Read
                        lstGroup.Items.Add(New DataItem(dr.Item("_wc") & "||" & dr.Item("_description"), dr.Item("_wc") & "  -  " & dr.Item("_description")))
                        If txtID.Text = "" Then
                            txtID.Text = dr.Item("_wc")
                        Else
                            txtID.Text = txtID.Text & "||" & dr.Item("_wc")
                        End If
                    End While
                    cn.Close()
                End With
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cn.Close()
        End Try


    End Sub
    Function refTxt(ByVal txt As String) As String
        Return Trim(Replace(txt, "'", "''"))
    End Function

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        txtRGID.Text = ""
        txtReID.Text = ""
        txtDesc.Text = ""
        txtID.Text = ""
        LoadRG()
        CallWC()
    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(txtRGID.Text) = "" Then
            MsgBox("select the resource group id!", MsgBoxStyle.Information)
            Exit Sub
        End If
        ''Try
        ''    With com
        ''        .Connection = cn
        ''        .CommandType = CommandType.Text
        ''        .CommandText = "delete from  tbMachinegroup  where _wc ='" & refTxt(txtw.Text) & "'"
        ''        cn.Open()
        ''        .ExecuteNonQuery()
        ''        cn.Close()
        ''        com.Parameters.Clear()
        ''        'MsgBox("Successfully updated!", MsgBoxStyle.Information)
        ''    End With
        ''Catch ex As SqlException
        ''    MsgBox(ex.Message, MsgBoxStyle.Critical)
        ''Catch ex As Exception
        ''    MsgBox(ex.Message, MsgBoxStyle.Critical)
        ''Finally
        ''    cn.Close()
        ''    com.Parameters.Clear()
        ''End Try
        Dim i As Integer
        Dim st As Integer = 1
        Dim S() As String = Split(txtID.Text, "||")


        If lstGroup.Items.Count > 0 Then
            For i = 0 To lstGroup.Items.Count - 1
                Dim stsave As Boolean = False
                Dim id As DataItem = lstGroup.Items(i)
                If S.Length > 0 Then
                    Dim j As Integer
                    For j = 0 To S.Length - 1
                        If S(j) = refTxt(Split(id.ID, "||")(0)) Then
                            S(j) = ""
                            stsave = True
                            Exit For
                        End If
                    Next
                End If
                If stsave = False Then
                    Try
                        With com
                            .Connection = cn
                            .CommandType = CommandType.Text
                            .CommandText = "Insert into tbWorkStation( _wc,_rid )values(@wc,@rid)"
                            parm = .Parameters.Add("@wc", SqlDbType.VarChar, 50)
                            parm.Value = refTxt(Split(id.ID, "||")(0))
                            parm = .Parameters.Add("@rid", SqlDbType.Int)
                            ' Dim id As DataItem = lstGroup.Items(i)
                            parm.Value = Val(txtReID.Text)
                            cn.Open()
                            .ExecuteNonQuery()
                            cn.Close()
                            com.Parameters.Clear()
                            If st = 0 Then
                                st = 1
                            End If
                        End With
                    Catch ex As SqlException
                        MsgBox(ex.Message, MsgBoxStyle.Critical)
                        st = 2
                    Catch ex As Exception
                        MsgBox(ex.Message, MsgBoxStyle.Critical)
                        st = 2
                    Finally
                        cn.Close()
                        com.Parameters.Clear()
                    End Try
                End If
            Next
        End If
        Dim delid As String = ""
        If S.Length > 0 Then
            Dim j As Integer
            For j = 0 To S.Length - 1
                If Trim(S(j)) <> "" Then
                    If Trim(delid) = "" Then
                        delid = "'" & Trim(S(j)) & "'"
                    Else
                        delid = delid & ",'" & Trim(S(j)) & "'"
                    End If
                End If

            Next
        End If

        If Trim(delid) <> "" Then
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "delete from tbWorkStation where _wc in(" & delid & ") and _rid=@rid"
                    parm = .Parameters.Add("@rid", SqlDbType.Int)
                    parm.Value = Val(txtReID.Text)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                End With
            Catch ex As Exception
            Finally
                cn.Close()
                com.Parameters.Clear()
            End Try
        End If



        If st = 1 Then
            MsgBox("Successfully updated!", MsgBoxStyle.Information)
        End If

        txtRGID.Text = ""
        txtDesc.Text = ""
        txtReID.Text = ""
        txtID.Text = ""
        LoadRG()
        CallWC()
    End Sub
End Class