Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings
Imports System.Collections.Specialized.NameValueCollection
Module ModMain
    Public Declare Function SetParent Lib "user32" Alias "SetParent" (ByVal hWndChild As Integer, ByVal hWndNewParent As Integer) As Integer
    Dim selectForm As String
    Dim config As System.Collections.Specialized.NameValueCollection = AppSettings()
    Public SqlString As String = config.Item("SQLConnString")
    Public APPPath As String = config.Item("Path")
    Public cn As New SqlClient.SqlConnection(SqlString)
    Dim Sqlser As String = config.Item("SQLconServer")
    Public cnser As New SqlClient.SqlConnection(Sqlser)
    Public com As New SqlClient.SqlCommand
    Public LogUserID As String
    Public LogUserName As String
    Public LogName As String
    Public LogPrivilege As String
    Public AccessRights As String = config.Item("AccessRights")
    Public SessionJobno As String
    Public ChHandOverID As String
    Public ChHandOverName As String
    Public excmd As Integer = 0
    Public PrinterName As String = config.Item("PrinterName")
    Public PrintMSG As String = config.Item("PrinterMSG")
    Public Newhash As New Hashtable
    Public TmVari As Integer
    Public strSaveStock As String = ""
    'Public selectForm As String
    Public Function fncstr(ByVal st As String) As String
        Dim stv As String
        Try
            stv = Trim(Replace(st, "'", "''"))
        Catch ex As Exception
            stv = ""
        End Try
        Return stv
    End Function

End Module
