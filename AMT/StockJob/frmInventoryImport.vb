Imports System.Data.SqlClient
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic.DateAndTime

Public Class frmInventoryImport

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        lstv.Items.Clear()
        txtFile.Text = ""
        butSave.Enabled = False
    End Sub

    Private Sub ButBrows_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButBrows.Click

        Try
            If (ofdBox.ShowDialog = 1) Then
                txtFile.Text = ofdBox.FileName
            Else
                txtFile.Text = ""
                Exit Sub
            End If


            Dim localAll As Process()

            localAll = Process.GetProcesses()
            Dim lstOldProcessID As New ListBox
            Dim EXhash As New Hashtable
            For i As Integer = 0 To localAll.Length - 1
                If localAll(i).ProcessName.ToUpper = "EXCEL" Then
                    If EXhash.ContainsKey(localAll(i).Id) = False Then
                        EXhash.Add(localAll(i).Id, localAll(i).Id)
                    End If
                End If
            Next



            'Dim xlApp As New Microsoft.Office.Interop.Excel.Application

            'xlApp.Visible = False



            'Dim xlWB As Microsoft.Office.Interop.Excel.Workbook = xlApp.Workbooks.OpenXML(txtFile.Text)

            'If xlWB Is Nothing = False Then
            '    For Each xlWsh As Microsoft.Office.Interop.Excel.Worksheet In xlWB.Sheets
            '        Dim eObj As Microsoft.Office.Interop.Excel.Range
            '        xlWsh.Application.Visible = False
            '        Dim intRows, intCols As Integer
            '        Dim i, J As Integer
            '        Dim bolst As Boolean = False
            '        Dim xlRng As Microsoft.Office.Interop.Excel.Range
            '        For i = 1 To 2000
            '            xlRng = xlWsh.Cells(i, 1)
            '            If UCase(xlRng.Value) = "TXN TYPE" And bolst = False Then
            '                bolst = True
            '                i = i + 1
            '            End If
            '            If bolst Then
            '                xlRng = xlWsh.Cells(i, 1)
            '                If xlRng.Value = Nothing Then
            '                    Exit For
            '                End If
            '                Dim strTxnType As String
            '                Dim strTransDate As Date
            '                Dim strDocRef As String
            '                Dim strCustomer As String
            '                Dim strpn As String
            '                Dim strDescription As String
            '                Dim strqty As String
            '                Dim strRemarks As String
            '                xlRng = xlWsh.Cells(i, 1)
            '                If xlRng.Value <> "" Then
            '                    strTxnType = xlRng.Value
            '                Else
            '                    strTxnType = ""
            '                End If
            '                xlRng = xlWsh.Cells(i, 2)
            '                If IsDBNull(xlRng.Value) = False Then
            '                    strTransDate = CDate(xlRng.Value)
            '                Else
            '                    strTransDate = ""
            '                End If
            '                xlRng = xlWsh.Cells(i, 3)
            '                If IsDBNull(xlRng.Value) = False Then
            '                    strDocRef = xlRng.Value
            '                Else
            '                    strDocRef = ""
            '                End If
            '                xlRng = xlWsh.Cells(i, 4)
            '                If IsDBNull(xlRng.Value) = False Then
            '                    strCustomer = xlRng.Value
            '                Else
            '                    strCustomer = ""
            '                End If
            '                xlRng = xlWsh.Cells(i, 5)
            '                If IsDBNull(xlRng.Value) = False Then
            '                    strpn = xlRng.Value
            '                Else
            '                    strpn = ""
            '                End If
            '                xlRng = xlWsh.Cells(i, 6)
            '                If IsDBNull(xlRng.Value) = False Then
            '                    strDescription = xlRng.Value
            '                Else
            '                    strDescription = ""
            '                End If
            '                xlRng = xlWsh.Cells(i, 7)
            '                If IsDBNull(xlRng.Value) = False Then
            '                    strqty = xlRng.Value
            '                Else
            '                    strqty = ""
            '                End If
            '                xlRng = xlWsh.Cells(i, 8)
            '                If IsDBNull(xlRng.Value) = False Then
            '                    strRemarks = xlRng.Value
            '                Else
            '                    strRemarks = ""
            '                End If
            '                Dim ls As New ListViewItem(Trim(strTxnType))   ' you can also use reader.GetSqlValue(0) 
            '                ls.SubItems.Add(Trim(strTransDate))
            '                ls.SubItems.Add(Trim(strDocRef))
            '                ls.SubItems.Add(Trim(strCustomer))
            '                ls.SubItems.Add(Trim(strpn))
            '                ls.SubItems.Add(Trim(strDescription))
            '                ls.SubItems.Add(Trim(strqty))
            '                ls.SubItems.Add(Trim(strRemarks))
            '                lstv.Items.Add(ls) '.BackColor = Color.Blue
            '            End If


            '        Next
            '        Exit For
            '    Next
            'End If









            Dim DellocalAll As Process()

            DellocalAll = Process.GetProcesses()


            For i As Integer = 0 To DellocalAll.Length - 1
                If DellocalAll(i).ProcessName.ToUpper = "EXCEL" Then
                    If EXhash.ContainsKey(DellocalAll(i).Id) = False Then
                        DellocalAll(i).Kill()
                    End If
                End If
            Next

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub frmInventoryImport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        With lstv
            .Columns.Add("Txn Type", 100, HorizontalAlignment.Left)
            .Columns.Add("Transaction Date", 150, HorizontalAlignment.Left)
            .Columns.Add("Doc Ref", 100, HorizontalAlignment.Left)
            .Columns.Add("Customer", 200, HorizontalAlignment.Left)
            .Columns.Add("P/N", 100, HorizontalAlignment.Left)
            .Columns.Add("Part Description", 150, HorizontalAlignment.Left)
            .Columns.Add("Quantity", 100, HorizontalAlignment.Left)
            .Columns.Add("Remarks", lstv.Width - 900, HorizontalAlignment.Left)
        End With
    End Sub
End Class