Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.DateAndTime
Public Class frmStockList
    Dim clsM As New clsMain
    Private Sub butSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
        Dim Sd, ed As Double
        lstv.Items.Clear()
        If Trim(txtJobNo.Text) = "" Then
            If Trim(txtfrom.Text) = "" Then
                MsgBox("Enter the form date")
                Exit Sub
            End If
            If Trim(txtto.Text) = "" Then
                MsgBox("Enter the to date")
                Exit Sub
            End If

            Sd = clsM.CheckDate(txtfrom.Text)
            ed = clsM.CheckDate(txtto.Text)
            If Sd = 0 Then
                MsgBox("Enter the form date")
                Exit Sub
            End If
            If ed = 0 Then
                MsgBox("Enter the form date")
                Exit Sub
            End If
        End If
        ed = ed + 1
        Dim stsql As String
        If Trim(txtJobNo.Text) = "" Then
            stsql = "select *  from tb_st_Inventory where _stock_Date between " & Sd & " and " & ed
        Else
            stsql = "select *  from tb_st_Inventory where _job='" & Trim(txtJobNo.Text.Replace("'", "''")) & "'"
        End If

        Try
            Dim dsJob As New DataSet
            dsJob = clsM.GetDataset(stsql, "tbJOB")
            Dim i As Integer
            If dsJob.Tables(0).Rows.Count > 0 Then
                For i = 0 To dsJob.Tables(0).Rows.Count - 1
                    With dsJob.Tables(0).Rows(i)
                        Dim ls As New ListViewItem(Trim(.Item("_tansnum")))    ' you can also use reader.GetSqlValue(0) 
                        ls.SubItems.Add(Trim(.Item("_job")))
                        ls.SubItems.Add(Trim(.Item("_jobsuffix")))
                        ls.SubItems.Add(Trim(.Item("_item")))
                        ls.SubItems.Add(Trim(DateTime.FromOADate(.Item("_stock_Date"))))
                        ls.SubItems.Add(Trim(.Item("_received_Qty")))
                        ls.SubItems.Add(Trim(.Item("_stock_Qty")))
                        ls.SubItems.Add(Trim(.Item("_delivery_qty")))
                        lstv.Items.Add(ls)
                    End With
                Next
            End If
        Catch ex As Exception

        End Try



    End Sub

    Private Sub frmStockList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim SDate As Date = DateAdd(DateInterval.Year, -1, DateSerial(Year(Now), Month(Now), Day(Now)))
        dtpFrom.Value = SDate
        dtpTo.Value = DateSerial(Year(Now), Month(Now), Day(Now))
        txtfrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        txtto.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        With lstv
            .Columns.Add("tansnum", 0, HorizontalAlignment.Left)
            .Columns.Add("Job", 100, HorizontalAlignment.Left)
            .Columns.Add("Suffix", 100, HorizontalAlignment.Left)
            .Columns.Add("Item", lstv.Width - 450, HorizontalAlignment.Left)
            .Columns.Add("Stock Date", 100, HorizontalAlignment.Left)
            .Columns.Add("Received", 50, HorizontalAlignment.Left)
            .Columns.Add("Stock", 50, HorizontalAlignment.Left)
            .Columns.Add("Deliverd", 50, HorizontalAlignment.Left)
        End With
    End Sub

    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub
End Class