Imports Microsoft.VisualBasic.DateAndTime
Public Class frmWIPList
    Dim clsM As New clsMain
    Dim i As Integer

    Private Sub frmWIPList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim SDate As Date = DateAdd(DateInterval.Year, -1, DateSerial(Year(Now), Month(Now), Day(Now)))
        dtpFrom.Value = SDate
        dtpTo.Value = DateSerial(Year(Now), Month(Now), Day(Now))
        txtfrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        txtto.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        With lstv
            .Columns.Add("tansnum", 0, HorizontalAlignment.Left)
            .Columns.Add("Job", 150, HorizontalAlignment.Left)
            .Columns.Add("Suffix", 100, HorizontalAlignment.Left)
            .Columns.Add("Item", lstv.Width - 550, HorizontalAlignment.Left)
            .Columns.Add("Quantity", 150, HorizontalAlignment.Left)
            .Columns.Add("Completed Date", 150, HorizontalAlignment.Left)
        End With
    End Sub

    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtfrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")

    End Sub

    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtto.Text = Format(dtpTo.Value, "dd/MM/yyyy")
    End Sub

    Private Sub butSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
        Dim Sd, ed As Double
        lstv.Items.Clear()
        If Trim(txtJobNo.Text) = "" Then
            If Trim(txtfrom.Text) = "" Then
                MsgBox("Enter the form date")
                Exit Sub
            End If
            If Trim(txtto.Text) = "" Then
                MsgBox("Enter the to date")
                Exit Sub
            End If

            Sd = clsM.CheckDate(txtfrom.Text)
            ed = clsM.CheckDate(txtto.Text)
            If Sd = 0 Then
                MsgBox("Enter the form date")
                Exit Sub
            End If
            If ed = 0 Then
                MsgBox("Enter the form date")
                Exit Sub
            End If
        End If
        ed = ed + 1

        Dim stsql As String
        If Trim(txtJobNo.Text) = "" Then

            stsql = "select A.* from tbvwJobTrans A where _job  not in (select _job from tb_st_Inventory where _Job=A._Job) and A._jobsuffix Not in(select _jobsuffix from tb_st_Inventory where _jobsuffix=A._jobsuffix and _Job=A._Job) and _end_Date between " & Sd & " and " & ed
        Else
            Dim strJob As String() = Split(txtJobNo.Text, "-")
            stsql = "select A.* from tbvwJobTrans A where _job  not in (select _job from tb_st_Inventory where _Job=A._Job) and A._jobsuffix Not in(select _jobsuffix from tb_st_Inventory where _jobsuffix=A._jobsuffix and _Job=A._Job) and A.Job='" & strJob(0) & "'"
        End If

        Try
            Dim dsJob As New DataSet
            dsJob = clsM.GetDataset(stsql, "tbJOB")
            Dim i As Integer
            If dsJob.Tables(0).Rows.Count > 0 Then
                For i = 0 To dsJob.Tables(0).Rows.Count - 1
                    With dsJob.Tables(0).Rows(i)
                        Dim ls As New ListViewItem(Trim(.Item("_tansnum")))    ' you can also use reader.GetSqlValue(0) 
                        ls.SubItems.Add(Trim(.Item("_job")))
                        ls.SubItems.Add(Trim(.Item("_jobsuffix")))
                        ls.SubItems.Add(Trim(.Item("_item")))
                        ls.SubItems.Add(Trim(.Item("_qty_complete")))
                        ls.SubItems.Add(Trim(DateTime.FromOADate(.Item("_end_Date"))))
                        lstv.Items.Add(ls)
                    End With
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click
        
        Try
            Dim obj As New frmStockEntry
            obj.TransNo = lstv.SelectedItems(0).Text
            obj.txtJob.Text = lstv.SelectedItems(0).SubItems(1).Text
            obj.txtSuffix.Text = lstv.SelectedItems(0).SubItems(2).Text
            obj.txtItem.Text = lstv.SelectedItems(0).SubItems(3).Text
            obj.txtCom.Text = lstv.SelectedItems(0).SubItems(4).Text
            obj.txtEndDate.Text = lstv.SelectedItems(0).SubItems(5).Text
            strSaveStock = ""
            obj.ShowDialog()
            If strSaveStock = "SAVE" Then
                butSearch_Click(sender, e)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub





    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub
End Class