Imports System.Data.SqlClient

Public Class frmViewJob
    Dim dr As SqlDataReader
    Dim parm As SqlParameter


    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub frmViewJob_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sd As Date
        sd = DateSerial(Year(Now), Month(Now), 1)
        dtpFrom.Value = sd
        txtfrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        sd = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 1, sd))
        dtpTo.Value = sd
        txtto.Text = Format(dtpTo.Value, "dd/MM/yyyy")

        With lstv
            .Columns.Add("Job", 100, HorizontalAlignment.Left)
            .Columns.Add("Suffix", 50, HorizontalAlignment.Left)
            .Columns.Add("Date", 80, HorizontalAlignment.Left)
            .Columns.Add("Job Type", 100, HorizontalAlignment.Left)
            .Columns.Add("Item", 100, HorizontalAlignment.Left)
            .Columns.Add("Quantity Released", 120, HorizontalAlignment.Left)
            .Columns.Add("Quantity Completed", 120, HorizontalAlignment.Left)
            .Columns.Add("Quantity Scrapped", 120, HorizontalAlignment.Left)
            .Columns.Add("Status", 100, HorizontalAlignment.Left)

        End With

    End Sub

    Private Sub Panel3_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel3.Paint

    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
        Load_ListView("")
    End Sub

    Sub Load_ListView(ByVal strScrh As String)
        lstv.Items.Clear()

        Dim s() As String = Split(txtfrom.Text, "/")
        Dim SD As Date = DateSerial(s(2), s(1), s(0))
        Dim s1() As String = Split(txtto.Text, "/")
        Dim ED As Date = DateSerial(s1(2), s1(1), s1(0))
        Dim dt As Date
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                If Trim(txtsearch.Text) = "" Then
                    .CommandText = "select * from tbjob where _jobdate between @SD and @ED order by _jobdate"
                    parm = .Parameters.Add("@SD", SqlDbType.Float)
                    parm.Value = SD.ToOADate
                    parm = .Parameters.Add("@ED", SqlDbType.Float)
                    parm.Value = ED.ToOADate
                Else
                    .CommandText = "select * from tbjob  where  _job like @scrch order by _jobdate"
                    'parm = .Parameters.Add("@SD", SqlDbType.Float)
                    'parm.Value = SD.ToOADate
                    'parm = .Parameters.Add("@ED", SqlDbType.Float)
                    'parm.Value = ED.ToOADate
                    parm = .Parameters.Add("@scrch", SqlDbType.VarChar)
                    parm.Value = txtsearch.Text & "%"
                End If

                cn.Open()
                dr = com.ExecuteReader()
                If dr.HasRows = True Then

                    While dr.Read
                        Dim ls As New ListViewItem(Trim(dr.Item("_job")))
                        ls.SubItems.Add(Trim(dr.Item("_jobsuffix")))
                        dt = Date.FromOADate(Trim(dr.Item("_jobdate")))
                        ls.SubItems.Add(Format(dt, "dd/MM/yyyy"))
                        ls.SubItems.Add(Trim(dr.Item("_jobtype")))
                        ls.SubItems.Add(Trim(dr.Item("_item")))
                        ls.SubItems.Add(Trim(dr.Item("_qtyreleased")))
                        ls.SubItems.Add(Trim(dr.Item("_qtycompleted")))
                        ls.SubItems.Add(Trim(dr.Item("_qtyscrapped")))
                        ls.SubItems.Add(Trim(dr.Item("_status")))
                        lstv.Items.Add(ls)
                    End While
                Else
                    MsgBox("No Record Found")
                End If
                dr.Close()
            End With

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
    End Sub

    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtfrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False
    End Sub

    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtto.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        dtpTo.Checked = False
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs)

    End Sub

    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click
        Dim objViewJobDetails As New frmViewJobDetails()
        objViewJobDetails.strjob = Trim(lstv.SelectedItems(0).Text)
        objViewJobDetails.strsuffix = Trim(lstv.SelectedItems(0).SubItems(1).Text)
        objViewJobDetails.strdate = Trim(lstv.SelectedItems(0).SubItems(2).Text)
        objViewJobDetails.stritem = Trim(lstv.SelectedItems(0).SubItems(4).Text)
        objViewJobDetails.strqtyreleased = Trim(lstv.SelectedItems(0).SubItems(5).Text)
        objViewJobDetails.strqtycompleted = Trim(lstv.SelectedItems(0).SubItems(6).Text)
        objViewJobDetails.strqtyscrapped = Trim(lstv.SelectedItems(0).SubItems(7).Text)
        objViewJobDetails.ShowDialog()

    End Sub

    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub

    Private Sub txtsearch_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtsearch.KeyPress
        
    End Sub

    Private Sub txtsearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtsearch.TextChanged
        '   Load_ListView(Trim(txtsearch.Text))
    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub
End Class