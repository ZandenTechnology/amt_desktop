Imports System.Data.SqlClient
Public Class frmWorkStation
    Inherits System.Windows.Forms.Form
    Dim parm As SqlParameter
    Friend WithEvents cmbwkid As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtWCDesc As System.Windows.Forms.TextBox
    Friend WithEvents txtRGid As System.Windows.Forms.TextBox
    Friend WithEvents butEXIT As System.Windows.Forms.Button
    Dim dr As SqlDataReader
    Dim strFoCus As Integer
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butDelete As System.Windows.Forms.Button
    Friend WithEvents butUpdate As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lstv As System.Windows.Forms.ListView
    Friend WithEvents cmbRG As System.Windows.Forms.ComboBox
    Friend WithEvents txtRGDesc As System.Windows.Forms.TextBox
    Friend WithEvents txtWSIDNo As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWorkStation))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label3 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.txtRGid = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtWCDesc = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.cmbwkid = New System.Windows.Forms.ComboBox
        Me.txtRGDesc = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cmbRG = New System.Windows.Forms.ComboBox
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.butEXIT = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.butDelete = New System.Windows.Forms.Button
        Me.butUpdate = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.txtWSIDNo = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.lstv = New System.Windows.Forms.ListView
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(1, 2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(736, 687)
        Me.Panel1.TabIndex = 3
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Location = New System.Drawing.Point(8, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(720, 672)
        Me.Panel2.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(572, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(136, 15)
        Me.Label3.TabIndex = 35
        Me.Label3.Text = "WORK CENTER GROUP"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.txtRGid)
        Me.Panel3.Controls.Add(Me.Label6)
        Me.Panel3.Controls.Add(Me.txtWCDesc)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.cmbwkid)
        Me.Panel3.Controls.Add(Me.txtRGDesc)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.cmbRG)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Controls.Add(Me.txtWSIDNo)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Location = New System.Drawing.Point(8, 32)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(704, 632)
        Me.Panel3.TabIndex = 0
        '
        'txtRGid
        '
        Me.txtRGid.BackColor = System.Drawing.Color.White
        Me.txtRGid.Location = New System.Drawing.Point(431, 98)
        Me.txtRGid.MaxLength = 256
        Me.txtRGid.Name = "txtRGid"
        Me.txtRGid.ReadOnly = True
        Me.txtRGid.Size = New System.Drawing.Size(74, 20)
        Me.txtRGid.TabIndex = 53
        Me.txtRGid.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(339, 61)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(36, 15)
        Me.Label6.TabIndex = 52
        Me.Label6.Text = "Desc"
        '
        'txtWCDesc
        '
        Me.txtWCDesc.BackColor = System.Drawing.Color.White
        Me.txtWCDesc.Location = New System.Drawing.Point(381, 58)
        Me.txtWCDesc.MaxLength = 256
        Me.txtWCDesc.Name = "txtWCDesc"
        Me.txtWCDesc.ReadOnly = True
        Me.txtWCDesc.Size = New System.Drawing.Size(315, 20)
        Me.txtWCDesc.TabIndex = 51
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(339, 22)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(36, 15)
        Me.Label5.TabIndex = 50
        Me.Label5.Text = "Desc"
        '
        'cmbwkid
        '
        Me.cmbwkid.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbwkid.Location = New System.Drawing.Point(184, 58)
        Me.cmbwkid.Name = "cmbwkid"
        Me.cmbwkid.Size = New System.Drawing.Size(149, 21)
        Me.cmbwkid.TabIndex = 49
        '
        'txtRGDesc
        '
        Me.txtRGDesc.BackColor = System.Drawing.Color.White
        Me.txtRGDesc.Location = New System.Drawing.Point(381, 19)
        Me.txtRGDesc.MaxLength = 256
        Me.txtRGDesc.Name = "txtRGDesc"
        Me.txtRGDesc.ReadOnly = True
        Me.txtRGDesc.Size = New System.Drawing.Size(315, 20)
        Me.txtRGDesc.TabIndex = 48
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 61)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 15)
        Me.Label4.TabIndex = 47
        Me.Label4.Text = "WC ID"
        '
        'cmbRG
        '
        Me.cmbRG.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbRG.Location = New System.Drawing.Point(184, 19)
        Me.cmbRG.Name = "cmbRG"
        Me.cmbRG.Size = New System.Drawing.Size(149, 21)
        Me.cmbRG.TabIndex = 46
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Panel4.Controls.Add(Me.butEXIT)
        Me.Panel4.Controls.Add(Me.butCancel)
        Me.Panel4.Controls.Add(Me.butDelete)
        Me.Panel4.Controls.Add(Me.butUpdate)
        Me.Panel4.Controls.Add(Me.butSave)
        Me.Panel4.Location = New System.Drawing.Point(184, 93)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(291, 72)
        Me.Panel4.TabIndex = 39
        '
        'butEXIT
        '
        Me.butEXIT.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butEXIT.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butEXIT.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butEXIT.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butEXIT.Image = CType(resources.GetObject("butEXIT.Image"), System.Drawing.Image)
        Me.butEXIT.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butEXIT.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butEXIT.Location = New System.Drawing.Point(230, 5)
        Me.butEXIT.Name = "butEXIT"
        Me.butEXIT.Size = New System.Drawing.Size(48, 59)
        Me.butEXIT.TabIndex = 8
        Me.butEXIT.Text = "Exit"
        Me.butEXIT.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.butEXIT.UseVisualStyleBackColor = False
        '
        'butCancel
        '
        Me.butCancel.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butCancel.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCancel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butCancel.Image = CType(resources.GetObject("butCancel.Image"), System.Drawing.Image)
        Me.butCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(176, 5)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(48, 59)
        Me.butCancel.TabIndex = 3
        Me.butCancel.Text = "Cancel"
        Me.butCancel.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butCancel.UseVisualStyleBackColor = False
        '
        'butDelete
        '
        Me.butDelete.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butDelete.Enabled = False
        Me.butDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butDelete.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelete.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butDelete.Image = CType(resources.GetObject("butDelete.Image"), System.Drawing.Image)
        Me.butDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butDelete.Location = New System.Drawing.Point(120, 5)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(48, 59)
        Me.butDelete.TabIndex = 2
        Me.butDelete.Text = "Delete"
        Me.butDelete.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butDelete.UseVisualStyleBackColor = False
        '
        'butUpdate
        '
        Me.butUpdate.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butUpdate.Enabled = False
        Me.butUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butUpdate.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butUpdate.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butUpdate.Image = CType(resources.GetObject("butUpdate.Image"), System.Drawing.Image)
        Me.butUpdate.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butUpdate.Location = New System.Drawing.Point(64, 5)
        Me.butUpdate.Name = "butUpdate"
        Me.butUpdate.Size = New System.Drawing.Size(48, 59)
        Me.butUpdate.TabIndex = 1
        Me.butUpdate.Text = "Update"
        Me.butUpdate.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butUpdate.UseVisualStyleBackColor = False
        '
        'butSave
        '
        Me.butSave.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butSave.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSave.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butSave.Image = CType(resources.GetObject("butSave.Image"), System.Drawing.Image)
        Me.butSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(8, 5)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(48, 59)
        Me.butSave.TabIndex = 0
        Me.butSave.Text = "Save"
        Me.butSave.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butSave.UseVisualStyleBackColor = False
        '
        'txtWSIDNo
        '
        Me.txtWSIDNo.Location = New System.Drawing.Point(606, 90)
        Me.txtWSIDNo.MaxLength = 30
        Me.txtWSIDNo.Name = "txtWSIDNo"
        Me.txtWSIDNo.Size = New System.Drawing.Size(28, 20)
        Me.txtWSIDNo.TabIndex = 38
        Me.txtWSIDNo.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(113, 15)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "Resource Group ID"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.LightGray
        Me.Panel5.Controls.Add(Me.lstv)
        Me.Panel5.Location = New System.Drawing.Point(8, 174)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(688, 443)
        Me.Panel5.TabIndex = 1
        '
        'lstv
        '
        Me.lstv.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(8, 11)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(672, 418)
        Me.lstv.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lstv.TabIndex = 7
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'frmWorkStation
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(238, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(738, 690)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmWorkStation"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Sub frmWorkStation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With lstv
            .Columns.Add("SIDno", 0, HorizontalAlignment.Left)
            .Columns.Add("WC ID", 230, HorizontalAlignment.Left)
            .Columns.Add("WC Description", lstv.Width - 230, HorizontalAlignment.Left)
            GetData()
            Getwc()
        End With
    End Sub
    Sub list_Displaydata()
        lstv.Items.Clear()
        If Trim(cmbRG.Text) <> "-Select-" And Trim(cmbRG.Text) <> "" Then
            Try
                With com
                    com.Connection = cn
                    com.CommandType = CommandType.Text
                    Dim sqlstr As String = "select * from tbWorkStation A,tbWc B,tbResourceGroup C where A._wc=B._Wc and A._rid=C._rid and A._rid=" & Val(txtRGid.Text)
                    com.CommandText = "select * from tbWorkStation A,tbWc B,tbResourceGroup C where A._wc=B._Wc and A._rid=C._rid and A._rid=" & Val(txtRGid.Text)
                    'parm = .Parameters.Add("@rGroupid", SqlDbType.VarChar, 50)
                    'Dim DI As DataItem
                    'DI = cmbRG.Items(cmbRG.SelectedIndex)
                    'parm.Value = DI.ID
                    cn.Open()
                    dr = com.ExecuteReader(CommandBehavior.CloseConnection)
                    Dim rcount As Integer = 0
                    Dim i As Integer = -1
                    While dr.Read
                        Dim ls As New ListViewItem(Trim(dr.Item("_sIDno")))    ' you can also use reader.GetSqlValue(0) 
                        ls.SubItems.Add(Trim(dr.Item("_wc")))
                        ls.SubItems.Add(Trim(dr.Item("_description")))
                        lstv.Items.Add(ls)
                        If strFoCus = dr.Item("_sIDno") Then
                            rcount = i
                        End If
                    End While
                    If i <> -1 Then
                        lstv.Focus()
                        lstv.Items(rcount).Selected = True
                    End If
                    strFoCus = 0
                End With
                
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")

            Finally
                cn.Close()
                com.Parameters.Clear()
            End Try
        End If
    End Sub

    Sub GetData()
        cmbRG.Items.Clear()
        cmbRG.Items.Add(New DataItem("-Select-", "-Select-"))
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                'Dim DI As DataItem
                .CommandText = "select _rGroupid,_rGroupName,_rid from tbResourceGroup"
                cn.Open()
                dr = .ExecuteReader(CommandBehavior.CloseConnection)
                While dr.Read
                    cmbRG.Items.Add(New DataItem(dr.Item("_rGroupName") & "||" & dr.Item("_rid"), dr.Item("_rGroupid")))
                End While
                cn.Close()
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
        cmbRG.Text = "-Select-"
    End Sub

    Private Sub cmbRG_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRG.SelectedIndexChanged
        If cmbRG.Text <> "-Select-" Then
            Dim DI As DataItem
            DI = cmbRG.Items(cmbRG.SelectedIndex)
            txtRGDesc.Text = Split(DI.ID, "||")(0)
            txtRGid.Text = Split(DI.ID, "||")(1)
        Else
            txtRGid.Text = ""
            txtRGDesc.Text = ""
        End If

        list_Displaydata()

    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(cmbRG.Text) = "-Select-" Or Trim(cmbRG.Text) = "" Then
            MsgBox("Select Resource Group ID!", MsgBoxStyle.Information, "eWIP")
        End If
        'If Trim(txtStationID.Text) = "" Then
        '    MsgBox("Enter the station id!", MsgBoxStyle.Information, "eWIP")
        '    Exit Sub
        'End If
        If Trim(txtWCDesc.Text) = "" Then
            MsgBox("Select WC ID!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "insert into tbworkstation(_wc,_rid)values(@wc,@rid)"
                parm = .Parameters.Add("@wc", SqlDbType.VarChar, 50)
                Dim DI As DataItem
                DI = cmbwkid.Items(cmbwkid.SelectedIndex)
                parm.Value = DI.Value
                parm = .Parameters.Add("@rid", SqlDbType.Int)
                parm.Value = Val(txtRGid.Text)

                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully saved!", MsgBoxStyle.Information, "eWIP")
            End With
        Catch ex As SqlException
            If ex.Number = 2627 Then
                MsgBox("This WC Id already in group!", MsgBoxStyle.Information, "eWIP")
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub


    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click

        txtWSIDNo.Text = lstv.SelectedItems(0).Text
        ' txtStationID.Text = lstv.SelectedItems(0).SubItems(1).Text
        cmbwkid.Text = lstv.SelectedItems(0).SubItems(1).Text
        butSave.Enabled = False
        butUpdate.Enabled = True
        butDelete.Enabled = True
        ' txtUserCode.ReadOnly = True
        'StbUser.Text = "You Can Edit And Delete This Records"
    End Sub
    Sub textReset()
        txtWSIDNo.Clear()
        'txtStationID.Clear()
        ' txtRGid.Clear()
        ' txtRGDesc.Clear()
        txtWCDesc.Clear()
        'txtRGDesc.Clear()
        txtWSIDNo.Clear()
        butSave.Enabled = True
        butDelete.Enabled = False
        butUpdate.Enabled = False
        cmbwkid.Text = "-Select-"
    End Sub

    Function refTxt(ByVal txt As String) As String
        Return Trim(Replace(txt, "'", "''"))
    End Function

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        textReset()
    End Sub

    Private Sub butUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdate.Click
        If Trim(cmbRG.Text) = "-Select-" Or Trim(cmbRG.Text) = "" Then
            MsgBox("Select Resource Group Description!", MsgBoxStyle.Information, "eWIP")
        End If
        If Trim(cmbwkid.Text) = "-Select-" Or Trim(cmbwkid.Text) = "" Then
            MsgBox("Select WC ID!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtRGDesc.Text) = "" Then
            MsgBox("Enter the station description!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "Update  tbworkstation set _wc=@wc,_rid=@rid where _sIDno=@sIDno"
                parm = .Parameters.Add("@wc", SqlDbType.VarChar, 50)
                Dim DI As DataItem
                DI = cmbwkid.Items(cmbwkid.SelectedIndex)
                parm.Value = DI.Value
                parm = .Parameters.Add("@rid", SqlDbType.Int)
                parm.Value = Val(txtRGid.Text)
                parm = .Parameters.Add("@sIDno", SqlDbType.Int)
                parm.Value = Val(txtWSIDNo.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully saved!", MsgBoxStyle.Information, "eWIP")
            End With
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        Dim result As New DialogResult
        result = MessageBox.Show("Do you Want to delete this Records?", "Caption", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = Windows.Forms.DialogResult.Yes Then
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "delete from  tbworkstation  where _sIDno=@sIDno"
                    parm = .Parameters.Add("@sIDno", SqlDbType.Int)
                    parm.Value = Val(txtWSIDNo.Text)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()
                    textReset()
                    MsgBox("Successfully deleted!", MsgBoxStyle.Information, "eWIP")
                End With
            Catch ex As SqlException
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Finally
                cn.Close()
                com.Parameters.Clear()
            End Try
            list_Displaydata()
        End If
    End Sub

    Sub Getwc()
        cmbwkid.Items.Clear()
        cmbwkid.Items.Add(New DataItem("-Select-", "-Select-"))
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                'Dim DI As DataItem
                .CommandText = "select _wc,_description from tbwc order by _wc"
                cn.Open()
                dr = .ExecuteReader(CommandBehavior.CloseConnection)
                While dr.Read
                    cmbwkid.Items.Add(New DataItem(dr.Item("_description"), dr.Item("_wc")))
                End While
                cn.Close()
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
        cmbwkid.Text = "-Select-"
    End Sub

    Private Sub cmbwkid_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbwkid.SelectedIndexChanged
        If cmbwkid.Text <> "-Select-" Then
            Dim DI As DataItem
            DI = cmbwkid.Items(cmbwkid.SelectedIndex)
            txtWCDesc.Text = DI.ID
        Else
            txtWCDesc.Text = ""
        End If
    End Sub
    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub
End Class
