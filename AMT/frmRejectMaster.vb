Imports System.Data.SqlClient
Public Class frmRejectMaster
    Dim dr As SqlDataReader
    Dim parm As SqlParameter
    Private Sub frmRejectMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With lstv
            .Columns.Add("RNo", 0, HorizontalAlignment.Left)
            .Columns.Add("Rejected Code", 230, HorizontalAlignment.Left)
            .Columns.Add("Description", lstv.Width - 230, HorizontalAlignment.Left)
            list_Displaydata()

        End With
    End Sub

    Sub list_Displaydata()
        lstv.Items.Clear()
        Try
            com.Connection = cn
            com.CommandType = CommandType.Text
            com.CommandText = "select _RejID,_RejectedCode,_RejectedDesc from tbRejected order by _RejectedCode"
            'parm = com.Parameters.Add("@Type", SqlDbType.VarChar, 256)
            'parm.Value = refTxt(cmbType.Text)
            cn.Open()
            dr = com.ExecuteReader(CommandBehavior.CloseConnection)
            While dr.Read
                Dim ls As New ListViewItem(Trim(dr.Item("_RejID")))    ' you can also use reader.GetSqlValue(0) 
                ls.SubItems.Add(Trim(dr.Item("_RejectedCode")))
                ls.SubItems.Add(Trim(dr.Item("_RejectedDesc")))
                lstv.Items.Add(ls)
            End While
        Catch exce As Exception
            MsgBox(exce.ToString)
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(txtCode.Text) = "" Then
            MsgBox("Enter the rejected code!", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Trim(txtDesc.Text) = "" Then
            MsgBox("Enter the rejected description!", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "insert into tbRejected( _RejectedCode,_RejectedDesc)values(@RejectedCode,@RejectedDesc)"
                parm = .Parameters.Add("@RejectedCode", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtCode.Text)
                parm = .Parameters.Add("@RejectedDesc", SqlDbType.VarChar, 256)
                parm.Value = refTxt(txtDesc.Text)
                'parm = .Parameters.Add("@Type", SqlDbType.VarChar, 256)
                'parm.Value = refTxt(cmbType.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully saved!", MsgBoxStyle.Information)
            End With
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub
    Function refTxt(ByVal txt As String) As String
        Return Trim(Replace(txt, "'", "''"))
    End Function
    Sub textReset()
        txtRNo.Clear()
        txtCode.Clear()
        txtDesc.Clear()
        butSave.Enabled = True
        butDelete.Enabled = False
        butUpdate.Enabled = False
    End Sub
    Private Sub butUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdate.Click
        If Trim(txtCode.Text) = "" Then
            MsgBox("Enter the rejected code!", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Trim(txtDesc.Text) = "" Then
            MsgBox("Enter the rejected description!", MsgBoxStyle.Information)
            Exit Sub
        End If

        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "Update  tbRejected set _RejectedCode=@RejectedCode,_RejectedDesc=@RejectedDesc where _RejID =@RejID"
                parm = .Parameters.Add("@machineID", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtCode.Text)
                parm = .Parameters.Add("@MachineDesc", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtDesc.Text)
                parm = .Parameters.Add("@RejID ", SqlDbType.Int)
                parm.Value = Val(txtRNo.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully updated!", MsgBoxStyle.Information)
            End With
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        Dim result As New DialogResult
        result = MessageBox.Show("Do you Want to delete this Records?", "Caption", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = Windows.Forms.DialogResult.Yes Then
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "delete from   tbRejected  where _RejID =@RejID "
                    parm = .Parameters.Add("@RejID ", SqlDbType.Int)
                    parm.Value = Val(txtRNo.Text)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()
                    textReset()
                    MsgBox("Successfully deleted!", MsgBoxStyle.Information)
                End With
            Catch ex As SqlException
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            Finally
                cn.Close()
                com.Parameters.Clear()
            End Try
            list_Displaydata()
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        textReset()
    End Sub

    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click
        txtRNo.Text = lstv.SelectedItems(0).Text
        txtCode.Text = lstv.SelectedItems(0).SubItems(1).Text
        txtDesc.Text = lstv.SelectedItems(0).SubItems(2).Text
        butSave.Enabled = False
        butUpdate.Enabled = True
        butDelete.Enabled = True
        ' txtUserCode.ReadOnly = True
        'StbUser.Text = "You Can Edit And Delete This Records"
    End Sub
End Class