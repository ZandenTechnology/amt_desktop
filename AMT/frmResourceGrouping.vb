Imports System.Data.SqlClient
Public Class frmResourceGrouping
    Inherits System.Windows.Forms.Form
    Dim parm As SqlParameter
    Dim dr As SqlDataReader
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butDelete As System.Windows.Forms.Button
    Friend WithEvents butUpdate As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents txtRIDNo As System.Windows.Forms.TextBox
    Friend WithEvents txtRGDescription As System.Windows.Forms.TextBox
    Friend WithEvents txtRGID As System.Windows.Forms.TextBox
    Friend WithEvents lstv As System.Windows.Forms.ListView
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmResourceGrouping))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label3 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.butCancel = New System.Windows.Forms.Button
        Me.butDelete = New System.Windows.Forms.Button
        Me.butUpdate = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.txtRIDNo = New System.Windows.Forms.TextBox
        Me.txtRGDescription = New System.Windows.Forms.TextBox
        Me.txtRGID = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.lstv = New System.Windows.Forms.ListView
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(1, 2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(736, 687)
        Me.Panel1.TabIndex = 2
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Location = New System.Drawing.Point(8, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(720, 672)
        Me.Panel2.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(10, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(114, 15)
        Me.Label3.TabIndex = 35
        Me.Label3.Text = "RESOURCE GROUP"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Controls.Add(Me.txtRIDNo)
        Me.Panel3.Controls.Add(Me.txtRGDescription)
        Me.Panel3.Controls.Add(Me.txtRGID)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Location = New System.Drawing.Point(8, 32)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(704, 632)
        Me.Panel3.TabIndex = 0
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Panel4.Controls.Add(Me.butCancel)
        Me.Panel4.Controls.Add(Me.butDelete)
        Me.Panel4.Controls.Add(Me.butUpdate)
        Me.Panel4.Controls.Add(Me.butSave)
        Me.Panel4.Location = New System.Drawing.Point(184, 104)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(232, 72)
        Me.Panel4.TabIndex = 39
        '
        'butCancel
        '
        Me.butCancel.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butCancel.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCancel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butCancel.Image = CType(resources.GetObject("butCancel.Image"), System.Drawing.Image)
        Me.butCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(176, 5)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(48, 59)
        Me.butCancel.TabIndex = 3
        Me.butCancel.Text = "Cancel"
        Me.butCancel.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butCancel.UseVisualStyleBackColor = False
        '
        'butDelete
        '
        Me.butDelete.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butDelete.Enabled = False
        Me.butDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butDelete.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelete.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butDelete.Image = CType(resources.GetObject("butDelete.Image"), System.Drawing.Image)
        Me.butDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butDelete.Location = New System.Drawing.Point(120, 5)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(48, 59)
        Me.butDelete.TabIndex = 2
        Me.butDelete.Text = "Delete"
        Me.butDelete.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butDelete.UseVisualStyleBackColor = False
        '
        'butUpdate
        '
        Me.butUpdate.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butUpdate.Enabled = False
        Me.butUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butUpdate.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butUpdate.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butUpdate.Image = CType(resources.GetObject("butUpdate.Image"), System.Drawing.Image)
        Me.butUpdate.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butUpdate.Location = New System.Drawing.Point(64, 5)
        Me.butUpdate.Name = "butUpdate"
        Me.butUpdate.Size = New System.Drawing.Size(48, 59)
        Me.butUpdate.TabIndex = 1
        Me.butUpdate.Text = "Update"
        Me.butUpdate.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butUpdate.UseVisualStyleBackColor = False
        '
        'butSave
        '
        Me.butSave.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butSave.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSave.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butSave.Image = CType(resources.GetObject("butSave.Image"), System.Drawing.Image)
        Me.butSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(8, 5)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(48, 59)
        Me.butSave.TabIndex = 0
        Me.butSave.Text = "Save"
        Me.butSave.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butSave.UseVisualStyleBackColor = False
        '
        'txtRIDNo
        '
        Me.txtRIDNo.Location = New System.Drawing.Point(344, 24)
        Me.txtRIDNo.MaxLength = 30
        Me.txtRIDNo.Name = "txtRIDNo"
        Me.txtRIDNo.Size = New System.Drawing.Size(28, 20)
        Me.txtRIDNo.TabIndex = 38
        Me.txtRIDNo.Visible = False
        '
        'txtRGDescription
        '
        Me.txtRGDescription.Location = New System.Drawing.Point(128, 48)
        Me.txtRGDescription.MaxLength = 256
        Me.txtRGDescription.Name = "txtRGDescription"
        Me.txtRGDescription.Size = New System.Drawing.Size(488, 20)
        Me.txtRGDescription.TabIndex = 37
        '
        'txtRGID
        '
        Me.txtRGID.Location = New System.Drawing.Point(128, 24)
        Me.txtRGID.MaxLength = 30
        Me.txtRGID.Name = "txtRGID"
        Me.txtRGID.Size = New System.Drawing.Size(176, 20)
        Me.txtRGID.TabIndex = 36
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 15)
        Me.Label2.TabIndex = 35
        Me.Label2.Text = "Description"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 15)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = " Group Code"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.LightGray
        Me.Panel5.Controls.Add(Me.lstv)
        Me.Panel5.Location = New System.Drawing.Point(8, 184)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(688, 440)
        Me.Panel5.TabIndex = 1
        '
        'lstv
        '
        Me.lstv.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(8, 8)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(672, 424)
        Me.lstv.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lstv.TabIndex = 7
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'frmResourceGrouping
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(738, 690)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmResourceGrouping"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(txtRGID.Text) = "" Then
            MsgBox("Enter the resource group code!", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Trim(txtRGDescription.Text) = "" Then
            MsgBox("Enter the resource group description!", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "insert into tbResourceGroup( _rGroupID,_rGroupName)values( @rGroupID,@rGroupName)"
                parm = .Parameters.Add("@rGroupID", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtRGID.Text)
                parm = .Parameters.Add("@rGroupName", SqlDbType.VarChar, 256)
                parm.Value = refTxt(txtRGDescription.Text)
                'parm = .Parameters.Add("@Type", SqlDbType.VarChar, 256)
                'parm.Value = refTxt(cmbType.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully saved!", MsgBoxStyle.Information)
            End With
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub
    Sub list_Displaydata()
        lstv.Items.Clear()
        Try
            com.Connection = cn
            com.CommandType = CommandType.Text
            com.CommandText = "select * from tbResourceGroup"
            'parm = com.Parameters.Add("@Type", SqlDbType.VarChar, 256)
            'parm.Value = refTxt(cmbType.Text)
            cn.Open()
            dr = com.ExecuteReader(CommandBehavior.CloseConnection)
            While dr.Read
                Dim ls As New ListViewItem(Trim(dr.Item("_rid")))    ' you can also use reader.GetSqlValue(0) 
                ls.SubItems.Add(Trim(dr.Item("_rGroupID")))
                ls.SubItems.Add(Trim(dr.Item("_rGroupName")))
                lstv.Items.Add(ls)
            End While
        Catch exce As Exception
            MsgBox(exce.ToString)
        Finally
            com.Parameters.Clear()
            cn.Close()
        End Try
    End Sub

    Private Sub frmResourceGrouping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With lstv
            .Columns.Add("IDNo", 0, HorizontalAlignment.Left)
            .Columns.Add("Group Code", 230, HorizontalAlignment.Left)
            .Columns.Add("Description", lstv.Width - 230, HorizontalAlignment.Left)
            list_Displaydata()
            'list_Displaydata()
        End With
    End Sub
    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click

        txtRIDNo.Text = lstv.SelectedItems(0).Text
        txtRGID.Text = lstv.SelectedItems(0).SubItems(1).Text
        txtRGDescription.Text = lstv.SelectedItems(0).SubItems(2).Text
        butSave.Enabled = False
        butUpdate.Enabled = True
        butDelete.Enabled = True
        ' txtUserCode.ReadOnly = True
        'StbUser.Text = "You Can Edit And Delete This Records"
    End Sub
    Function refTxt(ByVal txt As String) As String
        Return Trim(Replace(txt, "'", "''"))
    End Function
    Sub textReset()
        txtRIDNo.Clear()
        txtRGID.Clear()
        txtRGDescription.Clear()
        butSave.Enabled = True
        butDelete.Enabled = False
        butUpdate.Enabled = False
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        textReset()
    End Sub

    Private Sub butUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdate.Click
        If Trim(txtRGID.Text) = "" Then
            MsgBox("Enter the resource group code!", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Trim(txtRGDescription.Text) = "" Then
            MsgBox("Enter the resource group description!", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "Update  tbResourceGroup set _rGroupID=@rGroupID,_rGroupName=@rGroupName where _rid =@rid"
                parm = .Parameters.Add("@rGroupID", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtRGID.Text)
                parm = .Parameters.Add("@rGroupName", SqlDbType.VarChar, 256)
                parm.Value = refTxt(txtRGDescription.Text)
                parm = .Parameters.Add("@rid ", SqlDbType.Int)
                parm.Value = Val(txtRIDNo.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully updated!", MsgBoxStyle.Information)
            End With
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        'If Trim(txtRGID.Text) = "" Then
        '    MsgBox("Enter the resource group id!", MsgBoxStyle.Information)
        '    Exit Sub
        'End If
        'If Trim(txtRGDescription.Text) = "" Then
        '    MsgBox("Enter the resource group description!", MsgBoxStyle.Information)
        '    Exit Sub
        'End If
        Dim result As New DialogResult
        result = MessageBox.Show("Do you Want to delete this Records?", "Caption", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = Windows.Forms.DialogResult.Yes Then
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "delete from   tbResourceGroup  where _rid =@rid "
                    parm = .Parameters.Add("@rid ", SqlDbType.Int)
                    parm.Value = Val(txtRIDNo.Text)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()
                    textReset()
                    MsgBox("Successfully deleted!", MsgBoxStyle.Information)
                End With
            Catch ex As SqlException
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            Finally
                cn.Close()
                com.Parameters.Clear()
            End Try
            list_Displaydata()
        End If
    End Sub

    'Sub GetData()

    '    Try
    '        With com
    '            .Connection = cn
    '            .CommandType = CommandType.Text
    '            Dim DI As DataItem
    '            .CommandText = "select  _desc from tbResourceType"
    '            cn.Open()
    '            dr = .ExecuteReader(CommandBehavior.CloseConnection)
    '            While dr.Read
    '                cmbType.Items.Add(dr.Item("_desc"))
    '            End While
    '            cn.Close()
    '        End With
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    Finally
    '        cn.Close()
    '    End Try
    '    cmbType.Text = "-Select-"
    'End Sub

    Private Sub cmbType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        list_Displaydata()
    End Sub

   
    
End Class
