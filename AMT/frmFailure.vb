Imports System.Data.SqlClient
Public Class frmFailure
    Inherits System.Windows.Forms.Form
    Dim parm As SqlParameter
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtFailureCode As System.Windows.Forms.TextBox
    Friend WithEvents cmbwkid As System.Windows.Forms.ComboBox
    Friend WithEvents txtRGid As System.Windows.Forms.TextBox
    Friend WithEvents txtwcid As System.Windows.Forms.TextBox
    Dim dr As SqlDataReader
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butDelete As System.Windows.Forms.Button
    Friend WithEvents butUpdate As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents txtFailureDescription As System.Windows.Forms.TextBox
    Friend WithEvents cmbRG As System.Windows.Forms.ComboBox
    Friend WithEvents txtFIDNo As System.Windows.Forms.TextBox
    Friend WithEvents lstv As System.Windows.Forms.ListView
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFailure))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label3 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.txtwcid = New System.Windows.Forms.TextBox
        Me.txtRGid = New System.Windows.Forms.TextBox
        Me.txtFailureCode = New System.Windows.Forms.TextBox
        Me.cmbwkid = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtFailureDescription = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cmbRG = New System.Windows.Forms.ComboBox
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.butCancel = New System.Windows.Forms.Button
        Me.butDelete = New System.Windows.Forms.Button
        Me.butUpdate = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.txtFIDNo = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.lstv = New System.Windows.Forms.ListView
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(10, 8)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(736, 687)
        Me.Panel1.TabIndex = 4
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Location = New System.Drawing.Point(8, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(720, 672)
        Me.Panel2.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(10, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(146, 15)
        Me.Label3.TabIndex = 35
        Me.Label3.Text = "REJECTED DESCRIPTION"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.txtwcid)
        Me.Panel3.Controls.Add(Me.txtRGid)
        Me.Panel3.Controls.Add(Me.txtFailureCode)
        Me.Panel3.Controls.Add(Me.cmbwkid)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.txtFailureDescription)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.cmbRG)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Controls.Add(Me.txtFIDNo)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Location = New System.Drawing.Point(8, 32)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(704, 632)
        Me.Panel3.TabIndex = 0
        '
        'txtwcid
        '
        Me.txtwcid.BackColor = System.Drawing.Color.White
        Me.txtwcid.Location = New System.Drawing.Point(342, 33)
        Me.txtwcid.MaxLength = 256
        Me.txtwcid.Name = "txtwcid"
        Me.txtwcid.ReadOnly = True
        Me.txtwcid.Size = New System.Drawing.Size(74, 20)
        Me.txtwcid.TabIndex = 55
        Me.txtwcid.Visible = False
        '
        'txtRGid
        '
        Me.txtRGid.BackColor = System.Drawing.Color.White
        Me.txtRGid.Location = New System.Drawing.Point(382, 7)
        Me.txtRGid.MaxLength = 256
        Me.txtRGid.Name = "txtRGid"
        Me.txtRGid.ReadOnly = True
        Me.txtRGid.Size = New System.Drawing.Size(74, 20)
        Me.txtRGid.TabIndex = 54
        Me.txtRGid.Visible = False
        '
        'txtFailureCode
        '
        Me.txtFailureCode.Location = New System.Drawing.Point(184, 60)
        Me.txtFailureCode.MaxLength = 256
        Me.txtFailureCode.Name = "txtFailureCode"
        Me.txtFailureCode.Size = New System.Drawing.Size(102, 20)
        Me.txtFailureCode.TabIndex = 51
        '
        'cmbwkid
        '
        Me.cmbwkid.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbwkid.Location = New System.Drawing.Point(184, 33)
        Me.cmbwkid.Name = "cmbwkid"
        Me.cmbwkid.Size = New System.Drawing.Size(149, 21)
        Me.cmbwkid.TabIndex = 50
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(8, 36)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 15)
        Me.Label5.TabIndex = 49
        Me.Label5.Text = "WC ID"
        '
        'txtFailureDescription
        '
        Me.txtFailureDescription.Location = New System.Drawing.Point(184, 87)
        Me.txtFailureDescription.MaxLength = 256
        Me.txtFailureDescription.Name = "txtFailureDescription"
        Me.txtFailureDescription.Size = New System.Drawing.Size(480, 20)
        Me.txtFailureDescription.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 63)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(89, 15)
        Me.Label4.TabIndex = 47
        Me.Label4.Text = "Rejected Code"
        '
        'cmbRG
        '
        Me.cmbRG.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbRG.Items.AddRange(New Object() {"-Select-", "Admin", "Supervisor", "Worker"})
        Me.cmbRG.Location = New System.Drawing.Point(184, 6)
        Me.cmbRG.Name = "cmbRG"
        Me.cmbRG.Size = New System.Drawing.Size(192, 21)
        Me.cmbRG.TabIndex = 1
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Panel4.Controls.Add(Me.butCancel)
        Me.Panel4.Controls.Add(Me.butDelete)
        Me.Panel4.Controls.Add(Me.butUpdate)
        Me.Panel4.Controls.Add(Me.butSave)
        Me.Panel4.Location = New System.Drawing.Point(184, 112)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(232, 72)
        Me.Panel4.TabIndex = 39
        '
        'butCancel
        '
        Me.butCancel.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butCancel.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCancel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butCancel.Image = CType(resources.GetObject("butCancel.Image"), System.Drawing.Image)
        Me.butCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(176, 5)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(48, 59)
        Me.butCancel.TabIndex = 7
        Me.butCancel.Text = "Cancel"
        Me.butCancel.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butCancel.UseVisualStyleBackColor = False
        '
        'butDelete
        '
        Me.butDelete.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butDelete.Enabled = False
        Me.butDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butDelete.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelete.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butDelete.Image = CType(resources.GetObject("butDelete.Image"), System.Drawing.Image)
        Me.butDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butDelete.Location = New System.Drawing.Point(120, 5)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(48, 59)
        Me.butDelete.TabIndex = 6
        Me.butDelete.Text = "Delete"
        Me.butDelete.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butDelete.UseVisualStyleBackColor = False
        '
        'butUpdate
        '
        Me.butUpdate.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butUpdate.Enabled = False
        Me.butUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butUpdate.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butUpdate.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butUpdate.Image = CType(resources.GetObject("butUpdate.Image"), System.Drawing.Image)
        Me.butUpdate.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butUpdate.Location = New System.Drawing.Point(64, 5)
        Me.butUpdate.Name = "butUpdate"
        Me.butUpdate.Size = New System.Drawing.Size(48, 59)
        Me.butUpdate.TabIndex = 5
        Me.butUpdate.Text = "Update"
        Me.butUpdate.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butUpdate.UseVisualStyleBackColor = False
        '
        'butSave
        '
        Me.butSave.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butSave.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSave.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butSave.Image = CType(resources.GetObject("butSave.Image"), System.Drawing.Image)
        Me.butSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(8, 5)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(48, 59)
        Me.butSave.TabIndex = 4
        Me.butSave.Text = "Save"
        Me.butSave.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butSave.UseVisualStyleBackColor = False
        '
        'txtFIDNo
        '
        Me.txtFIDNo.Location = New System.Drawing.Point(555, 56)
        Me.txtFIDNo.MaxLength = 30
        Me.txtFIDNo.Name = "txtFIDNo"
        Me.txtFIDNo.Size = New System.Drawing.Size(28, 20)
        Me.txtFIDNo.TabIndex = 38
        Me.txtFIDNo.TabStop = False
        Me.txtFIDNo.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 90)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(125, 15)
        Me.Label2.TabIndex = 35
        Me.Label2.Text = "Rejected Description"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(113, 15)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "Resource Group ID"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.LightGray
        Me.Panel5.Controls.Add(Me.lstv)
        Me.Panel5.Location = New System.Drawing.Point(8, 192)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(688, 432)
        Me.Panel5.TabIndex = 1
        '
        'lstv
        '
        Me.lstv.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(8, 8)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(672, 416)
        Me.lstv.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lstv.TabIndex = 7
        Me.lstv.TabStop = False
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'frmFailure
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(756, 702)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmFailure"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmFailure_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With lstv
            .Columns.Add("SIDno", 0, HorizontalAlignment.Left)
            .Columns.Add("Rejected ID", 230, HorizontalAlignment.Left)
            .Columns.Add("Rejected Description", lstv.Width - 330, HorizontalAlignment.Left)
            .Columns.Add("WC", 100, HorizontalAlignment.Left)
            GetData()
        End With
    End Sub
    Sub GetData()

        cmbRG.Items.Clear()
        cmbRG.Items.Add(New DataItem("-Select-", "-Select-"))
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                'Dim DI As DataItem
                .CommandText = "select _rGroupid,_rGroupName,_rid from tbResourceGroup"
                cn.Open()
                dr = .ExecuteReader(CommandBehavior.CloseConnection)
                While dr.Read
                    cmbRG.Items.Add(New DataItem(dr.Item("_rGroupName") & "||" & dr.Item("_rid"), dr.Item("_rGroupid")))
                End While
                cn.Close()
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cn.Close()
        End Try
        cmbRG.Text = "-Select-"
        'cmbRG.Items.Clear()
        'cmbRG.Items.Add(New DataItem("-Select-", "-Select-"))
        'Try
        '    With com
        '        .Connection = cn
        '        .CommandType = CommandType.Text
        '        Dim DI As DataItem
        '        .CommandText = "select _rGroupID,_rGroupName from tbResourceGroup"
        '        cn.Open()
        '        dr = .ExecuteReader(CommandBehavior.CloseConnection)
        '        While dr.Read
        '            cmbRG.Items.Add(New DataItem(dr.Item("_rGroupID"), dr.Item("_rGroupName")))
        '        End While
        '        cn.Close()
        '    End With
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'Finally
        '    cn.Close()
        'End Try
        'cmbRG.Text = "-Select-"
    End Sub

    Private Sub cmbRG_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRG.SelectedIndexChanged
        cmbwkid.Items.Clear()
        cmbwkid.Items.Add(New DataItem("-Select-", "-Select-"))
        Try
            If cmbRG.Text <> "-Select-" Then
                If cmbRG.Text <> "-Select-" Then
                    Dim DI As DataItem
                    DI = cmbRG.Items(cmbRG.SelectedIndex)
                    ' txtRGDesc.Text = Split(DI.ID, "||")(0)
                    txtRGid.Text = Split(DI.ID, "||")(1)
                Else
                    txtRGid.Text = ""
                    ' txtRGDesc.Text = ""
                End If

                With com
                    .Connection = cn
                    .CommandType = CommandType.Text

                    .CommandText = "select * from tbWorkStation A,tbWc B,tbResourceGroup C where A._wc=B._Wc and A._rid=C._rid and A._rid=" & Val(txtRGid.Text)
                    cn.Open()
                    dr = .ExecuteReader(CommandBehavior.CloseConnection)
                    While dr.Read
                        cmbwkid.Items.Add(New DataItem(dr.Item("_description") & "||" & dr.Item("_sIDno"), dr.Item("_wc")))
                    End While
                    cn.Close()
                End With
            End If
            Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cn.Close()
        End Try
        cmbwkid.Text = "-Select-"




        list_Displaydata()
    End Sub
    Sub list_Displaydata()
        lstv.Items.Clear()
        If Trim(cmbRG.Text) <> "-Select-" And Trim(cmbRG.Text) <> "" Then
            Try
                With com
                    com.Connection = cn
                    com.CommandType = CommandType.Text
                    'Dim sql As String = "select * from tbFailure A,tbWorkStation B, tbWC C where A._sIDno=B._sIDno and B._wc=C._wc and B._rid=" & Val(txtRGid.Text) & " and A._sIDno=" & Val(txtwcid.Text)
                    If Trim(txtwcid.Text) <> "" Then
                        com.CommandText = "select * from tbFailure A,tbWorkStation B, tbWC C where A._sIDno=B._sIDno and B._wc=C._wc and B._rid=" & Val(txtRGid.Text) & " and A._sIDno=" & Val(txtwcid.Text)
                    Else
                        com.CommandText = "select * from tbFailure A,tbWorkStation B, tbWC C where A._sIDno=B._sIDno and B._wc=C._wc and B._rid=" & Val(txtRGid.Text)
                    End If

                    'parm = .Parameters.Add("@rGroupID", SqlDbType.VarChar, 50)
                    'Dim DI As DataItem
                    'DI = cmbRG.Items(cmbRG.SelectedIndex)
                    'parm.Value = DI.ID
                    cn.Open()
                    dr = com.ExecuteReader(CommandBehavior.CloseConnection)
                    While dr.Read
                        Dim ls As New ListViewItem(Trim(dr.Item("_FIDno")))    ' you can also use reader.GetSqlValue(0) 
                        ls.SubItems.Add(Trim(dr.Item("_failureCode")))
                        ls.SubItems.Add(Trim(dr.Item("_failureDescription")))
                        ls.SubItems.Add(Trim(dr.Item("_wc")))
                        lstv.Items.Add(ls)
                    End While
                End With
            Catch exce As Exception
                MsgBox(exce.Message)

            Finally
                cn.Close()
                com.Parameters.Clear()
            End Try
        End If
    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(cmbRG.Text) = "-Select-" Or Trim(cmbRG.Text) = "" Then
            MsgBox("Select Resource Group ID!", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Trim(cmbwkid.Text) = "-Select-" Or Trim(cmbwkid.Text) = "" Then
            MsgBox("Select WC ID!", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Trim(txtFailureCode.Text) = "" Then
            MsgBox("Enter the failure code!", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Trim(txtFailureDescription.Text) = "" Then
            MsgBox("Enter the failure description!", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "insert into tbFailure(_FailureCode,_failureDescription,_sIDno)values(@FailureCode,@failureDescription,@sIDno)"
                parm = .Parameters.Add("@FailureCode", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtFailureCode.Text)
                parm = .Parameters.Add("@failureDescription", SqlDbType.VarChar, 256)
                parm.Value = refTxt(txtFailureDescription.Text)
                parm = .Parameters.Add("@sIDno", SqlDbType.Int)
                parm.Value = Val(txtwcid.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully saved!", MsgBoxStyle.Information)
            End With
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub
    Function refTxt(ByVal txt As String) As String
        Return Trim(Replace(txt, "'", "''"))
    End Function

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        textReset()
    End Sub
    Sub textReset()
        txtFIDNo.Clear()
        txtFailureCode.Clear()
        txtFailureDescription.Clear()
        butSave.Enabled = True
        butDelete.Enabled = False
        butUpdate.Enabled = False
    End Sub

    Private Sub butUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdate.Click
        If Trim(cmbRG.Text) = "-Select-" Or Trim(cmbRG.Text) = "" Then
            MsgBox("Select Resource Group Description!", MsgBoxStyle.Information)
        End If
        If Trim(txtFailureCode.Text) = "" Then
            MsgBox("Enter the failure code!", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Trim(txtFailureDescription.Text) = "" Then
            MsgBox("Enter the failure description!", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "Update  tbFailure set _FailureCode=@FailureCode,_failureDescription=@failureDescription where _FIDno=@FIDno"
                parm = .Parameters.Add("@FailureCode", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtFailureCode.Text)
                parm = .Parameters.Add("@failureDescription", SqlDbType.VarChar, 256)
                parm.Value = refTxt(txtFailureDescription.Text)
                parm = .Parameters.Add("@FIDno", SqlDbType.Int)
                parm.Value = Val(txtFIDNo.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully updated!", MsgBoxStyle.Information)
            End With
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        Dim result As New DialogResult
        result = MessageBox.Show("Do you Want to delete this Records?", "Caption", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = Windows.Forms.DialogResult.Yes Then
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "delete from tbFailure  where _FIDno=@FIDno"

                    parm = .Parameters.Add("@FIDno", SqlDbType.Int)
                    parm.Value = Val(txtFIDNo.Text)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()
                    textReset()
                    MsgBox("Successfully updated!", MsgBoxStyle.Information)
                End With
            Catch ex As SqlException
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            Finally
                cn.Close()
                com.Parameters.Clear()
            End Try
            list_Displaydata()
        End If
    End Sub

    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub

    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click

        txtFIDNo.Text = lstv.SelectedItems(0).Text
        txtFailureCode.Text = lstv.SelectedItems(0).SubItems(1).Text
        txtFailureDescription.Text = lstv.SelectedItems(0).SubItems(2).Text
        cmbwkid.Text = lstv.SelectedItems(0).SubItems(3).Text
        butSave.Enabled = False
        butUpdate.Enabled = True
        butDelete.Enabled = True
    End Sub

    Private Sub Panel4_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel4.Paint

    End Sub

    Private Sub cmbwkid_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbwkid.SelectedIndexChanged
        If cmbwkid.Text <> "-Select-" Then
            Dim DI As DataItem
            DI = cmbwkid.Items(cmbwkid.SelectedIndex)
            txtwcid.Text = Split(DI.ID, "||")(1)
        Else
            txtwcid.Text = ""
        End If
        list_Displaydata()
    End Sub

End Class
