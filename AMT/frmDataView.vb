Imports System.Data.SqlClient

Public Class frmDataView
    Inherits System.Windows.Forms.Form
    Dim dr As SqlDataReader
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtwc As System.Windows.Forms.TextBox
    Friend WithEvents txtJobNo As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents butSearch As System.Windows.Forms.Button
    Friend WithEvents dtpFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtto As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtfrom As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents lstv As System.Windows.Forms.ListView
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents butEXIT As System.Windows.Forms.Button
    Dim parm As SqlParameter

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.butEXIT = New System.Windows.Forms.Button
        Me.txtwc = New System.Windows.Forms.TextBox
        Me.txtJobNo = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.butSearch = New System.Windows.Forms.Button
        Me.dtpFrom = New System.Windows.Forms.DateTimePicker
        Me.dtpTo = New System.Windows.Forms.DateTimePicker
        Me.txtto = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtfrom = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.lstv = New System.Windows.Forms.ListView
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Location = New System.Drawing.Point(11, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(873, 647)
        Me.Panel2.TabIndex = 2
        '
        'Label9
        '
        Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(351, 622)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(66, 13)
        Me.Label9.TabIndex = 56
        Me.Label9.Text = "Completed"
        '
        'Label8
        '
        Me.Label8.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(200, 622)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(56, 13)
        Me.Label8.TabIndex = 55
        Me.Label8.Text = "Progress"
        '
        'Label6
        '
        Me.Label6.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label6.BackColor = System.Drawing.Color.Green
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(328, 622)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(18, 13)
        Me.Label6.TabIndex = 54
        '
        'Label7
        '
        Me.Label7.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label7.BackColor = System.Drawing.Color.Orange
        Me.Label7.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(180, 622)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(18, 13)
        Me.Label7.TabIndex = 53
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(13, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(79, 15)
        Me.Label3.TabIndex = 35
        Me.Label3.Text = "Job Process"
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.butEXIT)
        Me.Panel3.Controls.Add(Me.txtwc)
        Me.Panel3.Controls.Add(Me.txtJobNo)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.butSearch)
        Me.Panel3.Controls.Add(Me.dtpFrom)
        Me.Panel3.Controls.Add(Me.dtpTo)
        Me.Panel3.Controls.Add(Me.txtto)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.txtfrom)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Location = New System.Drawing.Point(16, 40)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(843, 579)
        Me.Panel3.TabIndex = 0
        '
        'butEXIT
        '
        Me.butEXIT.BackColor = System.Drawing.Color.Gray
        Me.butEXIT.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butEXIT.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
        Me.butEXIT.ForeColor = System.Drawing.Color.White
        Me.butEXIT.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butEXIT.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butEXIT.Location = New System.Drawing.Point(488, 30)
        Me.butEXIT.Name = "butEXIT"
        Me.butEXIT.Size = New System.Drawing.Size(59, 28)
        Me.butEXIT.TabIndex = 60
        Me.butEXIT.Text = "Exit"
        Me.butEXIT.UseVisualStyleBackColor = False
        '
        'txtwc
        '
        Me.txtwc.Location = New System.Drawing.Point(129, 40)
        Me.txtwc.Name = "txtwc"
        Me.txtwc.Size = New System.Drawing.Size(143, 20)
        Me.txtwc.TabIndex = 48
        '
        'txtJobNo
        '
        Me.txtJobNo.Location = New System.Drawing.Point(129, 13)
        Me.txtJobNo.Name = "txtJobNo"
        Me.txtJobNo.Size = New System.Drawing.Size(143, 20)
        Me.txtJobNo.TabIndex = 47
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(21, 43)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(82, 15)
        Me.Label5.TabIndex = 46
        Me.Label5.Text = "Work Center "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(21, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 15)
        Me.Label2.TabIndex = 45
        Me.Label2.Text = "Job No "
        '
        'butSearch
        '
        Me.butSearch.BackColor = System.Drawing.Color.Gray
        Me.butSearch.Font = New System.Drawing.Font("Arial", 8.75!, System.Drawing.FontStyle.Bold)
        Me.butSearch.ForeColor = System.Drawing.Color.White
        Me.butSearch.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butSearch.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSearch.Location = New System.Drawing.Point(418, 30)
        Me.butSearch.Name = "butSearch"
        Me.butSearch.Size = New System.Drawing.Size(59, 28)
        Me.butSearch.TabIndex = 0
        Me.butSearch.Text = "Search"
        Me.butSearch.UseVisualStyleBackColor = False
        '
        'dtpFrom
        '
        Me.dtpFrom.Location = New System.Drawing.Point(224, 71)
        Me.dtpFrom.Name = "dtpFrom"
        Me.dtpFrom.Size = New System.Drawing.Size(19, 20)
        Me.dtpFrom.TabIndex = 44
        '
        'dtpTo
        '
        Me.dtpTo.Location = New System.Drawing.Point(382, 71)
        Me.dtpTo.Name = "dtpTo"
        Me.dtpTo.Size = New System.Drawing.Size(19, 20)
        Me.dtpTo.TabIndex = 43
        '
        'txtto
        '
        Me.txtto.Location = New System.Drawing.Point(301, 71)
        Me.txtto.Name = "txtto"
        Me.txtto.Size = New System.Drawing.Size(100, 20)
        Me.txtto.TabIndex = 42
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(251, 74)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(50, 15)
        Me.Label4.TabIndex = 41
        Me.Label4.Text = "To Date"
        '
        'txtfrom
        '
        Me.txtfrom.Location = New System.Drawing.Point(129, 71)
        Me.txtfrom.Name = "txtfrom"
        Me.txtfrom.Size = New System.Drawing.Size(100, 20)
        Me.txtfrom.TabIndex = 40
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(21, 74)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 15)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "From Date"
        '
        'Panel5
        '
        Me.Panel5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Panel5.BackColor = System.Drawing.Color.LightGray
        Me.Panel5.Controls.Add(Me.lstv)
        Me.Panel5.Location = New System.Drawing.Point(7, 97)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(833, 473)
        Me.Panel5.TabIndex = 1
        '
        'lstv
        '
        Me.lstv.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.lstv.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(17, 10)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(813, 452)
        Me.lstv.TabIndex = 7
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'frmDataView
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.Ivory
        Me.ClientSize = New System.Drawing.Size(896, 669)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmDataView"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmDataView_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sd As Date
        sd = Now
        dtpFrom.Value = sd
        txtfrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        ' sd = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 1, sd))
        dtpTo.Value = sd
        txtto.Text = Format(dtpTo.Value, "dd/MM/yyyy")

        With lstv
            .Columns.Add("Job No", 100, HorizontalAlignment.Left)
            .Columns.Add("Item", 100, HorizontalAlignment.Left)
            .Columns.Add("Op", 100, HorizontalAlignment.Left)
            .Columns.Add("Work Center", 100, HorizontalAlignment.Left)
            .Columns.Add("Parent Suff", 100, HorizontalAlignment.Left)
            .Columns.Add("Child Suff", 100, HorizontalAlignment.Left)
            .Columns.Add("Rel Qty", 150, HorizontalAlignment.Right)
            .Columns.Add("Recd Qty", 150, HorizontalAlignment.Right)
            .Columns.Add("Comp Qty", 150, HorizontalAlignment.Right)
            .Columns.Add("Rej Qty", 150, HorizontalAlignment.Right)
            .Columns.Add("Start Date", 150, HorizontalAlignment.Left)
            .Columns.Add("End Date", 150, HorizontalAlignment.Left)

        End With

        txtJobNo.Select()
    End Sub

    Private Sub txtfrom_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub dtpFrom_ValueChanged2(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub dtpTo_ValueChanged2(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub butSearch_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs)

    End Sub









    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtfrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False

        Dim sd As Date
        sd = DateSerial(Year(dtpFrom.Value), Month(dtpFrom.Value), 1)
        sd = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 1, sd))
        dtpTo.Value = sd
        txtto.Text = Format(dtpTo.Value, "dd/MM/yyyy")
    End Sub

    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtto.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        dtpTo.Checked = False
    End Sub

    Private Sub butSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
        lstv.Items.Clear()

        Dim dt As Date
        Dim s() As String = Split(txtfrom.Text, "/")
        Dim SD As Date = DateSerial(s(2), s(1), s(0))
        Dim s1() As String = Split(txtto.Text, "/")
        Dim ED As Date = DateSerial(s1(2), s1(1), s1(0))


        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                'If txtwc.Text <> "" And txtJobNo.Text <> "" Then
                Dim strid() As String = Split(Trim(txtJobNo.Text), "-")
                If txtJobNo.Text = "" Then
                    .CommandText = "select * from tbjobtrans where _job like(@jobno) and _wc like (@wc) and (_Start_Date>=@SD and _Start_Date<=@ED) order by _Job,_oper_num,_Start_Date"
                Else
                    .CommandText = "select * from tbjobtrans where _job like(@jobno) and _wc like (@wc)  order by _Job,_oper_num,_Start_Date"
                End If
                If txtJobNo.Text = "" Then
                    parm = .Parameters.Add("@SD", SqlDbType.Float)
                    parm.Value = SD.ToOADate
                    parm = .Parameters.Add("@ED", SqlDbType.Float)
                    parm.Value = ED.ToOADate
                End If
                parm = .Parameters.Add("@wc", SqlDbType.VarChar)
                parm.Value = Trim(txtwc.Text & "%")
                parm = .Parameters.Add("@jobno", SqlDbType.VarChar)
                parm.Value = Trim(strid(0) & "%")
                'Else
                'If txtwc.Text <> "" And txtJobNo.Text = "" Then
                '    .CommandText = "select * from tbjobtrans where _wc=@wc and (_end_Date>=@SD and _end_Date<=@ED) order by _job,_jobsuffix"
                '    parm = .Parameters.Add("@SD", SqlDbType.Float)
                '    parm.Value = SD.ToOADate
                '    parm = .Parameters.Add("@ED", SqlDbType.Float)
                '    parm.Value = ED.ToOADate
                '    parm = .Parameters.Add("@wc", SqlDbType.VarChar)
                '    parm.Value = Trim(txtwc.Text)
                '    'parm = .Parameters.Add("@jobno", SqlDbType.VarChar)
                '    'parm.Value = Trim(txtJobNo.Text)
                'Else
                '    If txtwc.Text = "" And txtJobNo.Text <> "" Then
                '        .CommandText = "select * from tbjobtrans where _job=@jobno and (_end_Date>=@SD and _end_Date<=@ED) order by _job,_jobsuffix"
                '        parm = .Parameters.Add("@SD", SqlDbType.Float)
                '        parm.Value = SD.ToOADate
                '        parm = .Parameters.Add("@ED", SqlDbType.Float)
                '        parm.Value = ED.ToOADate
                '        parm = .Parameters.Add("@jobno", SqlDbType.VarChar)
                '        parm.Value = Trim(txtJobNo.Text)
                '    Else
                '        .CommandText = "select * from tbjobtrans where (_end_Date>=@SD and _end_Date<=@ED) order by _job,_jobsuffix"
                '        parm = .Parameters.Add("@SD", SqlDbType.Float)
                '        parm.Value = SD.ToOADate
                '        parm = .Parameters.Add("@ED", SqlDbType.Float)
                '        parm.Value = ED.ToOADate

                '    End If

                'End If
                'End If


                cn.Open()
                dr = .ExecuteReader
                If dr.HasRows = True Then
                    While dr.Read
                        Dim ls As New ListViewItem(Trim(dr.Item("_job")))
                        ls.SubItems.Add(Trim(dr.Item("_item")))
                        ls.SubItems.Add(Trim(dr.Item("_oper_num")))
                        ls.SubItems.Add(Trim(dr.Item("_wc")))
                        ls.SubItems.Add(Trim(dr.Item("_jobsuffixParent")))
                        ls.SubItems.Add(Trim(dr.Item("_jobsuffix")))
                        ls.SubItems.Add(Trim(dr.Item("_qty_Rele_qty")))
                        ls.SubItems.Add(Trim(dr.Item("_qty_op_qty")))
                        ls.SubItems.Add(Trim(dr.Item("_qty_complete")))
                        ls.SubItems.Add(Trim(dr.Item("_qty_scrapped")))

                        Dim sd1 As Double = CDbl(dr.Item("_Start_Date"))
                        dt = Date.FromOADate(sd1)
                        ls.SubItems.Add(Format(dt, "dd/MM/yyyy HH:mm"))
                        If dr.Item("_end_Date") = 0 Then
                            ls.SubItems.Add("-")
                        Else
                            sd1 = CDbl(dr.Item("_end_Date"))
                            ls.SubItems.Add(Format(dt, "dd/MM/yyyy HH:mm"))
                        End If

                        If dr.Item("_end_Date") = 0 Then
                            lstv.Items.Add(ls).ForeColor = Color.Orange
                        Else
                            lstv.Items.Add(ls).ForeColor = Color.Green
                        End If
                    End While
                Else
                    MsgBox("No Record Found!", MsgBoxStyle.Information, "eWIP")
                End If

            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub

   
End Class
