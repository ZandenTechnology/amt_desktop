Imports System.Data.SqlClient
Public Class frmCheckingJobProblem
    Dim dr As SqlDataReader
    Dim parm As SqlParameter
    Dim clsD As New clsMain
    Private Sub frmCheckingJobProblem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    
    End Sub

    Private Sub butFind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butFind.Click
        PB1.Value = 0
        PB1.Minimum = 0
        PB1.Maximum = 0

        If Trim(txtFindJob.Text) = "" Then
            MsgBox("Enter the Job order")
            Exit Sub
        End If
        Try
            Dim strJobGet As String() = Split(txtFindJob.Text, vbCrLf)
            Dim k As Integer
            PB1.Maximum = strJobGet.Length
            For k = 0 To strJobGet.Length - 1
                Dim dsJob As New DataSet
                Dim dsJobOP As New DataSet
                Dim dsJobAF As New DataSet
                Dim dsJobBF As New DataSet
                Dim dsjobroute As New DataSet
                dsjobroute = clsD.GetDataset("select * from tbJob A,tbjobRoute B where A._Job=B._Job and A._job='" & Trim(strJobGet(k)) & "' order by B._operationNo", "tbJOB")
                Dim i As Integer
                Dim intRe As String = ""
                If dsjobroute.Tables(0).Rows.Count > 0 Then
                    For i = 0 To dsjobroute.Tables(0).Rows.Count - 1
                        Dim strMain, strChild As String
                        Dim dsJobTrans As New DataSet
                        Dim dsJobTransHis As New DataSet
                        Dim dsJobTransMain As New DataSet
                        Dim comQty As Double = 0
                        Dim comOpqty As Double = 0
                        Dim comRejqty As Double = 0

                        Dim comMainQty As Double = 0
                        Dim comMainOpqty As Double = 0
                        Dim comMainRejqty As Double = 0

                        dsJobTrans = clsD.GetDataset("select * from tbJobTrans where _job='" & Trim(strJobGet(k)) & "' and _oper_num=" & Val(dsjobroute.Tables(0).Rows(i).Item("_operationNo")), "tbJOB")
                        dsJobTransHis = clsD.GetDataset("select * from tbJobTransHis where _job='" & Trim(strJobGet(k)) & "' and _oper_num=" & Val(dsjobroute.Tables(0).Rows(i).Item("_operationNo")), "tbJOB")
                        dsJobTransMain = clsD.GetDataset("select * from tbJobTransMain where _job='" & Trim(strJobGet(k)) & "' and _oper_num=" & Val(dsjobroute.Tables(0).Rows(i).Item("_operationNo")), "tbJOB")
                        If dsJobTrans.Tables(0).Rows.Count > 0 Then
                            Dim j As Integer



                            strMain = ""
                            strChild = ""
                            Dim MisOpMain As String = ""
                            Dim MisOpChild As String = ""
                            If dsJobTransMain.Tables(0).Rows.Count > 0 Then
                                strMain = "True"

                                comMainQty = dsJobTransMain.Tables(0).Rows(0).Item("_qty_complete")
                                comMainOpqty = dsJobTransMain.Tables(0).Rows(0).Item("_qty_op_qty")
                                comMainRejqty = dsJobTransMain.Tables(0).Rows(0).Item("_qty_scrapped")
                                If comMainQty <> 0 And comMainRejqty <> 0 Then
                                    MisOpMain = "1"
                                End If
                                If comMainQty = 0 And comMainRejqty = 0 Then
                                    strMain = ""

                                End If
                            End If



                            For j = 0 To dsJobTrans.Tables(0).Rows.Count - 1

                                With dsJobTrans.Tables(0).Rows(j)

                                    If .Item("_Rework") = 0 Then
                                        If UCase(.Item("_status")) = "COMPLETED" Then
                                            comQty = comQty + .Item("_qty_complete")
                                            comRejqty = comRejqty + .Item("_qty_scrapped")
                                            comOpqty = comOpqty + .Item("_qty_op_qty")
                                            strChild = "True"
                                            If comQty = 0 And comRejqty = 0 Then
                                                strChild = ""
                                            End If
                                        End If
                                    Else
                                        intRe = "--Rework"
                                        'comQty = comQty + .Item("_qty_complete")
                                        'comRejqty = comRejqty + .Item("_qty_scrapped")
                                        comRejqty = comRejqty + .Item("_qty_scrapped")
                                        strChild = "True"
                                        If dsJobTransHis.Tables(0).Rows.Count > 0 Then


                                            Dim drHis() As DataRow = dsJobTransHis.Tables(0).Select("_jobsuffix='" & .Item("_jobsuffixParent") & "'")
                                            If drHis.Length > 0 Then
                                                Dim DSRejectHis As New DataSet
                                                DSRejectHis = clsD.GetDataset("select sum(_qty_scrapped) as _qty_scrapped from tbJobTransHis where _job='" & Trim(strJobGet(k)) & "' and _oper_num<" & Val(dsjobroute.Tables(0).Rows(i).Item("_operationNo")) & " and _Rework>=" & drHis(0).Item("_Rework"), "tbJOB")

                                                Dim DSReject As New DataSet
                                                DSReject = clsD.GetDataset("select sum(_qty_scrapped) as _qty_scrapped from tbJobTrans where _job='" & Trim(strJobGet(k)) & "' and _oper_num<" & Val(dsjobroute.Tables(0).Rows(i).Item("_operationNo")) & " and _Rework>=" & drHis(0).Item("_Rework"), "tbJOB")


                                                comOpqty = comOpqty + drHis(0).Item("_qty_op_qty")
                                                comQty = comQty + drHis(0).Item("_qty_complete")
                                                comQty = comQty - comRejqty
                                                comRejqty = comRejqty + drHis(0).Item("_qty_scrapped")
                                                If DSRejectHis.Tables(0).Rows.Count > 0 Then
                                                    If IsDBNull(DSRejectHis.Tables(0).Rows(0).Item("_qty_scrapped")) = False Then
                                                        comOpqty = comOpqty - DSRejectHis.Tables(0).Rows(0).Item("_qty_scrapped")
                                                        comQty = comQty - DSRejectHis.Tables(0).Rows(0).Item("_qty_scrapped")
                                                    End If
                                                End If
                                                If DSReject.Tables(0).Rows.Count > 0 Then
                                                    If IsDBNull(DSReject.Tables(0).Rows(0).Item("_qty_scrapped")) = False Then
                                                        comOpqty = comOpqty - DSReject.Tables(0).Rows(0).Item("_qty_scrapped")
                                                        comQty = comQty - DSReject.Tables(0).Rows(0).Item("_qty_scrapped")
                                                    End If
                                                End If

                                            End If
                                        End If
                                    End If
                                End With
                            Next



                        End If


                        If (strMain = "True" And strChild = "") Or (strMain = "" And strChild = "True") Then
                            txtProb.Text = txtProb.Text & vbCrLf & dsjobroute.Tables(0).Rows(i).Item("_job") & "--" & dsjobroute.Tables(0).Rows(i).Item("_operationNo") & "--" & "(Trans and Trans Records not Matching)" & intRe
                            Exit For
                        ElseIf (comMainQty <> comQty) Or comRejqty <> comMainRejqty Then
                            txtProb.Text = txtProb.Text & vbCrLf & dsjobroute.Tables(0).Rows(i).Item("_job") & "--" & dsjobroute.Tables(0).Rows(i).Item("_operationNo") & "--" & "(Trans and Trans Main qty not Matching)" & intRe
                            Exit For
                        End If



                    Next
                End If





                PB1.Value = k + 1
                Application.DoEvents()

            Next


          

            'dsJob = clsD.GetDataset("select * from tbJob A,tbjobRoute B where A._Job=B._Job and A._job='" & Trim(txtJob.Text) & "' and B._operationNo=" & Val(txtOP.Text), "tbJOB")
            'dsJobOP = clsD.GetDataset("select * from tbJobTrans where _job='" & Trim(txtJob.Text) & "' and _oper_num=" & Val(txtOP.Text), "tbJOB")
            'dsJobBF = clsD.GetDataset("select * from tbJobTrans where _job='" & Trim(txtJob.Text) & "' and _status='COMPLETED' and  _oper_num in(select max(_oper_num) from tbJobTrans where _job='" & Trim(txtJob.Text) & "' and _oper_num<" & Val(txtOP.Text) & ")", "tbJOB")
            'dsJobAF = clsD.GetDataset("select * from tbJobTrans where _job='" & Trim(txtJob.Text) & "' and _oper_num in(select min(_oper_num) from tbJobTrans where _job='" & Trim(txtJob.Text) & "' and _oper_num>" & Val(txtOP.Text) & ")", "tbJOB")

            'If dsJob.Tables(0).Rows.Count > 0 Then
            '    Dim strWC As String = dsJob.Tables(0).Rows(0).Item("_WC")
            '    If dsJobOP.Tables(0).Rows.Count > 0 Then
            '        MsgBox("This operation no. in transaction table")
            '        Exit Sub
            '    End If
            '    If dsJobBF.Tables(0).Rows.Count > 0 Then
            '        Dim i As Integer
            '        Dim Rlsqty As Double = 0
            '        Dim opQty As Double = 0
            '        Dim CompQty As Double = 0
            '        Dim OldOPNo As Integer
            '        Dim sterr As String = ""
            '        For i = 0 To dsJobBF.Tables(0).Rows.Count - 1
            '            Dim drN As DataRow()
            '            Dim strBool As Boolean = False
            '            If dsJobAF.Tables(0).Rows.Count > 0 Then
            '                drN = dsJobAF.Tables(0).Select("_jobsuffix='" & dsJobBF.Tables(0).Rows(i).Item("_jobsuffix") & "'")
            '                strBool = True
            '            End If
            '            If strBool = True Then
            '                With com
            '                    Try
            '                        OldOPNo = dsJobBF.Tables(0).Rows(i).Item("_oper_num")
            '                        .Connection = cn
            '                        .CommandType = CommandType.StoredProcedure
            '                        .CommandText = "sp_EditData_insertOP_zanden"
            '                        parm = .Parameters.Add("@Job", SqlDbType.VarChar, 50)
            '                        parm.Value = Trim(txtJob.Text)
            '                        parm = .Parameters.Add("@Opold", SqlDbType.Int)
            '                        parm.Value = dsJobBF.Tables(0).Rows(i).Item("_oper_num")
            '                        parm = .Parameters.Add("@OpNew", SqlDbType.Int)
            '                        parm.Value = Trim(txtOP.Text)
            '                        parm = .Parameters.Add("@WC", SqlDbType.VarChar, 100)
            '                        parm.Value = strWC
            '                        parm = .Parameters.Add("@SF", SqlDbType.VarChar, 10)
            '                        parm.Value = dsJobBF.Tables(0).Rows(i).Item("_jobsuffix")
            '                        parm = .Parameters.Add("@Status", SqlDbType.VarChar, 10)
            '                        parm.Value = "COMPLETED"
            '                        cn.Open()
            '                        CompQty = opQty + dsJobBF.Tables(0).Rows(i).Item("_qty_complete")


            '                        .ExecuteNonQuery()
            '                        sterr = "UP"
            '                        cn.Close()
            '                        ' MsgBox("Job deleted")
            '                    Catch ex As Exception
            '                        MsgBox(ex.Message)
            '                    Finally
            '                        cn.Close()
            '                        com.Parameters.Clear()
            '                    End Try
            '                End With
            '            Else
            '                With com
            '                    Try
            '                        .Connection = cn
            '                        .CommandType = CommandType.StoredProcedure
            '                        .CommandText = "sp_EditData_insertOP_zanden"
            '                        parm = .Parameters.Add("@Job", SqlDbType.VarChar, 50)
            '                        parm.Value = Trim(txtJob.Text)
            '                        parm = .Parameters.Add("@Opold", SqlDbType.Int)
            '                        parm.Value = dsJobBF.Tables(0).Rows(i).Item("_oper_num")
            '                        parm = .Parameters.Add("@OpNew", SqlDbType.Int)
            '                        parm.Value = Trim(txtOP.Text)
            '                        parm = .Parameters.Add("@WC", SqlDbType.VarChar, 100)
            '                        parm.Value = strWC
            '                        parm = .Parameters.Add("@SF", SqlDbType.VarChar, 10)
            '                        parm.Value = dsJobBF.Tables(0).Rows(i).Item("_jobsuffix")
            '                        parm = .Parameters.Add("@Status", SqlDbType.VarChar, 10)
            '                        parm.Value = "New"
            '                        cn.Open()
            '                        .ExecuteNonQuery()
            '                        cn.Close()
            '                        sterr = "UP"
            '                        '  MsgBox("Job deleted")
            '                    Catch ex As Exception
            '                        MsgBox(ex.Message)
            '                    Finally
            '                        cn.Close()
            '                        com.Parameters.Clear()
            '                    End Try
            '                End With


            '            End If

            '        Next

            '        If CompQty <> 0 Then

            '            With com
            '                Try
            '                    .Connection = cn
            '                    .CommandType = CommandType.StoredProcedure
            '                    .CommandText = "sp_EditData_insertOP_Main_zanden"
            '                    parm = .Parameters.Add("@Job", SqlDbType.VarChar, 50)
            '                    parm.Value = Trim(txtJob.Text)
            '                    parm = .Parameters.Add("@Opold", SqlDbType.Int)
            '                    parm.Value = OldOPNo
            '                    parm = .Parameters.Add("@OpNew", SqlDbType.Int)
            '                    parm.Value = Trim(txtOP.Text)
            '                    parm = .Parameters.Add("@WC", SqlDbType.VarChar, 100)
            '                    parm.Value = strWC

            '                    parm = .Parameters.Add("@Completed", SqlDbType.Float)
            '                    parm.Value = CompQty
            '                    cn.Open()
            '                    .ExecuteNonQuery()
            '                    cn.Close()
            '                    ' MsgBox("Job deleted")
            '                Catch ex As Exception
            '                    MsgBox(ex.Message)
            '                Finally
            '                    cn.Close()
            '                    com.Parameters.Clear()
            '                End Try
            '            End With
            '            If sterr <> "" Then
            '                MsgBox("Updated")
            '            End If
            '            'sp_EditData_insertOP_Main_zanden
            '        End If
            '    Else
            '        MsgBox("there is no op no.")
            '    End If


            'Else
            '    MsgBox("there is no op no.")
            'End If


        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
    End Sub
End Class