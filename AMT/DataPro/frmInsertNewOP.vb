Imports System.Data.SqlClient
Public Class frmInsertNewOP
    Dim dr As SqlDataReader
    Dim parm As SqlParameter
    Dim clsD As New clsMain
    Private Sub frmInsertNewOP_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub butFind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butFind.Click
        If Trim(txtJob.Text) = "" Then
            MsgBox("Enter the Job order")
            Exit Sub
        End If
        If Trim(txtOP.Text) = "" Then
            MsgBox("Enter the operation no.")
            Exit Sub
        End If
        Try

            Dim dsJob As New DataSet
            Dim dsJobOP As New DataSet
            Dim dsJobAF As New DataSet
            Dim dsJobBF As New DataSet

            dsJob = clsD.GetDataset("select * from tbJob A,tbjobRoute B where A._Job=B._Job and A._job='" & Trim(txtJob.Text) & "' and B._operationNo=" & Val(txtOP.Text), "tbJOB")
            dsJobOP = clsD.GetDataset("select * from tbJobTrans where _job='" & Trim(txtJob.Text) & "' and _oper_num=" & Val(txtOP.Text), "tbJOB")
            dsJobBF = clsD.GetDataset("select * from tbJobTrans where _job='" & Trim(txtJob.Text) & "' and _status='COMPLETED' and  _oper_num in(select max(_oper_num) from tbJobTrans where _job='" & Trim(txtJob.Text) & "' and _oper_num<" & Val(txtOP.Text) & ")", "tbJOB")
            dsJobAF = clsD.GetDataset("select * from tbJobTrans where _job='" & Trim(txtJob.Text) & "' and _oper_num in(select min(_oper_num) from tbJobTrans where _job='" & Trim(txtJob.Text) & "' and _oper_num>" & Val(txtOP.Text) & ")", "tbJOB")

            If dsJob.Tables(0).Rows.Count > 0 Then
                Dim strWC As String = dsJob.Tables(0).Rows(0).Item("_WC")
                If dsJobOP.Tables(0).Rows.Count > 0 Then
                    MsgBox("This operation no. in transaction table")
                    Exit Sub
                End If
                If dsJobBF.Tables(0).Rows.Count > 0 Then
                    Dim i As Integer
                    Dim Rlsqty As Double = 0
                    Dim opQty As Double = 0
                    Dim CompQty As Double = 0
                    Dim OldOPNo As Integer
                    Dim sterr As String = ""
                    For i = 0 To dsJobBF.Tables(0).Rows.Count - 1
                        Dim drN As DataRow()
                        Dim strBool As Boolean = False
                        If dsJobAF.Tables(0).Rows.Count > 0 Then
                            drN = dsJobAF.Tables(0).Select("_jobsuffix='" & dsJobBF.Tables(0).Rows(i).Item("_jobsuffix") & "'")
                            strBool = True
                        End If
                        If strBool = True Then
                            With com
                                Try
                                    OldOPNo = dsJobBF.Tables(0).Rows(i).Item("_oper_num")
                                    .Connection = cn
                                    .CommandType = CommandType.StoredProcedure
                                    .CommandText = "sp_EditData_insertOP_zanden"
                                    parm = .Parameters.Add("@Job", SqlDbType.VarChar, 50)
                                    parm.Value = Trim(txtJob.Text)
                                    parm = .Parameters.Add("@Opold", SqlDbType.Int)
                                    parm.Value = dsJobBF.Tables(0).Rows(i).Item("_oper_num")
                                    parm = .Parameters.Add("@OpNew", SqlDbType.Int)
                                    parm.Value = Trim(txtOP.Text)
                                    parm = .Parameters.Add("@WC", SqlDbType.VarChar, 100)
                                    parm.Value = strWC
                                    parm = .Parameters.Add("@SF", SqlDbType.VarChar, 10)
                                    parm.Value = dsJobBF.Tables(0).Rows(i).Item("_jobsuffix")
                                    parm = .Parameters.Add("@Status", SqlDbType.VarChar, 10)
                                    parm.Value = "COMPLETED"
                                    cn.Open()
                                    CompQty = opQty + dsJobBF.Tables(0).Rows(i).Item("_qty_complete")


                                    .ExecuteNonQuery()
                                    sterr = "UP"
                                    cn.Close()
                                    ' MsgBox("Job deleted")
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                Finally
                                    cn.Close()
                                    com.Parameters.Clear()
                                End Try
                            End With
                        Else
                            With com
                                Try
                                    .Connection = cn
                                    .CommandType = CommandType.StoredProcedure
                                    .CommandText = "sp_EditData_insertOP_zanden"
                                    parm = .Parameters.Add("@Job", SqlDbType.VarChar, 50)
                                    parm.Value = Trim(txtJob.Text)
                                    parm = .Parameters.Add("@Opold", SqlDbType.Int)
                                    parm.Value = dsJobBF.Tables(0).Rows(i).Item("_oper_num")
                                    parm = .Parameters.Add("@OpNew", SqlDbType.Int)
                                    parm.Value = Trim(txtOP.Text)
                                    parm = .Parameters.Add("@WC", SqlDbType.VarChar, 100)
                                    parm.Value = strWC
                                    parm = .Parameters.Add("@SF", SqlDbType.VarChar, 10)
                                    parm.Value = dsJobBF.Tables(0).Rows(i).Item("_jobsuffix")
                                    parm = .Parameters.Add("@Status", SqlDbType.VarChar, 10)
                                    parm.Value = "New"
                                    cn.Open()
                                    .ExecuteNonQuery()
                                    cn.Close()
                                    sterr = "UP"
                                    '  MsgBox("Job deleted")
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                Finally
                                    cn.Close()
                                    com.Parameters.Clear()
                                End Try
                            End With


                        End If

                    Next

                    If CompQty <> 0 Then

                        With com
                            Try
                                .Connection = cn
                                .CommandType = CommandType.StoredProcedure
                                .CommandText = "sp_EditData_insertOP_Main_zanden"
                                parm = .Parameters.Add("@Job", SqlDbType.VarChar, 50)
                                parm.Value = Trim(txtJob.Text)
                                parm = .Parameters.Add("@Opold", SqlDbType.Int)
                                parm.Value = OldOPNo
                                parm = .Parameters.Add("@OpNew", SqlDbType.Int)
                                parm.Value = Trim(txtOP.Text)
                                parm = .Parameters.Add("@WC", SqlDbType.VarChar, 100)
                                parm.Value = strWC

                                parm = .Parameters.Add("@Completed", SqlDbType.Float)
                                parm.Value = CompQty
                                cn.Open()
                                .ExecuteNonQuery()
                                cn.Close()
                                ' MsgBox("Job deleted")
                            Catch ex As Exception
                                MsgBox(ex.Message)
                            Finally
                                cn.Close()
                                com.Parameters.Clear()
                            End Try
                        End With
                        If sterr <> "" Then
                            MsgBox("Updated")
                        End If
                        'sp_EditData_insertOP_Main_zanden
                    End If
                Else
                    MsgBox("there is no op no.")
                End If


            Else
                MsgBox("there is no op no.")
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub
End Class