Public Class frmDeleteDuplicateJob
    Dim clsD As New clsMain
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub frmDeleteDuplicateJob_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        With lstv
            .Columns.Add(New ColHeader("Trans No.", 0, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("OP No.", 0, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Suf", 225, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("OP Qty", lstv.Width - 295, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Com qty", 50, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Rej qty", 50, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Status", 50, HorizontalAlignment.Left, True))

        End With
    End Sub

    Private Sub butFind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butFind.Click
        If Trim(txtJob.Text) = "" Then
            MsgBox("Enter the job no.")
            Exit Sub
        End If
        Try
            Dim dsJob As New DataSet
            dsJob = clsD.GetDataset("select * from tbJobTrans where _job='" & Trim(txtJob.Text) & "'", "tbJOB")
            Dim i As Integer
            If dsJob.Tables(0).Rows.Count > 0 Then
                For i = 0 To dsJob.Tables(0).Rows.Count - 1
                    With dsJob.Tables(0).Rows(i)
                        Dim ls As New ListViewItem(Trim(.Item("_tansnum")))    ' you can also use reader.GetSqlValue(0) 
                        ls.SubItems.Add(Trim(.Item("_job")))
                        ls.SubItems.Add(Trim(.Item("_jobsuffix")))
                        ls.SubItems.Add(Trim(.Item("_oper_num")))
                        ls.SubItems.Add(Trim(.Item("_qty_op_qty")))
                        ls.SubItems.Add(Trim(.Item("_qty_complete")))
                        ls.SubItems.Add(Trim(.Item("_qty_scrapped")))
                        ls.SubItems.Add(Trim(.Item("_status")))
                        lstv.Items.Add(ls)
                    End With
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSuf.TextChanged

    End Sub

    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click
        txtTrans.Text = lstv.SelectedItems(0).Text
        txtJobNo.Text = lstv.SelectedItems(0).SubItems(1).Text
        txtSuf.Text = lstv.SelectedItems(0).SubItems(2).Text
        txtOPNo.Text = lstv.SelectedItems(0).SubItems(3).Text
        txtOPQty.Text = lstv.SelectedItems(0).SubItems(4).Text
        txtComqty.Text = lstv.SelectedItems(0).SubItems(5).Text
        txtRejected.Text = lstv.SelectedItems(0).SubItems(6).Text
        txtStatus.Text = lstv.SelectedItems(0).SubItems(7).Text

    End Sub
End Class