Imports System.Data.SqlClient
Public Class FrmDeleteJoborder
    Dim dr As SqlDataReader
    Dim parm As SqlParameter
    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(txtJob.Text) = "" Then
            MsgBox("Enter the Job no.")
            Exit Sub
        End If
        Dim DA As New SqlDataAdapter
        With com
            If MsgBox("do you want to delete?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Try
                    .Connection = cn
                    .CommandType = CommandType.StoredProcedure
                    .CommandText = "sp_deleteJoborder_Zanden"
                    parm = .Parameters.Add("JobNo", SqlDbType.VarChar, 50)
                    parm.Value = Trim(txtJob.Text)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    MsgBox("Job deleted")
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    cn.Close()
                    com.Parameters.Clear()
                End Try
            End If
        End With

    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub

    Private Sub FrmDeleteJoborder_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class