Imports System.Data.SqlClient
Public Class frmMachineWRK
    Dim dr As SqlDataReader
    Dim parm As SqlParameter
    Private Sub butAssSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssSingle.Click
        Dim i As Integer
        Try
            lstMachine.Sorted = True
            For i = 0 To lstMachine.SelectedItems.Count - 1
                lstGroup.Items.Add(lstMachine.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstMachine.SelectedItems.Count - 1
                lstMachine.Items.Remove(lstMachine.SelectedItems.Item(0))
            Next
            lstMachine.Refresh()
            lstGroup.Refresh()
        Catch exce As Exception
            MsgBox(exce.ToString)
        End Try
    End Sub

    Private Sub butAssMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssMulti.Click
        Dim i As Integer
        Try
            lstMachine.Sorted = True
            For i = 0 To lstMachine.Items.Count - 1
                lstGroup.Items.Add(lstMachine.Items(i))
            Next
            i = 0
            For i = 0 To lstMachine.Items.Count - 1
                lstMachine.Items.Remove(lstMachine.Items(0))
            Next
            lstMachine.Refresh()
            lstGroup.Refresh()
        Catch exce As Exception
            MsgBox(exce.ToString)
        End Try
    End Sub

    Private Sub butDelSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelSingle.Click
        Dim i As Integer
        Try
            lstMachine.Sorted = True
            For i = 0 To lstGroup.SelectedItems.Count - 1
                lstMachine.Items.Add(lstGroup.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstGroup.SelectedItems.Count - 1
                lstGroup.Items.Remove(lstGroup.SelectedItems.Item(0))
            Next
            lstMachine.Refresh()
            lstGroup.Refresh()
        Catch exce As Exception
            MsgBox(exce.ToString)
        End Try
    End Sub
    Private Sub butDelMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelMulti.Click
        Dim i As Integer
        Try
            lstMachine.Sorted = True
            For i = 0 To lstGroup.Items.Count - 1
                lstMachine.Items.Add(lstGroup.Items(i))
            Next
            i = 0
            For i = 0 To lstGroup.Items.Count - 1
                lstGroup.Items.Remove(lstGroup.Items(0))
            Next
            lstMachine.Refresh()
            lstGroup.Refresh()
        Catch exce As Exception
            MsgBox(exce.ToString)
        End Try
    End Sub

    Private Sub frmMachineWRK_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadWk()
        CallMachine()
    End Sub

    Sub LoadWk()
        lstwrk.Items.Clear()

        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                'Dim DI As DataItem
                .CommandText = "select _wc,_description  from tbwc order by _description"
                cn.Open()
                dr = .ExecuteReader(CommandBehavior.CloseConnection)
                While dr.Read
                    lstwrk.Items.Add(New DataItem(dr.Item("_wc") & " || " & dr.Item("_description"), dr.Item("_wc") & " - " & dr.Item("_description")))
                    ' lstwrk.Items.Add(dr.Item("_wc") & " - " & dr.Item("_description"))
                End While
                cn.Close()
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cn.Close()
        End Try

    End Sub

    Private Sub lstwrk_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstwrk.SelectedIndexChanged
        txtwcID.Text = ""
        txtDesc.Text = ""
        lstMachine.Items.Clear()
        lstGroup.Items.Clear()
        Try
            If lstwrk.SelectedIndex >= 0 Then
                Dim id As DataItem = lstwrk.SelectedItem
                Dim S() As String = Split(id.ID, "||")
                txtwcID.Text = S(0)
                txtDesc.Text = S(1)

                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    'Dim DI As DataItem
                    .CommandText = "select * from tbMachine where _mid not in(select _mid from tbMachineGroup where _wc='" & refTxt(txtwcID.Text) & "') order by _machineID "
                    cn.Open()
                    dr = .ExecuteReader(CommandBehavior.CloseConnection)
                    While dr.Read
                        lstMachine.Items.Add(New DataItem(dr.Item("_mid"), dr.Item("_machineID")))
                        '/lstwrk.Items.Add(dr.Item("_wc") & " - " & dr.Item("_description"))
                    End While
                    cn.Close()
                End With
            End If
            Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cn.Close()
        End Try

        Try
            If lstwrk.SelectedIndex >= 0 Then
                Dim id As DataItem = lstwrk.SelectedItem
                Dim S() As String = Split(id.ID, "||")
                txtwcID.Text = S(0)
                txtDesc.Text = S(1)

                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    'Dim DI As DataItem
                    .CommandText = "select * from tbMachine where _mid  in(select _mid from tbMachineGroup where _wc='" & refTxt(txtwcID.Text) & "') order by _machineID "
                    cn.Open()
                    dr = .ExecuteReader(CommandBehavior.CloseConnection)
                    While dr.Read
                        lstGroup.Items.Add(New DataItem(dr.Item("_mid"), dr.Item("_machineID")))
                        '/lstwrk.Items.Add(dr.Item("_wc") & " - " & dr.Item("_description"))
                    End While
                    cn.Close()
                End With
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cn.Close()
        End Try


    End Sub
    Function refTxt(ByVal txt As String) As String
        Return Trim(Replace(txt, "'", "''"))
    End Function
    Sub CallMachine()

        lstMachine.Items.Clear()
        lstGroup.Items.Clear()

        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                'Dim DI As DataItem
                .CommandText = "select * from tbMachine order by _machineID"
                cn.Open()
                dr = .ExecuteReader(CommandBehavior.CloseConnection)
                While dr.Read
                    lstMachine.Items.Add(New DataItem(dr.Item("_mid"), dr.Item("_machineID")))
                    '/lstwrk.Items.Add(dr.Item("_wc") & " - " & dr.Item("_description"))
                End While
                cn.Close()
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cn.Close()
        End Try


    End Sub

    'Private Sub lstMachine_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstMachine.Click
    '    Try
    '        Dim id As DataItem
    '        id = lstMachine.SelectedItem
    '        MsgBox(id.ID)
    '        MsgBox(id.Value)
    '    Catch ex As Exception
    '    End Try
    'End Sub

    'Private Sub lstMachine_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstMachine.SelectedIndexChanged

    'End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click

    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(txtwcID.Text) = "" Then
            MsgBox("select the work center id!", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "delete from  tbMachinegroup  where _wc ='" & refTxt(txtwcID.Text) & "'"
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                'MsgBox("Successfully updated!", MsgBoxStyle.Information)
            End With
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        Dim i As Integer
        Dim st As Integer = 1
        If lstGroup.Items.Count > 0 Then

            For i = 0 To lstGroup.Items.Count - 1




                Try
                    With com
                        .Connection = cn
                        .CommandType = CommandType.Text
                        .CommandText = "insert into tbMachinegroup(_wc,_Mid)values(@wc,@Mid)"
                        parm = .Parameters.Add("@wc", SqlDbType.VarChar, 50)
                        parm.Value = refTxt(txtwcID.Text)
                        parm = .Parameters.Add("@Mid", SqlDbType.Int)
                        Dim id As DataItem = lstGroup.Items(i)
                        parm.Value = Val(id.ID)

                        cn.Open()
                        .ExecuteNonQuery()
                        cn.Close()
                        com.Parameters.Clear()
                        If st = 0 Then
                            st = 1
                        End If
                    End With
                Catch ex As SqlException
                    MsgBox(ex.Message, MsgBoxStyle.Critical)
                    st = 2
                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.Critical)
                    st = 2
                Finally
                    cn.Close()
                    com.Parameters.Clear()
                End Try





            Next
            If st = 1 Then
                MsgBox("Successfully updated!", MsgBoxStyle.Information)
            End If
        End If
        txtwcID.Text = ""
        txtDesc.Text = ""


        LoadWk()
        CallMachine()
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        txtwcID.Text = ""
        txtDesc.Text = ""
        LoadWk()
        CallMachine()
    End Sub


End Class