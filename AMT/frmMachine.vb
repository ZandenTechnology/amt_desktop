Imports System.Data.SqlClient
Public Class frmMachine
    Dim dr As SqlDataReader
    Dim parm As SqlParameter
    Private Sub frmMachine_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With lstv
            .Columns.Add("MNo", 0, HorizontalAlignment.Left)
            .Columns.Add("Machine ID", 230, HorizontalAlignment.Left)
            .Columns.Add("Description", lstv.Width - 230, HorizontalAlignment.Left)
            list_Displaydata()

        End With
    End Sub
    Sub list_Displaydata()
        lstv.Items.Clear()
        Try
            com.Connection = cn
            com.CommandType = CommandType.Text
            com.CommandText = "select _mid,_machineID,_MachineDesc from tbMachine order by _machineID"
            'parm = com.Parameters.Add("@Type", SqlDbType.VarChar, 256)
            'parm.Value = refTxt(cmbType.Text)
            cn.Open()
            dr = com.ExecuteReader(CommandBehavior.CloseConnection)
            While dr.Read
                Dim ls As New ListViewItem(Trim(dr.Item("_mid")))    ' you can also use reader.GetSqlValue(0) 
                ls.SubItems.Add(Trim(dr.Item("_machineID")))
                ls.SubItems.Add(Trim(dr.Item("_MachineDesc")))
                lstv.Items.Add(ls)
            End While
        Catch exce As Exception
            MsgBox(exce.ToString)
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click
        txtMIDNo.Text = lstv.SelectedItems(0).Text
        txtMachineid.Text = lstv.SelectedItems(0).SubItems(1).Text
        txtDesc.Text = lstv.SelectedItems(0).SubItems(2).Text
        butSave.Enabled = False
        butUpdate.Enabled = True
        butDelete.Enabled = True
        ' txtUserCode.ReadOnly = True
        'StbUser.Text = "You Can Edit And Delete This Records"
    End Sub

   
    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(txtMachineid.Text) = "" Then
            MsgBox("Enter the machine id!", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "insert into tbMachine( _machineID,_MachineDesc)values(@machineID,@MachineDesc)"
                parm = .Parameters.Add("@machineID", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtMachineid.Text)
                parm = .Parameters.Add("@MachineDesc", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtDesc.Text)
                'parm = .Parameters.Add("@Type", SqlDbType.VarChar, 256)
                'parm.Value = refTxt(cmbType.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully saved!", MsgBoxStyle.Information)
            End With
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub
    Sub textReset()
        txtMIDNo.Clear()
        txtMachineid.Clear()
        txtDesc.Clear()
        butSave.Enabled = True
        butDelete.Enabled = False
        butUpdate.Enabled = False
    End Sub
    Function refTxt(ByVal txt As String) As String
        Return Trim(Replace(txt, "'", "''"))
    End Function

    Private Sub butUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdate.Click
        If Trim(txtMachineid.Text) = "" Then
            MsgBox("Enter the machine id!", MsgBoxStyle.Information)
            Exit Sub
        End If
        
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "Update  tbMachine set _machineID=@machineID,_MachineDesc=@MachineDesc where _mid =@mid"
                parm = .Parameters.Add("@machineID", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtMachineid.Text)
                parm = .Parameters.Add("@MachineDesc", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtDesc.Text)
                parm = .Parameters.Add("@mid ", SqlDbType.Int)
                parm.Value = Val(txtMIDNo.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully updated!", MsgBoxStyle.Information)
            End With
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        Dim result As New DialogResult
        result = MessageBox.Show("Do you Want to delete this Records?", "Caption", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = Windows.Forms.DialogResult.Yes Then
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "delete from   tbMachine  where _mid =@mid "
                    parm = .Parameters.Add("@mid ", SqlDbType.Int)
                    parm.Value = Val(txtMIDNo.Text)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()
                    textReset()
                    MsgBox("Successfully deleted!", MsgBoxStyle.Information)
                End With
            Catch ex As SqlException
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            Finally
                cn.Close()
                com.Parameters.Clear()
            End Try
            list_Displaydata()
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        textReset()
    End Sub

    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub
End Class