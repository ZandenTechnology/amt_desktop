Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Public Class frmFailureAnalysis
    Dim clsM As New clsMain
    Public DSItem As New DataSet
    Public DSWC As New DataSet
    Dim dsrsg As DSReport
    Dim strReportDate As String
    Private Sub frmFailureAnalysis_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dtpFrom.Value = Now
        dtpTo.Value = Now
        txtFrom.Text = Format(Now, "dd/MM/yyyy")
        txtTo.Text = Format(Now, "dd/MM/yyyy")
    End Sub
    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If rdbWC.Checked = False Then
            Try
                If Trim(txtFrom.Text) = "" Or Trim(txtTo.Text) = "" Then
                    MsgBox("Please enter the date!", MsgBoxStyle.Information, "eWIP")
                    Exit Sub
                End If
                Dim dblSd As Double = clsM.CheckDate(txtFrom.Text)
                If dblSd = 0 Then
                    MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                    Exit Sub
                End If

                Dim dblEd As Double = clsM.CheckDate(txtTo.Text)
                If dblEd = 0 Then
                    MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                    Exit Sub
                End If
                Dim s() As String = Split(txtTo.Text, "/")

                Dim Dateto As Date = DateSerial(s(2), s(1), s(0))
                Dateto = DateAdd(DateInterval.Minute, -1, DateAdd(DateInterval.Day, 1, Dateto))
                dblEd = 0
                dblEd = Dateto.ToOADate

                Dim HASRe As New Hashtable
                Dim HAsCode As New Hashtable
                Dim StItem As Boolean
                If lstSelItem.Items.Count > 0 Then
                    Dim k As Integer
                    For k = 0 To lstSelItem.Items.Count - 1
                        HAsCode.Add(lstSelItem.Items(k).id, lstSelItem.Items(k).id)
                    Next
                Else
                    StItem = True
                    HAsCode.Add("ALL", "ALL")
                End If

                strReportDate = Format(DateTime.FromOADate(dblSd), "dd MMM yyyy") & " - " & Format(DateTime.FromOADate(dblEd), "dd MMM yyyy") 'txtFrom.Text & "-" & txtTo.Text
                Dim CreatedDate As String = Format(Now, "dd MMM yyyy")

                Dim DSrep As New DataSet
                Dim Strsql As String
                Dim strResource As String
                Dim DSRecGroup As New DataSet
                Dim DSItem As New DataSet
                DSItem = clsM.GetDataset("select * from tbItem", "tbItem")
                DSRecGroup = clsM.GetDataset("select A.*,B.* from tbWC A,tbResourceGroup B,tbWorkStation C where C._wc=A._wc and C._rid=B._rid", "tbRec")
                Strsql = "select * from tbJobTransMain where _end_Date between " & dblSd & " and " & dblEd & " and _qty_scrapped<>0"

                DSrep = clsM.GetDataset(Strsql, "tbJobTransMain")
                dsrsg = New DSReport
                Dim totqty As Double
                Dim drRp As DataRow
                Dim recount As Boolean = False
                PB1.Value = 0
                PB1.Minimum = 0
                PB1.Maximum = 0

                Dim ArrItemName(0) As String
                Dim ArrTotalRece(0) As String
                Dim ArrTotalRej(0) As String
                Dim HasItem As New Hashtable
                Dim stUp As Boolean = False
                If DSrep.Tables(0).Rows.Count > 0 Then
                    Dim i As Integer
                    PB1.Maximum = DSrep.Tables(0).Rows.Count




                    Dim j As Integer = 0
                    stUp = True
                    For i = 0 To DSrep.Tables(0).Rows.Count - 1

                        With DSrep.Tables(0).Rows(i)

                            ' Dim StItem As Boolean = False
                            Dim runt As Boolean = False
                            Dim ArrItem As String() = Split(.Item("_item"), "-")
                            Dim strCont As String = ArrItem(0)
                            If UCase(Mid(.Item("_item"), 1, 2)) = "SC" Then
                                strCont = .Item("_item")
                            End If

                            'If ArrItem.Length > 1 Then
                            '    MsgBox(.Item("_item"))
                            'End If
                            If StItem = True Then
                                runt = True
                            ElseIf HAsCode.ContainsKey(strCont) = True Then
                                runt = True
                            Else
                                runt = False
                            End If


                            If runt = True Then

                                Dim DRItem() As DataRow = DSItem.Tables(0).Select("_itemCode='" & .Item("_item") & "'")

                                Dim StrItemName As String
                                If DRItem.Length > 0 Then
                                    StrItemName = DRItem(0).Item("_itemdescription")
                                End If
                                Dim IName As String = ""

                                If StrItemName <> "" Then
                                    Dim StrArr() As String = Split(StrItemName, ",")
                                    IName = StrArr(0)
                                Else
                                    IName = .Item("_item")
                                End If


                                If HASRe.ContainsKey(IName) = False Then
                                    ReDim Preserve ArrItemName(j)
                                    ReDim Preserve ArrTotalRece(j)
                                    ReDim Preserve ArrTotalRej(j)
                                    ArrItemName(j) = IName
                                    ArrTotalRece(j) = .Item("_qty_op_qty")
                                    ArrTotalRej(j) = .Item("_qty_scrapped")
                                    HASRe.Add(IName, j)
                                    j = j + 1
                                Else
                                    Dim m As Integer
                                    m = HASRe(.Item("_item"))
                                    ArrTotalRece(m) = ArrTotalRece(m) + .Item("_qty_op_qty")
                                    ArrTotalRej(m) = ArrTotalRej(m) + .Item("_qty_scrapped")
                                End If
                            End If
                        End With
                        PB1.Value = i + 1
                        Application.DoEvents()

                    Next
                End If

                If stUp = True Then
                    If UBound(ArrItemName) > -1 Then
                        Dim j As Integer
                        For j = 0 To UBound(ArrItemName)
                            drRp = dsrsg.Tables("tbFailure").NewRow
                            drRp("sno") = j + 1
                            drRp("ColName") = "FG Description"
                            drRp("Title") = Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                            drRp("CreateDate") = CreatedDate
                            drRp("ReportDate") = strReportDate
                            drRp("WC") = ArrItemName(j)
                            drRp("failureQty") = ArrTotalRej(j)
                            If ArrTotalRece(j) <= 0 Then
                                drRp("Per") = 0
                            Else
                                drRp("Per") = (ArrTotalRej(j) * 100) / ArrTotalRece(j)
                            End If
                            dsrsg.Tables("tbFailure").Rows.Add(drRp)
                        Next
                    End If
                Else

                    drRp = dsrsg.Tables("tbFailure").NewRow
                    drRp("sno") = 0
                    drRp("ColName") = "FG Description"
                    drRp("Title") = strReportDate 'Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                    drRp("CreateDate") = CreatedDate
                    drRp("ReportDate") = strReportDate
                    drRp("WC") = ""
                    drRp("failureQty") = 0
                    drRp("Per") = 0

                    dsrsg.Tables("tbFailure").Rows.Add(drRp)






                End If


                butClose.Visible = True
                Dim cry As New cryFailureAnalysis
                cry.SetDataSource(dsrsg)
                RViewer.ReportSource = cry
                RViewer.Refresh()
                Panel1.Visible = False
                RViewer.Visible = True


            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End Try

        Else
            Try
                If Trim(txtFrom.Text) = "" Or Trim(txtTo.Text) = "" Then
                    MsgBox("Please enter the date!", MsgBoxStyle.Information, "eWIP")
                    Exit Sub
                End If
                Dim dblSd As Double = clsM.CheckDate(txtFrom.Text)
                If dblSd = 0 Then
                    MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                    Exit Sub
                End If

                Dim dblEd As Double = clsM.CheckDate(txtTo.Text)
                If dblEd = 0 Then
                    MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                    Exit Sub
                End If
                Dim s() As String = Split(txtTo.Text, "/")

                Dim Dateto As Date = DateSerial(s(2), s(1), s(0))
                Dateto = DateAdd(DateInterval.Minute, -1, DateAdd(DateInterval.Day, 1, Dateto))
                dblEd = 0
                dblEd = Dateto.ToOADate
                Dim StItem As Boolean = False
                Dim HAsCode As New Hashtable
                If lstSelItem.Items.Count > 0 Then
                    Dim k As Integer
                    For k = 0 To lstSelItem.Items.Count - 1
                        HAsCode.Add(lstSelItem.Items(k).id, lstSelItem.Items(k).id)
                    Next
                Else
                    StItem = True
                    HAsCode.Add("ALL", "ALL")

                End If






                Dim HASRe As New Hashtable


                strReportDate = Format(DateTime.FromOADate(dblSd), "dd MMM yyyy") & " - " & Format(DateTime.FromOADate(dblEd), "dd MMM yyyy") 'txtFrom.Text & "-" & txtTo.Text
                ' Dim strReportDate As String = txtFrom.Text & "-" & txtTo.Text
                Dim CreatedDate As String = Format(Now, "dd MMM yyyy")

                Dim DSrep As New DataSet
                Dim Strsql As String
                Dim strResource As String
                Dim DSRecGroup As New DataSet
                Dim DSItem As New DataSet
                'DSItem = clsM.GetDataset("select * from tbItem", "tbItem")
                DSRecGroup = clsM.GetDataset("select * from tbWC", "tbRec")
                Strsql = "select * from tbJobTransMain where _end_Date between " & dblSd & " and " & dblEd & " and _qty_scrapped<>0"

                DSrep = clsM.GetDataset(Strsql, "tbJobTransMain")
                dsrsg = New DSReport
                Dim totqty As Double
                Dim drRp As DataRow
                Dim recount As Boolean = False
                PB1.Value = 0
                PB1.Minimum = 0
                PB1.Maximum = 0

                Dim ArrItemName(0) As String
                Dim ArrTotalRece(0) As String
                Dim ArrTotalRej(0) As String
                Dim HasItem As New Hashtable
                Dim stUp As Boolean = False
                If DSrep.Tables(0).Rows.Count > 0 Then
                    Dim i As Integer
                    PB1.Maximum = DSrep.Tables(0).Rows.Count
                    Dim j As Integer = 0
                    stUp = True
                    For i = 0 To DSrep.Tables(0).Rows.Count - 1

                        With DSrep.Tables(0).Rows(i)


                            Dim DRWC() As DataRow = DSRecGroup.Tables(0).Select("_WC='" & .Item("_WC") & "'")

                            Dim StrItemName As String
                            Dim Iname As String
                            If DRWC.Length > 0 Then
                                Iname = DRWC(0).Item("_description")
                            Else
                                Iname = .Item("_WC")
                            End If



                            If HASRe.ContainsKey(Iname) = False Then
                                ReDim Preserve ArrItemName(j)
                                ReDim Preserve ArrTotalRece(j)
                                ReDim Preserve ArrTotalRej(j)
                                ArrItemName(j) = Iname
                                ArrTotalRece(j) = .Item("_qty_op_qty")
                                ArrTotalRej(j) = .Item("_qty_scrapped")
                                HASRe.Add(Iname, j)
                                j = j + 1
                            Else
                                Dim m As Integer
                                m = HASRe(.Item("_item"))
                                ArrTotalRece(m) = ArrTotalRece(m) + .Item("_qty_op_qty")
                                ArrTotalRej(m) = ArrTotalRej(m) + .Item("_qty_scrapped")
                            End If
                        End With
                        PB1.Value = i + 1
                        Application.DoEvents()
                    Next
                End If
                If stUp = True Then
                    If UBound(ArrItemName) > -1 Then
                        Dim j As Integer
                        For j = 0 To UBound(ArrItemName)
                            drRp = dsrsg.Tables("tbFailure").NewRow
                            drRp("sno") = j + 1
                            drRp("ColName") = "Work Center"
                            drRp("Title") = Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                            drRp("CreateDate") = CreatedDate
                            drRp("ReportDate") = strReportDate
                            drRp("WC") = ArrItemName(j)
                            drRp("failureQty") = ArrTotalRej(j)
                            If ArrTotalRece(j) <= 0 Then
                                drRp("Per") = 0
                            Else
                                drRp("Per") = (ArrTotalRej(j) * 100) / ArrTotalRece(j)
                            End If
                            dsrsg.Tables("tbFailure").Rows.Add(drRp)
                        Next
                    End If
                Else

                    drRp = dsrsg.Tables("tbFailure").NewRow
                    drRp("sno") = 0
                    drRp("ColName") = "Work Center"
                    drRp("Title") = strReportDate ' Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                    drRp("CreateDate") = CreatedDate
                    drRp("ReportDate") = strReportDate
                    drRp("WC") = ""
                    drRp("failureQty") = 0
                    drRp("Per") = 0

                    dsrsg.Tables("tbFailure").Rows.Add(drRp)

                End If
                Dim cry As New cryFailure
                cry.SetDataSource(dsrsg)
                RViewer.ReportSource = cry
                RViewer.Refresh()
                Panel1.Visible = False
                butClose.Visible = True
                butExport.Visible = True
                RViewer.Visible = True

            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End Try
        End If
        TmVari = 2
    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub

    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtTo.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        dtpTo.Checked = False
    End Sub
    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtFrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False
    End Sub

    'Private Sub RViewer_ClickPage(ByVal sender As Object, ByVal e As CrystalDecisions.Windows.Forms.PageMouseEventArgs) Handles RViewer.ClickPage
    '    '  Me.RViewer.TopLevelControl.Focus()
    '    ' Me.WindowState = FormWindowState.Maximized


    '    ' RViewer.m()
    '    TmVari = 2
    'End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        RViewer.DataBindings.Clear()
        GC.GetGeneration(RViewer)
        RViewer.Visible = False
        butClose.Visible = False
        butExport.Visible = False
        PB1.Value = 0
        PB1.Minimum = 0
        PB1.Maximum = 0
        Panel1.Visible = True
    End Sub

    Private Sub butExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExport.Click
        ExportDatatoExcel()
    End Sub
    Function ExportDatatoExcel() As Boolean
        Dim txtFile As String
        txtFile = ""
        sfdSave.FileName = ""
        sfdSave.Filter = "Excel File|*.xls"
        sfdSave.Title = "Save an Excel File"
        If sfdSave.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtFile = sfdSave.FileName
        Else
            Exit Function
        End If


        Panel3.Visible = True
        PB2.Value = 0
        PB2.Minimum = 0
        PB2.Maximum = 0
        Dim myTrans As System.Data.SqlClient.SqlTransaction

        Dim delFile As Boolean = False
        '= "C:\ZandenTemp\ComReport.xls"
        Try
            If IO.File.Exists(txtFile) Then
                IO.File.Delete(txtFile)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Function
        Finally
        End Try
        ' Dim at As Integer
        If txtFile = "" Then
            MsgBox("Please enter the file name!", MsgBoxStyle.Information, "eWIP")
            Exit Function
        End If

        Dim str, filename As String
        str = ""
        filename = ""
        Dim col, row As Integer
        col = 0
        row = 0
        Dim strHisNo As String = ""
        Dim strAcrNo As String = ""
        Dim valHis As String
        Dim strHisVal As String = ""
        Dim strCurVal As String = ""

        Dim localAll As Process()
        localAll = Process.GetProcesses()
        Dim EXhash As New Hashtable
        For i As Integer = 0 To localAll.Length - 1
            If localAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(localAll(i).Id) = False Then
                    EXhash.Add(localAll(i).Id, localAll(i).Id)
                End If
            End If
        Next

        Dim Excel As Object = CreateObject("Excel.Application")


        P1.Visible = True
        Try
            Dim i As Integer = 1
            With Excel
                .SheetsInNewWorkbook = 1
                .Workbooks.Add()
                .Worksheets(1).Select()

                'Dim chartRange As Excel.Range

                'chartRange = .Range("A1", "C1")
                'chartRange.Merge()
                'If rdbWC.Checked = True Then
                '    chartRange.FormulaR1C1 = "FAILURE ANALYSIS REPORT BY WORK CENTER"
                'Else
                '    chartRange.FormulaR1C1 = "FAILURE ANALYSIS REPORT BY ITEM"
                'End If
                'chartRange.HorizontalAlignment = 3
                'chartRange.VerticalAlignment = 3

                '.cells(2, 1).MergeCells = True
                '.cells(2, 1).EntireRow.Font.Bold = True
                '.cells(2, 1).value = "Created Date :" & Format(Now, "dd MMM yyyy")

                'chartRange = .Range("B2", "C2")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "Reported Date : " & strReportDate 'txtFrom.Text & "-" & txtTo.Text
                '' chartRange.FormulaR1C1 = "Reported Date :" & txtFrom.Text & "-" & txtTo.Text
                'chartRange.HorizontalAlignment = 3
                'chartRange.VerticalAlignment = 3


                .cells(3, 1).MergeCells = True
                .cells(3, 1).EntireRow.Font.Bold = True
                If rdbWC.Checked = True Then
                    .cells(3, 1).value = "Work Center"
                Else
                    .cells(3, 1).value = "FG Description"
                End If

                .cells(3, 2).MergeCells = True
                .cells(3, 2).EntireRow.Font.Bold = True
                .cells(3, 2).value = "Rejected Quantity"


                .cells(3, 3).MergeCells = True
                .cells(3, 3).EntireRow.Font.Bold = True
                .cells(3, 3).value = "Percentage(%)"






                i = 3

                i += 1

            End With
            Application.DoEvents()

            Dim NEDR() As DataRow = dsrsg.Tables("tbFailure").Select("WC<>'-1'", "WC")
            PB2.Value = 0
            If NEDR.Length > 0 Then
                PB2.Maximum = NEDR.Length
                Dim M1 As Integer
                For M1 = 0 To NEDR.Length - 1
                    With NEDR(M1)
                        Excel.cells(i, 1).value = .Item("WC")
                        Excel.cells(i, 2).value = .Item("failureQty")
                        Excel.cells(i, 3).value = Format(.Item("per"), "00.00")
                    End With
                    i = i + 1
                    PB2.Value = M1 + 1
                Next
            End If
            filename = txtFile
            Excel.ActiveCell.Worksheet.SaveAs(filename)
            System.Runtime.InteropServices.Marshal.ReleaseComObject(Excel)
            Excel = Nothing



            MsgBox("Data's are exported to Excel Succesfully in '" & filename & "'", MsgBoxStyle.Information, "eWIP")

            '  lblDataV.Text = "Data has been Successfully Exported"




        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "eWIP")
            delFile = True


        Finally
            Panel3.Visible = False
            cn.Close()
            com.Parameters.Clear()
        End Try

        ' The excel is created and opened for insert value. We most close this excel using this system
        'Dim pro() As Process = System.Diagnostics.Process.GetProcessesByName("EXCEL")

        'For Each i As Process In pro
        '    i.Kill()
        'Next


        Dim DellocalAll As Process()

        DellocalAll = Process.GetProcesses()


        For i As Integer = 0 To DellocalAll.Length - 1
            If DellocalAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(DellocalAll(i).Id) = False Then
                    DellocalAll(i).Kill()
                End If
            End If
        Next



        If delFile = True Then
            Try
                If IO.File.Exists(filename) Then
                    IO.File.Delete(filename)
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End Try
        End If
        txtFile = ""


    End Function

    Private Sub RViewer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RViewer.Load

    End Sub

    Private Sub rdbWC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbWC.CheckedChanged
        If rdbWC.Checked = True Then
            lblCode.Text = "WC"
            lblSelect.Text = "Selected WC"
            GetWC()
        End If
    End Sub

    Private Sub rdbItem_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbItem.CheckedChanged
        lblCode.Text = "lblCode"
        lblSelect.Text = "Selected Item Code"
        GetItem()
    End Sub
    Sub GetItem()
        DSItem.Tables.Clear()
        lstSelItem.Items.Clear()
        lstItem.Items.Clear()
        Try
            Dim ds As New DataSet
            'ds = clsM.GetDataset("select * from tbItem where  order by _itemCode", "tbItem")
            ds = clsM.GetDataset("select * from tbItem where _type<>'O' and (_itemCode like('F%') or _itemCode like('FG%') or _itemCode like('AC%') or  _itemCode like('SC%')) order by _itemCode", "tbItem")
            '
            DSItem = ds
            If IsDBNull(ds) = False Then
                Dim i As Integer
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            '  If UCase(Mid(.Item("_itemCode"), 1, 2)) <> "SC" Then
                            'Dim st As String() = Split(.Item("_itemCode"), "-")
                            'If st.Length = 1 Then
                            'lstItem.Items.Add(New DataItem(Trim(.Item("_itemCode")), Trim(.Item("_itemCode")) & "  :  " & Trim(.Item("_itemdescription")) & ""))
                            'End If
                            'Else
                            lstItem.Items.Add(New DataItem(Trim(.Item("_itemCode")), Trim(.Item("_itemCode")) & "  :  " & Trim(.Item("_itemdescription")) & ""))
                            'End If
                        End With
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try

    End Sub


    Sub GetWC()
        DSItem.Tables.Clear()
        lstSelItem.Items.Clear()
        lstItem.Items.Clear()
        Try
            Dim ds As New DataSet
            'ds = clsM.GetDataset("select * from tbItem where  order by _itemCode", "tbItem")
            ds = clsM.GetDataset("select * from tbWC order by _wc", "tbItem")
            '
            DSItem = ds
            If IsDBNull(ds) = False Then
                Dim i As Integer
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            lstItem.Items.Add(New DataItem(Trim(.Item("_wc")), Trim(.Item("_wc")) & "  :  " & Trim(.Item("_description")) & ""))
                        End With
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try

    End Sub

    Private Sub butAssSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssSingle.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstItem.SelectedItems.Count - 1
                lstSelItem.Items.Add(lstItem.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstItem.SelectedItems.Count - 1
                lstItem.Items.Remove(lstItem.SelectedItems.Item(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butAssMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssMulti.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstItem.Items.Count - 1
                lstSelItem.Items.Add(lstItem.Items(i))
            Next
            i = 0
            For i = 0 To lstItem.Items.Count - 1
                lstItem.Items.Remove(lstItem.Items(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butDelSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelSingle.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstSelItem.SelectedItems.Count - 1
                lstItem.Items.Add(lstSelItem.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstSelItem.SelectedItems.Count - 1
                lstSelItem.Items.Remove(lstSelItem.SelectedItems.Item(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub
    Private Sub butDelMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelMulti.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstSelItem.Items.Count - 1
                lstItem.Items.Add(lstSelItem.Items(i))
            Next
            i = 0
            For i = 0 To lstSelItem.Items.Count - 1
                lstSelItem.Items.Remove(lstSelItem.Items(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub
End Class