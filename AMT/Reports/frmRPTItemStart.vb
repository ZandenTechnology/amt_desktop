Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Microsoft.VisualBasic.DateAndTime
Public Class frmRPTItemStart
    Dim clsM As New clsMain
    Public DSItem As New DataSet
    Public DSWC As New DataSet
    Dim dsrsg As DSReport
    Dim ArrexJob() As String
    Dim ArrexOP() As Integer
    Dim ArrexRes() As String
    Dim ArrexItem() As String
    Dim exArr(0) As String
    Dim dblTotAll As Double
    Dim strReportDate As String
    Sub GetItem()
        DSItem.Tables.Clear()
        lstSelItem.Items.Clear()
        lstItem.Items.Clear()
        Try
            Dim ds As New DataSet
            'ds = clsM.GetDataset("select * from tbItem where  order by _itemCode", "tbItem")
            ds = clsM.GetDataset("select * from tbItem where _type<>'O' and (_itemCode like('F%') or _itemCode like('FG%') or _itemCode like('AC%') or  _itemCode like('SC%')) order by _itemCode", "tbItem")
            '
            DSItem = ds
            If IsDBNull(ds) = False Then
                Dim i As Integer
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            '  If UCase(Mid(.Item("_itemCode"), 1, 2)) <> "SC" Then
                            'Dim st As String() = Split(.Item("_itemCode"), "-")
                            'If st.Length = 1 Then
                            'lstItem.Items.Add(New DataItem(Trim(.Item("_itemCode")), Trim(.Item("_itemCode")) & "  :  " & Trim(.Item("_itemdescription")) & ""))
                            'End If
                            'Else
                            lstItem.Items.Add(New DataItem(Trim(.Item("_itemCode")), Trim(.Item("_itemCode")) & "  :  " & Trim(.Item("_itemdescription")) & ""))
                            'End If
                        End With
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try

    End Sub
    Sub comletedTransaction()
        dblTotAll = 0
        Dim strTransCont As String = ""
        If ckbTransfer.Checked = True Then
            If lblReportTit.Text <> UCase("WIP by Resource Group") Then
                strTransCont = " and _trans='Yes'"
            End If
        End If
        Try
            Dim HashNewI As New Hashtable
            If Trim(txtFrom.Text) = "" Then
                MsgBox("Please enter the date!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim dblSd As Double = clsM.CheckDate(txtFrom.Text)
            If dblSd = 0 Then
                MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            exArr = Nothing
            Dim n1 As Integer = 0
            'Dim dblEd As Double = clsM.CheckDate(txtTo.Text)
            'If dblEd = 0 Then
            '    MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
            '    Exit Sub
            'End If
            'Dim s() As String = Split(txtTo.Text, "/")
            dsrsg = New DSReport
            'Dim Dateto As Date = DateSerial(s(2), s(1), s(0))
            'Dateto = DateAdd(DateInterval.Minute, -1, DateAdd(DateInterval.Day, 1, Dateto))
            'dblEd = 0
            'dblEd = Dateto.ToOADate

            Dim HASRe As New Hashtable
            Dim StItem As Boolean = False
            If lstSelItem.Items.Count > 0 Then
                Dim k As Integer
                For k = 0 To lstSelItem.Items.Count - 1
                    HASRe.Add(lstSelItem.Items(k).id, lstSelItem.Items(k).id)
                Next
            Else
                StItem = True
                HASRe.Add("ALL", "ALL")

            End If
            strReportDate = Format(DateTime.FromOADate(dblSd), "dd MMM yyyy") '& "-" & Format(DateTime.FromOADate(dblEd), "dd MMM yyyy")
            'Dim strReportDate As String = txtFrom.Text '& "-" & txtTo.Text
            Dim CreatedDate As String = Format(Now, "dd MMM yyyy")

            Dim DSrep As New DataSet
            Dim Strsql As String
            Dim strResource As String
            Dim DSRecGroup As New DataSet
            Dim DSItem As New DataSet
            DSItem = clsM.GetDataset("select * from tbItem", "tbItem")
            Dim DSRecWC As New DataSet
            DSRecWC = clsM.GetDataset("select * from tbWC", "tbWC")
            DSRecGroup = clsM.GetDataset("select A.*,B.* from tbWC A,tbResourceGroup B,tbWorkStation C where C._wc=A._wc and C._rid=B._rid", "tbRec")

            Dim hashFindUnique As New Hashtable
            Dim ArrTot() As Double
            Dim ArrRej() As Double
            Dim ArrPer() As Double
            Dim ArrCount() As Double
            ArrexOP = Nothing
            ArrexItem = Nothing
            ArrexRes = Nothing
            ArrexJob = Nothing
            '
            '           If cmbResource.Text <> "" And cmbResource.Text <> "-Select-" And cmbWC.Text = "-Select-" Then
            'Dim Strx As String() = Split(cmbResource.SelectedItem.id, "||")
            'Strsql = "select *  from tbJobTrans where _status<>'COMPLETED' and _CreateDate between " & dblSd & " and " & dblEd & " and _wc in (select _wc from tbWorkStation where _rid=" & Val(Strx(1)) & ")"
            'ElseIf cmbWC.Text <> "-Select-" Then
            'Strsql = "select *  from tbJobTrans where _status<>'COMPLETED' and _CreateDate between " & dblSd & " and " & dblEd & " and _wc ='" & Replace(cmbWC.SelectedItem.id, "'", "''") & "'"
            'Else

            If ckbRework.Checked = True Then
                Strsql = "select *  from tbJobTrans where _status<>'COMPLETED'   and _jobsuffix='M' and  _qty_op_qty <> 0 and _rework<>0 " & strTransCont
            Else
                Strsql = "select *  from tbJobTrans where _status<>'COMPLETED'   and  _qty_op_qty <> 0 " & strTransCont
            End If

            'End If
            DSrep = clsM.GetDataset(Strsql, "tbJobTransMain")

            Dim totqty As Double
            Dim drRp As DataRow
            Dim recount As Boolean = False
            PB1.Value = 0
            PB1.Minimum = 0
            PB1.Maximum = 0
            Dim IA As Integer = 0
            Dim i As Integer
            If DSrep.Tables(0).Rows.Count > 0 Then

                PB1.Maximum = DSrep.Tables(0).Rows.Count
                For i = 0 To DSrep.Tables(0).Rows.Count - 1

                    With DSrep.Tables(0).Rows(i)
                        If .Item("_Job") = "SD22000785" Then
                            MsgBox("SD22000785 ")
                        End If
                        Dim runt As Boolean = False
                        Dim ArrItem As String() = Split(.Item("_item"), "-")
                        'If ArrItem.Length > 1 Then
                        '    MsgBox(.Item("_item"))
                        'End If
                        If StItem = True Then
                            runt = True
                        ElseIf HASRe.ContainsKey(.Item("_item")) = True Then
                            runt = True
                        Else
                            runt = False
                        End If
                        If runt = True Then
                            If .Item("_qty_op_qty") <> 0 Then
                                recount = True
                                Dim StrItemCode As String = ""
                                Dim StrItemName As String = ""
                                Dim StrRecName As String = ""
                                Dim StrWCCode As String = ""
                                Dim StrWCName As String = ""
                                Dim dblQty As Double
                                Dim StrOP As String = ""
                                Dim strjob As String
                                StrOP = .Item("_oper_num")
                                StrWCCode = .Item("_WC")
                                Dim DRResou() As DataRow = DSRecGroup.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                If DRResou.Length > 0 Then
                                    StrRecName = DRResou(0).Item("_rGroupID")
                                    StrWCName = DRResou(0).Item("_description")

                                Else
                                    Dim DRResouNew() As DataRow = DSRecWC.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                    If DRResouNew.Length > 0 Then
                                        StrWCName = DRResouNew(0).Item("_description")
                                    End If
                                    StrRecName = ""
                                End If
                                Dim DRItem() As DataRow = DSItem.Tables(0).Select("_itemCode='" & .Item("_item") & "'")

                                StrItemCode = .Item("_item")
                                strjob = UCase(.Item("_job"))
                                If DRItem.Length > 0 Then
                                    StrItemName = DRItem(0).Item("_itemdescription")
                                End If
                                ' Dim WeelCount As Integer = GetWeek(DateTime.FromOADate(.Item("_CreateDate")))
                                Dim strID As String = StrItemCode & "||" & CStr(StrOP) & "||" & CStr(strjob)
                                Dim ArrstrID As String = StrRecName & "||" & StrItemCode & "||"
                                totqty = totqty + .Item("_qty_op_qty")
                                'check for Export


                                Dim strUId As String = StrItemCode & "||" & CStr(StrOP) & "||" & CStr(.Item("_WC"))
                                If HashNewI.ContainsKey(strUId) = False Then
                                    ReDim Preserve exArr(n1)
                                    exArr(n1) = strUId
                                    HashNewI.Add(strUId, strUId)
                                    If StrItemCode = "AC0001" Then
                                        '  MsgBox("Hi")
                                    End If
                                    n1 = n1 + 1
                                End If



                                If hashFindUnique.ContainsKey(strID) = False Then
                                    hashFindUnique.Add(strID, IA)
                                    ReDim Preserve ArrTot(IA)
                                    ReDim Preserve ArrRej(IA)
                                    ReDim Preserve ArrPer(IA)
                                    ReDim Preserve ArrCount(IA)

                                    ReDim Preserve ArrexOP(IA)
                                    ReDim Preserve ArrexItem(IA)
                                    ReDim Preserve ArrexRes(IA)
                                    ReDim Preserve ArrexJob(IA)
                                    ArrexOP(IA) = Val(StrOP)
                                    ArrexItem(IA) = .Item("_item")
                                    ArrexRes(IA) = StrRecName
                                    ArrexJob(IA) = UCase(.Item("_job"))

                                    ArrTot(IA) = .Item("_qty_complete")
                                    ArrRej(IA) = .Item("_qty_scrapped")

                                    ' Dim dblCom1 As Double = .Item("_qty_complete") + .Item("_qty_scrapped")
                                    'ArrPer(IA) = Format(((.Item("_qty_complete") * 100) / dblCom1), "00.00")
                                    ' ArrCount(IA) = ArrCount(IA) + 1
                                    IA = IA + 1
                                Else
                                    Dim strNV As Integer = hashFindUnique(strID)
                                    '  ArrTot(strNV) = .Item("_qty_complete")
                                    'ArrRej(strNV) = .Item("_qty_scrapped")
                                    '
                                    ' Dim dblCom1 As Double = .Item("_qty_complete") + .Item("_qty_scrapped")
                                    ' ArrPer(strNV) = ArrPer(strNV) + Format(((.Item("_qty_complete") * 100) / dblCom1), "00.00")
                                    ArrCount(strNV) = ArrCount(strNV) + 1


                                End If
                                'end Check

                                drRp = dsrsg.Tables("tbItemNewJob").NewRow
                                drRp("sno") = i + 1
                                drRp("Title") = strReportDate ' Trim(txtFrom.Text) '& "-" & Trim(txtTo.Text)
                                drRp("CreateDate") = CreatedDate
                                drRp("ReportDate") = strReportDate
                                Dim StrArr() As String = Split(StrItemName, ",")
                                drRp("SelRSCGroup") = StrRecName
                                drRp("StockCard") = ""
                                drRp("RSCGroup") = StrRecName
                                drRp("FGCode") = StrItemCode
                                drRp("FGDesc") = StrArr(0)
                                drRp("Job") = UCase(.Item("_job"))
                                ' If Trim(.Item("_jobsuffix")) = "M" Then
                                'drRp("ChildR") = ""
                                ' Else
                                If .Item("_Reworkst") <> "" Then
                                    drRp("ChildR") = .Item("_jobsuffix") & "-" & .Item("_Reworkst")
                                Else
                                    drRp("ChildR") = .Item("_jobsuffix")
                                End If

                                ' End If
                                If .Item("_Start_date") <> 0 Then
                                    drRp("Sdate") = Format(DateTime.FromOADate(.Item("_Start_date")), "d MMM yyyy HH:mm")
                                Else
                                    drRp("Sdate") = ""
                                End If
                                If .Item("_CreateDate") = 0 Then
                                    drRp("Tdate") = ""
                                ElseIf UCase(.Item("_trans")) = "YES" Or .Item("_trans") = "Y" Then
                                    drRp("Tdate") = Format(DateTime.FromOADate(.Item("_CreateDate")), "d MMM yyyy HH:mm")
                                Else
                                    drRp("Tdate") = ""
                                End If
                               

                                drRp("OP") = StrOP
                                drRp("WC") = StrWCCode
                                drRp("WCDesc") = StrWCName
                                drRp("Qty") = .Item("_qty_op_qty")
                                drRp("weekd") = 0
                                drRp("Status") = .Item("_status")
                                dsrsg.Tables("tbItemNewJob").Rows.Add(drRp)

                            End If
                        End If
                    End With
                    PB1.Value = i + 1
                    Application.DoEvents()
                Next
            End If
            If ckbRework.Checked = False Then
                If strTransCont = "" Then
                    FNCJobNotAction(dsrsg, i, CreatedDate, DSItem, DSRecWC, DSRecGroup, HASRe, recount, HashNewI, exArr, n1)
                End If
            End If
            i = 0

            If recount = False Then

                drRp = dsrsg.Tables("tbItemNewJob").NewRow
                drRp("sno") = 0
                drRp("Title") = strReportDate 'Trim(txtFrom.Text) '& "-" & Trim(txtTo.Text)
                drRp("CreateDate") = CreatedDate
                drRp("ReportDate") = strReportDate
                drRp("WC") = ""
                drRp("SelRSCGroup") = ""
                drRp("StockCard") = ""
                drRp("RSCGroup") = ""
                drRp("FGCode") = ""
                drRp("FGDesc") = ""
                drRp("Job") = ""
                drRp("Sdate") = 0
                drRp("Tdate") = 0
                drRp("OP") = 0
                drRp("WCDesc") = ""
                drRp("ChildR") = ""
                drRp("Qty") = 0
                drRp("weekd") = 0
                drRp("Status") = ""
                dsrsg.Tables("tbItemNewJob").Rows.Add(drRp)
                hashFindUnique.Add("", 0)
                ReDim Preserve ArrTot(0)
                ReDim Preserve ArrRej(0)
                ReDim Preserve ArrPer(0)
                ReDim Preserve ArrCount(0)
                ArrTot(0) = 0
                ArrRej(0) = 0







                'dsrsg.Tables("tbItemNewJob").Rows.Add(drRp)
            End If






            If dsrsg.Tables("tbItemNewJob").Rows.Count > 0 Then

                For i = 0 To dsrsg.Tables("tbItemNewJob").Rows.Count - 1
                    dsrsg.Tables("tbItemNewJob").Rows(i).Item("TotalQty") = totqty

                Next
                dblTotAll = totqty
            End If


            dsrsg.Tables("tbItemNewJob").DefaultView.Sort = "OP,Job ASC"
            If lblReportTit.Text = UCase("WIP by Resource Group") Then
                Dim cry As New cryItemNewReport
                cry.SetDataSource(dsrsg)
                RViewer.ReportSource = cry
            Else
                Dim cry As New cryItemNewStart
                cry.SetDataSource(dsrsg)
                RViewer.ReportSource = cry
            End If

            RViewer.Refresh()
            Panel1.Visible = False
            RViewer.Visible = True
            RViewer.Enabled = True
            butClose.Visible = True
            butExport.Visible = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub



   

  
    'Private Sub RViewer_ClickPage(ByVal sender As Object, ByVal e As CrystalDecisions.Windows.Forms.PageMouseEventArgs)
    '    TmVari = 2
    'End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        RViewer.DataBindings.Clear()
        GC.GetGeneration(RViewer)
        RViewer.Visible = False
        butClose.Visible = False
        butExport.Visible = False
        PB1.Value = 0
        PB1.Minimum = 0
        PB1.Maximum = 0
        Panel1.Visible = True
    End Sub





    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim GetD As Date = DateSerial(Year(Now), 1, 1)
        Dim FirstDate As String = UCase(Format(GetD, "ddd"))
        Dim FistWeek As Integer = 0
        Dim CalFirstDate As Date
        If FirstDate = "SUN" Then
            CalFirstDate = GetD
            FistWeek = 0
        ElseIf FirstDate = "MON" Then
            CalFirstDate = DateAdd(DateInterval.Day, 6, GetD)
            FistWeek = 1
        ElseIf FirstDate = "TUE" Then
            CalFirstDate = DateAdd(DateInterval.Day, 5, GetD)
            FistWeek = 1
        ElseIf FirstDate = "WED" Then
            CalFirstDate = DateAdd(DateInterval.Day, 4, GetD)
            FistWeek = 1
        ElseIf FirstDate = "THU" Then
            CalFirstDate = DateAdd(DateInterval.Day, 3, GetD)
            FistWeek = 1
        ElseIf FirstDate = "FRI" Then
            CalFirstDate = DateAdd(DateInterval.Day, 2, GetD)
            FistWeek = 1
        ElseIf FirstDate = "SAT" Then
            CalFirstDate = DateAdd(DateInterval.Day, 1, GetD)
            FistWeek = 1
        End If
        Dim GetD1 As Date = DateSerial(Year(dtpFrom.Value), Month(dtpFrom.Value), Day(dtpFrom.Value))
        Dim intday As Integer = DateDiff(DateInterval.Day, CalFirstDate, GetD1) + 1
        Dim countWeek As Double
        If intday = 0 Then
            countWeek = 1
        Else
            countWeek = (intday / 7) + FistWeek
        End If
        Dim S() As String = Split(CStr(countWeek), ".")
        Dim allcont As Integer
        If S.Length > 1 Then
            If Val(S(1)) <> 0 Then
                allcont = Val(S(0)) + 1
            Else
                allcont = Val(S(0))
            End If
        Else
            allcont = Val(S(0))
        End If

        MsgBox(allcont)

    End Sub




    Function GetWeek(ByVal sd As Date) As Integer
        Dim allcont As Integer
        Try


            Dim GetD As Date = DateSerial(Year(Now), 1, 1)
            Dim FirstDate As String = UCase(Format(GetD, "ddd"))
            Dim FistWeek As Integer = 0
            Dim CalFirstDate As Date
            If FirstDate = "SUN" Then
                CalFirstDate = GetD
                FistWeek = 0
            ElseIf FirstDate = "MON" Then
                CalFirstDate = DateAdd(DateInterval.Day, 6, GetD)
                FistWeek = 1
            ElseIf FirstDate = "TUE" Then
                CalFirstDate = DateAdd(DateInterval.Day, 5, GetD)
                FistWeek = 1
            ElseIf FirstDate = "WED" Then
                CalFirstDate = DateAdd(DateInterval.Day, 4, GetD)
                FistWeek = 1
            ElseIf FirstDate = "THU" Then
                CalFirstDate = DateAdd(DateInterval.Day, 3, GetD)
                FistWeek = 1
            ElseIf FirstDate = "FRI" Then
                CalFirstDate = DateAdd(DateInterval.Day, 2, GetD)
                FistWeek = 1
            ElseIf FirstDate = "SAT" Then
                CalFirstDate = DateAdd(DateInterval.Day, 1, GetD)
                FistWeek = 1
            End If
            Dim GetD1 As Date = DateSerial(Year(sd), Month(sd), Day(sd))
            Dim intday As Integer = DateDiff(DateInterval.Day, CalFirstDate, GetD1) + 1
            Dim countWeek As Double
            If intday = 0 Then
                countWeek = 1
            Else
                countWeek = (intday / 7) + FistWeek
            End If
            Dim S() As String = Split(CStr(countWeek), ".")

            If S.Length > 1 Then
                If Val(S(1)) <> 0 Then
                    allcont = Val(S(0)) + 1
                Else
                    allcont = Val(S(0))
                End If
            Else
                allcont = Val(S(0))
            End If

        Catch ex As Exception
            allcont = 0
        End Try
        Return allcont
    End Function


    Private Sub butExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExport.Click
        If lblReportTit.Text = UCase("WIP by Resource Group") Then
            Resource_ExportDatatoExcel()
        Else
            ExportDatatoExcel()
        End If

    End Sub

    Private Sub frmRPTItemStart_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'lblReportTit.Text = "WIP BY ITEM REPORT"

        If lblReportTit.Text <> UCase("WIP by Resource Group") Then
            ckbTransfer.Checked = False
            ckbTransfer.Visible = True

        Else
            ckbTransfer.Checked = False
            ckbTransfer.Visible = False
        End If
        dtpFrom.Value = Now
        '  dtpTo.Value = Now
        txtFrom.Text = Format(Now, "dd/MM/yyyy")
        ' txtTo.Text = Format(Now, "dd/MM/yyyy")
        ' ResourceData()
        ' GetWC()
        GetItem()
    End Sub
    'Sub ResourceData()
    '    Dim ds As New DataSet
    '    cmbResource.Items.Clear()
    '    cmbResource.Items.Add(New DataItem("-Select-", "-Select-"))
    '    Try
    '        With com
    '            .Connection = cn
    '            .CommandType = CommandType.Text
    '            'Dim DI As DataItem

    '            ds = clsM.GetDataset("select _rGroupid,_rGroupName,_rid from tbResourceGroup order by _rGroupid", "tbWC")
    '            If IsDBNull(ds) = False Then
    '                Dim i As Integer
    '                If ds.Tables(0).Rows.Count > 0 Then
    '                    For i = 0 To ds.Tables(0).Rows.Count - 1
    '                        With ds.Tables(0).Rows(i)
    '                            cmbResource.Items.Add(New DataItem(.Item("_rGroupName") & "||" & .Item("_rid"), .Item("_rGroupid")))
    '                        End With
    '                    Next
    '                End If
    '            End If
    '        End With
    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
    '    Finally
    '        cn.Close()
    '    End Try
    '    cmbResource.Text = "-Select-"
    'End Sub
    'Private Sub cmbResource_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    GetWC()
    'End Sub
    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub
    Private Sub butAssSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssSingle.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstItem.SelectedItems.Count - 1
                lstSelItem.Items.Add(lstItem.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstItem.SelectedItems.Count - 1
                lstItem.Items.Remove(lstItem.SelectedItems.Item(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butAssMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssMulti.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstItem.Items.Count - 1
                lstSelItem.Items.Add(lstItem.Items(i))
            Next
            i = 0
            For i = 0 To lstItem.Items.Count - 1
                lstItem.Items.Remove(lstItem.Items(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butDelSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelSingle.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstSelItem.SelectedItems.Count - 1
                lstItem.Items.Add(lstSelItem.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstSelItem.SelectedItems.Count - 1
                lstSelItem.Items.Remove(lstSelItem.SelectedItems.Item(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub
    Private Sub butDelMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelMulti.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstSelItem.Items.Count - 1
                lstItem.Items.Add(lstSelItem.Items(i))
            Next
            i = 0
            For i = 0 To lstSelItem.Items.Count - 1
                lstSelItem.Items.Remove(lstSelItem.Items(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        dtpFrom.Value = Now
        '  dtpTo.Value = Now
        ' ResourceData()
        'GetWC()
        GetItem()
    End Sub
    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        comletedTransaction()
        TmVari = 2

        '  Parent.Focus()

    End Sub
    'Sub GetWC()
    '    cmbWC.Items.Clear()
    '    DSWC.Tables.Clear()
    '    cmbWC.Items.Add(New DataItem("-Select-", "-Select-"))
    '    Dim selcon As String = ""
    '    Dim sql As String = ""
    '    If cmbResource.Text <> "-Select-" Then
    '        sql = "select * from tbWC where _wc in(select _WC from tbWorkStation where _rid in(select _rid from tbResourceGroup where _rGroupid='" & Trim(cmbResource.Text) & "')) order by _wc"

    '    Else
    '        sql = "select * from tbWC order by _wc"
    '    End If
    '    Try
    '        Dim ds As New DataSet
    '        ds = clsM.GetDataset(sql, "tbWC")
    '        DSWC = ds
    '        If IsDBNull(ds) = False Then
    '            Dim i As Integer
    '            If ds.Tables(0).Rows.Count > 0 Then
    '                For i = 0 To ds.Tables(0).Rows.Count - 1
    '                    With ds.Tables(0).Rows(i)
    '                        cmbWC.Items.Add(New DataItem(Trim(.Item("_wc")), Trim(.Item("_wc")) & "  :  " & Trim(.Item("_description")) & ""))
    '                    End With
    '                Next
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
    '    Finally
    '        cn.Close()
    '    End Try
    '    cmbWC.Text = "-Select-"
    'End Sub


    'Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    txtTo.Text = Format(dtpTo.Value, "dd/MM/yyyy")
    '    dtpTo.Checked = False
    'End Sub
    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtFrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False
    End Sub

    Private Sub lblReportTit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblReportTit.Click

    End Sub
    'Function ExportDatatoExcel() As Boolean '//Modified date 2010-10-10
    '    Dim txtFile As String
    '    txtFile = ""
    '    sfdSave.FileName = ""
    '    sfdSave.Filter = "Excel File|*.xls"
    '    sfdSave.Title = "Save an Excel File"
    '    If sfdSave.ShowDialog = System.Windows.Forms.DialogResult.OK Then
    '        txtFile = sfdSave.FileName
    '    Else
    '        Exit Function
    '    End If
    '    Panel3.Visible = True
    '    PB2.Value = 0
    '    PB2.Minimum = 0
    '    PB2.Maximum = 0
    '    Dim myTrans As System.Data.SqlClient.SqlTransaction

    '    Dim delFile As Boolean = False
    '    '= "C:\ZandenTemp\ComReport.xls"
    '    Try
    '        If IO.File.Exists(txtFile) Then
    '            IO.File.Delete(txtFile)
    '        End If
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '        Exit Function
    '    Finally
    '    End Try
    '    ' Dim at As Integer
    '    If txtFile = "" Then
    '        MsgBox("Please enter the file name!", MsgBoxStyle.Information, "eWIP")
    '        Exit Function
    '    End If

    '    Dim str, filename As String
    '    str = ""
    '    filename = ""
    '    Dim col, row As Integer
    '    col = 0
    '    row = 0
    '    Dim strHisNo As String = ""
    '    Dim strAcrNo As String = ""
    '    Dim valHis As String
    '    Dim strHisVal As String = ""
    '    Dim strCurVal As String = ""
    '    Dim localAll As Process()
    '    localAll = Process.GetProcesses()
    '    Dim EXhash As New Hashtable
    '    For i As Integer = 0 To localAll.Length - 1
    '        If localAll(i).ProcessName.ToUpper = "EXCEL" Then
    '            If EXhash.ContainsKey(localAll(i).Id) = False Then
    '                EXhash.Add(localAll(i).Id, localAll(i).Id)
    '            End If
    '        End If
    '    Next

    '    Dim ExcelA As Object = CreateObject("Excel.Application")



    '    P1.Visible = True
    '    Try
    '        Dim i As Integer = 1
    '        With ExcelA
    '            .SheetsInNewWorkbook = 1
    '            .Workbooks.Add()
    '            .Worksheets(1).Select()

    '            Dim chartRange As Excel.Range

    '            chartRange = .Range("A1", "H1")
    '            chartRange.Merge()
    '            chartRange.FormulaR1C1 = "WIP BY ITEM REPORT"
    '            chartRange.HorizontalAlignment = 3
    '            chartRange.VerticalAlignment = 3

    '            chartRange = .Range("A2", "D2")
    '            chartRange.Merge()
    '            chartRange.FormulaR1C1 = "Created Date :" & Format(Now, "dd MMM yyyy")
    '            chartRange.HorizontalAlignment = 1
    '            chartRange.VerticalAlignment = 1

    '            chartRange = .Range("E2", "H2")
    '            chartRange.Merge()
    '            chartRange.FormulaR1C1 = "Total : " & Format(dblTotAll, "0.00")
    '            chartRange.HorizontalAlignment = 1
    '            chartRange.VerticalAlignment = 1

    '            chartRange = .Range("A3", "D3")
    '            chartRange.Merge()
    '            chartRange.FormulaR1C1 = "" ' "FG Description :"
    '            chartRange.HorizontalAlignment = 1
    '            chartRange.VerticalAlignment = 1


    '            chartRange = .Range("E3", "H3")
    '            chartRange.Merge()
    '            chartRange.FormulaR1C1 = "Stock Card :"
    '            chartRange.HorizontalAlignment = 1
    '            chartRange.VerticalAlignment = 1

    '            If dsrsg.Tables("tbItemNewJob").Rows.Count > 0 Then
    '                If exArr.Length > 0 Then
    '                    Dim M1 As Integer
    '                    Dim stroldICode As String = ""
    '                    Array.Sort(exArr)
    '                    i = 3
    '                    PB2.Maximum = exArr.Length


    '                    For M1 = 0 To exArr.Length - 1
    '                        Dim s() As String = Split(exArr(M1), "||")
    '                        Dim drItem() As DataRow = dsrsg.Tables("tbItemNewJob").Select("FGCode='" & s(0) & "' and OP='" & s(1) & "' and WC='" & s(2) & "'", "WCDesc,Job")
    '                        Dim k1 As Integer
    '                        Dim stBoll As Boolean = False
    '                        Dim strOpN As String
    '                        Dim strWCDES As String
    '                        Dim strQTY As Double = 0

    '                        If drItem.Length > 0 Then
    '                            For k1 = 0 To drItem.Length - 1
    '                                If stBoll = False Then
    '                                    If stroldICode <> drItem(k1).Item("FGCode") Then
    '                                        stroldICode = drItem(k1).Item("FGCode")
    '                                        i += 1
    '                                        chartRange = .Range("A" & i, "D" & i)
    '                                        chartRange.Merge()
    '                                        chartRange.FormulaR1C1 = "FG Code : " & drItem(k1).Item("FGCode")
    '                                        chartRange.HorizontalAlignment = 1
    '                                        chartRange.VerticalAlignment = 1
    '                                        i += 1
    '                                        chartRange = .Range("A" & i, "D" & i)
    '                                        chartRange.Merge()
    '                                        chartRange.FormulaR1C1 = "FG Description : " & drItem(k1).Item("FGDesc")
    '                                        chartRange.HorizontalAlignment = 1
    '                                        chartRange.VerticalAlignment = 1
    '                                    End If


    '                                    i += 1
    '                                    ExcelA.Range("A" & i, "H" & i).Interior.ColorIndex = 37
    '                                    .cells(i, 1).MergeCells = True
    '                                    .cells(i, 1).EntireRow.Font.Bold = True
    '                                    .cells(i, 1).value = "Job"


    '                                    .cells(i, 2).MergeCells = True
    '                                    .cells(i, 2).EntireRow.Font.Bold = True
    '                                    .cells(i, 2).value = "SP"

    '                                    .cells(i, 3).MergeCells = True
    '                                    .cells(i, 3).EntireRow.Font.Bold = True
    '                                    .cells(i, 3).value = "Transfer Date"


    '                                    .cells(i, 4).MergeCells = True
    '                                    .cells(i, 4).EntireRow.Font.Bold = True
    '                                    .cells(i, 4).value = "Start Date"

    '                                    .cells(i, 5).MergeCells = True
    '                                    .cells(i, 5).EntireRow.Font.Bold = True
    '                                    .cells(i, 5).value = "Status"

    '                                    .cells(i, 6).MergeCells = True
    '                                    .cells(i, 6).EntireRow.Font.Bold = True
    '                                    .cells(i, 6).value = "OP"

    '                                    .cells(i, 7).MergeCells = True
    '                                    .cells(i, 7).EntireRow.Font.Bold = True
    '                                    .cells(i, 7).value = "WC Description"

    '                                    .cells(i, 8).MergeCells = True
    '                                    .cells(i, 8).EntireRow.Font.Bold = True
    '                                    .cells(i, 8).value = "OP QTY"

    '                                    strOpN = drItem(k1).Item("OP")
    '                                    strWCDES = drItem(k1).Item("WCDesc")
    '                                    strQTY = strQTY + Format(drItem(k1).Item("Qty"), "0.00")
    '                                    i += 1
    '                                    .cells(i, 1).value = drItem(k1).Item("JOB")
    '                                    .cells(i, 2).value = drItem(k1).Item("ChildR")
    '                                    .cells(i, 3).value = "'" & drItem(k1).Item("Tdate")
    '                                    .cells(i, 4).value = "'" & drItem(k1).Item("Sdate")
    '                                    .cells(i, 5).value = drItem(k1).Item("Status")
    '                                    .cells(i, 6).value = drItem(k1).Item("OP")
    '                                    .cells(i, 7).value = drItem(k1).Item("WCDesc")
    '                                    .cells(i, 8).value = Format(drItem(k1).Item("QTY"), "0.00")
    '                                    stBoll = True




    '                                Else
    '                                    strOpN = drItem(k1).Item("OP")
    '                                    strWCDES = drItem(k1).Item("WCDesc")
    '                                    strQTY = strQTY + Format(drItem(k1).Item("Qty"), "0.00")
    '                                    i += 1
    '                                    .cells(i, 1).value = drItem(k1).Item("JOB")
    '                                    .cells(i, 2).value = drItem(k1).Item("ChildR")
    '                                    .cells(i, 3).value = "'" & drItem(k1).Item("Tdate")
    '                                    .cells(i, 4).value = "'" & drItem(k1).Item("Sdate")
    '                                    .cells(i, 5).value = drItem(k1).Item("Status")
    '                                    .cells(i, 6).value = drItem(k1).Item("OP")
    '                                    .cells(i, 7).value = drItem(k1).Item("WCDesc")
    '                                    .cells(i, 8).value = Format(drItem(k1).Item("QTY"), "0.00")
    '                                    stBoll = True

    '                                End If


    '                            Next
    '                            If stBoll = True Then
    '                                i += 1
    '                                ExcelA.Range("A" & i, "H" & i).Font.ColorIndex = 3
    '                                .cells(i, 1).value = ""
    '                                .cells(i, 2).value = ""
    '                                .cells(i, 3).value = ""
    '                                .cells(i, 4).value = ""
    '                                .cells(i, 5).value = ""
    '                                .cells(i, 6).value = strOpN 'Format(dblScrap / intCount, "0.00")
    '                                .cells(i, 7).value = strWCDES
    '                                .cells(i, 8).value = Format(strQTY, "0.00")
    '                            End If
    '                        End If
    '                        PB2.Value = M1 + 1
    '                        Application.DoEvents()
    '                    Next
    '                End If



    '            End If
    '            filename = txtFile
    '            ExcelA.ActiveCell.Worksheet.SaveAs(filename)
    '            System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelA)
    '            ExcelA = Nothing



    '            MsgBox("Data's are exported to Excel Succesfully in '" & filename & "'", MsgBoxStyle.Information, "eWIP")

    '            '  lblDataV.Text = "Data has been Successfully Exported"



    '        End With
    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Information, "eWIP")
    '        delFile = True


    '    Finally
    '        Panel3.Visible = False
    '        cn.Close()
    '        com.Parameters.Clear()
    '    End Try

    '    ' The excel is created and opened for insert value. We most close this excel using this system
    '    ' Dim pro() As Process = System.Diagnostics.Process.GetProcessesByName("Excel")

    '    Dim DellocalAll As Process()

    '    DellocalAll = Process.GetProcesses()


    '    For i As Integer = 0 To DellocalAll.Length - 1
    '        If DellocalAll(i).ProcessName.ToUpper = "EXCEL" Then
    '            If EXhash.ContainsKey(DellocalAll(i).Id) = False Then
    '                DellocalAll(i).Kill()
    '            End If
    '        End If
    '    Next



    '    'For Each i As Process In pro
    '    '    If localAll(i).ProcessName.ToUpper = "EXCEL" Then
    '    '        lstOldProcessID.Items.Add(localAll(i).Id)
    '    '    End If
    '    '    i.Kill()
    '    'Next
    '    If delFile = True Then
    '        Try
    '            If IO.File.Exists(filename) Then
    '                IO.File.Delete(filename)
    '            End If
    '        Catch ex As Exception
    '            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
    '        End Try
    '    End If
    '    txtFile = ""


    'End Function


    Function ExportDatatoExcel() As Boolean '//Modified date 2010-10-10
        Dim txtFile As String
        txtFile = ""
        sfdSave.FileName = ""
        sfdSave.Filter = "Excel File|*.xls"
        sfdSave.Title = "Save an Excel File"
        If sfdSave.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtFile = sfdSave.FileName
        Else
            Exit Function
        End If
        Panel3.Visible = True
        PB2.Value = 0
        PB2.Minimum = 0
        PB2.Maximum = 0
        Dim myTrans As System.Data.SqlClient.SqlTransaction

        Dim delFile As Boolean = False
        '= "C:\ZandenTemp\ComReport.xls"
        Try
            If IO.File.Exists(txtFile) Then
                IO.File.Delete(txtFile)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Function
        Finally
        End Try
        ' Dim at As Integer
        If txtFile = "" Then
            MsgBox("Please enter the file name!", MsgBoxStyle.Information, "eWIP")
            Exit Function
        End If

        Dim str, filename As String
        str = ""
        filename = ""
        Dim col, row As Integer
        col = 0
        row = 0
        Dim strHisNo As String = ""
        Dim strAcrNo As String = ""
        Dim valHis As String
        Dim strHisVal As String = ""
        Dim strCurVal As String = ""
        Dim localAll As Process()
        localAll = Process.GetProcesses()
        Dim EXhash As New Hashtable
        For i As Integer = 0 To localAll.Length - 1
            If localAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(localAll(i).Id) = False Then
                    EXhash.Add(localAll(i).Id, localAll(i).Id)
                End If
            End If
        Next

        Dim ExcelA As Object = CreateObject("Excel.Application")



        P1.Visible = True
        Try
            Dim i As Integer = 1
            With ExcelA
                .SheetsInNewWorkbook = 1
                .Workbooks.Add()
                .Worksheets(1).Select()

                'Dim chartRange As Excel.Range

                'chartRange = .Range("A1", "J1")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "WIP BY ITEM REPORT"
                'chartRange.HorizontalAlignment = 3
                'chartRange.VerticalAlignment = 3

                'chartRange = .Range("A2", "D2")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "Created Date :" & Format(Now, "dd MMM yyyy")
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1

                'chartRange = .Range("E2", "J2")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "Total : " & Format(dblTotAll, "0.00")
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1

                'chartRange = .Range("A3", "D3")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "" ' "FG Description :"
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1


                'chartRange = .Range("E3", "J3")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "Stock Card :"
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1

                i = i + 3
                .cells(i, 1).MergeCells = True
                .cells(i, 1).EntireRow.Font.Bold = True
                .cells(i, 1).value = "FG Code"

                .cells(i, 2).MergeCells = True
                .cells(i, 2).EntireRow.Font.Bold = True
                .cells(i, 2).value = "FG Description"

                .cells(i, 3).MergeCells = True
                .cells(i, 3).EntireRow.Font.Bold = True
                .cells(i, 3).value = "Job"


                .cells(i, 4).MergeCells = True
                .cells(i, 4).EntireRow.Font.Bold = True
                .cells(i, 4).value = "SP"

                .cells(i, 5).MergeCells = True
                .cells(i, 5).EntireRow.Font.Bold = True
                .cells(i, 5).value = "Transfer Date"


                .cells(i, 6).MergeCells = True
                .cells(i, 6).EntireRow.Font.Bold = True
                .cells(i, 6).value = "Start Date"

                .cells(i, 7).MergeCells = True
                .cells(i, 7).EntireRow.Font.Bold = True
                .cells(i, 7).value = "Status"

                .cells(i, 8).MergeCells = True
                .cells(i, 8).EntireRow.Font.Bold = True
                .cells(i, 8).value = "OP"

                .cells(i, 9).MergeCells = True
                .cells(i, 9).EntireRow.Font.Bold = True
                .cells(i, 9).value = "WC Description"

                .cells(i, 10).MergeCells = True
                .cells(i, 10).EntireRow.Font.Bold = True
                .cells(i, 10).value = "OP QTY"

                'i += 1


                If dsrsg.Tables("tbItemNewJob").Rows.Count > 0 Then
                    If exArr.Length > 0 Then
                        Dim M1 As Integer
                        Dim stroldICode As String = ""
                        Array.Sort(exArr)
                        ' i = i + 1
                        PB2.Maximum = exArr.Length


                        For M1 = 0 To exArr.Length - 1
                            Dim s() As String = Split(exArr(M1), "||")
                            Dim drItem() As DataRow = dsrsg.Tables("tbItemNewJob").Select("FGCode='" & s(0) & "' and OP='" & s(1) & "' and WC='" & s(2) & "'", "WCDesc,Job")
                            Dim k1 As Integer
                            Dim stBoll As Boolean = False
                            Dim strOpN As String
                            Dim strWCDES As String
                            Dim strQTY As Double = 0


                            '  ExcelA.Range("A" & i, "H" & i).Interior.ColorIndex = 37

                           
                            If drItem.Length > 0 Then
                                For k1 = 0 To drItem.Length - 1
                                    'If stBoll = False Then
                                    '    If stroldICode <> drItem(k1).Item("FGCode") Then
                                    '        stroldICode = drItem(k1).Item("FGCode")
                                    '        i += 1
                                    '        chartRange = .Range("A" & i, "D" & i)
                                    '        chartRange.Merge()
                                    '        chartRange.FormulaR1C1 = "FG Code : " & drItem(k1).Item("FGCode")
                                    '        chartRange.HorizontalAlignment = 1
                                    '        chartRange.VerticalAlignment = 1
                                    '        i += 1
                                    '        chartRange = .Range("A" & i, "D" & i)
                                    '        chartRange.Merge()
                                    '        chartRange.FormulaR1C1 = "FG Description : " & drItem(k1).Item("FGDesc")
                                    '        chartRange.HorizontalAlignment = 1
                                    '        chartRange.VerticalAlignment = 1
                                    '    End If





                                    '    strOpN = drItem(k1).Item("OP")
                                    '    strWCDES = drItem(k1).Item("WCDesc")
                                    '    strQTY = strQTY + Format(drItem(k1).Item("Qty"), "0.00")
                                    '    i += 1
                                    '    .cells(i, 1).value = drItem(k1).Item("FGCode")
                                    '    .cells(i, 2).value = drItem(k1).Item("FGDesc")
                                    '    .cells(i, 3).value = drItem(k1).Item("JOB")
                                    '    .cells(i, 4).value = drItem(k1).Item("ChildR")
                                    '    .cells(i, 5).value = "'" & drItem(k1).Item("Tdate")
                                    '    .cells(i, 6).value = "'" & drItem(k1).Item("Sdate")
                                    '    .cells(i, 7).value = drItem(k1).Item("Status")
                                    '    .cells(i, 8).value = drItem(k1).Item("OP")
                                    '    .cells(i, 9).value = drItem(k1).Item("WCDesc")
                                    '    .cells(i, 10).value = Format(drItem(k1).Item("QTY"), "0.00")
                                    '    stBoll = True




                                    'Else
                                    i += 1
                                    strOpN = drItem(k1).Item("OP")
                                    strWCDES = drItem(k1).Item("WCDesc")
                                    strQTY = strQTY + Format(drItem(k1).Item("Qty"), "0.00")

                                    .cells(i, 1).value = drItem(k1).Item("FGCode")
                                    .cells(i, 2).value = drItem(k1).Item("FGDesc")
                                    .cells(i, 3).value = drItem(k1).Item("JOB")
                                    .cells(i, 4).value = drItem(k1).Item("ChildR")
                                    .cells(i, 5).value = "'" & drItem(k1).Item("Tdate")
                                    .cells(i, 6).value = "'" & drItem(k1).Item("Sdate")
                                    .cells(i, 7).value = drItem(k1).Item("Status")
                                    .cells(i, 8).value = drItem(k1).Item("OP")
                                    .cells(i, 9).value = drItem(k1).Item("WCDesc")
                                    .cells(i, 10).value = Format(drItem(k1).Item("QTY"), "0.00")
                                    stBoll = True

                                    '  End If


                                Next
                                If stBoll = True Then
                                    i += 1
                                    ExcelA.Range("A" & i, "H" & i).Font.ColorIndex = 3
                                    .cells(i, 1).value = ""
                                    .cells(i, 2).value = ""
                                    .cells(i, 3).value = ""
                                    .cells(i, 4).value = ""
                                    .cells(i, 5).value = ""
                                    .cells(i, 6).value = ""
                                    .cells(i, 7).value = ""
                                    .cells(i, 8).value = strOpN 'Format(dblScrap / intCount, "0.00")
                                    .cells(i, 9).value = strWCDES
                                    .cells(i, 10).value = Format(strQTY, "0.00")
                                End If
                            End If
                            PB2.Value = M1 + 1
                            Application.DoEvents()
                        Next
                    End If



                End If
                filename = txtFile
                ExcelA.ActiveCell.Worksheet.SaveAs(filename)
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelA)
                ExcelA = Nothing



                MsgBox("Data's are exported to Excel Succesfully in '" & filename & "'", MsgBoxStyle.Information, "eWIP")

                '  lblDataV.Text = "Data has been Successfully Exported"



            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "eWIP")
            delFile = True


        Finally
            Panel3.Visible = False
            cn.Close()
            com.Parameters.Clear()
        End Try

        ' The excel is created and opened for insert value. We most close this excel using this system
        ' Dim pro() As Process = System.Diagnostics.Process.GetProcessesByName("Excel")

        Dim DellocalAll As Process()

        DellocalAll = Process.GetProcesses()


        For i As Integer = 0 To DellocalAll.Length - 1
            If DellocalAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(DellocalAll(i).Id) = False Then
                    DellocalAll(i).Kill()
                End If
            End If
        Next



        'For Each i As Process In pro
        '    If localAll(i).ProcessName.ToUpper = "EXCEL" Then
        '        lstOldProcessID.Items.Add(localAll(i).Id)
        '    End If
        '    i.Kill()
        'Next
        If delFile = True Then
            Try
                If IO.File.Exists(filename) Then
                    IO.File.Delete(filename)
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End Try
        End If
        txtFile = ""


    End Function

    Function Resource_ExportDatatoExcel() As Boolean '//Modified date 2010-10-10
        Dim txtFile As String
        txtFile = ""
        sfdSave.FileName = ""
        sfdSave.Filter = "Excel File|*.xls"
        sfdSave.Title = "Save an Excel File"
        If sfdSave.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtFile = sfdSave.FileName
        Else
            Exit Function
        End If
        Panel3.Visible = True
        PB2.Value = 0
        PB2.Minimum = 0
        PB2.Maximum = 0
        Dim myTrans As System.Data.SqlClient.SqlTransaction

        Dim delFile As Boolean = False
        '= "C:\ZandenTemp\ComReport.xls"
        Try
            If IO.File.Exists(txtFile) Then
                IO.File.Delete(txtFile)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Function
        Finally
        End Try
        ' Dim at As Integer
        If txtFile = "" Then
            MsgBox("Please enter the file name!", MsgBoxStyle.Information, "eWIP")
            Exit Function
        End If

        Dim str, filename As String
        str = ""
        filename = ""
        Dim col, row As Integer
        col = 0
        row = 0
        Dim strHisNo As String = ""
        Dim strAcrNo As String = ""
        Dim valHis As String
        Dim strHisVal As String = ""
        Dim strCurVal As String = ""
        Dim localAll As Process()
        localAll = Process.GetProcesses()
        Dim EXhash As New Hashtable
        For i As Integer = 0 To localAll.Length - 1
            If localAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(localAll(i).Id) = False Then
                    EXhash.Add(localAll(i).Id, localAll(i).Id)
                End If
            End If
        Next

        Dim ExcelA As Object = CreateObject("Excel.Application")



        P1.Visible = True
        Try
            Dim i As Integer = 1
            With ExcelA
                .SheetsInNewWorkbook = 1
                .Workbooks.Add()
                .Worksheets(1).Select()

                'Dim chartRange As Excel.Range

                'chartRange = .Range("A1", "K1")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "WIP BY RESOURCE GROUP"
                'chartRange.HorizontalAlignment = 3
                'chartRange.VerticalAlignment = 3

                'chartRange = .Range("A2", "D2")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "Created Date :" & Format(Now, "dd MMM yyyy")
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1

                'chartRange = .Range("E2", "K2")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "Total : " & Format(dblTotAll, "0.00")
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1

                'chartRange = .Range("A3", "D3")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "" ' "FG Description :"
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1


                'chartRange = .Range("E3", "K3")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "Stock Card :"
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1

                i = i + 3
                .cells(i, 1).MergeCells = True
                .cells(i, 1).EntireRow.Font.Bold = True
                .cells(i, 1).value = "Resource Group"

                .cells(i, 2).MergeCells = True
                .cells(i, 2).EntireRow.Font.Bold = True
                .cells(i, 2).value = "FG Code"

                .cells(i, 3).MergeCells = True
                .cells(i, 3).EntireRow.Font.Bold = True
                .cells(i, 3).value = "FG Description"

                .cells(i, 4).MergeCells = True
                .cells(i, 4).EntireRow.Font.Bold = True
                .cells(i, 4).value = "Job"


                .cells(i, 5).MergeCells = True
                .cells(i, 5).EntireRow.Font.Bold = True
                .cells(i, 5).value = "SP"

                .cells(i, 6).MergeCells = True
                .cells(i, 6).EntireRow.Font.Bold = True
                .cells(i, 6).value = "Transfer Date"


                .cells(i, 7).MergeCells = True
                .cells(i, 7).EntireRow.Font.Bold = True
                .cells(i, 7).value = "Start Date"

                .cells(i, 8).MergeCells = True
                .cells(i, 8).EntireRow.Font.Bold = True
                .cells(i, 8).value = "Status"

                .cells(i, 9).MergeCells = True
                .cells(i, 9).EntireRow.Font.Bold = True
                .cells(i, 9).value = "OP"

                .cells(i, 10).MergeCells = True
                .cells(i, 10).EntireRow.Font.Bold = True
                .cells(i, 10).value = "WC Description"

                .cells(i, 11).MergeCells = True
                .cells(i, 11).EntireRow.Font.Bold = True
                .cells(i, 11).value = "OP QTY"

                'i += 1


                If dsrsg.Tables("tbItemNewJob").Rows.Count > 0 Then
                    If exArr.Length > 0 Then
                        Dim M1 As Integer
                        Dim stroldICode As String = ""
                        Array.Sort(exArr)
                        ' i = i + 1
                        PB2.Maximum = exArr.Length


                        For M1 = 0 To exArr.Length - 1
                            Dim s() As String = Split(exArr(M1), "||")
                            Dim drItem() As DataRow = dsrsg.Tables("tbItemNewJob").Select("FGCode='" & s(0) & "' and OP='" & s(1) & "' and WC='" & s(2) & "'", "WCDesc,Job")
                            Dim k1 As Integer
                            Dim stBoll As Boolean = False
                            Dim strOpN As String
                            Dim strWCDES As String
                            Dim strQTY As Double = 0


                            '  ExcelA.Range("A" & i, "H" & i).Interior.ColorIndex = 37


                            If drItem.Length > 0 Then
                                For k1 = 0 To drItem.Length - 1
                                    If drItem(k1).Item("JOB") = "ES0070853." Then
                                        ' MsgBox("Hi")
                                    End If

                                    'If stBoll = False Then
                                    '    If stroldICode <> drItem(k1).Item("FGCode") Then
                                    '        stroldICode = drItem(k1).Item("FGCode")
                                    '        i += 1
                                    '        chartRange = .Range("A" & i, "D" & i)
                                    '        chartRange.Merge()
                                    '        chartRange.FormulaR1C1 = "FG Code : " & drItem(k1).Item("FGCode")
                                    '        chartRange.HorizontalAlignment = 1
                                    '        chartRange.VerticalAlignment = 1
                                    '        i += 1
                                    '        chartRange = .Range("A" & i, "D" & i)
                                    '        chartRange.Merge()
                                    '        chartRange.FormulaR1C1 = "FG Description : " & drItem(k1).Item("FGDesc")
                                    '        chartRange.HorizontalAlignment = 1
                                    '        chartRange.VerticalAlignment = 1
                                    '    End If





                                    '    strOpN = drItem(k1).Item("OP")
                                    '    strWCDES = drItem(k1).Item("WCDesc")
                                    '    strQTY = strQTY + Format(drItem(k1).Item("Qty"), "0.00")
                                    '    i += 1
                                    '    .cells(i, 1).value = drItem(k1).Item("FGCode")
                                    '    .cells(i, 2).value = drItem(k1).Item("FGDesc")
                                    '    .cells(i, 3).value = drItem(k1).Item("JOB")
                                    '    .cells(i, 4).value = drItem(k1).Item("ChildR")
                                    '    .cells(i, 5).value = "'" & drItem(k1).Item("Tdate")
                                    '    .cells(i, 6).value = "'" & drItem(k1).Item("Sdate")
                                    '    .cells(i, 7).value = drItem(k1).Item("Status")
                                    '    .cells(i, 8).value = drItem(k1).Item("OP")
                                    '    .cells(i, 9).value = drItem(k1).Item("WCDesc")
                                    '    .cells(i, 10).value = Format(drItem(k1).Item("QTY"), "0.00")
                                    '    stBoll = True




                                    'Else
                                    i += 1
                                    strOpN = drItem(k1).Item("OP")
                                    strWCDES = drItem(k1).Item("WCDesc")
                                    strQTY = strQTY + Format(drItem(k1).Item("Qty"), "0.00")
                                    .cells(i, 1).value = drItem(k1).Item("RSCGroup")
                                    .cells(i, 2).value = drItem(k1).Item("FGCode")
                                    .cells(i, 3).value = drItem(k1).Item("FGDesc")
                                    .cells(i, 4).value = drItem(k1).Item("JOB")
                                    .cells(i, 5).value = drItem(k1).Item("ChildR")
                                    .cells(i, 6).value = "'" & drItem(k1).Item("Tdate")
                                    .cells(i, 7).value = "'" & drItem(k1).Item("Sdate")
                                    .cells(i, 8).value = drItem(k1).Item("Status")
                                    .cells(i, 9).value = drItem(k1).Item("OP")
                                    .cells(i, 10).value = drItem(k1).Item("WCDesc")
                                    .cells(i, 11).value = Format(drItem(k1).Item("QTY"), "0.00")
                                    stBoll = True

                                    '  End If


                                Next
                                If stBoll = True Then
                                    i += 1
                                    ExcelA.Range("A" & i, "H" & i).Font.ColorIndex = 3
                                    .cells(i, 1).value = ""
                                    .cells(i, 2).value = ""
                                    .cells(i, 3).value = ""
                                    .cells(i, 4).value = ""
                                    .cells(i, 5).value = ""
                                    .cells(i, 6).value = ""
                                    .cells(i, 7).value = ""
                                    .cells(i, 8).value = ""
                                    .cells(i,9).value = strOpN 'Format(dblScrap / intCount, "0.00")
                                    .cells(i, 10).value = strWCDES
                                    .cells(i, 11).value = Format(strQTY, "0.00")
                                End If
                            End If
                            PB2.Value = M1 + 1
                            Application.DoEvents()
                        Next
                    End If



                End If
                filename = txtFile
                ExcelA.ActiveCell.Worksheet.SaveAs(filename)
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelA)
                ExcelA = Nothing



                MsgBox("Data's are exported to Excel Succesfully in '" & filename & "'", MsgBoxStyle.Information, "eWIP")

                '  lblDataV.Text = "Data has been Successfully Exported"



            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "eWIP")
            delFile = True


        Finally
            Panel3.Visible = False
            cn.Close()
            com.Parameters.Clear()
        End Try

        ' The excel is created and opened for insert value. We most close this excel using this system
        ' Dim pro() As Process = System.Diagnostics.Process.GetProcessesByName("Excel")

        Dim DellocalAll As Process()

        DellocalAll = Process.GetProcesses()


        For i As Integer = 0 To DellocalAll.Length - 1
            If DellocalAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(DellocalAll(i).Id) = False Then
                    DellocalAll(i).Kill()
                End If
            End If
        Next



        'For Each i As Process In pro
        '    If localAll(i).ProcessName.ToUpper = "EXCEL" Then
        '        lstOldProcessID.Items.Add(localAll(i).Id)
        '    End If
        '    i.Kill()
        'Next
        If delFile = True Then
            Try
                If IO.File.Exists(filename) Then
                    IO.File.Delete(filename)
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End Try
        End If
        txtFile = ""


    End Function

    'Private Sub RViewer_ClickPage(ByVal sender As Object, ByVal e As CrystalDecisions.Windows.Forms.PageMouseEventArgs) Handles RViewer.ClickPage
    '    TmVari = 2
    'End Sub
    Function FNCJobNotAction(ByRef Ds As DataSet, ByVal j As Integer, ByVal CreatedDate As String, ByVal DSItem As DataSet, ByVal DSRecWC As DataSet, ByVal DSRecGroup As DataSet, ByVal HASRe As Hashtable, ByRef recount As Boolean, ByRef HashNewI As Hashtable, ByRef exArr() As String, ByRef n1 As Integer) As Boolean
        Dim dsntSt As New DataSet
        Dim sql As String = "select B.*,A.* from tbJobRoute A,tbJob B  where B._job=A._job and A._operationNo in (select Min(_operationNo) from  tbJobRoute where _Job=A._Job) and A._Job not in(select distinct _job from tbJobTrans)"
        dsntSt = clsM.GetDataset(sql, "tbItem")
        Dim drRp As DataRow
        If dsntSt.Tables(0).Rows.Count > 0 Then
            PB1.Maximum = PB1.Maximum + dsntSt.Tables(0).Rows.Count
            Dim i As Integer
            For i = 0 To dsntSt.Tables(0).Rows.Count - 1
                With dsntSt.Tables(0).Rows(i)
                    'If .Item("_Job") = "SD22000785" Then
                    '    MsgBox("SD22000785 ")
                    'End If
                    Dim runt As Boolean = False
                    Dim ArrItem As String() = Split(.Item("_item"), "-")
                    'If ArrItem.Length > 1 Then
                    '    MsgBox(.Item("_item"))
                    'End If
                    If HASRe.ContainsKey(.Item("_item")) = True Then
                        runt = True
                    Else
                        runt = False
                    End If
                    If runt = True Then
                        If .Item("_qtyReleased") <> 0 Then
                            Dim StrItemCode As String = ""
                            Dim StrItemName As String = ""
                            Dim StrRecName As String = ""
                            Dim StrWCCode As String = ""
                            Dim StrWCName As String = ""
                            Dim dblQty As Double
                            Dim StrOP As String = ""
                            Dim strjob As String
                            StrOP = .Item("_operationNo")
                            StrWCCode = .Item("_WC")
                            Dim DRResou() As DataRow = DSRecGroup.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                            If DRResou.Length > 0 Then
                                StrRecName = DRResou(0).Item("_rGroupID")
                                StrWCName = DRResou(0).Item("_description")

                            Else
                                Dim DRResouNew() As DataRow = DSRecWC.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                If DRResouNew.Length > 0 Then
                                    StrWCName = DRResouNew(0).Item("_description")
                                End If
                                StrRecName = ""
                            End If
                            Dim DRItem() As DataRow = DSItem.Tables(0).Select("_itemCode='" & .Item("_item") & "'")

                            StrItemCode = .Item("_item")
                            strjob = UCase(.Item("_job"))
                            If DRItem.Length > 0 Then
                                StrItemName = DRItem(0).Item("_itemdescription")
                            End If
                            ' Dim WeelCount As Integer = GetWeek(DateTime.FromOADate(.Item("_CreateDate")))
                            Dim strID As String = StrItemCode & "||" & CStr(StrOP) & "||" & CStr(strjob)
                            Dim ArrstrID As String = StrRecName & "||" & StrItemCode & "||"

                            'check for Export
                            Dim strUId As String = StrItemCode & "||" & CStr(StrOP) & "||" & CStr(.Item("_WC"))
                            If HashNewI.ContainsKey(strUId) = False Then
                                ReDim Preserve exArr(n1)
                                exArr(n1) = strUId
                                HashNewI.Add(strUId, strUId)
                                If StrItemCode = "AC0001" Then
                                    ' MsgBox("Hi")
                                End If
                                n1 = n1 + 1
                            End If


                            recount = True

                            drRp = dsrsg.Tables("tbItemNewJob").NewRow
                            drRp("sno") = i + 1
                            drRp("Title") = strReportDate ' Trim(txtFrom.Text) '& "-" & Trim(txtTo.Text)
                            drRp("CreateDate") = CreatedDate
                            drRp("ReportDate") = strReportDate
                            Dim StrArr() As String = Split(StrItemName, ",")
                            drRp("SelRSCGroup") = StrRecName
                            drRp("StockCard") = ""
                            drRp("RSCGroup") = StrRecName
                            drRp("FGCode") = StrItemCode
                            drRp("FGDesc") = StrArr(0)
                            drRp("Job") = UCase(.Item("_job"))
                            '  If .Item("_Reworkst") <> "" Then
                            'drRp("ChildR") = .Item("_jobsuffix") & "-" & .Item("_Reworkst")
                            ' Else
                            drRp("ChildR") = "M"
                            ' End If

                            'If .Item("_Start_date") <> 0 Then
                            '  drRp("Sdate") = "" 'Format(DateTime.FromOADate(.Item("_Start_date")), "d MMM yyyy HH:mm")
                            'Else
                            drRp("Sdate") = ""
                            ' End If
                            ' If .Item("_CreateDate") = 0 Then
                            drRp("Tdate") = ""
                            'Else
                            '   drRp("Tdate") = Format(DateTime.FromOADate(.Item("_CreateDate")), "d MMM yyyy HH:mm")
                            ' End If

                            drRp("OP") = StrOP
                            drRp("WC") = StrWCCode
                            drRp("WCDesc") = StrWCName
                            drRp("Qty") = .Item("_qtyReleased")
                            drRp("weekd") = 0
                            drRp("Status") = "NEW" '.Item("_status")
                            dsrsg.Tables("tbItemNewJob").Rows.Add(drRp)

                        End If
                    End If
                End With
                j = j + 1
                PB1.Value = j
                Application.DoEvents()

            Next


        End If
    End Function
    
End Class