Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Microsoft.VisualBasic.DateAndTime
Public Class frmRptItem
    Dim clsM As New clsMain
    Public DSItem As New DataSet
    ' Public ReportTitle As String
    Dim dsrsg As DSReport
    Dim ArrexOP() As Integer
    Dim ArrexRes() As String
    Dim ArrexItem() As String
    Dim strReportDate As String

    Private Sub frmRptItem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblTitle.Text = "CO BY ITEM REPORT"
        dtpFrom.Value = Now
        dtpTo.Value = Now
        txtFrom.Text = Format(Now, "dd/MM/yyyy")
        txtTo.Text = Format(Now, "dd/MM/yyyy")
        GetItem()
    End Sub
    Sub GetItem()
       
        lstItem.Items.Clear()
        Try
            Dim ds As New DataSet
            ds = clsM.GetDataset("select * from tbItem where  _type<>'O' and( _itemCode like('F%') or _itemCode like('FG%') or  _itemCode like('AC%') or  _itemCode like('SC%')) order by _itemCode", "tbItem")
            DSItem = ds
            If IsDBNull(ds) = False Then
                Dim i As Integer
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            ' lstItem.Items.Add(New DataItem(Trim(.Item("_itemCode")), Trim(.Item("_itemCode")) & "  :  " & Trim(.Item("_itemdescription")) & ""))

                            Dim st As String() = Split(.Item("_itemCode"), "-")
                            If st.Length = 1 Then
                                lstItem.Items.Add(New DataItem(Trim(.Item("_itemCode")), Trim(.Item("_itemCode")) & "  :  " & Trim(.Item("_itemdescription")) & ""))
                            End If
                        End With


                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try

    End Sub

    'Private Sub butDone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDone.Click
    '    Try
    '        If Trim(txtFrom.Text) = "" Or Trim(txtTo.Text) = "" Then
    '            MsgBox("Please enter the date!", MsgBoxStyle.Information, "eWIP")
    '            Exit Sub
    '        End If
    '        Dim dblSd As Double = clsM.CheckDate(txtFrom.Text)
    '        If dblSd = 0 Then
    '            MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
    '            Exit Sub
    '        End If

    '        Dim dblEd As Double = clsM.CheckDate(txtTo.Text)
    '        If dblEd = 0 Then
    '            MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
    '            Exit Sub
    '        End If
    '        If lstItem.SelectedIndex < 0 Then
    '            MsgBox("Please select item!", MsgBoxStyle.Information, "eWIP")
    '            Exit Sub
    '        End If
    '        Dim s() As String = Split(txtTo.Text, "/")

    '        Dim Dateto As Date = DateSerial(s(2), s(1), s(0))
    '        Dateto = DateAdd(DateInterval.Minute, -1, DateAdd(DateInterval.Day, 1, Dateto))
    '        dblEd = 0
    '        dblEd = Dateto.ToOADate


    '        Dim StItem As Boolean = False


    '        Dim strReportDate As String = txtFrom.Text & "-" & txtTo.Text
    '        Dim CreatedDate As String = Format(Now, "dd/MM/yyyy")

    '        Dim DSrep As New DataSet
    '        Dim Strsql As String
    '        Dim strResource As String
    '        Dim DSRecGroup As New DataSet
    '        Dim DSRecWC As New DataSet
    '        Dim DSItem As New DataSet
    '        DSItem = clsM.GetDataset("select * from tbItem", "tbItem")
    '        DSRecGroup = clsM.GetDataset("select A.*,B.* from tbWC A,tbResourceGroup B,tbWorkStation C where C._wc=A._wc and C._rid=B._rid", "tbRec")
    '        DSRecWC = clsM.GetDataset("select * from tbWC", "tbWC")

    '        Dim hashFindUnique As New Hashtable
    '        Dim ArrTot() As Double
    '        Dim ArrRej() As Double
    '        Dim ArrPer() As Double
    '        Dim ArrCount() As Double
    '        ArrexOP = Nothing
    '        ArrexItem = Nothing
    '        ArrexRes = Nothing



    '        Strsql = "select * from tbJobTransMain where _end_Date between " & dblSd & " and " & dblEd
    '        DSrep = clsM.GetDataset(Strsql, "tbJobTransMain")
    '        dsrsg = New DSReport
    '        Dim totqty As Double
    '        Dim drRp As DataRow
    '        Dim recount As Boolean = False
    '        PB1.Value = 0
    '        PB1.Minimum = 0
    '        PB1.Maximum = 0
    '        If DSrep.Tables(0).Rows.Count > 0 Then
    '            Dim i As Integer
    '            PB1.Maximum = DSrep.Tables(0).Rows.Count
    '            For i = 0 To DSrep.Tables(0).Rows.Count - 1
    '                With DSrep.Tables(0).Rows(i)
    '                    Dim runt As Boolean = False
    '                    Dim ArrItem As String() = Split(.Item("_item"), "-")

    '                    If lstItem.SelectedItems(0).id = ArrItem(0) Then
    '                        If .Item("_qty_complete") <> 0 Then
    '                            recount = True
    '                            Dim StrItemCode As String = ""
    '                            Dim StrItemName As String = ""
    '                            Dim StrRecName As String = ""
    '                            Dim StrWCCode As String = ""
    '                            Dim StrWCName As String = ""
    '                            Dim dblQty As Double
    '                            Dim StrOP As String = ""
    '                            StrOP = .Item("_oper_num")
    '                            StrWCCode = .Item("_WC")
    '                            Dim DRResou() As DataRow = DSRecGroup.Tables(0).Select("_wc='" & .Item("_WC") & "'")
    '                            If DRResou.Length > 0 Then
    '                                StrRecName = DRResou(0).Item("_rGroupID")
    '                                StrWCName = DRResou(0).Item("_description")
    '                            Else
    '                                Dim DRResouNew() As DataRow = DSRecWC.Tables(0).Select("_wc='" & .Item("_WC") & "'")
    '                                If DRResouNew.Length > 0 Then
    '                                    StrWCName = DRResouNew(0).Item("_description")
    '                                End If
    '                            End If
    '                            Dim DRItem() As DataRow = DSItem.Tables(0).Select("_itemCode='" & .Item("_item") & "'")

    '                            StrItemCode = .Item("_item")
    '                            If DRItem.Length > 0 Then
    '                                StrItemName = DRItem(0).Item("_itemdescription")
    '                            End If







    '                            drRp = dsrsg.Tables("tbRSCGroupReport").NewRow
    '                            drRp("sno") = i + 1
    '                            drRp("Title") = Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
    '                            drRp("CreateDate") = CreatedDate
    '                            drRp("ReportDate") = strReportDate
    '                            Dim StrArr() As String = Split(StrItemName, ",")
    '                            drRp("SelRSCGroup") = StrRecName
    '                            drRp("StockCard") = ""
    '                            drRp("RSCGroup") = StrRecName
    '                            drRp("FGCode") = StrItemCode
    '                            drRp("FGDesc") = StrArr(0)
    '                            drRp("Job") = .Item("_job")
    '                            drRp("Sdate") = Format(DateTime.FromOADate(.Item("_TransDate")), "dd/MM/yyyy HH:mm")
    '                            drRp("Edate") = Format(DateTime.FromOADate(.Item("_end_Date")), "dd/MM/yyyy HH:mm")
    '                            drRp("OP") = StrOP
    '                            drRp("WCDesc") = StrWCName
    '                            drRp("Qty") = .Item("_qty_complete")
    '                            drRp("Rej") = .Item("_qty_scrapped")
    '                            Dim dblCom As Double = .Item("_qty_complete") + .Item("_qty_scrapped")
    '                            Dim dblPer As Double = 0
    '                            If dblCom <> 0 Then
    '                                dblPer = Format(((.Item("_qty_complete") * 100) / dblCom), "00.00")
    '                            Else
    '                                dblPer = 0.0
    '                            End If
    '                            drRp("Per") = Format(dblPer, "00.00")
    '                            totqty = totqty + .Item("_qty_complete")
    '                            drRp("Total") = totqty
    '                            dsrsg.Tables("tbRSCGroupReport").Rows.Add(drRp)



    '                        End If
    '                    End If
    '                End With
    '                PB1.Value = i + 1
    '                Application.DoEvents()
    '            Next
    '        End If
    '        If recount = False Then
    '            drRp = dsrsg.Tables("tbRSCGroupReport").NewRow
    '            Dim StrItemCode As String = ""
    '            Dim StrItemName As String = ""
    '            Dim StrRecName As String = ""
    '            Dim StrWCCode As String = ""
    '            Dim StrWCName As String = ""
    '            Dim dblQty As Double
    '            Dim StrOP As String = ""
    '            StrOP = 0
    '            StrWCCode = ""
    '            drRp = dsrsg.Tables("tbRSCGroupReport").NewRow
    '            drRp("sno") = 0
    '            drRp("Title") = Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
    '            drRp("CreateDate") = CreatedDate
    '            drRp("ReportDate") = strReportDate
    '            drRp("SelRSCGroup") = ""
    '            drRp("StockCard") = ""
    '            drRp("RSCGroup") = StrRecName
    '            drRp("FGCode") = StrItemCode

    '            drRp("FGDesc") = StrItemName
    '            drRp("Job") = ""
    '            drRp("Sdate") = ""
    '            drRp("Edate") = ""
    '            drRp("OP") = val(StrOP)
    '            drRp("WCDesc") = ""
    '            drRp("Qty") = 0
    '            totqty = 0
    '            drRp("Total") = 0
    '            dsrsg.Tables("tbRSCGroupReport").Rows.Add(drRp)
    '        End If
    '        Dim cry As New CryItemReport

    '        If dsrsg.Tables("tbRSCGroupReport").Rows.Count > 0 Then
    '            Dim i As Integer
    '            For i = 0 To dsrsg.Tables("tbRSCGroupReport").Rows.Count - 1
    '                dsrsg.Tables("tbRSCGroupReport").Rows(i).Item("Total") = totqty
    '            Next
    '        End If




    '        cry.SetDataSource(dsrsg)
    '        RViewer.ReportSource = cry
    '        butClose.Visible = True
    '        RViewer.Refresh()
    '        Panel1.Visible = False
    '        RViewer.Visible = True

    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
    '    End Try
    '    TmVari = 2
    'End Sub

    Private Sub butDone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDone.Click
        Try
            If Trim(txtFrom.Text) = "" Or Trim(txtTo.Text) = "" Then
                MsgBox("Please enter the date!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim dblSd As Double = clsM.CheckDate(txtFrom.Text)
            If dblSd = 0 Then
                MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If

            Dim dblEd As Double = clsM.CheckDate(txtTo.Text)
            If dblEd = 0 Then
                MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            If lstItem.SelectedIndex < 0 Then
                MsgBox("Please select item!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim s() As String = Split(txtTo.Text, "/")

            Dim Dateto As Date = DateSerial(s(2), s(1), s(0))
            Dateto = DateAdd(DateInterval.Minute, -1, DateAdd(DateInterval.Day, 1, Dateto))
            dblEd = 0
            dblEd = Dateto.ToOADate

            strReportDate = Format(DateTime.FromOADate(dblSd), "dd MMM yyyy") & " - " & Format(DateTime.FromOADate(dblEd), "dd MMM yyyy")
            Dim StItem As Boolean = False


            ' Dim strReportDate As String = txtFrom.Text & "-" & txtTo.Text
            Dim CreatedDate As String = Format(Now, "dd MMM yyyy")

            Dim DSrep As New DataSet
            Dim Strsql As String
            Dim strResource As String
            Dim DSRecGroup As New DataSet
            Dim DSRecWC As New DataSet
            Dim DSItem As New DataSet
            DSItem = clsM.GetDataset("select * from tbItem", "tbItem")
            DSRecGroup = clsM.GetDataset("select A.*,B.* from tbWC A,tbResourceGroup B,tbWorkStation C where C._wc=A._wc and C._rid=B._rid", "tbRec")
            DSRecWC = clsM.GetDataset("select * from tbWC", "tbWC")

            Dim hashFindUnique As New Hashtable
            Dim ArrTot() As Double
            Dim ArrRej() As Double
            Dim ArrPer() As Double
            Dim ArrCount() As Double
            ArrexOP = Nothing
            ArrexItem = Nothing
            ArrexRes = Nothing



            Strsql = "select * from tbJobTransMain where _end_Date between " & dblSd & " and " & dblEd & " Order by _Job"
            DSrep = clsM.GetDataset(Strsql, "tbJobTransMain")
            dsrsg = New DSReport
            Dim totqty As Double
            Dim drRp As DataRow
            Dim recount As Boolean = False
            PB1.Value = 0
            PB1.Minimum = 0
            PB1.Maximum = 0
            Dim IA As Integer = 0
            If DSrep.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                PB1.Maximum = DSrep.Tables(0).Rows.Count
                For i = 0 To DSrep.Tables(0).Rows.Count - 1
                    With DSrep.Tables(0).Rows(i)
                        Dim runt As Boolean = False
                        ' Dim ArrItem As String() = Split(.Item("_item"), "-")
                        Dim ArrItem As String() = Split(.Item("_item"), "-")
                        Dim strCont As String = ArrItem(0)
                        If UCase(Mid(.Item("_item"), 1, 2)) = "SC" Then
                            strCont = .Item("_item")
                        End If



                        If lstItem.SelectedItems(0).id = strCont Then
                            If .Item("_qty_complete") <> 0 Then
                                recount = True
                                Dim StrItemCode As String = ""
                                Dim StrItemName As String = ""
                                Dim StrRecName As String = ""
                                Dim StrWCCode As String = ""
                                Dim StrWCName As String = ""
                                Dim dblQty As Double
                                Dim StrOP As String = ""
                                StrOP = .Item("_oper_num")
                                StrWCCode = .Item("_WC")
                                Dim DRResou() As DataRow = DSRecGroup.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                If DRResou.Length > 0 Then
                                    StrRecName = DRResou(0).Item("_rGroupID")
                                    StrWCName = DRResou(0).Item("_description")
                                Else
                                    Dim DRResouNew() As DataRow = DSRecWC.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                    If DRResouNew.Length > 0 Then
                                        StrWCName = DRResouNew(0).Item("_description")
                                    End If
                                End If
                                Dim DRItem() As DataRow = DSItem.Tables(0).Select("_itemCode='" & .Item("_item") & "'")

                                StrItemCode = .Item("_item")
                                If DRItem.Length > 0 Then
                                    StrItemName = DRItem(0).Item("_itemdescription")
                                End If


                                Dim WeelCount As Integer = GetWeek(DateTime.FromOADate(.Item("_end_Date")))


                                Dim strID As String = CStr(WeelCount) & "||" & StrItemCode & "||" & CStr(StrOP)
                                Dim ArrstrID As String = CStr(WeelCount) & "||" & StrItemCode & "||"
                                'check for Export
                                If hashFindUnique.ContainsKey(strID) = False Then
                                    hashFindUnique.Add(strID, IA)
                                    ReDim Preserve ArrTot(IA)
                                    ReDim Preserve ArrRej(IA)
                                    ReDim Preserve ArrPer(IA)
                                    ReDim Preserve ArrCount(IA)

                                    ReDim Preserve ArrexOP(IA)
                                    ReDim Preserve ArrexItem(IA)
                                    ReDim Preserve ArrexRes(IA)
                                    ArrexOP(IA) = Val(StrOP)
                                    ArrexItem(IA) = .Item("_item")
                                    ArrexRes(IA) = WeelCount 'StrRecName

                                    ArrTot(IA) = .Item("_qty_complete")
                                    ArrRej(IA) = .Item("_qty_scrapped")

                                    Dim dblCom1 As Double = .Item("_qty_complete") + .Item("_qty_scrapped")
                                    ArrPer(IA) = Format(((.Item("_qty_complete") * 100) / dblCom1), "00.00")
                                    ArrCount(IA) = ArrCount(IA) + 1
                                    IA = IA + 1
                                Else
                                    Dim strNV As Integer = hashFindUnique(strID)
                                    ArrTot(strNV) = .Item("_qty_complete")
                                    ArrRej(strNV) = .Item("_qty_scrapped")

                                    Dim dblCom1 As Double = .Item("_qty_complete") + .Item("_qty_scrapped")
                                    ArrPer(strNV) = ArrPer(strNV) + Format(((.Item("_qty_complete") * 100) / dblCom1), "00.00")
                                    ArrCount(strNV) = ArrCount(strNV) + 1


                                End If
                                'end Check

                                drRp = dsrsg.Tables("tbRSCGroupReport").NewRow
                                drRp("sno") = i + 1
                                drRp("Title") = Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                                drRp("CreateDate") = CreatedDate
                                drRp("ReportDate") = strReportDate
                                Dim StrArr() As String = Split(StrItemName, ",")
                                drRp("SelRSCGroup") = StrRecName
                                drRp("StockCard") = ""
                                drRp("RSCGroup") = StrRecName
                                drRp("FGCode") = StrItemCode
                                drRp("FGDesc") = StrArr(0)
                                drRp("Job") = UCase(.Item("_job"))
                                drRp("Sdate") = Format(DateTime.FromOADate(.Item("_TransDate")), "d MMM yyyy HH:mm")
                                drRp("Edate") = Format(DateTime.FromOADate(.Item("_end_Date")), "d MMM yyyy HH:mm")
                                drRp("OP") = StrOP
                                drRp("WCDesc") = StrWCName
                                drRp("Qty") = .Item("_qty_complete")
                                drRp("Rej") = .Item("_qty_scrapped")
                                drRp("weekd") = WeelCount
                                Dim dblCom As Double = .Item("_qty_complete") + .Item("_qty_scrapped")
                                Dim dblPer As Double = 0
                                If dblCom <> 0 Then
                                    dblPer = Format(((.Item("_qty_complete") * 100) / dblCom), "00.00")
                                Else
                                    dblPer = 0.0
                                End If


                                drRp("Per") = Val(Format(dblPer, "00.00"))
                                totqty = totqty + .Item("_qty_complete")
                                drRp("Total") = totqty

                                dsrsg.Tables("tbRSCGroupReport").Rows.Add(drRp)

                            End If
                        End If
                    End With
                    PB1.Value = i + 1
                    Application.DoEvents()
                Next
            End If
            If recount = False Then
                drRp = dsrsg.Tables("tbRSCGroupReport").NewRow
                Dim StrItemCode As String = ""
                Dim StrItemName As String = ""
                Dim StrRecName As String = ""
                Dim StrWCCode As String = ""
                Dim StrWCName As String = ""
                Dim dblQty As Double
                Dim StrOP As String = ""
                StrOP = 0
                StrWCCode = ""
                drRp = dsrsg.Tables("tbRSCGroupReport").NewRow
                drRp("sno") = 0
                drRp("Title") = strReportDate 'Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                drRp("CreateDate") = CreatedDate
                drRp("ReportDate") = strReportDate
                drRp("SelRSCGroup") = ""
                drRp("StockCard") = ""
                drRp("RSCGroup") = StrRecName
                drRp("FGCode") = StrItemCode

                drRp("FGDesc") = StrItemName
                drRp("Job") = ""
                drRp("Sdate") = ""
                drRp("Edate") = ""
                drRp("OP") = Val(StrOP)
                drRp("WCDesc") = ""
                drRp("Qty") = 0
                totqty = 0
                drRp("Per") = 0
                drRp("Total") = 0
                drRp("weekd") = 0

                hashFindUnique.Add("", 0)
                ReDim Preserve ArrTot(0)
                ReDim Preserve ArrRej(0)
                ReDim Preserve ArrPer(0)
                ReDim Preserve ArrCount(0)
                ArrTot(0) = 0
                ArrRej(0) = 0







                dsrsg.Tables("tbRSCGroupReport").Rows.Add(drRp)
            End If
            Dim cry As New CryItemReport

            dsrsg.Tables(0).DefaultView.Sort = "SelRSCGroup,FGCode,OP ASC"

            If dsrsg.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For i = 0 To dsrsg.Tables("tbRSCGroupReport").Rows.Count - 1
                    Dim strFind As String
                    strFind = dsrsg.Tables("tbRSCGroupReport").Rows(i).Item("weekd") & "||" & dsrsg.Tables("tbRSCGroupReport").Rows(i).Item("FGCode") & "||" & CStr(dsrsg.Tables("tbRSCGroupReport").Rows(i).Item("OP"))

                    Dim strNV As Integer = hashFindUnique(strFind)
                    Dim IntTot As Double
                    IntTot = ArrPer(strNV)
                    Dim IntCnt As Integer
                    IntCnt = ArrCount(strNV)
                    'If IntCnt > 1 Then
                    'MsgBox(IntCnt)
                    'End If
                    If IntCnt > 0 Then
                        dsrsg.Tables("tbRSCGroupReport").Rows(i).Item("totPer") = IntTot / IntCnt
                    Else
                        dsrsg.Tables("tbRSCGroupReport").Rows(i).Item("totPer") = IntTot
                    End If

                    dsrsg.Tables("tbRSCGroupReport").Rows(i).Item("Total") = totqty
                Next
            End If




            cry.SetDataSource(dsrsg)
            RViewer.ReportSource = cry
            butClose.Visible = True
            RViewer.Refresh()
            Panel1.Visible = False
            RViewer.Visible = True
            butExport.Visible = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
        TmVari = 2
    End Sub

    Sub Itemreport()
       
    End Sub
    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtFrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False
    End Sub

    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtTo.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        dtpTo.Checked = False
    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub
    'Private Sub RViewer_ClickPage(ByVal sender As Object, ByVal e As CrystalDecisions.Windows.Forms.PageMouseEventArgs) Handles RViewer.ClickPage
    '    TmVari = 2
    'End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        RViewer.DataBindings.Clear()
        GC.GetGeneration(RViewer)
        RViewer.Visible = False
        butClose.Visible = False
        butExport.Visible = False
        PB1.Value = 0
        PB1.Minimum = 0
        PB1.Maximum = 0
        Panel1.Visible = True
    End Sub


  


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim GetD As Date = DateSerial(Year(Now), 1, 1)
        Dim FirstDate As String = UCase(Format(GetD, "ddd"))
        Dim FistWeek As Integer = 0
        Dim CalFirstDate As Date
        If FirstDate = "SUN" Then
            CalFirstDate = GetD
            FistWeek = 0
        ElseIf FirstDate = "MON" Then
            CalFirstDate = DateAdd(DateInterval.Day, 6, GetD)
            FistWeek = 1
        ElseIf FirstDate = "TUE" Then
            CalFirstDate = DateAdd(DateInterval.Day, 5, GetD)
            FistWeek = 1
        ElseIf FirstDate = "WED" Then
            CalFirstDate = DateAdd(DateInterval.Day, 4, GetD)
            FistWeek = 1
        ElseIf FirstDate = "THU" Then
            CalFirstDate = DateAdd(DateInterval.Day, 3, GetD)
            FistWeek = 1
        ElseIf FirstDate = "FRI" Then
            CalFirstDate = DateAdd(DateInterval.Day, 2, GetD)
            FistWeek = 1
        ElseIf FirstDate = "SAT" Then
            CalFirstDate = DateAdd(DateInterval.Day, 1, GetD)
            FistWeek = 1
        End If
        Dim GetD1 As Date = DateSerial(Year(dtpFrom.Value), Month(dtpFrom.Value), Day(dtpFrom.Value))
        Dim intday As Integer = DateDiff(DateInterval.Day, CalFirstDate, GetD1) + 1
        Dim countWeek As Double
        If intday = 0 Then
            countWeek = 1
        Else
            countWeek = (intday / 7) + FistWeek
        End If
        Dim S() As String = Split(CStr(countWeek), ".")
        Dim allcont As Integer
        If S.Length > 1 Then
            If Val(S(1)) <> 0 Then
                allcont = Val(S(0)) + 1
            Else
                allcont = Val(S(0))
            End If
        Else
            allcont = Val(S(0))
        End If

        MsgBox(allcont)
      
    End Sub




    Function GetWeek(ByVal sd As Date) As Integer
        Dim allcont As Integer
        Try


            Dim GetD As Date = DateSerial(Year(Now), 1, 1)
            Dim FirstDate As String = UCase(Format(GetD, "ddd"))
            Dim FistWeek As Integer = 0
            Dim CalFirstDate As Date
            If FirstDate = "SUN" Then
                CalFirstDate = GetD
                FistWeek = 0
            ElseIf FirstDate = "MON" Then
                CalFirstDate = DateAdd(DateInterval.Day, 6, GetD)
                FistWeek = 1
            ElseIf FirstDate = "TUE" Then
                CalFirstDate = DateAdd(DateInterval.Day, 5, GetD)
                FistWeek = 1
            ElseIf FirstDate = "WED" Then
                CalFirstDate = DateAdd(DateInterval.Day, 4, GetD)
                FistWeek = 1
            ElseIf FirstDate = "THU" Then
                CalFirstDate = DateAdd(DateInterval.Day, 3, GetD)
                FistWeek = 1
            ElseIf FirstDate = "FRI" Then
                CalFirstDate = DateAdd(DateInterval.Day, 2, GetD)
                FistWeek = 1
            ElseIf FirstDate = "SAT" Then
                CalFirstDate = DateAdd(DateInterval.Day, 1, GetD)
                FistWeek = 1
            End If
            Dim GetD1 As Date = DateSerial(Year(sd), Month(sd), Day(sd))
            Dim intday As Integer = DateDiff(DateInterval.Day, CalFirstDate, GetD1) + 1
            Dim countWeek As Double
            If intday = 0 Then
                countWeek = 1
            Else
                countWeek = (intday / 7) + FistWeek
            End If
            Dim S() As String = Split(CStr(countWeek), ".")

            If S.Length > 1 Then
                If Val(S(1)) <> 0 Then
                    allcont = Val(S(0)) + 1
                Else
                    allcont = Val(S(0))
                End If
            Else
                allcont = Val(S(0))
            End If

        Catch ex As Exception
            allcont = 0
        End Try
        Return allcont
    End Function


    Private Sub butExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExport.Click
        ExportDatatoExcel()
    End Sub


    Function ExportDatatoExcel() As Boolean

        Dim txtFile As String
        txtFile = ""
        sfdSave.FileName = ""
        sfdSave.Filter = "Excel File|*.xls"
        sfdSave.Title = "Save an Excel File"
        If sfdSave.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtFile = sfdSave.FileName
        Else
            Exit Function
        End If


        Panel3.Visible = True
        PB2.Value = 0
        PB2.Minimum = 0
        PB2.Maximum = 0
        Dim myTrans As System.Data.SqlClient.SqlTransaction

        Dim delFile As Boolean = False
        '= "C:\ZandenTemp\ComReport.xls"
        Try
            If IO.File.Exists(txtFile) Then
                IO.File.Delete(txtFile)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Function
        Finally
        End Try
        ' Dim at As Integer
        If txtFile = "" Then
            MsgBox("Please enter the file name!", MsgBoxStyle.Information, "eWIP")
            Exit Function
        End If

        Dim str, filename As String
        str = ""
        filename = ""
        Dim col, row As Integer
        col = 0
        row = 0
        Dim strHisNo As String = ""
        Dim strAcrNo As String = ""
        Dim valHis As String
        Dim strHisVal As String = ""
        Dim strCurVal As String = ""


        Dim localAll As Process()

        localAll = Process.GetProcesses()
        Dim lstOldProcessID As New ListBox
        Dim EXhash As New Hashtable
        For i As Integer = 0 To localAll.Length - 1
            If localAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(localAll(i).Id) = False Then
                    EXhash.Add(localAll(i).Id, localAll(i).Id)
                End If
            End If
        Next





        Dim Excel As Object = CreateObject("Excel.Application")


        P1.Visible = True
        Try
            Dim i As Integer = 1
            With Excel
                .SheetsInNewWorkbook = 1
                .Workbooks.Add()
                .Worksheets(1).Select()

                'Dim chartRange As Excel.Range

                'chartRange = .Range("A1", "J1")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "COMPLETED OPERATION BY ITEM"
                'chartRange.HorizontalAlignment = 3
                'chartRange.VerticalAlignment = 3

                'chartRange = .Range("A2", "F2")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "Created Date :" & Format(Now, "dd MMM yyyy")
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1

                'chartRange = .Range("A3", "F3")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "" ' "FG Code :"
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1

                'chartRange = .Range("A4", "F4")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "" ' "FG Description :"
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1


                'chartRange = .Range("G2", "J2")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "Reported Date :" & strReportDate ' txtFrom.Text & "-" & txtTo.Text
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1

                'chartRange = .Range("G3", "J3")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "" '"Total :" & dsrsg.Tables(0).Rows.Count
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1

                'chartRange = .Range("G4", "j4")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "" '"Total :" & dsrsg.Tables(0).Rows.Count
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1



                '   .cells(3, 1).MergeCells = True
                '  .cells(3, 1).EntireRow.Font.Bold = True
                ' .cells(3, 1).value = "Week"
                'chartRange = .Range("A3", "F3")

                .cells(5, 1).MergeCells = True
                .cells(5, 1).EntireRow.Font.Bold = True
                .cells(5, 1).value = "Week"

                .cells(5, 2).MergeCells = True
                .cells(5, 2).EntireRow.Font.Bold = True
                .cells(5, 2).value = "Resource group"


                .cells(5, 3).MergeCells = True
                .cells(5, 3).EntireRow.Font.Bold = True
                .cells(5, 3).value = "Job"

                .cells(5, 4).MergeCells = True
                .cells(5, 4).EntireRow.Font.Bold = True
                .cells(5, 4).value = "Transfer Date"

                .cells(5, 5).MergeCells = True
                .cells(5, 5).EntireRow.Font.Bold = True
                .cells(5, 5).value = "Completed Date"

                .cells(5, 6).MergeCells = True
                .cells(5, 6).EntireRow.Font.Bold = True
                .cells(5, 6).value = "OP"

                .cells(5, 7).MergeCells = True
                .cells(5, 7).EntireRow.Font.Bold = True
                .cells(5, 7).value = "WC Description"

                .cells(5, 8).MergeCells = True
                .cells(5, 8).EntireRow.Font.Bold = True
                .cells(5, 8).value = "Com Qty"

                .cells(5, 9).MergeCells = True
                .cells(5, 9).EntireRow.Font.Bold = True
                .cells(5, 9).value = "Rej QTY"

                .cells(5, 10).MergeCells = True
                .cells(5, 10).EntireRow.Font.Bold = True
                .cells(5, 10).value = "%"




                i = 5

                i += 1

            End With
            Application.DoEvents()
            Dim strFGCode As String = ""
            Dim strFGDesc As String = ""
            Dim AllTot As Double = 0
            If dsrsg.Tables(0).Rows.Count > 0 Then
          
                PB2.Maximum = ArrexRes.Length
                Dim M1 As Integer
                Array.Sort(ArrexOP)
                Array.Sort(ArrexItem)
                Array.Sort(ArrexRes)
                Dim HashCheck As New Hashtable
                For M1 = 0 To ArrexRes.Length - 1
                    Dim N1 As Integer
                    For N1 = 0 To ArrexItem.Length - 1
                        Dim j1 As Integer
                        For j1 = 0 To ArrexOP.Length - 1

                            Dim strChV As String = ArrexRes(M1) & "||" & ArrexItem(N1) & "||" & ArrexOP(j1)
                            If HashCheck.ContainsKey(strChV) = False Then
                                Dim RWdata() As DataRow = dsrsg.Tables(0).Select("weekd=" & Val(ArrexRes(M1)) & " and FGCode='" & ArrexItem(N1) & "' and OP=" & ArrexOP(j1), "weekd,OP,Job")
                                If RWdata.Length > 0 Then
                                    Dim k1 As Integer

                                    Dim strDe As String = ""
                                    Dim strComQ As Double = 0
                                    Dim strRejQ As Double = 0
                                    Dim strPerQ As Double = 0
                                    Dim StrWK As String = ""
                                    Dim StrRSG As String = ""
                                    Dim StOpNo As String = ""
                                    For k1 = 0 To RWdata.Length - 1
                                        With RWdata(k1)
                                            StOpNo = .Item("OP")
                                            Excel.cells(i, 1).value = .Item("weekd")
                                            StrWK = .Item("weekd")
                                            Excel.cells(i, 2).value = .Item("RSCGroup")
                                            StrRSG = .Item("RSCGroup")
                                            Excel.cells(i, 3).value = .Item("Job")
                                            Excel.cells(i, 4).value = "'" & .Item("Sdate") 'DateTime.FromOADate(.Item("Sdate"))
                                            Excel.cells(i, 5).value = "'" & .Item("Edate") 'DateTime.FromOADate(.Item("Edate"))
                                            Excel.cells(i, 6).value = .Item("OP")
                                            Excel.cells(i, 7).value = .Item("WCDesc")
                                            Excel.cells(i, 8).value = .Item("Qty")
                                            Excel.cells(i, 9).value = .Item("Rej")
                                            ' Excel.cells(i, 10).value = .Item("Total")
                                            Excel.cells(i, 10).value = .Item("Per")
                                            strDe = .Item("WCDesc")
                                            strComQ = strComQ + .Item("Qty")
                                            strRejQ = strRejQ + .Item("Rej")
                                            strPerQ = .Item("totPer")
                                            strFGCode = .Item("FGCode")
                                            strFGDesc = .Item("FGDesc")
                                            i = i + 1
                                        End With
                                    Next
                                    'Dim chartRange As Excel.Range
                                    'Excel.Range("A" & i, "J" & i).Font.ColorIndex = 3
                                    '' chartRange1 = Excel.Range("A" & i, "L" & i)
                                    'Excel.cells(i, 1).value = StrWK
                                    'Excel.cells(i, 2).value = StrRSG

                                    'chartRange = Excel.Range("C" & i, "E" & i)
                                    ''chartRange.Interior.ColorIndex = 27
                                    'AllTot = AllTot + strComQ
                                    'chartRange.Merge()
                                    'chartRange.FormulaR1C1 = ""
                                    'chartRange.HorizontalAlignment = 3
                                    'chartRange.VerticalAlignment = 3
                                    'Excel.cells(i, 6).value = StOpNo ' ArrexOP(j1)
                                    ''Excel.cells(i, 7).Interior.ColorIndex = 27
                                    'Excel.cells(i, 7).value = strDe
                                    ''Excel.cells(i, 8).Interior.ColorIndex = 27
                                    'Excel.cells(i, 8).value = strComQ
                                    ''Excel.cells(i, 9).Interior.ColorIndex = 27
                                    'Excel.cells(i, 9).value = strRejQ
                                    ''Excel.cells(i, 10).Interior.ColorIndex = 27
                                    'Excel.cells(i, 10).value = strPerQ
                                    '' Excel.cells(i, 11).Interior.ColorIndex = 27

                                    strRejQ = 0
                                    strComQ = 0

                                    i = i + 1




                                End If
                                HashCheck.Add(strChV, strChV)

                            End If
                        Next
                    Next
                    PB2.Value = M1 + 1
                Next

            End If
            Excel.cells(3, 1) = "FG Code :" & strFGCode
            Excel.cells(4, 1) = "FG Description :" & strFGDesc
            Excel.cells(4, 7) = "Total :" & AllTot
            filename = txtFile
            Excel.ActiveCell.Worksheet.SaveAs(filename)
            System.Runtime.InteropServices.Marshal.ReleaseComObject(Excel)
            Excel = Nothing



            MsgBox("Data's are exported to Excel Succesfully in '" & filename & "'", MsgBoxStyle.Information, "eWIP")

            '  lblDataV.Text = "Data has been Successfully Exported"




        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "eWIP")
            delFile = True


        Finally
            Panel3.Visible = False
            cn.Close()
            com.Parameters.Clear()
        End Try



        Dim DellocalAll As Process()

        DellocalAll = Process.GetProcesses()


        For i As Integer = 0 To DellocalAll.Length - 1
            If DellocalAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(DellocalAll(i).Id) = False Then
                    DellocalAll(i).Kill()
                End If
            End If
        Next




        ' The excel is created and opened for insert value. We most close this excel using this system
        'Dim pro() As Process = System.Diagnostics.Process.GetProcessesByName("EXCEL")

        'For Each i As Process In pro
        '    i.Kill()
        'Next
        If delFile = True Then
            Try
                If IO.File.Exists(filename) Then
                    IO.File.Delete(filename)
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End Try
        End If
        txtFile = ""


    End Function
End Class