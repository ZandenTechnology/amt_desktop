Imports System.Data.SqlClient
Public Class frmHandSup
    Public IDNo As Integer
    Dim Dr As SqlDataReader
    Dim parm As SqlParameter
    Private Sub txtHandoverid_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtHandoverid.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If (txtHandoverid.Text) <> "" Then

                If txtHandoverid.TextLength >= 2 Then
                    Dim idv As String = UCase(Mid(txtHandoverid.Text, 1, 2))
                    Dim ACV As String = UCase(Mid(txtHandoverid.Text, 3, txtHandoverid.TextLength))
                    If UCase(idv) = "ID" Then
                        txtHandoverid.Text = ACV
                    End If
                    ' If idv = "ID" Then
                    If lblEMPID.Text <> Trim(txtHandoverid.Text) And lblEMPID.Text <> "" Then
                        If Check_opIDhand(txtHandoverid.Text) = True Then
                            butSave.Focus()
                        Else
                            MessageBox.Show("Please check Handover id(" & txtHandoverid.Text & ")")
                            txtHandoverid.Text = ""
                            txtHandoverName.Text = ""

                            txtHandoverid.Focus()
                            Exit Sub
                        End If

                    Else
                        MessageBox.Show("Please check Handover id(" & txtHandoverid.Text & ")")
                        txtHandoverid.Text = ""
                        txtHandoverName.Text = ""
                        txtHandoverid.Focus()
                        Exit Sub
                    End If




                End If
            End If
        End If
        ' End If
    End Sub

    Function Check_opIDhand(ByVal idno As String) As Boolean
        Check_opIDhand = False
        txtHandoverid.Text = ""
        txtHandoverName.Text = ""
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "SELECT     * FROM  tbOperator where _operatorID='" & fncstr(idno) & "'"
                cn.Open()
                Dr = .ExecuteReader()
                If Dr.Read Then
                    txtHandoverid.Text = Dr.Item("_operatorID")
                    txtHandoverName.Text = Dr.Item("_operatorName")
                    Check_opIDhand = True
                Else
                    MsgBox("Please check your operator code(" & idno & " )!", MsgBoxStyle.Information, "eWIP")
                    txtHandoverid.Text = ""
                    txtHandoverName.Text = ""
                    Check_opIDhand = False
                End If
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Check_opIDhand = False
            txtHandoverid.Text = ""
            txtHandoverName.Text = ""
        Finally
            cn.Close()
        End Try

        'If Check_opID = False Then
        '    lblMsgBox.Text = "Please check your operator code(" & idno & " )!"
        'End If
        Return Check_opIDhand

    End Function

    Function SaveData() As Boolean
        SaveData = False
        If IDNo = 0 Then
            MsgBox("Please Check data!", MsgBoxStyle.Information, "eWIP")
            txtHandoverid.Text = ""
            txtHandoverName.Text = ""
            txtHandoverid.Focus()
            Return SaveData
            Exit Function
        End If
        If Trim(txtHandoverid.Text) = "" Then
            MsgBox("Please enter handover id!", MsgBoxStyle.Information, "eWIP")
            txtHandoverid.Text = ""
            txtHandoverName.Text = ""
            txtHandoverid.Focus()
            Return SaveData
            Exit Function

        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "sp_JobHandover_Add"
                parm = .Parameters.Add("@tansnum", SqlDbType.Int)
                parm.Value = Val(IDNo)
                parm = .Parameters.Add("@Handoverid", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtHandoverid.Text)
                parm = .Parameters.Add("@HandoverName", SqlDbType.VarChar, 255)
                parm.Value = Trim(txtHandoverName.Text)
                cn.Open()
                .ExecuteNonQuery()
                SaveData = True
                cn.Close()
                com.Parameters.Clear()
                '   txtWKID.Select()
                MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")
            End With


        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try

        Return SaveData
    End Function

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If SaveData() = True Then
            ChHandOverID = txtHandoverid.Text
            ChHandOverName = txtHandoverName.Text
            Try
                ' lblMsgBox.Text = ""
                Me.Close()

            Catch ex As Exception
            End Try

        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub
End Class