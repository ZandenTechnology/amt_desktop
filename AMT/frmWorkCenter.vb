Public Class frmWorkCenter

    Private Sub frmWorkCenter_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtWCCode.TabIndex = 1

        Panel1.Focus()
        Panel2.Focus()
        Panel3.Focus()
        txtOperatorID.Focus()
        With lstv
            .Columns.Add("IDNo", 0, HorizontalAlignment.Left)
            .Columns.Add("Operator ID", 230, HorizontalAlignment.Left)
            .Columns.Add("Operator Name", lstv.Width - 330, HorizontalAlignment.Left)
            .Columns.Add("Skip", 50, HorizontalAlignment.Left)
            .Columns.Add("Split", 50, HorizontalAlignment.Left)
            list_Displaydata()
        End With
        txtOperatorID.Text = 1
        txtOperatorID.Focus()
        txtOperatorID.Text = ""
    End Sub
End Class