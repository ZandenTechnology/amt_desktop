<[Global].Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
[Partial] Class frmExportData
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components [IsNot] Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExportData))
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.DBG1 = New System.Windows.Forms.DataGridView
        Me.Label3 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.txtRedoDate = New System.Windows.Forms.TextBox
        Me.butRedo = New System.Windows.Forms.Button
        Me.lblTotal = New System.Windows.Forms.Label
        Me.lblDataV = New System.Windows.Forms.Label
        Me.lblRunning = New System.Windows.Forms.Label
        Me.butCancel = New System.Windows.Forms.Button
        Me.P1 = New System.Windows.Forms.Panel
        Me.PB1 = New System.Windows.Forms.ProgressBar
        Me.butExport = New System.Windows.Forms.Button
        Me.butBrows = New System.Windows.Forms.Button
        Me.txtFile = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.sfdSave = New System.Windows.Forms.SaveFileDialog
        Me.Panel2.SuspendLayout()
        CType(Me.DBG1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.P1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.DBG1)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Location = New System.Drawing.Point(5, 2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(760, 215)
        Me.Panel2.TabIndex = 1
        '
        'DBG1
        '
        Me.DBG1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DBG1.Location = New System.Drawing.Point(16, 190)
        Me.DBG1.Name = "DBG1"
        Me.DBG1.Size = New System.Drawing.Size(731, 10)
        Me.DBG1.TabIndex = 36
        Me.DBG1.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(20, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 15)
        Me.Label3.TabIndex = 35
        Me.Label3.Text = "EXPORT DATA"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.txtRedoDate)
        Me.Panel3.Controls.Add(Me.butRedo)
        Me.Panel3.Controls.Add(Me.lblTotal)
        Me.Panel3.Controls.Add(Me.lblDataV)
        Me.Panel3.Controls.Add(Me.lblRunning)
        Me.Panel3.Controls.Add(Me.butCancel)
        Me.Panel3.Controls.Add(Me.P1)
        Me.Panel3.Controls.Add(Me.butExport)
        Me.Panel3.Controls.Add(Me.butBrows)
        Me.Panel3.Controls.Add(Me.txtFile)
        Me.Panel3.Controls.Add(Me.Label6)
        Me.Panel3.Location = New System.Drawing.Point(16, 33)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(734, 173)
        Me.Panel3.TabIndex = 0
        '
        'txtRedoDate
        '
        Me.txtRedoDate.BackColor = System.Drawing.Color.White
        Me.txtRedoDate.Location = New System.Drawing.Point(368, 136)
        Me.txtRedoDate.Name = "txtRedoDate"
        Me.txtRedoDate.ReadOnly = True
        Me.txtRedoDate.Size = New System.Drawing.Size(165, 20)
        Me.txtRedoDate.TabIndex = 67
        Me.txtRedoDate.Visible = False
        '
        'butRedo
        '
        Me.butRedo.BackColor = System.Drawing.Color.DimGray
        Me.butRedo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butRedo.ForeColor = System.Drawing.Color.White
        Me.butRedo.Location = New System.Drawing.Point(14, 134)
        Me.butRedo.Name = "butRedo"
        Me.butRedo.Size = New System.Drawing.Size(152, 23)
        Me.butRedo.TabIndex = 66
        Me.butRedo.Text = "Redo(Last Export)"
        Me.butRedo.UseVisualStyleBackColor = False
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(678, 108)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(14, 13)
        Me.lblTotal.TabIndex = 62
        Me.lblTotal.Text = "0"
        '
        'lblDataV
        '
        Me.lblDataV.AutoSize = True
        Me.lblDataV.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDataV.ForeColor = System.Drawing.Color.Green
        Me.lblDataV.Location = New System.Drawing.Point(17, 79)
        Me.lblDataV.Name = "lblDataV"
        Me.lblDataV.Size = New System.Drawing.Size(0, 16)
        Me.lblDataV.TabIndex = 65
        '
        'lblRunning
        '
        Me.lblRunning.AutoSize = True
        Me.lblRunning.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRunning.Location = New System.Drawing.Point(301, 134)
        Me.lblRunning.Name = "lblRunning"
        Me.lblRunning.Size = New System.Drawing.Size(14, 13)
        Me.lblRunning.TabIndex = 61
        Me.lblRunning.Text = "0"
        '
        'butCancel
        '
        Me.butCancel.BackColor = System.Drawing.Color.DimGray
        Me.butCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCancel.ForeColor = System.Drawing.Color.White
        Me.butCancel.Location = New System.Drawing.Point(557, 34)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(62, 23)
        Me.butCancel.TabIndex = 1
        Me.butCancel.Text = "Cancel"
        Me.butCancel.UseVisualStyleBackColor = False
        '
        'P1
        '
        Me.P1.BackColor = System.Drawing.Color.Gray
        Me.P1.Controls.Add(Me.PB1)
        Me.P1.Location = New System.Drawing.Point(14, 97)
        Me.P1.Name = "P1"
        Me.P1.Size = New System.Drawing.Size(658, 34)
        Me.P1.TabIndex = 64
        '
        'PB1
        '
        Me.PB1.Location = New System.Drawing.Point(2, 0)
        Me.PB1.Name = "PB1"
        Me.PB1.Size = New System.Drawing.Size(653, 34)
        Me.PB1.TabIndex = 0
        '
        'butExport
        '
        Me.butExport.BackColor = System.Drawing.Color.DimGray
        Me.butExport.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butExport.ForeColor = System.Drawing.Color.White
        Me.butExport.Location = New System.Drawing.Point(492, 34)
        Me.butExport.Name = "butExport"
        Me.butExport.Size = New System.Drawing.Size(62, 23)
        Me.butExport.TabIndex = 0
        Me.butExport.Text = "Export"
        Me.butExport.UseVisualStyleBackColor = False
        '
        'butBrows
        '
        Me.butBrows.BackColor = System.Drawing.Color.DimGray
        Me.butBrows.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBrows.ForeColor = System.Drawing.Color.White
        Me.butBrows.Location = New System.Drawing.Point(467, 37)
        Me.butBrows.Name = "butBrows"
        Me.butBrows.Size = New System.Drawing.Size(17, 16)
        Me.butBrows.TabIndex = 8
        Me.butBrows.Text = "..."
        Me.butBrows.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butBrows.UseVisualStyleBackColor = False
        '
        'txtFile
        '
        Me.txtFile.BackColor = System.Drawing.Color.White
        Me.txtFile.Location = New System.Drawing.Point(83, 35)
        Me.txtFile.Name = "txtFile"
        Me.txtFile.ReadOnly = True
        Me.txtFile.Size = New System.Drawing.Size(382, 20)
        Me.txtFile.TabIndex = 63
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(14, 39)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(63, 13)
        Me.Label6.TabIndex = 62
        Me.Label6.Text = "File Name"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "icon_check.jpg")
        Me.ImageList1.Images.SetKeyName(1, "alert_icon_error.jpg")
        Me.ImageList1.Images.SetKeyName(2, "icon_tick.jpg")
        '
        'frmExportData
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Ivory
        Me.ClientSize = New System.Drawing.Size(771, 219)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel2)
        Me.Name = "frmExportData"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.DBG1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.P1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents P1 As System.Windows.Forms.Panel
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents lblRunning As System.Windows.Forms.Label
    Friend WithEvents PB1 As System.Windows.Forms.ProgressBar
    Friend WithEvents butBrows As System.Windows.Forms.Button
    Friend WithEvents txtFile As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butExport As System.Windows.Forms.Button
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents sfdSave As System.Windows.Forms.SaveFileDialog
    Friend WithEvents DBG1 As System.Windows.Forms.DataGridView
    Friend WithEvents lblDataV As System.Windows.Forms.Label
    Friend WithEvents butRedo As System.Windows.Forms.Button
    Friend WithEvents txtRedoDate As System.Windows.Forms.TextBox
End Class
