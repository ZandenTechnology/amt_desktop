Imports System.Data.SqlClient
Imports System.Runtime.InteropServices
'Imports Excel
Imports Microsoft.VisualBasic.DateAndTime
Public Class frmExportData

    Dim parm As SqlParameter

    Private Sub butBrows_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBrows.Click
        txtFile.Text = ""
        sfdSave.FileName = ""
        sfdSave.Filter = "Excel File|*.xls"
        sfdSave.Title = "Save an Excel File"
        If sfdSave.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtFile.Text = sfdSave.FileName
        End If
    End Sub

    Private Sub butExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExport.Click
        ''Dim clsM As New clsMain
        ''lblDataV.Text = "Data Verify to Export.."
        ''lblRunning.Text = 0
        ' '' Dim at As Integer
        ''If txtFile.Text = "" Then
        ''    MsgBox("Please enter the file name!", MsgBoxStyle.Information, "eWIP")
        ''    Exit Sub
        ''End If
        ''lblTotal.Text = 0
        ''lblRunning.Text = 0
        ''P1.Visible = True

        ''Try
        ''    Dim dsTrans As New DataSet
        ''    Dim dsTransHis As New DataSet
        ''    Dim strSqlTrans As String = "select * from tbJobTrans where _export='N' and _status='COMPLETED' order by _oper_num"
        ''    dsTrans = clsM.GetDataset(strSqlTrans, "tbJobTrans")
        ''    Dim strSqlHis As String = "select * from tbJobTransHis where _exportHis='N' and _status='COMPLETED'"
        ''    dsTransHis = clsM.GetDataset(strSqlHis, "tbJobTrans")
        ''    Dim HsHis As New Hashtable
        ''    Dim i As Integer
        ''    Dim dsExport As New DSExportData
        ''    Dim drExpRow As DataRow
        ''    Dim m As Integer = 0
        ''    Dim HsCur As New Hashtable
        ''    lblTotal.Text = dsTrans.Tables(0).Rows.Count
        ''    PB1.Value = 0
        ''    PB1.Minimum = 0

        ''    If dsTrans.Tables(0).Rows.Count > 0 Then
        ''        PB1.Maximum = dsTrans.Tables(0).Rows.Count
        ''        For i = 0 To dsTrans.Tables(0).Rows.Count - 1
        ''            With dsTrans.Tables(0).Rows(i)

        ''                If Not HsCur.ContainsKey(.Item("_tansnum")) = True Then
        ''                    drExpRow = dsExport.Tables("tbExport").NewRow
        ''                    m = m + 1
        ''                    drExpRow("idno") = m
        ''                    drExpRow("Job") = .Item("_job")
        ''                    drExpRow("JobSuffix") = .Item("_jobsuffix")
        ''                    drExpRow("JobParent") = .Item("_jobsuffixParent")
        ''                    drExpRow("Jobdate") = .Item("_jobDate")
        ''                    drExpRow("Operation") = .Item("_oper_num")
        ''                    drExpRow("ItemName") = .Item("_item")
        ''                    drExpRow("WC") = .Item("_wc")
        ''                    drExpRow("WCDes") = .Item("_wc")
        ''                    If .Item("_reworkst") = "R" Then
        ''                        Dim DSCheckTransHis As New DataSet
        ''                        DSCheckTransHis = clsM.GetDataset("select * from tbJobTransHis where _job='" & .Item("_job") & "' and  _jobsuffix='" & .Item("_jobsuffix") & "' and _jobsuffixParent='" & .Item("_jobsuffixParent") & "' and _oper_num=" & .Item("_oper_num") & " and _reworkCount=" & .Item("_reworkCount") - 1, "TBCheckData")
        ''                        If DSCheckTransHis.Tables(0).Rows.Count > 0 Then




        ''                            Dim DSRecCheck As New DataSet
        ''                            DSRecCheck = clsM.GetDataset("select * from tbJobTransHis where _job='" & .Item("_job") & "' and  _jobsuffix='" & .Item("_jobsuffix") & "' and _jobsuffixParent='" & .Item("_jobsuffixParent") & "' and _oper_num<=" & .Item("_oper_num") & " and  _reworkCount=" & .Item("_reworkCount") - 1 & " ORDER BY _oper_num", "TBCheckData")
        ''                            Dim dblRJQ As Double = 0
        ''                            Dim dblInt As Double = 0
        ''                            Dim dblRec As Double = 0
        ''                            Dim l1 As Integer
        ''                            If DSRecCheck.Tables(0).Rows.Count > 0 Then
        ''                                For l1 = 0 To DSRecCheck.Tables(0).Rows.Count - 1
        ''                                    If DSRecCheck.Tables(0).Rows(l1).Item("_oper_num") = .Item("_oper_num") Then
        ''                                        dblInt = DSRecCheck.Tables(0).Rows(l1).Item("_qty_scrapped")
        ''                                    Else
        ''                                        dblRJQ = dblRJQ + DSRecCheck.Tables(0).Rows(l1).Item("_qty_scrapped")
        ''                                    End If
        ''                                Next
        ''                                drExpRow("Received") = dblRJQ * (-1)
        ''                                drExpRow("Completed") = (dblRJQ + dblInt) * (-1)
        ''                                drExpRow("Rejected") = dblInt * (1)
        ''                            Else
        ''                                drExpRow("Received") = .Item("_qty_op_qty")
        ''                                drExpRow("Completed") = .Item("_qty_complete")
        ''                                drExpRow("Rejected") = .Item("_qty_scrapped")
        ''                            End If
        ''                        Else
        ''                            drExpRow("Received") = .Item("_qty_op_qty")
        ''                            drExpRow("Completed") = .Item("_qty_complete")
        ''                            drExpRow("Rejected") = .Item("_qty_scrapped")
        ''                        End If

        ''                    Else
        ''                        drExpRow("Received") = .Item("_qty_op_qty")
        ''                        drExpRow("Completed") = .Item("_qty_complete")
        ''                        drExpRow("Rejected") = .Item("_qty_scrapped")
        ''                    End If

        ''                    drExpRow("NextOp") = .Item("_nextoper_num")
        ''                    drExpRow("StartTime") = .Item("_start_Date")
        ''                    drExpRow("EndTime") = .Item("_end_Date")
        ''                    drExpRow("remarks1") = ""
        ''                    drExpRow("remarks2") = ""
        ''                    drExpRow("remarks3") = ""

        ''                    Dim dsRej As New DataSet
        ''                    dsRej = clsM.GetDataset("select * from tbRejectedTrans where _refID=" & .Item("_tansnum"), "tbRejectedTrans")
        ''                    If dsRej.Tables(0).Rows.Count > 0 Then
        ''                        drExpRow("remarks1") = dsRej.Tables(0).Rows(0).Item("_RejectedDesc")
        ''                    End If
        ''                    If dsRej.Tables(0).Rows.Count > 1 Then
        ''                        drExpRow("remarks2") = dsRej.Tables(0).Rows(1).Item("_RejectedDesc")
        ''                    End If
        ''                    If dsRej.Tables(0).Rows.Count > 2 Then
        ''                        drExpRow("remarks2") = dsRej.Tables(0).Rows(2).Item("_RejectedDesc")
        ''                    End If


        ''                    dsExport.Tables("tbExport").Rows.Add(drExpRow)
        ''                    HsCur.Add(.Item("_tansnum"), .Item("_tansnum"))
        ''                End If

        ''                If .Item("_reworkst") = "R" Then

        ''                    Dim DSGetAcRewk As New DataSet


        ''                    DSGetAcRewk = clsM.GetDataset("select * from tbJobTransHis where _job='" & .Item("_job") & "' and  _jobsuffix='" & .Item("_jobsuffix") & "' and _jobsuffixParent='" & .Item("_jobsuffixParent") & "' and _oper_num<" & .Item("_oper_num") & " and _reworkCount=" & .Item("_reworkCount") & " order by _reworkCount", "tbJobHis")

        ''                    Dim IntACRejQty As Double
        ''                    Dim X As Integer
        ''                    If DSGetAcRewk.Tables(0).Rows.Count > 0 Then
        ''                        For X = 0 To DSGetAcRewk.Tables(0).Rows.Count - 1
        ''                            IntACRejQty = IntACRejQty + DSGetAcRewk.Tables(0).Rows(X).Item("_qty_scrapped")
        ''                        Next
        ''                        If IntACRejQty <> 0 Then

        ''                            drExpRow = dsExport.Tables("tbExport").NewRow
        ''                            m = m + 1
        ''                            drExpRow("idno") = m
        ''                            drExpRow("Job") = .Item("_job")
        ''                            drExpRow("JobSuffix") = .Item("_jobsuffix")
        ''                            drExpRow("JobParent") = .Item("_jobsuffixParent")
        ''                            drExpRow("Jobdate") = .Item("_jobDate")
        ''                            drExpRow("Operation") = .Item("_oper_num")
        ''                            drExpRow("ItemName") = .Item("_item")
        ''                            drExpRow("WC") = .Item("_wc")
        ''                            drExpRow("WCDes") = .Item("_wc")
        ''                            drExpRow("Received") = IntACRejQty * (-1)
        ''                            drExpRow("Completed") = IntACRejQty * (-1)
        ''                            drExpRow("Rejected") = 0
        ''                            drExpRow("NextOp") = .Item("_nextoper_num")
        ''                            drExpRow("StartTime") = .Item("_start_Date")
        ''                            drExpRow("EndTime") = .Item("_end_Date")
        ''                            drExpRow("remarks1") = ""
        ''                            drExpRow("remarks2") = ""
        ''                            drExpRow("remarks3") = ""
        ''                            dsExport.Tables("tbExport").Rows.Add(drExpRow)
        ''                        End If
        ''                    End If






        ''                    'History Records Claculation



        ''                    Dim DSReworkHIsCount As New DataSet
        ''                    DSReworkHIsCount = clsM.GetDataset("select * from tbJobTransHis where _job='" & .Item("_job") & "' and  _jobsuffix='" & .Item("_jobsuffix") & "' and _jobsuffixParent='" & .Item("_jobsuffixParent") & "' and _oper_num=" & .Item("_oper_num") & " order by _reworkCount", "tbJobHis")
        ''                    If DSReworkHIsCount.Tables(0).Rows.Count > 0 Then
        ''                        Dim k As Integer
        ''                        For k = 0 To DSReworkHIsCount.Tables(0).Rows.Count - 1
        ''                            If DSReworkHIsCount.Tables(0).Rows(k).Item("_reworkCount") = 0 Then
        ''                                If DSReworkHIsCount.Tables(0).Rows(k).Item("_exportHis") = "N" Then
        ''                                    drExpRow = dsExport.Tables("tbExport").NewRow
        ''                                    m = m + 1
        ''                                    drExpRow("idno") = m
        ''                                    drExpRow("Job") = DSReworkHIsCount.Tables(0).Rows(k).Item("_job")
        ''                                    drExpRow("JobSuffix") = DSReworkHIsCount.Tables(0).Rows(k).Item("_jobsuffix")
        ''                                    drExpRow("JobParent") = DSReworkHIsCount.Tables(0).Rows(k).Item("_jobsuffixParent")
        ''                                    drExpRow("Jobdate") = DSReworkHIsCount.Tables(0).Rows(k).Item("_jobDate")
        ''                                    drExpRow("Operation") = DSReworkHIsCount.Tables(0).Rows(k).Item("_oper_num")
        ''                                    drExpRow("ItemName") = DSReworkHIsCount.Tables(0).Rows(k).Item("_item")
        ''                                    drExpRow("WC") = DSReworkHIsCount.Tables(0).Rows(k).Item("_wc")
        ''                                    drExpRow("WCDes") = DSReworkHIsCount.Tables(0).Rows(k).Item("_wc")
        ''                                    drExpRow("Received") = DSReworkHIsCount.Tables(0).Rows(k).Item("_qty_op_qty")
        ''                                    drExpRow("Completed") = DSReworkHIsCount.Tables(0).Rows(k).Item("_qty_complete")
        ''                                    drExpRow("Rejected") = DSReworkHIsCount.Tables(0).Rows(k).Item("_qty_scrapped")
        ''                                    drExpRow("NextOp") = DSReworkHIsCount.Tables(0).Rows(k).Item("_nextoper_num")
        ''                                    drExpRow("StartTime") = DSReworkHIsCount.Tables(0).Rows(k).Item("_start_Date")
        ''                                    drExpRow("EndTime") = DSReworkHIsCount.Tables(0).Rows(k).Item("_end_Date")
        ''                                    drExpRow("remarks1") = ""
        ''                                    drExpRow("remarks2") = ""
        ''                                    drExpRow("remarks3") = ""
        ''                                    Dim dsRej As New DataSet
        ''                                    dsRej = clsM.GetDataset("select * from tbRejectedTrans where _refID=" & .Item("_tansnum"), "tbRejectedTrans")
        ''                                    If dsRej.Tables(0).Rows.Count > 0 Then
        ''                                        drExpRow("remarks1") = dsRej.Tables(0).Rows(0).Item("_RejectedDesc")
        ''                                    End If
        ''                                    If dsRej.Tables(0).Rows.Count > 1 Then
        ''                                        drExpRow("remarks2") = dsRej.Tables(0).Rows(1).Item("_RejectedDesc")
        ''                                    End If
        ''                                    If dsRej.Tables(0).Rows.Count > 2 Then
        ''                                        drExpRow("remarks2") = dsRej.Tables(0).Rows(2).Item("_RejectedDesc")
        ''                                    End If
        ''                                    dsExport.Tables("tbExport").Rows.Add(drExpRow)
        ''                                    HsHis.Add(DSReworkHIsCount.Tables(0).Rows(k).Item("_tansnum"), DSReworkHIsCount.Tables(0).Rows(k).Item("_tansnum"))
        ''                                Else
        ''                                    HsHis.Add(DSReworkHIsCount.Tables(0).Rows(k).Item("_tansnum"), DSReworkHIsCount.Tables(0).Rows(k).Item("_tansnum"))
        ''                                End If
        ''                            Else
        ''                                If DSReworkHIsCount.Tables(0).Rows(k).Item("_exportHis") = "N" Then
        ''                                    Dim dsRjcHisCount As New DataSet
        ''                                    dsRjcHisCount = clsM.GetDataset("select * from tbJobTransHis where _job='" & .Item("_job") & "' and  _jobsuffix='" & .Item("_jobsuffix") & "' and _jobsuffixParent='" & .Item("_jobsuffixParent") & "' and _oper_num<" & .Item("_oper_num") & " and _reworkCount=" & DSReworkHIsCount.Tables(0).Rows(k).Item("_reworkCount") & " order by _reworkCount", "tbJobHis")
        ''                                    Dim IntRejQty As Double
        ''                                    Dim y As Integer
        ''                                    If dsRjcHisCount.Tables(0).Rows.Count > 0 Then
        ''                                        For y = 0 To dsRjcHisCount.Tables(0).Rows.Count - 1
        ''                                            IntRejQty = IntRejQty + dsRjcHisCount.Tables(0).Rows(y).Item("_qty_scrapped")
        ''                                        Next
        ''                                        If IntRejQty <> 0 Then
        ''                                            drExpRow = dsExport.Tables("tbExport").NewRow
        ''                                            m = m + 1
        ''                                            drExpRow("idno") = m
        ''                                            drExpRow("Job") = DSReworkHIsCount.Tables(0).Rows(k).Item("_job")
        ''                                            drExpRow("JobSuffix") = DSReworkHIsCount.Tables(0).Rows(k).Item("_jobsuffix")
        ''                                            drExpRow("JobParent") = DSReworkHIsCount.Tables(0).Rows(k).Item("_jobsuffixParent")
        ''                                            drExpRow("Jobdate") = DSReworkHIsCount.Tables(0).Rows(k).Item("_jobDate")
        ''                                            drExpRow("Operation") = DSReworkHIsCount.Tables(0).Rows(k).Item("_oper_num")
        ''                                            drExpRow("ItemName") = DSReworkHIsCount.Tables(0).Rows(k).Item("_item")
        ''                                            drExpRow("WC") = DSReworkHIsCount.Tables(0).Rows(k).Item("_wc")
        ''                                            drExpRow("WCDes") = DSReworkHIsCount.Tables(0).Rows(k).Item("_wc")
        ''                                            drExpRow("Received") = (IntRejQty) * (-1)
        ''                                            drExpRow("Completed") = (IntRejQty) * (-1)
        ''                                            drExpRow("Rejected") = 0
        ''                                            drExpRow("NextOp") = DSReworkHIsCount.Tables(0).Rows(k).Item("_nextoper_num")
        ''                                            drExpRow("StartTime") = DSReworkHIsCount.Tables(0).Rows(k).Item("_start_Date")
        ''                                            drExpRow("EndTime") = DSReworkHIsCount.Tables(0).Rows(k).Item("_end_Date")
        ''                                            drExpRow("remarks1") = ""
        ''                                            drExpRow("remarks2") = ""
        ''                                            drExpRow("remarks3") = ""
        ''                                        End If

        ''                                        drExpRow = dsExport.Tables("tbExport").NewRow
        ''                                        m = m + 1
        ''                                        drExpRow("idno") = m
        ''                                        drExpRow("Job") = DSReworkHIsCount.Tables(0).Rows(k).Item("_job")
        ''                                        drExpRow("JobSuffix") = DSReworkHIsCount.Tables(0).Rows(k).Item("_jobsuffix")
        ''                                        drExpRow("JobParent") = DSReworkHIsCount.Tables(0).Rows(k).Item("_jobsuffixParent")
        ''                                        drExpRow("Jobdate") = DSReworkHIsCount.Tables(0).Rows(k).Item("_jobDate")
        ''                                        drExpRow("Operation") = DSReworkHIsCount.Tables(0).Rows(k).Item("_oper_num")
        ''                                        drExpRow("ItemName") = DSReworkHIsCount.Tables(0).Rows(k).Item("_item")
        ''                                        drExpRow("WC") = DSReworkHIsCount.Tables(0).Rows(k).Item("_wc")
        ''                                        drExpRow("WCDes") = DSReworkHIsCount.Tables(0).Rows(k).Item("_wc")
        ''                                        drExpRow("Received") = DSReworkHIsCount.Tables(0).Rows(k).Item("_qty_op_qty")
        ''                                        drExpRow("Completed") = DSReworkHIsCount.Tables(0).Rows(k).Item("_qty_complete")
        ''                                        drExpRow("Rejected") = DSReworkHIsCount.Tables(0).Rows(k).Item("_qty_scrapped")
        ''                                        drExpRow("NextOp") = DSReworkHIsCount.Tables(0).Rows(k).Item("_nextoper_num")
        ''                                        drExpRow("StartTime") = DSReworkHIsCount.Tables(0).Rows(k).Item("_start_Date")
        ''                                        drExpRow("EndTime") = DSReworkHIsCount.Tables(0).Rows(k).Item("_end_Date")
        ''                                        drExpRow("remarks1") = ""
        ''                                        drExpRow("remarks2") = ""
        ''                                        drExpRow("remarks3") = ""
        ''                                        Dim dsRej As New DataSet
        ''                                        dsRej = clsM.GetDataset("select * from tbRejectedTrans where _refID=" & .Item("_tansnum"), "tbRejectedTrans")
        ''                                        If dsRej.Tables(0).Rows.Count > 0 Then
        ''                                            drExpRow("remarks1") = dsRej.Tables(0).Rows(0).Item("_RejectedDesc")
        ''                                        End If
        ''                                        If dsRej.Tables(0).Rows.Count > 1 Then
        ''                                            drExpRow("remarks2") = dsRej.Tables(0).Rows(1).Item("_RejectedDesc")
        ''                                        End If
        ''                                        If dsRej.Tables(0).Rows.Count > 2 Then
        ''                                            drExpRow("remarks2") = dsRej.Tables(0).Rows(2).Item("_RejectedDesc")
        ''                                        End If
        ''                                        dsExport.Tables("tbExport").Rows.Add(drExpRow)
        ''                                        HsHis.Add(DSReworkHIsCount.Tables(0).Rows(k).Item("_tansnum"), DSReworkHIsCount.Tables(0).Rows(k).Item("_tansnum"))
        ''                                    End If

        ''                                End If
        ''                            End If
        ''                        Next


        ''                        '  Dim DSReworkori As New DataSet
        ''                        ' Dim DSReworkHIs As New DataSet











        ''                        ''Else
        ''                        ''    drExpRow = dsExport.Tables("tbExport").NewRow
        ''                        ''    m = m + 1
        ''                        ''    drExpRow("idno") = m
        ''                        ''    drExpRow("Job") = .Item("_job")
        ''                        ''    drExpRow("JobSuffix") = .Item("_jobsuffix")
        ''                        ''    drExpRow("JobParent") = .Item("_jobsuffixParent")
        ''                        ''    drExpRow("Jobdate") = .Item("_jobDate")
        ''                        ''    drExpRow("Operation") = .Item("_oper_num")
        ''                        ''    drExpRow("ItemName") = .Item("_item")
        ''                        ''    drExpRow("WC") = .Item("_wc")
        ''                        ''    drExpRow("WCDes") = .Item("_wc")
        ''                        ''    drExpRow("Received") = .Item("_qty_op_qty")
        ''                        ''    drExpRow("Completed") = .Item("_qty_complete")
        ''                        ''    drExpRow("Rejected") = .Item("_qty_scrapped")
        ''                        ''    drExpRow("NextOp") = .Item("_nextoper_num")
        ''                        ''    drExpRow("StartTime") = .Item("_start_Date")
        ''                        ''    drExpRow("EndTime") = .Item("_end_Date")
        ''                        ''    drExpRow("remarks1") = ""
        ''                        ''    drExpRow("remarks2") = ""
        ''                        ''    drExpRow("remarks3") = ""

        ''                        ''    Dim dsRej As New DataSet
        ''                        ''    dsRej = clsM.GetDataset("select * from tbRejectedTrans where _refID=" & .Item("_tansnum"), "tbRejectedTrans")
        ''                        ''    If dsRej.Tables(0).Rows.Count > 0 Then
        ''                        ''        drExpRow("remarks1") = dsRej.Tables(0).Rows(0).Item("_RejectedDesc")
        ''                        ''    End If
        ''                        ''    If dsRej.Tables(0).Rows.Count > 1 Then
        ''                        ''        drExpRow("remarks2") = dsRej.Tables(0).Rows(1).Item("_RejectedDesc")
        ''                        ''    End If
        ''                        ''    If dsRej.Tables(0).Rows.Count > 2 Then
        ''                        ''        drExpRow("remarks2") = dsRej.Tables(0).Rows(2).Item("_RejectedDesc")
        ''                        ''    End If
        ''                        ''    dsExport.Tables("tbExport").Rows.Add(drExpRow)
        ''                        ''    HsCur.Add(.Item("_tansnum"), .Item("_tansnum"))
        ''                    End If
        ''                End If
        ''            End With
        ''            PB1.Value = i + 1
        ''            lblRunning.Text = i + 1
        ''            Application.DoEvents()
        ''        Next
        ''        Dim dsExtable As New DSExportData
        ''        If dsExport.Tables(0).Rows.Count > 0 Then
        ''            i = 0
        ''            Dim HasAc As New Hashtable

        ''            Dim drExpTb As DataRow
        ''            For i = 0 To dsExport.Tables(0).Rows.Count - 1
        ''                With dsExport.Tables(0).Rows(i)
        ''                    If Not HasAc.ContainsKey(.Item("idno")) = True Then
        ''                        Dim drAC As DataRow() = dsExport.Tables(0).Select("Job='" & .Item("Job") & "' and JobSuffix='" & .Item("JobSuffix") & "' and JobParent='" & .Item("JobParent") & "' and Operation=" & .Item("Operation"))
        ''                        If drAC.Length > 0 Then
        ''                            drExpTb = dsExtable.Tables("tbExportOrg").NewRow
        ''                            drExpTb("idno") = i
        ''                            drExpTb("Job") = .Item("Job")
        ''                            drExpTb("JobSuffix") = "-"
        ''                            drExpTb("JobDate") = .Item("JobDate")
        ''                            drExpTb("Operation") = .Item("Operation")
        ''                            drExpTb("WC") = .Item("WC")
        ''                            drExpTb("WCDes") = .Item("WCDes")
        ''                            Dim j As Integer
        ''                            Dim intRecQty As Double = 0
        ''                            Dim intComQty As Double = 0
        ''                            Dim intRejQty As Double = 0
        ''                            Dim strRm1 As String = ""
        ''                            Dim strRm2 As String = ""
        ''                            Dim strRm3 As String = ""
        ''                            Dim dblSt As Double = 0
        ''                            Dim dblEd As Double = 0
        ''                            ' HasAc.Add(.Item("IDno"), .Item("IDno"))
        ''                            For j = 0 To drAC.Length - 1
        ''                                intRecQty = intRecQty + drAC(j).Item("Received")
        ''                                intComQty = intComQty + drAC(j).Item("Completed")
        ''                                intRejQty = intRejQty + drAC(j).Item("Rejected")
        ''                                If Trim(strRm1) = "" Then
        ''                                    strRm1 = drAC(j).Item("remarks1")
        ''                                End If
        ''                                If Trim(strRm2) = "" Then
        ''                                    strRm1 = drAC(j).Item("remarks2")
        ''                                End If
        ''                                If Trim(strRm2) = "" Then
        ''                                    strRm1 = drAC(j).Item("remarks3")
        ''                                End If
        ''                                If (dblSt > CDbl(drAC(j).Item("StartTime"))) Or dblSt = 0 Then
        ''                                    dblSt = CDbl(drAC(j).Item("StartTime"))
        ''                                End If

        ''                                If dblEd < CDbl(drAC(j).Item("EndTime")) Then
        ''                                    dblEd = CDbl(drAC(j).Item("EndTime"))
        ''                                End If

        ''                                HasAc.Add(drAC(j).Item("IDno"), drAC(j).Item("IDno"))

        ''                            Next
        ''                            drExpTb("Received") = intRecQty
        ''                            drExpTb("Completed") = intComQty
        ''                            drExpTb("Rejected") = intRejQty
        ''                            drExpTb("NextOp") = .Item("JobSuffix")
        ''                            drExpTb("StartTime") = dblSt
        ''                            drExpTb("EndTime") = dblEd
        ''                            drExpTb("remarks1") = strRm1
        ''                            drExpTb("remarks2") = strRm2
        ''                            drExpTb("remarks3") = strRm3
        ''                            dsExtable.Tables("tbExportOrg").Rows.Add(drExpTb)
        ''                        End If
        ''                    End If
        ''                End With
        ''            Next
        ''        End If
        ''        ExportDatatoExcel(dsExtable, dsExport, HsCur, HsHis)
        ''        '  DBG1.DataSource = dsExtable.Tables("tbExportOrg")
        ''        DBG1.DataSource = dsExport.Tables("tbExport")
        ''    Else
        ''        lblDataV.Text = ""
        ''        MsgBox("Sorry there is no data to Export!", MsgBoxStyle.Information, "eWIP")
        ''    End If
        ''Catch ex As Exception
        ''    MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        ''Finally
        ''    cn.Close()
        ''End Try
        UpdateStatus()
    End Sub
    Function ExportDatatoExcel(ByVal DSEx As DataSet, ByVal dsExport As DataSet, ByVal HsCur As Hashtable, ByVal HsHis As Hashtable) As Boolean
        Dim myTrans As System.Data.SqlClient.SqlTransaction
        lblDataV.Text = "Data Export in Processing.."
        Dim delFile As Boolean = False
        lblRunning.Text = 0
        ' Dim at As Integer
        If txtFile.Text = "" Then
            MsgBox("Please enter the file name!", MsgBoxStyle.Information, "eWIP")
            Exit Function
        End If

        Dim str, filename As String
        str = ""
        filename = ""
        Dim col, row As Integer
        col = 0
        row = 0
        Dim strHisNo As String = ""
        Dim strAcrNo As String = ""
        Dim valHis As String
        Dim strHisVal As String = ""
        Dim strCurVal As String = ""
        If HsHis.Count > 0 Then
            For Each valHis In HsHis.Keys
                If strHisVal = "" Then
                    strHisVal = "" & valHis & ""
                Else
                    strHisVal = strHisVal & "," & valHis & ""
                End If
            Next
        End If

        If HsCur.Count > 0 Then
            For Each valHis In HsCur.Keys
                If strCurVal = "" Then
                    strCurVal = "" & valHis & ""
                Else
                    strCurVal = strCurVal & "," & valHis & ""
                End If
            Next
        End If
        Dim strJob As String = ""
        Dim HasNewJo As New Hashtable
        If dsExport.Tables("tbExport").Rows.Count > 0 Then
            Dim k As Integer
            For k = 0 To dsExport.Tables("tbExport").Rows.Count - 1
                With dsExport.Tables("tbExport").Rows(k)
                    If Not HasNewJo.ContainsKey(.Item("job")) = True Then
                        If Trim(strJob) = "" Then
                            strJob = "'" & .Item("JoB") & "'"
                        Else
                            strJob = strJob & ",'" & .Item("JoB") & "'"
                        End If
                        HasNewJo.Add(.Item("job"), .Item("job"))
                    End If
                End With
            Next

        End If
        Dim clsM As New clsMain
        Dim DSJob As New DataSet
        Dim DSJobRoute As New DataSet
        DSJob = clsM.GetDataset("select * from tbJob where _job in(" & strJob & ")", "tbJOB")
        DSJobRoute = clsM.GetDataset("select * from tbJobroute where _job in(" & strJob & ")", "tbJOBRoute")
        Dim Excel As Object = CreateObject("Excel.Application")
        If Excel Is Nothing Then
            MsgBox("It appears that Excel is not installed on this machine. This operation requires MS Excel to be installed on this machine.", MsgBoxStyle.Critical, "eWIP")
            Exit Function
        End If
        Dim DSWC As New DataSet

        DSWC = clsM.GetDataset("select * from tbWC", "tbWC")
        P1.Visible = True
        Try
            With Excel
                .SheetsInNewWorkbook = 1
                .Workbooks.Add()
                .Worksheets(1).Select()
                Dim i As Integer = 1

                .cells(1, 1).value = "Transaction"
                .cells(1, 1).EntireRow.Font.Bold = True
                .cells(1, 2).value = "Transaction Type"
                .cells(1, 2).EntireRow.Font.Bold = True
                .cells(1, 3).value = "Transaction Date"
                .cells(1, 3).EntireRow.Font.Bold = True

                .cells(1, 4).value = "Job"
                .cells(1, 4).EntireRow.Font.Bold = True
                .cells(1, 5).value = "Job Suffix"
                .cells(1, 5).EntireRow.Font.Bold = True
                .cells(1, 6).value = "Operation"
                .cells(1, 6).EntireRow.Font.Bold = True
                .cells(1, 7).value = "Item"
                .cells(1, 7).EntireRow.Font.Bold = True
                .cells(1, 8).value = "Item Description"
                .cells(1, 8).EntireRow.Font.Bold = True
                .cells(1, 9).value = "WC"
                .cells(1, 9).EntireRow.Font.Bold = True
                .cells(1, 10).value = "WC Description"
                .cells(1, 10).EntireRow.Font.Bold = True


                .cells(1, 11).value = "User Initials"
                .cells(1, 11).EntireRow.Font.Bold = True
                .cells(1, 12).value = "Employee"
                .cells(1, 12).EntireRow.Font.Bold = True
                .cells(1, 13).value = "Employee Name"
                .cells(1, 13).EntireRow.Font.Bold = True
                .cells(1, 14).value = "Shift"
                .cells(1, 14).EntireRow.Font.Bold = True
                .cells(1, 15).value = "Pay Type"
                .cells(1, 15).EntireRow.Font.Bold = True
                .cells(1, 16).value = "Pay Rate"
                .cells(1, 16).EntireRow.Font.Bold = True
                .cells(1, 17).value = "Job Cost Rate"
                .cells(1, 17).EntireRow.Font.Bold = True
                .cells(1, 18).value = "Indirect Code"
                .cells(1, 18).EntireRow.Font.Bold = True
                .cells(1, 19).value = "Description"
                .cells(1, 19).EntireRow.Font.Bold = True
                .cells(1, 20).value = "Completed"
                .cells(1, 20).EntireRow.Font.Bold = True

                .cells(1, 21).value = "Scrapped"
                .cells(1, 21).EntireRow.Font.Bold = True
                .cells(1, 22).value = "Moved"
                .cells(1, 22).EntireRow.Font.Bold = True
                .cells(1, 23).value = "U/M"
                .cells(1, 23).EntireRow.Font.Bold = True
                .cells(1, 24).value = "Warehouse"
                .cells(1, 24).EntireRow.Font.Bold = True
                .cells(1, 25).value = "Reason"
                .cells(1, 25).EntireRow.Font.Bold = True
                .cells(1, 26).value = "Reason Description"
                .cells(1, 26).EntireRow.Font.Bold = True
                .cells(1, 27).value = "Next Operation"
                .cells(1, 27).EntireRow.Font.Bold = True
                .cells(1, 28).value = "Work Center"
                .cells(1, 28).EntireRow.Font.Bold = True
                .cells(1, 29).value = "Work Center Description"
                .cells(1, 29).EntireRow.Font.Bold = True
                .cells(1, 30).value = "Move To Location"
                .cells(1, 30).EntireRow.Font.Bold = True
                .cells(1, 31).value = "Location Description"
                .cells(1, 31).EntireRow.Font.Bold = True
                .cells(1, 32).value = "Lot"
                .cells(1, 32).EntireRow.Font.Bold = True
                .cells(1, 33).value = "Start Time"
                .cells(1, 33).EntireRow.Font.Bold = True
                .cells(1, 34).value = "End Time"
                .cells(1, 34).EntireRow.Font.Bold = True
                .cells(1, 35).value = "Total Hours"
                .cells(1, 35).EntireRow.Font.Bold = True

                .cells(1, 36).value = "Cost Code"
                .cells(1, 36).EntireRow.Font.Bold = True
                .cells(1, 37).value = "Control Point"
                .cells(1, 37).EntireRow.Font.Bold = True
                .cells(1, 38).value = "Oper Complete"
                .cells(1, 38).EntireRow.Font.Bold = True
                .cells(1, 39).value = "Close Job"
                .cells(1, 39).EntireRow.Font.Bold = True
                .cells(1, 40).value = "Issue to Parent"
                .cells(1, 40).EntireRow.Font.Bold = True
                ' .cells(1, 41).value = "Remark 1"
                ' .cells(1, 41).EntireRow.Font.Bold = True
                ' .cells(1, 42).value = "Remark 2"
                ' .cells(1, 42).EntireRow.Font.Bold = True
                ' .cells(1, 43).value = "Remark 3"
                ' .cells(1, 43).EntireRow.Font.Bold = True
                ' .Cells(2, 1).Value = "Test"
                '.Cells(2, 2).Value = "Data"
                i += 1

            End With
            Application.DoEvents()

            Dim xl As Integer = 1
            lblTotal.Text = 0
            lblRunning.Text = 0
            PB1.Value = 0
            PB1.Minimum = 0

            If DSEx.Tables("tbExportOrg").Rows.Count > 0 Then
                Dim j As Integer
                PB1.Maximum = DSEx.Tables("tbExportOrg").Rows.Count
                lblTotal.Text = DSEx.Tables("tbExportOrg").Rows.Count
                For j = 0 To DSEx.Tables("tbExportOrg").Rows.Count - 1

                    With DSEx.Tables("tbExportOrg").Rows(j)
                        xl = xl + 1
                        Dim drJob As DataRow() = DSJob.Tables(0).Select("_job='" & .Item("Job") & "'")
                        Dim dblJob As Double = 0

                        If drJob.Length > 0 Then
                            dblJob = drJob(0).Item("_jobDate")
                        End If


                        Dim DROP() As DataRow = DSJobRoute.Tables(0).Select("_Job='" & .Item("Job") & "' and _operationNo=" & .Item("Operation"))

                        Dim strOpnostatus As String
                        Dim dblLbrhr As Double = 0
                        Dim dbMChHr As Double = 0
                        If DROP.Length > 0 Then
                            dblLbrhr = DROP(0).Item("_runlbrhrs")
                            dbMChHr = DROP(0).Item("_runmchhrs")
                        End If
                        Dim RnRate As Double = 0

                        Dim DRWC As DataRow() = DSWC.Tables(0).Select("_wc='" & .Item("WC") & "'")
                        Dim strRun As String = "Run"
                        Dim strEMP As String = ""
                        If DRWC.Length > 0 Then
                            RnRate = DRWC(0).Item("_Run_Rate")
                            If DRWC(0).Item("_labour") = "L" Then
                                strRun = "Run"

                                strEMP = "PROD"
                            Else
                                strRun = "Machine"
                                strEMP = ""
                            End If
                        End If

                        Dim StrNext As String = ""
                        Dim DROPst() As DataRow = DSJobRoute.Tables(0).Select("_Job='" & .Item("Job") & "' and (_operationNo)>" & .Item("Operation"))
                        Dim l1 As Integer

                        If DROPst.Length > 0 Then
                            Dim dimcntOpno As Integer = 0
                            For l1 = 0 To DROPst.Length - 1
                                If dimcntOpno = 0 Then
                                    dimcntOpno = DROPst(l1).Item("_operationNo")
                                Else
                                    If dimcntOpno > DROPst(l1).Item("_operationNo") Then
                                        dimcntOpno = DROPst(l1).Item("_operationNo")
                                    End If
                                End If
                            Next


                            strOpnostatus = ""
                            StrNext = dimcntOpno
                        Else
                            StrNext = ""
                            'strOpnostatus = "LT"
                            strOpnostatus = "LOT"
                        End If
                        Dim strMoveLot As String = ""
                        If strOpnostatus = "LOT" Then
                            If .Item("ItemName") <> "" Then
                                If CStr(.Item("ItemName")).Length >= 2 Then
                                    Dim strss As String = UCase(Mid(.Item("ItemName"), 1, 2))
                                    If strss = "FG" Then
                                        strMoveLot = "TL"
                                    Else
                                        strMoveLot = "FS"
                                    End If
                                Else
                                End If
                            End If
                        End If

                        'calculate Total Hour
                        Dim dblTotHr As Double
                        If strRun = "Run" Then
                            dblTotHr = .Item("Completed") * dblLbrhr
                        Else
                            dblTotHr = .Item("Completed") * dbMChHr
                        End If


                        Excel.cells(xl, 1).value = ""
                        Excel.cells(xl, 2).value = strRun
                        Excel.cells(xl, 3).value = Format(DateTime.FromOADate(.Item("StartTime")), "yyyy-MM-dd HH:mm") 'Format(DateTime.FromOADate(dblJob), "yyyy-MM-dd")
                        Excel.cells(xl, 4).value = .Item("Job")

                        Excel.cells(xl, 5).value = .Item("JobSuffix")
                        Excel.cells(xl, 6).value = .Item("Operation")
                        Excel.cells(xl, 7).value = .Item("ItemName")
                        Excel.cells(xl, 8).value = ""
                        Excel.cells(xl, 9).value = .Item("WC")
                        Excel.cells(xl, 10).value = .Item("WCDes")
                        Excel.cells(xl, 11).value = ""
                        Excel.cells(xl, 12).value = strEMP
                        Excel.cells(xl, 13).value = ""
                        Excel.cells(xl, 14).value = "1"
                        Excel.cells(xl, 15).value = "Regular"

                        Excel.cells(xl, 16) = CStr(Format(RnRate, "0.00"))
                     
                        'Dim strRnt12 As String = CStr(Format(RnRate, "0.00"))
                        ' Excel.cells(xl, 16) = vbString & strRnt12
                        ' Excel.Colum(xl, 16).Select()
                        ' Excel.Selection.NumberFormat = "0.00"

                        ' Excel.cells(xl, 16).value = CStr(Format(RnRate, "0.00"))

                        Excel.cells(xl, 17).value = CStr(Format(RnRate, "0.00"))
                        Excel.cells(xl, 18).value = 0
                        Excel.cells(xl, 19).value = ""
                        Excel.cells(xl, 20).value = .Item("Completed")
                        Excel.cells(xl, 21).value = .Item("Rejected")
                        Excel.cells(xl, 22).value = .Item("Completed")
                        Excel.cells(xl, 23).value = ""
                        Excel.cells(xl, 24).value = "MAIN"
                        Excel.cells(xl, 25).value = "PF"
                        Excel.cells(xl, 26).value = ""
                        Excel.cells(xl, 27).value = StrNext
                        Excel.cells(xl, 28).value = ""
                        Excel.cells(xl, 29).value = ""
                        Excel.cells(xl, 30).value = strMoveLot
                        Excel.cells(xl, 31).value = ""
                        Excel.cells(xl, 32).value = strOpnostatus
                        Excel.cells(xl, 33).value = CStr(Format(DateTime.FromOADate(.Item("StartTime")), "HH:mm"))
                        Excel.cells(xl, 34).value = CStr(Format(DateTime.FromOADate(.Item("EndTime")), "HH:mm"))
                        Excel.cells(xl, 35).value = dblTotHr
                        Excel.cells(xl, 36).value = ""
                        Excel.cells(xl, 37).value = ""
                        Excel.cells(xl, 38).value = "1"
                        Excel.cells(xl, 39).value = ""
                        Excel.cells(xl, 40).value = ""
                        '   Excel.cells(xl, 41).value = .Item("remarks1")
                        ' Excel.cells(xl, 42).value = .Item("remarks2")
                        ' Excel.cells(xl, 43).value = .Item("remarks3")
                    End With
                    Application.DoEvents()
                    PB1.Value = j + 1
                    lblRunning.Text = j + 1
                    Application.DoEvents()
                Next

            End If
            filename = txtFile.Text
            Excel.ActiveCell.Worksheet.SaveAs(filename)
            System.Runtime.InteropServices.Marshal.ReleaseComObject(Excel)
            Excel = Nothing

            cn.Open()
            myTrans = cn.BeginTransaction
            Dim strDate As String = (Format(Now.ToOADate, "#.00000"))
            Dim dblExDate As Double = strDate
            With com
                If Trim(strHisVal) <> "" Then
                    .Connection = cn
                    .Transaction = myTrans
                    .CommandType = CommandType.Text
                    .CommandText = " Update tbJobTransHis set _export='Y',_exportHis='Y',_ExportDate=" & dblExDate & "  where _tansnum in(" & strHisVal & ")"

                    .ExecuteNonQuery()
                    .Parameters.Clear()
                End If
                If Trim(strCurVal) <> "" Then
                    .Connection = cn
                    .Transaction = myTrans
                    .CommandType = CommandType.Text
                    .CommandText = " Update tbJobTrans set _export='Y',_ExportDate=" & dblExDate & " where _tansnum in(" & strCurVal & ")"
                    .ExecuteNonQuery()
                    .Parameters.Clear()
                End If

                If Trim(strCurVal) <> "" Or Trim(strHisVal) <> "" Then
                    .Connection = cn
                    .Transaction = myTrans
                    .CommandType = CommandType.Text
                    .CommandText = " Update tbExportDate set _exportDate=" & dblExDate
                    .ExecuteNonQuery()
                    .Parameters.Clear()
                    txtRedoDate.Text = (Format(dblExDate, "#.00000"))
                    butRedo.Enabled = True
                End If
                myTrans.Commit()
            End With

            MsgBox("Data's are exported to Excel Succesfully in '" & filename & "'", MsgBoxStyle.Information, "eWIP")

            lblDataV.Text = "Data has been Successfully Exported"




        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "eWIP")
            delFile = True
            myTrans.Rollback()

        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try

        ' The excel is created and opened for insert value. We most close this excel using this system
        Dim pro() As Process = System.Diagnostics.Process.GetProcessesByName("EXCEL")

        For Each i As Process In pro
            i.Kill()
        Next
        If delFile = True Then
            Try
                If IO.File.Exists(filename) Then
                    IO.File.Delete(filename)
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End Try
        End If
        txtFile.Text = ""


    End Function
    Private Sub frmExportData_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblDataV.Text = ""
        CheckRedo()
    End Sub
    Function CheckRedo() As Boolean
        CheckRedo = False
        txtRedoDate.Text = ""
        Try
            Dim DSRedo As New DataSet
            Dim clsM As New clsMain
            DSRedo = clsM.GetDataset("select * from tbExportDate", "tbExportDate")
            If DSRedo.Tables(0).Rows.Count > 0 Then
                If DSRedo.Tables(0).Rows(0).Item("_exportDate") <> 0 Then
                    txtRedoDate.Text = DSRedo.Tables(0).Rows(0).Item("_exportDate")
                    butRedo.Enabled = True
                Else
                    butRedo.Enabled = False
                End If
            Else
                butRedo.Enabled = False
            End If
        Catch ex As Exception
        End Try
    End Function


    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click

        'MsgBox(Format(8989898.9909, "#.00"))
        txtFile.Text = ""
        lblDataV.Text = ""
        PB1.Value = 0
        PB1.Value = 0
        PB1.Minimum = 0
        lblTotal.Text = 0
        lblRunning.Text = 0
    End Sub


    Sub UpdateStatus()
        Dim clsM As New clsMain
        lblDataV.Text = "Data Verify to Export.."
        lblRunning.Text = 0
        ' Dim at As Integer
        If txtFile.Text = "" Then
            MsgBox("Please enter the file name!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        lblTotal.Text = 0
        lblRunning.Text = 0
        P1.Visible = True

        Try
            Dim dsTrans As New DataSet
            Dim dsTransHis As New DataSet
            Dim strSqlTrans As String = "select * from tbJobTrans where _export='N' and _status='COMPLETED' order by _oper_num"
            dsTrans = clsM.GetDataset(strSqlTrans, "tbJobTrans")
            Dim strSqlHis As String = "select * from tbJobTransHis where _exportHis='N' and _status='COMPLETED'"
            dsTransHis = clsM.GetDataset(strSqlHis, "tbJobTrans")
            Dim HsHis As New Hashtable
            Dim i As Integer
            Dim dsExport As New DSExportData
            Dim drExpRow As DataRow
            Dim m As Integer = 0
            Dim HsCur As New Hashtable
            lblTotal.Text = dsTrans.Tables(0).Rows.Count
            PB1.Value = 0
            PB1.Minimum = 0
            If dsTrans.Tables(0).Rows.Count > 0 Then
                PB1.Maximum = dsTrans.Tables(0).Rows.Count
                For i = 0 To dsTrans.Tables(0).Rows.Count - 1
                    Dim stUs As Boolean = False
                    With dsTrans.Tables(0).Rows(i)
                        If Not HsCur.ContainsKey(.Item("_tansnum")) = True Then
                            If .Item("_reworkst") <> "R" Then
                                'Not Reworks
                                drExpRow = dsExport.Tables("tbExport").NewRow
                                m = m + 1
                                drExpRow("idno") = m
                                drExpRow("Job") = .Item("_job")
                                drExpRow("JobSuffix") = .Item("_jobsuffix")
                                drExpRow("JobParent") = .Item("_jobsuffixParent")
                                drExpRow("Jobdate") = .Item("_jobDate")
                                drExpRow("Operation") = .Item("_oper_num")
                                drExpRow("ItemName") = .Item("_item")
                                drExpRow("WC") = .Item("_wc")
                                drExpRow("WCDes") = .Item("_wc")
                                drExpRow("Received") = .Item("_qty_op_qty")
                                drExpRow("Completed") = .Item("_qty_complete")
                                drExpRow("Rejected") = .Item("_qty_scrapped")
                                drExpRow("NextOp") = .Item("_nextoper_num")
                                drExpRow("StartTime") = .Item("_start_Date")
                                drExpRow("EndTime") = .Item("_end_Date")
                                drExpRow("remarks1") = ""
                                drExpRow("remarks2") = ""
                                drExpRow("remarks3") = ""

                                Dim dsRejNotRew As New DataSet
                                dsRejNotRew = clsM.GetDataset("select * from tbRejectedTrans where _refID=" & .Item("_tansnum"), "tbRejectedTrans")
                                If dsRejNotRew.Tables(0).Rows.Count > 0 Then
                                    drExpRow("remarks1") = dsRejNotRew.Tables(0).Rows(0).Item("_RejectedDesc")
                                End If
                                If dsRejNotRew.Tables(0).Rows.Count > 1 Then
                                    drExpRow("remarks2") = dsRejNotRew.Tables(0).Rows(1).Item("_RejectedDesc")
                                End If
                                If dsRejNotRew.Tables(0).Rows.Count > 2 Then
                                    drExpRow("remarks2") = dsRejNotRew.Tables(0).Rows(2).Item("_RejectedDesc")
                                End If
                                dsExport.Tables("tbExport").Rows.Add(drExpRow)
                                HsCur.Add(.Item("_tansnum"), .Item("_tansnum"))
                            Else
                                Dim DSNewHisWrk As New DataSet
                                DSNewHisWrk = clsM.GetDataset("select * from tbJobTransHis where _job='" & .Item("_job") & "' and  _jobsuffix='" & .Item("_jobsuffix") & "' and _jobsuffixParent='" & .Item("_jobsuffixParent") & "' and _oper_num=" & .Item("_oper_num") & " and  _status='COMPLETED' ORDER BY _reworkCount", "TBCheckData")
                                If DSNewHisWrk.Tables(0).Rows.Count > 0 Then
                                    Dim j As Integer
                                    stUs = True
                                    For j = 0 To DSNewHisWrk.Tables(0).Rows.Count - 1
                                        If DSNewHisWrk.Tables(0).Rows(j).Item("_reworkCount") = 0 Then
                                            drExpRow = dsExport.Tables("tbExport").NewRow
                                            m = m + 1
                                            drExpRow("idno") = m
                                            drExpRow("Job") = DSNewHisWrk.Tables(0).Rows(j).Item("_job")
                                            drExpRow("JobSuffix") = DSNewHisWrk.Tables(0).Rows(j).Item("_jobsuffix")
                                            drExpRow("JobParent") = DSNewHisWrk.Tables(0).Rows(j).Item("_jobsuffixParent")
                                            drExpRow("Jobdate") = DSNewHisWrk.Tables(0).Rows(j).Item("_jobDate")
                                            drExpRow("Operation") = DSNewHisWrk.Tables(0).Rows(j).Item("_oper_num")
                                            drExpRow("ItemName") = DSNewHisWrk.Tables(0).Rows(j).Item("_item")
                                            drExpRow("WC") = DSNewHisWrk.Tables(0).Rows(j).Item("_wc")
                                            drExpRow("WCDes") = DSNewHisWrk.Tables(0).Rows(j).Item("_wc")
                                            drExpRow("Received") = DSNewHisWrk.Tables(0).Rows(j).Item("_qty_op_qty")
                                            drExpRow("Completed") = DSNewHisWrk.Tables(0).Rows(j).Item("_qty_complete")
                                            drExpRow("Rejected") = DSNewHisWrk.Tables(0).Rows(j).Item("_qty_scrapped")
                                            drExpRow("NextOp") = DSNewHisWrk.Tables(0).Rows(j).Item("_nextoper_num")
                                            drExpRow("StartTime") = DSNewHisWrk.Tables(0).Rows(j).Item("_start_Date")
                                            drExpRow("EndTime") = DSNewHisWrk.Tables(0).Rows(j).Item("_end_Date")
                                            drExpRow("remarks1") = ""
                                            drExpRow("remarks2") = ""
                                            drExpRow("remarks3") = ""

                                            Dim dsRejNotRew1 As New DataSet
                                            dsRejNotRew1 = clsM.GetDataset("select * from tbRejectedTrans where _refID=" & DSNewHisWrk.Tables(0).Rows(j).Item("_oldnum"), "tbRejectedTrans")
                                            If dsRejNotRew1.Tables(0).Rows.Count > 0 Then
                                                drExpRow("remarks1") = dsRejNotRew1.Tables(0).Rows(0).Item("_RejectedDesc")
                                            End If
                                            If dsRejNotRew1.Tables(0).Rows.Count > 1 Then
                                                drExpRow("remarks2") = dsRejNotRew1.Tables(0).Rows(1).Item("_RejectedDesc")
                                            End If
                                            If dsRejNotRew1.Tables(0).Rows.Count > 2 Then
                                                drExpRow("remarks2") = dsRejNotRew1.Tables(0).Rows(2).Item("_RejectedDesc")
                                            End If
                                            dsExport.Tables("tbExport").Rows.Add(drExpRow)
                                            HsHis.Add(DSNewHisWrk.Tables(0).Rows(j).Item("_tansnum"), DSNewHisWrk.Tables(0).Rows(j).Item("_tansnum"))
                                        Else
                                            Dim DSHisCal As New DataSet
                                            DSHisCal = clsM.GetDataset("select * from tbJobTransHis where _job='" & DSNewHisWrk.Tables(0).Rows(j).Item("_job") & "' and  _jobsuffix='" & DSNewHisWrk.Tables(0).Rows(j).Item("_jobsuffix") & "' and _jobsuffixParent='" & DSNewHisWrk.Tables(0).Rows(j).Item("_jobsuffixParent") & "' and _oper_num<" & DSNewHisWrk.Tables(0).Rows(j).Item("_oper_num") & " and _reworkCount=" & DSNewHisWrk.Tables(0).Rows(j).Item("_reworkCount") & " ORDER BY _reworkCount", "TBCheckData")
                                            Dim HisRejQty1 As Double
                                            Dim k1 As Integer
                                            If DSHisCal.Tables(0).Rows.Count > 0 Then
                                                For k1 = 0 To DSHisCal.Tables(0).Rows.Count - 1
                                                    HisRejQty1 = HisRejQty1 + DSHisCal.Tables(0).Rows(k1).Item("_qty_scrapped")
                                                Next
                                            End If
                                            Dim DSCurCal As New DataSet
                                            DSCurCal = clsM.GetDataset("select * from tbJobTrans where _job='" & DSNewHisWrk.Tables(0).Rows(j).Item("_job") & "' and  _jobsuffix='" & DSNewHisWrk.Tables(0).Rows(j).Item("_jobsuffix") & "' and _jobsuffixParent='" & DSNewHisWrk.Tables(0).Rows(j).Item("_jobsuffixParent") & "' and _oper_num<" & DSNewHisWrk.Tables(0).Rows(j).Item("_oper_num") & " and _reworkCount=" & DSNewHisWrk.Tables(0).Rows(j).Item("_reworkCount") & "  ORDER BY _reworkCount", "TBCheckData")
                                            If DSCurCal.Tables(0).Rows.Count > 0 Then
                                                For k1 = 0 To DSCurCal.Tables(0).Rows.Count - 1
                                                    HisRejQty1 = HisRejQty1 + DSCurCal.Tables(0).Rows(k1).Item("_qty_scrapped")
                                                Next
                                            End If
                                            drExpRow = dsExport.Tables("tbExport").NewRow
                                            m = m + 1
                                            drExpRow("idno") = m
                                            drExpRow("Job") = DSNewHisWrk.Tables(0).Rows(j).Item("_job")
                                            drExpRow("JobSuffix") = DSNewHisWrk.Tables(0).Rows(j).Item("_jobsuffix")
                                            drExpRow("JobParent") = DSNewHisWrk.Tables(0).Rows(j).Item("_jobsuffixParent")
                                            drExpRow("Jobdate") = DSNewHisWrk.Tables(0).Rows(j).Item("_jobDate")
                                            drExpRow("Operation") = DSNewHisWrk.Tables(0).Rows(j).Item("_oper_num")
                                            drExpRow("ItemName") = DSNewHisWrk.Tables(0).Rows(j).Item("_item")
                                            drExpRow("WC") = DSNewHisWrk.Tables(0).Rows(j).Item("_wc")
                                            drExpRow("WCDes") = DSNewHisWrk.Tables(0).Rows(j).Item("_wc")
                                            drExpRow("Received") = (HisRejQty1) * (-1)
                                            drExpRow("Completed") = (HisRejQty1 + DSNewHisWrk.Tables(0).Rows(j).Item("_qty_scrapped")) * (-1)
                                            drExpRow("Rejected") = DSNewHisWrk.Tables(0).Rows(j).Item("_qty_scrapped")
                                            drExpRow("NextOp") = DSNewHisWrk.Tables(0).Rows(j).Item("_nextoper_num")
                                            drExpRow("StartTime") = DSNewHisWrk.Tables(0).Rows(j).Item("_start_Date")
                                            drExpRow("EndTime") = DSNewHisWrk.Tables(0).Rows(j).Item("_end_Date")
                                            drExpRow("remarks1") = ""
                                            drExpRow("remarks2") = ""
                                            drExpRow("remarks3") = ""

                                            Dim dsRejNotRewHis As New DataSet
                                            dsRejNotRewHis = clsM.GetDataset("select * from tbRejectedTrans where _refID=" & DSNewHisWrk.Tables(0).Rows(j).Item("_oldnum"), "tbRejectedTrans")
                                            If dsRejNotRewHis.Tables(0).Rows.Count > 0 Then
                                                drExpRow("remarks1") = dsRejNotRewHis.Tables(0).Rows(0).Item("_RejectedDesc")
                                            End If
                                            If dsRejNotRewHis.Tables(0).Rows.Count > 1 Then
                                                drExpRow("remarks2") = dsRejNotRewHis.Tables(0).Rows(1).Item("_RejectedDesc")
                                            End If
                                            If dsRejNotRewHis.Tables(0).Rows.Count > 2 Then
                                                drExpRow("remarks2") = dsRejNotRewHis.Tables(0).Rows(2).Item("_RejectedDesc")
                                            End If
                                            dsExport.Tables("tbExport").Rows.Add(drExpRow)
                                            HsHis.Add(DSNewHisWrk.Tables(0).Rows(j).Item("_tansnum"), DSNewHisWrk.Tables(0).Rows(j).Item("_tansnum"))
                                        End If
                                    Next
                                End If


                                Dim DSHisCalRel As New DataSet
                                DSHisCalRel = clsM.GetDataset("select * from tbJobTransHis where _job='" & .Item("_job") & "' and  _jobsuffix='" & .Item("_jobsuffix") & "' and _jobsuffixParent='" & .Item("_jobsuffixParent") & "' and _oper_num<" & .Item("_oper_num") & " and _reworkCount=" & .Item("_reworkCount") & " ORDER BY _reworkCount", "TBCheckData")
                                Dim HisRejQty As Double
                                Dim k As Integer
                                If DSHisCalRel.Tables(0).Rows.Count > 0 Then
                                    For k = 0 To DSHisCalRel.Tables(0).Rows.Count - 1
                                        HisRejQty = HisRejQty + DSHisCalRel.Tables(0).Rows(k).Item("_qty_scrapped")
                                    Next
                                End If
                                Dim DSCurCalRel As New DataSet
                                DSCurCalRel = clsM.GetDataset("select * from tbJobTrans where _job='" & .Item("_job") & "' and  _jobsuffix='" & .Item("_jobsuffix") & "' and _jobsuffixParent='" & .Item("_jobsuffixParent") & "' and _oper_num<" & .Item("_oper_num") & " and _reworkCount= " & .Item("_reworkCount") & " ORDER BY _reworkCount", "TBCheckData")
                                If DSCurCalRel.Tables(0).Rows.Count > 0 Then
                                    For k = 0 To DSCurCalRel.Tables(0).Rows.Count - 1
                                        HisRejQty = HisRejQty + DSCurCalRel.Tables(0).Rows(k).Item("_qty_scrapped")
                                    Next
                                End If


                                drExpRow = dsExport.Tables("tbExport").NewRow
                                m = m + 1
                                drExpRow("idno") = m
                                drExpRow("Job") = .Item("_job")
                                drExpRow("JobSuffix") = .Item("_jobsuffix")
                                drExpRow("JobParent") = .Item("_jobsuffixParent")
                                drExpRow("Jobdate") = .Item("_jobDate")
                                drExpRow("Operation") = .Item("_oper_num")
                                drExpRow("ItemName") = .Item("_item")
                                drExpRow("WC") = .Item("_wc")
                                drExpRow("WCDes") = .Item("_wc")
                                If stUs = True Then
                                    drExpRow("Received") = (HisRejQty) * (-1)
                                    drExpRow("Completed") = (HisRejQty + .Item("_qty_scrapped")) * (-1)
                                    drExpRow("Rejected") = .Item("_qty_scrapped")
                                Else
                                    drExpRow("Received") = .Item("_qty_op_qty")
                                    drExpRow("Completed") = .Item("_qty_complete")
                                    drExpRow("Rejected") = .Item("_qty_scrapped")
                                End If
                                drExpRow("NextOp") = .Item("_nextoper_num")
                                drExpRow("StartTime") = .Item("_start_Date")
                                drExpRow("EndTime") = .Item("_end_Date")
                                drExpRow("remarks1") = ""
                                drExpRow("remarks2") = ""
                                drExpRow("remarks3") = ""

                                Dim dsRejNotRew As New DataSet
                                dsRejNotRew = clsM.GetDataset("select * from tbRejectedTrans where _refID=" & .Item("_tansnum"), "tbRejectedTrans")
                                If dsRejNotRew.Tables(0).Rows.Count > 0 Then
                                    drExpRow("remarks1") = dsRejNotRew.Tables(0).Rows(0).Item("_RejectedDesc")
                                End If
                                If dsRejNotRew.Tables(0).Rows.Count > 1 Then
                                    drExpRow("remarks2") = dsRejNotRew.Tables(0).Rows(1).Item("_RejectedDesc")
                                End If
                                If dsRejNotRew.Tables(0).Rows.Count > 2 Then
                                    drExpRow("remarks2") = dsRejNotRew.Tables(0).Rows(2).Item("_RejectedDesc")
                                End If
                                dsExport.Tables("tbExport").Rows.Add(drExpRow)
                                HsCur.Add(.Item("_tansnum"), .Item("_tansnum"))



                            End If

                        End If












                    End With
                    PB1.Value = i + 1
                    lblRunning.Text = i + 1
                    Application.DoEvents()
                Next
                Dim dsExtable As New DSExportData
                If dsExport.Tables("tbExport").Rows.Count > 0 Then
                    i = 0
                    Dim HasAc As New Hashtable

                    Dim drExpTb As DataRow
                    For i = 0 To dsExport.Tables("tbExport").Rows.Count - 1
                        Dim ssss As String
                        If i = 100 Then
                            ssss = "OK"
                        End If
                        If i = 200 Then
                            ssss = "OK"
                        End If
                        If i = 300 Then
                            ssss = "OK"
                        End If
                        If i = 400 Then
                            ssss = "OK"
                        End If
                        If i = 500 Then
                            ssss = "OK"
                        End If
                        If i = 600 Then
                            ssss = "OK"
                        End If
                        If i = 919 Then
                            ssss = "OK"
                        End If
                        If i = 916 Then
                            ssss = "OK"
                        End If
                        If i = 917 Then
                            ssss = "OK"
                        End If
                        If i = 918 Then
                            ssss = "OK"
                        End If
                        With dsExport.Tables("tbExport").Rows(i)
                            If Not HasAc.ContainsKey(.Item("idno")) = True Then
                                Dim drAC As DataRow() = dsExport.Tables("tbExport").Select("Job='" & .Item("Job") & "' and JobSuffix='" & .Item("JobSuffix") & "' and JobParent='" & .Item("JobParent") & "' and Operation='" & .Item("Operation") & "'")
                                If drAC.Length > 0 Then
                                    drExpTb = dsExtable.Tables("tbExportOrg").NewRow
                                    drExpTb("idno") = i
                                    drExpTb("Job") = .Item("Job")
                                    drExpTb("JobSuffix") = "-"
                                    drExpTb("JobDate") = .Item("JobDate")
                                    drExpTb("Operation") = .Item("Operation")
                                    drExpTb("WC") = .Item("WC")
                                    drExpTb("ItemName") = .Item("ItemName")
                                    drExpTb("WCDes") = .Item("WCDes")
                                    Dim j As Integer
                                    Dim intRecQty As Double = 0
                                    Dim intComQty As Double = 0
                                    Dim intRejQty As Double = 0
                                    Dim strRm1 As String = ""
                                    Dim strRm2 As String = ""
                                    Dim strRm3 As String = ""
                                    Dim dblSt As Double = 0
                                    Dim dblEd As Double = 0
                                    ' HasAc.Add(.Item("IDno"), .Item("IDno"))
                                    For j = 0 To drAC.Length - 1
                                        intRecQty = intRecQty + drAC(j).Item("Received")
                                        intComQty = intComQty + drAC(j).Item("Completed")
                                        intRejQty = intRejQty + drAC(j).Item("Rejected")
                                        If Trim(strRm1) = "" Then
                                            strRm1 = drAC(j).Item("remarks1")
                                        End If
                                        If Trim(strRm2) = "" Then
                                            strRm1 = drAC(j).Item("remarks2")
                                        End If
                                        If Trim(strRm2) = "" Then
                                            strRm1 = drAC(j).Item("remarks3")
                                        End If
                                        If (dblSt > CDbl(drAC(j).Item("StartTime"))) Or dblSt = 0 Then
                                            dblSt = CDbl(drAC(j).Item("StartTime"))
                                        End If

                                        If dblEd < CDbl(drAC(j).Item("EndTime")) Then
                                            dblEd = CDbl(drAC(j).Item("EndTime"))
                                        End If

                                        HasAc.Add(drAC(j).Item("IDno"), drAC(j).Item("IDno"))

                                    Next
                                    drExpTb("Received") = intRecQty
                                    drExpTb("Completed") = intComQty
                                    drExpTb("Rejected") = intRejQty
                                    drExpTb("NextOp") = .Item("JobSuffix")
                                    drExpTb("StartTime") = dblSt
                                    drExpTb("EndTime") = dblEd
                                    drExpTb("remarks1") = strRm1
                                    drExpTb("remarks2") = strRm2
                                    drExpTb("remarks3") = strRm3
                                    dsExtable.Tables("tbExportOrg").Rows.Add(drExpTb)
                                End If
                            End If
                        End With
                    Next
                End If
                ExportDatatoExcel(dsExtable, dsExport, HsCur, HsHis)
                '  DBG1.DataSource = dsExtable.Tables("tbExportOrg")
                DBG1.DataSource = dsExport.Tables("tbExport")
            Else
                lblDataV.Text = ""
                MsgBox("Sorry there is no data to Export!", MsgBoxStyle.Information, "eWIP")
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Information, "eWIP")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "eWIP")
        Finally
            cn.Close()
        End Try
    End Sub
    Private Sub butRedo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butRedo.Click

        txtFile.Text = ""
        lblDataV.Text = ""
        PB1.Value = 0
        PB1.Value = 0
        PB1.Minimum = 0
        lblTotal.Text = 0
        lblRunning.Text = 0

        Dim myTrans As System.Data.SqlClient.SqlTransaction
        Try
            Dim result As New DialogResult
            result = MessageBox.Show("do you want reto last export data?", "eWIP", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = Windows.Forms.DialogResult.Yes Then

                cn.Open()
                myTrans = cn.BeginTransaction
                Dim dblExDate As Double = Now.ToOADate
                With com

                    .Connection = cn
                    .Transaction = myTrans
                    .CommandType = CommandType.Text
                    .CommandText = " Update tbJobTransHis set _export='N',_exportHis='N',_ExportDate=0 where _ExportDate>=" & Val(txtRedoDate.Text)

                    .ExecuteNonQuery()
                    .Parameters.Clear()

                    .Connection = cn
                    .Transaction = myTrans
                    .CommandType = CommandType.Text
                    .CommandText = " Update tbJobTrans set _export='N',_ExportDate=0 where   _ExportDate>=" & Val(txtRedoDate.Text)
                    .ExecuteNonQuery()
                    .Parameters.Clear()
                    .Connection = cn
                    .Transaction = myTrans
                    .CommandType = CommandType.Text
                    .CommandText = " Update tbExportDate set _exportDate=0"
                    .ExecuteNonQuery()
                    .Parameters.Clear()

                    butRedo.Enabled = False
                    myTrans.Commit()
                    MsgBox("Redo has been successfully completed", MsgBoxStyle.Information, "eWIP")

                End With





            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "eWIP")
            myTrans.Rollback()
        Finally
            cn.Close()

        End Try
    End Sub
End Class