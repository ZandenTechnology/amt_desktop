Imports System.Data.SqlClient
Public Class frmUser
    Inherits System.Windows.Forms.Form
    Dim parm As SqlParameter
    Friend WithEvents butEXIT As System.Windows.Forms.Button
    Dim dr As SqlDataReader
    Dim strFoCus As Integer
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butDelete As System.Windows.Forms.Button
    Friend WithEvents butUpdate As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents txtIDNo As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents txtUserCode As System.Windows.Forms.TextBox
    Friend WithEvents txtUserName As System.Windows.Forms.TextBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents lstv As System.Windows.Forms.ListView
    Friend WithEvents cmpPrivilege As System.Windows.Forms.ComboBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.butEXIT = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.cmpPrivilege = New System.Windows.Forms.ComboBox
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.txtUserName = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtIDNo = New System.Windows.Forms.TextBox
        Me.txtName = New System.Windows.Forms.TextBox
        Me.txtUserCode = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.lstv = New System.Windows.Forms.ListView
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.butUpdate = New System.Windows.Forms.Button
        Me.butDelete = New System.Windows.Forms.Button
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel2.BackColor = System.Drawing.Color.DarkGray
        Me.Panel2.Controls.Add(Me.butEXIT)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Controls.Add(Me.butCancel)
        Me.Panel2.Controls.Add(Me.butSave)
        Me.Panel2.Controls.Add(Me.butUpdate)
        Me.Panel2.Controls.Add(Me.butDelete)
        Me.Panel2.Location = New System.Drawing.Point(5, 5)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(720, 549)
        Me.Panel2.TabIndex = 0
        '
        'butEXIT
        '
        Me.butEXIT.BackColor = System.Drawing.Color.Gray
        Me.butEXIT.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butEXIT.ForeColor = System.Drawing.Color.White
        Me.butEXIT.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butEXIT.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butEXIT.Location = New System.Drawing.Point(255, 2)
        Me.butEXIT.Name = "butEXIT"
        Me.butEXIT.Size = New System.Drawing.Size(57, 24)
        Me.butEXIT.TabIndex = 46
        Me.butEXIT.Text = "Exit"
        Me.butEXIT.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(663, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 15)
        Me.Label3.TabIndex = 35
        Me.Label3.Text = "USER "
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.cmpPrivilege)
        Me.Panel3.Controls.Add(Me.txtPassword)
        Me.Panel3.Controls.Add(Me.txtUserName)
        Me.Panel3.Controls.Add(Me.Label6)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.txtIDNo)
        Me.Panel3.Controls.Add(Me.txtName)
        Me.Panel3.Controls.Add(Me.txtUserCode)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Location = New System.Drawing.Point(8, 29)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(704, 513)
        Me.Panel3.TabIndex = 0
        '
        'cmpPrivilege
        '
        Me.cmpPrivilege.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmpPrivilege.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.cmpPrivilege.Items.AddRange(New Object() {"-Select-", "Admin", "Supervisor", "Rework"})
        Me.cmpPrivilege.Location = New System.Drawing.Point(116, 130)
        Me.cmpPrivilege.Name = "cmpPrivilege"
        Me.cmpPrivilege.Size = New System.Drawing.Size(121, 22)
        Me.cmpPrivilege.TabIndex = 45
        '
        'txtPassword
        '
        Me.txtPassword.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.txtPassword.Location = New System.Drawing.Point(116, 100)
        Me.txtPassword.MaxLength = 30
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(176, 20)
        Me.txtPassword.TabIndex = 44
        '
        'txtUserName
        '
        Me.txtUserName.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.txtUserName.Location = New System.Drawing.Point(116, 70)
        Me.txtUserName.MaxLength = 30
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.Size = New System.Drawing.Size(176, 20)
        Me.txtUserName.TabIndex = 43
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 132)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 15)
        Me.Label6.TabIndex = 42
        Me.Label6.Text = "Privilege"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(8, 102)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(65, 15)
        Me.Label5.TabIndex = 41
        Me.Label5.Text = "Password"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 15)
        Me.Label4.TabIndex = 40
        Me.Label4.Text = "User Name"
        '
        'txtIDNo
        '
        Me.txtIDNo.Location = New System.Drawing.Point(247, 12)
        Me.txtIDNo.MaxLength = 30
        Me.txtIDNo.Name = "txtIDNo"
        Me.txtIDNo.Size = New System.Drawing.Size(28, 20)
        Me.txtIDNo.TabIndex = 38
        Me.txtIDNo.Visible = False
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.txtName.Location = New System.Drawing.Point(116, 40)
        Me.txtName.MaxLength = 256
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(488, 20)
        Me.txtName.TabIndex = 37
        '
        'txtUserCode
        '
        Me.txtUserCode.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.txtUserCode.Location = New System.Drawing.Point(116, 10)
        Me.txtUserCode.MaxLength = 15
        Me.txtUserCode.Name = "txtUserCode"
        Me.txtUserCode.Size = New System.Drawing.Size(121, 20)
        Me.txtUserCode.TabIndex = 36
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 15)
        Me.Label2.TabIndex = 35
        Me.Label2.Text = "Name"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 15)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "User ID"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.LightGray
        Me.Panel5.Controls.Add(Me.lstv)
        Me.Panel5.Location = New System.Drawing.Point(8, 166)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(688, 336)
        Me.Panel5.TabIndex = 1
        '
        'lstv
        '
        Me.lstv.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(8, 8)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(672, 320)
        Me.lstv.TabIndex = 7
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'butCancel
        '
        Me.butCancel.BackColor = System.Drawing.Color.Gray
        Me.butCancel.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butCancel.ForeColor = System.Drawing.Color.White
        Me.butCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(194, 2)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(57, 24)
        Me.butCancel.TabIndex = 3
        Me.butCancel.Text = "Cancel"
        Me.butCancel.UseVisualStyleBackColor = False
        '
        'butSave
        '
        Me.butSave.BackColor = System.Drawing.Color.Gray
        Me.butSave.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butSave.ForeColor = System.Drawing.Color.White
        Me.butSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(11, 2)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(57, 24)
        Me.butSave.TabIndex = 0
        Me.butSave.Text = "Save"
        Me.butSave.UseVisualStyleBackColor = False
        '
        'butUpdate
        '
        Me.butUpdate.BackColor = System.Drawing.Color.Gray
        Me.butUpdate.Enabled = False
        Me.butUpdate.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butUpdate.ForeColor = System.Drawing.Color.White
        Me.butUpdate.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butUpdate.Location = New System.Drawing.Point(72, 2)
        Me.butUpdate.Name = "butUpdate"
        Me.butUpdate.Size = New System.Drawing.Size(57, 24)
        Me.butUpdate.TabIndex = 1
        Me.butUpdate.Text = "Update"
        Me.butUpdate.UseVisualStyleBackColor = False
        '
        'butDelete
        '
        Me.butDelete.BackColor = System.Drawing.Color.Gray
        Me.butDelete.Enabled = False
        Me.butDelete.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butDelete.ForeColor = System.Drawing.Color.White
        Me.butDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butDelete.Location = New System.Drawing.Point(133, 2)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(57, 24)
        Me.butDelete.TabIndex = 2
        Me.butDelete.Text = "Delete"
        Me.butDelete.UseVisualStyleBackColor = False
        '
        'frmUser
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.Ivory
        Me.ClientSize = New System.Drawing.Size(729, 557)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmUser"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim strclsH As String
    Private Sub frmUser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With lstv
            .Columns.Add(New ColHeader("Uid", 0, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("User ID", 70, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Name", 225, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("User Name", 155, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Password", 0, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Privilege", lstv.Width - 450, HorizontalAlignment.Left, True))
            list_Displaydata()
            cmpPrivilege.Text = "-Select-"
        End With
    End Sub
    Sub list_Displaydata()
        lstv.Items.Clear()
        Try
            com.Connection = cn
            com.CommandType = CommandType.Text
            strclsH = " where  _userName='" & Trim(Replace(LogUserName, "'", "''")) & "'"
            If UCase(LogPrivilege) = UCase("ADMIN") Then
                com.CommandText = "select * from tbUser"
                cmpPrivilege.Enabled = True
            Else
                com.CommandText = "select * from tbUser " & strclsH
                cmpPrivilege.Enabled = False
            End If
            cn.Open()
            dr = com.ExecuteReader(CommandBehavior.CloseConnection)
            Dim rcount As Integer = 0
            Dim i As Integer = -1
            While dr.Read
                i = i + 1
                Dim ls As New ListViewItem(Trim(dr.Item("_idno")))    ' you can also use reader.GetSqlValue(0) 
                ls.SubItems.Add(Trim(dr.Item("_userID")))
                ls.SubItems.Add(Trim(dr.Item("_name")))
                ls.SubItems.Add(Trim(dr.Item("_userName")))
                ls.SubItems.Add(Trim(dr.Item("_password")))
                ls.SubItems.Add(Trim(dr.Item("_privilege")))
                lstv.Items.Add(ls)
                If strFoCus = dr.Item("_idno") Then
                    rcount = i
                End If
            End While
            lstv.Focus()
            If i <> -1 Then
                lstv.Items(rcount).Selected = True
            End If
            strFoCus = 0
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(txtUserCode.Text) = "" Then
            MsgBox("Enter the user id!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtName.Text) = "" Then
            MsgBox("Enter the name!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtUserName.Text) = "" Then
            MsgBox("Enter the username!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtPassword.Text) = "" Then
            MsgBox("Enter the password!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(cmpPrivilege.Text) = "" Or Trim(cmpPrivilege.Text) = "-Select-" Then
            MsgBox("Select the privilege!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "insert into tbUser(_userID,_name,_userName,_password,_privilege)values(@userID,@name,@userName,@password,@privilege)"
                parm = .Parameters.Add("@userID", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtUserCode.Text)
                parm = .Parameters.Add("@name", SqlDbType.VarChar, 256)
                parm.Value = refTxt(txtName.Text)
                parm = .Parameters.Add("@userName", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtUserName.Text)
                parm = .Parameters.Add("@password", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtPassword.Text)
                parm = .Parameters.Add("@privilege", SqlDbType.VarChar, 50)
                parm.Value = refTxt(cmpPrivilege.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully saved!", MsgBoxStyle.Information, "eWIP")
            End With
        Catch ex As SqlException
            If ex.Number = 2627 Then
                MsgBox("This user id already exist", MsgBoxStyle.Critical, "eWIP")
            ElseIf ex.Number = 2601 Then
                MsgBox("This user name already exist", MsgBoxStyle.Critical, "eWIP")
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub
    Sub textReset()
        txtIDNo.Clear()
        txtUserCode.Clear()
        txtName.Clear()
        txtUserName.Clear()
        txtPassword.Clear()
        cmpPrivilege.Text = "-Select-"
        butSave.Enabled = True
        butDelete.Enabled = False
        butUpdate.Enabled = False
    End Sub
    Function refTxt(ByVal txt As String) As String
        Return Trim(Replace(txt, "'", "''"))
    End Function

    Private Sub butUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdate.Click
        If Trim(txtUserCode.Text) = "" Then
            MsgBox("Enter the user id!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtName.Text) = "" Then
            MsgBox("Enter the name!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtUserName.Text) = "" Then
            MsgBox("Enter the username!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtPassword.Text) = "" Then
            MsgBox("Enter the password!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(cmpPrivilege.Text) = "" Or Trim(cmpPrivilege.Text) = "-Select-" Then
            MsgBox("Select the privilege!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Try
            With com
                strFoCus = Val(txtIDNo.Text)
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "Update tbUser set _userID=@userID,_name=@name,_userName=@userName,_password=@password,_privilege=@privilege where _idno=@idno"
                parm = .Parameters.Add("@userID", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtUserCode.Text)
                parm = .Parameters.Add("@name", SqlDbType.VarChar, 256)
                parm.Value = refTxt(txtName.Text)
                parm = .Parameters.Add("@userName", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtUserName.Text)
                parm = .Parameters.Add("@password", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtPassword.Text)
                parm = .Parameters.Add("@privilege", SqlDbType.VarChar, 50)
                parm.Value = refTxt(cmpPrivilege.Text)
                parm = .Parameters.Add("@idno", SqlDbType.Int)
                parm.Value = Val(txtIDNo.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully saved!", MsgBoxStyle.Information, "eWIP")
            End With
        Catch ex As SqlException
            If ex.Number = 2627 Then
                MsgBox("This user id already exist", MsgBoxStyle.Critical, "eWIP")
            ElseIf ex.Number = 2601 Then
                MsgBox("This user name already exist", MsgBoxStyle.Critical, "eWIP")
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        Dim result As New DialogResult
        result = MessageBox.Show("Do you Want to delete this Records?", "Caption", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = Windows.Forms.DialogResult.Yes Then
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "delete from  tbUser  where _idno=@idno"
                    parm = .Parameters.Add("@idno", SqlDbType.Int)
                    parm.Value = refTxt(txtIDNo.Text)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()
                    textReset()
                    MsgBox("Successfully saved!", MsgBoxStyle.Information, "eWIP")
                End With
            Catch ex As SqlException
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Finally
                cn.Close()
                com.Parameters.Clear()
            End Try
            list_Displaydata()
        End If
    End Sub

    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub

    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click
        txtIDNo.Text = lstv.SelectedItems(0).Text
        txtUserCode.Text = lstv.SelectedItems(0).SubItems(1).Text
        txtName.Text = lstv.SelectedItems(0).SubItems(2).Text
        txtUserName.Text = lstv.SelectedItems(0).SubItems(3).Text
        txtPassword.Text = lstv.SelectedItems(0).SubItems(4).Text
        cmpPrivilege.Text = lstv.SelectedItems(0).SubItems(5).Text
        butSave.Enabled = False
        butUpdate.Enabled = True
        butDelete.Enabled = True
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        textReset()
    End Sub

    Private Sub lstv_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lstv.ColumnClick
        Dim clickedCol As ColHeader = CType(Me.lstv.Columns(e.Column), ColHeader)

        clickedCol.ascending = Not clickedCol.ascending
        '_userID,_name,_userName,_password,_privilege
        Dim strby As String = ""
        If clickedCol.ascending = False Then
            strby = " DESC"
        End If
        Select Case e.Column
            Case 0
                strclsH = " Order by _idno " & strby
            Case 1
                strclsH = " Order by _userID " & strby
            Case 2
                strclsH = " Order by _name " & strby
            Case 3
                strclsH = " Order by _userName " & strby
            Case 4
                strclsH = " Order by _password " & strby
            Case 5
                strclsH = " Order by _privilege " & strby
        End Select
       



        Dim numItems As Integer = Me.lstv.Items.Count
        Me.lstv.BeginUpdate()
        Dim SortArray As New ArrayList
        Dim i As Integer
        For i = 0 To numItems - 1
            SortArray.Add(New SortWrapper(Me.lstv.Items(i), e.Column))
        Next i
        SortArray.Sort(0, SortArray.Count, New SortWrapper.SortComparer(clickedCol.ascending))
        Me.lstv.Items.Clear()
        Dim z As Integer
        For z = 0 To numItems - 1
            Me.lstv.Items.Add(CType(SortArray(z), SortWrapper).sortItem)
        Next z
        Me.lstv.EndUpdate()
    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub
End Class
