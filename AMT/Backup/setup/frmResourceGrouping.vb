Imports System.Data.SqlClient
Public Class frmResourceGrouping
    Inherits System.Windows.Forms.Form
    Dim parm As SqlParameter
    Friend WithEvents butEXIT As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butDelete As System.Windows.Forms.Button
    Friend WithEvents butUpdate As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Dim dr As SqlDataReader
    Friend WithEvents ckbTransfer As System.Windows.Forms.CheckBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Dim strFoCus As Integer = 0
    Friend WithEvents txtCC As System.Windows.Forms.TextBox
    Friend WithEvents txtTo As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtYield As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Dim strclsH As String
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents txtRIDNo As System.Windows.Forms.TextBox
    Friend WithEvents txtRGDescription As System.Windows.Forms.TextBox
    Friend WithEvents txtRGID As System.Windows.Forms.TextBox
    Friend WithEvents lstv As System.Windows.Forms.ListView
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.butEXIT = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.butDelete = New System.Windows.Forms.Button
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.txtCC = New System.Windows.Forms.TextBox
        Me.txtTo = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.ckbTransfer = New System.Windows.Forms.CheckBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtRIDNo = New System.Windows.Forms.TextBox
        Me.txtRGDescription = New System.Windows.Forms.TextBox
        Me.txtRGID = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.lstv = New System.Windows.Forms.ListView
        Me.butUpdate = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtYield = New System.Windows.Forms.TextBox
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel2.BackColor = System.Drawing.Color.DarkGray
        Me.Panel2.Controls.Add(Me.butEXIT)
        Me.Panel2.Controls.Add(Me.butCancel)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.butDelete)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Controls.Add(Me.butUpdate)
        Me.Panel2.Controls.Add(Me.butSave)
        Me.Panel2.Location = New System.Drawing.Point(6, 21)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(713, 574)
        Me.Panel2.TabIndex = 0
        '
        'butEXIT
        '
        Me.butEXIT.BackColor = System.Drawing.Color.Gray
        Me.butEXIT.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butEXIT.ForeColor = System.Drawing.Color.White
        Me.butEXIT.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butEXIT.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butEXIT.Location = New System.Drawing.Point(263, 4)
        Me.butEXIT.Name = "butEXIT"
        Me.butEXIT.Size = New System.Drawing.Size(57, 24)
        Me.butEXIT.TabIndex = 51
        Me.butEXIT.Text = "Exit"
        Me.butEXIT.UseVisualStyleBackColor = False
        '
        'butCancel
        '
        Me.butCancel.BackColor = System.Drawing.Color.Gray
        Me.butCancel.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butCancel.ForeColor = System.Drawing.Color.White
        Me.butCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(202, 4)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(57, 24)
        Me.butCancel.TabIndex = 50
        Me.butCancel.Text = "Cancel"
        Me.butCancel.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(596, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(114, 15)
        Me.Label3.TabIndex = 35
        Me.Label3.Text = "RESOURCE GROUP"
        '
        'butDelete
        '
        Me.butDelete.BackColor = System.Drawing.Color.Gray
        Me.butDelete.Enabled = False
        Me.butDelete.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butDelete.ForeColor = System.Drawing.Color.White
        Me.butDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butDelete.Location = New System.Drawing.Point(141, 4)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(57, 24)
        Me.butDelete.TabIndex = 49
        Me.butDelete.Text = "Delete"
        Me.butDelete.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.txtYield)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.txtCC)
        Me.Panel3.Controls.Add(Me.txtTo)
        Me.Panel3.Controls.Add(Me.Label6)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.ckbTransfer)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.txtRIDNo)
        Me.Panel3.Controls.Add(Me.txtRGDescription)
        Me.Panel3.Controls.Add(Me.txtRGID)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Location = New System.Drawing.Point(5, 34)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(704, 534)
        Me.Panel3.TabIndex = 0
        '
        'txtCC
        '
        Me.txtCC.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.txtCC.Location = New System.Drawing.Point(128, 111)
        Me.txtCC.MaxLength = 256
        Me.txtCC.Multiline = True
        Me.txtCC.Name = "txtCC"
        Me.txtCC.Size = New System.Drawing.Size(568, 39)
        Me.txtCC.TabIndex = 112
        '
        'txtTo
        '
        Me.txtTo.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.txtTo.Location = New System.Drawing.Point(128, 62)
        Me.txtTo.MaxLength = 256
        Me.txtTo.Multiline = True
        Me.txtTo.Name = "txtTo"
        Me.txtTo.Size = New System.Drawing.Size(568, 43)
        Me.txtTo.TabIndex = 111
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(17, 123)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(62, 15)
        Me.Label6.TabIndex = 110
        Me.Label6.Text = "Email(CC)"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(17, 76)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 15)
        Me.Label4.TabIndex = 109
        Me.Label4.Text = "Email(To)"
        '
        'ckbTransfer
        '
        Me.ckbTransfer.AutoSize = True
        Me.ckbTransfer.Location = New System.Drawing.Point(128, 182)
        Me.ckbTransfer.Name = "ckbTransfer"
        Me.ckbTransfer.Size = New System.Drawing.Size(15, 14)
        Me.ckbTransfer.TabIndex = 108
        Me.ckbTransfer.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(17, 182)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 15)
        Me.Label5.TabIndex = 107
        Me.Label5.Text = "Transfer"
        '
        'txtRIDNo
        '
        Me.txtRIDNo.Location = New System.Drawing.Point(344, 10)
        Me.txtRIDNo.MaxLength = 30
        Me.txtRIDNo.Name = "txtRIDNo"
        Me.txtRIDNo.Size = New System.Drawing.Size(28, 20)
        Me.txtRIDNo.TabIndex = 38
        Me.txtRIDNo.Visible = False
        '
        'txtRGDescription
        '
        Me.txtRGDescription.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.txtRGDescription.Location = New System.Drawing.Point(128, 36)
        Me.txtRGDescription.MaxLength = 256
        Me.txtRGDescription.Name = "txtRGDescription"
        Me.txtRGDescription.Size = New System.Drawing.Size(488, 20)
        Me.txtRGDescription.TabIndex = 37
        '
        'txtRGID
        '
        Me.txtRGID.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.txtRGID.Location = New System.Drawing.Point(128, 10)
        Me.txtRGID.MaxLength = 30
        Me.txtRGID.Name = "txtRGID"
        Me.txtRGID.Size = New System.Drawing.Size(176, 20)
        Me.txtRGID.TabIndex = 36
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(17, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 15)
        Me.Label2.TabIndex = 35
        Me.Label2.Text = "Description"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(17, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 15)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "Group Code"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.LightGray
        Me.Panel5.Controls.Add(Me.lstv)
        Me.Panel5.Location = New System.Drawing.Point(8, 205)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(688, 325)
        Me.Panel5.TabIndex = 1
        '
        'lstv
        '
        Me.lstv.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(8, 6)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(672, 312)
        Me.lstv.TabIndex = 7
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'butUpdate
        '
        Me.butUpdate.BackColor = System.Drawing.Color.Gray
        Me.butUpdate.Enabled = False
        Me.butUpdate.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butUpdate.ForeColor = System.Drawing.Color.White
        Me.butUpdate.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butUpdate.Location = New System.Drawing.Point(80, 4)
        Me.butUpdate.Name = "butUpdate"
        Me.butUpdate.Size = New System.Drawing.Size(57, 24)
        Me.butUpdate.TabIndex = 48
        Me.butUpdate.Text = "Update"
        Me.butUpdate.UseVisualStyleBackColor = False
        '
        'butSave
        '
        Me.butSave.BackColor = System.Drawing.Color.Gray
        Me.butSave.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butSave.ForeColor = System.Drawing.Color.White
        Me.butSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(19, 4)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(57, 24)
        Me.butSave.TabIndex = 47
        Me.butSave.Text = "Save"
        Me.butSave.UseVisualStyleBackColor = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(17, 159)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 15)
        Me.Label7.TabIndex = 113
        Me.Label7.Text = "Yield(%) "
        '
        'txtYield
        '
        Me.txtYield.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.txtYield.Location = New System.Drawing.Point(128, 156)
        Me.txtYield.MaxLength = 6
        Me.txtYield.Name = "txtYield"
        Me.txtYield.Size = New System.Drawing.Size(97, 20)
        Me.txtYield.TabIndex = 114
        '
        'frmResourceGrouping
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.Ivory
        Me.ClientSize = New System.Drawing.Size(725, 607)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmResourceGrouping"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(txtRGID.Text) = "" Then
            MsgBox("Enter the resource group code!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtRGDescription.Text) = "" Then
            MsgBox("Enter the resource group description!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If



        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "insert into tbResourceGroup( _rGroupID,_rGroupName,_transfer,_to,_cc,_yieldper)values( @rGroupID,@rGroupName,@transfer,@to,@cc,@yieldper)"
                parm = .Parameters.Add("@rGroupID", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtRGID.Text)
                parm = .Parameters.Add("@rGroupName", SqlDbType.VarChar, 256)
                parm.Value = refTxt(txtRGDescription.Text)
                parm = .Parameters.Add("@transfer", SqlDbType.Char, 1)
                If ckbTransfer.Checked = True Then
                    parm.Value = "Y"
                Else
                    parm.Value = "N"
                End If
                parm = .Parameters.Add("@to", SqlDbType.VarChar, 500)
                parm.Value = Trim(txtTo.Text)
                parm = .Parameters.Add("@CC", SqlDbType.VarChar, 500)
                parm.Value = Trim(txtCC.Text)
                parm = .Parameters.Add("@yieldper", SqlDbType.Float)
                parm.Value = Val(txtYield.Text)
                'parm = .Parameters.Add("@Type", SqlDbType.VarChar, 256)
                'parm.Value = refTxt(cmbType.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully saved!", MsgBoxStyle.Information, "eWIP")
            End With
        Catch ex As SqlException
            If ex.Number = 2627 Then
                MsgBox("This group code already exist", MsgBoxStyle.Critical, "eWIP")
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub
    Sub list_Displaydata()
        lstv.Items.Clear()
        Try
            com.Connection = cn
            com.CommandType = CommandType.Text

            com.CommandText = "select * from tbResourceGroup " & strclsH
            'parm = com.Parameters.Add("@Type", SqlDbType.VarChar, 256)
            'parm.Value = refTxt(cmbType.Text)
            cn.Open()
            dr = com.ExecuteReader(CommandBehavior.CloseConnection)
            Dim i As Integer = -1
            Dim rcount As Integer
            While dr.Read
                i = i + 1
                Dim ls As New ListViewItem(Trim(dr.Item("_rid")))    ' you can also use reader.GetSqlValue(0) 
                ls.SubItems.Add(Trim(dr.Item("_rGroupID")))
                ls.SubItems.Add(Trim(dr.Item("_rGroupName")))
                ls.SubItems.Add(Trim(dr.Item("_transfer")))
                ls.SubItems.Add(Trim(dr.Item("_to")))
                ls.SubItems.Add(Trim(dr.Item("_cc")))
                ls.SubItems.Add(Trim(dr.Item("_yieldper")))
                lstv.Items.Add(ls)

                If strFoCus = dr.Item("_rid") Then
                    rcount = i
                End If
            End While
            If i <> -1 Then
                lstv.Focus()
                lstv.Items(rcount).Selected = True
            End If
            strFoCus = 0
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            com.Parameters.Clear()
            cn.Close()
        End Try
    End Sub

    Private Sub frmResourceGrouping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With lstv
            .Columns.Add(New ColHeader("IDNo", 0, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Group Code", 225, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Description", lstv.Width - 190, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Transfer", lstv.Width - 60, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Email(To)", 200, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Email(CC)", 200, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Yield(%)", 200, HorizontalAlignment.Left, True))
            list_Displaydata()
            'list_Displaydata()
        End With
    End Sub
    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click

        txtRIDNo.Text = lstv.SelectedItems(0).Text
        txtRGID.Text = lstv.SelectedItems(0).SubItems(1).Text
        txtRGDescription.Text = lstv.SelectedItems(0).SubItems(2).Text
        If lstv.SelectedItems(0).SubItems(3).Text = "Y" Then
            ckbTransfer.Checked = True
        Else
            ckbTransfer.Checked = False
        End If
        txtTo.Text = lstv.SelectedItems(0).SubItems(4).Text
        txtCC.Text = lstv.SelectedItems(0).SubItems(5).Text
        txtYield.Text = lstv.SelectedItems(0).SubItems(6).Text
        butSave.Enabled = False
        butUpdate.Enabled = True
        butDelete.Enabled = True
        ' txtUserCode.ReadOnly = True
        'StbUser.Text = "You Can Edit And Delete This Records"
    End Sub
    Function refTxt(ByVal txt As String) As String
        Return Trim(Replace(txt, "'", "''"))
    End Function
    Sub textReset()
        txtRIDNo.Clear()
        txtRGID.Clear()
        txtRGDescription.Clear()
        txtCC.Text = ""
        txtTo.Text = ""
        ckbTransfer.Checked = False
        butSave.Enabled = True
        butDelete.Enabled = False
        butUpdate.Enabled = False
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        textReset()
    End Sub

    Private Sub butUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdate.Click
        If Trim(txtRGID.Text) = "" Then
            MsgBox("Enter the resource group code!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtRGDescription.Text) = "" Then
            MsgBox("Enter the resource group description!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Try
            With com
                strFoCus = Val(txtRIDNo.Text)
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "Update  tbResourceGroup set _rGroupID=@rGroupID,_rGroupName=@rGroupName,_transfer=@transfer,_to=@to,_CC=@cc,_yieldper=@yieldper where _rid =@rid"
                parm = .Parameters.Add("@rGroupID", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtRGID.Text)
                parm = .Parameters.Add("@rGroupName", SqlDbType.VarChar, 256)
                parm.Value = refTxt(txtRGDescription.Text)
                parm = .Parameters.Add("@rid", SqlDbType.Int)
                parm.Value = Val(txtRIDNo.Text)
                parm = .Parameters.Add("@transfer", SqlDbType.Char, 1)
                If ckbTransfer.Checked = True Then
                    parm.Value = "Y"
                Else
                    parm.Value = "N"
                End If
                parm = .Parameters.Add("@to", SqlDbType.VarChar, 500)
                parm.Value = Trim(txtTo.Text)
                parm = .Parameters.Add("@CC", SqlDbType.VarChar, 500)
                parm.Value = Trim(txtCC.Text)
                parm = .Parameters.Add("@yieldper", SqlDbType.Float)
                parm.Value = Val(txtYield.Text)

                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")
            End With
        Catch ex As SqlException
            If ex.Number = 2627 Then
                MsgBox("This group code already exist", MsgBoxStyle.Critical, "eWIP")
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try

        Try
            With com
                'strFoCus = Val(txtRIDNo.Text)
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "delete from tbWorkStation where _rid not in(select _rid from tbResourceGroup)"
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                'MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")
            End With
        Catch ex As SqlException
            If ex.Number = 2627 Then
                MsgBox("This group code already exist", MsgBoxStyle.Critical, "eWIP")
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try








        list_Displaydata()
    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        'If Trim(txtRGID.Text) = "" Then
        '    MsgBox("Enter the resource group id!", MsgBoxStyle.Information, "eWIP")
        '    Exit Sub
        'End If
        'If Trim(txtRGDescription.Text) = "" Then
        '    MsgBox("Enter the resource group description!", MsgBoxStyle.Information, "eWIP")
        '    Exit Sub
        'End If
        Dim result As New DialogResult
        result = MessageBox.Show("Do you Want to delete this Records?", "Caption", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = Windows.Forms.DialogResult.Yes Then
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "delete from   tbResourceGroup  where _rid =@rid "
                    parm = .Parameters.Add("@rid ", SqlDbType.Int)
                    parm.Value = Val(txtRIDNo.Text)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()
                    textReset()
                    MsgBox("Successfully deleted!", MsgBoxStyle.Information, "eWIP")
                End With
            Catch ex As SqlException
                If ex.Number = 2627 Then
                    MsgBox("This group code already exist", MsgBoxStyle.Critical, "eWIP")
                Else
                    MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Finally
                cn.Close()
                com.Parameters.Clear()
            End Try
            list_Displaydata()
        End If
    End Sub

    'Sub GetData()

    '    Try
    '        With com
    '            .Connection = cn
    '            .CommandType = CommandType.Text
    '            Dim DI As DataItem
    '            .CommandText = "select  _desc from tbResourceType"
    '            cn.Open()
    '            dr = .ExecuteReader(CommandBehavior.CloseConnection)
    '            While dr.Read
    '                cmbType.Items.Add(dr.Item("_desc"))
    '            End While
    '            cn.Close()
    '        End With
    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
    '    Finally
    '        cn.Close()
    '    End Try
    '    cmbType.Text = "-Select-"
    'End Sub

    Private Sub cmbType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        list_Displaydata()
    End Sub


    Private Sub lstv_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lstv.ColumnClick


        Dim clickedCol As ColHeader = CType(Me.lstv.Columns(e.Column), ColHeader)


        clickedCol.ascending = Not clickedCol.ascending


        Dim strby As String = ""
        If clickedCol.ascending = False Then
            strby = " DESC"
        End If

        Select Case e.Column
            Case 0
                strclsH = " Order by _rid " & strby
            Case 1
                strclsH = " Order by _rGroupID " & strby
            Case 2
                strclsH = " Order by _rGroupName " & strby
            Case 3
                strclsH = " Order by _transfer " & strby
            Case 4
                strclsH = " Order by _to " & strby
            Case 5
                strclsH = " Order by _cc " & strby
        End Select



        Dim numItems As Integer = Me.lstv.Items.Count


        Me.lstv.BeginUpdate()


        Dim SortArray As New ArrayList
        Dim i As Integer
        For i = 0 To numItems - 1
            SortArray.Add(New SortWrapper(Me.lstv.Items(i), e.Column))
        Next i


        SortArray.Sort(0, SortArray.Count, New SortWrapper.SortComparer(clickedCol.ascending))


        Me.lstv.Items.Clear()
        Dim z As Integer
        For z = 0 To numItems - 1
            Me.lstv.Items.Add(CType(SortArray(z), SortWrapper).sortItem)
        Next z

        Me.lstv.EndUpdate()


    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub

    
    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub

    Private Sub txtTo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTo.KeyPress
        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
        Else
            e.Handled = False
        End If
    End Sub

    Private Sub txtCC_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCC.KeyPress
        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
        Else
            e.Handled = False
        End If
    End Sub

   
   
End Class
