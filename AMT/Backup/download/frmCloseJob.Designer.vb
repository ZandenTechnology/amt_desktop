<[Global].Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
[Partial] Class frmCloseJob
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components [IsNot] Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.butData = New System.Windows.Forms.Button
        Me.txtJob = New System.Windows.Forms.TextBox
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.PB1 = New System.Windows.Forms.ProgressBar
        Me.lblTot = New System.Windows.Forms.Label
        Me.Panel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'butData
        '
        Me.butData.Location = New System.Drawing.Point(328, 274)
        Me.butData.Name = "butData"
        Me.butData.Size = New System.Drawing.Size(75, 23)
        Me.butData.TabIndex = 3
        Me.butData.Text = "DONE"
        Me.butData.UseVisualStyleBackColor = True
        '
        'txtJob
        '
        Me.txtJob.Location = New System.Drawing.Point(178, 12)
        Me.txtJob.Multiline = True
        Me.txtJob.Name = "txtJob"
        Me.txtJob.Size = New System.Drawing.Size(370, 256)
        Me.txtJob.TabIndex = 2
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Gray
        Me.Panel4.Controls.Add(Me.PB1)
        Me.Panel4.Location = New System.Drawing.Point(141, 309)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(448, 32)
        Me.Panel4.TabIndex = 54
        '
        'PB1
        '
        Me.PB1.Location = New System.Drawing.Point(8, 5)
        Me.PB1.Name = "PB1"
        Me.PB1.Size = New System.Drawing.Size(432, 23)
        Me.PB1.TabIndex = 0
        '
        'lblTot
        '
        Me.lblTot.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTot.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblTot.Location = New System.Drawing.Point(595, 314)
        Me.lblTot.Name = "lblTot"
        Me.lblTot.Size = New System.Drawing.Size(64, 17)
        Me.lblTot.TabIndex = 55
        '
        'frmCloseJob
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(730, 493)
        Me.Controls.Add(Me.lblTot)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.butData)
        Me.Controls.Add(Me.txtJob)
        Me.Name = "frmCloseJob"
        Me.Text = "frmCloseJob"
        Me.Panel4.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents butData As System.Windows.Forms.Button
    Friend WithEvents txtJob As System.Windows.Forms.TextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents PB1 As System.Windows.Forms.ProgressBar
    Friend WithEvents lblTot As System.Windows.Forms.Label
End Class
