Public Class MDINEW
    Dim selectForm
    Dim objOperator As frmOperative
    Dim objUser As frmUser
    Dim objRGrouping As frmResourceGrouping

    Dim objWorkStation As frmWCRCGroup

    Dim objRejGroup As frmWCRejectGrouping

    Dim objRejected As frmRejectMaster
    Dim objView As frmViewJob



    Dim objDownload As frmDownload
  
    Dim objDataEntry As frmSupJobEdit
    Dim objDataVerify As frmDataView

    Dim objop As frmDataEntryOP
   
    Dim objTansDis As frmTransDis
    Dim objMachine As frmMachine
   
    Dim objMachineWRK As frmMachineWRK
   
    'Dim objExport As frmExportData
    Dim objExport As frmNewExport
    Dim objExportIN As frmNewExportIN


    Dim objwc As New frmWorkCenter

    Dim objItem As New frmItem
    Dim objWIPReSourceGroupReport As New frmReport
    Dim objCompletedTransaction As New frmRptCompletedTransaction
    Dim objItemReport As New frmRptItem


    Dim objOPstatus As New frmOperationStatus
    Dim objFinalYield As New frmFinalYield
    Dim objWCYield As New frmWCYield
    Dim objFailureAnalysis As New frmRptFailureByItem
    Dim objdwloadall As New frmDownloadAll
    Dim objNewItemReport As New frmRPTItemStart
    Dim objrptCLResource As New frmRptTimebyResource
    Dim objrptCLoperator As New frmRptTimeByOperator
    Dim objrptCLItem As New frmRptTimeByItem
    Dim objrptException As New frmRptException
    Dim objInventoryTrans As frmWIPList
    Dim objStockList As frmStockList
    Dim objCustomer As frmCustomer
    Dim objDeliveryOrder As frmfrmDeliveryOrder
    Dim objInventoryImport As frmInventoryImport
    'Private Sub UserToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UserToolStripMenuItem1.Click
    '    frmClose()
    '    selectForm = "User"
    '    frmOpen()
    'End Sub

    Sub frmClose()
        Select Case selectForm
            Case "Operator"
                objOperator.Close()
                selectForm = ""
            Case "User"
                objUser.Close()
                selectForm = ""
            Case "RGrouping"
                objRGrouping.Close()
                selectForm = ""
            Case "WStation"
                objWorkStation.Close()
                selectForm = ""
            Case "RejectedGroup"
                objRejGroup.Close()
                selectForm = ""
            Case "Download"
                objDownload.Close()
                selectForm = ""
            Case "View"                            'By Yeo 20081011
                objView.Close()
                selectForm = ""
            Case "DataEntry"                            'By Yeo 20081011
                objDataEntry.Close()
                selectForm = ""
            Case "DataVerify"                            'By Yeo 20081011
                objDataVerify.Close()
                selectForm = ""
            Case "OP"                            'By Yeo 20081011
                objop.Close()
                selectForm = ""
            Case "DataTran"
                objTansDis.Close()
                selectForm = ""
            Case "Machine"
                objMachine.Close()
                selectForm = ""
            Case "MachineWRK"
                objMachineWRK.Close()
                selectForm = ""
            Case "Export"
                objExport.Close()
                selectForm = ""
            Case "ExportIN"
                objExportIN.Close()
                selectForm = ""
            Case "Rejected"
                objRejected.Close()
                selectForm = ""
            Case "WC"
                objwc.Close()
                selectForm = ""
            Case "Item"
                objItem.Close()
                selectForm = ""
            Case "ActionControl"
                objOPstatus.Close()
                selectForm = ""
            Case "WIPResourceGroupReport"
                objWIPReSourceGroupReport.Close()
                selectForm = ""
            Case "Completed Transaction"
                objCompletedTransaction.Close()
                selectForm = ""
            Case "Item Report"
                objItemReport.Close()
                selectForm = ""
            Case "Final Yield"
                objFinalYield.Close()
                selectForm = ""
            Case "WC Yield"
                objWCYield.Close()
                selectForm = ""
            Case "Failure Analysis"
                objFailureAnalysis.Close()
                selectForm = ""
            Case "DownloadALL"
                objdwloadall.Close()
                selectForm = ""
            Case "NewItemReport"
                objNewItemReport.Close()
                selectForm = ""
            Case "NewItemReportResource"
                objNewItemReport.Close()
                selectForm = ""
            Case "Actual Cycle Time by Resource Group"
                objrptCLResource.Close()
                selectForm = ""
            Case "Actual Cycle Time by Resource Group(Resource Group)"
                objrptCLResource.Close()
                selectForm = ""
            Case "Actual Cycle Time by Operator"
                objrptCLoperator.Close()
                selectForm = ""
            Case "Actual Cycle Time by Resource Group(Work Center / Machines)"
                objrptCLResource.Close()
                selectForm = ""
            Case "Exception Report"
                objrptException.Close()
                selectForm = ""

            Case "Invetory Transfer"
                objInventoryTrans.Close()
                selectForm = ""
            Case "JobStockCheck"
                objStockList.Close()
                selectForm = ""
            Case "Customer"
                objCustomer.Close()
                selectForm = ""
            Case "Delivery Order"
                objDeliveryOrder.Close()
                selectForm = ""
            Case "Inventory Import"
                objInventoryImport.Close()
                selectForm = ""
        End Select
    End Sub

    Sub frmOpen()
        Select Case selectForm
            Case "Operator"
                objOperator = New frmOperative
                SetParent(objOperator.Handle.ToInt32, P1.Handle.ToInt32)
                objOperator.Left = -P1.Left
                objOperator.Top = -P1.Top
                objOperator.Width = P1.Width
                objOperator.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objOperator.Show()
                Pwait.Visible = False
            Case "User"
                objUser = New frmUser
                SetParent(objUser.Handle.ToInt32, P1.Handle.ToInt32)
                objUser.Left = -P1.Left
                objUser.Top = -P1.Top
                objUser.Width = P1.Width
                objUser.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objUser.Show()
                Pwait.Visible = False
            Case "RGrouping"
                objRGrouping = New frmResourceGrouping
                SetParent(objRGrouping.Handle.ToInt32, P1.Handle.ToInt32)
                objRGrouping.Left = -P1.Left
                objRGrouping.Top = -P1.Top
                objRGrouping.Width = P1.Width
                objRGrouping.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objRGrouping.Show()
                Pwait.Visible = False
            Case "WStation"
                objWorkStation = New frmWCRCGroup
                SetParent(objWorkStation.Handle.ToInt32, P1.Handle.ToInt32)
                objWorkStation.Left = -P1.Left
                objWorkStation.Top = -P1.Top
                objWorkStation.Width = P1.Width
                objWorkStation.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objWorkStation.Show()
                Pwait.Visible = False
            Case "WC"
                objwc = New frmWorkCenter
                SetParent(objwc.Handle.ToInt32, P1.Handle.ToInt32)
                objwc.Left = -P1.Left
                objwc.Top = -P1.Top
                objwc.Width = P1.Width
                objwc.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objwc.Show()
                Pwait.Visible = False
            Case "Item"
                objItem = New frmItem
                SetParent(objItem.Handle.ToInt32, P1.Handle.ToInt32)
                objItem.Left = -P1.Left
                objItem.Top = -P1.Top
                objItem.Width = P1.Width
                objItem.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objItem.Show()
                Pwait.Visible = False
            Case "RejectedGroup"
                objRejGroup = New frmWCRejectGrouping
                SetParent(objRejGroup.Handle.ToInt32, P1.Handle.ToInt32)
                objRejGroup.Left = -P1.Left
                objRejGroup.Top = -P1.Top
                objRejGroup.Width = P1.Width
                objRejGroup.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objRejGroup.Show()
                Pwait.Visible = False
            Case "Rejected"
                objRejected = New frmRejectMaster
                SetParent(objRejected.Handle.ToInt32, P1.Handle.ToInt32)
                objRejected.Left = -P1.Left
                objRejected.Top = -P1.Top
                objRejected.Width = P1.Width
                objRejected.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objRejected.Show()
                Pwait.Visible = False
            Case "Download"
                objDownload = New frmDownload
                SetParent(objDownload.Handle.ToInt32, P1.Handle.ToInt32)
                objDownload.Left = -P1.Left
                objDownload.Top = -P1.Top
                objDownload.Width = P1.Width
                objDownload.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objDownload.Show()
                Pwait.Visible = False

            Case "View"                                     'By Yeo 20081110
                objView = New frmViewJob
                SetParent(objView.Handle.ToInt32, P1.Handle.ToInt32)
                objView.Left = -P1.Left
                objView.Top = -P1.Top
                objView.Width = P1.Width
                objView.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objView.Show()
                Pwait.Visible = False
            Case "DataEntry"
                objDataEntry = New frmSupJobEdit
                SetParent(objDataEntry.Handle.ToInt32, P1.Handle.ToInt32)
                objDataEntry.Left = -P1.Left
                objDataEntry.Top = -P1.Top
                objDataEntry.Width = P1.Width
                objDataEntry.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objDataEntry.txtJobNo.Select()
                objDataEntry.Show()
                Pwait.Visible = False
            Case "OP"
                Try
                    objop = New frmDataEntryOP
                    'SetParent(objop.Handle.ToInt32, PMain.Handle.ToInt32)
                    objop.Left = -P1.Left
                    objop.Top = -P1.Top
                    objop.Width = P1.Width
                    objop.Height = P1.Height
                    Pwait.Visible = True
                    objop.txtC.Select()
                    Application.DoEvents()
                    objop.txtC.Select()
                    'Application.DoEvents()
                    objop.ShowDialog()
                    Application.Exit()
                    'objop.txtC.Focus()
                    ' objop.txtC.Select()
                    'Pwait.Visible = False
                Catch ex As Exception
                End Try

            Case "DataVerify"                            'By Yeo 20081011
                objDataVerify = New frmDataView
                SetParent(objDataVerify.Handle.ToInt32, P1.Handle.ToInt32)
                objDataVerify.Left = -P1.Left
                objDataVerify.Top = -P1.Top
                objDataVerify.Width = P1.Width
                objDataVerify.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objDataVerify.Show()
                Pwait.Visible = False
            Case "DataTran"
                objTansDis = New frmTransDis
                SetParent(objTansDis.Handle.ToInt32, P1.Handle.ToInt32)
                objTansDis.Left = -P1.Left
                objTansDis.Top = -P1.Top
                objTansDis.Width = P1.Width
                objTansDis.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objTansDis.Show()
                Pwait.Visible = False
            Case "Machine"
                objMachine = New frmMachine
                SetParent(objMachine.Handle.ToInt32, P1.Handle.ToInt32)
                objMachine.Left = -P1.Left
                objMachine.Top = -P1.Top
                objMachine.Width = P1.Width
                objMachine.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objMachine.Show()
                Pwait.Visible = False
            Case "MachineWRK"
                objMachineWRK = New frmMachineWRK
                SetParent(objMachineWRK.Handle.ToInt32, P1.Handle.ToInt32)
                objMachineWRK.Left = -P1.Left
                objMachineWRK.Top = -P1.Top
                objMachineWRK.Width = P1.Width
                objMachineWRK.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objMachineWRK.Show()
                Pwait.Visible = False
            Case "Export"
                objExport = New frmNewExport
                SetParent(objExport.Handle.ToInt32, P1.Handle.ToInt32)
                objExport.Left = -P1.Left
                objExport.Top = -P1.Top
                objExport.Width = P1.Width
                objExport.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objExport.Show()
                Pwait.Visible = False
            Case "ExportIN"
                objExportIN = New frmNewExportIN
                SetParent(objExportIN.Handle.ToInt32, P1.Handle.ToInt32)
                objExportIN.Left = -P1.Left
                objExportIN.Top = -P1.Top
                objExportIN.Width = P1.Width
                objExportIN.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objExportIN.Show()
                Pwait.Visible = False
            Case "ActionControl"
                objOPstatus = New frmOperationStatus
                SetParent(objOPstatus.Handle.ToInt32, P1.Handle.ToInt32)
                objOPstatus.Left = -P1.Left
                objOPstatus.Top = -P1.Top
                objOPstatus.Width = P1.Width
                objOPstatus.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objOPstatus.Show()
                Pwait.Visible = False
            Case "WIPResourceGroupReport"
                objWIPReSourceGroupReport = New frmReport
                SetParent(objWIPReSourceGroupReport.Handle.ToInt32, P1.Handle.ToInt32)
                objWIPReSourceGroupReport.Left = -P1.Left
                objWIPReSourceGroupReport.Top = -P1.Top
                objWIPReSourceGroupReport.Width = P1.Width
                objWIPReSourceGroupReport.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objWIPReSourceGroupReport.Show()
                Pwait.Visible = False
            Case "Completed Transaction"

                objCompletedTransaction = New frmRptCompletedTransaction
                'objCompletedTransaction.MdiParent = Me
                SetParent(objCompletedTransaction.Handle.ToInt32, P1.Handle.ToInt32)
                objCompletedTransaction.Left = -P1.Left
                objCompletedTransaction.Top = -P1.Top
                objCompletedTransaction.Width = P1.Width
                objCompletedTransaction.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objCompletedTransaction.Show()
                Pwait.Visible = False

            Case "Item Report"
                objItemReport = New frmRptItem
                SetParent(objItemReport.Handle.ToInt32, P1.Handle.ToInt32)
                objItemReport.Left = -P1.Left
                objItemReport.Top = -P1.Top
                objItemReport.Width = P1.Width
                objItemReport.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objItemReport.Show()
                Pwait.Visible = False
            Case "Final Yield"
                objFinalYield = New frmFinalYield
                SetParent(objFinalYield.Handle.ToInt32, P1.Handle.ToInt32)
                objFinalYield.Left = -P1.Left
                objFinalYield.Top = -P1.Top
                objFinalYield.Width = P1.Width
                objFinalYield.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objFinalYield.Show()
                Pwait.Visible = False
            Case "WC Yield"
                objWCYield = New frmWCYield
                SetParent(objWCYield.Handle.ToInt32, P1.Handle.ToInt32)
                objWCYield.Left = -P1.Left
                objWCYield.Top = -P1.Top
                objWCYield.Width = P1.Width
                objWCYield.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objWCYield.Show()
                Pwait.Visible = False
            Case "Failure Analysis"
                objFailureAnalysis = New frmRptFailureByItem
                SetParent(objFailureAnalysis.Handle.ToInt32, P1.Handle.ToInt32)
                objFailureAnalysis.Left = -P1.Left
                objFailureAnalysis.Top = -P1.Top
                objFailureAnalysis.Width = P1.Width
                objFailureAnalysis.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objFailureAnalysis.Show()
                Pwait.Visible = False
            Case "DownloadALL"
                objdwloadall = New frmDownloadAll
                SetParent(objdwloadall.Handle.ToInt32, P1.Handle.ToInt32)
                objdwloadall.Left = -P1.Left
                objdwloadall.Top = -P1.Top
                objdwloadall.Width = P1.Width
                objdwloadall.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objdwloadall.Show()
                Pwait.Visible = False
            Case "NewItemReport"
                objNewItemReport = New frmRPTItemStart
                objNewItemReport.lblReportTit.Text = "WIP BY ITEM REPORT"
                SetParent(objNewItemReport.Handle.ToInt32, P1.Handle.ToInt32)
                objNewItemReport.Left = -P1.Left
                objNewItemReport.Top = -P1.Top
                objNewItemReport.Width = P1.Width
                objNewItemReport.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objNewItemReport.Show()
                Pwait.Visible = False
            Case "NewItemReportResource"
                objNewItemReport = New frmRPTItemStart
                objNewItemReport.lblReportTit.Text = UCase("WIP by Resource Group")
                SetParent(objNewItemReport.Handle.ToInt32, P1.Handle.ToInt32)
                objNewItemReport.Left = -P1.Left
                objNewItemReport.Top = -P1.Top
                objNewItemReport.Width = P1.Width
                objNewItemReport.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objNewItemReport.Show()
                Pwait.Visible = False


            Case "Actual Cycle Time by Resource Group(Work Center / Machines)"


                objrptCLResource = New frmRptTimebyResource
                SetParent(objrptCLResource.Handle.ToInt32, P1.Handle.ToInt32)
                objrptCLResource.lblReportTit.Text = UCase("Actual Cycle Time by Resource Group(with details in Work Center)")
                objrptCLResource.Left = -P1.Left
                objrptCLResource.Top = -P1.Top
                objrptCLResource.Width = P1.Width
                objrptCLResource.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objrptCLResource.Show()
                Pwait.Visible = False
            Case "Actual Cycle Time by Resource Group(Resource Group)"
                objrptCLResource = New frmRptTimebyResource
                SetParent(objrptCLResource.Handle.ToInt32, P1.Handle.ToInt32)
                objrptCLResource.lblReportTit.Text = UCase("Actual Cycle Time by Resource Group(Summary by Items)")
                objrptCLResource.Left = -P1.Left
                objrptCLResource.Top = -P1.Top
                objrptCLResource.Width = P1.Width
                objrptCLResource.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objrptCLResource.Show()
                Pwait.Visible = False
            Case "Actual Cycle Time by Operator"

                objrptCLoperator = New frmRptTimeByOperator
                SetParent(objrptCLoperator.Handle.ToInt32, P1.Handle.ToInt32)
                objrptCLoperator.lblReportTit.Text = UCase("Actual Cycle Time by Operator")
                objrptCLoperator.Left = -P1.Left
                objrptCLoperator.Top = -P1.Top
                objrptCLoperator.Width = P1.Width
                objrptCLoperator.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objrptCLoperator.Show()
                Pwait.Visible = False
            Case "Actual Cycle Time by Item(Operation)"
                objrptCLItem = New frmRptTimeByItem
                SetParent(objrptCLItem.Handle.ToInt32, P1.Handle.ToInt32)
                objrptCLItem.lblReportTit.Text = UCase("Actual Cycle Time by Item(with details in Operations)")
                objrptCLItem.Left = -P1.Left
                objrptCLItem.Top = -P1.Top
                objrptCLItem.Width = P1.Width
                objrptCLItem.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objrptCLItem.Show()
                Pwait.Visible = False

            Case "Actual Cycle Time by Item(Item)"
                objrptCLItem = New frmRptTimeByItem
                SetParent(objrptCLItem.Handle.ToInt32, P1.Handle.ToInt32)
                objrptCLItem.lblReportTit.Text = UCase("Actual Cycle Time by Item(Summary)")
                objrptCLItem.Left = -P1.Left
                objrptCLItem.Top = -P1.Top
                objrptCLItem.Width = P1.Width
                objrptCLItem.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objrptCLItem.Show()
                Pwait.Visible = False
            Case "Exception Report"
                objrptException = New frmRptException
                SetParent(objrptException.Handle.ToInt32, P1.Handle.ToInt32)

                objrptException.Left = -P1.Left
                objrptException.Top = -P1.Top
                objrptException.Width = P1.Width
                objrptException.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objrptException.Show()
                Pwait.Visible = False
            Case "Invetory Transfer"
                objInventoryTrans = New frmWIPList
                SetParent(objInventoryTrans.Handle.ToInt32, P1.Handle.ToInt32)

                objInventoryTrans.Left = -P1.Left
                objInventoryTrans.Top = -P1.Top
                objInventoryTrans.Width = P1.Width
                objInventoryTrans.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objInventoryTrans.Show()
                Pwait.Visible = False
            Case "JobStockCheck"
                objStockList = New frmStockList
                SetParent(objStockList.Handle.ToInt32, P1.Handle.ToInt32)
                objStockList.Left = -P1.Left
                objStockList.Top = -P1.Top
                objStockList.Width = P1.Width
                objStockList.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objStockList.Show()
                Pwait.Visible = False
            Case "Customer"
                objCustomer = New frmCustomer
                SetParent(objCustomer.Handle.ToInt32, P1.Handle.ToInt32)
                objCustomer.Left = -P1.Left
                objCustomer.Top = -P1.Top
                objCustomer.Width = P1.Width
                objCustomer.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objCustomer.Show()
                Pwait.Visible = False
            Case "Delivery Order"
                objDeliveryOrder = New frmfrmDeliveryOrder
                SetParent(objDeliveryOrder.Handle.ToInt32, P1.Handle.ToInt32)
                objDeliveryOrder.Left = -P1.Left
                objDeliveryOrder.Top = -P1.Top
                objDeliveryOrder.Width = P1.Width
                objDeliveryOrder.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objDeliveryOrder.Show()
                Pwait.Visible = False
            Case "Inventory Import"
                objInventoryImport = New frmInventoryImport
                SetParent(objInventoryImport.Handle.ToInt32, P1.Handle.ToInt32)
                objInventoryImport.Left = -P1.Left
                objInventoryImport.Top = -P1.Top
                objInventoryImport.Width = P1.Width
                objInventoryImport.Height = P1.Height
                Pwait.Visible = True
                Application.DoEvents()
                objInventoryImport.Show()
                Pwait.Visible = False
        End Select
    End Sub

    Private Sub MDINEW_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        TmVari = 0
        EXITToolStripMenuItem.ShowShortcutKeys = True
        Dim frm As New frmLogin
        frm.ShowDialog()

        If LogUserName = "Zanden" Then
            MnDataEdit.Visible = True
            mnudAll.Visible = False
            TestToolStripMenuItem.Visible = False
            mnudAll.Enabled = False
            TestToolStripMenuItem.Enabled = False
            DataProblemToolStripMenuItem.Visible = False
            ExportCompletedOperationsToolStripMenuItem.Visible = False
            'mnudAll.Visible = True
            TestToolStripMenuItem.Visible = True
            'mnudAll.Enabled = True
            TestToolStripMenuItem.Enabled = True
            DeleteDuplicateToolStripMenuItem.Visible = True
            'DataProblemToolStripMenuItem.Visible = True
            'ExportCompletedOperationsToolStripMenuItem.Visible = True
        Else

            MnDataEdit.Visible = False
            mnudAll.Visible = False
            TestToolStripMenuItem.Visible = False
            mnudAll.Enabled = False
            TestToolStripMenuItem.Enabled = False
            DataProblemToolStripMenuItem.Visible = False
            ExportCompletedOperationsToolStripMenuItem.Visible = False
            DeleteDuplicateToolStripMenuItem.Visible = False
        End If



        Try
            If UCase(LogPrivilege) = "REWORK" Then


                WIPDATAENTRYToolStripMenuItem.Enabled = False

                MMMaster.Visible = False
                MMGROUP.Visible = False
                DOWNLOADToolStripMenuItem.Visible = False
                WIPDATAENTRYToolStripMenuItem.Visible = False
                REPORTToolStripMenuItem.Visible = False

                'Master
                MMMaster.Enabled = False
                MMMaster.Enabled = False
                UMUser.Enabled = True
                UMOP.Enabled = False
                UMResourceGroup.Enabled = False
                UMACT.Enabled = False
                UMWRK.Enabled = False
                UMItem.Enabled = False
                UMMacheine.Enabled = False
                UMRejDESC.Enabled = False
                'Group
                MMGROUP.Enabled = False
                UMREJECT_Group.Enabled = False
                UM_Machine_Group.Enabled = False
                UCWRK_Group.Enabled = False
                'Download
                DOWNLOADToolStripMenuItem.Enabled = False
                DOWNLOADToolStripMenuItem.Enabled = False
                DownloadJToolStripMenuItem.Enabled = False
                ViewJobToolStripMenuItem.Enabled = False
                ExportExcelToolStripMenuItem.Enabled = False

                'WIP
                WIPDATAENTRYToolStripMenuItem.Visible = False
                WIPDATAENTRYToolStripMenuItem.Enabled = False
                JobVerificationToolStripMenuItem.Enabled = False
                ExportExcelToolStripMenuItem.Enabled = False
                'Report
                REPORTToolStripMenuItem.Enabled = False
                mnuCompletedTransaction.Enabled = False
                mnIItemReport.Enabled = False
                WIPFinalYieldToolStripMenuItem.Enabled = False
                WIPWorkCenterYieldToolStripMenuItem.Enabled = False
                WIPFailureAnalysisToolStripMenuItem.Enabled = False

                frmClose()
                selectForm = "DataEntry"
                frmOpen()

            ElseIf UCase(LogPrivilege) = "SUPERVISOR" Then



                UMUser.Enabled = True

                MMMaster.Visible = True
                MMGROUP.Visible = True



                ' ElseIf UCase(AccessRights) = "ADMIN" Then

                'Else


            End If
            ' lblLogin.Text = LogName
            ' lblDate.Text = Format(Now, "dd/MM/yyyy")
            'If UCase(LogPrivilege) = "WORKER" Then
            '    '  Panel7.Visible = True
            '    '  Panel7.Width = 244
            '    'ToolBar2.Buttons(0).Visible = False
            '    'ToolBar2.Buttons(1).Visible = False
            '    'ToolBar2.Buttons(2).Visible = False
            '    ''ToolBar2.Buttons(3).Visible = False
            '    '  ToolBar2.Buttons(4).Visible = False
            '    'frmClose()
            '    'selectForm = "OP"
            '    'frmOpen()
            'ElseIf UCase(LogPrivilege) <> "OP" Then
            '    ' Panel7.Visible = True
            'End If
            'If UCase(LogPrivilege) = "REWORK" Then
            '    MMMaster.Visible = False
            '    MMGROUP.Visible = False

            '    '   SETUPToolStripMenuItem123.Visible = False
            '    DOWNLOADToolStripMenuItem.Visible = False
            '    WIPDATAENTRYToolStripMenuItem.Visible = False
            '    REPORTToolStripMenuItem.Visible = False
            '    frmClose()
            '    selectForm = "DataEntry"
            '    frmOpen()
            'Else
            '    MMMaster.Visible = True
            '    MMGROUP.Visible = True
            '    ' SETUPToolStripMenuItem123.Visible = True
            '    DOWNLOADToolStripMenuItem.Visible = True
            '    WIPDATAENTRYToolStripMenuItem.Visible = True
            '    REPORTToolStripMenuItem.Visible = True


            'End If
        Catch ex As Exception

        End Try
    End Sub

  

    'Private Sub OperatorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OperatorToolStripMenuItem.Click
    '    Timer1.Enabled = False
    '    frmClose()
    '    selectForm = "Operator"
    '    frmOpen()
    'End Sub

    'Private Sub ResourceGroupToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResourceGroupToolStripMenuItem.Click
    '    Timer1.Enabled = False
    '    frmClose()
    '    selectForm = "RGrouping"
    '    frmOpen()
    'End Sub

    'Private Sub WorkCenterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WorkCenterToolStripMenuItem.Click
    '    Timer1.Enabled = False
    '    frmClose()
    '    selectForm = "WC"
    '    frmOpen()
    'End Sub

    'Private Sub ItemToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ItemToolStripMenuItem.Click
    '    Timer1.Enabled = False
    '    frmClose()
    '    selectForm = "Item"
    '    frmOpen()
    'End Sub

    'Private Sub MachineToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MachineToolStripMenuItem.Click
    '    Timer1.Enabled = False
    '    frmClose()
    '    selectForm = "Machine"
    '    frmOpen()
    'End Sub

    'Private Sub RejectedDescriptionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RejectedDescriptionToolStripMenuItem.Click
    '    Timer1.Enabled = False
    '    frmClose()
    '    selectForm = "Rejected"
    '    frmOpen()
    'End Sub

    'Private Sub ActionControlToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActionControlToolStripMenuItem.Click
    '    Timer1.Enabled = False
    '    frmClose()
    '    selectForm = "ActionControl"
    '    frmOpen()
    'End Sub

    Private Sub WorkCenterGroupingToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Timer1.Enabled = False
        frmClose()
        selectForm = "WStation"
        frmOpen()
    End Sub

    Private Sub RejectedGroupingToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmClose()
        selectForm = "RejectedGroup"
        frmOpen()
    End Sub

    Private Sub MachineGroupingToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Timer1.Enabled = False
        frmClose()
        selectForm = "MachineWRK"
        frmOpen()
    End Sub

    Private Sub DownloadJToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DownloadJToolStripMenuItem.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "Download"
        frmOpen()
    End Sub

    Private Sub ViewJobToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewJobToolStripMenuItem.Click
        frmClose()
        selectForm = "View"
        frmOpen()
    End Sub

    Private Sub DataEntryToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataEntryToolStripMenuItem.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "DataEntry"
        frmOpen()
    End Sub

    Private Sub JobVerificationToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles JobVerificationToolStripMenuItem.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "DataTran"
        frmOpen()
    End Sub

    Private Sub JobProcessToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles JobProcessToolStripMenuItem.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "DataVerify"
        frmOpen()
    End Sub

    Private Sub EXPORTToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Timer1.Enabled = False
        frmClose()
        selectForm = "Export"
        frmOpen()
    End Sub

    Private Sub EXITToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EXITToolStripMenuItem.Click
        frmClose()
        Timer1.Enabled = False
        If MessageBox.Show("Do you want to quit?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
            Application.Exit()
        End If
    End Sub

    Private Sub LOGOFFToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmClose()
        Timer1.Enabled = True
        LogUserID = ""
        LogUserName = ""
        LogName = ""
        LogPrivilege = ""
        ' lblLogin.Text = ""
        Dim OBJLogin As New frmLogin
        OBJLogin.ShowDialog()

    End Sub

    Private Sub ExportExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportExcelToolStripMenuItem.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "Export"
        frmOpen()
    End Sub

    Private Sub WIPReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Timer1.Enabled = False
        frmClose()
        selectForm = "WIPResourceGroupReport"
        frmOpen()
    End Sub

    Private Sub mnuCompletedTransaction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCompletedTransaction.Click
        frmClose()
        selectForm = "Completed Transaction"
        frmOpen()
        Timer1.Enabled = True
        TmVari = 1
        'Dim str123 As New frmRptCompletedTransaction
        'str123.ShowDialog()
    End Sub

    Private Sub mnIItemReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnIItemReport.Click
        frmClose()
        selectForm = "Item Report"
        frmOpen()
        Timer1.Enabled = True
        TmVari = 1
    End Sub

    Private Sub WIPFinalYieldToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WIPFinalYieldToolStripMenuItem.Click
        frmClose()
        selectForm = "Final Yield"
        frmOpen()
        Timer1.Enabled = True
        TmVari = 1
    End Sub

    Private Sub WIPWorkCenterYieldToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WIPWorkCenterYieldToolStripMenuItem.Click
        frmClose()
        selectForm = "WC Yield"
        frmOpen()
        Timer1.Enabled = True
        TmVari = 1
    End Sub

    Private Sub WIPFailureAnalysisToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WIPFailureAnalysisToolStripMenuItem.Click
        frmClose()
        selectForm = "Failure Analysis"
        frmOpen()
        Timer1.Enabled = True
        TmVari = 1
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If TmVari = 2 Then
            TmVari = 0
            Me.Focus()
        End If
    End Sub

   
    Private Sub UMUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UMUser.Click
        frmClose()
        selectForm = "User"
        frmOpen()
    End Sub

    Private Sub UMOP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UMOP.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "Operator"
        frmOpen()
    End Sub

    Private Sub UMResourceGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UMResourceGroup.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "RGrouping"
        frmOpen()
    End Sub

    Private Sub UMWRK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UMWRK.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "WC"
        frmOpen()
    End Sub

    Private Sub UMItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UMItem.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "Item"
        frmOpen()
    End Sub

    Private Sub UMMacheine_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UMMacheine.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "Machine"
        frmOpen()
    End Sub

    Private Sub UMRejDESC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UMRejDESC.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "Rejected"
        frmOpen()
    End Sub

    Private Sub UMACT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UMACT.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "ActionControl"
        frmOpen()
    End Sub

    Private Sub UCWRK_Group_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UCWRK_Group.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "WStation"
        frmOpen()
    End Sub

    Private Sub UM_Machine_Group_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UM_Machine_Group.Click


        Timer1.Enabled = False
        frmClose()
        selectForm = "MachineWRK"
        frmOpen()
    End Sub

    Private Sub UMREJECT_Group_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UMREJECT_Group.Click
        frmClose()
        selectForm = "RejectedGroup"
        frmOpen()
    End Sub

   
    Private Sub mnudAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnudAll.Click
        frmClose()
        selectForm = "DownloadALL"
        frmOpen()
    End Sub

   
    Private Sub TestToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TestToolStripMenuItem.Click
        Dim frmotext As New frmtest
        frmotext.ShowDialog()

    End Sub

    Private Sub ItemReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ItemReportToolStripMenuItem.Click
        frmClose()
        selectForm = "NewItemReport"
        frmOpen()
        Timer1.Enabled = True
        TmVari = 1
    End Sub

    

    Private Sub ResourceGroupToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResourceGroupToolStripMenuItem.Click
        frmClose()
        selectForm = "Actual Cycle Time by Resource Group(Work Center / Machines)"
        frmOpen()
        Timer1.Enabled = True
        TmVari = 1
    End Sub

    Private Sub SummaryToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SummaryToolStripMenuItem.Click
        frmClose()
        selectForm = "Actual Cycle Time by Resource Group(Resource Group)"
        frmOpen()
        Timer1.Enabled = True
        TmVari = 1
    End Sub

    Private Sub SummaryByOperatorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SummaryByOperatorToolStripMenuItem.Click
        frmClose()
        selectForm = "Actual Cycle Time by Operator"
        frmOpen()
        Timer1.Enabled = True
        TmVari = 1
    End Sub

    Private Sub ActualByItemToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActualByItemToolStripMenuItem.Click
        frmClose()
        selectForm = "Actual Cycle Time by Item(Operation)"
        frmOpen()
        Timer1.Enabled = True
        TmVari = 1
    End Sub

    Private Sub SummaryByItemToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SummaryByItemToolStripMenuItem.Click
        frmClose()
        selectForm = "Actual Cycle Time by Item(Item)"
        frmOpen()
        Timer1.Enabled = True
        TmVari = 1
    End Sub

    Private Sub DataProblemToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataProblemToolStripMenuItem.Click
        'Dim objd As New frmCheckDataMissing
        'objd.ShowDialog()
        Dim objd As New frmCloseJob
        objd.ShowDialog()
    End Sub

    Private Sub WIPByToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WIPByToolStripMenuItem.Click
        frmClose()
        selectForm = "NewItemReportResource"
        frmOpen()
        Timer1.Enabled = True
        TmVari = 1
    End Sub

    Private Sub ExportCompletedOperationsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportCompletedOperationsToolStripMenuItem.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "ExportIN"
        frmOpen()
    End Sub

    Private Sub JobCloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles JobCloseToolStripMenuItem.Click
        'Dim objd As New frmCheckDataMissing
        'objd.ShowDialog()
        Dim objd As New FrmAutoCloseJOb
        objd.ShowDialog()
    End Sub

    Private Sub JobDeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles JobDeleteToolStripMenuItem.Click
        Dim frm As New FrmDeleteJoborder
        frm.ShowDialog()

    End Sub

    Private Sub InsertOPToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InsertOPToolStripMenuItem.Click
        Dim frm As New frmInsertNewOP
        frm.ShowDialog()
    End Sub

    Private Sub TransferReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TransferReportToolStripMenuItem.Click
        frmClose()
        selectForm = "Exception Report"
        frmOpen()
        Timer1.Enabled = True
        TmVari = 1
    End Sub

    Private Sub DeleteDuplicateToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteDuplicateToolStripMenuItem.Click
        Dim frm As New frmCheckingJobProblem
        frm.ShowDialog()
    End Sub

    Private Sub toolInvenTrans_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles toolInvenTrans.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "Invetory Transfer"
        frmOpen()
    End Sub

    Private Sub StockCheckToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StockCheckToolStripMenuItem.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "JobStockCheck"
        frmOpen()
    End Sub

    Private Sub menuICustomer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuICustomer.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "Customer"
        frmOpen()
    End Sub

    Private Sub DeliveryOrderToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeliveryOrderToolStripMenuItem.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "Delivery Order"
        frmOpen()
    End Sub

    Private Sub ImportListToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImportListToolStripMenuItem.Click
        Timer1.Enabled = False
        frmClose()
        selectForm = "Inventory Import"
        frmOpen()
    End Sub
End Class