Imports System.Data.SqlClient
Public Class frmDataEntryOP
    Dim dr As SqlDataReader
    Dim parm As SqlParameter
    Dim Countdown As Integer
    Dim RelQtyAll As Double
    'Private Sub frmDataEntry_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '    'lblMsgBox.Text = ""
    '    'cmbsuf.SelectedIndex = 0
    '    'With lstv
    '    '    .Columns.Add("IDNo", 0, HorizontalAlignment.Left)
    '    '    .Columns.Add("Job ID", 230, HorizontalAlignment.Left)
    '    '    .Columns.Add("Suffix", 100, HorizontalAlignment.Left)
    '    '    .Columns.Add("Work Station ID", 230, HorizontalAlignment.Left)
    '    '    .Columns.Add("Operation No.", 230, HorizontalAlignment.Left)
    '    '    .Columns.Add("Item No.", 100, HorizontalAlignment.Left)
    '    '    .Columns.Add("Date Started.", 100, HorizontalAlignment.Left)
    '    '    .Columns.Add("Date End", 100, HorizontalAlignment.Left)
    '    '    .Columns.Add("Time Started", 100, HorizontalAlignment.Left)
    '    '    .Columns.Add("Time Ented", 100, HorizontalAlignment.Left)
    '    '    .Columns.Add("Complited Qty", 100, HorizontalAlignment.Left)
    '    '    .Columns.Add("Rejected Qty", 100, HorizontalAlignment.Left)
    '    '    lbltotCom.Text = 0.0

    '    '    lbltotRej.Text = 0.0
    '    '    list_Displaydata()
    '    '    'list_Displaydata()
    '    'End With
    '    Me.Select()
    '    txtC.Select()

    'End Sub
    Sub list_Displaydata()
        lstv.Items.Clear()
        Dim suf As Integer = 0
        Dim TotC, TotR, TotOP As Double
        TotC = 0.0
        TotR = 0.0
        TotOP = 0.0
        Dim cf As Boolean = False
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                If txtSufParent.Text = "" Or txtSufParent.Text = "-" Then
                    .CommandText = "select * from tbJobtrans where _job='" & fncstr(txtJobNo.Text) & "' and _oper_num=" & Val(txtOperationNo.Text) & " order by _trans_dateto, _tansnum"
                Else
                    .CommandText = "select * from tbJobtrans where _job='" & fncstr(txtJobNo.Text) & "' and _oper_num=" & Val(txtOperationNo.Text) & " and _jobsuffixParent='" & Trim(txtSufParent.Text) & "' order by _trans_dateto, _tansnum"
                End If
                cn.Open()
                dr = .ExecuteReader()
                txtStartDate.Text = Format(Now, "dd/MM/yyyy")
                txtStartTime.Text = Format(Now, "HHmm")
                txtTansID.Text = 0
                cf = False

                '---------------
                txtOperators.Text = ""
                txtOPQTY.Text = ""
                txtOPQTY1.Text = ""
                ' cmbsuf.Text = "-"
                txtTansID.Text = 0
                '---------------



                While dr.Read
                    If cf = False Then
                        If txtEmpid.Text = dr.Item("_emp_num") And txtMachineid.Text = "-" Then
                            If dr.Item("_trans_dateto") = 0 Then
                                txtStartDate.Text = Format(DateTime.FromOADate(dr.Item("_trans_datefrm")), "dd/MM/yyyy")
                                txtStartTime.Text = dr.Item("_start_time")
                                ' txtEndDate.Text = Format(Now, "dd/MM/yyyy")
                                '   txtEndTime.Text = Format(Now, "HHmm")
                                txtOperators.Text = dr.Item("_no_oper")
                                txtOPQTY.Text = dr.Item("_qty_op_qty")
                                txtOPQTY1.Text = dr.Item("_qty_op_qty")
                                'cmbsuf.Text = dr.Item("_jobsuffix")
                                txtTansID.Text = dr.Item("_tansnum")
                                cf = True
                                TotOP = TotOP - dr.Item("_qty_op_qty")
                            End If
                        ElseIf txtEmpid.Text = dr.Item("_emp_num") And txtMachineid.Text = dr.Item("_machineid") Then
                            If dr.Item("_trans_dateto") = 0 Then
                                txtStartDate.Text = Format(DateTime.FromOADate(dr.Item("_trans_datefrm")), "dd/MM/yyyy")
                                txtStartTime.Text = dr.Item("_start_time")
                                txtOperators.Text = dr.Item("_no_oper")
                                txtOPQTY.Text = dr.Item("_qty_op_qty")
                                txtOPQTY1.Text = dr.Item("_qty_op_qty")
                                txtTansID.Text = dr.Item("_tansnum")
                                cf = True
                                TotOP = TotOP - dr.Item("_qty_op_qty")
                            End If
                        Else
                            ' txtStartDate.Text = Format(DateTime.FromOADate(dr.Item("_trans_datefrm")), "dd/MM/yyyy")
                            '  txtStartTime.Text = dr.Item("_start_time")
                            ' txtEndDate.Text = Format(Now, "dd/MM/yyyy")
                            'txtEndTime.Text = Format(Now, "HHmm")
                            txtOperators.Text = ""
                            txtOPQTY.Text = ""
                            txtOPQTY1.Text = ""
                            ' cmbsuf.Text = "-"
                            txtTansID.Text = 0
                        End If
                    End If
                    Dim ls As New ListViewItem(Trim(dr.Item("_tansnum")))    ' you can also use reader.GetSqlValue(0) 
                    ls.SubItems.Add(Trim(dr.Item("_job")))
                    ls.SubItems.Add(Trim(dr.Item("_emp_num")))
                    ls.SubItems.Add(Trim(dr.Item("_jobsuffixParent")))
                    ls.SubItems.Add(Trim(dr.Item("_jobsuffix")))
                    ls.SubItems.Add(Trim(dr.Item("_wc")))
                    ls.SubItems.Add(Trim(dr.Item("_oper_num")))
                    ls.SubItems.Add(Trim(dr.Item("_item")))
                    ls.SubItems.Add(Format(DateTime.FromOADate(dr.Item("_trans_datefrm")), "dd/MM/yyyy"))
                    If dr.Item("_trans_dateto") = 0 Then
                        ls.SubItems.Add("-")
                    Else
                        ls.SubItems.Add(Format(DateTime.FromOADate(dr.Item("_trans_dateto")), "dd/MM/yyyy"))
                    End If

                    ls.SubItems.Add(dr.Item("_start_time"))
                    ls.SubItems.Add(dr.Item("_end_time"))
                    ls.SubItems.Add(dr.Item("_qty_op_qty"))
                    ls.SubItems.Add(dr.Item("_qty_complete"))
                    ls.SubItems.Add(dr.Item("_qty_scrapped"))
                    If dr.Item("_trans_dateto") = 0 Then
                        lstv.Items.Add(ls).ForeColor = Color.Red
                    Else
                        lstv.Items.Add(ls)
                    End If
                    suf = suf + 1
                    TotC = TotC + dr.Item("_qty_complete")
                    TotR = TotR + dr.Item("_qty_scrapped")
                    TotOP = TotOP + dr.Item("_qty_op_qty")
                End While
            End With
        Catch ex As Exception
            lblMsgBox.Text = (ex.Message)
            txtC.Select()
        Finally
            cn.Close()

        End Try
        lbltotCom.Text = TotC
        lbltotRej.Text = TotR
        lblTotOPQTY.Text = TotOP
        'cmbsuf.Enabled = False
        If cf = False Then
            'cmbsuf.SelectedIndex = suf
        End If
    End Sub
    Private Function txtWKID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
        txtWKID_KeyPress = False
        lblMsgBox.Text = ""
        ' lblCom.Visible = False
        If Asc(e.KeyChar) = 13 Then
            ' ClearAll()

            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "select * from tbJob where _job='" & fncstr(txtJobNo.Text) & "'"
                    cn.Open()
                    dr = .ExecuteReader()
                    If dr.Read Then
                        RelQtyAll = dr.Item("_qtyReleased")
                        txtReleasedqty.Text = dr.Item("_qtyReleased")
                        txtItemNoTemp.Text = dr.Item("_item")
                        txtJobNo.Text = dr.Item("_job")
                        txtItemNo.Text = txtItemNoTemp.Text
                        txtWKID_KeyPress = True
                        If Val(dr.Item("_qtyReleased")) <= (Val(dr.Item("_qtyCompleted")) + Val(dr.Item("_qtyScrapped"))) Then

                        End If

                    Else
                        txtWKID_KeyPress = False
                        lblMsgBox.Text = "Please check your JOB NO.!(" & txtJobNo.Text & ")"
                        wstClear()
                    End If
                End With
            Catch ex As Exception
                lblMsgBox.Text = ex.Message
            Finally
                cn.Close()
            End Try
        End If

    End Function

    Private Sub txtWKSatationID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtWKSatationID.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Trim(txtJobNo.Text) <> "" Then
                ' wstClear()
                Try
                    With com
                        .Connection = cn
                        .CommandType = CommandType.Text
                        .CommandText = "select A.*,B._description as _desc from tbJobRoute A,tbWC B where A._wc=B._WC and  A._job='" & fncstr(txtJobNo.Text) & "' and _operationNo=" & Val(fncstr(txtOperationNo.Text))
                        cn.Open()
                        dr = .ExecuteReader()
                        '  txtJobNo.Text = ""
                        txtWKSatationID.Text = ""
                        'txtItemNo.Text = ""
                        If dr.Read Then
                            txtJobNo.Text = dr.Item("_job")
                            txtWKSatationID.Text = dr.Item("_wc")
                            txtWKSatation.Text = dr.Item("_desc")
                            txtItemNo.Text = txtItemNoTemp.Text
                            'txtOperators.Select()
                            cn.Close()
                            list_Displaydata()
                        Else
                            If Trim(txtJobNo.Text) <> "" And Trim(txtOperationNo.Text) <> "" Then
                                lblMsgBox.Text = "Please check received No.!"
                            End If
                        End If
                    End With

                    cn.Close()
                    ' Dim strsql As String
                    'Dim booOP As Boolean = False

                    'strsql = "select min(_operationNo) as _operationNo  from tbJobRoute where _job='" & fncstr(txtWKID.Text) & "'"
                    'Dim ds As New DataSet
                    'Dim da As New SqlDataAdapter
                    'da = New SqlDataAdapter(strsql, cn)
                    'da.Fill(ds)
                    'da.Dispose()
                    'If ds.Tables(0).Rows(0).IsNull(0) = False Then
                    '    If ds.Tables(0).Rows.Count > 0 Then
                    '        If ds.Tables(0).Rows(0).Item("_operationNo") = Val(txtOperationNo.Text) Then
                    '            booOP = True
                    '        Else
                    '            txtReleasedqty.Text = ""
                    '            getReasedQty()


                    '        End If

                    '    Else
                    '        lblMsgBox.Text = "Please scan SCAN (Job No.)!"
                    '    End If '
                    'Else
                    '    If Trim(txtWKID.Text) <> "" Then
                    '        lblMsgBox.Text = "Please check Job No.!"
                    '    End If
                    '    End If






                Catch ex As Exception
                    lblMsgBox.Text = (ex.Message)
                Finally
                    cn.Close()
                End Try

            Else
                'lblMsgBox.Text = "Please scan SCAN (JOB NO.)!"
            End If

        End If
    End Sub
    Sub getReasedQty()
        Dim strsql As String
        Dim booOP As Boolean = False

        strsql = "select sum(_qty_complete) as _qty_complete from tbJobtrans where _job='" & fncstr(txtJobNo.Text) & "' and _nextoper_num=" & Val(fncstr(txtOperationNo.Text))
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        da = New SqlDataAdapter(strsql, cn)
        da.Fill(ds)
        da.Dispose()
        If ds.Tables(0).Rows(0).IsNull(0) = False Then
            txtReleasedqty.Text = ds.Tables(0).Rows(0).Item("_qty_complete")
        Else
            txtReleasedqty.Text = ""
        End If
    End Sub
    Sub ClearAll()
        RelQtyAll = 0
        txtJobNo.Text = ""
        txtEmpid.Text = ""
        txtEmpName.Text = ""
        txtWKSatation.Clear()
        txtWKSatationID.Clear()
        txtOperationNo.Clear()
        txtJobNo.Clear()
        txtItemNo.Clear()
        'cmbsuf.SelectedIndex = 0
        txtItemNoTemp.Clear()
        txtReleasedqty.Clear()
        txtRejectedQty.Clear()
        txtStartDate.Clear()
        txtEndDate.Clear()
        txtStartTime.Clear()
        txtEndTime.Clear()
        txtOperators.Clear()
        txtCompletedQty.Clear()
        txtRejCode1.Clear()
        txtRejCode2.Clear()
        txtRejCode3.Clear()
        txtRejDes1.Clear()
        txtRejDes2.Clear()
        txtRejDes3.Clear()
        txtRejqty1.Clear()
        txtRejqty2.Clear()
        txtRejqty2.Clear()
        lstv.Items.Clear()
        lbltotCom.Text = 0.0
        lbltotRej.Text = 0.0
        lstv.Items.Clear()
        txtStartDate.Text = ""
        txtStartTime.Text = ""
        txtEndDate.Text = ""
        txtEndTime.Text = ""
        txtOPQTY.Text = ""
        txtOPQTY1.Text = ""
        txtTansID.Text = 0
        ' plHand.Visible = False
        txtHandoverid.Text = ""
        txtHandoverName.Text = ""
        txtMachineid.Text = ""
        txtSubval.Text = ""
        txtSufParent.Text = "-"
        txtsuf.Text = "-"
        'txtNextOPNo.Text = ""
        txtC.Focus()
    End Sub

    Sub wstClear()
        txtCompletedQty.Text = ""
        txtReleasedqty.Text = 0
        txtOPQTY.Text = ""
        txtOPQTY1.Text = ""
        txtOperationNo.Clear()
        ' txtJobNo.Clear()
        txtsuf.Text = "-"
        txtSufParent.Text = "-"
        txtItemNo.Clear()
        'cmbsuf.SelectedIndex = 0
        txtRejectedQty.Clear()
        txtStartDate.Clear()
        txtEndDate.Clear()
        txtStartTime.Clear()
        txtEndTime.Clear()
        txtOperators.Clear()
        txtCompletedQty.Clear()
        txtRejCode1.Clear()
        txtRejCode2.Clear()
        txtRejCode3.Clear()
        txtRejDes1.Clear()
        txtRejDes2.Clear()
        txtRejDes3.Clear()
        txtRejqty1.Clear()
        txtRejqty2.Clear()
        txtRejqty2.Clear()
        lstv.Items.Clear()
        lbltotCom.Text = 0.0
        lbltotRej.Text = 0.0
    End Sub

    Private Sub txtOperators_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtOperators.KeyPress
        If (Asc(e.KeyChar) > 46 And Asc(e.KeyChar) < 58) Or Asc(e.KeyChar) = 8 Or Asc(e.KeyChar) = 13 Then
            e.Handled = False
            If Asc(e.KeyChar) = 13 Then
                txtC.Focus()
            End If
        Else
            e.Handled = True
        End If
       
    End Sub

    Private Sub txtStartTime_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtStartTime.KeyPress
        If (Asc(e.KeyChar) > 47 And Asc(e.KeyChar) < 58) Or Asc(e.KeyChar) = 8 Or Asc(e.KeyChar) = 13 Then
            e.Handled = False
            If Asc(e.KeyChar) = 13 Then
                txtEndDate.Select()
            End If
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtStartDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtStartDate.KeyPress
        If (Asc(e.KeyChar) > 46 And Asc(e.KeyChar) < 58) Or Asc(e.KeyChar) = 8 Or Asc(e.KeyChar) = 13 Then
            e.Handled = False
            If Asc(e.KeyChar) = 13 Then
                txtStartTime.Select()
            End If
        Else
            e.Handled = True
        End If
    End Sub




  

    Private Sub txtEndDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEndDate.KeyPress
        If (Asc(e.KeyChar) > 46 And Asc(e.KeyChar) < 58) Or Asc(e.KeyChar) = 8 Or Asc(e.KeyChar) = 13 Then
            e.Handled = False
            If Asc(e.KeyChar) = 13 Then
                txtEndTime.Select()
            End If
        Else
            e.Handled = True
        End If
    End Sub



    Private Sub txtEndTime_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEndTime.KeyPress
        If (Asc(e.KeyChar) > 47 And Asc(e.KeyChar) < 58) Or Asc(e.KeyChar) = 8 Or Asc(e.KeyChar) = 13 Then
            e.Handled = False
            If Asc(e.KeyChar) = 13 Then
                txtCompletedQty.Select()
            End If
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtCompletedQty_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCompletedQty.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtEndDate.Text = ""
            txtEndTime.Text = ""
            txtCompletedQty.BackColor = Color.White
            If Trim(txtCompletedQty.Text) <> "" Then
                txtEndDate.Text = Format(Now, "dd/MM/yyyy")
                txtEndTime.Text = Format(Now, "HHmm")
            Else
                txtEndDate.Text = ""
                txtEndTime.Text = ""
            End If

            txtC.Select()

        End If
    End Sub

    Private Sub txtRejectedQty_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRejectedQty.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtRejectedQty.BackColor = Color.White
            txtC.Select()
        End If
    End Sub
    Private Sub txtRejCode1_KeyPress()

        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select * from tbRejected where _RejectedCode='" & fncstr(txtRejCode1.Text) & "' and _RejID in(select _RejID from tbRejectedGroup where _sIDno in(select _sIDno from tbWorkStation Where _wc='" & fncstr(txtWKSatationID.Text) & "')) "
                '.CommandText = "select * from tbFailure where _FailureCode='" & fncstr(txtRejCode1.Text) & "' and _sIDno in(select _sIDno from tbWorkStation where _wc='" & txtWKSatationID.Text & "')"
                cn.Open()
                dr = .ExecuteReader()
                If dr.Read Then
                    txtRejDes1.Text = dr.Item("_RejectedDesc")
                    txtRejqty1.BackColor = Color.Yellow
                    txtRejqty1.Focus()
                Else
                    lblMsgBox.Text = "Please check rejected cod (" & txtRejCode1.Text & " )!"
                    txtRejqty1.BackColor = Color.White
                    txtRejCode1.Clear()
                    txtRejDes1.Clear()
                    txtRejqty1.Clear()
                    txtC.Focus()
                End If
            End With

        Catch ex As Exception
            lblMsgBox.Text = (ex.Message)
            txtC.Focus()
        Finally
            cn.Close()

        End Try

    End Sub
    Private Sub txtRejqty1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRejqty1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtRejqty1.BackColor = Color.White
            If Trim(txtRejqty1.Text) = "" Then
                txtRejCode1.Text = ""
                txtRejDes1.Text = ""
            End If
            txtC.Focus()
        End If

    End Sub
    Private Sub txtRejCode2_KeyPress()
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select * from tbRejected where _RejectedCode='" & fncstr(txtRejCode2.Text) & "' and _RejID in(select _RejID from tbRejectedGroup where _sIDno in(select _sIDno from tbWorkStation Where _wc='" & fncstr(txtWKSatationID.Text) & "')) "
                '.CommandText = "select * from tbFailure where _FailureCode='" & fncstr(txtRejCode2.Text) & "' and _sIDno in(select _sIDno from tbWorkStation where _wc='" & txtWKSatationID.Text & "')"
                cn.Open()
                dr = .ExecuteReader()
                If dr.Read Then
                    txtRejDes2.Text = dr.Item("_RejectedDesc")
                    txtRejqty2.BackColor = Color.Yellow
                    txtRejqty2.Focus()
                Else
                    lblMsgBox.Text = "Please check rejected cod (" & txtRejCode2.Text & " )!"
                    txtRejCode2.Clear()
                    txtRejDes2.Clear()
                    txtRejqty2.Clear()
                    txtC.Focus()
                End If
            End With

        Catch ex As Exception
            lblMsgBox.Text = (ex.Message)
            txtC.Focus()
        Finally
            cn.Close()

        End Try
    End Sub
    Private Sub txtRejqty2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRejqty2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtRejqty2.BackColor = Color.White

            If Trim(txtRejqty2.Text) = "" Then
                txtRejCode2.Text = ""
                txtRejDes2.Text = ""
            End If
            txtC.Focus()
        End If
    End Sub

    Private Sub txtRejCode3_KeyPress()

        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select * from tbRejected where _RejectedCode='" & fncstr(txtRejCode3.Text) & "' and _RejID in(select _RejID from tbRejectedGroup where _sIDno in(select _sIDno from tbWorkStation Where _wc='" & fncstr(txtWKSatationID.Text) & "')) "
                '.CommandText = "select * from tbFailure where _FailureCode='" & fncstr(txtRejCode3.Text) & "' and _sIDno in(select _sIDno from tbWorkStation where _wc='" & txtWKSatationID.Text & "')"
                cn.Open()
                dr = .ExecuteReader()
                If dr.Read Then
                    txtRejDes3.Text = dr.Item("_RejectedDesc")
                    txtRejqty3.BackColor = Color.Yellow
                    txtRejqty3.Focus()

                Else
                    lblMsgBox.Text = "Please check rejected cod (" & txtRejCode3.Text & " )!"
                    txtRejCode3.Clear()
                    txtRejDes3.Clear()
                    txtRejqty3.Clear()
                    txtC.Focus()
                End If
            End With

        Catch ex As Exception
            lblMsgBox.Text = (ex.Message)
            txtC.Focus()
        Finally
            cn.Close()
            'txtC.Focus()
        End Try

    End Sub

    Private Sub butSave_Click()
        'If Trim(txtCompletedQty.Text) = "" Then
        '    txtEndDate.Text = ""
        '    txtEndTime.Text = ""
        'End If
        'If Trim(txtJobNo.Text) = "" Then
        '    lblMsgBox.Text = "Please scan JOB NO.!"
        '    Exit Sub
        'End If
        'If Trim(txtWKSatationID.Text) = "" Then
        '    lblMsgBox.Text = "Please scan workstation id!"
        '    Exit Sub
        'End If
        'If Trim(txtStartDate.Text) = "" Then
        '    lblMsgBox.Text = "Please enter start date!"
        '    Exit Sub
        'End If
        'If Trim(txtStartTime.Text) = "" Then
        '    lblMsgBox.Text = "Please enter start time!"
        '    Exit Sub
        'End If
        'If Trim(txtMachineid.Text) = "" Then
        '    lblMsgBox.Text = "Please enter machine ID!"
        '    Exit Sub
        'End If
        'If Trim(txtEmpid.Text) = "" Then
        '    lblMsgBox.Text = "Please enter Operator ID!"
        '    Exit Sub
        'End If
        'If txtCompletedQty.Text <> "" And Val(txtCompletedQty.Text) <> 0 Then
        '    If Trim(txtEndDate.Text) = "" Then
        '        lblMsgBox.Text = "Please enter start date!"
        '        Exit Sub
        '    End If
        'End If
        'If Trim(txtEndDate.Text) <> "" Then
        '    If Val(txtCompletedQty.Text) > Val(txtReleasedqty.Text) Then
        '        lblMsgBox.Text = "Please check completed quantity!"
        '        Exit Sub
        '    End If
        '    If Val(txtReleasedqty.Text) < Val(txtRejectedQty.Text) Then
        '        lblMsgBox.Text = "Please check rejected quantity!"
        '        Exit Sub
        '    End If
        '    'If Trim(txtOPQTY.Text) > Val(txtCompletedQty.Text) + Val(txtRejectedQty.Text) Then
        '    '    txtOPQTY.Text = Val(txtCompletedQty.Text) + Val(txtRejectedQty.Text)
        '    'End If
        '    If Val(txtReleasedqty.Text) < (Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text)) Then
        '        lblMsgBox.Text = "Please check completed and rejected  quantity!"
        '        Exit Sub
        '    End If

        '    Dim Totq As Double

        '    Totq = Val(lbltotCom.Text) + Val(lbltotRej.Text) + Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text)

        '    If Val(txtReleasedqty.Text) < Totq Then
        '        lblMsgBox.Text = "Please check completed and rejected  quantity!"
        '        Exit Sub
        '    End If
        '    If Val(txtRejectedQty.Text) > 0 Then
        '        If Trim(txtRejDes1.Text) = "" Then
        '            lblMsgBox.Text = "Please scan rejected code!"
        '            Exit Sub
        '        End If
        '    End If

        '    'If MaxOPNO() = False Then
        '    '    If txtNextOPNo.Text = "" Or Val(txtNextOPNo.Text) = 0 Then
        '    '        lblMsgBox.Text = "Please enter next operation no.!"
        '    '        Exit Sub

        '    '    End If


        '    '    If Val(txtNextOPNo.Text) <= Val(txtOperationNo.Text) Then
        '    '        lblMsgBox.Text = "Please check next operation no.!"
        '    '        Exit Sub
        '    '    End If

        '    'End If

        'End If

        'If Trim(txtOPQTY.Text) = "" Or Val(txtOPQTY.Text) = 0 Then
        '    lblMsgBox.Text = "Please enter received quantity!"
        '    Exit Sub
        'End If



        'Dim s() As String
        'Dim s1() As String
        'Dim sd, ED As Date
        'Dim sddbl, EDdbl As Double
        'Try
        '    s = Split(txtStartDate.Text, "/")
        '    sd = DateSerial(s(2), s(1), s(0))
        '    sddbl = sd.ToOADate
        '    If Val(txtEndDate.Text) = 0 Or Trim(txtEndDate.Text) = "" Then
        '        EDdbl = 0

        '    Else
        '        s1 = Split(txtEndDate.Text, "/")
        '        ED = DateSerial(s1(2), s1(1), s1(0))
        '        EDdbl = ED.ToOADate
        '    End If

        'Catch ex As Exception
        '    lblMsgBox.Text = "Please check date!"
        '    Exit Sub
        'End Try

        'Try
        '    Dim n As Integer = Val(Mid(txtStartTime.Text, 1, 2))
        '    If n > 23 Then
        '        lblMsgBox.Text = "Please check time!"
        '        Exit Sub
        '    End If
        '    n = Val(Mid(txtStartTime.Text, 3, 4))
        '    If n > 59 Then
        '        lblMsgBox.Text = "Please check time!"
        '        Exit Sub
        '    End If

        '    n = Val(Mid(txtEndTime.Text, 1, 2))
        '    If n > 23 Then
        '        lblMsgBox.Text = "Please check time!"
        '        Exit Sub
        '    End If
        '    n = Val(Mid(txtEndTime.Text, 3, 4))
        '    If n > 59 Then
        '        lblMsgBox.Text = "Please check time!"
        '        Exit Sub
        '    End If
        'Catch ex As Exception
        'End Try
        'If Val(txtTansID.Text) = 0 Then
        '    If (Val(lblTotOPQTY.Text) + Val(txtOPQTY.Text)) > Val(txtReleasedqty.Text) Then
        '        lblMsgBox.Text = "Please check received quantity!"
        '        Exit Sub
        '    End If
        'Else
        '    If ((Val(lblTotOPQTY.Text) - Val(txtOPQTY.Text)) + Val(txtOPQTY.Text)) > Val(txtReleasedqty.Text) Then
        '        lblMsgBox.Text = "Please check received quantity!"
        '        Exit Sub
        '    End If
        'End If

        'Try


        '    With com


        '        .Connection = cn
        '        .CommandType = CommandType.StoredProcedure
        '        .CommandText = "sp_Job_Add"
        '        parm = .Parameters.Add("@job", SqlDbType.VarChar, 50)
        '        parm.Value = fncstr(txtJobNo.Text)
        '        parm = .Parameters.Add("@jobsuffix", SqlDbType.VarChar, 50)
        '        'If cmbsuf.SelectedIndex = 0 Then
        '        '    parm.Value = "0"
        '        'Else
        '        '    parm.Value = cmbsuf.SelectedIndex
        '        'End If
        '        Dim Id As DataItem = cmbsuf.SelectedItem
        '        parm.Value = Id.Value
        '        parm = .Parameters.Add("@wc", SqlDbType.VarChar, 50)
        '        parm.Value = fncstr(txtWKSatationID.Text)
        '        parm = .Parameters.Add("@oper_num", SqlDbType.Int)
        '        parm.Value = Val(fncstr(txtOperationNo.Text))
        '        parm = .Parameters.Add("@item", SqlDbType.VarChar, 256)
        '        parm.Value = fncstr(txtItemNo.Text)
        '        parm = .Parameters.Add("@JobDate", SqlDbType.Float)
        '        parm.Value = 0
        '        parm = .Parameters.Add("@trans_type", SqlDbType.Char, 10)
        '        parm.Value = ""
        '        parm = .Parameters.Add("@trans_datefrm", SqlDbType.Float)
        '        parm.Value = sddbl
        '        parm = .Parameters.Add("@trans_dateto", SqlDbType.Float)
        '        parm.Value = EDdbl
        '        parm = .Parameters.Add("@trans_Timefrm", SqlDbType.Int)
        '        parm.Value = Val(txtStartTime.Text)
        '        parm = .Parameters.Add("@trans_TimeTo", SqlDbType.Int)
        '        parm.Value = Val(txtEndTime.Text)
        '        parm = .Parameters.Add("@qty_op_qty", SqlDbType.Decimal)
        '        parm.Value = Val(txtOPQTY.Text)
        '        parm = .Parameters.Add("@qty_complete", SqlDbType.Decimal)
        '        parm.Value = Val(txtCompletedQty.Text)
        '        parm = .Parameters.Add("@qty_scrapped", SqlDbType.Decimal)
        '        parm.Value = Val(txtRejectedQty.Text)
        '        parm = .Parameters.Add("@no_oper", SqlDbType.Int)
        '        parm.Value = Val(txtOperationNo.Text)
        '        parm = .Parameters.Add("@CreatedBy", SqlDbType.VarChar, 255)
        '        parm.Value = txtEmpName.Text

        '        parm = .Parameters.Add("@rejectedCode1", SqlDbType.VarChar, 50)
        '        parm.Value = fncstr(txtRejCode1.Text)
        '        parm = .Parameters.Add("@rejectedDesc1", SqlDbType.VarChar, 256)
        '        parm.Value = fncstr(txtRejDes1.Text)
        '        parm = .Parameters.Add("@rejectedqty1", SqlDbType.Decimal)
        '        parm.Value = Val(fncstr(txtRejqty1.Text))

        '        parm = .Parameters.Add("@rejectedCode2", SqlDbType.VarChar, 50)
        '        parm.Value = fncstr(txtRejCode2.Text)
        '        parm = .Parameters.Add("@rejectedDesc2", SqlDbType.VarChar, 256)
        '        parm.Value = fncstr(txtRejDes2.Text)
        '        parm = .Parameters.Add("@rejectedqty2", SqlDbType.Decimal)
        '        parm.Value = Val(fncstr(txtRejqty2.Text))

        '        parm = .Parameters.Add("@rejectedCode3", SqlDbType.VarChar, 50)
        '        parm.Value = fncstr(txtRejCode3.Text)
        '        parm = .Parameters.Add("@rejectedDesc3", SqlDbType.VarChar, 256)
        '        parm.Value = fncstr(txtRejDes3.Text)
        '        parm = .Parameters.Add("@rejectedqty3", SqlDbType.Decimal)
        '        parm.Value = Val(fncstr(txtRejqty3.Text))
        '        parm = .Parameters.Add("@emp_num", SqlDbType.VarChar, 50)
        '        parm.Value = fncstr(txtEmpid.Text)
        '        parm = .Parameters.Add("@emp_name", SqlDbType.VarChar, 256)
        '        parm.Value = fncstr(txtEmpName.Text)
        '        parm = .Parameters.Add("@tansnum", SqlDbType.Int)
        '        parm.Value = Val(txtTansID.Text)
        '        parm = .Parameters.Add("@nextopno", SqlDbType.Int)
        '        parm.Value = Val(0)
        '        parm = .Parameters.Add("@machineid", SqlDbType.VarChar, 50)
        '        parm.Value = fncstr(txtMachineid.Text)
        '        parm = .Parameters.Add("@Handoverid", SqlDbType.VarChar, 50)
        '        parm.Value = fncstr(txtHandoverid.Text)
        '        parm = .Parameters.Add("@HandoverName", SqlDbType.VarChar, 255)
        '        parm.Value = fncstr(txtHandoverName.Text)




        '        cn.Open()
        '        .ExecuteNonQuery()
        '        cn.Close()
        '        com.Parameters.Clear()
        '        txtJobNo.Clear()
        '        ClearAll()
        '        list_Displaydata()
        '        '   txtWKID.Select()
        '        lblMsgBox.Text = "Successfully updated!"
        '    End With

        'Catch ex As Exception
        '    lblMsgBox.Text = (ex.Message)
        'Finally
        '    cn.Close()
        '    com.Parameters.Clear()
        'End Try

        If Trim(txtJobNo.Text) = "" Then
            lblMsgBox.Text = "Please scan JOB NO.!"
            Exit Sub
        End If
        If Trim(txtWKSatationID.Text) = "" Then
            lblMsgBox.Text = "Please scan workstation id!"
            Exit Sub
        End If
        If Trim(txtStartDate.Text) = "" Then
            lblMsgBox.Text = "Please enter start date!"
            Exit Sub
        End If
        If Trim(txtStartTime.Text) = "" Then
            lblMsgBox.Text = "Please enter start time!"
            Exit Sub
        End If
        If Trim(txtEmpid.Text) = "" Then
            lblMsgBox.Text = "Please enter Operator ID!"
            Exit Sub
        End If
        If txtCompletedQty.Text <> "" And Val(txtCompletedQty.Text) <> 0 Then
            If Trim(txtEndDate.Text) = "" Then
                lblMsgBox.Text = "Please enter start date!"
                Exit Sub
            End If
        End If
        If Trim(txtEndDate.Text) <> "" Then
            'If Val(txtCompletedQty.Text) > Val(txtReleasedqty.Text) Then
            '    lblMsgBox.Text = "Please check completed quantity!"
            '    Exit Sub
            'End If

            'If Trim(txtOPQTY.Text) < Val(txtCompletedQty.Text) + Val(txtRejectedQty.Text) Then
            '    lblMsgBox.Text = "Please check completed quantity and rejected  quantity!"
            '    Exit Sub
            'End If

            'If Val(txtReleasedqty.Text) < Val(txtRejectedQty.Text) Then
            '    lblMsgBox.Text = "Please check rejected quantity!"
            '    Exit Sub
            'End If
            'If Val(txtReleasedqty.Text) < (Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text)) Then
            '    lblMsgBox.Text = "Please check completed and rejected  quantity!"
            '    Exit Sub
            'End If

            'Dim Totq As Double
            'Totq = Val(lbltotCom.Text) + Val(lbltotRej.Text) + Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text)

            'If Val(txtReleasedqty.Text) < Totq Then
            '    lblMsgBox.Text = "Please check completed and rejected  quantity!"
            '    Exit Sub
            'End If
            'If Val(txtRejectedQty.Text) > 0 Then
            '    If Trim(txtRejDes1.Text) = "" Then
            '        lblMsgBox.Text = "Please scan rejected code!"
            '        Exit Sub
            '    End If
            'End If

            If Val(txtCompletedQty.Text) > Val(txtReleasedqty.Text) Then
                lblMsgBox.Text = "Please check completed quantity!"
                Exit Sub
            End If
            If Val(txtReleasedqty.Text) < Val(txtRejectedQty.Text) Then
                lblMsgBox.Text = "Please check rejected quantity!"
                Exit Sub
            End If
            'If Trim(txtOPQTY.Text) > Val(txtCompletedQty.Text) + Val(txtRejectedQty.Text) Then
            '    txtOPQTY.Text = Val(txtCompletedQty.Text) + Val(txtRejectedQty.Text)
            'End If
            If Trim(txtOPQTY.Text) < Val(txtCompletedQty.Text) + Val(txtRejectedQty.Text) Then
                lblMsgBox.Text = "Please check completed and rejected  quantity!"
            End If
            If Val(txtOPQTY.Text) < (Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text)) Then
                lblMsgBox.Text = "Please check completed and rejected  quantity!"
                Exit Sub
            End If

            Dim Totq As Double
            Totq = Val(lbltotCom.Text) + Val(lbltotRej.Text) + Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text)
            'Totq = Val(lbltotCom.Text) + Val(lbltotRej.Text) + Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text) - Val(txtARCQTY.Text)

            If Val(txtReleasedqty.Text) < Totq Then
                lblMsgBox.Text = "Please check completed and rejected  quantity!"
                Exit Sub
            End If
            If Val(txtRejectedQty.Text) > 0 Then
                If Trim(txtRejDes1.Text) = "" Then
                    lblMsgBox.Text = "Please scan rejected code!"
                    Exit Sub
                End If
            End If


        End If
        If Trim(txtMachineid.Text) = "" Then
            lblMsgBox.Text = "Please enter machine ID!"
            Exit Sub
        End If
        If Trim(txtMachineid.Text) <> "" Then
            If CheckMachineID() = False Then
                lblMsgBox.Text = "Please check machine id(" & txtMachineid.Text & ")"
                txtMachineid.Text = ""
            End If
        End If

        If Trim(txtOPQTY.Text) = "" Or Val(txtOPQTY.Text) = 0 Then
            lblMsgBox.Text = "Please enter received quantity!"
            Exit Sub
        End If



        Dim s() As String
        Dim s1() As String
        Dim sd, ED As Date
        Dim sddbl, EDdbl As Double
        Try
            s = Split(txtStartDate.Text, "/")
            sd = DateSerial(s(2), s(1), s(0))
            sddbl = sd.ToOADate
            If Val(txtEndDate.Text) = 0 Or Trim(txtEndDate.Text) = "" Then
                EDdbl = 0

            Else
                s1 = Split(txtEndDate.Text, "/")
                ED = DateSerial(s1(2), s1(1), s1(0))
                EDdbl = ED.ToOADate
            End If

        Catch ex As Exception
            lblMsgBox.Text = "Please check date!"
            Exit Sub
        End Try

        Try
            Dim n As Integer = Val(Mid(txtStartTime.Text, 1, 2))
            If n > 23 Then
                lblMsgBox.Text = "Please check time!"
                Exit Sub
            End If
            n = Val(Mid(txtStartTime.Text, 3, 4))
            If n > 59 Then
                lblMsgBox.Text = "Please check time!"
                Exit Sub
            End If

            n = Val(Mid(txtEndTime.Text, 1, 2))
            If n > 23 Then
                lblMsgBox.Text = "Please check time!"
                Exit Sub
            End If
            n = Val(Mid(txtEndTime.Text, 3, 4))
            If n > 59 Then
                lblMsgBox.Text = "Please check time!"
                Exit Sub
            End If
        Catch ex As Exception
        End Try

        If Val(txtTansID.Text) = 0 Then
            If (Val(lblTotOPQTY.Text) + Val(txtOPQTY.Text)) > Val(txtReleasedqty.Text) Then
                lblMsgBox.Text = "Please check received quantity!"
                Exit Sub
            End If
        Else
            If ((Val(lblTotOPQTY.Text) - Val(txtOPQTY1.Text)) + Val(txtOPQTY.Text)) > Val(txtReleasedqty.Text) Then
                lblMsgBox.Text = "Please check received quantity!"
                Exit Sub
            End If
        End If
        If Val(txtTansID.Text) <> 0 Then
            If Val(txtRejectedQty.Text) <> Val(txtRejqty1.Text) + Val(txtRejqty2.Text) + Val(txtRejqty3.Text) Then
                lblMsgBox.Text = "Please check rejected quantity!"
                Exit Sub
            End If
            If Val(txtCompletedQty.Text) <> 0 Or Val(txtRejectedQty.Text) <> 0 Then
                If Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text) < Val(txtOPQTY.Text) Then
                    txtOPQTY.Text = Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text)
                End If
            End If
        End If

        Try
            If Val(txtTansID.Text) = 0 Then
                If txtOPQTY.Text = Val(txtReleasedqty.Text) Then
                    txtsuf.Text = "-"
                Else
                    txtsuf.Text = GetMaxSufID()
                    If txtsuf.Text = "" Then
                        txtsuf.Text = "-"
                        MessageBox.Show("Can not Split this job!", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        lblMsgBox.Text = "Can not Split this job!"
                        Exit Sub
                    End If
                End If
            Else

                'If Val(txtRejectedQty.Text) <> 0 Or Val(txtCompletedQty.Text) <> 0 Then
                '    If (Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text)) < Val(txtReleasedqty.Text) Then
                '        txtsuf.Text = GetMaxSufID()
                '        If txtsuf.Text = "" Then
                '            txtsuf.Text = "-"
                '            MessageBox.Show("Can not Split this job!", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                '            lblMsgBox.Text = "Can not Split this job!"
                '            Exit Sub
                '        End If
                '    Else
                '        txtsuf.Text = GetMaxSufID()
                '        If txtsuf.Text = "" Then
                '            txtsuf.Text = "-"
                '            MessageBox.Show("Can not Split this job!", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                '            lblMsgBox.Text = "Can not Split this job!"
                '            Exit Sub
                '        End If
                '        'txtsuf.Text = "-"
                '    End If
                'Else
                txtsuf.Text = "-"
                If Val(txtOPQTY1.Text) <> Val(txtOPQTY.Text) Then
                    txtsuf.Text = GetMaxSufID()
                    If txtsuf.Text = "" Then
                        txtsuf.Text = "-"
                        MessageBox.Show("Can not Split this job!", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        lblMsgBox.Text = "Can not Split this job!"
                        Exit Sub
                    End If
                End If





                'End If
            End If

            With com


                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "sp_Job_Add"
                parm = .Parameters.Add("@job", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtJobNo.Text)
                parm = .Parameters.Add("@jobsuffix", SqlDbType.VarChar, 50)

                parm.Value = txtsuf.Text
                parm = .Parameters.Add("@jobsuffixParent", SqlDbType.VarChar, 50)
                parm.Value = txtSufParent.Text

                parm = .Parameters.Add("@wc", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtWKSatationID.Text)
                parm = .Parameters.Add("@oper_num", SqlDbType.Int)
                parm.Value = Val(Trim(txtOperationNo.Text))
                parm = .Parameters.Add("@item", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtItemNo.Text)
                parm = .Parameters.Add("@JobDate", SqlDbType.Float)
                parm.Value = 0
                parm = .Parameters.Add("@trans_type", SqlDbType.Char, 10)
                parm.Value = ""
                parm = .Parameters.Add("@trans_datefrm", SqlDbType.Float)
                parm.Value = sddbl
                parm = .Parameters.Add("@trans_dateto", SqlDbType.Float)
                parm.Value = EDdbl
                parm = .Parameters.Add("@trans_Timefrm", SqlDbType.Int)
                parm.Value = Val(txtStartTime.Text)
                parm = .Parameters.Add("@trans_TimeTo", SqlDbType.Int)
                parm.Value = Val(txtEndTime.Text)

                parm = .Parameters.Add("@qty_Rele_qty", SqlDbType.Decimal)
                parm.Value = Val(txtReleasedqty.Text)
                parm = .Parameters.Add("@qty_op_qty", SqlDbType.Decimal)
                parm.Value = Val(txtOPQTY.Text)
                parm = .Parameters.Add("@qty_complete", SqlDbType.Decimal)
                parm.Value = Val(txtCompletedQty.Text)
                parm = .Parameters.Add("@qty_scrapped", SqlDbType.Decimal)
                parm.Value = Val(txtRejectedQty.Text)
                parm = .Parameters.Add("@no_oper", SqlDbType.Int)
                parm.Value = Val(txtOperators.Text)
                parm = .Parameters.Add("@CreatedBy", SqlDbType.VarChar, 255)
                parm.Value = txtEmpName.Text

                parm = .Parameters.Add("@rejectedCode1", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtRejCode1.Text)
                parm = .Parameters.Add("@rejectedDesc1", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtRejDes1.Text)
                parm = .Parameters.Add("@rejectedqty1", SqlDbType.Decimal)
                parm.Value = Val(Trim(txtRejqty1.Text))

                parm = .Parameters.Add("@rejectedCode2", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtRejCode2.Text)
                parm = .Parameters.Add("@rejectedDesc2", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtRejDes2.Text)
                parm = .Parameters.Add("@rejectedqty2", SqlDbType.Decimal)
                parm.Value = Val(Trim(txtRejqty2.Text))

                parm = .Parameters.Add("@rejectedCode3", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtRejCode3.Text)
                parm = .Parameters.Add("@rejectedDesc3", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtRejDes3.Text)
                parm = .Parameters.Add("@rejectedqty3", SqlDbType.Decimal)
                parm.Value = Val(Trim(txtRejqty3.Text))
                parm = .Parameters.Add("@emp_num", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtEmpid.Text)
                parm = .Parameters.Add("@emp_name", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtEmpName.Text)
                parm = .Parameters.Add("@tansnum", SqlDbType.Int)
                parm.Value = Val(txtTansID.Text)
                parm = .Parameters.Add("@nextopno", SqlDbType.Int)
                parm.Value = Val(0)
                parm = .Parameters.Add("@machineid", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtMachineid.Text)
                parm = .Parameters.Add("@Handoverid", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtHandoverid.Text)
                parm = .Parameters.Add("@HandoverName", SqlDbType.VarChar, 255)
                parm.Value = Trim(txtHandoverName.Text)






                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                txtJobNo.Clear()
                ClearAll()
                list_Displaydata()
                '   txtWKID.Select()
                lblMsgBox.Text = "Successfully updated!"
            End With

        Catch ex As Exception
            lblMsgBox.Text = (ex.Message)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try

    End Sub

    Private Sub dtpStart_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'txtStartDate.Text = Format(dtpStart.Value, "dd/MM/yyyy")
        'dtpStart.Checked = False
    End Sub

    Private Sub dtpEnd_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'txtEndDate.Text = Format(dtpEnd.Value, "dd/MM/yyyy")
        'dtpEnd.Checked = False
    End Sub

 

    Private Sub frmDataEntryOP_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMsgBox.Text = ""
        'LaodFix()
        ' cmbsuf.SelectedIndex = 0
        'MsgBox("Veeranar Appa")
        With lstv
            .Columns.Add("IDNo", 0, HorizontalAlignment.Left)
            .Columns.Add("Job ID", 30, HorizontalAlignment.Left)
            .Columns.Add("OP ID", 65, HorizontalAlignment.Left)
            .Columns.Add("PS", 30, HorizontalAlignment.Left)
            .Columns.Add("CS", 30, HorizontalAlignment.Left)
            .Columns.Add("Station ID", 75, HorizontalAlignment.Left)
            .Columns.Add("OP No.", 50, HorizontalAlignment.Left)
            .Columns.Add("Item No.", 70, HorizontalAlignment.Left)
            .Columns.Add("SD", 70, HorizontalAlignment.Left)
            .Columns.Add("ED", 70, HorizontalAlignment.Left)
            .Columns.Add("ST", 40, HorizontalAlignment.Left)
            .Columns.Add("ET", 40, HorizontalAlignment.Left)
            .Columns.Add("RQ ", 50, HorizontalAlignment.Left)
            .Columns.Add("CQ ", 50, HorizontalAlignment.Left)
            .Columns.Add("Rej-Q", 50, HorizontalAlignment.Left)
            lbltotCom.Text = 0.0

            lbltotRej.Text = 0.0
            list_Displaydata()
            ' txtStartDate.Text = Format(Now, "dd/MM/yyyy")
            ' txtStartTime.Text = Format(Now, "HHmm")
            'list_Displaydata()
        End With
        ' Me.Select()
        Me.txtC.Select()

    End Sub

   
    Private Sub butExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Application.Exit()
    End Sub


    Private Sub txtC_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtC.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtOPQTY.BackColor = Color.White
            txtCompletedQty.BackColor = Color.White
            txtRejectedQty.BackColor = Color.White
            txtRejqty1.BackColor = Color.White
            txtRejqty2.BackColor = Color.White
            txtRejqty3.BackColor = Color.White
            If (txtC.Text) <> "" Then
                lblMsgBox.Text = "Incorrect scan code"
                If txtC.TextLength >= 2 Then
                    Dim idv As String = UCase(Mid(txtC.Text, 1, 2))
                    Dim ACV As String = UCase(Mid(txtC.Text, 3, txtC.TextLength))
                    txtC.Text = ""
                    If idv = "JN" Then  'Job No
                        lblSplit.Visible = False
                        lblMsgBox.Text = ""
                        txtJobNo.Text = ""
                        txtJobNo.Text = ACV
                        If txtWKID_KeyPress(sender, e) = False Then
                            Exit Sub
                        End If
                        txtWKSatationID_KeyPress(sender, e)
                        Dim St As Boolean
                        St = CheckTansdata()
                        txtC.Focus()
                    ElseIf idv = "ON" Then 'Work Center
                        lblSplit.Visible = False
                        ClearReject()
                        txtSubval.Text = ""
                        txtSufParent.Text = "-"
                        txtEndDate.Text = ""
                        txtEndTime.Text = ""
                        lblMsgBox.Text = ""
                        txtWKSatationID.Text = ""
                        txtWKSatation.Text = ""
                        txtOperationNo.Text = ACV
                        txtWKID_KeyPress(sender, e)
                        txtWKSatationID_KeyPress(sender, e)
                        Dim St As Boolean
                        St = CheckTansdata()
                        txtC.Focus()

                        If Trim(txtMachineid.Text) <> "" Then
                            If CheckMachineID() = False Then
                                lblMsgBox.Text = "Please check machine id(" & txtMachineid.Text & ")"
                                txtMachineid.Text = ""
                            End If
                        End If
                        txtC.Focus()
                    ElseIf idv = "SF" Then
                        txtSufParent.Text = ACV
                        txtReleasedqty.Text = RelQtyAll


                        'selreciedQty()
                        ' CheckTansdata()
                        selreciedQty()
                        CheckTansdata()
                        If txtSufParent.Text <> "" Then
                            Timer1.Stop()
                            lblSplit.Visible = False
                        End If
                        list_Displaydata()
                        'txtWKID_KeyPress
                        ' txtWKID_KeyPress(sender, e)
                        txtWKSatationID_KeyPress(sender, e)
                        Dim St As Boolean
                        St = CheckTansdata()
                        txtC.Focus()

                        'ElseIf idv = "WC" Then 'Work Center
                        '    lblMsgBox.Text = ""
                        '    txtWKSatationID.Text = ""
                        '    txtWKSatationID.Text = ACV
                        '    txtWKID_KeyPress(sender, e)
                        '    txtWKSatationID_KeyPress(sender, e)
                        'ElseIf idv = "NX" Then 'Work Center
                        '    lblMsgBox.Text = ""
                        '    txtNextOPNo.Text = ACV
                        '    txtC.Focus()
                        '    Exit Sub
                    ElseIf idv = "ID" Then 'Operator ID No.
                        txtEndDate.Text = ""
                        txtEndTime.Text = ""
                        lblMsgBox.Text = ""
                        txtEmpid.Text = ""
                        txtEmpid.Text = ACV
                        If Check_opID(ACV) = True Then
                            txtWKSatationID_KeyPress(sender, e)
                        End If
                    ElseIf idv = "OP" Then  'No of Operator Key in Filed
                        txtEndDate.Text = ""
                        txtEndTime.Text = ""
                        lblMsgBox.Text = ""
                        txtOperators.Text = ""
                        txtOperators.Text = ACV

                        'txtOperators.Select()
                        'txtOperators.Focus()
                        txtC.Focus()
                        Exit Sub




                    ElseIf idv = "HO" Then 'Operator ID No(Handover).
                        If Val(txtTansID.Text) = 0 Or Trim(txtTansID.Text) = "" Then
                            lblMsgBox.Text = "Please check transaction data!"
                            txtC.Focus()
                            Exit Sub
                        End If

                        ' plHand.Visible = False
                        lblMsgBox.Text = ""
                        txtHandoverid.Text = ""
                        txtHandoverName.Text = ""
                        txtHandoverid.Text = ACV

                        If ACV = "" Then
                            Timer2.Stop()

                            Dim objhand As New frmHandOver
                            objhand.IDNo = Val(txtTansID.Text)
                            objhand.lblJob.Text = txtJobNo.Text
                            objhand.lblMachine.Text = txtMachineid.Text
                            objhand.lblSplit.Text = txtSufParent.Text
                            objhand.lblOperation.Text = txtOperationNo.Text
                            objhand.lblEMPID.Text = txtEmpid.Text
                            ChHandOverID = ""
                            ChHandOverName = ""
                            objhand.ShowDialog()
                            If ChHandOverID <> "" Then
                                txtEmpid.Text = ChHandOverID
                                txtEmpName.Text = ChHandOverName
                            End If
                            Timer2.Start()
                            ' plHand.Visible = True
                            '  txtHandoverid.ReadOnly = False
                            '  txtHandoverid.Focus()
                            Exit Sub
                        Else
                            If txtEmpid.Text <> txtHandoverid.Text And txtEmpid.Text <> "" Then
                                txtHandoverid.ReadOnly = True
                                If Check_opIDhand(ACV) = True Then
                                    ' plHand.Visible = True
                                    txtC.Focus()
                                Else
                                    lblMsgBox.Text = "Please check Handover id(" & ACV & ")"
                                    txtC.Focus()
                                    Exit Sub

                                End If
                            Else
                                lblMsgBox.Text = "Please check Handover id(" & ACV & ")"
                                txtC.Focus()
                                Exit Sub
                            End If
                        End If





                    ElseIf idv = "MC" Then  'Machine ID
                        txtEndDate.Text = ""
                        txtEndTime.Text = ""
                        lblMsgBox.Text = ""
                        txtMachineid.Text = ""
                        txtMachineid.Text = ACV
                        If txtOperationNo.Text <> "" Then
                            If CheckMachineID() = False Then
                                lblMsgBox.Text = "Please check machine id(" & ACV & ")"

                            End If
                        End If
                        list_Displaydata()
                        txtC.Focus()
                        Exit Sub
                    ElseIf idv = "OQ" Then    'No of Quantiry Take for Opration. Key in Filed
                        lblMsgBox.Text = ""
                        txtOPQTY.Text = ""
                        txtOPQTY1.Text = ""
                        txtOPQTY.BackColor = Color.Yellow
                        txtOPQTY.Select()
                        txtOPQTY.Focus()
                        Exit Sub
                    ElseIf idv = "CQ" Then
                        lblMsgBox.Text = ""
                        txtCompletedQty.Text = ""
                        txtCompletedQty.BackColor = Color.Yellow
                        txtCompletedQty.Focus()
                        Exit Sub
                    ElseIf idv = "RJ" Then
                        lblMsgBox.Text = ""
                        txtRejectedQty.Text = ""
                        txtRejectedQty.BackColor = Color.Yellow
                        txtRejectedQty.Focus()
                        Exit Sub
                    ElseIf idv = "RA" Then
                        lblMsgBox.Text = ""
                        txtRejCode1.Text = ""
                        txtRejCode1.Text = ACV
                        txtRejCode1_KeyPress()
                        'txtC.Focus()
                        Exit Sub
                    ElseIf idv = "QA" Then
                        txtRejqty1.Text = ""
                        txtRejqty1.BackColor = Color.Yellow
                        txtRejqty1.Focus()
                        Exit Sub
                    ElseIf idv = "RB" Then
                        lblMsgBox.Text = ""
                        txtRejCode2.Text = ""
                        txtRejCode2.Text = ACV
                        txtRejCode2_KeyPress()
                        ' txtC.Focus()
                        Exit Sub
                    ElseIf idv = "QB" Then
                        txtRejqty2.Text = ""
                        txtRejqty2.BackColor = Color.Yellow
                        txtRejqty2.Focus()
                        Exit Sub
                    ElseIf idv = "RC" Then
                        lblMsgBox.Text = ""
                        txtRejCode3.Text = ""
                        txtRejCode3.Text = ACV
                        txtRejCode3_KeyPress()
                        Exit Sub
                    ElseIf idv = "QC" Then
                        lblMsgBox.Text = ""
                        txtRejqty3.Text = ""
                        txtRejqty3.BackColor = Color.Yellow
                        txtRejqty3.Focus()
                        txtC.Focus()
                        Exit Sub



                    ElseIf idv = "SA" Then
                        lblMsgBox.Text = ""
                        butSave_Click()
                        txtC.Select()
                        txtC.Focus()
                        Exit Sub
                    ElseIf idv = "EX" Then
                        Try
                            lblMsgBox.Text = ""
                            Me.Close()

                        Catch ex As Exception
                        End Try
                    ElseIf idv = "CL" Then
                        lblMsgBox.Text = ""
                        ClearAll()
                    End If

                    If idv <> "WC" Or idv <> "WK" Then
                        list_Displaydata()
                    Else
                        lstv.Items.Clear()
                    End If

                End If

            End If


            txtC.Text = ""
            txtC.Select()
            txtC.Focus()
        End If

    End Sub
    Function Check_opID(ByVal idno As String) As Boolean
        Check_opID = False
        txtEmpid.Text = ""
        txtEmpName.Text = ""
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "SELECT     * FROM  tbOperator where _operatorID='" & fncstr(idno) & "'"
                cn.Open()
                dr = .ExecuteReader()
                If dr.Read Then
                    txtEmpid.Text = dr.Item("_operatorID")
                    txtEmpName.Text = dr.Item("_operatorName")
                    Check_opID = True
                Else
                    lblMsgBox.Text = "Please check your operator code(" & idno & " )!"
                    txtEmpid.Text = ""
                    txtEmpName.Text = ""
                    Check_opID = False
                End If
            End With
        Catch ex As Exception
            lblMsgBox.Text = ex.Message
            Check_opID = False
            txtEmpid.Text = ""
            txtEmpName.Text = ""
        Finally
            cn.Close()
        End Try

        'If Check_opID = False Then
        '    lblMsgBox.Text = "Please check your operator code(" & idno & " )!"
        'End If
        Return Check_opID

    End Function


    Function Check_opIDhand(ByVal idno As String) As Boolean
        Check_opIDhand = False
        txtHandoverid.Text = ""
        txtHandoverName.Text = ""
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "SELECT     * FROM  tbOperator where _operatorID='" & fncstr(idno) & "'"
                cn.Open()
                dr = .ExecuteReader()
                If dr.Read Then
                    txtHandoverid.Text = dr.Item("_operatorID")
                    txtHandoverName.Text = dr.Item("_operatorName")
                    Check_opIDhand = True
                Else
                    lblMsgBox.Text = "Please check your operator code(" & idno & " )!"
                    txtHandoverid.Text = ""
                    txtHandoverName.Text = ""
                    Check_opIDhand = False
                End If
            End With
        Catch ex As Exception
            lblMsgBox.Text = ex.Message
            Check_opIDhand = False
            txtHandoverid.Text = ""
            txtHandoverName.Text = ""
        Finally
            cn.Close()
        End Try

        'If Check_opID = False Then
        '    lblMsgBox.Text = "Please check your operator code(" & idno & " )!"
        'End If
        Return Check_opIDhand

    End Function
    Private Sub txtOPQTY_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtOPQTY.KeyPress
        If (Asc(e.KeyChar) > 46 And Asc(e.KeyChar) < 58) Or Asc(e.KeyChar) = 8 Or Asc(e.KeyChar) = 13 Then
            e.Handled = False
            If Asc(e.KeyChar) = 13 Then
                txtOPQTY.BackColor = Color.White
                txtC.Focus()
            End If
        Else
            e.Handled = True
        End If
    End Sub

    
    

    Private Sub txtRejqty3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRejqty3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtRejqty3.BackColor = Color.White
            If Trim(txtRejqty3.Text) = "" Then
                txtRejCode3.Text = ""
                txtRejDes3.Text = ""
            End If
            txtC.Focus()
        End If
    End Sub

   
  
   
    Private Sub Panel4_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel4.Paint
        '  txtC.Focus()
    End Sub

    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click
        txtC.Focus()
    End Sub

    Private Sub lstv_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lstv.ColumnClick
        txtC.Focus()
    End Sub

    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub

   

    Private Sub txtWKID_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        txtC.Focus()
    End Sub
    Private Sub txtOperationNo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOperationNo.GotFocus
        txtC.Focus()
    End Sub
    Private Sub txtNextOPNo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        txtC.Focus()
    End Sub

   

    Private Sub txtEmpid_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpid.GotFocus
        txtC.Focus()
    End Sub
    Private Sub txtWKSatationID_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWKSatationID.GotFocus
        txtC.Focus()
    End Sub
    Private Sub txtJobNo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtJobNo.GotFocus
        txtC.Focus()
    End Sub
   
    Private Sub txtReleasedqty_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtReleasedqty.GotFocus
        txtC.Focus()
    End Sub
  
    Private Sub txtRejCode1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRejCode1.GotFocus
        txtC.Focus()
    End Sub
    Private Sub txtRejCode2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRejCode2.GotFocus
        txtC.Focus()
    End Sub
    Private Sub txtRejCode3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRejCode3.GotFocus
        txtC.Focus()
    End Sub
    Private Sub txtRejDes1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRejDes1.GotFocus
        txtC.Focus()
    End Sub
    Private Sub txtRejDes2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRejDes2.GotFocus
        txtC.Focus()
    End Sub
    Private Sub txtRejDes3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRejDes3.GotFocus
        txtC.Focus()
    End Sub
    Private Sub txtItemNo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtItemNo.GotFocus
        txtC.Focus()
    End Sub
    'Private Function MaxOPNO() As Boolean
    '    MaxOPNO = False
    '    Try
    '        With com
    '            .Connection = cn
    '            .CommandType = CommandType.Text
    '            .CommandText = "select max(_operationNo) as _operationNo from tbJobroute where _job='" & fncstr(txtWKID.Text) & "'"
    '            cn.Open()
    '            dr = .ExecuteReader()
    '            If dr.Read Then
    '                If txtOperationNo.Text = dr.Item("_operationNo") Then
    '                    txtNextOPNo.Text = 0
    '                    MaxOPNO = True
    '                End If
    '            End If
    '        End With

    '    Catch ex As Exception
    '        lblMsgBox.Text = (ex.Message)
    '    Finally
    '        cn.Close()
    '        txtC.Focus()
    '    End Try

    'End Function


    Private Function CheckMachineID() As Boolean
        CheckMachineID = False
        Try
            With com
                Dim Ds As New DataSet
                Dim AD As New SqlDataAdapter
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select * from tbMachineGroup A,tbMachine B where  B._mid=A._mid and _wc='" & fncstr(txtWKSatationID.Text) & "'" ' and _mid=(select _mid from tbMachine where _machineID='" & fncstr(txtMachineid.Text) & "')"
                AD.SelectCommand = com
                AD.Fill(Ds, "tbMachineGroup")
                If Ds.Tables(0).Rows.Count > 0 Then
                    Dim NDR As DataRow() = Ds.Tables(0).Select("_machineID='" & txtMachineid.Text & "'")
                    If NDR.Length > 0 Then
                        CheckMachineID = True
                    Else
                        txtMachineid.Text = ""
                        CheckMachineID = False
                    End If
                Else
                    txtMachineid.Text = "-"
                    CheckMachineID = True
                End If
                ' Dim sql As String
                ' sql = "select * from tbMachineGroup where  _wc='" & fncstr(txtWKSatationID.Text) & "' and _mid=(select _mid from tbMachine where _machineID='" & fncstr(txtMachineid.Text) & "')"
                ' cn.Open()
                ' dr = .ExecuteReader()
                'If dr.Read Then
                '    CheckMachineID = True
                'End If
            End With

        Catch ex As Exception
            lblMsgBox.Text = (ex.Message)
        Finally
            cn.Close()
            txtC.Focus()
        End Try
        Return CheckMachineID
    End Function

    'Private Sub txtEmpid_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpid.GotFocus
    '    txtC.Focus()
    'End Sub
    'Private Sub txtEmpid_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEmpid.TextChanged

    'End Sub

    Private Sub txtEmpName_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEmpName.GotFocus
        txtC.Focus()
    End Sub

    'Private Sub txtHandoverid_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtHandoverid.GotFocus
    '    txtC.Focus()
    'End Sub

    Private Sub txtHandoverName_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtHandoverName.GotFocus
        txtC.Focus()
    End Sub

    Private Sub txtMachineid_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMachineid.GotFocus
        txtC.Focus()
    End Sub
    Private Sub txtStartDate_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtStartDate.GotFocus
        txtC.Focus()
    End Sub

    
    Private Sub txtStartTime_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtStartTime.GotFocus
        txtC.Focus()
    End Sub

    Private Sub txtEndDate_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEndDate.GotFocus
        txtC.Focus()
    End Sub

    Private Sub txtEndTime_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEndTime.GotFocus
        txtC.Focus()
    End Sub


    'Sub LaodFix()
    '    ' cmbsuf.Items.Clear()
    '    Try
    '        With com
    '            .Connection = cn
    '            .CommandType = CommandType.Text
    '            'Dim DI As DataItem
    '            .CommandText = "select * from tbSuffix order by _sidNo"
    '            cn.Open()
    '            dr = .ExecuteReader(CommandBehavior.CloseConnection)
    '            While dr.Read
    '                ' cmbsuf.Items.Add(New DataItem(dr.Item("_sidNo"), dr.Item("_sno")))
    '                ' lstwrk.Items.Add(dr.Item("_wc") & " - " & dr.Item("_description"))
    '            End While
    '            cn.Close()
    '        End With
    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
    '    Finally
    '        cn.Close()
    '    End Try
    'End Sub

    Private Sub txtWKSatationID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtWKSatationID.TextChanged

    End Sub

    Private Sub txtWKSatation_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtWKSatation.GotFocus
        txtC.Focus()
    End Sub

    Private Sub txtCompletedQty_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCompletedQty.TextChanged

    End Sub
    Private Sub txtHandoverid_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtHandoverid.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If txtEmpid.Text <> txtHandoverid.Text And txtEmpid.Text <> "" Then
                txtHandoverid.ReadOnly = False
                Dim idv As String = UCase(Mid(txtHandoverid.Text, 1, 2))
                If idv = "ID" Then
                    txtHandoverid.Text = UCase(Mid(txtHandoverid.Text, 3, txtC.TextLength))
                    txtHandoverid.Text = ""
                    txtHandoverName.Text = ""
                    txtHandoverid.Focus()
                    Exit Sub

                End If
                If txtHandoverid.Text = "" Then
                    '   plHand.Visible = False
                End If
                If Check_opIDhand(txtHandoverid.Text) = True Then
                    ' plHand.Visible = True
                    txtC.Focus()
                Else
                    lblMsgBox.Text = "Please check Handover id(" & txtHandoverid.Text & ")"
                    txtHandoverid.Text = ""
                    txtHandoverName.Text = ""
                    'plHand.Visible = False
                    txtC.Focus()
                    Exit Sub
                End If
            Else
             
                lblMsgBox.Text = "Please check Handover id(" & txtHandoverid.Text & ")"
                txtHandoverid.Text = ""
                txtHandoverName.Text = ""
                txtHandoverid.Focus()
                Exit Sub
            End If
        End If
    End Sub
    Function GetMaxSufID() As String
        GetMaxSufID = "-"
        If txtSubval.Text = "" Then
            txtSubval.Text = "''"
        End If

        Dim DSTrans As New DataSet

        Dim AdTrans As New SqlDataAdapter
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text

                .CommandText = "select * from tbJobTrans where _job='" & Trim(txtJobNo.Text) & "'"
                AdTrans.SelectCommand = com
                AdTrans.Fill(DSTrans, "AdTrans")

            End With


            Dim DRCheckChiled() As DataRow = DSTrans.Tables(0).Select("_jobsuffixParent='" & Trim(txtSufParent.Text) & "' and _oper_num=" & Val(txtOperationNo.Text))
            If DRCheckChiled.Length > 0 Then
                Dim i As Integer
                For i = 0 To DRCheckChiled.Length - 1
                    If txtSubval.Text = "" Then
                        txtSubval.Text = "'" & Trim(DRCheckChiled(i).Item("_jobsuffix")) & "'"
                    Else
                        txtSubval.Text = txtSubval.Text & ",'" & Trim(DRCheckChiled(i).Item("_jobsuffix")) & "'"
                    End If
                Next
            End If
            Dim DRCheckAfter() As DataRow = DSTrans.Tables(0).Select("_jobsuffixParent='" & Trim(txtSufParent.Text) & "' and _oper_num>" & Val(txtOperationNo.Text))
            If DRCheckAfter.Length > 0 Then
                GetMaxSufID = ""
                Return GetMaxSufID
                Exit Function
            End If
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                Dim sql As String = "select Min(_lel_1_Name) as _lel_1_Name from tbLevel1 where _lel_1_Parent='" & Trim(txtSufParent.Text) & "' and _lel_1_Name not in (" & txtSubval.Text & ")"
                .CommandText = "select Min(_lel_1_Name) as _lel_1_Name from tbLevel1 where _lel_1_Parent='" & Trim(txtSufParent.Text) & "' and _lel_1_Name not in (" & txtSubval.Text & ")"
                cn.Open()
                dr = .ExecuteReader
                If dr.Read Then
                    GetMaxSufID = dr.Item("_lel_1_Name")
                    cn.Close()
                End If
                cn.Close()
                Return GetMaxSufID
            End With

        Catch ex As Exception
        Finally
            cn.Close()
        End Try
    End Function

    Function CheckTansdata() As Boolean
        txtSubval.Text = ""
        txtsuf.Text = "-"
        CheckTansdata = False
        lblMsgBox.Text = ""
        If Trim(txtJobNo.Text) <> "" And Trim(txtOperationNo.Text) <> "" Then
            Dim DSRoute As New DataSet
            Dim DSTrans As New DataSet
            Dim DSLevel As New DataSet
            Dim AdLevel As New SqlDataAdapter
            Dim AdRoute As New SqlDataAdapter
            Dim AdTrans As New SqlDataAdapter
            'select * from tbJobRoute where _job='TH00081625'
            'select * from tbJobTrans where _job='TH00081625'
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "select * from tbLevel1 "
                    AdLevel.SelectCommand = com
                    AdLevel.Fill(DSLevel, "tbLevel")


                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "select * from tbJobRoute where _job='" & Trim(txtJobNo.Text) & " '"
                    AdRoute.SelectCommand = com
                    AdRoute.Fill(DSRoute, "tbroute")


                    .CommandText = "select * from tbJobTrans where _job='" & Trim(txtJobNo.Text) & "'"
                    AdTrans.SelectCommand = com
                    AdTrans.Fill(DSTrans, "AdTrans")
                    If Trim(txtSufParent.Text) = "" Then
                        txtSufParent.Text = "-"
                    End If
                    Dim DRCheckPerant() As DataRow = DSTrans.Tables(0).Select("_jobsuffixParent='" & Trim(txtSufParent.Text) & "' and _oper_num<" & Val(txtOperationNo.Text))
                    Dim DRCheckAfter() As DataRow = DSTrans.Tables(0).Select("_jobsuffixParent='" & Trim(txtSufParent.Text) & "' and _oper_num>" & Val(txtOperationNo.Text))

                    If DRCheckPerant.Length > 0 Then

                        Dim i As Integer
                        Dim Stu As Boolean = False
                        For i = 0 To DRCheckPerant.Length - 1
                            If DRCheckPerant(i).Item("_jobsuffix") <> "-" Then
                                Stu = True
                            End If
                        Next
                        If Stu = True Then
                            txtSufParent.Text = ""
                            txtReleasedqty.Text = 0
                            lblMsgBox.Text = "Please Check split id"
                            'txtSufParent.Select()
                            'txtSufParent.Focus()
                            lblSplit.Visible = False
                            Timer1.Start()
                            CheckTansdata = True
                        Else
                            Timer1.Stop()
                            txtReleasedqty.Text = DRCheckPerant(0).Item("_qty_complete")
                            lblSplit.Visible = False
                            CheckTansdata = False
                        End If





                    Else

                        Dim DRCheckChiled() As DataRow = DSTrans.Tables(0).Select("_jobsuffixParent='" & Trim(txtSufParent.Text) & "' and _oper_num=" & Val(txtOperationNo.Text))
                        If DRCheckChiled.Length > 0 Then
                            Dim i As Integer
                            For i = 0 To DRCheckChiled.Length - 1
                                If txtSubval.Text = "" Then
                                    txtSubval.Text = "'" & Trim(DRCheckChiled(i).Item("_jobsuffix")) & "'"
                                Else
                                    txtSubval.Text = txtSubval.Text & ",'" & Trim(DRCheckChiled(i).Item("_jobsuffix")) & "'"
                                End If
                            Next
                        End If
                        txtsuf.Text = GetMaxSufID()
                    End If
                End With
            Catch ex As Exception
                lblMsgBox.Text = ex.Message
            Finally
                cn.Close()
                com.Parameters.Clear()

            End Try
        End If
        Return CheckTansdata
    End Function

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If txtSufParent.Text = "" Then
            If lblSplit.Visible = False Then
                lblSplit.Visible = True
            Else
                lblSplit.Visible = False
            End If

        End If
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick

        If lblMsgBox.Text = "" Then
            Countdown = 0
        ElseIf lblMsgBox.Text <> "" Then
            Countdown += 1
            pic1.Visible = True
            If lblMsgBox.Visible = True Then

                lblMsgBox.Visible = False
            Else

                lblMsgBox.Visible = True
            End If
        End If

        If Trim(lblMsgBox.Text) = "" Then
            pic1.Visible = False
        End If

        If Countdown = 10 Then
            pic1.Visible = False
            lblMsgBox.Visible = True
            lblMsgBox.Text = ""
        End If
        If txtEmpid.Text <> "" Then
            lbl1.Visible = True
        End If

        If txtJobNo.Text <> "" Then
            lbl2.Visible = True
        End If
        If txtOperationNo.Text <> "" Then
            lbl3.Visible = True
        End If
        If txtMachineid.Text <> "" Then
            lbl4.Visible = True
        End If
        If txtOperators.Text <> "" Then
            lbl5.Visible = True
        End If
        If txtOPQTY.Text <> "" Then
            lbl6.Visible = True
        End If
        If txtCompletedQty.Text <> "" Then
            lbl7.Visible = True
        End If
        'lbl3.Visible = True
        'lbl4.Visible = True
        'lbl5.Visible = True
        'lbl6.Visible = True
        'lbl7.Visible = True
        If txtEmpid.Text = "" Then
            If lbl1.Visible = True Then
                lbl1.Visible = False
            Else
                lbl1.Visible = True
            End If
        ElseIf txtJobNo.Text = "" Then
            If lbl2.Visible = True Then
                lbl2.Visible = False
            Else
                lbl2.Visible = True
            End If
        ElseIf txtOperationNo.Text = "" Then
            If lbl3.Visible = True Then
                lbl3.Visible = False
            Else
                lbl3.Visible = True
            End If
        ElseIf txtMachineid.Text = "" Then
            If txtSufParent.Text <> "" Then
                If lbl4.Visible = True Then
                    lbl4.Visible = False
                Else
                    lbl4.Visible = True
                End If
            End If
            ElseIf txtOPQTY.Text = "" Then
                If lbl6.Visible = True Then
                    lbl6.Visible = False
                Else
                    lbl6.Visible = True
                End If
            ElseIf txtCompletedQty.Text = "" Then
                If lbl7.Visible = True Then
                    lbl7.Visible = False
                Else
                    lbl7.Visible = True
                End If
            ElseIf txtOperators.Text = "" Then
                If lbl5.Visible = True Then
                    lbl5.Visible = False
                Else
                    lbl5.Visible = True
                End If
            Else
                lbl1.Visible = True
                lbl2.Visible = True
                lbl3.Visible = True
                lbl4.Visible = True
                lbl5.Visible = True
                lbl6.Visible = True
                lbl7.Visible = True


            End If


    End Sub

    Sub selreciedQty()

        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                Dim sql As String = "select Max(_oper_num),_qty_complete from tbJobTrans where _job='" & Trim(txtJobNo.Text) & "' and _jobsuffix='" & (txtSufParent.Text) & "' and _oper_num<" & Val(txtOperationNo.Text) & " group by _oper_num,_qty_complete"
                .CommandText = "select Max(_oper_num),_qty_complete from tbJobTrans where _job='" & Trim(txtJobNo.Text) & "' and _jobsuffix='" & (txtSufParent.Text) & "' and _oper_num<" & Val(txtOperationNo.Text) & " group by _oper_num,_qty_complete"
                cn.Open()
                dr = .ExecuteReader()
                If dr.Read Then
                    txtReleasedqty.Text = dr.Item("_qty_complete")
                    cn.Close()
                   

                Else
                    txtReleasedqty.Text = 0
                    cn.Close()
                  
                End If
            End With

        Catch ex As Exception
            lblMsgBox.Show()
        Finally
            cn.Close()

        End Try
    End Sub
    Private Sub txtsuf_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtsuf.GotFocus
        txtC.Focus()
    End Sub
    Private Sub txtItemNoTemp_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtItemNoTemp.GotFocus
        txtC.Focus()
    End Sub
    
    Sub ClearReject()
        txtRejCode1.Clear()
        txtRejCode2.Clear()
        txtRejCode3.Clear()
        txtRejDes1.Clear()
        txtRejDes2.Clear()
        txtRejDes3.Clear()
        txtRejqty1.Clear()
        txtRejqty2.Clear()
        txtRejqty2.Clear()


    End Sub

    Private Sub txtRejqty1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRejqty1.TextChanged

    End Sub

    Private Sub txtRejqty3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRejqty3.TextChanged

    End Sub
End Class