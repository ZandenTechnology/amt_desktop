<[Global].Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
[Partial] Class frmSupJobEdit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components [IsNot] Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSupJobEdit))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.lblTot = New System.Windows.Forms.Label
        Me.txtTot = New System.Windows.Forms.Label
        Me.butAdd = New System.Windows.Forms.Button
        Me.Label9 = New System.Windows.Forms.Label
        Me.lstv = New System.Windows.Forms.ListView
        Me.pl = New System.Windows.Forms.Panel
        Me.Label11 = New System.Windows.Forms.Label
        Me.cmbName = New System.Windows.Forms.ComboBox
        Me.txtRemarks = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.butPrint = New System.Windows.Forms.Button
        Me.butDelete = New System.Windows.Forms.Button
        Me.txtsuf = New System.Windows.Forms.TextBox
        Me.butUpdate = New System.Windows.Forms.Button
        Me.txtOperatorName = New System.Windows.Forms.TextBox
        Me.txtParent = New System.Windows.Forms.Label
        Me.txtIDNo = New System.Windows.Forms.TextBox
        Me.txtExport = New System.Windows.Forms.Label
        Me.lbReWork = New System.Windows.Forms.Label
        Me.lblExport = New System.Windows.Forms.Label
        Me.cmbOPerator = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lblss = New System.Windows.Forms.Label
        Me.txtNoofOperator = New System.Windows.Forms.TextBox
        Me.cmbMachine = New System.Windows.Forms.ComboBox
        Me.txtWorkCenter = New System.Windows.Forms.TextBox
        Me.txtOPNo = New System.Windows.Forms.TextBox
        Me.cmbAction = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtOPRlq = New System.Windows.Forms.TextBox
        Me.Panel10 = New System.Windows.Forms.Panel
        Me.dtpEnd = New System.Windows.Forms.DateTimePicker
        Me.dtPStart = New System.Windows.Forms.DateTimePicker
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtEnd = New System.Windows.Forms.TextBox
        Me.txtRejected = New System.Windows.Forms.TextBox
        Me.txtCompleted = New System.Windows.Forms.TextBox
        Me.txtStart = New System.Windows.Forms.TextBox
        Me.txtReceived = New System.Windows.Forms.TextBox
        Me.lblReQ = New System.Windows.Forms.Label
        Me.lblE1 = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.lblComqty = New System.Windows.Forms.Label
        Me.lblRejQ = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Panel7 = New System.Windows.Forms.Panel
        Me.butEXIT = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.lblnoofOP = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.lblOperatorid = New System.Windows.Forms.Label
        Me.lblMachineId = New System.Windows.Forms.Label
        Me.lblWorkcenter = New System.Windows.Forms.Label
        Me.lblOperationno = New System.Windows.Forms.Label
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.ckbRework = New System.Windows.Forms.CheckBox
        Me.lstJobNo = New System.Windows.Forms.ListBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.lstJobRoute = New System.Windows.Forms.ListBox
        Me.Panel8 = New System.Windows.Forms.Panel
        Me.ButPrintHis = New System.Windows.Forms.Button
        Me.ButSearch = New System.Windows.Forms.Button
        Me.Label36 = New System.Windows.Forms.Label
        Me.txtJobNo = New System.Windows.Forms.TextBox
        Me.Panel9 = New System.Windows.Forms.Panel
        Me.lblItemcode = New System.Windows.Forms.Label
        Me.lblACRlQ = New System.Windows.Forms.Label
        Me.lblJobDate = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.lblDesc = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.lbl1 = New System.Windows.Forms.Label
        Me.lbl3 = New System.Windows.Forms.Label
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.pl.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.ForeColor = System.Drawing.Color.Black
        Me.Panel1.Location = New System.Drawing.Point(26, 2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(832, 671)
        Me.Panel1.TabIndex = 6
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.lblTot)
        Me.Panel2.Controls.Add(Me.txtTot)
        Me.Panel2.Controls.Add(Me.butAdd)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.lstv)
        Me.Panel2.Controls.Add(Me.pl)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Location = New System.Drawing.Point(4, 5)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(824, 661)
        Me.Panel2.TabIndex = 0
        '
        'lblTot
        '
        Me.lblTot.AutoSize = True
        Me.lblTot.Location = New System.Drawing.Point(19, 646)
        Me.lblTot.Name = "lblTot"
        Me.lblTot.Size = New System.Drawing.Size(37, 13)
        Me.lblTot.TabIndex = 122
        Me.lblTot.Text = "Total :"
        '
        'txtTot
        '
        Me.txtTot.AutoSize = True
        Me.txtTot.Location = New System.Drawing.Point(70, 705)
        Me.txtTot.Name = "txtTot"
        Me.txtTot.Size = New System.Drawing.Size(0, 13)
        Me.txtTot.TabIndex = 121
        '
        'butAdd
        '
        Me.butAdd.BackColor = System.Drawing.Color.Gray
        Me.butAdd.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butAdd.ForeColor = System.Drawing.Color.White
        Me.butAdd.Location = New System.Drawing.Point(97, 524)
        Me.butAdd.Name = "butAdd"
        Me.butAdd.Size = New System.Drawing.Size(52, 22)
        Me.butAdd.TabIndex = 120
        Me.butAdd.Text = "ADD"
        Me.butAdd.UseVisualStyleBackColor = False
        Me.butAdd.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(16, 529)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(71, 13)
        Me.Label9.TabIndex = 38
        Me.Label9.Text = "REJECTED"
        '
        'lstv
        '
        Me.lstv.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.lstv.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(16, 548)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(792, 95)
        Me.lstv.TabIndex = 9
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'pl
        '
        Me.pl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.pl.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pl.Controls.Add(Me.Label11)
        Me.pl.Controls.Add(Me.cmbName)
        Me.pl.Controls.Add(Me.txtRemarks)
        Me.pl.Controls.Add(Me.Label10)
        Me.pl.Controls.Add(Me.butPrint)
        Me.pl.Controls.Add(Me.butDelete)
        Me.pl.Controls.Add(Me.txtsuf)
        Me.pl.Controls.Add(Me.butUpdate)
        Me.pl.Controls.Add(Me.txtOperatorName)
        Me.pl.Controls.Add(Me.txtParent)
        Me.pl.Controls.Add(Me.txtIDNo)
        Me.pl.Controls.Add(Me.txtExport)
        Me.pl.Controls.Add(Me.lbReWork)
        Me.pl.Controls.Add(Me.lblExport)
        Me.pl.Controls.Add(Me.cmbOPerator)
        Me.pl.Controls.Add(Me.lblStatus)
        Me.pl.Controls.Add(Me.lblss)
        Me.pl.Controls.Add(Me.txtNoofOperator)
        Me.pl.Controls.Add(Me.cmbMachine)
        Me.pl.Controls.Add(Me.txtWorkCenter)
        Me.pl.Controls.Add(Me.txtOPNo)
        Me.pl.Controls.Add(Me.cmbAction)
        Me.pl.Controls.Add(Me.Label4)
        Me.pl.Controls.Add(Me.txtOPRlq)
        Me.pl.Controls.Add(Me.Panel10)
        Me.pl.Controls.Add(Me.Label3)
        Me.pl.Controls.Add(Me.Panel7)
        Me.pl.Controls.Add(Me.lblnoofOP)
        Me.pl.Controls.Add(Me.lblName)
        Me.pl.Controls.Add(Me.lblOperatorid)
        Me.pl.Controls.Add(Me.lblMachineId)
        Me.pl.Controls.Add(Me.lblWorkcenter)
        Me.pl.Controls.Add(Me.lblOperationno)
        Me.pl.Controls.Add(Me.Panel5)
        Me.pl.Controls.Add(Me.Panel8)
        Me.pl.Controls.Add(Me.Panel9)
        Me.pl.Controls.Add(Me.Panel6)
        Me.pl.Location = New System.Drawing.Point(0, 32)
        Me.pl.Name = "pl"
        Me.pl.Size = New System.Drawing.Size(829, 494)
        Me.pl.TabIndex = 37
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(253, 232)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(42, 13)
        Me.Label11.TabIndex = 155
        Me.Label11.Text = "NAME"
        '
        'cmbName
        '
        Me.cmbName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbName.FormattingEnabled = True
        Me.cmbName.Location = New System.Drawing.Point(398, 227)
        Me.cmbName.Name = "cmbName"
        Me.cmbName.Size = New System.Drawing.Size(249, 21)
        Me.cmbName.TabIndex = 154
        '
        'txtRemarks
        '
        Me.txtRemarks.Location = New System.Drawing.Point(398, 281)
        Me.txtRemarks.MaxLength = 50
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.Size = New System.Drawing.Size(323, 20)
        Me.txtRemarks.TabIndex = 153
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(253, 285)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(67, 13)
        Me.Label10.TabIndex = 152
        Me.Label10.Text = "REMARKS"
        '
        'butPrint
        '
        Me.butPrint.BackColor = System.Drawing.Color.Gray
        Me.butPrint.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butPrint.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butPrint.ForeColor = System.Drawing.Color.White
        Me.butPrint.Location = New System.Drawing.Point(507, 175)
        Me.butPrint.Name = "butPrint"
        Me.butPrint.Size = New System.Drawing.Size(74, 22)
        Me.butPrint.TabIndex = 151
        Me.butPrint.Text = "PRINT"
        Me.butPrint.UseVisualStyleBackColor = False
        Me.butPrint.Visible = False
        '
        'butDelete
        '
        Me.butDelete.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butDelete.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butDelete.Enabled = False
        Me.butDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butDelete.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelete.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butDelete.Image = CType(resources.GetObject("butDelete.Image"), System.Drawing.Image)
        Me.butDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butDelete.Location = New System.Drawing.Point(783, 290)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(48, 59)
        Me.butDelete.TabIndex = 18
        Me.butDelete.Text = "Delete"
        Me.butDelete.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butDelete.UseVisualStyleBackColor = False
        Me.butDelete.Visible = False
        '
        'txtsuf
        '
        Me.txtsuf.Location = New System.Drawing.Point(607, 255)
        Me.txtsuf.Name = "txtsuf"
        Me.txtsuf.Size = New System.Drawing.Size(100, 20)
        Me.txtsuf.TabIndex = 150
        Me.txtsuf.TabStop = False
        Me.txtsuf.Visible = False
        '
        'butUpdate
        '
        Me.butUpdate.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butUpdate.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butUpdate.Enabled = False
        Me.butUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butUpdate.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butUpdate.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butUpdate.Image = CType(resources.GetObject("butUpdate.Image"), System.Drawing.Image)
        Me.butUpdate.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butUpdate.Location = New System.Drawing.Point(730, 284)
        Me.butUpdate.Name = "butUpdate"
        Me.butUpdate.Size = New System.Drawing.Size(48, 59)
        Me.butUpdate.TabIndex = 17
        Me.butUpdate.Text = "Update"
        Me.butUpdate.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butUpdate.UseVisualStyleBackColor = False
        Me.butUpdate.Visible = False
        '
        'txtOperatorName
        '
        Me.txtOperatorName.Location = New System.Drawing.Point(785, 255)
        Me.txtOperatorName.Name = "txtOperatorName"
        Me.txtOperatorName.Size = New System.Drawing.Size(26, 20)
        Me.txtOperatorName.TabIndex = 138
        Me.txtOperatorName.TabStop = False
        Me.txtOperatorName.Visible = False
        '
        'txtParent
        '
        Me.txtParent.AutoSize = True
        Me.txtParent.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtParent.ForeColor = System.Drawing.Color.Black
        Me.txtParent.Location = New System.Drawing.Point(581, 267)
        Me.txtParent.Name = "txtParent"
        Me.txtParent.Size = New System.Drawing.Size(0, 19)
        Me.txtParent.TabIndex = 149
        Me.txtParent.Visible = False
        '
        'txtIDNo
        '
        Me.txtIDNo.Location = New System.Drawing.Point(472, 255)
        Me.txtIDNo.Name = "txtIDNo"
        Me.txtIDNo.ReadOnly = True
        Me.txtIDNo.Size = New System.Drawing.Size(103, 20)
        Me.txtIDNo.TabIndex = 148
        Me.txtIDNo.TabStop = False
        Me.txtIDNo.Visible = False
        '
        'txtExport
        '
        Me.txtExport.AutoSize = True
        Me.txtExport.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExport.Location = New System.Drawing.Point(668, 336)
        Me.txtExport.Name = "txtExport"
        Me.txtExport.Size = New System.Drawing.Size(0, 13)
        Me.txtExport.TabIndex = 146
        Me.txtExport.Visible = False
        '
        'lbReWork
        '
        Me.lbReWork.AutoSize = True
        Me.lbReWork.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbReWork.ForeColor = System.Drawing.Color.Red
        Me.lbReWork.Location = New System.Drawing.Point(462, 336)
        Me.lbReWork.Name = "lbReWork"
        Me.lbReWork.Size = New System.Drawing.Size(62, 13)
        Me.lbReWork.TabIndex = 145
        Me.lbReWork.Text = "REWORK"
        Me.lbReWork.Visible = False
        '
        'lblExport
        '
        Me.lblExport.AutoSize = True
        Me.lblExport.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExport.Location = New System.Drawing.Point(549, 336)
        Me.lblExport.Name = "lblExport"
        Me.lblExport.Size = New System.Drawing.Size(118, 13)
        Me.lblExport.TabIndex = 144
        Me.lblExport.Text = "EXPORT STATUS :"
        Me.lblExport.Visible = False
        '
        'cmbOPerator
        '
        Me.cmbOPerator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOPerator.FormattingEnabled = True
        Me.cmbOPerator.Location = New System.Drawing.Point(702, 224)
        Me.cmbOPerator.Name = "cmbOPerator"
        Me.cmbOPerator.Size = New System.Drawing.Size(109, 21)
        Me.cmbOPerator.TabIndex = 5
        Me.cmbOPerator.Visible = False
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.Red
        Me.lblStatus.Location = New System.Drawing.Point(374, 336)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(0, 13)
        Me.lblStatus.TabIndex = 142
        '
        'lblss
        '
        Me.lblss.AutoSize = True
        Me.lblss.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblss.Location = New System.Drawing.Point(253, 336)
        Me.lblss.Name = "lblss"
        Me.lblss.Size = New System.Drawing.Size(56, 13)
        Me.lblss.TabIndex = 141
        Me.lblss.Text = "STATUS"
        '
        'txtNoofOperator
        '
        Me.txtNoofOperator.Location = New System.Drawing.Point(398, 255)
        Me.txtNoofOperator.Name = "txtNoofOperator"
        Me.txtNoofOperator.Size = New System.Drawing.Size(41, 20)
        Me.txtNoofOperator.TabIndex = 7
        '
        'cmbMachine
        '
        Me.cmbMachine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMachine.FormattingEnabled = True
        Me.cmbMachine.Location = New System.Drawing.Point(398, 202)
        Me.cmbMachine.Name = "cmbMachine"
        Me.cmbMachine.Size = New System.Drawing.Size(109, 21)
        Me.cmbMachine.TabIndex = 4
        '
        'txtWorkCenter
        '
        Me.txtWorkCenter.Location = New System.Drawing.Point(607, 139)
        Me.txtWorkCenter.Name = "txtWorkCenter"
        Me.txtWorkCenter.Size = New System.Drawing.Size(171, 20)
        Me.txtWorkCenter.TabIndex = 135
        Me.txtWorkCenter.TabStop = False
        '
        'txtOPNo
        '
        Me.txtOPNo.Location = New System.Drawing.Point(398, 140)
        Me.txtOPNo.Name = "txtOPNo"
        Me.txtOPNo.Size = New System.Drawing.Size(100, 20)
        Me.txtOPNo.TabIndex = 134
        Me.txtOPNo.TabStop = False
        '
        'cmbAction
        '
        Me.cmbAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAction.FormattingEnabled = True
        Me.cmbAction.Items.AddRange(New Object() {"-Select-", "COMPLETED", "HANDOVER", "PAUSE", "RESUME", "REWORK", "SKIP", "SPLIT", "START"})
        Me.cmbAction.Location = New System.Drawing.Point(702, 364)
        Me.cmbAction.Name = "cmbAction"
        Me.cmbAction.Size = New System.Drawing.Size(112, 21)
        Me.cmbAction.TabIndex = 15
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Red
        Me.Label4.Location = New System.Drawing.Point(724, 346)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 13)
        Me.Label4.TabIndex = 132
        Me.Label4.Text = "ACTION"
        '
        'txtOPRlq
        '
        Me.txtOPRlq.Location = New System.Drawing.Point(398, 176)
        Me.txtOPRlq.Name = "txtOPRlq"
        Me.txtOPRlq.Size = New System.Drawing.Size(103, 20)
        Me.txtOPRlq.TabIndex = 140
        Me.txtOPRlq.TabStop = False
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.Beige
        Me.Panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel10.Controls.Add(Me.dtpEnd)
        Me.Panel10.Controls.Add(Me.dtPStart)
        Me.Panel10.Controls.Add(Me.Label7)
        Me.Panel10.Controls.Add(Me.txtEnd)
        Me.Panel10.Controls.Add(Me.txtRejected)
        Me.Panel10.Controls.Add(Me.txtCompleted)
        Me.Panel10.Controls.Add(Me.txtStart)
        Me.Panel10.Controls.Add(Me.txtReceived)
        Me.Panel10.Controls.Add(Me.lblReQ)
        Me.Panel10.Controls.Add(Me.lblE1)
        Me.Panel10.Controls.Add(Me.lblStartDate)
        Me.Panel10.Controls.Add(Me.lblComqty)
        Me.Panel10.Controls.Add(Me.lblRejQ)
        Me.Panel10.Controls.Add(Me.Label8)
        Me.Panel10.Location = New System.Drawing.Point(258, 362)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(438, 125)
        Me.Panel10.TabIndex = 131
        Me.Panel10.TabStop = True
        '
        'dtpEnd
        '
        Me.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpEnd.Location = New System.Drawing.Point(274, 98)
        Me.dtpEnd.Name = "dtpEnd"
        Me.dtpEnd.Size = New System.Drawing.Size(15, 20)
        Me.dtpEnd.TabIndex = 14
        Me.dtpEnd.Value = New Date(2009, 4, 17, 11, 34, 0, 0)
        '
        'dtPStart
        '
        Me.dtPStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtPStart.Location = New System.Drawing.Point(274, 75)
        Me.dtPStart.Name = "dtPStart"
        Me.dtPStart.Size = New System.Drawing.Size(15, 20)
        Me.dtPStart.TabIndex = 12
        Me.dtPStart.Value = New Date(2009, 4, 17, 11, 34, 0, 0)
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Red
        Me.Label7.Location = New System.Drawing.Point(284, 79)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(143, 13)
        Me.Label7.TabIndex = 146
        Me.Label7.Text = "(DD/MM/YYYY HH:MM)"
        '
        'txtEnd
        '
        Me.txtEnd.Location = New System.Drawing.Point(152, 98)
        Me.txtEnd.Name = "txtEnd"
        Me.txtEnd.Size = New System.Drawing.Size(123, 20)
        Me.txtEnd.TabIndex = 13
        '
        'txtRejected
        '
        Me.txtRejected.Location = New System.Drawing.Point(152, 52)
        Me.txtRejected.Name = "txtRejected"
        Me.txtRejected.Size = New System.Drawing.Size(113, 20)
        Me.txtRejected.TabIndex = 10
        '
        'txtCompleted
        '
        Me.txtCompleted.Location = New System.Drawing.Point(152, 29)
        Me.txtCompleted.Name = "txtCompleted"
        Me.txtCompleted.Size = New System.Drawing.Size(113, 20)
        Me.txtCompleted.TabIndex = 9
        '
        'txtStart
        '
        Me.txtStart.Location = New System.Drawing.Point(152, 75)
        Me.txtStart.Name = "txtStart"
        Me.txtStart.Size = New System.Drawing.Size(123, 20)
        Me.txtStart.TabIndex = 11
        '
        'txtReceived
        '
        Me.txtReceived.Location = New System.Drawing.Point(152, 6)
        Me.txtReceived.Name = "txtReceived"
        Me.txtReceived.Size = New System.Drawing.Size(113, 20)
        Me.txtReceived.TabIndex = 8
        '
        'lblReQ
        '
        Me.lblReQ.AutoSize = True
        Me.lblReQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReQ.Location = New System.Drawing.Point(3, 10)
        Me.lblReQ.Name = "lblReQ"
        Me.lblReQ.Size = New System.Drawing.Size(102, 13)
        Me.lblReQ.TabIndex = 46
        Me.lblReQ.Text = "RECEIVED QTY "
        '
        'lblE1
        '
        Me.lblE1.AutoSize = True
        Me.lblE1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblE1.Location = New System.Drawing.Point(3, 102)
        Me.lblE1.Name = "lblE1"
        Me.lblE1.Size = New System.Drawing.Size(110, 13)
        Me.lblE1.TabIndex = 56
        Me.lblE1.Text = "END DATE/TIME "
        '
        'lblStartDate
        '
        Me.lblStartDate.AutoSize = True
        Me.lblStartDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(3, 79)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(125, 13)
        Me.lblStartDate.TabIndex = 54
        Me.lblStartDate.Text = "START DATE/TIME "
        '
        'lblComqty
        '
        Me.lblComqty.AutoSize = True
        Me.lblComqty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComqty.Location = New System.Drawing.Point(3, 33)
        Me.lblComqty.Name = "lblComqty"
        Me.lblComqty.Size = New System.Drawing.Size(115, 13)
        Me.lblComqty.TabIndex = 55
        Me.lblComqty.Text = "COMPLETED QTY "
        '
        'lblRejQ
        '
        Me.lblRejQ.AutoSize = True
        Me.lblRejQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRejQ.Location = New System.Drawing.Point(3, 56)
        Me.lblRejQ.Name = "lblRejQ"
        Me.lblRejQ.Size = New System.Drawing.Size(104, 13)
        Me.lblRejQ.TabIndex = 57
        Me.lblRejQ.Text = "REJECTED QTY "
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Red
        Me.Label8.Location = New System.Drawing.Point(284, 102)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(143, 13)
        Me.Label8.TabIndex = 149
        Me.Label8.Text = "(DD/MM/YYYY HH:MM)"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(253, 180)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(143, 13)
        Me.Label3.TabIndex = 67
        Me.Label3.Text = "RELEASED QUANTITY "
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Transparent
        Me.Panel7.Controls.Add(Me.butEXIT)
        Me.Panel7.Controls.Add(Me.butCancel)
        Me.Panel7.Controls.Add(Me.butSave)
        Me.Panel7.Location = New System.Drawing.Point(607, 175)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(193, 44)
        Me.Panel7.TabIndex = 67
        '
        'butEXIT
        '
        Me.butEXIT.BackColor = System.Drawing.Color.Gray
        Me.butEXIT.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butEXIT.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
        Me.butEXIT.ForeColor = System.Drawing.Color.White
        Me.butEXIT.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butEXIT.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butEXIT.Location = New System.Drawing.Point(126, 5)
        Me.butEXIT.Name = "butEXIT"
        Me.butEXIT.Size = New System.Drawing.Size(55, 29)
        Me.butEXIT.TabIndex = 58
        Me.butEXIT.Text = "Exit"
        Me.butEXIT.UseVisualStyleBackColor = False
        '
        'butCancel
        '
        Me.butCancel.BackColor = System.Drawing.Color.Gray
        Me.butCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butCancel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
        Me.butCancel.ForeColor = System.Drawing.Color.White
        Me.butCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(67, 5)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(55, 29)
        Me.butCancel.TabIndex = 19
        Me.butCancel.Text = "Cancel"
        Me.butCancel.UseVisualStyleBackColor = False
        '
        'butSave
        '
        Me.butSave.BackColor = System.Drawing.Color.Gray
        Me.butSave.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butSave.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
        Me.butSave.ForeColor = System.Drawing.Color.White
        Me.butSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(8, 5)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(55, 29)
        Me.butSave.TabIndex = 16
        Me.butSave.Text = "Save"
        Me.butSave.UseVisualStyleBackColor = False
        '
        'lblnoofOP
        '
        Me.lblnoofOP.AutoSize = True
        Me.lblnoofOP.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblnoofOP.Location = New System.Drawing.Point(253, 259)
        Me.lblnoofOP.Name = "lblnoofOP"
        Me.lblnoofOP.Size = New System.Drawing.Size(125, 13)
        Me.lblnoofOP.TabIndex = 130
        Me.lblnoofOP.Text = "NO. OF OPERATOR "
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(702, 441)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(118, 13)
        Me.lblName.TabIndex = 129
        Me.lblName.Text = "OPERATOR NAME "
        Me.lblName.Visible = False
        '
        'lblOperatorid
        '
        Me.lblOperatorid.AutoSize = True
        Me.lblOperatorid.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperatorid.Location = New System.Drawing.Point(653, 230)
        Me.lblOperatorid.Name = "lblOperatorid"
        Me.lblOperatorid.Size = New System.Drawing.Size(96, 13)
        Me.lblOperatorid.TabIndex = 128
        Me.lblOperatorid.Text = "OPERATOR ID "
        Me.lblOperatorid.Visible = False
        '
        'lblMachineId
        '
        Me.lblMachineId.AutoSize = True
        Me.lblMachineId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMachineId.Location = New System.Drawing.Point(253, 206)
        Me.lblMachineId.Name = "lblMachineId"
        Me.lblMachineId.Size = New System.Drawing.Size(84, 13)
        Me.lblMachineId.TabIndex = 127
        Me.lblMachineId.Text = "MACHINE ID "
        '
        'lblWorkcenter
        '
        Me.lblWorkcenter.AutoSize = True
        Me.lblWorkcenter.BackColor = System.Drawing.Color.Gainsboro
        Me.lblWorkcenter.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkcenter.Location = New System.Drawing.Point(501, 143)
        Me.lblWorkcenter.Name = "lblWorkcenter"
        Me.lblWorkcenter.Size = New System.Drawing.Size(103, 13)
        Me.lblWorkcenter.TabIndex = 126
        Me.lblWorkcenter.Text = "WORK CENTER "
        '
        'lblOperationno
        '
        Me.lblOperationno.AutoSize = True
        Me.lblOperationno.BackColor = System.Drawing.Color.Gainsboro
        Me.lblOperationno.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperationno.Location = New System.Drawing.Point(255, 143)
        Me.lblOperationno.Name = "lblOperationno"
        Me.lblOperationno.Size = New System.Drawing.Size(109, 13)
        Me.lblOperationno.TabIndex = 70
        Me.lblOperationno.Text = "OPERATION NO. "
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel5.Controls.Add(Me.ckbRework)
        Me.Panel5.Controls.Add(Me.lstJobNo)
        Me.Panel5.Controls.Add(Me.Label2)
        Me.Panel5.Controls.Add(Me.lstJobRoute)
        Me.Panel5.Location = New System.Drawing.Point(11, 127)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(235, 360)
        Me.Panel5.TabIndex = 39
        '
        'ckbRework
        '
        Me.ckbRework.AutoSize = True
        Me.ckbRework.Location = New System.Drawing.Point(86, 336)
        Me.ckbRework.Name = "ckbRework"
        Me.ckbRework.Size = New System.Drawing.Size(58, 17)
        Me.ckbRework.TabIndex = 151
        Me.ckbRework.Text = "Master"
        Me.ckbRework.UseVisualStyleBackColor = True
        '
        'lstJobNo
        '
        Me.lstJobNo.FormattingEnabled = True
        Me.lstJobNo.Location = New System.Drawing.Point(86, 17)
        Me.lstJobNo.Name = "lstJobNo"
        Me.lstJobNo.Size = New System.Drawing.Size(139, 316)
        Me.lstJobNo.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label2.Location = New System.Drawing.Point(3, 2)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 13)
        Me.Label2.TabIndex = 74
        Me.Label2.Text = "Operation No."
        '
        'lstJobRoute
        '
        Me.lstJobRoute.FormattingEnabled = True
        Me.lstJobRoute.Location = New System.Drawing.Point(7, 18)
        Me.lstJobRoute.Name = "lstJobRoute"
        Me.lstJobRoute.Size = New System.Drawing.Size(75, 316)
        Me.lstJobRoute.TabIndex = 1
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.Gold
        Me.Panel8.Controls.Add(Me.ButPrintHis)
        Me.Panel8.Controls.Add(Me.ButSearch)
        Me.Panel8.Controls.Add(Me.Label36)
        Me.Panel8.Controls.Add(Me.txtJobNo)
        Me.Panel8.Location = New System.Drawing.Point(0, 2)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(364, 120)
        Me.Panel8.TabIndex = 123
        Me.Panel8.TabStop = True
        '
        'ButPrintHis
        '
        Me.ButPrintHis.BackColor = System.Drawing.Color.Gray
        Me.ButPrintHis.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButPrintHis.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButPrintHis.ForeColor = System.Drawing.Color.White
        Me.ButPrintHis.Location = New System.Drawing.Point(212, 71)
        Me.ButPrintHis.Name = "ButPrintHis"
        Me.ButPrintHis.Size = New System.Drawing.Size(148, 22)
        Me.ButPrintHis.TabIndex = 156
        Me.ButPrintHis.Text = "PRINT TRANSACTION"
        Me.ButPrintHis.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ButPrintHis.UseVisualStyleBackColor = False
        Me.ButPrintHis.Visible = False
        '
        'ButSearch
        '
        Me.ButSearch.BackColor = System.Drawing.Color.Gray
        Me.ButSearch.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButSearch.ForeColor = System.Drawing.Color.White
        Me.ButSearch.Location = New System.Drawing.Point(212, 42)
        Me.ButSearch.Name = "ButSearch"
        Me.ButSearch.Size = New System.Drawing.Size(78, 23)
        Me.ButSearch.TabIndex = 119
        Me.ButSearch.Text = "SEARCH"
        Me.ButSearch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ButSearch.UseVisualStyleBackColor = False
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label36.Location = New System.Drawing.Point(4, 46)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(47, 13)
        Me.Label36.TabIndex = 73
        Me.Label36.Text = "Job No"
        '
        'txtJobNo
        '
        Me.txtJobNo.BackColor = System.Drawing.Color.White
        Me.txtJobNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobNo.Location = New System.Drawing.Point(51, 40)
        Me.txtJobNo.MaxLength = 50
        Me.txtJobNo.Name = "txtJobNo"
        Me.txtJobNo.Size = New System.Drawing.Size(158, 26)
        Me.txtJobNo.TabIndex = 0
        Me.txtJobNo.TabStop = False
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.White
        Me.Panel9.Controls.Add(Me.lblItemcode)
        Me.Panel9.Controls.Add(Me.lblACRlQ)
        Me.Panel9.Controls.Add(Me.lblJobDate)
        Me.Panel9.Controls.Add(Me.Label6)
        Me.Panel9.Controls.Add(Me.lblDesc)
        Me.Panel9.Controls.Add(Me.Label5)
        Me.Panel9.Controls.Add(Me.lbl1)
        Me.Panel9.Controls.Add(Me.lbl3)
        Me.Panel9.Location = New System.Drawing.Point(341, 2)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(473, 120)
        Me.Panel9.TabIndex = 125
        Me.Panel9.TabStop = True
        '
        'lblItemcode
        '
        Me.lblItemcode.AutoEllipsis = True
        Me.lblItemcode.AutoSize = True
        Me.lblItemcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItemcode.Location = New System.Drawing.Point(130, 7)
        Me.lblItemcode.Name = "lblItemcode"
        Me.lblItemcode.Size = New System.Drawing.Size(0, 13)
        Me.lblItemcode.TabIndex = 75
        '
        'lblACRlQ
        '
        Me.lblACRlQ.AutoSize = True
        Me.lblACRlQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblACRlQ.Location = New System.Drawing.Point(168, 69)
        Me.lblACRlQ.Name = "lblACRlQ"
        Me.lblACRlQ.Size = New System.Drawing.Size(0, 13)
        Me.lblACRlQ.TabIndex = 74
        '
        'lblJobDate
        '
        Me.lblJobDate.AutoSize = True
        Me.lblJobDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobDate.Location = New System.Drawing.Point(107, 96)
        Me.lblJobDate.Name = "lblJobDate"
        Me.lblJobDate.Size = New System.Drawing.Size(0, 13)
        Me.lblJobDate.TabIndex = 73
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(25, 96)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 13)
        Me.Label6.TabIndex = 72
        Me.Label6.Text = "JOB DATE :"
        '
        'lblDesc
        '
        Me.lblDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDesc.Location = New System.Drawing.Point(130, 32)
        Me.lblDesc.Name = "lblDesc"
        Me.lblDesc.Size = New System.Drawing.Size(329, 33)
        Me.lblDesc.TabIndex = 71
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(25, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(99, 13)
        Me.Label5.TabIndex = 70
        Me.Label5.Text = "DESCRIPTION :"
        '
        'lbl1
        '
        Me.lbl1.AutoSize = True
        Me.lbl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl1.Location = New System.Drawing.Point(25, 69)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(147, 13)
        Me.lbl1.TabIndex = 68
        Me.lbl1.Text = "RELEASED QUANTITY :"
        '
        'lbl3
        '
        Me.lbl3.AutoSize = True
        Me.lbl3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl3.Location = New System.Drawing.Point(25, 7)
        Me.lbl3.Name = "lbl3"
        Me.lbl3.Size = New System.Drawing.Size(87, 13)
        Me.lbl3.TabIndex = 69
        Me.lbl3.Text = "ITEM  CODE :"
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel6.Location = New System.Drawing.Point(252, 129)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(548, 36)
        Me.Panel6.TabIndex = 147
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Khaki
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Location = New System.Drawing.Point(2, 2)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(822, 31)
        Me.Panel3.TabIndex = 36
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(133, 13)
        Me.Label1.TabIndex = 37
        Me.Label1.Text = "EDIT OPERATION DATA "
        '
        'frmSupJobEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Ivory
        Me.ClientSize = New System.Drawing.Size(884, 678)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmSupJobEdit"
        Me.ShowInTaskbar = False
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.pl.ResumeLayout(False)
        Me.pl.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents pl As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtJobNo As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butDelete As System.Windows.Forms.Button
    Friend WithEvents butUpdate As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents lstJobRoute As System.Windows.Forms.ListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents lstJobNo As System.Windows.Forms.ListBox
    Friend WithEvents lbl1 As System.Windows.Forms.Label
    Friend WithEvents lbl3 As System.Windows.Forms.Label
    Friend WithEvents lblOperationno As System.Windows.Forms.Label
    Friend WithEvents lblWorkcenter As System.Windows.Forms.Label
    Friend WithEvents lblMachineId As System.Windows.Forms.Label
    Friend WithEvents lblOperatorid As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblnoofOP As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblReQ As System.Windows.Forms.Label
    Friend WithEvents lblE1 As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents lblComqty As System.Windows.Forms.Label
    Friend WithEvents lblRejQ As System.Windows.Forms.Label
    Friend WithEvents cmbAction As System.Windows.Forms.ComboBox
    Friend WithEvents lblDesc As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblACRlQ As System.Windows.Forms.Label
    Friend WithEvents lblJobDate As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblItemcode As System.Windows.Forms.Label
    Friend WithEvents cmbMachine As System.Windows.Forms.ComboBox
    Friend WithEvents txtWorkCenter As System.Windows.Forms.TextBox
    Friend WithEvents txtOPNo As System.Windows.Forms.TextBox
    Friend WithEvents txtNoofOperator As System.Windows.Forms.TextBox
    Friend WithEvents txtOperatorName As System.Windows.Forms.TextBox
    Friend WithEvents txtCompleted As System.Windows.Forms.TextBox
    Friend WithEvents txtStart As System.Windows.Forms.TextBox
    Friend WithEvents txtReceived As System.Windows.Forms.TextBox
    Friend WithEvents txtOPRlq As System.Windows.Forms.TextBox
    Friend WithEvents dtPStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtEnd As System.Windows.Forms.TextBox
    Friend WithEvents txtRejected As System.Windows.Forms.TextBox
    Friend WithEvents ButSearch As System.Windows.Forms.Button
    Friend WithEvents dtpEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblss As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cmbOPerator As System.Windows.Forms.ComboBox
    Friend WithEvents lblExport As System.Windows.Forms.Label
    Friend WithEvents lbReWork As System.Windows.Forms.Label
    Friend WithEvents txtExport As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents lstv As System.Windows.Forms.ListView
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents butAdd As System.Windows.Forms.Button
    Friend WithEvents txtIDNo As System.Windows.Forms.TextBox
    Friend WithEvents txtParent As System.Windows.Forms.Label
    Friend WithEvents txtsuf As System.Windows.Forms.TextBox
    Friend WithEvents ckbRework As System.Windows.Forms.CheckBox
    Friend WithEvents butPrint As System.Windows.Forms.Button
    Friend WithEvents lblTot As System.Windows.Forms.Label
    Friend WithEvents txtTot As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents butEXIT As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cmbName As System.Windows.Forms.ComboBox
    Friend WithEvents ButPrintHis As System.Windows.Forms.Button
End Class
