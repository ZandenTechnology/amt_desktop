<[Global].Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
[Partial] Class frmViewJobDetails
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components [IsNot] Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewJobDetails))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.butClose = New System.Windows.Forms.Button
        Me.lblitem = New System.Windows.Forms.Label
        Me.lbldate = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.lblqtyscrapped = New System.Windows.Forms.Label
        Me.lblqtycompleted = New System.Windows.Forms.Label
        Me.lblqtyreleased = New System.Windows.Forms.Label
        Me.lblsuffix = New System.Windows.Forms.Label
        Me.lbljobno = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.lstv = New System.Windows.Forms.ListView
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Silver
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(-2, 1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(741, 443)
        Me.Panel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(14, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Job Details"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.Controls.Add(Me.butClose)
        Me.Panel2.Controls.Add(Me.lblitem)
        Me.Panel2.Controls.Add(Me.lbldate)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.lblqtyscrapped)
        Me.Panel2.Controls.Add(Me.lblqtycompleted)
        Me.Panel2.Controls.Add(Me.lblqtyreleased)
        Me.Panel2.Controls.Add(Me.lblsuffix)
        Me.Panel2.Controls.Add(Me.lbljobno)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Location = New System.Drawing.Point(14, 25)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(715, 404)
        Me.Panel2.TabIndex = 0
        '
        'butClose
        '
        Me.butClose.Location = New System.Drawing.Point(19, 377)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(48, 23)
        Me.butClose.TabIndex = 15
        Me.butClose.Text = "Close"
        Me.butClose.UseVisualStyleBackColor = True
        '
        'lblitem
        '
        Me.lblitem.AutoSize = True
        Me.lblitem.Location = New System.Drawing.Point(588, 12)
        Me.lblitem.Name = "lblitem"
        Me.lblitem.Size = New System.Drawing.Size(45, 13)
        Me.lblitem.TabIndex = 14
        Me.lblitem.Text = "Label10"
        '
        'lbldate
        '
        Me.lbldate.AutoSize = True
        Me.lbldate.Location = New System.Drawing.Point(76, 62)
        Me.lbldate.Name = "lbldate"
        Me.lbldate.Size = New System.Drawing.Size(38, 13)
        Me.lbldate.TabIndex = 13
        Me.lbldate.Text = "lbldate"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(488, 11)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(38, 14)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "Item :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(23, 61)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(37, 14)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Date :"
        '
        'lblqtyscrapped
        '
        Me.lblqtyscrapped.AutoSize = True
        Me.lblqtyscrapped.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblqtyscrapped.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblqtyscrapped.Location = New System.Drawing.Point(588, 84)
        Me.lblqtyscrapped.Name = "lblqtyscrapped"
        Me.lblqtyscrapped.Size = New System.Drawing.Size(45, 14)
        Me.lblqtyscrapped.TabIndex = 10
        Me.lblqtyscrapped.Text = "Label11"
        Me.lblqtyscrapped.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'lblqtycompleted
        '
        Me.lblqtycompleted.AutoSize = True
        Me.lblqtycompleted.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblqtycompleted.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblqtycompleted.Location = New System.Drawing.Point(588, 61)
        Me.lblqtycompleted.Name = "lblqtycompleted"
        Me.lblqtycompleted.Size = New System.Drawing.Size(45, 14)
        Me.lblqtycompleted.TabIndex = 9
        Me.lblqtycompleted.Text = "Label10"
        Me.lblqtycompleted.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'lblqtyreleased
        '
        Me.lblqtyreleased.AutoSize = True
        Me.lblqtyreleased.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblqtyreleased.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblqtyreleased.Location = New System.Drawing.Point(588, 34)
        Me.lblqtyreleased.Name = "lblqtyreleased"
        Me.lblqtyreleased.Size = New System.Drawing.Size(39, 14)
        Me.lblqtyreleased.TabIndex = 8
        Me.lblqtyreleased.Text = "Label9"
        Me.lblqtyreleased.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'lblsuffix
        '
        Me.lblsuffix.AutoSize = True
        Me.lblsuffix.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblsuffix.Location = New System.Drawing.Point(76, 38)
        Me.lblsuffix.Name = "lblsuffix"
        Me.lblsuffix.Size = New System.Drawing.Size(39, 14)
        Me.lblsuffix.TabIndex = 7
        Me.lblsuffix.Text = "Label8"
        '
        'lbljobno
        '
        Me.lbljobno.AutoSize = True
        Me.lbljobno.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbljobno.Location = New System.Drawing.Point(76, 11)
        Me.lbljobno.Name = "lbljobno"
        Me.lbljobno.Size = New System.Drawing.Size(13, 14)
        Me.lbljobno.TabIndex = 6
        Me.lbljobno.Text = "d"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(487, 84)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(86, 14)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Qty Scrapped :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(487, 61)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(95, 14)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Qty Completed :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(488, 34)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 14)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Qty Released :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(23, 38)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 14)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Suffix :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(23, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 14)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Job No :"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.LightGray
        Me.Panel3.Controls.Add(Me.lstv)
        Me.Panel3.Location = New System.Drawing.Point(19, 102)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(680, 272)
        Me.Panel3.TabIndex = 0
        '
        'lstv
        '
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(3, 12)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(670, 252)
        Me.lstv.TabIndex = 0
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'frmViewJobDetails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(739, 445)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmViewJobDetails"
        Me.Text = "View Job Details"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lstv As System.Windows.Forms.ListView
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblqtyscrapped As System.Windows.Forms.Label
    Friend WithEvents lblqtycompleted As System.Windows.Forms.Label
    Friend WithEvents lblqtyreleased As System.Windows.Forms.Label
    Friend WithEvents lblsuffix As System.Windows.Forms.Label
    Friend WithEvents lbljobno As System.Windows.Forms.Label
    Friend WithEvents lblitem As System.Windows.Forms.Label
    Friend WithEvents lbldate As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents butClose As System.Windows.Forms.Button
End Class
