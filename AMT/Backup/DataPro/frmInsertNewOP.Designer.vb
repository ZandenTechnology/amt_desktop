<[Global].Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
[Partial] Class frmInsertNewOP
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components [IsNot] Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.butEXIT = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtJob = New System.Windows.Forms.TextBox
        Me.txtOP = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtSufix = New System.Windows.Forms.ListBox
        Me.butFind = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'butEXIT
        '
        Me.butEXIT.BackColor = System.Drawing.Color.Gray
        Me.butEXIT.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butEXIT.ForeColor = System.Drawing.Color.White
        Me.butEXIT.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butEXIT.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butEXIT.Location = New System.Drawing.Point(384, 212)
        Me.butEXIT.Name = "butEXIT"
        Me.butEXIT.Size = New System.Drawing.Size(57, 24)
        Me.butEXIT.TabIndex = 121
        Me.butEXIT.Text = "Exit"
        Me.butEXIT.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(223, 83)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(297, 28)
        Me.Label1.TabIndex = 119
        Me.Label1.Text = "Insert New Operation No."
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(219, 141)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(85, 15)
        Me.Label7.TabIndex = 118
        Me.Label7.Text = "Job Order No."
        '
        'txtJob
        '
        Me.txtJob.BackColor = System.Drawing.Color.White
        Me.txtJob.Location = New System.Drawing.Point(312, 141)
        Me.txtJob.Name = "txtJob"
        Me.txtJob.Size = New System.Drawing.Size(155, 20)
        Me.txtJob.TabIndex = 117
        '
        'txtOP
        '
        Me.txtOP.BackColor = System.Drawing.Color.White
        Me.txtOP.Location = New System.Drawing.Point(312, 167)
        Me.txtOP.Name = "txtOP"
        Me.txtOP.Size = New System.Drawing.Size(66, 20)
        Me.txtOP.TabIndex = 122
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(219, 172)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(81, 15)
        Me.Label2.TabIndex = 123
        Me.Label2.Text = "Operation No"
        '
        'txtSufix
        '
        Me.txtSufix.FormattingEnabled = True
        Me.txtSufix.Location = New System.Drawing.Point(512, 141)
        Me.txtSufix.Name = "txtSufix"
        Me.txtSufix.Size = New System.Drawing.Size(120, 95)
        Me.txtSufix.TabIndex = 124
        '
        'butFind
        '
        Me.butFind.BackColor = System.Drawing.Color.Gray
        Me.butFind.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butFind.ForeColor = System.Drawing.Color.White
        Me.butFind.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butFind.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butFind.Location = New System.Drawing.Point(321, 212)
        Me.butFind.Name = "butFind"
        Me.butFind.Size = New System.Drawing.Size(57, 24)
        Me.butFind.TabIndex = 125
        Me.butFind.Text = "Update"
        Me.butFind.UseVisualStyleBackColor = False
        '
        'frmInsertNewOP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(654, 324)
        Me.Controls.Add(Me.butFind)
        Me.Controls.Add(Me.txtSufix)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtOP)
        Me.Controls.Add(Me.butEXIT)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtJob)
        Me.Name = "frmInsertNewOP"
        Me.Text = "frmInsertNewOP"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents butEXIT As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtJob As System.Windows.Forms.TextBox
    Friend WithEvents txtOP As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtSufix As System.Windows.Forms.ListBox
    Friend WithEvents butFind As System.Windows.Forms.Button
End Class
