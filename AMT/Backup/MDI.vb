
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.DateAndTime
Public Class MDI
    Inherits System.Windows.Forms.Form
    Dim selectForm
    Dim objOperator As frmOperative
    Dim objUser As frmUser
    Dim objRGrouping As frmResourceGrouping
    'Dim objWorkStation As frmWorkStation
    Dim objWorkStation As frmWCRCGroup
    'Dim objFailure As frmFailure
    Dim objRejGroup As frmWCRejectGrouping
    '  Dim objFailure As frmFailure
    Dim objRejected As frmRejectMaster
    Dim objView As frmViewJob
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    'By Yeo 20081110
    Dim objDownload As frmDownload
    Friend WithEvents PExport As System.Windows.Forms.Panel
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents butExport As System.Windows.Forms.Button
    'Dim objDataEntry As frmDataEntry
    Dim objDataEntry As frmSupJobEdit
    Dim objDataVerify As frmDataView
    Friend WithEvents butop As System.Windows.Forms.Button
    Dim objop As frmDataEntryOP
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents butMachineGroup As System.Windows.Forms.Button
    Friend WithEvents butMachine As System.Windows.Forms.Button
    Dim objTansDis As frmTransDis
    Dim objMachine As frmMachine
    Friend WithEvents butTansDis As System.Windows.Forms.Button
    Friend WithEvents butDataVerify As System.Windows.Forms.Button
    Dim objMachineWRK As frmMachineWRK
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents PlSuper As System.Windows.Forms.Panel
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents butRejectedGroup As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents butWC As System.Windows.Forms.Button
    Dim objExport As frmExportMain
    Friend WithEvents butItem As System.Windows.Forms.Button
    Dim objwc As New frmWorkCenter
    Friend WithEvents butActionControl As System.Windows.Forms.Button
    Dim objItem As New frmItem
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox

    Dim objOPstatus As New frmOperationStatus
    'Dim objExport As frmExport
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.

    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents PTop As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblLogin As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel

    Friend WithEvents ToolBarButton2 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton5 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton3 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton4 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton6 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton7 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBar2 As System.Windows.Forms.ToolBar
    Friend WithEvents Button1 As System.Windows.Forms.Button

    Friend WithEvents PData As System.Windows.Forms.Panel
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox

    Friend WithEvents PMain As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Pwait As System.Windows.Forms.Panel
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PSetup As System.Windows.Forms.Panel
    Friend WithEvents butOperator As System.Windows.Forms.Button
    Friend WithEvents butUser As System.Windows.Forms.Button
    Friend WithEvents butRGrouping As System.Windows.Forms.Button
    Friend WithEvents butWS As System.Windows.Forms.Button
    Friend WithEvents butRejected As System.Windows.Forms.Button
    Friend WithEvents PDownload As System.Windows.Forms.Panel
    Friend WithEvents butDounload As System.Windows.Forms.Button
    Friend WithEvents butDlinf As System.Windows.Forms.Button
    Friend WithEvents PdataEntry As System.Windows.Forms.Panel
    Friend WithEvents butDataEntry As System.Windows.Forms.Button
    Friend WithEvents PAdmin As System.Windows.Forms.Panel
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button



    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MDI))
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.ToolBar2 = New System.Windows.Forms.ToolBar
        Me.ToolBarButton2 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton5 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton4 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton3 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton6 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton7 = New System.Windows.Forms.ToolBarButton
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.PTop = New System.Windows.Forms.Panel
        Me.PictureBox4 = New System.Windows.Forms.PictureBox
        Me.Panel7 = New System.Windows.Forms.Panel
        Me.PictureBox3 = New System.Windows.Forms.PictureBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.PMain = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Pwait = New System.Windows.Forms.Panel
        Me.Label5 = New System.Windows.Forms.Label
        Me.PictureBox5 = New System.Windows.Forms.PictureBox
        Me.PData = New System.Windows.Forms.Panel
        Me.PictureBox6 = New System.Windows.Forms.PictureBox
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Button1 = New System.Windows.Forms.Button
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblLogin = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.PlSuper = New System.Windows.Forms.Panel
        Me.Button3 = New System.Windows.Forms.Button
        Me.PSetup = New System.Windows.Forms.Panel
        Me.butActionControl = New System.Windows.Forms.Button
        Me.butItem = New System.Windows.Forms.Button
        Me.butWC = New System.Windows.Forms.Button
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.butRejectedGroup = New System.Windows.Forms.Button
        Me.butMachineGroup = New System.Windows.Forms.Button
        Me.butMachine = New System.Windows.Forms.Button
        Me.butWS = New System.Windows.Forms.Button
        Me.butRGrouping = New System.Windows.Forms.Button
        Me.butRejected = New System.Windows.Forms.Button
        Me.butOperator = New System.Windows.Forms.Button
        Me.butUser = New System.Windows.Forms.Button
        Me.PDownload = New System.Windows.Forms.Panel
        Me.Button6 = New System.Windows.Forms.Button
        Me.butDlinf = New System.Windows.Forms.Button
        Me.butDounload = New System.Windows.Forms.Button
        Me.PAdmin = New System.Windows.Forms.Panel
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.PdataEntry = New System.Windows.Forms.Panel
        Me.butTansDis = New System.Windows.Forms.Button
        Me.butDataVerify = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.butop = New System.Windows.Forms.Button
        Me.butDataEntry = New System.Windows.Forms.Button
        Me.PExport = New System.Windows.Forms.Panel
        Me.Button2 = New System.Windows.Forms.Button
        Me.butExport = New System.Windows.Forms.Button
        Me.Panel4.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PTop.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.PMain.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Pwait.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PData.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel6.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PlSuper.SuspendLayout()
        Me.PSetup.SuspendLayout()
        Me.PDownload.SuspendLayout()
        Me.PAdmin.SuspendLayout()
        Me.PdataEntry.SuspendLayout()
        Me.PExport.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.Panel4.Controls.Add(Me.ToolBar2)
        Me.Panel4.Location = New System.Drawing.Point(-16, -4)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(698, 64)
        Me.Panel4.TabIndex = 11
        '
        'ToolBar2
        '
        Me.ToolBar2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ToolBar2.BackColor = System.Drawing.Color.Bisque
        Me.ToolBar2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ToolBar2.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButton2, Me.ToolBarButton1, Me.ToolBarButton5, Me.ToolBarButton4, Me.ToolBarButton3, Me.ToolBarButton6, Me.ToolBarButton7})
        Me.ToolBar2.ButtonSize = New System.Drawing.Size(50, 50)
        Me.ToolBar2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ToolBar2.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolBar2.DropDownArrows = True
        Me.ToolBar2.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolBar2.ImageList = Me.ImageList1
        Me.ToolBar2.Location = New System.Drawing.Point(19, 0)
        Me.ToolBar2.Name = "ToolBar2"
        Me.ToolBar2.ShowToolTips = True
        Me.ToolBar2.Size = New System.Drawing.Size(416, 64)
        Me.ToolBar2.TabIndex = 6
        '
        'ToolBarButton2
        '
        Me.ToolBarButton2.ImageIndex = 0
        Me.ToolBarButton2.Name = "ToolBarButton2"
        Me.ToolBarButton2.ToolTipText = "Setup"
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.ImageIndex = 1
        Me.ToolBarButton1.Name = "ToolBarButton1"
        Me.ToolBarButton1.ToolTipText = "Download Data From ERP"
        '
        'ToolBarButton5
        '
        Me.ToolBarButton5.ImageIndex = 2
        Me.ToolBarButton5.Name = "ToolBarButton5"
        Me.ToolBarButton5.ToolTipText = "Generate WIP Process File"
        '
        'ToolBarButton4
        '
        Me.ToolBarButton4.ImageIndex = 3
        Me.ToolBarButton4.Name = "ToolBarButton4"
        Me.ToolBarButton4.ToolTipText = "WIP Data"
        '
        'ToolBarButton3
        '
        Me.ToolBarButton3.ImageIndex = 4
        Me.ToolBarButton3.Name = "ToolBarButton3"
        Me.ToolBarButton3.ToolTipText = "Reports"
        '
        'ToolBarButton6
        '
        Me.ToolBarButton6.ImageIndex = 5
        Me.ToolBarButton6.Name = "ToolBarButton6"
        Me.ToolBarButton6.ToolTipText = "Log off"
        '
        'ToolBarButton7
        '
        Me.ToolBarButton7.ImageIndex = 6
        Me.ToolBarButton7.Name = "ToolBarButton7"
        Me.ToolBarButton7.ToolTipText = "Exit"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "setupFile.jpg")
        Me.ImageList1.Images.SetKeyName(1, "downloadFile.jpg")
        Me.ImageList1.Images.SetKeyName(2, "exitFile.jpg")
        Me.ImageList1.Images.SetKeyName(3, "WIPdata.jpg")
        Me.ImageList1.Images.SetKeyName(4, "reportfile.jpg")
        Me.ImageList1.Images.SetKeyName(5, "logoff.jpg")
        Me.ImageList1.Images.SetKeyName(6, "exit1234.jpg")
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(980, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(160, 55)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'PTop
        '
        Me.PTop.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.PTop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PTop.Controls.Add(Me.PictureBox4)
        Me.PTop.Controls.Add(Me.PictureBox1)
        Me.PTop.Controls.Add(Me.Panel7)
        Me.PTop.Controls.Add(Me.Panel4)
        Me.PTop.Controls.Add(Me.PictureBox3)
        Me.PTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.PTop.Location = New System.Drawing.Point(0, 0)
        Me.PTop.Name = "PTop"
        Me.PTop.Size = New System.Drawing.Size(1148, 64)
        Me.PTop.TabIndex = 9
        '
        'PictureBox4
        '
        Me.PictureBox4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox4.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(417, -2)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(561, 64)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 5
        Me.PictureBox4.TabStop = False
        '
        'Panel7
        '
        Me.Panel7.AutoSize = True
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel7.Location = New System.Drawing.Point(1144, 0)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(0, 60)
        Me.Panel7.TabIndex = 13
        '
        'PictureBox3
        '
        Me.PictureBox3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.PictureBox3.BackgroundImage = CType(resources.GetObject("PictureBox3.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(768, -2)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(378, 64)
        Me.PictureBox3.TabIndex = 12
        Me.PictureBox3.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.PMain)
        Me.Panel1.Controls.Add(Me.PData)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(0, 64)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1148, 684)
        Me.Panel1.TabIndex = 8
        '
        'PMain
        '
        Me.PMain.BackColor = System.Drawing.Color.Transparent
        Me.PMain.Controls.Add(Me.Panel2)
        Me.PMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PMain.Location = New System.Drawing.Point(168, 0)
        Me.PMain.Name = "PMain"
        Me.PMain.Size = New System.Drawing.Size(978, 682)
        Me.PMain.TabIndex = 6
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Pwait)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(978, 682)
        Me.Panel2.TabIndex = 0
        '
        'Pwait
        '
        Me.Pwait.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Pwait.BackColor = System.Drawing.Color.Gray
        Me.Pwait.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Pwait.Controls.Add(Me.Label5)
        Me.Pwait.Controls.Add(Me.PictureBox5)
        Me.Pwait.Location = New System.Drawing.Point(356, 256)
        Me.Pwait.Name = "Pwait"
        Me.Pwait.Size = New System.Drawing.Size(336, 56)
        Me.Pwait.TabIndex = 1
        Me.Pwait.Visible = False
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label5.BackColor = System.Drawing.Color.LightGray
        Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label5.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(72, 11)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(227, 29)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Wait few seconds....."
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(7, 3)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(65, 48)
        Me.PictureBox5.TabIndex = 2
        Me.PictureBox5.TabStop = False
        '
        'PData
        '
        Me.PData.BackColor = System.Drawing.Color.Transparent
        Me.PData.Controls.Add(Me.PictureBox6)
        Me.PData.Location = New System.Drawing.Point(248, 32)
        Me.PData.Name = "PData"
        Me.PData.Size = New System.Drawing.Size(152, 392)
        Me.PData.TabIndex = 5
        Me.PData.Visible = False
        '
        'PictureBox6
        '
        Me.PictureBox6.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(0, 32)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(120, 240)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox6.TabIndex = 0
        Me.PictureBox6.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.Desktop
        Me.Panel3.Controls.Add(Me.Button1)
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.Panel6)
        Me.Panel3.Controls.Add(Me.PlSuper)
        Me.Panel3.Controls.Add(Me.PSetup)
        Me.Panel3.Controls.Add(Me.PDownload)
        Me.Panel3.Controls.Add(Me.PAdmin)
        Me.Panel3.Controls.Add(Me.PdataEntry)
        Me.Panel3.Controls.Add(Me.PExport)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel3.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(168, 682)
        Me.Panel3.TabIndex = 4
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button1.BackColor = System.Drawing.Color.Transparent
        Me.Button1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(55, 625)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(64, 23)
        Me.Button1.TabIndex = 13
        Me.Button1.TabStop = False
        Me.Button1.Text = "Minimise"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.SystemColors.Desktop
        Me.Panel5.Controls.Add(Me.Label4)
        Me.Panel5.Controls.Add(Me.lblDate)
        Me.Panel5.Controls.Add(Me.lblLogin)
        Me.Panel5.Controls.Add(Me.Label1)
        Me.Panel5.Location = New System.Drawing.Point(0, 382)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(176, 57)
        Me.Panel5.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.InactiveCaption
        Me.Label4.Location = New System.Drawing.Point(2, 37)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(36, 15)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Date:"
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.ForeColor = System.Drawing.SystemColors.InactiveCaption
        Me.lblDate.Location = New System.Drawing.Point(42, 37)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(126, 17)
        Me.lblDate.TabIndex = 2
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLogin
        '
        Me.lblLogin.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLogin.ForeColor = System.Drawing.SystemColors.InactiveCaption
        Me.lblLogin.Location = New System.Drawing.Point(42, 13)
        Me.lblLogin.Name = "lblLogin"
        Me.lblLogin.Size = New System.Drawing.Size(134, 17)
        Me.lblLogin.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.InactiveCaption
        Me.Label1.Location = New System.Drawing.Point(2, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Login:"
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(32, 651)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(120, 24)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Copyright  @ 2005     ZanDen Singapore Pte Ltd. "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel6
        '
        Me.Panel6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Panel6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel6.Controls.Add(Me.PictureBox2)
        Me.Panel6.Location = New System.Drawing.Point(-8, 424)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(176, 200)
        Me.Panel6.TabIndex = 4
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(16, 4)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(152, 188)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 0
        Me.PictureBox2.TabStop = False
        '
        'PlSuper
        '
        Me.PlSuper.BackColor = System.Drawing.SystemColors.Desktop
        Me.PlSuper.Controls.Add(Me.Button3)
        Me.PlSuper.Location = New System.Drawing.Point(2, 19)
        Me.PlSuper.Name = "PlSuper"
        Me.PlSuper.Size = New System.Drawing.Size(157, 179)
        Me.PlSuper.TabIndex = 2
        Me.PlSuper.Visible = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.SteelBlue
        Me.Button3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.LightYellow
        Me.Button3.Location = New System.Drawing.Point(28, 16)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(102, 151)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "Download"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'PSetup
        '
        Me.PSetup.BackColor = System.Drawing.SystemColors.Desktop
        Me.PSetup.Controls.Add(Me.butActionControl)
        Me.PSetup.Controls.Add(Me.butItem)
        Me.PSetup.Controls.Add(Me.butWC)
        Me.PSetup.Controls.Add(Me.Label6)
        Me.PSetup.Controls.Add(Me.Label7)
        Me.PSetup.Controls.Add(Me.butRejectedGroup)
        Me.PSetup.Controls.Add(Me.butMachineGroup)
        Me.PSetup.Controls.Add(Me.butMachine)
        Me.PSetup.Controls.Add(Me.butWS)
        Me.PSetup.Controls.Add(Me.butRGrouping)
        Me.PSetup.Controls.Add(Me.butRejected)
        Me.PSetup.Controls.Add(Me.butOperator)
        Me.PSetup.Controls.Add(Me.butUser)
        Me.PSetup.Location = New System.Drawing.Point(4, 13)
        Me.PSetup.Name = "PSetup"
        Me.PSetup.Size = New System.Drawing.Size(152, 360)
        Me.PSetup.TabIndex = 2
        Me.PSetup.Visible = False
        '
        'butActionControl
        '
        Me.butActionControl.BackColor = System.Drawing.Color.SteelBlue
        Me.butActionControl.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butActionControl.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butActionControl.ForeColor = System.Drawing.Color.LightYellow
        Me.butActionControl.Location = New System.Drawing.Point(9, 216)
        Me.butActionControl.Name = "butActionControl"
        Me.butActionControl.Size = New System.Drawing.Size(135, 26)
        Me.butActionControl.TabIndex = 11
        Me.butActionControl.Text = "Action Control"
        Me.butActionControl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butActionControl.UseVisualStyleBackColor = False
        '
        'butItem
        '
        Me.butItem.BackColor = System.Drawing.Color.SteelBlue
        Me.butItem.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butItem.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butItem.ForeColor = System.Drawing.Color.LightYellow
        Me.butItem.Location = New System.Drawing.Point(10, 132)
        Me.butItem.Name = "butItem"
        Me.butItem.Size = New System.Drawing.Size(135, 26)
        Me.butItem.TabIndex = 10
        Me.butItem.Text = "Item"
        Me.butItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butItem.UseVisualStyleBackColor = False
        '
        'butWC
        '
        Me.butWC.BackColor = System.Drawing.Color.SteelBlue
        Me.butWC.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butWC.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butWC.ForeColor = System.Drawing.Color.LightYellow
        Me.butWC.Location = New System.Drawing.Point(10, 104)
        Me.butWC.Name = "butWC"
        Me.butWC.Size = New System.Drawing.Size(135, 26)
        Me.butWC.TabIndex = 9
        Me.butWC.Text = "Work Center"
        Me.butWC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butWC.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Goldenrod
        Me.Label6.Location = New System.Drawing.Point(9, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 16)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "SETUP"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Goldenrod
        Me.Label7.Location = New System.Drawing.Point(9, 252)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(84, 16)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "GROUPING"
        '
        'butRejectedGroup
        '
        Me.butRejectedGroup.BackColor = System.Drawing.Color.SteelBlue
        Me.butRejectedGroup.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butRejectedGroup.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butRejectedGroup.ForeColor = System.Drawing.Color.LightYellow
        Me.butRejectedGroup.Location = New System.Drawing.Point(10, 329)
        Me.butRejectedGroup.Name = "butRejectedGroup"
        Me.butRejectedGroup.Size = New System.Drawing.Size(135, 26)
        Me.butRejectedGroup.TabIndex = 7
        Me.butRejectedGroup.Text = "Rejected Group"
        Me.butRejectedGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butRejectedGroup.UseVisualStyleBackColor = False
        '
        'butMachineGroup
        '
        Me.butMachineGroup.BackColor = System.Drawing.Color.SteelBlue
        Me.butMachineGroup.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butMachineGroup.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butMachineGroup.ForeColor = System.Drawing.Color.LightYellow
        Me.butMachineGroup.Location = New System.Drawing.Point(10, 301)
        Me.butMachineGroup.Name = "butMachineGroup"
        Me.butMachineGroup.Size = New System.Drawing.Size(135, 26)
        Me.butMachineGroup.TabIndex = 6
        Me.butMachineGroup.Text = "Machine Group"
        Me.butMachineGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butMachineGroup.UseVisualStyleBackColor = False
        '
        'butMachine
        '
        Me.butMachine.BackColor = System.Drawing.Color.SteelBlue
        Me.butMachine.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butMachine.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butMachine.ForeColor = System.Drawing.Color.LightYellow
        Me.butMachine.Location = New System.Drawing.Point(10, 160)
        Me.butMachine.Name = "butMachine"
        Me.butMachine.Size = New System.Drawing.Size(135, 26)
        Me.butMachine.TabIndex = 5
        Me.butMachine.Text = "Machine"
        Me.butMachine.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butMachine.UseVisualStyleBackColor = False
        '
        'butWS
        '
        Me.butWS.BackColor = System.Drawing.Color.SteelBlue
        Me.butWS.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butWS.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butWS.ForeColor = System.Drawing.Color.LightYellow
        Me.butWS.Location = New System.Drawing.Point(10, 271)
        Me.butWS.Name = "butWS"
        Me.butWS.Size = New System.Drawing.Size(135, 26)
        Me.butWS.TabIndex = 4
        Me.butWS.Text = "Work Center Group"
        Me.butWS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butWS.UseVisualStyleBackColor = False
        '
        'butRGrouping
        '
        Me.butRGrouping.BackColor = System.Drawing.Color.SteelBlue
        Me.butRGrouping.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butRGrouping.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butRGrouping.ForeColor = System.Drawing.Color.LightYellow
        Me.butRGrouping.Location = New System.Drawing.Point(10, 76)
        Me.butRGrouping.Name = "butRGrouping"
        Me.butRGrouping.Size = New System.Drawing.Size(135, 26)
        Me.butRGrouping.TabIndex = 3
        Me.butRGrouping.Text = "Resource Group"
        Me.butRGrouping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butRGrouping.UseVisualStyleBackColor = False
        '
        'butRejected
        '
        Me.butRejected.BackColor = System.Drawing.Color.SteelBlue
        Me.butRejected.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butRejected.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butRejected.ForeColor = System.Drawing.Color.LightYellow
        Me.butRejected.Location = New System.Drawing.Point(10, 188)
        Me.butRejected.Name = "butRejected"
        Me.butRejected.Size = New System.Drawing.Size(135, 26)
        Me.butRejected.TabIndex = 2
        Me.butRejected.Text = "Reject Description"
        Me.butRejected.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butRejected.UseVisualStyleBackColor = False
        '
        'butOperator
        '
        Me.butOperator.BackColor = System.Drawing.Color.SteelBlue
        Me.butOperator.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butOperator.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butOperator.ForeColor = System.Drawing.Color.LightYellow
        Me.butOperator.Location = New System.Drawing.Point(10, 48)
        Me.butOperator.Name = "butOperator"
        Me.butOperator.Size = New System.Drawing.Size(135, 26)
        Me.butOperator.TabIndex = 1
        Me.butOperator.Text = "Operators"
        Me.butOperator.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butOperator.UseVisualStyleBackColor = False
        '
        'butUser
        '
        Me.butUser.BackColor = System.Drawing.Color.SteelBlue
        Me.butUser.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butUser.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butUser.ForeColor = System.Drawing.Color.LightYellow
        Me.butUser.Location = New System.Drawing.Point(10, 20)
        Me.butUser.Name = "butUser"
        Me.butUser.Size = New System.Drawing.Size(135, 26)
        Me.butUser.TabIndex = 0
        Me.butUser.Text = "User"
        Me.butUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butUser.UseVisualStyleBackColor = False
        '
        'PDownload
        '
        Me.PDownload.BackColor = System.Drawing.SystemColors.Desktop
        Me.PDownload.Controls.Add(Me.Button6)
        Me.PDownload.Controls.Add(Me.butDlinf)
        Me.PDownload.Controls.Add(Me.butDounload)
        Me.PDownload.Location = New System.Drawing.Point(5, 5)
        Me.PDownload.Name = "PDownload"
        Me.PDownload.Size = New System.Drawing.Size(152, 288)
        Me.PDownload.TabIndex = 2
        Me.PDownload.Visible = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.SteelBlue
        Me.Button6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button6.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.LightYellow
        Me.Button6.Location = New System.Drawing.Point(6, 51)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(144, 26)
        Me.Button6.TabIndex = 3
        Me.Button6.Text = "View Job"
        Me.Button6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button6.UseVisualStyleBackColor = False
        '
        'butDlinf
        '
        Me.butDlinf.BackColor = System.Drawing.Color.SteelBlue
        Me.butDlinf.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butDlinf.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDlinf.ForeColor = System.Drawing.Color.LightYellow
        Me.butDlinf.Location = New System.Drawing.Point(5, 155)
        Me.butDlinf.Name = "butDlinf"
        Me.butDlinf.Size = New System.Drawing.Size(144, 26)
        Me.butDlinf.TabIndex = 2
        Me.butDlinf.Text = "Download Information"
        Me.butDlinf.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butDlinf.UseVisualStyleBackColor = False
        Me.butDlinf.Visible = False
        '
        'butDounload
        '
        Me.butDounload.BackColor = System.Drawing.Color.SteelBlue
        Me.butDounload.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butDounload.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDounload.ForeColor = System.Drawing.Color.LightYellow
        Me.butDounload.Location = New System.Drawing.Point(8, 16)
        Me.butDounload.Name = "butDounload"
        Me.butDounload.Size = New System.Drawing.Size(144, 26)
        Me.butDounload.TabIndex = 1
        Me.butDounload.Text = "Download"
        Me.butDounload.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butDounload.UseVisualStyleBackColor = False
        '
        'PAdmin
        '
        Me.PAdmin.BackColor = System.Drawing.SystemColors.Desktop
        Me.PAdmin.Controls.Add(Me.Button4)
        Me.PAdmin.Controls.Add(Me.Button5)
        Me.PAdmin.Location = New System.Drawing.Point(8, 21)
        Me.PAdmin.Name = "PAdmin"
        Me.PAdmin.Size = New System.Drawing.Size(160, 264)
        Me.PAdmin.TabIndex = 4
        Me.PAdmin.Visible = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.SteelBlue
        Me.Button4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.LightYellow
        Me.Button4.Location = New System.Drawing.Point(8, 54)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(144, 26)
        Me.Button4.TabIndex = 2
        Me.Button4.Text = "Download Information"
        Me.Button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.SteelBlue
        Me.Button5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.LightYellow
        Me.Button5.Location = New System.Drawing.Point(8, 16)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(144, 26)
        Me.Button5.TabIndex = 1
        Me.Button5.Text = "Download"
        Me.Button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button5.UseVisualStyleBackColor = False
        '
        'PdataEntry
        '
        Me.PdataEntry.BackColor = System.Drawing.SystemColors.Desktop
        Me.PdataEntry.Controls.Add(Me.butTansDis)
        Me.PdataEntry.Controls.Add(Me.butDataVerify)
        Me.PdataEntry.Controls.Add(Me.Label2)
        Me.PdataEntry.Controls.Add(Me.butop)
        Me.PdataEntry.Controls.Add(Me.butDataEntry)
        Me.PdataEntry.Location = New System.Drawing.Point(3, 22)
        Me.PdataEntry.Name = "PdataEntry"
        Me.PdataEntry.Size = New System.Drawing.Size(160, 268)
        Me.PdataEntry.TabIndex = 3
        Me.PdataEntry.Visible = False
        '
        'butTansDis
        '
        Me.butTansDis.BackColor = System.Drawing.Color.SteelBlue
        Me.butTansDis.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butTansDis.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butTansDis.ForeColor = System.Drawing.Color.LightYellow
        Me.butTansDis.Location = New System.Drawing.Point(5, 76)
        Me.butTansDis.Name = "butTansDis"
        Me.butTansDis.Size = New System.Drawing.Size(144, 26)
        Me.butTansDis.TabIndex = 6
        Me.butTansDis.Text = "Job Verification"
        Me.butTansDis.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butTansDis.UseVisualStyleBackColor = False
        '
        'butDataVerify
        '
        Me.butDataVerify.BackColor = System.Drawing.Color.SteelBlue
        Me.butDataVerify.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butDataVerify.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDataVerify.ForeColor = System.Drawing.Color.LightYellow
        Me.butDataVerify.Location = New System.Drawing.Point(5, 107)
        Me.butDataVerify.Name = "butDataVerify"
        Me.butDataVerify.Size = New System.Drawing.Size(144, 26)
        Me.butDataVerify.TabIndex = 5
        Me.butDataVerify.Text = "Job Process"
        Me.butDataVerify.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butDataVerify.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(6, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(140, 31)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "--------------"
        '
        'butop
        '
        Me.butop.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butop.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.butop.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butop.ForeColor = System.Drawing.Color.LightYellow
        Me.butop.Location = New System.Drawing.Point(8, 186)
        Me.butop.Name = "butop"
        Me.butop.Size = New System.Drawing.Size(108, 32)
        Me.butop.TabIndex = 3
        Me.butop.Text = "Data Verification"
        Me.butop.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butop.Visible = False
        '
        'butDataEntry
        '
        Me.butDataEntry.BackColor = System.Drawing.Color.SteelBlue
        Me.butDataEntry.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butDataEntry.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDataEntry.ForeColor = System.Drawing.Color.LightYellow
        Me.butDataEntry.Location = New System.Drawing.Point(8, 16)
        Me.butDataEntry.Name = "butDataEntry"
        Me.butDataEntry.Size = New System.Drawing.Size(120, 26)
        Me.butDataEntry.TabIndex = 1
        Me.butDataEntry.Text = "Data Entry"
        Me.butDataEntry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butDataEntry.UseVisualStyleBackColor = False
        '
        'PExport
        '
        Me.PExport.BackColor = System.Drawing.SystemColors.Desktop
        Me.PExport.Controls.Add(Me.Button2)
        Me.PExport.Controls.Add(Me.butExport)
        Me.PExport.Location = New System.Drawing.Point(2, 21)
        Me.PExport.Name = "PExport"
        Me.PExport.Size = New System.Drawing.Size(160, 293)
        Me.PExport.TabIndex = 4
        Me.PExport.Visible = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.SteelBlue
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.LightYellow
        Me.Button2.Location = New System.Drawing.Point(8, 54)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(144, 26)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Export Information"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.UseVisualStyleBackColor = False
        Me.Button2.Visible = False
        '
        'butExport
        '
        Me.butExport.BackColor = System.Drawing.Color.SteelBlue
        Me.butExport.Cursor = System.Windows.Forms.Cursors.Hand
        Me.butExport.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butExport.ForeColor = System.Drawing.Color.LightYellow
        Me.butExport.Location = New System.Drawing.Point(8, 16)
        Me.butExport.Name = "butExport"
        Me.butExport.Size = New System.Drawing.Size(144, 26)
        Me.butExport.TabIndex = 1
        Me.butExport.Text = "Export Data"
        Me.butExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.butExport.UseVisualStyleBackColor = False
        '
        'MDI
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1148, 748)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.PTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "MDI"
        Me.Text = "WIP-AMT"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PTop.ResumeLayout(False)
        Me.PTop.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.PMain.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Pwait.ResumeLayout(False)
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PData.ResumeLayout(False)
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PlSuper.ResumeLayout(False)
        Me.PSetup.ResumeLayout(False)
        Me.PSetup.PerformLayout()
        Me.PDownload.ResumeLayout(False)
        Me.PAdmin.ResumeLayout(False)
        Me.PdataEntry.ResumeLayout(False)
        Me.PdataEntry.PerformLayout()
        Me.PExport.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Public Declare Function SetParent Lib "user32" Alias "SetParent" (ByVal hWndChild As Integer, ByVal hWndNewParent As Integer) As Integer
    Private Sub ToolBar2_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar2.ButtonClick

        Try

            Panel5.Visible = True
            PlSuper.Visible = False
            Select Case e.Button.ImageIndex
                Case "0"
                    frmClose()
                    PSetup.Visible = True
                    PDownload.Visible = False
                    PdataEntry.Visible = False
                    PAdmin.Visible = False
                    PExport.Visible = False
                Case "1"
                    frmClose()
                    PSetup.Visible = False
                    PDownload.Visible = True
                    PdataEntry.Visible = False
                    PAdmin.Visible = False
                    PExport.Visible = False
                Case "2"
                    frmClose()
                    PSetup.Visible = False
                    PDownload.Visible = False
                    PdataEntry.Visible = False
                    PAdmin.Visible = False
                    PExport.Visible = True
                Case "3"
                    frmClose()
                    PSetup.Visible = False
                    PDownload.Visible = False
                    PdataEntry.Visible = True
                    PExport.Visible = False
                    PdataEntry.Visible = True
                    PAdmin.Visible = False

                Case "5"
                    frmClose()
                    PSetup.Visible = False
                    PdataEntry.Visible = False
                    PAdmin.Visible = False
                    PDownload.Visible = False
                    PExport.Visible = False
                    LogUserID = ""
                    LogUserName = ""
                    LogName = ""
                    LogPrivilege = ""
                    lblLogin.Text = ""
                    Dim OBJLogin As New frmLogin
                    OBJLogin.ShowDialog()
                    If LogPrivilege = "Super User" Then
                        PlSuper.Visible = True
                    End If




                    If UCase(LogPrivilege) = "WORKER" Then
                        Panel7.Visible = True
                        Panel7.Width = 244
                        'ToolBar2.Buttons(0).Visible = False
                        'ToolBar2.Buttons(1).Visible = False
                        'ToolBar2.Buttons(2).Visible = False
                        ''ToolBar2.Buttons(3).Visible = False
                        ToolBar2.Buttons(4).Visible = False
                    ElseIf LogPrivilege = "Super User" Then
                        PlSuper.Visible = True

                    Else
                        ToolBar2.Buttons(4).Visible = True
                        Panel7.Visible = True
                        Panel7.Width = 425
                    End If


                    lblDate.Text = Format(Now, "dd/MM/yyyy")
                Case "6"
                    frmClose()
                    PSetup.Visible = False
                    PdataEntry.Visible = False
                    PAdmin.Visible = False
                    PDownload.Visible = False
                    PExport.Visible = False
                    If MessageBox.Show("Do you want to quit?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                        Application.Exit()
                    End If

                Case Else
            End Select

        Catch ex As Exception

        End Try
    End Sub

    Sub frmClose()
        Select Case selectForm
            Case "Operator"
                objOperator.Close()
                selectForm = ""
            Case "User"
                objUser.Close()
                selectForm = ""
            Case "RGrouping"
                objRGrouping.Close()
                selectForm = ""
            Case "WStation"
                objWorkStation.Close()
                selectForm = ""
            Case "RejectedGroup"
                objRejGroup.Close()
                selectForm = ""
            Case "Download"
                objDownload.Close()
                selectForm = ""
            Case "View"                            'By Yeo 20081011
                objView.Close()
                selectForm = ""
            Case "DataEntry"                            'By Yeo 20081011
                objDataEntry.Close()
                selectForm = ""
            Case "DataVerify"                            'By Yeo 20081011
                objDataVerify.Close()
                selectForm = ""
            Case "OP"                            'By Yeo 20081011
                objop.Close()
                selectForm = ""
            Case "DataTran"
                objTansDis.Close()
                selectForm = ""
            Case "Machine"
                objMachine.Close()
                selectForm = ""
            Case "MachineWRK"
                objMachineWRK.Close()
                selectForm = ""
            Case "Export"
                objExport.Close()
                selectForm = ""
            Case "Rejected"
                objRejected.Close()
                selectForm = ""
            Case "WC"
                objwc.Close()
                selectForm = ""
            Case "Item"
                objItem.Close()
                selectForm = ""
            Case "ActionControl"
                objOPstatus.Close()
                selectForm = ""

        End Select
    End Sub
    Sub frmOpen()
        Select Case selectForm
            Case "Operator"
                objOperator = New frmOperative
                SetParent(objOperator.Handle.ToInt32, PMain.Handle.ToInt32)
                objOperator.Left = -PMain.Left
                objOperator.Top = -PMain.Top
                objOperator.Width = PMain.Width
                objOperator.Height = PMain.Height
                Pwait.Visible = True
                Application.DoEvents()
                objOperator.Show()
                Pwait.Visible = False
            Case "User"
                objUser = New frmUser
                SetParent(objUser.Handle.ToInt32, PMain.Handle.ToInt32)
                objUser.Left = -PMain.Left
                objUser.Top = -PMain.Top
                objUser.Width = PMain.Width
                objUser.Height = PMain.Height
                Pwait.Visible = True
                Application.DoEvents()
                objUser.Show()
                Pwait.Visible = False
            Case "RGrouping"
                objRGrouping = New frmResourceGrouping
                SetParent(objRGrouping.Handle.ToInt32, PMain.Handle.ToInt32)
                objRGrouping.Left = -PMain.Left
                objRGrouping.Top = -PMain.Top
                objRGrouping.Width = PMain.Width
                objRGrouping.Height = PMain.Height
                Pwait.Visible = True
                Application.DoEvents()
                objRGrouping.Show()
                Pwait.Visible = False
            Case "WStation"
                objWorkStation = New frmWCRCGroup
                SetParent(objWorkStation.Handle.ToInt32, PMain.Handle.ToInt32)
                objWorkStation.Left = -PMain.Left
                objWorkStation.Top = -PMain.Top
                objWorkStation.Width = PMain.Width
                objWorkStation.Height = PMain.Height
                Pwait.Visible = True
                Application.DoEvents()
                objWorkStation.Show()
                Pwait.Visible = False
            Case "WC"
                objwc = New frmWorkCenter
                SetParent(objwc.Handle.ToInt32, PMain.Handle.ToInt32)
                objwc.Left = -PMain.Left
                objwc.Top = -PMain.Top
                objwc.Width = PMain.Width
                objwc.Height = PMain.Height
                Pwait.Visible = True
                Application.DoEvents()
                objwc.Show()
                Pwait.Visible = False
            Case "Item"
                objItem = New frmItem
                SetParent(objItem.Handle.ToInt32, PMain.Handle.ToInt32)
                objItem.Left = -PMain.Left
                objItem.Top = -PMain.Top
                objItem.Width = PMain.Width
                objItem.Height = PMain.Height
                Pwait.Visible = True
                Application.DoEvents()
                objItem.Show()
                Pwait.Visible = False
            Case "RejectedGroup"
                objRejGroup = New frmWCRejectGrouping
                SetParent(objRejGroup.Handle.ToInt32, PMain.Handle.ToInt32)
                objRejGroup.Left = -PMain.Left
                objRejGroup.Top = -PMain.Top
                objRejGroup.Width = PMain.Width
                objRejGroup.Height = PMain.Height
                Pwait.Visible = True
                Application.DoEvents()
                objRejGroup.Show()
                Pwait.Visible = False
            Case "Rejected"
                objRejected = New frmRejectMaster
                SetParent(objRejected.Handle.ToInt32, PMain.Handle.ToInt32)
                objRejected.Left = -PMain.Left
                objRejected.Top = -PMain.Top
                objRejected.Width = PMain.Width
                objRejected.Height = PMain.Height
                Pwait.Visible = True
                Application.DoEvents()
                objRejected.Show()
                Pwait.Visible = False
            Case "Download"
                objDownload = New frmDownload
                SetParent(objDownload.Handle.ToInt32, PMain.Handle.ToInt32)
                objDownload.Left = -PMain.Left
                objDownload.Top = -PMain.Top
                objDownload.Width = PMain.Width
                objDownload.Height = PMain.Height
                Pwait.Visible = True
                Application.DoEvents()
                objDownload.Show()
                Pwait.Visible = False

            Case "View"                                     'By Yeo 20081110
                objView = New frmViewJob
                SetParent(objView.Handle.ToInt32, PMain.Handle.ToInt32)
                objView.Left = -PMain.Left
                objView.Top = -PMain.Top
                objView.Width = PMain.Width
                objView.Height = PMain.Height
                Pwait.Visible = True
                Application.DoEvents()
                objView.Show()
                Pwait.Visible = False
            Case "DataEntry"
                objDataEntry = New frmSupJobEdit
                SetParent(objDataEntry.Handle.ToInt32, PMain.Handle.ToInt32)
                objDataEntry.Left = -PMain.Left
                objDataEntry.Top = -PMain.Top
                objDataEntry.Width = PMain.Width
                objDataEntry.Height = PMain.Height
                Pwait.Visible = True
                Application.DoEvents()
                objDataEntry.txtJobNo.Select()
                objDataEntry.Show()
                Pwait.Visible = False
            Case "OP"
                Try
                    objop = New frmDataEntryOP
                    'SetParent(objop.Handle.ToInt32, PMain.Handle.ToInt32)
                    objop.Left = -PMain.Left
                    objop.Top = -PMain.Top
                    objop.Width = PMain.Width
                    objop.Height = PMain.Height
                    Pwait.Visible = True
                    objop.txtC.Select()
                    Application.DoEvents()
                    objop.txtC.Select()
                    'Application.DoEvents()
                    objop.ShowDialog()
                    Application.Exit()
                    'objop.txtC.Focus()
                    ' objop.txtC.Select()
                    'Pwait.Visible = False
                Catch ex As Exception
                End Try

            Case "DataVerify"                            'By Yeo 20081011
                objDataVerify = New frmDataView
                SetParent(objDataVerify.Handle.ToInt32, PMain.Handle.ToInt32)
                objDataVerify.Left = -PMain.Left
                objDataVerify.Top = -PMain.Top
                objDataVerify.Width = PMain.Width
                objDataVerify.Height = PMain.Height
                Pwait.Visible = True
                Application.DoEvents()
                objDataVerify.Show()
                Pwait.Visible = False
            Case "DataTran"
                objTansDis = New frmTransDis
                SetParent(objTansDis.Handle.ToInt32, PMain.Handle.ToInt32)
                objTansDis.Left = -PMain.Left
                objTansDis.Top = -PMain.Top
                objTansDis.Width = PMain.Width
                objTansDis.Height = PMain.Height
                Pwait.Visible = True
                Application.DoEvents()
                objTansDis.Show()
                Pwait.Visible = False
            Case "Machine"
                objMachine = New frmMachine
                SetParent(objMachine.Handle.ToInt32, PMain.Handle.ToInt32)
                objMachine.Left = -PMain.Left
                objMachine.Top = -PMain.Top
                objMachine.Width = PMain.Width
                objMachine.Height = PMain.Height
                Pwait.Visible = True
                Application.DoEvents()
                objMachine.Show()
                Pwait.Visible = False
            Case "MachineWRK"
                objMachineWRK = New frmMachineWRK
                SetParent(objMachineWRK.Handle.ToInt32, PMain.Handle.ToInt32)
                objMachineWRK.Left = -PMain.Left
                objMachineWRK.Top = -PMain.Top
                objMachineWRK.Width = PMain.Width
                objMachineWRK.Height = PMain.Height
                Pwait.Visible = True
                Application.DoEvents()
                objMachineWRK.Show()
                Pwait.Visible = False
            Case "Export"
                objExport = New frmExportMain
                SetParent(objExport.Handle.ToInt32, PMain.Handle.ToInt32)
                objExport.Left = -PMain.Left
                objExport.Top = -PMain.Top
                objExport.Width = PMain.Width
                objExport.Height = PMain.Height
                Pwait.Visible = True
                Application.DoEvents()
                objExport.Show()
                Pwait.Visible = False
            Case "ActionControl"
                objOPstatus = New frmOperationStatus
                SetParent(objOPstatus.Handle.ToInt32, PMain.Handle.ToInt32)
                objOPstatus.Left = -PMain.Left
                objOPstatus.Top = -PMain.Top
                objOPstatus.Width = PMain.Width
                objOPstatus.Height = PMain.Height
                Pwait.Visible = True
                Application.DoEvents()
                objOPstatus.Show()
                Pwait.Visible = False



        End Select
    End Sub

    Private Sub butOperator_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butOperator.Click
        frmClose()
        selectForm = "Operator"
        frmOpen()
    End Sub

    Private Sub butUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUser.Click
        frmClose()
        selectForm = "User"
        frmOpen()
    End Sub

    Private Sub butRGrouping_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butRGrouping.Click
        frmClose()
        selectForm = "RGrouping"
        frmOpen()
    End Sub

    Private Sub butWS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butWS.Click
        frmClose()
        selectForm = "WStation"
        frmOpen()
    End Sub

    Private Sub butRejected_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butRejected.Click
        frmClose()
        selectForm = "Rejected"
        frmOpen()
    End Sub

    Private Sub MDI_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'FW.Movie = APPPath & "title.swf"
        Try

    
            If UCase(AccessRights) = "DATA ENTRY" Then
                Panel7.Visible = True
                Panel7.Width = 125
                'ToolBar2.Buttons(0).Visible = False
                'ToolBar2.Buttons(1).Visible = False
                'ToolBar2.Buttons(2).Visible = False
                ''ToolBar2.Buttons(3).Visible = False
                ToolBar2.Buttons(4).Visible = False
                lblLogin.Text = "Operator"
                lblDate.Text = Format(Now, "dd/MM/yyyy")
                LogPrivilege = "OP"
            Else
                Dim objLogIn As New frmLogin
                Panel7.Visible = False
                objLogIn.ShowDialog()
                If excmd = 1 Then
                    Application.Exit()
                End If
            End If
            lblLogin.Text = LogName
            lblDate.Text = Format(Now, "dd/MM/yyyy")
            If UCase(LogPrivilege) = "WORKER" Then
                Panel7.Visible = True
                Panel7.Width = 244
                'ToolBar2.Buttons(0).Visible = False
                'ToolBar2.Buttons(1).Visible = False
                'ToolBar2.Buttons(2).Visible = False
                ''ToolBar2.Buttons(3).Visible = False
                ToolBar2.Buttons(4).Visible = False
                'frmClose()
                'selectForm = "OP"
                'frmOpen()
            ElseIf UCase(LogPrivilege) <> "OP" Then
                Panel7.Visible = True
            End If
            If UCase(LogPrivilege) = "OP" Then
                'butop.Select()
                butop_Click(sender, e)
            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub butDounload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDounload.Click
        frmClose()
        selectForm = "Download"
        frmOpen()
    End Sub



    Private Sub Button6_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        frmClose()
        selectForm = "View"
        frmOpen()
    End Sub

    Private Sub butDataEntry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDataEntry.Click
        frmClose()
        selectForm = "DataEntry"
        frmOpen()
    End Sub

    Private Sub butDataVerify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDataVerify.Click
        frmClose()
        selectForm = "DataVerify"
        frmOpen()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub butop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butop.Click
        'TextBox1.Select()
        frmClose()
        selectForm = "OP"
        frmOpen()

        ' butDataEntry_Click(sender, e)
    End Sub

    Private Sub butTansDis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butTansDis.Click
        frmClose()
        selectForm = "DataTran"
        frmOpen()
    End Sub

    Private Sub butMachine_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butMachine.Click
        frmClose()
        selectForm = "Machine"
        frmOpen()
    End Sub

    Private Sub butMachineGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butMachineGroup.Click
        frmClose()
        selectForm = "MachineWRK"
        frmOpen()
    End Sub


    Private Sub butExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExport.Click
        frmClose()
        selectForm = "Export"
        frmOpen()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim objDALL As New frmDownloadAll

        objDALL.ShowDialog()
    End Sub

    Private Sub butRejectedGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butRejectedGroup.Click
        frmClose()
        selectForm = "RejectedGroup"
        frmOpen()
    End Sub

    Private Sub butWC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butWC.Click
        frmClose()
        selectForm = "WC"
        frmOpen()
    End Sub

    Private Sub butItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butItem.Click
        frmClose()
        selectForm = "Item"
        frmOpen()
    End Sub

    Private Sub butActionControl_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butActionControl.Click
        frmClose()
        selectForm = "ActionControl"
        frmOpen()
    End Sub
End Class
