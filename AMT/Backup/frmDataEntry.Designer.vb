<[Global].Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
[Partial] Class frmDataEntry
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components [IsNot] Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDataEntry))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.lblMsgBox = New System.Windows.Forms.Label
        Me.lblTotOPQTY = New System.Windows.Forms.Label
        Me.Label32 = New System.Windows.Forms.Label
        Me.lbltotRej = New System.Windows.Forms.Label
        Me.lbltotCom = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.lstv = New System.Windows.Forms.ListView
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.ButHand = New System.Windows.Forms.Button
        Me.txtSubval = New System.Windows.Forms.TextBox
        Me.txtHandoverName = New System.Windows.Forms.TextBox
        Me.txtSpStatus = New System.Windows.Forms.TextBox
        Me.txtHandoverid = New System.Windows.Forms.TextBox
        Me.txtSufParent = New System.Windows.Forms.TextBox
        Me.lblSplit = New System.Windows.Forms.Label
        Me.txtsuf = New System.Windows.Forms.TextBox
        Me.txtWKSatation = New System.Windows.Forms.TextBox
        Me.txtARCQTY = New System.Windows.Forms.TextBox
        Me.txtC = New System.Windows.Forms.TextBox
        Me.txtMachineid = New System.Windows.Forms.TextBox
        Me.Label31 = New System.Windows.Forms.Label
        Me.txtTansID = New System.Windows.Forms.TextBox
        Me.txtWKID = New System.Windows.Forms.TextBox
        Me.butPre = New System.Windows.Forms.Button
        Me.butNext = New System.Windows.Forms.Button
        Me.txtOPQTY = New System.Windows.Forms.TextBox
        Me.Label30 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.txtItemNoTemp = New System.Windows.Forms.TextBox
        Me.Panel7 = New System.Windows.Forms.Panel
        Me.butExit = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.butDelete = New System.Windows.Forms.Button
        Me.butUpdate = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.txtRejqty3 = New System.Windows.Forms.TextBox
        Me.txtRejDes3 = New System.Windows.Forms.TextBox
        Me.txtRejCode3 = New System.Windows.Forms.TextBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.txtRejqty2 = New System.Windows.Forms.TextBox
        Me.txtRejDes2 = New System.Windows.Forms.TextBox
        Me.txtRejCode2 = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtRejqty1 = New System.Windows.Forms.TextBox
        Me.txtRejDes1 = New System.Windows.Forms.TextBox
        Me.txtRejCode1 = New System.Windows.Forms.TextBox
        Me.txtRejectedQty = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtCompletedQty = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtReleasedqty = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.dtpEnd = New System.Windows.Forms.DateTimePicker
        Me.txtEndTime = New System.Windows.Forms.TextBox
        Me.txtStartTime = New System.Windows.Forms.TextBox
        Me.txtEndDate = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtStartDate = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtOperators = New System.Windows.Forms.TextBox
        Me.txtItemNo = New System.Windows.Forms.TextBox
        Me.txtOperationNo = New System.Windows.Forms.TextBox
        Me.txtWKSatationID = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.dtpStart = New System.Windows.Forms.DateTimePicker
        Me.Panel8 = New System.Windows.Forms.Panel
        Me.Label36 = New System.Windows.Forms.Label
        Me.Label38 = New System.Windows.Forms.Label
        Me.txtEmpid = New System.Windows.Forms.TextBox
        Me.txtEmpName = New System.Windows.Forms.TextBox
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.butNotepad = New System.Windows.Forms.Button
        Me.butCal = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(12, 22)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(832, 722)
        Me.Panel1.TabIndex = 5
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.lblMsgBox)
        Me.Panel2.Controls.Add(Me.lblTotOPQTY)
        Me.Panel2.Controls.Add(Me.Label32)
        Me.Panel2.Controls.Add(Me.lbltotRej)
        Me.Panel2.Controls.Add(Me.lbltotCom)
        Me.Panel2.Controls.Add(Me.Label28)
        Me.Panel2.Controls.Add(Me.Label27)
        Me.Panel2.Controls.Add(Me.Panel6)
        Me.Panel2.Controls.Add(Me.Panel4)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Location = New System.Drawing.Point(5, 7)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(824, 716)
        Me.Panel2.TabIndex = 0
        '
        'lblMsgBox
        '
        Me.lblMsgBox.AutoSize = True
        Me.lblMsgBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsgBox.ForeColor = System.Drawing.Color.DarkRed
        Me.lblMsgBox.Location = New System.Drawing.Point(39, 440)
        Me.lblMsgBox.Name = "lblMsgBox"
        Me.lblMsgBox.Size = New System.Drawing.Size(312, 13)
        Me.lblMsgBox.TabIndex = 118
        Me.lblMsgBox.Text = "TRANSACTION HISTORY FOR THIS WORK CENTER"
        '
        'lblTotOPQTY
        '
        Me.lblTotOPQTY.AutoSize = True
        Me.lblTotOPQTY.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotOPQTY.Location = New System.Drawing.Point(446, 440)
        Me.lblTotOPQTY.Name = "lblTotOPQTY"
        Me.lblTotOPQTY.Size = New System.Drawing.Size(25, 13)
        Me.lblTotOPQTY.TabIndex = 110
        Me.lblTotOPQTY.Text = "0.0"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(357, 440)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(88, 13)
        Me.Label32.TabIndex = 109
        Me.Label32.Text = "Received Qty:"
        '
        'lbltotRej
        '
        Me.lbltotRej.AutoSize = True
        Me.lbltotRej.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltotRej.Location = New System.Drawing.Point(752, 440)
        Me.lbltotRej.Name = "lbltotRej"
        Me.lbltotRej.Size = New System.Drawing.Size(25, 13)
        Me.lbltotRej.TabIndex = 107
        Me.lbltotRej.Text = "0.0"
        '
        'lbltotCom
        '
        Me.lbltotCom.AutoSize = True
        Me.lbltotCom.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltotCom.Location = New System.Drawing.Point(590, 440)
        Me.lbltotCom.Name = "lbltotCom"
        Me.lbltotCom.Size = New System.Drawing.Size(25, 13)
        Me.lbltotCom.TabIndex = 106
        Me.lbltotCom.Text = "0.0"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(663, 440)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(89, 13)
        Me.Label28.TabIndex = 104
        Me.Label28.Text = "Rejected Qty :"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(494, 440)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(97, 13)
        Me.Label27.TabIndex = 103
        Me.Label27.Text = "Completed Qty :"
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.LightGray
        Me.Panel6.Controls.Add(Me.lstv)
        Me.Panel6.Location = New System.Drawing.Point(11, 456)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(807, 256)
        Me.Panel6.TabIndex = 102
        Me.Panel6.TabStop = True
        '
        'lstv
        '
        Me.lstv.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(8, 8)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(786, 244)
        Me.lstv.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lstv.TabIndex = 7
        Me.lstv.TabStop = False
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'Panel4
        '
        Me.Panel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel4.Controls.Add(Me.Panel5)
        Me.Panel4.Controls.Add(Me.Panel8)
        Me.Panel4.Location = New System.Drawing.Point(0, 32)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(829, 406)
        Me.Panel4.TabIndex = 37
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel5.Controls.Add(Me.ButHand)
        Me.Panel5.Controls.Add(Me.txtSubval)
        Me.Panel5.Controls.Add(Me.txtHandoverName)
        Me.Panel5.Controls.Add(Me.txtSpStatus)
        Me.Panel5.Controls.Add(Me.txtHandoverid)
        Me.Panel5.Controls.Add(Me.txtSufParent)
        Me.Panel5.Controls.Add(Me.lblSplit)
        Me.Panel5.Controls.Add(Me.txtsuf)
        Me.Panel5.Controls.Add(Me.txtWKSatation)
        Me.Panel5.Controls.Add(Me.txtARCQTY)
        Me.Panel5.Controls.Add(Me.txtC)
        Me.Panel5.Controls.Add(Me.txtMachineid)
        Me.Panel5.Controls.Add(Me.Label31)
        Me.Panel5.Controls.Add(Me.txtTansID)
        Me.Panel5.Controls.Add(Me.txtWKID)
        Me.Panel5.Controls.Add(Me.butPre)
        Me.Panel5.Controls.Add(Me.butNext)
        Me.Panel5.Controls.Add(Me.txtOPQTY)
        Me.Panel5.Controls.Add(Me.Label30)
        Me.Panel5.Controls.Add(Me.Label13)
        Me.Panel5.Controls.Add(Me.Label26)
        Me.Panel5.Controls.Add(Me.Label25)
        Me.Panel5.Controls.Add(Me.Label24)
        Me.Panel5.Controls.Add(Me.Label23)
        Me.Panel5.Controls.Add(Me.txtItemNoTemp)
        Me.Panel5.Controls.Add(Me.Panel7)
        Me.Panel5.Controls.Add(Me.GroupBox3)
        Me.Panel5.Controls.Add(Me.GroupBox2)
        Me.Panel5.Controls.Add(Me.GroupBox1)
        Me.Panel5.Controls.Add(Me.txtRejectedQty)
        Me.Panel5.Controls.Add(Me.Label12)
        Me.Panel5.Controls.Add(Me.txtCompletedQty)
        Me.Panel5.Controls.Add(Me.Label11)
        Me.Panel5.Controls.Add(Me.txtReleasedqty)
        Me.Panel5.Controls.Add(Me.Label10)
        Me.Panel5.Controls.Add(Me.dtpEnd)
        Me.Panel5.Controls.Add(Me.txtEndTime)
        Me.Panel5.Controls.Add(Me.txtStartTime)
        Me.Panel5.Controls.Add(Me.txtEndDate)
        Me.Panel5.Controls.Add(Me.Label9)
        Me.Panel5.Controls.Add(Me.txtStartDate)
        Me.Panel5.Controls.Add(Me.Label8)
        Me.Panel5.Controls.Add(Me.txtOperators)
        Me.Panel5.Controls.Add(Me.txtItemNo)
        Me.Panel5.Controls.Add(Me.txtOperationNo)
        Me.Panel5.Controls.Add(Me.txtWKSatationID)
        Me.Panel5.Controls.Add(Me.Label7)
        Me.Panel5.Controls.Add(Me.Label6)
        Me.Panel5.Controls.Add(Me.Label5)
        Me.Panel5.Controls.Add(Me.Label4)
        Me.Panel5.Controls.Add(Me.Label3)
        Me.Panel5.Controls.Add(Me.dtpStart)
        Me.Panel5.Location = New System.Drawing.Point(11, 37)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(807, 360)
        Me.Panel5.TabIndex = 39
        '
        'ButHand
        '
        Me.ButHand.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ButHand.Location = New System.Drawing.Point(668, 127)
        Me.ButHand.Name = "ButHand"
        Me.ButHand.Size = New System.Drawing.Size(83, 23)
        Me.ButHand.TabIndex = 130
        Me.ButHand.Text = "HANDOVER"
        Me.ButHand.UseVisualStyleBackColor = False
        Me.ButHand.Visible = False
        '
        'txtSubval
        '
        Me.txtSubval.Location = New System.Drawing.Point(448, 285)
        Me.txtSubval.Name = "txtSubval"
        Me.txtSubval.Size = New System.Drawing.Size(100, 20)
        Me.txtSubval.TabIndex = 129
        Me.txtSubval.Visible = False
        '
        'txtHandoverName
        '
        Me.txtHandoverName.BackColor = System.Drawing.Color.White
        Me.txtHandoverName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHandoverName.Location = New System.Drawing.Point(731, 162)
        Me.txtHandoverName.MaxLength = 50
        Me.txtHandoverName.Name = "txtHandoverName"
        Me.txtHandoverName.ReadOnly = True
        Me.txtHandoverName.Size = New System.Drawing.Size(10, 26)
        Me.txtHandoverName.TabIndex = 16
        Me.txtHandoverName.TabStop = False
        Me.txtHandoverName.Visible = False
        '
        'txtSpStatus
        '
        Me.txtSpStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSpStatus.Location = New System.Drawing.Point(365, 295)
        Me.txtSpStatus.MaxLength = 50
        Me.txtSpStatus.Name = "txtSpStatus"
        Me.txtSpStatus.Size = New System.Drawing.Size(57, 20)
        Me.txtSpStatus.TabIndex = 127
        Me.txtSpStatus.Text = "True"
        Me.txtSpStatus.Visible = False
        '
        'txtHandoverid
        '
        Me.txtHandoverid.BackColor = System.Drawing.Color.White
        Me.txtHandoverid.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHandoverid.Location = New System.Drawing.Point(699, 162)
        Me.txtHandoverid.MaxLength = 50
        Me.txtHandoverid.Name = "txtHandoverid"
        Me.txtHandoverid.Size = New System.Drawing.Size(10, 26)
        Me.txtHandoverid.TabIndex = 15
        Me.txtHandoverid.TabStop = False
        Me.txtHandoverid.Visible = False
        '
        'txtSufParent
        '
        Me.txtSufParent.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSufParent.Location = New System.Drawing.Point(299, 11)
        Me.txtSufParent.MaxLength = 50
        Me.txtSufParent.Name = "txtSufParent"
        Me.txtSufParent.Size = New System.Drawing.Size(57, 20)
        Me.txtSufParent.TabIndex = 128
        '
        'lblSplit
        '
        Me.lblSplit.AutoSize = True
        Me.lblSplit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSplit.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblSplit.Location = New System.Drawing.Point(362, 15)
        Me.lblSplit.Name = "lblSplit"
        Me.lblSplit.Size = New System.Drawing.Size(96, 13)
        Me.lblSplit.TabIndex = 127
        Me.lblSplit.Text = "SCAN SPLIT ID"
        Me.lblSplit.Visible = False
        '
        'txtsuf
        '
        Me.txtsuf.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsuf.Location = New System.Drawing.Point(204, 67)
        Me.txtsuf.MaxLength = 50
        Me.txtsuf.Name = "txtsuf"
        Me.txtsuf.Size = New System.Drawing.Size(57, 20)
        Me.txtsuf.TabIndex = 126
        '
        'txtWKSatation
        '
        Me.txtWKSatation.BackColor = System.Drawing.Color.White
        Me.txtWKSatation.Location = New System.Drawing.Point(137, 95)
        Me.txtWKSatation.Name = "txtWKSatation"
        Me.txtWKSatation.ReadOnly = True
        Me.txtWKSatation.Size = New System.Drawing.Size(119, 20)
        Me.txtWKSatation.TabIndex = 125
        Me.txtWKSatation.TabStop = False
        '
        'txtARCQTY
        '
        Me.txtARCQTY.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtARCQTY.Location = New System.Drawing.Point(770, 78)
        Me.txtARCQTY.Name = "txtARCQTY"
        Me.txtARCQTY.Size = New System.Drawing.Size(22, 20)
        Me.txtARCQTY.TabIndex = 124
        Me.txtARCQTY.TabStop = False
        Me.txtARCQTY.Visible = False
        '
        'txtC
        '
        Me.txtC.Location = New System.Drawing.Point(668, 15)
        Me.txtC.Name = "txtC"
        Me.txtC.Size = New System.Drawing.Size(61, 20)
        Me.txtC.TabIndex = 40
        Me.txtC.TabStop = False
        Me.txtC.Visible = False
        '
        'txtMachineid
        '
        Me.txtMachineid.BackColor = System.Drawing.Color.White
        Me.txtMachineid.Location = New System.Drawing.Point(137, 123)
        Me.txtMachineid.Name = "txtMachineid"
        Me.txtMachineid.Size = New System.Drawing.Size(68, 20)
        Me.txtMachineid.TabIndex = 4
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(21, 127)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(72, 13)
        Me.Label31.TabIndex = 120
        Me.Label31.Text = "Machine ID"
        '
        'txtTansID
        '
        Me.txtTansID.BackColor = System.Drawing.Color.White
        Me.txtTansID.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTansID.Location = New System.Drawing.Point(715, 162)
        Me.txtTansID.MaxLength = 50
        Me.txtTansID.Name = "txtTansID"
        Me.txtTansID.ReadOnly = True
        Me.txtTansID.Size = New System.Drawing.Size(14, 26)
        Me.txtTansID.TabIndex = 119
        Me.txtTansID.TabStop = False
        Me.txtTansID.Visible = False
        '
        'txtWKID
        '
        Me.txtWKID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWKID.Location = New System.Drawing.Point(137, 11)
        Me.txtWKID.MaxLength = 50
        Me.txtWKID.Name = "txtWKID"
        Me.txtWKID.Size = New System.Drawing.Size(156, 20)
        Me.txtWKID.TabIndex = 1
        '
        'butPre
        '
        Me.butPre.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.butPre.Image = CType(resources.GetObject("butPre.Image"), System.Drawing.Image)
        Me.butPre.Location = New System.Drawing.Point(607, 289)
        Me.butPre.Name = "butPre"
        Me.butPre.Size = New System.Drawing.Size(61, 41)
        Me.butPre.TabIndex = 28
        Me.butPre.UseVisualStyleBackColor = True
        '
        'butNext
        '
        Me.butNext.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.butNext.Image = CType(resources.GetObject("butNext.Image"), System.Drawing.Image)
        Me.butNext.Location = New System.Drawing.Point(684, 289)
        Me.butNext.Name = "butNext"
        Me.butNext.Size = New System.Drawing.Size(61, 41)
        Me.butNext.TabIndex = 29
        Me.butNext.UseVisualStyleBackColor = True
        '
        'txtOPQTY
        '
        Me.txtOPQTY.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtOPQTY.Location = New System.Drawing.Point(668, 95)
        Me.txtOPQTY.Name = "txtOPQTY"
        Me.txtOPQTY.Size = New System.Drawing.Size(96, 20)
        Me.txtOPQTY.TabIndex = 10
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(582, 99)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(84, 13)
        Me.Label30.TabIndex = 114
        Me.Label30.Text = "Received Qty"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.DarkRed
        Me.Label13.Location = New System.Drawing.Point(468, 338)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(312, 13)
        Me.Label13.TabIndex = 39
        Me.Label13.Text = "TRANSACTION HISTORY FOR THIS WORK CENTER"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Red
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label26.Location = New System.Drawing.Point(740, 43)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(41, 12)
        Me.Label26.TabIndex = 73
        Me.Label26.Text = "HHMM"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Red
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label25.Location = New System.Drawing.Point(740, 71)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(41, 12)
        Me.Label25.TabIndex = 72
        Me.Label25.Text = "HHMM"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Red
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label24.Location = New System.Drawing.Point(582, 71)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(73, 12)
        Me.Label24.TabIndex = 71
        Me.Label24.Text = "DD/MM/YYYY"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Red
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label23.Location = New System.Drawing.Point(582, 43)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(73, 12)
        Me.Label23.TabIndex = 70
        Me.Label23.Text = "DD/MM/YYYY"
        '
        'txtItemNoTemp
        '
        Me.txtItemNoTemp.Location = New System.Drawing.Point(299, 39)
        Me.txtItemNoTemp.Name = "txtItemNoTemp"
        Me.txtItemNoTemp.Size = New System.Drawing.Size(26, 20)
        Me.txtItemNoTemp.TabIndex = 69
        Me.txtItemNoTemp.TabStop = False
        Me.txtItemNoTemp.Visible = False
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Transparent
        Me.Panel7.Controls.Add(Me.butExit)
        Me.Panel7.Controls.Add(Me.butCancel)
        Me.Panel7.Controls.Add(Me.butDelete)
        Me.Panel7.Controls.Add(Me.butUpdate)
        Me.Panel7.Controls.Add(Me.butSave)
        Me.Panel7.Location = New System.Drawing.Point(8, 279)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(296, 67)
        Me.Panel7.TabIndex = 67
        '
        'butExit
        '
        Me.butExit.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butExit.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butExit.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butExit.Image = CType(resources.GetObject("butExit.Image"), System.Drawing.Image)
        Me.butExit.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butExit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butExit.Location = New System.Drawing.Point(230, 5)
        Me.butExit.Name = "butExit"
        Me.butExit.Size = New System.Drawing.Size(48, 59)
        Me.butExit.TabIndex = 27
        Me.butExit.Text = "Exit"
        Me.butExit.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.butExit.UseVisualStyleBackColor = False
        Me.butExit.Visible = False
        '
        'butCancel
        '
        Me.butCancel.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butCancel.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCancel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butCancel.Image = CType(resources.GetObject("butCancel.Image"), System.Drawing.Image)
        Me.butCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(176, 5)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(48, 59)
        Me.butCancel.TabIndex = 26
        Me.butCancel.Text = "Cancel"
        Me.butCancel.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butCancel.UseVisualStyleBackColor = False
        '
        'butDelete
        '
        Me.butDelete.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butDelete.Enabled = False
        Me.butDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butDelete.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelete.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butDelete.Image = CType(resources.GetObject("butDelete.Image"), System.Drawing.Image)
        Me.butDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butDelete.Location = New System.Drawing.Point(120, 5)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(48, 59)
        Me.butDelete.TabIndex = 25
        Me.butDelete.Text = "Delete"
        Me.butDelete.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butDelete.UseVisualStyleBackColor = False
        '
        'butUpdate
        '
        Me.butUpdate.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butUpdate.Enabled = False
        Me.butUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butUpdate.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butUpdate.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butUpdate.Image = CType(resources.GetObject("butUpdate.Image"), System.Drawing.Image)
        Me.butUpdate.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butUpdate.Location = New System.Drawing.Point(64, 5)
        Me.butUpdate.Name = "butUpdate"
        Me.butUpdate.Size = New System.Drawing.Size(48, 59)
        Me.butUpdate.TabIndex = 24
        Me.butUpdate.Text = "Update"
        Me.butUpdate.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butUpdate.UseVisualStyleBackColor = False
        '
        'butSave
        '
        Me.butSave.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butSave.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSave.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butSave.Image = CType(resources.GetObject("butSave.Image"), System.Drawing.Image)
        Me.butSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(8, 5)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(48, 59)
        Me.butSave.TabIndex = 20
        Me.butSave.Text = "Save"
        Me.butSave.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butSave.UseVisualStyleBackColor = False
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Ivory
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Controls.Add(Me.Label21)
        Me.GroupBox3.Controls.Add(Me.Label22)
        Me.GroupBox3.Controls.Add(Me.txtRejqty3)
        Me.GroupBox3.Controls.Add(Me.txtRejDes3)
        Me.GroupBox3.Controls.Add(Me.txtRejCode3)
        Me.GroupBox3.Location = New System.Drawing.Point(523, 194)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(282, 80)
        Me.GroupBox3.TabIndex = 66
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "REJECTED #3"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(4, 54)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(71, 13)
        Me.Label20.TabIndex = 57
        Me.Label20.Text = "Description"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(145, 15)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(26, 13)
        Me.Label21.TabIndex = 56
        Me.Label21.Text = "Qty"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(4, 24)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(36, 13)
        Me.Label22.TabIndex = 55
        Me.Label22.Text = "Code"
        '
        'txtRejqty3
        '
        Me.txtRejqty3.Location = New System.Drawing.Point(176, 20)
        Me.txtRejqty3.Name = "txtRejqty3"
        Me.txtRejqty3.Size = New System.Drawing.Size(49, 20)
        Me.txtRejqty3.TabIndex = 19
        '
        'txtRejDes3
        '
        Me.txtRejDes3.BackColor = System.Drawing.Color.White
        Me.txtRejDes3.Location = New System.Drawing.Point(77, 50)
        Me.txtRejDes3.Name = "txtRejDes3"
        Me.txtRejDes3.ReadOnly = True
        Me.txtRejDes3.Size = New System.Drawing.Size(156, 20)
        Me.txtRejDes3.TabIndex = 53
        Me.txtRejDes3.TabStop = False
        '
        'txtRejCode3
        '
        Me.txtRejCode3.Location = New System.Drawing.Point(77, 20)
        Me.txtRejCode3.Name = "txtRejCode3"
        Me.txtRejCode3.Size = New System.Drawing.Size(65, 20)
        Me.txtRejCode3.TabIndex = 18
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Ivory
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.txtRejqty2)
        Me.GroupBox2.Controls.Add(Me.txtRejDes2)
        Me.GroupBox2.Controls.Add(Me.txtRejCode2)
        Me.GroupBox2.Location = New System.Drawing.Point(245, 194)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(282, 80)
        Me.GroupBox2.TabIndex = 65
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "REJECTED #2"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(4, 54)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(71, 13)
        Me.Label17.TabIndex = 57
        Me.Label17.Text = "Description"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(145, 24)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(26, 13)
        Me.Label18.TabIndex = 56
        Me.Label18.Text = "Qty"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(4, 24)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(36, 13)
        Me.Label19.TabIndex = 55
        Me.Label19.Text = "Code"
        '
        'txtRejqty2
        '
        Me.txtRejqty2.Location = New System.Drawing.Point(176, 20)
        Me.txtRejqty2.Name = "txtRejqty2"
        Me.txtRejqty2.Size = New System.Drawing.Size(49, 20)
        Me.txtRejqty2.TabIndex = 17
        '
        'txtRejDes2
        '
        Me.txtRejDes2.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtRejDes2.Location = New System.Drawing.Point(77, 50)
        Me.txtRejDes2.Name = "txtRejDes2"
        Me.txtRejDes2.ReadOnly = True
        Me.txtRejDes2.Size = New System.Drawing.Size(156, 20)
        Me.txtRejDes2.TabIndex = 53
        Me.txtRejDes2.TabStop = False
        '
        'txtRejCode2
        '
        Me.txtRejCode2.Location = New System.Drawing.Point(77, 20)
        Me.txtRejCode2.Name = "txtRejCode2"
        Me.txtRejCode2.Size = New System.Drawing.Size(65, 20)
        Me.txtRejCode2.TabIndex = 16
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Ivory
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.txtRejqty1)
        Me.GroupBox1.Controls.Add(Me.txtRejDes1)
        Me.GroupBox1.Controls.Add(Me.txtRejCode1)
        Me.GroupBox1.Location = New System.Drawing.Point(0, 194)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(282, 80)
        Me.GroupBox1.TabIndex = 64
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "REJECTED #1"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(4, 54)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(71, 13)
        Me.Label16.TabIndex = 51
        Me.Label16.Text = "Description"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(145, 24)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(26, 13)
        Me.Label15.TabIndex = 50
        Me.Label15.Text = "Qty"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(4, 24)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(36, 13)
        Me.Label14.TabIndex = 49
        Me.Label14.Text = "Code"
        '
        'txtRejqty1
        '
        Me.txtRejqty1.Location = New System.Drawing.Point(176, 20)
        Me.txtRejqty1.Name = "txtRejqty1"
        Me.txtRejqty1.Size = New System.Drawing.Size(49, 20)
        Me.txtRejqty1.TabIndex = 15
        '
        'txtRejDes1
        '
        Me.txtRejDes1.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtRejDes1.Location = New System.Drawing.Point(77, 50)
        Me.txtRejDes1.Name = "txtRejDes1"
        Me.txtRejDes1.ReadOnly = True
        Me.txtRejDes1.Size = New System.Drawing.Size(156, 20)
        Me.txtRejDes1.TabIndex = 47
        Me.txtRejDes1.TabStop = False
        '
        'txtRejCode1
        '
        Me.txtRejCode1.Location = New System.Drawing.Point(77, 20)
        Me.txtRejCode1.Name = "txtRejCode1"
        Me.txtRejCode1.Size = New System.Drawing.Size(65, 20)
        Me.txtRejCode1.TabIndex = 14
        '
        'txtRejectedQty
        '
        Me.txtRejectedQty.Location = New System.Drawing.Point(467, 151)
        Me.txtRejectedQty.Name = "txtRejectedQty"
        Me.txtRejectedQty.Size = New System.Drawing.Size(111, 20)
        Me.txtRejectedQty.TabIndex = 13
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(350, 155)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(81, 13)
        Me.Label12.TabIndex = 62
        Me.Label12.Text = "Rejected Qty"
        '
        'txtCompletedQty
        '
        Me.txtCompletedQty.Location = New System.Drawing.Point(467, 123)
        Me.txtCompletedQty.Name = "txtCompletedQty"
        Me.txtCompletedQty.Size = New System.Drawing.Size(111, 20)
        Me.txtCompletedQty.TabIndex = 12
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(350, 127)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(89, 13)
        Me.Label11.TabIndex = 60
        Me.Label11.Text = "Completed Qty"
        '
        'txtReleasedqty
        '
        Me.txtReleasedqty.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtReleasedqty.Location = New System.Drawing.Point(467, 95)
        Me.txtReleasedqty.Name = "txtReleasedqty"
        Me.txtReleasedqty.ReadOnly = True
        Me.txtReleasedqty.Size = New System.Drawing.Size(111, 20)
        Me.txtReleasedqty.TabIndex = 10
        Me.txtReleasedqty.TabStop = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(350, 99)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(83, 13)
        Me.Label10.TabIndex = 58
        Me.Label10.Text = "Released Qty"
        '
        'dtpEnd
        '
        Me.dtpEnd.Location = New System.Drawing.Point(560, 67)
        Me.dtpEnd.Name = "dtpEnd"
        Me.dtpEnd.Size = New System.Drawing.Size(18, 20)
        Me.dtpEnd.TabIndex = 9
        Me.dtpEnd.TabStop = False
        '
        'txtEndTime
        '
        Me.txtEndTime.Location = New System.Drawing.Point(666, 67)
        Me.txtEndTime.MaxLength = 4
        Me.txtEndTime.Name = "txtEndTime"
        Me.txtEndTime.Size = New System.Drawing.Size(72, 20)
        Me.txtEndTime.TabIndex = 9
        '
        'txtStartTime
        '
        Me.txtStartTime.Location = New System.Drawing.Point(666, 39)
        Me.txtStartTime.MaxLength = 4
        Me.txtStartTime.Name = "txtStartTime"
        Me.txtStartTime.Size = New System.Drawing.Size(72, 20)
        Me.txtStartTime.TabIndex = 7
        '
        'txtEndDate
        '
        Me.txtEndDate.Location = New System.Drawing.Point(467, 67)
        Me.txtEndDate.MaxLength = 10
        Me.txtEndDate.Name = "txtEndDate"
        Me.txtEndDate.Size = New System.Drawing.Size(96, 20)
        Me.txtEndDate.TabIndex = 8
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(350, 71)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(111, 13)
        Me.Label9.TabIndex = 52
        Me.Label9.Text = "Date/Time Endted"
        '
        'txtStartDate
        '
        Me.txtStartDate.Location = New System.Drawing.Point(467, 39)
        Me.txtStartDate.MaxLength = 10
        Me.txtStartDate.Name = "txtStartDate"
        Me.txtStartDate.Size = New System.Drawing.Size(96, 20)
        Me.txtStartDate.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(350, 43)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(112, 13)
        Me.Label8.TabIndex = 50
        Me.Label8.Text = "Date/Time Started"
        '
        'txtOperators
        '
        Me.txtOperators.Location = New System.Drawing.Point(137, 151)
        Me.txtOperators.Name = "txtOperators"
        Me.txtOperators.Size = New System.Drawing.Size(34, 20)
        Me.txtOperators.TabIndex = 5
        '
        'txtItemNo
        '
        Me.txtItemNo.BackColor = System.Drawing.Color.White
        Me.txtItemNo.Location = New System.Drawing.Point(137, 39)
        Me.txtItemNo.Name = "txtItemNo"
        Me.txtItemNo.ReadOnly = True
        Me.txtItemNo.Size = New System.Drawing.Size(159, 20)
        Me.txtItemNo.TabIndex = 47
        Me.txtItemNo.TabStop = False
        '
        'txtOperationNo
        '
        Me.txtOperationNo.Location = New System.Drawing.Point(137, 67)
        Me.txtOperationNo.Name = "txtOperationNo"
        Me.txtOperationNo.Size = New System.Drawing.Size(61, 20)
        Me.txtOperationNo.TabIndex = 2
        '
        'txtWKSatationID
        '
        Me.txtWKSatationID.BackColor = System.Drawing.Color.White
        Me.txtWKSatationID.Location = New System.Drawing.Point(257, 95)
        Me.txtWKSatationID.Name = "txtWKSatationID"
        Me.txtWKSatationID.ReadOnly = True
        Me.txtWKSatationID.Size = New System.Drawing.Size(68, 20)
        Me.txtWKSatationID.TabIndex = 3
        Me.txtWKSatationID.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(21, 71)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(86, 13)
        Me.Label7.TabIndex = 43
        Me.Label7.Text = "Operation No."
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(21, 155)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(101, 13)
        Me.Label6.TabIndex = 42
        Me.Label6.Text = "No. of Operators"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(21, 43)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 13)
        Me.Label5.TabIndex = 41
        Me.Label5.Text = "Item No."
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(21, 99)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 13)
        Me.Label4.TabIndex = 40
        Me.Label4.Text = "Work Center"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(21, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(51, 13)
        Me.Label3.TabIndex = 39
        Me.Label3.Text = "Job No."
        '
        'dtpStart
        '
        Me.dtpStart.Location = New System.Drawing.Point(560, 39)
        Me.dtpStart.Name = "dtpStart"
        Me.dtpStart.Size = New System.Drawing.Size(18, 20)
        Me.dtpStart.TabIndex = 6
        Me.dtpStart.TabStop = False
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.LightYellow
        Me.Panel8.Controls.Add(Me.Label36)
        Me.Panel8.Controls.Add(Me.Label38)
        Me.Panel8.Controls.Add(Me.txtEmpid)
        Me.Panel8.Controls.Add(Me.txtEmpName)
        Me.Panel8.Location = New System.Drawing.Point(11, 5)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(807, 37)
        Me.Panel8.TabIndex = 123
        Me.Panel8.TabStop = True
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label36.Location = New System.Drawing.Point(339, 12)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(92, 13)
        Me.Label36.TabIndex = 73
        Me.Label36.Text = "Operator Name"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label38.Location = New System.Drawing.Point(3, 12)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(73, 13)
        Me.Label38.TabIndex = 72
        Me.Label38.Text = "Operator ID"
        '
        'txtEmpid
        '
        Me.txtEmpid.BackColor = System.Drawing.Color.White
        Me.txtEmpid.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpid.Location = New System.Drawing.Point(78, 5)
        Me.txtEmpid.MaxLength = 50
        Me.txtEmpid.Name = "txtEmpid"
        Me.txtEmpid.Size = New System.Drawing.Size(99, 26)
        Me.txtEmpid.TabIndex = 0
        '
        'txtEmpName
        '
        Me.txtEmpName.BackColor = System.Drawing.Color.White
        Me.txtEmpName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpName.Location = New System.Drawing.Point(431, 5)
        Me.txtEmpName.MaxLength = 50
        Me.txtEmpName.Name = "txtEmpName"
        Me.txtEmpName.ReadOnly = True
        Me.txtEmpName.Size = New System.Drawing.Size(276, 26)
        Me.txtEmpName.TabIndex = 118
        Me.txtEmpName.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Khaki
        Me.Panel3.Controls.Add(Me.butNotepad)
        Me.Panel3.Controls.Add(Me.butCal)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Location = New System.Drawing.Point(2, 2)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(883, 31)
        Me.Panel3.TabIndex = 36
        '
        'butNotepad
        '
        Me.butNotepad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.butNotepad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butNotepad.ForeColor = System.Drawing.Color.Transparent
        Me.butNotepad.Image = CType(resources.GetObject("butNotepad.Image"), System.Drawing.Image)
        Me.butNotepad.Location = New System.Drawing.Point(745, 1)
        Me.butNotepad.Name = "butNotepad"
        Me.butNotepad.Size = New System.Drawing.Size(20, 28)
        Me.butNotepad.TabIndex = 39
        Me.butNotepad.UseVisualStyleBackColor = True
        '
        'butCal
        '
        Me.butCal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.butCal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butCal.ForeColor = System.Drawing.Color.Transparent
        Me.butCal.Image = CType(resources.GetObject("butCal.Image"), System.Drawing.Image)
        Me.butCal.Location = New System.Drawing.Point(774, 1)
        Me.butCal.Name = "butCal"
        Me.butCal.Size = New System.Drawing.Size(20, 28)
        Me.butCal.TabIndex = 38
        Me.butCal.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(133, 13)
        Me.Label1.TabIndex = 37
        Me.Label1.Text = "EDIT OPERATION DATA "
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'Timer2
        '
        Me.Timer2.Enabled = True
        Me.Timer2.Interval = 1000
        '
        'frmDataEntry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(853, 749)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmDataEntry"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents butCal As System.Windows.Forms.Button
    Friend WithEvents butNotepad As System.Windows.Forms.Button
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents txtWKID As System.Windows.Forms.TextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtOperationNo As System.Windows.Forms.TextBox
    Friend WithEvents txtWKSatationID As System.Windows.Forms.TextBox
    Friend WithEvents txtOperators As System.Windows.Forms.TextBox
    Friend WithEvents txtItemNo As System.Windows.Forms.TextBox
    Friend WithEvents txtStartDate As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtEndDate As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtReleasedqty As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dtpEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtEndTime As System.Windows.Forms.TextBox
    Friend WithEvents txtStartTime As System.Windows.Forms.TextBox
    Friend WithEvents dtpStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtRejectedQty As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtCompletedQty As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents lstv As System.Windows.Forms.ListView
    Friend WithEvents txtRejDes1 As System.Windows.Forms.TextBox
    Friend WithEvents txtRejCode1 As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtRejqty3 As System.Windows.Forms.TextBox
    Friend WithEvents txtRejDes3 As System.Windows.Forms.TextBox
    Friend WithEvents txtRejCode3 As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtRejqty2 As System.Windows.Forms.TextBox
    Friend WithEvents txtRejDes2 As System.Windows.Forms.TextBox
    Friend WithEvents txtRejCode2 As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtRejqty1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butDelete As System.Windows.Forms.Button
    Friend WithEvents butUpdate As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents txtItemNoTemp As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents lbltotRej As System.Windows.Forms.Label
    Friend WithEvents lbltotCom As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtC As System.Windows.Forms.TextBox
    Friend WithEvents txtEmpid As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtOPQTY As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents lblTotOPQTY As System.Windows.Forms.Label
    Friend WithEvents butPre As System.Windows.Forms.Button
    Friend WithEvents butNext As System.Windows.Forms.Button
    Friend WithEvents lblMsgBox As System.Windows.Forms.Label
    Friend WithEvents txtTansID As System.Windows.Forms.TextBox
    Friend WithEvents txtEmpName As System.Windows.Forms.TextBox
    Friend WithEvents butExit As System.Windows.Forms.Button
    Friend WithEvents txtMachineid As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txtHandoverName As System.Windows.Forms.TextBox
    Friend WithEvents txtHandoverid As System.Windows.Forms.TextBox
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents txtARCQTY As System.Windows.Forms.TextBox
    Friend WithEvents txtWKSatation As System.Windows.Forms.TextBox
    Friend WithEvents txtsuf As System.Windows.Forms.TextBox
    Friend WithEvents lblSplit As System.Windows.Forms.Label
    Friend WithEvents txtSufParent As System.Windows.Forms.TextBox
    Friend WithEvents txtSpStatus As System.Windows.Forms.TextBox
    Friend WithEvents txtSubval As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents ButHand As System.Windows.Forms.Button
End Class
