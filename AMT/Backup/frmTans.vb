Imports System.Data.SqlClient
Public Class frmTans
    Public JobNo As String
    Public Jobdate As String
    Public JobItem As String
    Public JobQty As String

    Dim dr As SqlDataReader
    Private Sub frmTans_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With lstv
            .Columns.Add("Operation No", 100, HorizontalAlignment.Left)
            .Columns.Add(" Work Center", 100, HorizontalAlignment.Left)
            ' .Columns.Add("Operation No", 100, HorizontalAlignment.Left)
            .Columns.Add("Date/Time Started", 150, HorizontalAlignment.Left)
            .Columns.Add("Date/Time End", 150, HorizontalAlignment.Left)
            .Columns.Add("Received", 80, HorizontalAlignment.Left)
            .Columns.Add("Completed", 80, HorizontalAlignment.Left)
            .Columns.Add("Rejected", 80, HorizontalAlignment.Left)
        End With
        Load_ListView()
    End Sub


    Sub Load_ListView()
        lstv.Items.Clear()
        Try
            lbljobno.Text = JobNo
            lblJobdate.Text = Jobdate
            lblItemName.Text = JobItem
            lblreleased.Text = JobQty
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                Dim sql As String = " select A.*,B._description from tbJobRoute A,tbwc B where A._wc =B._wc and   A._job='" & JobNo & "' order by A._operationNo"
                .CommandText = " select A.*,B._description from tbJobRoute A,tbwc B where A._wc =B._wc and   A._job='" & JobNo & "' order by A._operationNo"
                Dim AD As New SqlDataAdapter
                AD.SelectCommand = com
                Dim DS As New DataSet
                AD.Fill(DS, "tbJobRoute")
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select *   from tbJobTrans where  _job='" & JobNo & "' order by _oper_num"
                Dim ADNew As New SqlDataAdapter
                ADNew.SelectCommand = com
                Dim DSTans As New DataSet
                ADNew.Fill(DSTans, "tbJobTrans")

                If DS.Tables(0).Rows.Count > 0 Then
                    Dim LCQty As Double
                    Dim LrecQty As Double
                    Dim LRejQty As Double

                    Dim i As Integer = 0
                    Dim stopno As Integer = 0
                    For i = 0 To DS.Tables(0).Rows.Count - 1

                        Dim Sd As Double = 0
                        Dim Ed As Double = 0
                        Dim StarDT As String = "-"
                        Dim EndDT As String = "-"
                        Dim RecQTY As Double = 0.0
                        Dim ComQTY As Double = 0.0
                        Dim RejQTY As Double = 0.0


                        Dim NDR() As DataRow = DSTans.Tables(0).Select("_oper_num=" & DS.Tables(0).Rows(i).Item("_operationNo"))
                        Dim j As Integer = 0
                        Dim st As Boolean = False

                        Dim ls As New ListViewItem(Trim(DS.Tables(0).Rows(i).Item("_operationNo")))
                        ls.SubItems.Add(DS.Tables(0).Rows(i).Item("_description"))

                        If NDR.Length > 0 Then
                            For j = 0 To NDR.Length - 1
                                If Sd = 0 Then
                                    Sd = CDbl((NDR(j).Item("_start_Date")))
                                ElseIf Sd > CDbl((NDR(j).Item("_start_Date"))) Then
                                    Sd = CDbl((NDR(j).Item("_start_Date")))
                                End If

                                If Ed = 0 Then
                                    If NDR(j).Item("_end_Date") = 0 Then
                                        st = True
                                        'Else
                                        '   Ed = CDbl((NDR(j).Item("_end_Date")) & "." & (NDR(j).Item("_end_time")))
                                    End If
                                    If st = False Then
                                        If Ed < CDbl((NDR(j).Item("_end_Date"))) Then
                                            Ed = CDbl((NDR(j).Item("_end_Date")))
                                        End If
                                    End If
                                End If

                                RecQTY = RecQTY + NDR(j).Item("_qty_op_qty")
                                ComQTY = ComQTY + NDR(j).Item("_qty_complete")
                                RejQTY = RejQTY + NDR(j).Item("_qty_scrapped")
                                LRejQty = LRejQty + RejQTY
                            Next




                        End If

                        If stopno < DS.Tables(0).Rows(i).Item("_operationNo") Then
                            stopno = DS.Tables(0).Rows(i).Item("_operationNo")
                            LrecQty = RecQTY
                            LCQty = ComQTY
                        End If
                        If Sd <> 0 Then
                            ls.SubItems.Add(Format(DateTime.FromOADate(Sd), "dd/MM/yyyy HH:mm"))
                        Else
                            ls.SubItems.Add("-")
                        End If
                        If Ed <> 0 Then
                            ls.SubItems.Add(Format(DateTime.FromOADate(Ed), "dd/MM/yyyy HH:mm"))
                        Else
                            ls.SubItems.Add("-")
                        End If
                        ls.SubItems.Add(Format(RecQTY, "00.00"))
                        ls.SubItems.Add(Format(ComQTY, "00.00"))
                        ls.SubItems.Add(Format(RejQTY, "00.00"))
                        If RecQTY = 0 And ComQTY = 0 And RejQTY = 0 Then
                            lstv.Items.Add(ls).ForeColor = Color.Black
                        ElseIf RecQTY > ComQTY + RejQTY Then
                            lstv.Items.Add(ls).ForeColor = Color.Orange
                        ElseIf RecQTY = ComQTY + RejQTY Then
                            lstv.Items.Add(ls).ForeColor = Color.Green
                        ElseIf RejQTY > ComQTY + RejQTY Then
                            lstv.Items.Add(ls).ForeColor = Color.Red
                        Else
                            lstv.Items.Add(ls).ForeColor = Color.Black
                        End If

                    Next
                    lblReceived.Text = Format(LrecQty, "00.00")
                    lblqtycompleted.Text = Format(LrecQty, "00.00")
                    lblqtyscrapped.Text = Format(LRejQty, "00.00")
                End If


            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
        End Try
    End Sub

    Private Sub lstv_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.DoubleClick

        Dim obj As New frmTransDetails
        obj.JobNo = JobNo
        obj.Jobdate = Jobdate
        obj.JobItem = JobItem
        obj.JobQty = JobQty
        obj.JobOP = CInt(lstv.SelectedItems(0).Text)
        obj.JobWC = lstv.SelectedItems(0).SubItems(1).Text
        'obj.Width = Me.Width
        'obj.Height = Me.Height
        'obj.Left = Me.Left
        'obj.Top = Me.Top
        obj.ShowDialog()


     
    End Sub

    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub
End Class