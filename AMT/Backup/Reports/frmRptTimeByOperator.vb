Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Microsoft.VisualBasic.DateAndTime
Public Class frmRptTimeByOperator

    Dim clsM As New clsMain
    Public DSItem As New DataSet
    Public DSWC As New DataSet
    Dim dsrsg As DSRlCycleTime
    Dim ArrexWeek() As Integer
    Dim ArrexOP() As Integer
    Dim ArrexRes() As String
    Dim ArrexItem() As String
    Dim ArrHasAll() As String
    Dim strReportDate As String
    Private Sub frmRptTimeByOperator_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dtpFrom.Value = Now
        dtpTo.Value = Now
        txtFrom.Text = Format(Now, "dd/MM/yyyy")
        txtTo.Text = Format(Now, "dd/MM/yyyy")
        ResourceData()
        GetWC()
        GetItem()
    End Sub

    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtTo.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        dtpTo.Checked = False
    End Sub
    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtFrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False
    End Sub
    Sub GetWC()
        cmbWC.Items.Clear()
        DSWC.Tables.Clear()
        cmbWC.Items.Add(New DataItem("-Select-", "-Select-"))
        Dim selcon As String = ""
        Dim sql As String = ""
        If cmbResource.Text <> "-Select-" Then
            sql = "select * from tbWC where _wc in(select _WC from tbWorkStation where _rid in(select _rid from tbResourceGroup where _rGroupid='" & Trim(cmbResource.Text) & "')) order by _wc"

        Else
            sql = "select * from tbWC order by _wc"
        End If
        Try
            Dim ds As New DataSet
            ds = clsM.GetDataset(sql, "tbWC")
            DSWC = ds
            If IsDBNull(ds) = False Then
                Dim i As Integer
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            cmbWC.Items.Add(New DataItem(Trim(.Item("_wc")), Trim(.Item("_wc")) & "  :  " & Trim(.Item("_description")) & ""))
                        End With
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
        cmbWC.Text = "-Select-"
    End Sub
    Sub ResourceData()
        Dim ds As New DataSet
        cmbResource.Items.Clear()
        cmbResource.Items.Add(New DataItem("-Select-", "-Select-"))
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                'Dim DI As DataItem

                ds = clsM.GetDataset("select _rGroupid,_rGroupName,_rid from tbResourceGroup order by _rGroupid", "tbWC")
                If IsDBNull(ds) = False Then
                    Dim i As Integer
                    If ds.Tables(0).Rows.Count > 0 Then
                        For i = 0 To ds.Tables(0).Rows.Count - 1
                            With ds.Tables(0).Rows(i)
                                cmbResource.Items.Add(New DataItem(.Item("_rGroupName") & "||" & .Item("_rid"), .Item("_rGroupid")))
                            End With
                        Next
                    End If
                End If
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
        cmbResource.Text = "-Select-"
    End Sub
    Sub GetItem()
        DSItem.Tables.Clear()
        lstSelItem.Items.Clear()
        lstItem.Items.Clear()
        Try
            Dim ds As New DataSet
            'ds = clsM.GetDataset("select * from tbItem where  order by _itemCode", "tbItem")
            ds = clsM.GetDataset("select _operatorID,_operatorName from tbOperator  order by _operatorID", "tbItem")
            '
            DSItem = ds
            If IsDBNull(ds) = False Then
                Dim i As Integer
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            lstItem.Items.Add(New DataItem(Trim(.Item("_operatorID")), Trim(.Item("_operatorID")) & "  :  " & Trim(.Item("_operatorName")) & ""))
                        End With
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try

    End Sub
    Private Sub cmbResource_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbResource.SelectedIndexChanged
        GetWC()
    End Sub
    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub
    Private Sub butAssSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssSingle.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstItem.SelectedItems.Count - 1
                lstSelItem.Items.Add(lstItem.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstItem.SelectedItems.Count - 1
                lstItem.Items.Remove(lstItem.SelectedItems.Item(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butAssMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssMulti.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstItem.Items.Count - 1
                lstSelItem.Items.Add(lstItem.Items(i))
            Next
            i = 0
            For i = 0 To lstItem.Items.Count - 1
                lstItem.Items.Remove(lstItem.Items(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butDelSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelSingle.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstSelItem.SelectedItems.Count - 1
                lstItem.Items.Add(lstSelItem.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstSelItem.SelectedItems.Count - 1
                lstSelItem.Items.Remove(lstSelItem.SelectedItems.Item(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub
    Private Sub butDelMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelMulti.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstSelItem.Items.Count - 1
                lstItem.Items.Add(lstSelItem.Items(i))
            Next
            i = 0
            For i = 0 To lstSelItem.Items.Count - 1
                lstSelItem.Items.Remove(lstSelItem.Items(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        RViewer.DataBindings.Clear()
        GC.GetGeneration(RViewer)
        RViewer.Visible = False
        butClose.Visible = False
        ' butExport.Visible = False
        PB1.Value = 0
        PB1.Minimum = 0
        PB1.Maximum = 0
        Panel1.Visible = True
    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click

        comletedTransaction()
        TmVari = 2

        '  Parent.Focus()

    End Sub
    Sub comletedTransaction()
        Try
            If Trim(txtFrom.Text) = "" Or Trim(txtTo.Text) = "" Then
                MsgBox("Please enter the date!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim dblSd As Double = clsM.CheckDate(txtFrom.Text)
            If dblSd = 0 Then
                MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If

            Dim dblEd As Double = clsM.CheckDate(txtTo.Text)
            If dblEd = 0 Then
                MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim s() As String = Split(txtTo.Text, "/")
            dsrsg = New DSRlCycleTime
            Dim Dateto As Date = DateSerial(s(2), s(1), s(0))
            Dateto = DateAdd(DateInterval.Minute, -1, DateAdd(DateInterval.Day, 1, Dateto))
            dblEd = 0
            dblEd = Dateto.ToOADate

            Dim HASRe As New Hashtable
            Dim StItem As Boolean = False
            If lstSelItem.Items.Count > 0 Then
                Dim k As Integer
                For k = 0 To lstSelItem.Items.Count - 1
                    HASRe.Add(lstSelItem.Items(k).id, lstSelItem.Items(k).id)
                Next
            Else
                StItem = True
                HASRe.Add("ALL", "ALL")

            End If
            strReportDate = Format(DateTime.FromOADate(dblSd), "dd MMM yyyy") & " - " & Format(DateTime.FromOADate(dblEd), "dd MMM yyyy")
            'Dim strReportDate As String = txtFrom.Text & "-" & txtTo.Text
            Dim CreatedDate As String = Format(Now, "dd MMM yyyy")

            Dim DSrep As New DataSet
            Dim DSrepHis As New DataSet
            Dim Strsql As String
            Dim strResource As String
            Dim DSRecGroup As New DataSet
            Dim DSItem As New DataSet
            DSItem = clsM.GetDataset("select * from tbItem", "tbItem")
            Dim DSRecWC As New DataSet
            DSRecWC = clsM.GetDataset("select * from tbWC", "tbWC")
            DSRecGroup = clsM.GetDataset("select A.*,B.* from tbWC A,tbResourceGroup B,tbWorkStation C where C._wc=A._wc and C._rid=B._rid", "tbRec")

            Dim hashFindUnique As New Hashtable
            Dim ArrResouce(0) As String
            Dim ArrDate(0) As Date
            Dim ArrResouceDesc(0) As String
            Dim ArrWC(0) As String
            Dim ArrWCDesc(0) As String
            Dim Arrhr(0) As Double
            Dim ArrMint(0) As Double
            Dim ArrtotMint(0) As Double
            Dim Arrtothr(0) As Double
            Dim ArrOperatorNo(0) As String
            Dim ArrOperatorName(0) As String
            Dim ArrQty(0) As Double



            If cmbResource.Text <> "" And cmbResource.Text <> "-Select-" And cmbWC.Text = "-Select-" Then
                Dim Strx As String() = Split(cmbResource.SelectedItem.id, "||")
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped,A._emp_num ,A._emp_name from tbjobtrans A,tbJobTransMain B where A._Status='COMPLETED' and A._job=b._job and A._oper_num=B._oper_num   and  A._end_Date between " & dblSd & " and " & dblEd & " and B._wc in (select _wc from tbWorkStation where _rid=" & Val(Strx(1)) & ") order by A._Job"
            ElseIf cmbWC.Text <> "-Select-" Then
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped ,A._emp_num ,A._emp_name from tbjobtrans A,tbJobTransMain B where A._Status='COMPLETED' and A._job=b._job and A._oper_num=B._oper_num   and  A._end_Date between " & dblSd & " and " & dblEd & " and B._wc ='" & Replace(cmbWC.SelectedItem.id, "'", "''") & "' order by A._Job"
            Else
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped ,A._emp_num ,A._emp_name from tbjobtrans A,tbJobTransMain B where A._Status='COMPLETED' and  A._job=b._job and A._oper_num=B._oper_num   and  A._end_Date between " & dblSd & " and " & dblEd & " order by A._Job"
            End If
            DSrep = clsM.GetDataset(Strsql, "tbJobTransMain")



            If cmbResource.Text <> "" And cmbResource.Text <> "-Select-" And cmbWC.Text = "-Select-" Then
                Dim Strx As String() = Split(cmbResource.SelectedItem.id, "||")
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped,A._emp_num ,A._emp_name  from tbjobtransHis A,tbJobTransMain B where A._Status='COMPLETED' and A._job=b._job and A._oper_num=B._oper_num   and  A._end_Date between " & dblSd & " and " & dblEd & " and B._wc in (select _wc from tbWorkStation where _rid=" & Val(Strx(1)) & ") order by A._Job"
            ElseIf cmbWC.Text <> "-Select-" Then
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped,A._emp_num ,A._emp_name  from tbjobtransHis A,tbJobTransMain B where A._Status='COMPLETED' and A._job=b._job and A._oper_num=B._oper_num   and  A._end_Date between " & dblSd & " and " & dblEd & " and B._wc ='" & Replace(cmbWC.SelectedItem.id, "'", "''") & "' order by A._Job"
            Else
                Strsql = "select A._job,A._item,A._jobDate,A._oper_num,A._wc,A._trans_type,A._start_Date,A._end_Date,A._emp_num,A._emp_name,A._qty_complete,A._qty_scrapped ,A._emp_num ,A._emp_name from tbjobtransHis A,tbJobTransMain B where A._Status='COMPLETED' and A._job=b._job and A._oper_num=B._oper_num   and A._end_Date between " & dblSd & " and " & dblEd & " order by A._Job"
            End If
            DSrepHis = clsM.GetDataset(Strsql, "tbJobTransMain")





            Dim totqty As Double
            Dim drRp As DataRow
            Dim recount As Boolean = False
            PB1.Value = 0
            PB1.Minimum = 0
            PB1.Maximum = 0
            Dim IA As Integer = 0
            Dim j As Integer = 0
            If DSrep.Tables(0).Rows.Count > 0 Then
                Dim i As Integer

                PB1.Maximum = DSrep.Tables(0).Rows.Count + DSrepHis.Tables(0).Rows.Count

                For i = 0 To DSrep.Tables(0).Rows.Count - 1
                    With DSrep.Tables(0).Rows(i)
                        If .Item("_qty_complete") <> 0 Or .Item("_qty_scrapped") <> 0 Then
                            recount = True
                            Dim runt As Boolean = False
                            Dim strResCode As String = ""
                            Dim strResDesc As String = ""
                            Dim strWC As String = ""
                            Dim strDate As String = ""
                            Dim strWCdesc As String = ""
                            Dim ArrItem As String() = Split(.Item("_item"), "-")
                            Dim strCont As String = .Item("_emp_num")

                            If HASRe.ContainsKey(strCont) = True Then
                                runt = True
                            Else
                                runt = False
                            End If


                            If runt = True Then


                                Dim strConst As String = ""
                                Dim DRResou() As DataRow = DSRecGroup.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                If DRResou.Length > 0 Then
                                    strResCode = DRResou(0).Item("_rGroupID")
                                    strResDesc = DRResou(0).Item("_rGroupName")
                                    strWC = DRResou(0).Item("_wc")
                                    strWCdesc = DRResou(0).Item("_description")

                                End If
                                Dim dtCondate As Date = DateTime.FromOADate(.Item("_end_Date"))
                                strDate = Format(dtCondate, "yyyyMMdd")
                                Dim strFindid As String = strDate & .Item("_emp_num") & strWC
                                If hashFindUnique.Contains(strFindid) = False Then

                                    ReDim Preserve ArrResouce(j)
                                    ReDim Preserve ArrResouceDesc(j)
                                    ReDim Preserve ArrWC(j)
                                    ReDim Preserve ArrWCDesc(j)
                                    ReDim Preserve Arrhr(j)
                                    ReDim Preserve ArrMint(j)
                                    ReDim Preserve ArrtotMint(j)
                                    ReDim Preserve ArrOperatorNo(j)
                                    ReDim Preserve ArrOperatorName(j)
                                    ReDim Preserve ArrDate(j)
                                    ReDim Preserve ArrQty(j)
                                    ArrOperatorNo(j) = .Item("_emp_num")
                                    ArrOperatorName(j) = .Item("_emp_name")
                                    hashFindUnique.Add(strFindid, j)
                                    ArrResouce(j) = strResCode
                                    ArrResouceDesc(j) = strResCode

                                    ArrDate(j) = DateSerial(Year(dtCondate), Month(dtCondate), Day(dtCondate))
                                    ArrWC(j) = strWC
                                    ArrWCDesc(j) = strWCdesc
                                    Dim sd, Ed As Date
                                    sd = DateTime.FromOADate(.Item("_start_Date"))
                                    Ed = DateTime.FromOADate(.Item("_end_Date"))
                                    ArrQty(j) = .Item("_qty_complete") + .Item("_qty_scrapped")
                                    Dim dblTot As Double = DateDiff(DateInterval.Second, sd, Ed)
                                    ArrtotMint(j) = dblTot
                                    Dim dblMint As Double = ArrtotMint(j) / 60
                                    'dblTot = DateDiff(DateInterval.Minute, sd, Ed)
                                    Dim dblHrs As Double = (ArrtotMint(j) / 60) / 60
                                    Arrhr(j) = dblHrs
                                    ArrMint(j) = dblMint
                                    j = j + 1
                                Else
                                    Dim m As Integer = hashFindUnique(strFindid)
                                    If ArrDate(m) < DateSerial(Year(dtCondate), Month(dtCondate), Day(dtCondate)) Then
                                        ArrDate(m) = DateSerial(Year(dtCondate), Month(dtCondate), Day(dtCondate))
                                    End If
                                    Dim sd, Ed As Date
                                    sd = DateTime.FromOADate(.Item("_start_Date"))
                                    Ed = DateTime.FromOADate(.Item("_end_Date"))
                                    ArrQty(m) = ArrQty(m) + .Item("_qty_complete") + .Item("_qty_scrapped")
                                    Dim dblTot As Double = DateDiff(DateInterval.Second, sd, Ed)
                                    ArrtotMint(m) = ArrtotMint(m) + dblTot
                                    Dim dblMint As Double = ArrtotMint(m) / 60
                                    'dblTot = DateDiff(DateInterval.Minute, sd, Ed)
                                    Dim dblHrs As Double = (ArrtotMint(m) / 60) / 60
                                    Arrhr(m) = dblHrs
                                    ArrMint(m) = dblMint
                                End If
                            End If
                        End If
                    End With
                    PB1.Value = PB1.Value + 1
                Next
            End If




            If DSrepHis.Tables(0).Rows.Count > 0 Then
                Dim i As Integer


                For i = 0 To DSrepHis.Tables(0).Rows.Count - 1
                    With DSrepHis.Tables(0).Rows(i)
                        If .Item("_qty_complete") <> 0 Or .Item("_qty_scrapped") <> 0 Then
                            recount = True
                            Dim runt As Boolean = False
                            Dim strResCode As String = ""
                            Dim strResDesc As String = ""
                            Dim strWC As String = ""
                            Dim strWCdesc As String = ""
                            Dim strDate As String = ""
                            Dim ArrItem As String() = Split(.Item("_item"), "-")
                            Dim strCont As String = .Item("_emp_num")

                            If UCase(Mid(.Item("_item"), 1, 2)) = "SC" Then
                                strCont = .Item("_item")
                            End If

                            If HASRe.ContainsKey(strCont) = True Then
                                runt = True
                            Else
                                runt = False
                            End If


                            If runt = True Then

                                Dim strConst As String = ""
                                Dim DRResou() As DataRow = DSRecGroup.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                If DRResou.Length > 0 Then
                                    strResCode = DRResou(0).Item("_rGroupID")
                                    strResDesc = DRResou(0).Item("_rGroupName")
                                    strWC = DRResou(0).Item("_wc")
                                    strWCdesc = DRResou(0).Item("_description")

                                End If
                                Dim dtCondate As Date = DateTime.FromOADate(.Item("_end_Date"))
                                strDate = Format(dtCondate, "yyyyMMdd")
                                Dim strFindid As String = strDate & .Item("_emp_num") & strWC

                                If hashFindUnique.Contains(strFindid) = False Then
                                    'If strResCode = "FURNCE" And strWC = "DEGRSE" Then
                                    '    MsgBox("Hi")

                                    'End If
                                    ReDim Preserve ArrResouce(j)
                                    ReDim Preserve ArrResouceDesc(j)
                                    ReDim Preserve ArrWC(j)
                                    ReDim Preserve ArrWCDesc(j)
                                    ReDim Preserve Arrhr(j)
                                    ReDim Preserve ArrMint(j)
                                    ReDim Preserve ArrtotMint(j)
                                    ReDim Preserve ArrOperatorNo(j)
                                    ReDim Preserve ArrOperatorName(j)
                                    ReDim Preserve ArrQty(j)
                                    ReDim Preserve ArrDate(j)
                                    hashFindUnique.Add(strFindid, j)
                                    ArrResouce(j) = strResCode
                                    ArrResouceDesc(j) = strResCode
                                    ArrWC(j) = strWC
                                    ArrWCDesc(j) = strWCdesc
                                    Dim sd, Ed As Date
                                    ArrOperatorNo(j) = .Item("_emp_num")
                                    ArrOperatorName(j) = .Item("_emp_name")
                                    sd = DateTime.FromOADate(.Item("_start_Date"))
                                    Ed = DateTime.FromOADate(.Item("_end_Date"))

                                    ArrDate(j) = DateSerial(Year(dtCondate), Month(dtCondate), Day(dtCondate))
                                    Dim dblTot As Double = DateDiff(DateInterval.Second, sd, Ed)
                                    ArrtotMint(j) = dblTot
                                    Dim dblMint As Double = ArrtotMint(j) / 60
                                    'dblTot = DateDiff(DateInterval.Minute, sd, Ed)
                                    Dim dblHrs As Double = (ArrtotMint(j) / 60) / 60
                                    ArrQty(j) = .Item("_qty_complete") + .Item("_qty_scrapped")
                                    Arrhr(j) = dblHrs
                                    ArrMint(j) = dblMint
                                    j = j + 1
                                Else
                                    Dim m As Integer = hashFindUnique(strFindid)
                                    If ArrDate(m) < DateSerial(Year(dtCondate), Month(dtCondate), Day(dtCondate)) Then
                                        ArrDate(m) = DateSerial(Year(dtCondate), Month(dtCondate), Day(dtCondate))
                                    End If

                                    Dim sd, Ed As Date
                                    sd = DateTime.FromOADate(.Item("_start_Date"))
                                    Ed = DateTime.FromOADate(.Item("_end_Date"))
                                    Dim dblTot As Double = DateDiff(DateInterval.Second, sd, Ed)
                                    ArrtotMint(m) = ArrtotMint(m) + dblTot
                                    Dim dblMint As Double = ArrtotMint(m) / 60
                                    'dblTot = DateDiff(DateInterval.Minute, sd, Ed)
                                    Dim dblHrs As Double = (ArrtotMint(m) / 60) / 60
                                    ArrQty(m) = ArrQty(m) + .Item("_qty_complete") + .Item("_qty_scrapped")
                                    Arrhr(m) = dblHrs
                                    ArrMint(m) = dblMint
                                End If
                            End If
                        End If
                    End With
                    PB1.Value = PB1.Value + 1
                Next
            End If










            If recount = True Then
                Dim i As Integer
                For i = 0 To ArrResouce.Length - 1
                    drRp = dsrsg.Tables("tbCTOp").NewRow
                    drRp("sn") = i + 1
                    drRp("Title") = strReportDate 'Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                    drRp("CreateDate") = CreatedDate
                    drRp("ReportDate") = strReportDate
                    drRp("FGCode") = ""
                    drRp("FGDesc") = ""
                    drRp("Job") = ""
                    drRp("OP") = ""
                    drRp("WC") = ArrWC(i)
                    drRp("WCDesc") = ArrWCDesc(i)
                    drRp("Resource") = ArrResouce(i)
                    drRp("ResourceDesc") = ArrResouceDesc(i)
                    drRp("hrvs") = Arrhr(i)
                    drRp("mints") = ArrMint(i)
                    drRp("OperatorNo") = Mid(ArrOperatorNo(i), 1, 4)
                    drRp("OperatorName") = ArrOperatorName(i)
                    drRp("OpDate") = ArrDate(i)
                    drRp("Qty") = ArrQty(i)
                    If drRp("Qty") = 0 Then
                        drRp("PreHR") = 0
                        drRp("PerMin") = 0
                    Else
                        drRp("PreHR") = Arrhr(i) / drRp("Qty")
                        drRp("PerMin") = ArrMint(i) / drRp("Qty")
                    End If

                    dsrsg.Tables("tbCTOp").Rows.Add(drRp)

                Next
            Else
                drRp = dsrsg.Tables("tbCTOp").NewRow
                drRp("sn") = 0
                drRp("Title") = strReportDate 'Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                drRp("CreateDate") = CreatedDate
                drRp("ReportDate") = strReportDate
                drRp("FGCode") = ""
                drRp("FGDesc") = ""
                drRp("Job") = ""
                drRp("OP") = ""
                drRp("WC") = ""
                drRp("WCDesc") = ""
                drRp("Resource") = ""
                drRp("ResourceDesc") = ""
                drRp("hrvs") = 0
                drRp("mints") = 0
                drRp("OperatorNo") = ""
                drRp("OperatorName") = ""
                drRp("Qty") = 0
                drRp("PreHR") = 0
                drRp("PerMin") = 0
                dsrsg.Tables("tbCTOp").Rows.Add(drRp)
            End If
           
            Dim cry As New cryCycleOP
            'Dim cry As New cryCyTimeSumResourceGroup
            cry.SetDataSource(dsrsg)
            RViewer.ReportSource = cry
            Panel1.Visible = False
            RViewer.Visible = True
            RViewer.Enabled = True
            Me.Activate()
            Application.DoEvents()
            RViewer.Refresh()
            butClose.Visible = True



            ' butExport.Visible = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub
End Class