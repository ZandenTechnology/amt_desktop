<[Global].Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
[Partial] Class frmRPTItemStart
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components [IsNot] Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRPTItemStart))
        Me.butExport = New System.Windows.Forms.Button
        Me.butClose = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblReportTit = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.ckbRework = New System.Windows.Forms.CheckBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.butDelMulti = New System.Windows.Forms.Button
        Me.butDelSingle = New System.Windows.Forms.Button
        Me.butAssMulti = New System.Windows.Forms.Button
        Me.butAssSingle = New System.Windows.Forms.Button
        Me.lstSelItem = New System.Windows.Forms.ListBox
        Me.lstItem = New System.Windows.Forms.ListBox
        Me.P1 = New System.Windows.Forms.Panel
        Me.PB1 = New System.Windows.Forms.ProgressBar
        Me.dtpFrom = New System.Windows.Forms.DateTimePicker
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtFrom = New System.Windows.Forms.TextBox
        Me.butEXIT = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.sfdSave = New System.Windows.Forms.SaveFileDialog
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.PB2 = New System.Windows.Forms.ProgressBar
        Me.RViewer = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.ckbTransfer = New System.Windows.Forms.CheckBox
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.P1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'butExport
        '
        Me.butExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butExport.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butExport.Image = CType(resources.GetObject("butExport.Image"), System.Drawing.Image)
        Me.butExport.Location = New System.Drawing.Point(663, 6)
        Me.butExport.Name = "butExport"
        Me.butExport.Size = New System.Drawing.Size(30, 23)
        Me.butExport.TabIndex = 131
        Me.butExport.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butExport.UseVisualStyleBackColor = True
        Me.butExport.Visible = False
        '
        'butClose
        '
        Me.butClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butClose.BackgroundImage = CType(resources.GetObject("butClose.BackgroundImage"), System.Drawing.Image)
        Me.butClose.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butClose.Location = New System.Drawing.Point(777, 3)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(11, 11)
        Me.butClose.TabIndex = 130
        Me.butClose.Text = "X"
        Me.butClose.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butClose.UseVisualStyleBackColor = True
        Me.butClose.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.BackColor = System.Drawing.Color.Silver
        Me.Panel1.Controls.Add(Me.lblReportTit)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(20, 33)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(748, 598)
        Me.Panel1.TabIndex = 132
        '
        'lblReportTit
        '
        Me.lblReportTit.AutoSize = True
        Me.lblReportTit.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportTit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblReportTit.Location = New System.Drawing.Point(9, 9)
        Me.lblReportTit.Name = "lblReportTit"
        Me.lblReportTit.Size = New System.Drawing.Size(128, 15)
        Me.lblReportTit.TabIndex = 36
        Me.lblReportTit.Text = "WIP BY ITEM REPORT"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.Controls.Add(Me.ckbTransfer)
        Me.Panel2.Controls.Add(Me.ckbRework)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.butDelMulti)
        Me.Panel2.Controls.Add(Me.butDelSingle)
        Me.Panel2.Controls.Add(Me.butAssMulti)
        Me.Panel2.Controls.Add(Me.butAssSingle)
        Me.Panel2.Controls.Add(Me.lstSelItem)
        Me.Panel2.Controls.Add(Me.lstItem)
        Me.Panel2.Controls.Add(Me.P1)
        Me.Panel2.Controls.Add(Me.dtpFrom)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.txtFrom)
        Me.Panel2.Controls.Add(Me.butEXIT)
        Me.Panel2.Controls.Add(Me.butSave)
        Me.Panel2.Location = New System.Drawing.Point(12, 27)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(719, 568)
        Me.Panel2.TabIndex = 0
        '
        'ckbRework
        '
        Me.ckbRework.AutoSize = True
        Me.ckbRework.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbRework.Location = New System.Drawing.Point(15, 25)
        Me.ckbRework.Name = "ckbRework"
        Me.ckbRework.Size = New System.Drawing.Size(96, 18)
        Me.ckbRework.TabIndex = 139
        Me.ckbRework.Text = "Rework Only"
        Me.ckbRework.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(386, 58)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(117, 15)
        Me.Label7.TabIndex = 138
        Me.Label7.Text = "Selected Item Code"
        '
        'butDelMulti
        '
        Me.butDelMulti.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butDelMulti.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelMulti.Location = New System.Drawing.Point(340, 332)
        Me.butDelMulti.Name = "butDelMulti"
        Me.butDelMulti.Size = New System.Drawing.Size(39, 23)
        Me.butDelMulti.TabIndex = 137
        Me.butDelMulti.Text = "<<"
        Me.butDelMulti.UseVisualStyleBackColor = True
        '
        'butDelSingle
        '
        Me.butDelSingle.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butDelSingle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelSingle.Location = New System.Drawing.Point(340, 293)
        Me.butDelSingle.Name = "butDelSingle"
        Me.butDelSingle.Size = New System.Drawing.Size(39, 23)
        Me.butDelSingle.TabIndex = 136
        Me.butDelSingle.Text = "<"
        Me.butDelSingle.UseVisualStyleBackColor = True
        '
        'butAssMulti
        '
        Me.butAssMulti.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butAssMulti.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butAssMulti.Location = New System.Drawing.Point(340, 255)
        Me.butAssMulti.Name = "butAssMulti"
        Me.butAssMulti.Size = New System.Drawing.Size(39, 23)
        Me.butAssMulti.TabIndex = 135
        Me.butAssMulti.Text = ">>"
        Me.butAssMulti.UseVisualStyleBackColor = True
        '
        'butAssSingle
        '
        Me.butAssSingle.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butAssSingle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butAssSingle.Location = New System.Drawing.Point(340, 213)
        Me.butAssSingle.Name = "butAssSingle"
        Me.butAssSingle.Size = New System.Drawing.Size(39, 23)
        Me.butAssSingle.TabIndex = 134
        Me.butAssSingle.Text = ">"
        Me.butAssSingle.UseVisualStyleBackColor = True
        '
        'lstSelItem
        '
        Me.lstSelItem.FormattingEnabled = True
        Me.lstSelItem.HorizontalScrollbar = True
        Me.lstSelItem.Location = New System.Drawing.Point(383, 76)
        Me.lstSelItem.Name = "lstSelItem"
        Me.lstSelItem.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lstSelItem.Size = New System.Drawing.Size(324, 381)
        Me.lstSelItem.Sorted = True
        Me.lstSelItem.TabIndex = 130
        '
        'lstItem
        '
        Me.lstItem.FormattingEnabled = True
        Me.lstItem.HorizontalScrollbar = True
        Me.lstItem.Location = New System.Drawing.Point(10, 76)
        Me.lstItem.Name = "lstItem"
        Me.lstItem.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lstItem.Size = New System.Drawing.Size(325, 381)
        Me.lstItem.Sorted = True
        Me.lstItem.TabIndex = 128
        '
        'P1
        '
        Me.P1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.P1.BackColor = System.Drawing.Color.Gray
        Me.P1.Controls.Add(Me.PB1)
        Me.P1.Location = New System.Drawing.Point(210, 518)
        Me.P1.Name = "P1"
        Me.P1.Size = New System.Drawing.Size(295, 32)
        Me.P1.TabIndex = 127
        '
        'PB1
        '
        Me.PB1.Location = New System.Drawing.Point(15, 7)
        Me.PB1.Name = "PB1"
        Me.PB1.Size = New System.Drawing.Size(266, 19)
        Me.PB1.TabIndex = 0
        '
        'dtpFrom
        '
        Me.dtpFrom.Location = New System.Drawing.Point(609, 25)
        Me.dtpFrom.Name = "dtpFrom"
        Me.dtpFrom.Size = New System.Drawing.Size(17, 20)
        Me.dtpFrom.TabIndex = 125
        Me.dtpFrom.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(12, 58)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 15)
        Me.Label6.TabIndex = 121
        Me.Label6.Text = "Item Code"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(478, 28)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(36, 15)
        Me.Label5.TabIndex = 116
        Me.Label5.Tag = ""
        Me.Label5.Text = " Date"
        Me.Label5.Visible = False
        '
        'txtFrom
        '
        Me.txtFrom.BackColor = System.Drawing.Color.White
        Me.txtFrom.Location = New System.Drawing.Point(520, 25)
        Me.txtFrom.Name = "txtFrom"
        Me.txtFrom.Size = New System.Drawing.Size(91, 20)
        Me.txtFrom.TabIndex = 115
        Me.txtFrom.Visible = False
        '
        'butEXIT
        '
        Me.butEXIT.BackColor = System.Drawing.Color.Gray
        Me.butEXIT.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butEXIT.ForeColor = System.Drawing.Color.White
        Me.butEXIT.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butEXIT.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butEXIT.Location = New System.Drawing.Point(380, 477)
        Me.butEXIT.Name = "butEXIT"
        Me.butEXIT.Size = New System.Drawing.Size(57, 24)
        Me.butEXIT.TabIndex = 114
        Me.butEXIT.Text = "Exit"
        Me.butEXIT.UseVisualStyleBackColor = False
        '
        'butSave
        '
        Me.butSave.BackColor = System.Drawing.Color.Gray
        Me.butSave.Font = New System.Drawing.Font("Arial", 7.75!, System.Drawing.FontStyle.Bold)
        Me.butSave.ForeColor = System.Drawing.Color.White
        Me.butSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(300, 477)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(57, 24)
        Me.butSave.TabIndex = 112
        Me.butSave.Text = "Done"
        Me.butSave.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Panel3.BackColor = System.Drawing.Color.Gray
        Me.Panel3.Controls.Add(Me.PB2)
        Me.Panel3.Location = New System.Drawing.Point(247, 316)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(295, 32)
        Me.Panel3.TabIndex = 133
        Me.Panel3.Visible = False
        '
        'PB2
        '
        Me.PB2.Location = New System.Drawing.Point(15, 7)
        Me.PB2.Name = "PB2"
        Me.PB2.Size = New System.Drawing.Size(266, 19)
        Me.PB2.TabIndex = 0
        '
        'RViewer
        '
        Me.RViewer.ActiveViewIndex = -1
        Me.RViewer.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange
        Me.RViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.RViewer.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.RViewer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RViewer.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.RViewer.Location = New System.Drawing.Point(0, 0)
        Me.RViewer.Name = "RViewer"
        ''Me.RViewer.ReuseParameterValuesOnRefresh = True
        Me.RViewer.ShowCloseButton = False
        Me.RViewer.Size = New System.Drawing.Size(789, 665)
        Me.RViewer.TabIndex = 134
        ''Me.RViewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.RViewer.Visible = False
        '
        'ckbTransfer
        '
        Me.ckbTransfer.AutoSize = True
        Me.ckbTransfer.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbTransfer.Location = New System.Drawing.Point(10, 463)
        Me.ckbTransfer.Name = "ckbTransfer"
        Me.ckbTransfer.Size = New System.Drawing.Size(74, 18)
        Me.ckbTransfer.TabIndex = 140
        Me.ckbTransfer.Text = "Transfer"
        Me.ckbTransfer.UseVisualStyleBackColor = True
        '
        'frmRPTItemStart
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Info
        Me.ClientSize = New System.Drawing.Size(789, 665)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.butClose)
        Me.Controls.Add(Me.butExport)
        Me.Controls.Add(Me.RViewer)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmRPTItemStart"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.P1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents butExport As System.Windows.Forms.Button
    Friend WithEvents butClose As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblReportTit As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lstItem As System.Windows.Forms.ListBox
    Friend WithEvents P1 As System.Windows.Forms.Panel
    Friend WithEvents PB1 As System.Windows.Forms.ProgressBar
    Friend WithEvents dtpFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtFrom As System.Windows.Forms.TextBox
    Friend WithEvents butEXIT As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents sfdSave As System.Windows.Forms.SaveFileDialog
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents PB2 As System.Windows.Forms.ProgressBar
    Friend WithEvents lstSelItem As System.Windows.Forms.ListBox
    Friend WithEvents butDelMulti As System.Windows.Forms.Button
    Friend WithEvents butDelSingle As System.Windows.Forms.Button
    Friend WithEvents butAssMulti As System.Windows.Forms.Button
    Friend WithEvents butAssSingle As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Public WithEvents RViewer As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents ckbRework As System.Windows.Forms.CheckBox
    Friend WithEvents ckbTransfer As System.Windows.Forms.CheckBox
End Class
