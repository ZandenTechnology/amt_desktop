Imports System.Data.SqlClient
Public Class frmJobStatus
    Dim clsM As New clsMain
    Public StrJobno As String

    Private Sub frmJobStatus_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

       
            ' StrJobno = "SV00082312"
            Dim strsql As String
            Dim dsRoute As New DataSet
            dsRoute = clsM.GetDataset("select *,_jobDate,_qtyReleased from tbJobRoute A,tbJob B where A._Job=B._Job and  A._job='" & fncstr(StrJobno) & "' order by _operationNo", "tbRoute")
            Dim dsJob As New DataSet
            strsql = "select _job,_jobsuffix,_item,_jobDate,_oper_num,_qty_op_qty,_qty_complete,_qty_scrapped,_reworkCount from tbJobTrans where _job='" & fncstr(StrJobno) & "'  union select _job,_jobsuffix,_item,_jobDate,_oper_num,_qty_op_qty,_qty_complete,_qty_scrapped,_reworkCount  from tbJobTransHis where _job='" & fncstr(StrJobno) & "' order by _oper_num,_jobsuffix,_reworkCount"
            dsJob = clsM.GetDataset(strsql, "tbJob")
            Dim drRp As DataRow
            Dim dsRet As New DSReport
            If dsRoute.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For i = 0 To dsRoute.Tables(0).Rows.Count - 1
                    If dsJob.Tables(0).Rows.Count > 0 Then
                        Dim drD() As DataRow = dsJob.Tables(0).Select("_oper_num=" & Val(dsRoute.Tables(0).Rows(i).Item("_operationNo")), "_jobsuffix asc")
                        Dim Hstb As New Hashtable

                        If drD.Length > 0 Then
                            Dim j As Integer
                            For j = 0 To drD.Length - 1
                                If Not (Hstb.ContainsKey(drD(j).Item("_jobsuffix"))) = True Then
                                    Dim cls As New clsSufData
                                    cls.strsufdata = drD(j).Item("_jobsuffix")
                                    Hstb.Add(drD(j).Item("_jobsuffix"), cls)
                                End If
                            Next

                            If Hstb.Count > 0 Then
                                Dim isuf As clsSufData
                                For Each isuf In Hstb.Values
                                    Dim DAACdata As DataRow() = dsJob.Tables(0).Select("_oper_num=" & Val(dsRoute.Tables(0).Rows(i).Item("_operationNo")) & " and _jobsuffix='" & isuf.strsufdata & "'", "_reworkCount asc")
                                    If DAACdata.Length > 0 Then


                                        drRp = dsRet.Tables("tbOPStatus").NewRow
                                        For j = 0 To DAACdata.Length - 1
                                            With DAACdata(j)


                                                drRp("idno") = i + 1
                                                drRp("JobNo") = StrJobno
                                                drRp("Jobdate") = Format(DateTime.FromOADate(.Item("_jobDate")), "dd/MM/yyyy")
                                                drRp("RelQty") = dsRoute.Tables(0).Rows(i).Item("_qtyReleased")
                                                drRp("suf") = dsRoute.Tables(0).Rows(i).Item("_jobsuffix")
                                                drRp("opno") = dsRoute.Tables(0).Rows(i).Item("_operationNo")
                                                If j = 0 Then
                                                    drRp("rp1_OPqty") = ""
                                                    drRp("rp1_comqty") = ""
                                                    drRp("rp1_rejQty") = ""

                                                    drRp("rp2_OPqty") = ""
                                                    drRp("rp2_comqty") = ""
                                                    drRp("rp2_rejQty") = ""

                                                    drRp("rp3_OPqty") = ""
                                                    drRp("rp3_comqty") = ""
                                                    drRp("rp3_rejQty") = ""

                                                    drRp("rp4_OPqty") = ""
                                                    drRp("rp4_comqty") = ""
                                                    drRp("rp4_rejQty") = ""

                                                    drRp("rp5_OPqty") = ""
                                                    drRp("rp5_comqty") = ""
                                                    drRp("rp5_rejQty") = ""

                                                    drRp("rp6_OPqty") = ""
                                                    drRp("rp6_comqty") = ""
                                                    drRp("rp6_rejQty") = ""

                                                    drRp("rp7_OPqty") = ""
                                                    drRp("rp7_comqty") = ""
                                                    drRp("rp7_rejQty") = ""


                                                    drRp("rp8_OPqty") = ""
                                                    drRp("rp8_comqty") = ""
                                                    drRp("rp8_rejQty") = ""

                                                    drRp("rp9_OPqty") = ""
                                                    drRp("rp9_comqty") = ""
                                                    drRp("rp9_rejQty") = ""
                                                End If


                                                Select Case .Item("_reworkCount")
                                                    Case 0
                                                        drRp("rp1_OPqty") = .Item("_qty_op_qty")
                                                        drRp("rp1_comqty") = .Item("_qty_complete")
                                                        drRp("rp1_rejQty") = .Item("_qty_scrapped")
                                                    Case 1
                                                        drRp("rp2_OPqty") = .Item("_qty_op_qty")
                                                        drRp("rp2_comqty") = .Item("_qty_complete")
                                                        drRp("rp2_rejQty") = .Item("_qty_scrapped")

                                                    Case 2
                                                        drRp("rp3_OPqty") = .Item("_qty_op_qty")
                                                        drRp("rp3_comqty") = .Item("_qty_complete")
                                                        drRp("rp3_rejQty") = .Item("_qty_scrapped")
                                                    Case 3
                                                        drRp("rp4_OPqty") = .Item("_qty_op_qty")
                                                        drRp("rp4_comqty") = .Item("_qty_complete")
                                                        drRp("rp4_rejQty") = .Item("_qty_scrapped")
                                                    Case 4
                                                        drRp("rp5_OPqty") = .Item("_qty_op_qty")
                                                        drRp("rp5_comqty") = .Item("_qty_complete")
                                                        drRp("rp5_rejQty") = .Item("_qty_scrapped")
                                                    Case 5
                                                        drRp("rp6_OPqty") = .Item("_qty_op_qty")
                                                        drRp("rp6_comqty") = .Item("_qty_complete")
                                                        drRp("rp6_rejQty") = .Item("_qty_scrapped")
                                                    Case 6
                                                        drRp("rp7_OPqty") = .Item("_qty_op_qty")
                                                        drRp("rp7_comqty") = .Item("_qty_complete")
                                                        drRp("rp7_rejQty") = .Item("_qty_scrapped")
                                                    Case 7
                                                        drRp("rp8_OPqty") = .Item("_qty_op_qty")
                                                        drRp("rp8_comqty") = .Item("_qty_complete")
                                                        drRp("rp8_rejQty") = .Item("_qty_scrapped")
                                                    Case 8
                                                        drRp("rp9_OPqty") = .Item("_qty_op_qty")
                                                        drRp("rp9_comqty") = .Item("_qty_complete")
                                                        drRp("rp9_rejQty") = .Item("_qty_scrapped")

                                                End Select





                                            End With


                                        Next
                                        dsRet.Tables("tbOPStatus").Rows.Add(drRp)
                                    End If


                                Next
                            End If















                        Else
                            drRp = dsRet.Tables("tbOPStatus").NewRow
                            drRp("idno") = i + 1
                            drRp("JobNo") = StrJobno
                            drRp("Jobdate") = Format(DateTime.FromOADate(dsRoute.Tables(0).Rows(i).Item("_jobDate")), "dd/MM/yyyy")
                            drRp("RelQty") = dsRoute.Tables(0).Rows(i).Item("_qtyReleased")
                            drRp("suf") = ""
                            drRp("opno") = dsRoute.Tables(0).Rows(i).Item("_operationNo")

                            drRp("rp1_OPqty") = ""
                            drRp("rp1_comqty") = ""
                            drRp("rp1_rejQty") = ""

                            drRp("rp2_OPqty") = ""
                            drRp("rp2_comqty") = ""
                            drRp("rp2_rejQty") = ""

                            drRp("rp3_OPqty") = ""
                            drRp("rp3_comqty") = ""
                            drRp("rp3_rejQty") = ""

                            drRp("rp4_OPqty") = ""
                            drRp("rp4_comqty") = ""
                            drRp("rp4_rejQty") = ""

                            drRp("rp5_OPqty") = ""
                            drRp("rp5_comqty") = ""
                            drRp("rp5_rejQty") = ""

                            drRp("rp6_OPqty") = ""
                            drRp("rp6_comqty") = ""
                            drRp("rp6_rejQty") = ""

                            drRp("rp7_OPqty") = ""
                            drRp("rp7_comqty") = ""
                            drRp("rp7_rejQty") = ""


                            drRp("rp8_OPqty") = ""
                            drRp("rp8_comqty") = ""
                            drRp("rp8_rejQty") = ""

                            drRp("rp9_OPqty") = ""
                            drRp("rp9_comqty") = ""
                            drRp("rp9_rejQty") = ""
                            dsRet.Tables("tbOPStatus").Rows.Add(drRp)
                        End If

                    Else
                        drRp = dsRet.Tables("tbOPStatus").NewRow
                        drRp("idno") = i + 1
                        drRp("JobNo") = StrJobno
                        drRp("Jobdate") = Format(DateTime.FromOADate(dsRoute.Tables(0).Rows(i).Item("_jobDate")), "dd/MM/yyyy")
                        drRp("RelQty") = dsRoute.Tables(0).Rows(i).Item("_qtyReleased")
                        drRp("suf") = ""
                        drRp("opno") = dsRoute.Tables(0).Rows(i).Item("_operationNo")

                        drRp("rp1_OPqty") = ""
                        drRp("rp1_comqty") = ""
                        drRp("rp1_rejQty") = ""

                        drRp("rp2_OPqty") = ""
                        drRp("rp2_comqty") = ""
                        drRp("rp2_rejQty") = ""

                        drRp("rp3_OPqty") = ""
                        drRp("rp3_comqty") = ""
                        drRp("rp3_rejQty") = ""

                        drRp("rp4_OPqty") = ""
                        drRp("rp4_comqty") = ""
                        drRp("rp4_rejQty") = ""

                        drRp("rp5_OPqty") = ""
                        drRp("rp5_comqty") = ""
                        drRp("rp5_rejQty") = ""

                        drRp("rp6_OPqty") = ""
                        drRp("rp6_comqty") = ""
                        drRp("rp6_rejQty") = ""

                        drRp("rp7_OPqty") = ""
                        drRp("rp7_comqty") = ""
                        drRp("rp7_rejQty") = ""


                        drRp("rp8_OPqty") = ""
                        drRp("rp8_comqty") = ""
                        drRp("rp8_rejQty") = ""

                        drRp("rp9_OPqty") = ""
                        drRp("rp9_comqty") = ""
                        drRp("rp9_rejQty") = ""

                        dsRet.Tables("tbOPStatus").Rows.Add(drRp)


                    End If



                Next


                Dim cry As New cryOperationStatus
                cry.SetDataSource(dsRet)
                RViewer.ReportSource = cry
                RViewer.Visible = True
            Else
                MsgBox("No Records found!", MsgBoxStyle.Information, "eWIP")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try



    End Sub
End Class

Public Class clsSufData
    Public strsufdata As String
End Class