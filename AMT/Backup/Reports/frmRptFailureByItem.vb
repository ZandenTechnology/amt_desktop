Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Public Class frmRptFailureByItem
    Dim clsM As New clsMain
    Public DSItem As New DataSet
    ' Public DSWC As New DataSet
    Dim dsrsg As DSFailItem
    Dim exArr(0) As String
    Dim xy As Integer = 0
    Dim strReportDate As String
    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub

    Private Sub dtpTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTo.ValueChanged
        txtTo.Text = Format(dtpTo.Value, "dd/MM/yyyy")
        dtpTo.Checked = False
    End Sub
    Private Sub dtpFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFrom.ValueChanged
        txtFrom.Text = Format(dtpFrom.Value, "dd/MM/yyyy")
        dtpFrom.Checked = False
    End Sub

    'Private Sub RViewer_ClickPage(ByVal sender As Object, ByVal e As CrystalDecisions.Windows.Forms.PageMouseEventArgs) Handles RViewer.ClickPage
    '    TmVari = 2
    'End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        RViewer.DataBindings.Clear()
        GC.GetGeneration(RViewer)
        RViewer.Visible = False
        butClose.Visible = False
        butExport.Visible = False
        PB1.Value = 0
        PB1.Minimum = 0
        PB1.Maximum = 0
        Panel1.Visible = True
    End Sub
    Sub GetItem()
        DSItem.Tables.Clear()
        lstSelItem.Items.Clear()
        lstItem.Items.Clear()
        Try
            Dim ds As New DataSet
            'ds = clsM.GetDataset("select * from tbItem where  order by _itemCode", "tbItem")
            ds = clsM.GetDataset("select * from tbItem where _type<>'O' and (_itemCode like('F%') or _itemCode like('FG%') or _itemCode like('AC%') or  _itemCode like('SC%')) order by _itemCode", "tbItem")
            '
            DSItem = ds
            If IsDBNull(ds) = False Then
                Dim i As Integer
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            '  If UCase(Mid(.Item("_itemCode"), 1, 2)) <> "SC" Then
                            'Dim st As String() = Split(.Item("_itemCode"), "-")
                            'If st.Length = 1 Then
                            'lstItem.Items.Add(New DataItem(Trim(.Item("_itemCode")), Trim(.Item("_itemCode")) & "  :  " & Trim(.Item("_itemdescription")) & ""))
                            'End If
                            'Else
                            lstItem.Items.Add(New DataItem(Trim(.Item("_itemCode")), Trim(.Item("_itemCode")) & "  :  " & Trim(.Item("_itemdescription")) & ""))
                            'End If
                        End With
                    Next
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try

    End Sub

    Private Sub butAssSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssSingle.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstItem.SelectedItems.Count - 1
                lstSelItem.Items.Add(lstItem.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstItem.SelectedItems.Count - 1
                lstItem.Items.Remove(lstItem.SelectedItems.Item(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butAssMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssMulti.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstItem.Items.Count - 1
                lstSelItem.Items.Add(lstItem.Items(i))
            Next
            i = 0
            For i = 0 To lstItem.Items.Count - 1
                lstItem.Items.Remove(lstItem.Items(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butDelSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelSingle.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstSelItem.SelectedItems.Count - 1
                lstItem.Items.Add(lstSelItem.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstSelItem.SelectedItems.Count - 1
                lstSelItem.Items.Remove(lstSelItem.SelectedItems.Item(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub
    Private Sub butDelMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelMulti.Click
        Dim i As Integer
        Try
            lstItem.Sorted = True
            For i = 0 To lstSelItem.Items.Count - 1
                lstItem.Items.Add(lstSelItem.Items(i))
            Next
            i = 0
            For i = 0 To lstSelItem.Items.Count - 1
                lstSelItem.Items.Remove(lstSelItem.Items(0))
            Next
            lstItem.Refresh()
            lstSelItem.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub
    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        Try
            If Trim(txtFrom.Text) = "" Or Trim(txtTo.Text) = "" Then
                MsgBox("Please enter the date!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim dblSd As Double = clsM.CheckDate(txtFrom.Text)
            If dblSd = 0 Then
                MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If

            Dim dblEd As Double = clsM.CheckDate(txtTo.Text)
            If dblEd = 0 Then
                MsgBox("Please check the date format!", MsgBoxStyle.Information, "eWIP")
                Exit Sub
            End If
            Dim s() As String = Split(txtTo.Text, "/")

            Dim Dateto As Date = DateSerial(s(2), s(1), s(0))
            Dateto = DateAdd(DateInterval.Minute, -1, DateAdd(DateInterval.Day, 1, Dateto))
            dblEd = 0
            dblEd = Dateto.ToOADate

            Dim HASRe As New Hashtable
            Dim HAsCode As New Hashtable
            Dim StItem As Boolean
            If lstSelItem.Items.Count > 0 Then
                Dim k As Integer
                For k = 0 To lstSelItem.Items.Count - 1
                    HAsCode.Add(lstSelItem.Items(k).id, lstSelItem.Items(k).id)
                Next
            Else
                StItem = True
                HAsCode.Add("ALL", "ALL")
            End If
            exArr = Nothing
            xy = 0
            ' Dim strReportDate As String = txtFrom.Text & "-" & txtTo.Text
            Dim CreatedDate As String = Format(Now, "dd MMM yyyy")
            strReportDate = Format(DateTime.FromOADate(dblSd), "dd MMM yyyy") & " - " & Format(DateTime.FromOADate(dblEd), "dd MMM yyyy")
            Dim DSrep As New DataSet
            Dim DSrepHis As New DataSet
            Dim Strsql As String
            Dim strResource As String
            Dim DSRecGroup As New DataSet
            Dim DSRecWC As New DataSet
            DSRecWC = clsM.GetDataset("select * from tbWC", "tbWC")
            Dim DSItem As New DataSet
            DSItem = clsM.GetDataset("select * from tbItem", "tbItem")
            DSRecGroup = clsM.GetDataset("select A.*,B.* from tbWC A,tbResourceGroup B,tbWorkStation C where C._wc=A._wc and C._rid=B._rid", "tbRec")
            Strsql = "select A.*,B.*,C._qty_op_qty as _opqty from tbJobTrans A,tbRejectedTrans B,tbJobTransMain C where A._tansnum=B._refID and  C._Job=A._Job and C._oper_num=A._oper_num and A._end_Date between " & dblSd & " and " & dblEd & " and A._qty_scrapped<>0"
            DSrep = clsM.GetDataset(Strsql, "tbJobTransMain")

            Strsql = "select A.*,B.*,C._qty_op_qty as _opqty from tbJobTransHis A,tbRejectedTrans B,tbJobTransMain C where A._oldnum=B._refID and  C._Job=A._Job and C._oper_num=A._oper_num and A._end_Date between " & dblSd & " and " & dblEd & " and A._qty_scrapped<>0"
            DSrepHis = clsM.GetDataset(Strsql, "tbJobTransMain")

            Dim DSMaster As New DataSet
            Strsql = "select * from tbJobTransMain where _end_Date between " & dblSd & " and " & dblEd & " and A._qty_scrapped<>0"
            DSMaster = clsM.GetDataset(Strsql, "tbMain")



            dsrsg = New DSFailItem
            Dim totqty As Double
            Dim drRp As DataRow
            Dim recount As Boolean = False
            PB1.Value = 0
            PB1.Minimum = 0
            PB1.Maximum = 0
            Dim ArrItemCode(0) As String
            Dim ArrItemName(0) As String
            Dim ArrOP(0) As String
            Dim ArrWC(0) As String
            Dim ArrWCCode(0) As String
            Dim ArrRJCode(0) As String
            Dim ArrRJDesc(0) As String
            Dim ArrJob(0) As String
            Dim ArrRJQTY(0) As Double
            Dim ArrOPQty(0) As Double
            Dim ArrSC(0) As Double
            Dim ArrTot(0) As Double
            Dim ArrAvg(0) As Double
            Dim ArrCnt(0) As Integer
            Dim HashAve As New Hashtable
            Dim x1 As Integer = 0
            Dim HasItem As New Hashtable
            Dim stUp As Boolean = False
            Dim maxV As Integer = 0
            If DSrep.Tables(0).Rows.Count > 0 Then
                maxV = DSrep.Tables(0).Rows.Count
            End If
            If DSrepHis.Tables(0).Rows.Count > 0 Then
                maxV = maxV + DSrepHis.Tables(0).Rows.Count
            End If
            PB1.Maximum = maxV
            Dim i As Integer
            Dim j As Integer = 0
            Dim HasClal As New Hashtable
            Dim M1 As Integer = 0
            Dim ARRRJAllqty(0) As Double
            Dim ARRRJAllPer(0) As Double
            Dim ARRRJAllcnt(0) As Double

            If DSrep.Tables(0).Rows.Count > 0 Then
              
                stUp = True
                For i = 0 To DSrep.Tables(0).Rows.Count - 1
                    With DSrep.Tables(0).Rows(i)
                        Dim runt As Boolean = False
                        Dim ArrItem As String() = Split(.Item("_item"), "-")
                        Dim strCont As String = ArrItem(0)
                        If UCase(Mid(.Item("_item"), 1, 2)) = "SC" Then
                            strCont = .Item("_item")
                        End If
                        If StItem = True Then
                            runt = True
                        ElseIf HAsCode.ContainsKey(strCont) = True Then
                            runt = True
                        Else
                            runt = False
                        End If
                        Dim strItem, strOP, StrWC, StrJob, strRCode As String
                        strItem = ""
                        strOP = ""
                        StrWC = ""
                        StrJob = ""
                        strRCode = ""
                        If runt = True Then
                            strItem = strCont
                            strOP = CStr(.Item("_oper_num"))
                            StrWC = .Item("_wc")
                            StrJob = UCase(.Item("_job"))
                            strRCode = CStr(.Item("_RejectedCode"))

                            Dim strID As String = strItem & "||" & strOP & "||" & StrWC & "||" & StrJob & "||" & strRCode
                            Dim strReID As String
                            strReID = strItem & "||" & strOP & "||" & StrWC & "||" & strRCode
                            Dim DRItem() As DataRow = DSItem.Tables(0).Select("_itemCode='" & .Item("_item") & "'")
                            Dim StrItemName As String
                            If DRItem.Length > 0 Then
                                StrItemName = DRItem(0).Item("_itemdescription")
                            End If
                            Dim IName As String = ""

                            'If StrItemName <> "" Then
                            '    Dim StrArr() As String = Split(StrItemName, ",")
                            '    IName = StrArr(0)
                            'Else
                            '    IName = .Item("_item")
                            'End If
                          


                            If HASRe.ContainsKey(strID) = False Then
                                Dim StrRecName As String
                                Dim StrWCName As String

                                Dim DRResou() As DataRow = DSRecGroup.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                If DRResou.Length > 0 Then
                                    StrRecName = DRResou(0).Item("_rGroupID")
                                    StrWCName = DRResou(0).Item("_description")

                                Else
                                    Dim DRResouNew() As DataRow = DSRecWC.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                    If DRResouNew.Length > 0 Then
                                        StrWCName = DRResouNew(0).Item("_description")
                                    End If
                                    StrRecName = ""
                                End If
                              

                                ReDim Preserve ArrItemCode(j)
                                ReDim Preserve ArrItemName(j)
                                ReDim Preserve ArrOP(j)
                                ReDim Preserve ArrWC(j)
                                ReDim Preserve ArrRJCode(j)
                                ReDim Preserve ArrRJDesc(j)
                                ReDim Preserve ArrJob(j)
                                ReDim Preserve ArrRJQTY(j)
                                ReDim Preserve ArrOPQty(j)
                                ReDim Preserve ArrSC(j)
                                ReDim Preserve ArrWCCode(j)
                                ArrWCCode(j) = .Item("_WC")
                                ArrItemCode(j) = .Item("_item")
                                ArrItemName(j) = StrItemName
                                ArrOP(j) = .Item("_oper_num")
                                ArrWC(j) = StrWCName
                                ArrRJCode(j) = .Item("_RejectedCode")
                                ArrRJDesc(j) = .Item("_RejectedDesc")
                                ArrJob(j) = UCase(.Item("_job"))
                                ArrRJQTY(j) = .Item("_RejectedQty")
                                ArrOPQty(j) = .Item("_opqty")
                                If .Item("_opqty") <> 0 Then
                                    ArrSC(j) = (.Item("_RejectedQty") * 100) / .Item("_opqty")
                                Else
                                    ArrSC(j) = 0
                                End If
                                If HashAve.ContainsKey(strReID) = False Then
                                    ReDim Preserve exArr(xy)
                                    exArr(xy) = strReID
                                    xy = xy + 1
                                    ReDim Preserve ArrTot(x1)
                                    ReDim Preserve ArrAvg(x1)
                                    ReDim Preserve ArrCnt(x1)
                                    ArrCnt(x1) = 1
                                    ArrTot(x1) = .Item("_RejectedQty")
                                    ArrAvg(x1) = (.Item("_RejectedQty") * 100) / .Item("_opqty")
                                    HashAve.Add(strReID, x1)
                                    x1 = x1 + 1
                                Else
                                    Dim x As Integer = HashAve(strReID)
                                    ArrTot(x) = ArrTot(x) + .Item("_RejectedQty")
                                    ArrAvg(x) = ArrAvg(x) + (.Item("_RejectedQty") * 100) / .Item("_opqty")
                                    ArrCnt(x) = ArrCnt(x) + 1
                                End If

                                HASRe.Add(strID, j)
                                j = j + 1
                            Else
                                Dim m As Integer
                                m = HASRe(.Item("_item"))
                                Dim dblOldAvg As Double
                                ArrRJQTY(m) = ArrRJQTY(m) + .Item("_RejectedQty")
                                If .Item("_opqty") <> 0 Then
                                    dblOldAvg = ArrSC(m)
                                    ArrSC(m) = (ArrRJQTY(m) * 100) / .Item("_opqty")
                                Else
                                    dblOldAvg = 0
                                    ArrSC(m) = 0
                                End If

                                If HashAve.ContainsKey(strReID) = False Then
                                    ReDim Preserve ArrTot(x1)
                                    ReDim Preserve ArrAvg(x1)
                                    ReDim Preserve ArrCnt(x1)

                                    ReDim Preserve exArr(xy)
                                    exArr(xy) = strReID
                                    xy = xy + 1


                                    ArrTot(x1) = .Item("_RejectedQty")
                                    ArrAvg(x1) = (.Item("_RejectedQty") * 100) / .Item("_opqty")
                                    HashAve.Add(strReID, x1)
                                    x1 = x1 + 1
                                    ArrCnt(x1) = 1
                                Else
                                    Dim x As Integer = HashAve(strReID)
                                    ArrTot(x) = ArrTot(x) + .Item("_RejectedQty")
                                    ArrAvg(x) = ArrAvg(x) - dblOldAvg
                                    ArrAvg(x) = ArrAvg(x) + (ArrRJQTY(m) * 100) / .Item("_opqty")
                                End If



                            End If
                        End If
                    End With
                    PB1.Value = i + 1
                    Application.DoEvents()

                Next
            End If



            If DSrepHis.Tables(0).Rows.Count > 0 Then
                stUp = True
                For i = 0 To DSrepHis.Tables(0).Rows.Count - 1
                    With DSrepHis.Tables(0).Rows(i)
                        Dim runt As Boolean = False
                        Dim ArrItem As String() = Split(.Item("_item"), "-")
                        Dim strCont As String = ArrItem(0)
                        If UCase(Mid(.Item("_item"), 1, 2)) = "SC" Then
                            strCont = .Item("_item")
                        End If
                        If StItem = True Then
                            runt = True
                        ElseIf HAsCode.ContainsKey(strCont) = True Then
                            runt = True
                        Else
                            runt = False
                        End If
                        Dim strItem, strOP, StrWC, StrJob, strRCode As String
                        strItem = ""
                        strOP = ""
                        StrWC = ""
                        StrJob = ""
                        strRCode = ""
                        If runt = True Then
                            strItem = strCont
                            strOP = CStr(.Item("_oper_num"))
                            StrWC = .Item("_wc")
                            StrJob = UCase(.Item("_job"))
                            strRCode = CStr(.Item("_RejectedCode"))
                            Dim strReID As String
                            strReID = strItem & "||" & strOP & "||" & StrWC & "||" & strRCode
                            Dim strID As String = strItem & "||" & strOP & "||" & StrWC & "||" & StrJob & "||" & strRCode
                            Dim DRItem() As DataRow = DSItem.Tables(0).Select("_itemCode='" & .Item("_item") & "'")
                            Dim StrItemName As String
                            If DRItem.Length > 0 Then
                                StrItemName = DRItem(0).Item("_itemdescription")
                            End If
                            Dim IName As String = ""

                            'If StrItemName <> "" Then
                            '    Dim StrArr() As String = Split(StrItemName, ",")
                            '    IName = StrArr(0)
                            'Else
                            '    IName = .Item("_item")
                            'End If


                            If HASRe.ContainsKey(strID) = False Then
                                Dim StrRecName As String
                                Dim StrWCName As String

                                Dim DRResou() As DataRow = DSRecGroup.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                If DRResou.Length > 0 Then
                                    StrRecName = DRResou(0).Item("_rGroupID")
                                    StrWCName = DRResou(0).Item("_description")

                                Else
                                    Dim DRResouNew() As DataRow = DSRecWC.Tables(0).Select("_wc='" & .Item("_WC") & "'")
                                    If DRResouNew.Length > 0 Then
                                        StrWCName = DRResouNew(0).Item("_description")
                                    End If
                                    StrRecName = ""
                                End If


                                ReDim Preserve ArrItemCode(j)
                                ReDim Preserve ArrItemName(j)
                                ReDim Preserve ArrOP(j)
                                ReDim Preserve ArrWC(j)
                                ReDim Preserve ArrRJCode(j)
                                ReDim Preserve ArrRJDesc(j)
                                ReDim Preserve ArrJob(j)
                                ReDim Preserve ArrRJQTY(j)
                                ReDim Preserve ArrOPQty(j)
                                ReDim Preserve ArrSC(j)
                                ReDim Preserve ArrWCCode(j)
                                ArrWCCode(j) = .Item("_WC")
                                ArrItemCode(j) = .Item("_item")
                                ArrItemName(j) = StrItemName
                                ArrOP(j) = .Item("_oper_num")
                                ArrWC(j) = StrWCName
                                ArrRJCode(j) = .Item("_RejectedCode")
                                ArrRJDesc(j) = .Item("_RejectedDesc")
                                ArrJob(j) = UCase(.Item("_job"))
                                ArrRJQTY(j) = .Item("_RejectedQty")
                                ArrOPQty(j) = .Item("_opqty")
                                If .Item("_opqty") <> 0 Then
                                    ArrSC(j) = (.Item("_RejectedQty") * 100) / .Item("_opqty")
                                Else
                                    ArrSC(j) = 0
                                End If
                                If HashAve.ContainsKey(strReID) = False Then
                                    ReDim Preserve ArrTot(x1)
                                    ReDim Preserve ArrAvg(x1)
                                    ReDim Preserve ArrCnt(x1)

                                    ReDim Preserve exArr(xy)
                                    exArr(xy) = strReID
                                    xy = xy + 1


                                    ArrCnt(x1) = 1
                                    ArrTot(x1) = .Item("_RejectedQty")
                                    ArrAvg(x1) = (.Item("_RejectedQty") * 100) / .Item("_opqty")
                                    HashAve.Add(strReID, x1)
                                    x1 = x1 + 1
                                Else
                                    Dim x As Integer = HashAve(strReID)
                                    ArrTot(x) = ArrTot(x) + .Item("_RejectedQty")
                                    ArrAvg(x) = ArrAvg(x) + (.Item("_RejectedQty") * 100) / .Item("_opqty")
                                    ArrCnt(x) = ArrCnt(x) + 1
                                End If
                                HASRe.Add(strID, j)
                                j = j + 1
                            Else
                                Dim m As Integer
                                m = HASRe(.Item("_item"))
                                Dim dblOldAvg As Double
                                ArrRJQTY(m) = ArrRJQTY(m) + .Item("_RejectedQty")
                                dblOldAvg = ArrSC(m)
                                If .Item("_opqty") <> 0 Then
                                    ArrSC(m) = (ArrRJQTY(m) * 100) / .Item("_opqty")
                                Else
                                    ArrSC(m) = 0
                                End If
                            


                                If HashAve.ContainsKey(strReID) = False Then
                                    ReDim Preserve ArrTot(x1)
                                    ReDim Preserve ArrAvg(x1)

                                    ReDim Preserve exArr(xy)
                                    exArr(xy) = strReID
                                    xy = xy + 1


                                    ArrCnt(x1) = 1
                                    ArrTot(x1) = .Item("_RejectedQty")
                                    ArrAvg(x1) = (.Item("_RejectedQty") * 100) / .Item("_opqty")
                                    HashAve.Add(strReID, x1)
                                    x1 = x1 + 1
                                Else
                                    Dim x As Integer = HashAve(strReID)
                                    ArrTot(x) = ArrTot(x) + .Item("_RejectedQty")
                                    ArrAvg(x) = ArrAvg(x) - dblOldAvg
                                    ArrAvg(x) = ArrAvg(x) + (ArrRJQTY(m) * 100) / .Item("_opqty")
                                End If


                            End If
                        End If
                    End With
                    If PB1.Value < maxV Then
                        PB1.Value = PB1.Value + 1
                        Application.DoEvents()
                    End If

                Next
            End If


            If stUp = True Then
                If UBound(ArrItemName) > -1 Then
                    '  Dim j As Integer
                    For j = 0 To UBound(ArrItemName)
                        drRp = dsrsg.Tables("tbFailureByItem").NewRow
                        drRp("sn") = j + 1
                        drRp("Title") = strReportDate 'Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                        drRp("CreateDate") = CreatedDate
                        drRp("ReportDate") = strReportDate
                        drRp("FGCode") = ArrItemCode(j)
                        drRp("FGDesc") = ArrItemName(j)
                        drRp("Job") = ArrJob(j)
                        drRp("OP") = ArrOP(j)
                        drRp("WC") = ArrWCCode(j)
                        drRp("WCDesc") = ArrWC(j)
                        drRp("RejCode") = ArrRJCode(j)
                        drRp("RejDesc") = ArrRJDesc(j)
                        drRp("RejQty") = ArrRJQTY(j)
                        drRp("RejPer") = ArrSC(j)
                        drRp("OpQty") = ArrOPQty(j)
                        Dim stN As String
                        stN = ArrItemCode(j) & "||" & ArrOP(j) & "||" & ArrWCCode(j) & "||" & ArrRJCode(j)
                        'If ArrItemCode(j) = "FG0007" Then
                        '    MsgBox(ArrItemCode(j))
                        'End If

                        If HashAve.ContainsKey(stN) = True Then
                            Dim x2 As Integer = HashAve(stN)
                            drRp("totRej") = ArrTot(x2)

                            If ArrCnt(x2) <> 0 Then
                                drRp("TotPer") = ArrAvg(x2) / ArrCnt(x2)
                            Else
                                drRp("TotPer") = ArrAvg(x2)
                            End If

                        Else
                            drRp("totRej") = 0
                            drRp("TotPer") = 0
                        End If

                        dsrsg.Tables("tbFailureByItem").Rows.Add(drRp)
                    Next
                End If
            Else
                drRp = dsrsg.Tables("tbFailureByItem").NewRow
                drRp("sn") = j + 1
                drRp("Title") = strReportDate ' Trim(txtFrom.Text) & "-" & Trim(txtTo.Text)
                drRp("CreateDate") = CreatedDate
                drRp("ReportDate") = strReportDate
                drRp("FGCode") = ""
                drRp("FGDesc") = ""
                drRp("Job") = ""
                drRp("OP") = ""
                drRp("WC") = ""
                drRp("WCDesc") = ""
                drRp("RejCode") = ""
                drRp("RejDesc") = ""
                drRp("RejQty") = 0
                drRp("RejPer") = 0
                drRp("totRej") = 0
                drRp("TotPer") = 0
                drRp("OpQty") = 0
                dsrsg.Tables("tbFailureByItem").Rows.Add(drRp)
            End If


 


           
            Dim cry As New cryFailureItem
            cry.SetDataSource(dsrsg)
            RViewer.ReportSource = cry
            RViewer.Refresh()
            Panel1.Visible = False
            RViewer.Visible = True
            butExport.Visible = True
            butClose.Visible = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            TmVari = 2
        End Try



    End Sub

    Private Sub frmRptFailureByItem_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblReportTit.Text = "FAILURE ANALYSIS FOR REPORT BY ITEM"

        dtpFrom.Value = Now
        dtpTo.Value = Now
        txtFrom.Text = Format(Now, "dd/MM/yyyy")
        txtTo.Text = Format(Now, "dd/MM/yyyy")
        GetItem()
    End Sub




    Private Sub butExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExport.Click
        ExportDatatoExcel()
    End Sub
    Function ExportDatatoExcel() As Boolean
        Dim txtFile As String
        txtFile = ""
        sfdSave.FileName = ""
        sfdSave.Filter = "Excel File|*.xls"
        sfdSave.Title = "Save an Excel File"
        If sfdSave.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtFile = sfdSave.FileName
        Else
            Exit Function
        End If


        Panel3.Visible = True
        PB2.Value = 0
        PB2.Minimum = 0
        PB2.Maximum = 0
        Dim myTrans As System.Data.SqlClient.SqlTransaction

        Dim delFile As Boolean = False
        '= "C:\ZandenTemp\ComReport.xls"
        Try
            If IO.File.Exists(txtFile) Then
                IO.File.Delete(txtFile)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Function
        Finally
        End Try
        ' Dim at As Integer
        If txtFile = "" Then
            MsgBox("Please enter the file name!", MsgBoxStyle.Information, "eWIP")
            Exit Function
        End If

        Dim str, filename As String
        str = ""
        filename = ""
        Dim col, row As Integer
        col = 0
        row = 0
        Dim strHisNo As String = ""
        Dim strAcrNo As String = ""
        Dim valHis As String
        Dim strHisVal As String = ""
        Dim strCurVal As String = ""

        Dim localAll As Process()
        localAll = Process.GetProcesses()
        Dim EXhash As New Hashtable
        For i As Integer = 0 To localAll.Length - 1
            If localAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(localAll(i).Id) = False Then
                    EXhash.Add(localAll(i).Id, localAll(i).Id)
                End If
            End If
        Next

        Dim Excel As Object = CreateObject("Excel.Application")


        P1.Visible = True
        Try
            Dim i As Integer = 1
            With Excel
                .SheetsInNewWorkbook = 1
                .Workbooks.Add()
                .Worksheets(1).Select()

                'Dim chartRange As Excel.Range

                'chartRange = .Range("A1", "G1")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "FAILURE ANALYSIS REPORT BY ITEM"
                'chartRange.HorizontalAlignment = 3
                'chartRange.VerticalAlignment = 3

                'chartRange = .Range("A2", "D2")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "Created Date :" & Format(Now, "dd MMM yyyy")
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1

                'chartRange = .Range("E2", "G2")
                'chartRange.Merge()
                'chartRange.FormulaR1C1 = "Reported Date :" & strReportDate 'txtFrom.Text & "-" & txtTo.Text
                'chartRange.HorizontalAlignment = 1
                'chartRange.VerticalAlignment = 1


                'If exArr.Length > 0 Then
                '    Array.Sort(exArr)
                '    Dim M1 As Integer
                '    i = 2
                '    PB2.Maximum = exArr.Length
                '    Dim stroldICode As String = ""
                '    Dim stroldIDesc As String = ""
                '    For M1 = 0 To exArr.Length - 1
                '        Dim s() As String = Split(exArr(M1), "||")
                '        'strReID = strItem & "||" & strOP & "||" & StrWC & "||" & strRCode
                '        Dim drFail() As DataRow = dsrsg.Tables("tbFailureByItem").Select("FGCode='" & s(0) & "' and OP='" & s(1) & "' and WC='" & s(2) & "' and RejCode='" & s(3) & "'", "RejCode,WCDesc")
                '        Dim stBoll As Boolean = False
                '        If drFail.Length > 0 Then
                '            Dim k1 As Integer
                '            Dim strOPC As String = ""
                '            Dim strWCdes As String = ""
                '            Dim strRjCode As String = ""
                '            Dim strRjDes As String = ""
                '            Dim dblRjQTY As Double = 0
                '            Dim dblScrap As Double = 0
                '            Dim intCount As Integer = 0
                '            For k1 = 0 To drFail.Length - 1
                '                If stBoll = False Then
                '                    If stroldICode <> drFail(k1).Item("FGCode") Then
                '                        stroldICode = drFail(k1).Item("FGCode")
                '                        i += 1
                '                        chartRange = .Range("A" & i, "D" & i)
                '                        chartRange.Merge()
                '                        chartRange.FormulaR1C1 = "FG Code : " & drFail(k1).Item("FGCode")
                '                        chartRange.HorizontalAlignment = 1
                '                        chartRange.VerticalAlignment = 1
                '                        i += 1
                '                        chartRange = .Range("A" & i, "D" & i)
                '                        chartRange.Merge()
                '                        chartRange.FormulaR1C1 = "FG Description : " & drFail(k1).Item("FGDesc")
                '                        chartRange.HorizontalAlignment = 1
                '                        chartRange.VerticalAlignment = 1
                '                    End If
                '                    i += 1
                '                    Excel.Range("A" & i, "G" & i).Interior.ColorIndex = 37
                '                    .cells(i, 1).MergeCells = True
                '                    .cells(i, 1).EntireRow.Font.Bold = True
                '                    .cells(i, 1).value = "OP"


                '                    .cells(i, 2).MergeCells = True
                '                    .cells(i, 2).EntireRow.Font.Bold = True
                '                    .cells(i, 2).value = "WC Description"

                '                    .cells(i, 3).MergeCells = True
                '                    .cells(i, 3).EntireRow.Font.Bold = True
                '                    .cells(i, 3).value = "Rejected Code"


                '                    .cells(i, 4).MergeCells = True
                '                    .cells(i, 4).EntireRow.Font.Bold = True
                '                    .cells(i, 4).value = "Rejected Description"

                '                    .cells(i, 5).MergeCells = True
                '                    .cells(i, 5).EntireRow.Font.Bold = True
                '                    .cells(i, 5).value = "Rejected Qty"

                '                    .cells(i, 6).MergeCells = True
                '                    .cells(i, 6).EntireRow.Font.Bold = True
                '                    .cells(i, 6).value = "Scrap %"

                '                    .cells(i, 7).MergeCells = True
                '                    .cells(i, 7).EntireRow.Font.Bold = True
                '                    .cells(i, 7).value = "Job"
                '                    strOPC = drFail(k1).Item("OP")

                '                    strWCdes = drFail(k1).Item("WCDesc")
                '                    strRjCode = drFail(k1).Item("RejDesc")
                '                    strRjDes = drFail(k1).Item("RejDesc")
                '                    dblRjQTY = dblRjQTY + Format(drFail(k1).Item("RejQty"), "0.00")
                '                    dblScrap = dblScrap + Format(drFail(k1).Item("RejPer"), "0.00")

                '                    intCount = intCount + 1
                '                    i += 1
                '                    .cells(i, 1).value = drFail(k1).Item("OP")
                '                    .cells(i, 2).value = drFail(k1).Item("WCDesc")
                '                    .cells(i, 3).value = drFail(k1).Item("RejCode")
                '                    .cells(i, 4).value = drFail(k1).Item("RejDesc")
                '                    .cells(i, 5).value = Format(drFail(k1).Item("RejQty"), "0.00")
                '                    .cells(i, 6).value = Format(drFail(k1).Item("RejPer"), "0.00")
                '                    .cells(i, 7).value = drFail(k1).Item("Job")





                '                    stBoll = True
                '                Else
                '                    strOPC = drFail(k1).Item("OP")

                '                    strWCdes = drFail(k1).Item("WCDesc")
                '                    strRjCode = drFail(k1).Item("RejDesc")
                '                    strRjDes = drFail(k1).Item("RejDesc")
                '                    dblRjQTY = dblRjQTY + Format(drFail(k1).Item("RejQty"), "0.00")
                '                    dblScrap = dblScrap + Format(drFail(k1).Item("RejPer"), "0.00")

                '                    intCount = intCount + 1
                '                    i += 1

                '                    .cells(i, 1).value = drFail(k1).Item("OP")
                '                    .cells(i, 2).value = drFail(k1).Item("WCDesc")
                '                    .cells(i, 3).value = "'" & drFail(k1).Item("RejCode")
                '                    .cells(i, 4).value = drFail(k1).Item("RejDesc")
                '                    .cells(i, 5).value = Format(drFail(k1).Item("RejQty"), "0.00")
                '                    .cells(i, 6).value = Format(drFail(k1).Item("RejPer"), "0.00")
                '                    .cells(i, 7).value = drFail(k1).Item("Job")
                '                    stBoll = True
                '                End If
                '            Next
                '            If stBoll = True Then
                '                i += 1
                '                Excel.Range("A" & i, "G" & i).Font.ColorIndex = 3
                '                .cells(i, 1).value = strOPC
                '                .cells(i, 2).value = strWCdes
                '                .cells(i, 3).value = "'" & strRjCode
                '                .cells(i, 4).value = strRjDes
                '                .cells(i, 5).value = Format(dblRjQTY, "0.00")
                '                .cells(i, 6).value = Format(dblScrap / intCount, "0.00")
                '            End If
                '        End If

                '        PB2.Value = M1 + 1
                '        Application.DoEvents()
                '    Next


                'End If





            End With


            filename = txtFile
            Excel.ActiveCell.Worksheet.SaveAs(filename)
            System.Runtime.InteropServices.Marshal.ReleaseComObject(Excel)
            Excel = Nothing



            MsgBox("Data's are exported to Excel Succesfully in '" & filename & "'", MsgBoxStyle.Information, "eWIP")

            '  lblDataV.Text = "Data has been Successfully Exported"




        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "eWIP")
            delFile = True


        Finally
            Panel3.Visible = False
            cn.Close()
            com.Parameters.Clear()
        End Try

        ' The excel is created and opened for insert value. We most close this excel using this system
        'Dim pro() As Process = System.Diagnostics.Process.GetProcessesByName("EXCEL")

        'For Each i As Process In pro
        '    i.Kill()
        'Next


        Dim DellocalAll As Process()

        DellocalAll = Process.GetProcesses()


        For i As Integer = 0 To DellocalAll.Length - 1
            If DellocalAll(i).ProcessName.ToUpper = "EXCEL" Then
                If EXhash.ContainsKey(DellocalAll(i).Id) = False Then
                    DellocalAll(i).Kill()
                End If
            End If
        Next



        If delFile = True Then
            Try
                If IO.File.Exists(filename) Then
                    IO.File.Delete(filename)
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End Try
        End If
        txtFile = ""


    End Function
    
    
    
End Class