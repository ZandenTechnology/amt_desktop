Imports System.Data.SqlClient
Public Class frmCustomer
    Dim parm As SqlParameter
    Dim dr As SqlDataReader
    Dim strFoCus As Integer = 0
    Dim strclsH As String
    Dim clsd As New clsMain
    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(txtCustID.Text) = "" Then
            MsgBox("Enter the customer id!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtCustName.Text) = "" Then
            MsgBox("Enter the Customer name!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "insert into tbCustomer( _custNo,_custName,_address1,_address2,_address3)values( @custNo,@custName,@address1,@address2,@address3)"
                parm = .Parameters.Add("@custNo", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtCustID.Text)
                parm = .Parameters.Add("@custName", SqlDbType.VarChar, 500)
                parm.Value = Trim(txtCustName.Text)
                parm = .Parameters.Add("@address1", SqlDbType.VarChar, 400)
                parm.Value = Trim(txtCustAdd1.Text)
                parm = .Parameters.Add("@address2", SqlDbType.VarChar, 400)
                parm.Value = Trim(txtCustAdd1.Text)
                parm = .Parameters.Add("@address3", SqlDbType.VarChar, 400)
                parm.Value = Trim(txtCustAdd1.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully saved!", MsgBoxStyle.Information, "eWIP")
            End With
        Catch ex As SqlException
            If ex.Number = 2627 Then
                MsgBox("This customer id already exist", MsgBoxStyle.Critical, "eWIP")
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub
    Sub list_Displaydata()
        lstv.Items.Clear()
        Try
            com.Connection = cn
            com.CommandType = CommandType.Text
            com.CommandText = "select * from tbCustomer order by _custName"
           
            cn.Open()
            dr = com.ExecuteReader(CommandBehavior.CloseConnection)
            Dim ss As String = ""
            Dim i As Integer = -1
            Dim rcount As Integer
            While dr.Read
                i = i + 1
                'Dim nr As DataRow()
                'nr.Item(0) = dr.Item("_oid")
                'DataGridView1.Rows.Add(nr)
                ss = dr.Item("_custNo")
                Dim ls As New ListViewItem(Trim(dr.Item("_custID")))    ' you can also use reader.GetSqlValue(0) 
                ls.SubItems.Add(Trim(dr.Item("_custNo")))
                ls.SubItems.Add(Trim(dr.Item("_custName")))
                ls.SubItems.Add(Trim(dr.Item("_address1")))
                ls.SubItems.Add(Trim(dr.Item("_address2")))
                ls.SubItems.Add(Trim(dr.Item("_address3")))
                ls.Selected = True
                lstv.Items.Add(ls)
                lstv.Focus()
                If strFoCus = dr.Item("_custID") Then
                    rcount = i
                End If


            End While
            If i <> -1 Then
                lstv.Items(rcount).Selected = True
            End If
            strFoCus = 0
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try

    End Sub
    Function refTxt(ByVal txt As String) As String
        Return Trim(Replace(txt, "'", "''"))
    End Function

    'Private Sub frmOperative_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '    txtOperatorID.TabIndex = 1

    '    ' Panel1.Focus()
    '    Panel2.Focus()
    '    Panel3.Focus()
    '    txtOperatorID.Focus()
    '    With lstv
    '        .Columns.Add(New ColHeader("IDNo", 0, HorizontalAlignment.Left, True))
    '        .Columns.Add(New ColHeader("Operator ID", 130, HorizontalAlignment.Left, True))
    '        .Columns.Add(New ColHeader("Operator Name", lstv.Width - 285, HorizontalAlignment.Left, True))
    '        .Columns.Add(New ColHeader("Skip", 50, HorizontalAlignment.Left, True))
    '        .Columns.Add(New ColHeader("Split", 50, HorizontalAlignment.Left, True))
    '        .Columns.Add(New ColHeader("Print", 50, HorizontalAlignment.Left, True))
    '        list_Displaydata()
    '    End With
    '    txtOperatorID.Text = 1
    '    txtOperatorID.Focus()
    '    txtOperatorID.Text = ""
    '    '  lstv.Focus()
    'End Sub



    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click




        txtIDNo.Text = lstv.SelectedItems(0).Text
        txtCustID.Text = lstv.SelectedItems(0).SubItems(1).Text
        txtCustName.Text = lstv.SelectedItems(0).SubItems(2).Text
        txtCustAdd1.Text = lstv.SelectedItems(0).SubItems(3).Text
        txtCustAdd2.Text = lstv.SelectedItems(0).SubItems(4).Text
        txtCustAdd3.Text = lstv.SelectedItems(0).SubItems(5).Text
        butSave.Enabled = False
        butUpdate.Enabled = True
        butDelete.Enabled = True
        ' txtUserCode.ReadOnly = True
        'StbUser.Text = "You Can Edit And Delete This Records"
    End Sub

    Private Sub butUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdate.Click
        If Trim(txtCustID.Text) = "" Then
            MsgBox("Enter the customer id!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Trim(txtCustName.Text) = "" Then
            MsgBox("Enter the customer name!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Try
            With com
                strFoCus = Val(txtIDNo.Text)
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "Update  tbCustomer set _custNo=@custNo,_custName=@custName,_address1=@address1,_address2=@address2,_address3=@address3 where _custID=@custID"
                parm = .Parameters.Add("@custNo", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtCustID.Text)
                parm = .Parameters.Add("@custName", SqlDbType.VarChar, 500)
                parm.Value = Trim(txtCustName.Text)
                parm = .Parameters.Add("@address1", SqlDbType.VarChar, 400)
                parm.Value = Trim(txtCustAdd1.Text)
                parm = .Parameters.Add("@address2", SqlDbType.VarChar, 400)
                parm.Value = Trim(txtCustAdd1.Text)
                parm = .Parameters.Add("@address3", SqlDbType.VarChar, 400)
                parm.Value = Trim(txtCustAdd1.Text)
                parm = .Parameters.Add("@custID", SqlDbType.Int)
                parm.Value = Val(txtIDNo.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")
            End With
        Catch ex As SqlException
            If ex.Number = 2627 Then
                MsgBox("This operator id already exist", MsgBoxStyle.Critical, "eWIP")
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub
    Sub textReset()
        txtIDNo.Clear()
        txtCustID.Clear()
        txtCustName.Clear()
        txtCustAdd1.Clear()
        txtCustAdd2.Clear()
        txtCustAdd3.Clear()
        butSave.Enabled = True
        butDelete.Enabled = False
        butUpdate.Enabled = False
        txtCustID.Focus()
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        textReset()
    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        'If Trim(txtOperatorID.Text) = "" Then
        '    MsgBox("Enter the operator id!", MsgBoxStyle.Information, "eWIP")
        '    Exit Sub
        'End If
        'If Trim(txtOperatorName.Text) = "" Then
        '    MsgBox("Enter the operator name!", MsgBoxStyle.Information, "eWIP")
        '    Exit Sub
        'End If
        Dim result As New DialogResult
        result = MessageBox.Show("Do you Want to delete this Records?", "Caption", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = Windows.Forms.DialogResult.Yes Then
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "delete from  tbcustomer where _custID=@custID"
                    parm = .Parameters.Add("@custID", SqlDbType.Int)
                    parm.Value = Val(txtIDNo.Text)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()
                    textReset()
                    MsgBox("Successfully deleted!", MsgBoxStyle.Information, "eWIP")
                End With
            Catch ex As SqlException
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Finally

                com.Parameters.Clear()
            End Try
            list_Displaydata()
        End If
    End Sub

    Private Sub lstv_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lstv.ColumnClick


        Dim clickedCol As ColHeader = CType(Me.lstv.Columns(e.Column), ColHeader)


        clickedCol.ascending = Not clickedCol.ascending

        Dim strby As String = ""
        If clickedCol.ascending = False Then
            strby = " DESC"
        End If



        Select Case e.Column
            Case 0
                strclsH = " Order by _custID " & strby
            Case 1
                strclsH = " Order by _custNo " & strby
            Case 2
                strclsH = " Order by _custName " & strby
            Case 3
                strclsH = " Order by _address1 " & strby
            Case 4
                strclsH = " Order by _address2 " & strby
            Case 5
                strclsH = " Order by _address3 " & strby
        End Select



        Dim numItems As Integer = Me.lstv.Items.Count


        Me.lstv.BeginUpdate()


        Dim SortArray As New ArrayList
        Dim i As Integer
        For i = 0 To numItems - 1
            SortArray.Add(New SortWrapper(Me.lstv.Items(i), e.Column))
        Next i


        SortArray.Sort(0, SortArray.Count, New SortWrapper.SortComparer(clickedCol.ascending))


        Me.lstv.Items.Clear()
        Dim z As Integer
        For z = 0 To numItems - 1
            Me.lstv.Items.Add(CType(SortArray(z), SortWrapper).sortItem)
        Next z

        Me.lstv.EndUpdate()


    End Sub

    'Private Sub txtopSerchby_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtopSerchby.KeyDown
    '    list_Displaydata()
    'End Sub

    'Private Sub txtopSerchby_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtopSerchby.KeyPress
    '    If Asc(e.KeyChar) = 13 Then
    '        list_Displaydata()
    '    End If
    'End Sub





    'Private Sub txtopSerchby_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtopSerchby.TextChanged
    '    list_Displaydata()
    'End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub

    Private Sub frmCustomer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtCustID.TabIndex = 1

        ' Panel1.Focus()
        Panel2.Focus()
        Panel3.Focus()
        txtCustID.Focus()
        With lstv
            .Columns.Add(New ColHeader("IDNo", 0, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Customer ID", 130, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Customer Name", lstv.Width - 285, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Address 1", 50, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Address 2", 50, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Address 3", 50, HorizontalAlignment.Left, True))
            list_Displaydata()
        End With
        txtCustID.Text = 1
        txtCustID.Focus()
        txtCustID.Text = ""
        ' Test()
        '  lstv.Focus()
    End Sub


    'Sub Test()
    '    Dim ds As DataSet
    '    ds = clsd.GetDataset("select * from tbCustomer order by _custName", "tbRoute")
    '    DataGridView1.DataSource = ds.Tables(0)
    'End Sub
End Class