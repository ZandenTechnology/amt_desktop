Imports System.Data.SqlClient
Public Class frmRejectedGroup
    Dim dr As SqlDataReader
    Dim parm As SqlParameter
    Sub RejectedCode()
        lstRejected.Items.Clear()
        Try
            com.Connection = cn
            com.CommandType = CommandType.Text
            com.CommandText = "select _RejID,_RejectedCode,_RejectedDesc from tbRejected order by _RejectedCode"
            'parm = com.Parameters.Add("@Type", SqlDbType.VarChar, 256)
            'parm.Value = refTxt(cmbType.Text)
            cn.Open()
            dr = com.ExecuteReader(CommandBehavior.CloseConnection)
            While dr.Read
                lstRejected.Items.Add(New DataItem(dr.Item("_RejID"), dr.Item("_RejectedCode") & " - " & dr.Item("_RejectedDesc")))
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
    End Sub
    Private Sub frmRejectedGroup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetData()
        RejectedCode()
    End Sub
    Sub GetData()

        cmbRG.Items.Clear()
        cmbRG.Items.Add(New DataItem("-Select-", "-Select-"))
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                'Dim DI As DataItem
                .CommandText = "select _rGroupid,_rGroupName,_rid from tbResourceGroup order by _rGroupName"
                cn.Open()
                dr = .ExecuteReader(CommandBehavior.CloseConnection)
                While dr.Read
                    cmbRG.Items.Add(New DataItem(dr.Item("_rGroupName") & "||" & dr.Item("_rid"), dr.Item("_rGroupid")))
                End While
                cn.Close()
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
        cmbRG.Text = "-Select-"
    End Sub
    Private Sub cmbRG_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRG.SelectedIndexChanged
        If cmbRG.Text = "-Select-" Then
            lstwrk.Items.Clear()
        End If
        lstwrk.Items.Clear()
        Try
            If cmbRG.Text <> "-Select-" Then
                If cmbRG.Text <> "-Select-" Then
                    Dim DI As DataItem
                    DI = cmbRG.Items(cmbRG.SelectedIndex)
                    ' txtRGDesc.Text = Split(DI.ID, "||")(0)
                    txtRGid.Text = Split(DI.ID, "||")(1)
                Else
                    txtRGid.Text = ""
                    ' txtRGDesc.Text = ""
                End If

                With com
                    .Connection = cn
                    .CommandType = CommandType.Text

                    .CommandText = "select * from tbWorkStation A,tbWc B,tbResourceGroup C where A._wc=B._Wc and A._rid=C._rid and A._rid=" & Val(txtRGid.Text)
                    cn.Open()
                    dr = .ExecuteReader(CommandBehavior.CloseConnection)
                    While dr.Read
                        lstwrk.Items.Add(New DataItem(dr.Item("_sIDno") & "||" & dr.Item("_wc") & "||" & dr.Item("_description"), dr.Item("_wc") & "-" & dr.Item("_description")))
                    End While
                    cn.Close()
                End With
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub butAssSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssSingle.Click
        Dim i As Integer
        Try
            lstRejected.Sorted = True
            For i = 0 To lstRejected.SelectedItems.Count - 1
                lstGroup.Items.Add(lstRejected.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstRejected.SelectedItems.Count - 1
                lstRejected.Items.Remove(lstRejected.SelectedItems.Item(0))
            Next
            lstRejected.Refresh()
            lstGroup.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butAssMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssMulti.Click
        Dim i As Integer
        Try
            lstRejected.Sorted = True
            For i = 0 To lstRejected.Items.Count - 1
                lstGroup.Items.Add(lstRejected.Items(i))
            Next
            i = 0
            For i = 0 To lstRejected.Items.Count - 1
                lstRejected.Items.Remove(lstRejected.Items(0))
            Next
            lstRejected.Refresh()
            lstGroup.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butDelSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelSingle.Click
        Dim i As Integer
        Try
            lstRejected.Sorted = True
            For i = 0 To lstGroup.SelectedItems.Count - 1
                lstRejected.Items.Add(lstGroup.SelectedItems.Item(i))
            Next
            i = 0
            For i = 0 To lstGroup.SelectedItems.Count - 1
                lstGroup.Items.Remove(lstGroup.SelectedItems.Item(0))
            Next
            lstRejected.Refresh()
            lstGroup.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub
    Private Sub butDelMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelMulti.Click
        Dim i As Integer
        Try
            lstRejected.Sorted = True
            For i = 0 To lstGroup.Items.Count - 1
                lstRejected.Items.Add(lstGroup.Items(i))
            Next
            i = 0
            For i = 0 To lstGroup.Items.Count - 1
                lstGroup.Items.Remove(lstGroup.Items(0))
            Next
            lstRejected.Refresh()
            lstGroup.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        End Try
    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        Dim Alltxt As String = ""
        If Trim(txtwcID.Text) = "" Then
            MsgBox("select the work center id!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If

        Dim i As Integer
        Dim st As Integer = 1
        If lstGroup.Items.Count > 0 Then
            For i = 0 To lstGroup.Items.Count - 1
                Try
                    With com
                        .Connection = cn
                        .CommandType = CommandType.StoredProcedure
                        .CommandText = "sp_Add_RejectedGroup"
                        ',_sIDno
                        parm = .Parameters.Add("@sIDno", SqlDbType.Int)
                        parm.Value = Val(txtRID.Text)
                        parm = .Parameters.Add("@RejID", SqlDbType.Int)
                        Dim id As DataItem = lstGroup.Items(i)
                        parm.Value = Val(id.ID)

                        cn.Open()
                        dr = .ExecuteReader
                        If dr.Read Then
                            If Alltxt = "" Then
                                Alltxt = dr.Item("_RejGroupid")
                            Else
                                Alltxt = Alltxt & "," & dr.Item("_RejGroupid")
                            End If
                        End If
                        cn.Close()
                        com.Parameters.Clear()
                        If st = 0 Then
                            st = 1
                        End If
                    End With
                Catch ex As SqlException
                    MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
                    st = 2
                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
                    st = 2
                Finally
                    cn.Close()
                    com.Parameters.Clear()
                End Try

            Next

            If Alltxt <> "" Then
                Try
                    With com
                        .Connection = cn
                        .CommandType = CommandType.Text
                        .CommandText = "delete from tbRejectedGroup where _RejGroupid not in(" & Alltxt & ") and  _sIDno=" & Val(txtRID.Text)
                        cn.Open()
                        .ExecuteNonQuery()

                        cn.Close()
                        com.Parameters.Clear()

                    End With
                Catch ex As SqlException
                    MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")

                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
                Finally
                    cn.Close()
                    com.Parameters.Clear()
                End Try

            End If






            If st = 1 Then
                MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")
            End If
        Else

            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "delete from tbRejectedGroup where   _sIDno=" & Val(txtRID.Text)
                    cn.Open()
                    .ExecuteNonQuery()

                    cn.Close()
                    com.Parameters.Clear()
                    MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")
                End With
            Catch ex As SqlException
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")

            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Finally
                cn.Close()
                com.Parameters.Clear()
            End Try

        End If
        txtwcID.Text = ""
        txtDesc.Text = ""
        Alltxt = ""
        txtRID.Text = ""
        lstGroup.Items.Clear()
        RejectedCode()

        ' lstwrk.SelectedIndex = -1
       



    End Sub

    Private Sub lstwrk_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstwrk.SelectedIndexChanged
        txtwcID.Text = ""
        txtDesc.Text = ""
        lstRejected.Items.Clear()
        lstGroup.Items.Clear()

        Try
            If lstwrk.SelectedIndex >= 0 Then
                Dim id As DataItem = lstwrk.SelectedItem

                Dim S() As String = Split(id.ID, "||")
                txtRID.Text = S(0)
                txtwcID.Text = S(1)
                txtDesc.Text = S(2)

                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    'Dim DI As DataItem
                    .CommandText = "select * from tbRejected A where _RejID not in(select _RejID from tbRejectedGroup where _sIDno='" & Val(txtRID.Text) & "') order by _RejectedCode "
                    cn.Open()
                    dr = .ExecuteReader(CommandBehavior.CloseConnection)
                    While dr.Read
                        lstRejected.Items.Add(New DataItem(dr.Item("_RejID"), dr.Item("_RejectedCode") & "-" & dr.Item("_RejectedDesc")))
                        '/lstwrk.Items.Add(dr.Item("_wc") & " - " & dr.Item("_description"))
                    End While
                    cn.Close()
                End With
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try

        Try
            If lstwrk.SelectedIndex >= 0 Then
                Dim id As DataItem = lstwrk.SelectedItem
                Dim S() As String = Split(id.ID, "||")
                txtRID.Text = S(0)
                txtwcID.Text = S(1)
                txtDesc.Text = S(2)

                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    'Dim DI As DataItem
                    .CommandText = "select * from tbRejected A where _RejID  in(select _RejID from tbRejectedGroup where _sIDno='" & Val(txtRID.Text) & "') order by _RejectedCode "
                    cn.Open()
                    dr = .ExecuteReader(CommandBehavior.CloseConnection)
                    While dr.Read
                        lstGroup.Items.Add(New DataItem(dr.Item("_RejID"), dr.Item("_RejectedCode") & "-" & dr.Item("_RejectedDesc")))
                        '/lstwrk.Items.Add(dr.Item("_wc") & " - " & dr.Item("_description"))
                    End While
                    cn.Close()
                End With
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try


    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        txtRID.Text = ""
        txtwcID.Text = ""
        txtDesc.Text = ""
        ' cmbRG.SelectedIndex = 0
        lstGroup.Items.Clear()
        RejectedCode()
        'lstwrk.Items.Clear()
        lstwrk.SelectedIndex = -1



    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub
End Class