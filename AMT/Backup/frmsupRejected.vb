Imports System.Data.SqlClient
Public Class frmsupRejected
    Dim DS As New DataSet
    Dim clsM As New clsMain
    Public WCId As String

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        txtClear()
    End Sub
    Sub txtClear()
        cmbRejectedCode.SelectedIndex = 0
        txtRejectedDesc.Text = ""
        txtKeyReject.Text = ""
        cmbRejectedCode.Focus()
    End Sub

    Private Sub frmsupRejected_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With lstv
            .Columns.Add("RJ-Code", 100, HorizontalAlignment.Left)
            .Columns.Add("Desc.", lstv.Width - 201, HorizontalAlignment.Left)
            .Columns.Add("QTY", 100, HorizontalAlignment.Left)
        End With
        loadView()
        getReject()
        LoadReject()
        cmbRejectedCode.Focus()
    End Sub
    Sub getReject()
        DS = clsM.GetDataset("select * from tbRejected A inner join  tbRejectedGroup B on A._RejID=B._RejID where _wc='" & fncstr(WCId) & "'", "tbReject")
    End Sub
    Sub loadView()
        lstv.Items.Clear()
        txttot.Text = 0.0
        If Newhash.Count > 0 Then
            Dim irej As clsRej
            For Each irej In Newhash.Values
                Dim ls As New ListViewItem(Trim(irej.Rcode))    ' you can also use reader.GetSqlValue(0) 
                ls.SubItems.Add(Trim(irej.Desc))
                ls.SubItems.Add(Trim(irej.rQty))
                lstv.Items.Add(ls)
                txttot.Text = Val(txttot.Text) + Val(irej.rQty)
            Next
        End If


    End Sub
    Sub LoadReject()
        cmbRejectedCode.Items.Clear()
        cmbRejectedCode.Items.Add(New DataItem(Trim("-Select-"), Trim("-Select-")))
        If DS.Tables(0).Rows.Count > 0 Then
            Dim i As Integer
            For i = 0 To DS.Tables(0).Rows.Count - 1
                With DS.Tables(0).Rows(i)
                    cmbRejectedCode.Items.Add(New DataItem(.Item("_RejectedDesc"), .Item("_RejectedCode")))
                End With
            Next
        End If
        cmbRejectedCode.SelectedIndex = 0
    End Sub








    'Private Sub txtMain_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMain.KeyPress
    '    If Asc(e.KeyChar) = 13 Then

    '        'If UCase(txtMain.Text) = "CL" Then
    '        '    Newhash.Clear()
    '        '    lstv.Items.Clear()
    '        '    txttot.Text = 0.0
    '        '    txtMain.Text = ""
    '        '    txtMain.Focus()
    '        '    Exit Sub
    '        'End If
    '        If UCase(txtMain.Text) = "SA" Then
    '            If Val(txtRejQty.Text) <> Val(txttot.Text) Then
    '                MsgBox("Rejected quantity not equal to total quantity", MsgBoxStyle.Information, "eWIP")
    '                Me.Close()
    '            End If
    '        End If
    '        If UCase(txtMain.Text) = "EX" Then
    '            If Val(txtRejQty.Text) <> Val(txttot.Text) Then
    '                MsgBox("Rejected quantity not equal to total quantity", MsgBoxStyle.Information, "eWIP")
    '                Me.Close()
    '            Else
    '                Me.Close()
    '            End If
    '        End If

    '        If Trim(txtMain.Text) <> "" Then

    '            Dim iItem As clsRej
    '            Dim DR As DataRow() = DS.Tables(0).Select("_RejectedCode='" & fncstr(txtMain.Text) & "'")
    '            txtMain.Text = ""
    '            If DR.Length > 0 Then
    '                If DR(0).Item("_itemCode") = Trim(lblItem.Text) Or DR(0).Item("_itemCode") = "" Then
    '                    Dim str As String = ""
    '                    Dim strCode As String = ""
    '                    strCode = DR(0).Item("_RejectedCode")
    '                    If Not (Newhash.ContainsKey(strCode)) Then
    '                        iItem = New clsRej
    '                        lblRejectCode.Text = DR(0).Item("_RejectedCode")
    '                        lblDesc.Text = DR(0).Item("_RejectedDesc")
    '                        txtKeyReject.Text = ""
    '                        txtKeyReject.Visible = True
    '                        txtKeyReject.Focus()
    '                    Else
    '                        Dim irej As clsRej = Newhash(DR(0).Item("_RejectedCode"))
    '                        lblRejectCode.Text = DR(0).Item("_RejectedCode")
    '                        lblDesc.Text = DR(0).Item("_RejectedDesc")
    '                        txtKeyReject.Text = irej.rQty
    '                        txtKeyReject.Visible = True
    '                        txtKeyReject.Focus()
    '                    End If

    '                Else
    '                    lblMsg.Text = "Please check rejected code!"
    '                    txtMain.Text = ""
    '                End If
    '            Else

    '                lblMsg.Text = "Please check rejected code!"
    '                txtMain.Text = ""
    '            End If
    '        End If



    '    End If
    'End Sub

    'Private Sub txtKeyReject_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtKeyReject.KeyPress
    '    If Asc(e.KeyChar) = 13 Then
    '        If Val(txtKeyReject.Text) = 0 Then
    '            Dim strCode As String = ""
    '            strCode = lblRejectCode.Text
    '            If (Newhash.ContainsKey(strCode)) Then
    '                Newhash.Remove(strCode)
    '                lblRejectCode.Text = ""
    '                lblDesc.Text = ""
    '                txtKeyReject.Text = ""
    '                txtKeyReject.Visible = False
    '                txtMain.Focus()
    '                loadView()
    '            Else
    '                lblRejectCode.Text = ""
    '                lblDesc.Text = ""
    '                txtKeyReject.Text = ""
    '                txtKeyReject.Visible = False
    '                txtMain.Focus()
    '                loadView()
    '            End If
    '        Else

    '            Dim strCode As String = ""
    '            strCode = lblRejectCode.Text
    '            If (Newhash.ContainsKey(strCode)) Then
    '                Newhash.Remove(strCode)
    '                Dim clsI As New clsRej
    '                clsI.Rcode = lblRejectCode.Text
    '                clsI.Desc = lblDesc.Text
    '                clsI.rQty = Val(txtKeyReject.Text)
    '                Newhash.Add(strCode, clsI)
    '                lblRejectCode.Text = ""
    '                lblDesc.Text = ""
    '                txtKeyReject.Text = ""
    '                txtKeyReject.Visible = False
    '                txtMain.Focus()
    '                loadView()
    '            Else
    '                Dim clsI As New clsRej
    '                clsI.Rcode = lblRejectCode.Text
    '                clsI.Desc = lblDesc.Text
    '                clsI.rQty = Val(txtKeyReject.Text)
    '                Newhash.Add(strCode, clsI)
    '                lblRejectCode.Text = ""
    '                lblDesc.Text = ""
    '                txtKeyReject.Text = ""
    '                txtKeyReject.Visible = False
    '                txtMain.Focus()
    '                loadView()
    '            End If





    '        End If
    '    End If

    '    If (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57) Or Asc(e.KeyChar) = 46 Or Asc(e.KeyChar) = 8 Then
    '        e.Handled = False
    '    Else
    '        e.Handled = True
    '    End If
    'End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        Me.Close()
    End Sub

    Private Sub butRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butRemove.Click

        Dim strCode As String = ""
        strCode = cmbRejectedCode.Text
        If (Newhash.ContainsKey(cmbRejectedCode.Text)) Then
            Newhash.Remove(cmbRejectedCode.Text)
            cmbRejectedCode.SelectedIndex = 0
            txtRejectedDesc.Text = ""
            txtKeyReject.Text = ""
            ' txtKeyReject.Visible = False
            cmbRejectedCode.Focus()
            loadView()
        Else
            cmbRejectedCode.SelectedIndex = 0
            txtRejectedDesc.Text = ""
            txtKeyReject.Text = ""
            ' txtKeyReject.Visible = False
            cmbRejectedCode.Focus()
            loadView()
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If cmbRejectedCode.SelectedIndex = 0 Then
            MsgBox("select rejected code!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If Val(txtKeyReject.Text) = 0 Then
            MsgBox("Enter rejected quantity!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        If (Newhash.ContainsKey(cmbRejectedCode.Text)) Then
            Newhash.Remove(cmbRejectedCode.Text)
            Dim clsI As New clsRej
            clsI.Rcode = cmbRejectedCode.Text
            clsI.Desc = txtRejectedDesc.Text
            clsI.rQty = Val(txtKeyReject.Text)
            Newhash.Add(cmbRejectedCode.Text, clsI)
            cmbRejectedCode.SelectedIndex = 0
            txtRejectedDesc.Text = ""
            txtKeyReject.Text = ""
            cmbRejectedCode.Focus()
            loadView()
        Else
            Dim clsI As New clsRej
            clsI.Rcode = cmbRejectedCode.Text
            clsI.Desc = txtRejectedDesc.Text
            clsI.rQty = Val(txtKeyReject.Text)
            Newhash.Add(cmbRejectedCode.Text, clsI)
            cmbRejectedCode.SelectedIndex = 0
            txtRejectedDesc.Text = ""
            txtKeyReject.Text = ""
            cmbRejectedCode.Focus()
            loadView()
        End If


    End Sub

    Private Sub cmbRejectedCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRejectedCode.SelectedIndexChanged
        If cmbRejectedCode.SelectedIndex > 0 Then
            txtRejectedDesc.Text = cmbRejectedCode.SelectedItem.id
        Else
            txtRejectedDesc.Text = ""
        End If

    End Sub

    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click
        cmbRejectedCode.Text = "-Select-"
        cmbRejectedCode.Text = lstv.SelectedItems(0).Text
        If cmbRejectedCode.Text <> lstv.SelectedItems(0).Text Then
            cmbRejectedCode.Items.Add(New DataItem(lstv.SelectedItems(0).SubItems(1).Text, lstv.SelectedItems(0).Text))
        End If
        cmbRejectedCode.Text = lstv.SelectedItems(0).Text
        txtRejectedDesc.Text = lstv.SelectedItems(0).SubItems(1).Text
        txtKeyReject.Text = lstv.SelectedItems(0).SubItems(2).Text
    End Sub
   
End Class