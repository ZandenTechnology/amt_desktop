Imports System.Data.SqlClient
Public Class frmDataEntry
    Dim dr As SqlDataReader
    Dim parm As SqlParameter
    Dim Vdd As Integer = 0
    Dim orid As String
    Dim RelQtyAll As Double
    Dim Countdown As Integer = 0
    Private Sub butNotepad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butNotepad.Click
        Try
            Shell("C:\WINDOWS\system32\notepad.exe", AppWinStyle.NormalFocus)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub butCal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCal.Click
        Try
            Shell("C:\WINDOWS\system32\calc.exe", AppWinStyle.NormalFocus)
        Catch ex As Exception
        End Try
    End Sub
    Private Sub frmDataEntry_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtSubval.Text = ""
        'LaodFix()
        CheckTansdata()
        If SessionJobno <> "" Then
            butExit.Visible = True
            txtWKID.Text = SessionJobno
            SessionJobno = ""
            selData()
        End If
        lblMsgBox.Text = ""
        'cmbsuf.SelectedIndex = 0
        txtSufParent.Text = "M"
        With lstv
            .Columns.Add("IDNo", 0, HorizontalAlignment.Left)
            .Columns.Add("Job ID", 100, HorizontalAlignment.Left)
            .Columns.Add("Parent", 50, HorizontalAlignment.Left)
            .Columns.Add("Suffix", 50, HorizontalAlignment.Left)
            .Columns.Add("Work Station ID", 100, HorizontalAlignment.Left)
            .Columns.Add("Operation No.", 50, HorizontalAlignment.Left)
            .Columns.Add("Item No.", 100, HorizontalAlignment.Left)
            .Columns.Add("Machine ID", 100, HorizontalAlignment.Left)
            .Columns.Add("Date Started.", 100, HorizontalAlignment.Left)
            .Columns.Add("Date End", 100, HorizontalAlignment.Left)
            .Columns.Add("Time Started", 100, HorizontalAlignment.Left)
            .Columns.Add("Time Ented", 100, HorizontalAlignment.Left)
            .Columns.Add("Received Qty", 100, HorizontalAlignment.Left)
            .Columns.Add("Complited Qty", 100, HorizontalAlignment.Left)
            .Columns.Add("Rejected Qty", 100, HorizontalAlignment.Left)
            lbltotCom.Text = 0.0

            lbltotRej.Text = 0.0
            list_Displaydata()
            'list_Displaydata()
        End With

        txtWKID.Select()

    End Sub
    Sub list_Displaydata()
        lstv.Items.Clear()
        Dim suf As Integer = 0
        Dim TotC, TotR, TotOP As Double
        TotC = 0.0
        TotR = 0.0
        TotOP = 0.0
        Dim cf As Boolean = False
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                If txtSufParent.Text = "-" Then
                    .CommandText = "select * from tbJobtrans where _job='" & fncstr(txtWKID.Text) & "' and _oper_num=" & Val(txtOperationNo.Text) & "  order by _trans_dateto, _tansnum"
                Else
                    .CommandText = "select * from tbJobtrans where _job='" & fncstr(txtWKID.Text) & "' and _oper_num=" & Val(txtOperationNo.Text) & " and _jobsuffixParent='" & Trim(txtSufParent.Text) & "'  order by _trans_dateto, _tansnum"
                End If
                cn.Open()
                dr = .ExecuteReader()
                ' Dim cf As Boolean = False
                While dr.Read


                    If cf = False Then
                        If txtEmpid.Text = dr.Item("_emp_num") And txtMachineid.Text = "-" Then
                            If dr.Item("_trans_dateto") = 0 Then
                                txtStartDate.Text = Format(DateTime.FromOADate(dr.Item("_trans_datefrm")), "dd/MM/yyyy")
                                txtStartTime.Text = dr.Item("_start_time")
                                '  txtEndDate.Text = Format(Now, "dd/MM/yyyy")
                                ' txtEndTime.Text = Format(Now, "HHmm")
                                txtOperators.Text = dr.Item("_no_oper")
                                txtOPQTY.Text = dr.Item("_qty_op_qty")
                                ' cmbsuf.Text = dr.Item("_jobsuffix")
                                txtsuf.Text = dr.Item("_jobsuffix")
                                txtTansID.Text = dr.Item("_tansnum")
                                cf = True
                                TotOP = TotOP - dr.Item("_qty_op_qty")
                                ' plHand.Visible = True
                            End If
                        ElseIf txtEmpid.Text = dr.Item("_emp_num") And txtMachineid.Text = dr.Item("_machineid") Then
                            If dr.Item("_trans_dateto") = 0 Then
                                txtStartDate.Text = Format(DateTime.FromOADate(dr.Item("_trans_datefrm")), "dd/MM/yyyy")
                                txtStartTime.Text = dr.Item("_start_time")
                                '  txtEndDate.Text = Format(Now, "dd/MM/yyyy")
                                ' txtEndTime.Text = Format(Now, "HHmm")
                                txtOperators.Text = dr.Item("_no_oper")
                                txtOPQTY.Text = dr.Item("_qty_op_qty")
                                txtsuf.Text = dr.Item("_jobsuffix")
                                'cmbsuf.Text = dr.Item("_jobsuffix")
                                txtTansID.Text = dr.Item("_tansnum")
                                cf = True
                                TotOP = TotOP - dr.Item("_qty_op_qty")
                                'plHand.Visible = True
                            End If
                        Else
                            ' txtStartDate.Text = Format(DateTime.FromOADate(dr.Item("_trans_datefrm")), "dd/MM/yyyy")
                            '  txtStartTime.Text = dr.Item("_start_time")
                            ' txtEndDate.Text = Format(Now, "dd/MM/yyyy")
                            'txtEndTime.Text = Format(Now, "HHmm")
                            txtOperators.Text = ""
                            txtOPQTY.Text = ""
                            'cmbsuf.Text = "-"
                            txtTansID.Text = 0
                            'plHand.Visible = False
                        End If
                    End If


















                    Dim ls As New ListViewItem(Trim(dr.Item("_tansnum")))    ' you can also use reader.GetSqlValue(0) 
                    ls.SubItems.Add(Trim(dr.Item("_job")))
                    ls.SubItems.Add(Trim(dr.Item("_jobsuffixParent")))
                    ls.SubItems.Add(Trim(dr.Item("_jobsuffix")))
                    ls.SubItems.Add(Trim(dr.Item("_wc")))
                    ls.SubItems.Add(Trim(dr.Item("_oper_num")))
                    ls.SubItems.Add(Trim(dr.Item("_item")))
                    ls.SubItems.Add(Trim(dr.Item("_machineid")))
                    ls.SubItems.Add(Format(DateTime.FromOADate(dr.Item("_trans_datefrm")), "dd/MM/yyyy"))
                    If dr.Item("_trans_dateto") = 0 Then
                        ls.SubItems.Add("-")
                    Else
                        ls.SubItems.Add(Format(DateTime.FromOADate(dr.Item("_trans_dateto")), "dd/MM/yyyy"))
                    End If

                    ls.SubItems.Add(dr.Item("_start_time"))
                    ls.SubItems.Add(dr.Item("_end_time"))
                    ls.SubItems.Add(dr.Item("_qty_op_qty"))
                    ls.SubItems.Add(dr.Item("_qty_complete"))
                    ls.SubItems.Add(dr.Item("_qty_scrapped"))
                    If dr.Item("_trans_dateto") = 0 Then
                        lstv.Items.Add(ls).ForeColor = Color.Red
                    Else
                        lstv.Items.Add(ls)
                    End If
                    suf = suf + 1
                    TotC = TotC + dr.Item("_qty_complete")
                    TotR = TotR + dr.Item("_qty_scrapped")
                    TotOP = TotOP + dr.Item("_qty_op_qty")
                End While
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            txtWKID.Select()
        Finally
            cn.Close()

        End Try
        lbltotCom.Text = TotC
        lbltotRej.Text = TotR
        lblTotOPQTY.Text = TotOP
        'cmbsuf.Enabled = False
        If cf = False Then
            ' cmbsuf.SelectedIndex = suf
        End If
        Vdd = suf
    End Sub

    Private Sub txtWKID_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWKID.Enter
        orid = ""
        orid = txtWKID.Text
    End Sub
    Private Sub getWKID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtWKID.KeyPress
        lblMsgBox.Text = ""
        If Asc(e.KeyChar) = 13 Then

            If txtWKID.TextLength >= 2 Then
                Dim idv As String = UCase(Mid(txtWKID.Text, 1, 2))
                If UCase(idv) = "ID" Then
                    txtWKID.Text = Trim(UCase(Mid(txtWKID.Text, 3, txtWKID.TextLength)))
                End If
            End If


            RelQtyAll = 0
            selData()
            CheckTansdata()
        End If


    End Sub

    Sub selData()
        ClearAll()
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select * from tbJob where _job='" & fncstr(txtWKID.Text) & "'"
                cn.Open()
                dr = .ExecuteReader()
                If dr.Read Then
                    txtReleasedqty.Text = dr.Item("_qtyReleased")
                    RelQtyAll = dr.Item("_qtyReleased")
                    txtItemNo.Text = dr.Item("_item")
                    txtItemNoTemp.Text = dr.Item("_item")
                    txtOperationNo.Select()
                    txtOperationNo.Focus()
                Else
                    lblMsgBox.Text = "Please check your JOB NO.!(" & txtWKID.Text & ")"
                    txtWKID.Clear()
                    txtWKID.Select()
                End If
            End With

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            txtWKID.Select()
        Finally
            cn.Close()

        End Try
    End Sub

    'Private Sub txtWKSatationID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtWKSatationID.KeyPress
    '    If Asc(e.KeyChar) = 13 Then
    '        If Trim(txtWKID.Text) <> "" Then
    '            wstClear()
    '            Try
    '                With com
    '                    .Connection = cn
    '                    .CommandType = CommandType.Text
    '                    .CommandText = "select * from tbJobRoute where _job='" & fncstr(txtWKID.Text) & "' and _wc='" & fncstr(txtWKSatationID.Text) & "'"
    '                    cn.Open()
    '                    dr = .ExecuteReader()
    '                    If dr.Read Then
    '                        txtJobNo.Text = dr.Item("_job")
    '                        txtOperationNo.Text = dr.Item("_operationNo")
    '                        txtItemNo.Text = txtItemNoTemp.Text
    '                        txtOperators.Select()
    '                        cn.Close()
    '                        list_Displaydata()
    '                    End If
    '                End With

    '            Catch ex As Exception
    '                MessageBox.Show(ex.Message)
    '            Finally
    '                cn.Close()
    '            End Try

    '        Else
    '            lblMsgBox.Text = "Please scan SCAN (JOB NO.)!"
    '        End If

    '    End If
    'End Sub
    Sub ClearAll()
        txtSufParent.Text = "-"
        txtsuf.Text = "-"
        txtWKSatationID.Clear()
        txtWKSatation.Clear()
        txtOperationNo.Clear()
        'txtJobNo.Clear()
        txtItemNo.Clear()
        'cmbsuf.SelectedIndex = 0
        txtItemNoTemp.Clear()
        txtReleasedqty.Clear()
        txtRejectedQty.Clear()
        txtStartDate.Clear()
        txtEndDate.Clear()
        txtStartTime.Clear()
        txtEndTime.Clear()
        txtOperators.Clear()
        txtCompletedQty.Clear()
        txtRejCode1.Clear()
        txtRejCode2.Clear()
        txtRejCode3.Clear()
        txtRejDes1.Clear()
        txtRejDes2.Clear()
        txtRejDes3.Clear()
        txtRejqty1.Clear()
        txtRejqty2.Clear()
        txtRejqty2.Clear()
        lstv.Items.Clear()
        lbltotCom.Text = 0.0
        lbltotRej.Text = 0.0
        lstv.Items.Clear()
        ' txtNextOPNo.Text = ""
        txtOPQTY.Text = ""
        'txtEmpid.Text = ""
        'txtEmpName.Text = ""
    End Sub

    Sub wstClear()
        txtOperationNo.Clear()
        'txtJobNo.Clear()
        txtItemNo.Clear()
        txtsuf.Text = "-"
        'cmbsuf.SelectedIndex = 0
        txtRejectedQty.Clear()
        txtStartDate.Clear()
        txtEndDate.Clear()
        txtStartTime.Clear()
        txtEndTime.Clear()
        txtOperators.Clear()
        txtCompletedQty.Clear()
        txtRejCode1.Clear()
        txtRejCode2.Clear()
        txtRejCode3.Clear()
        txtRejDes1.Clear()
        txtRejDes2.Clear()
        txtRejDes3.Clear()
        txtRejqty1.Clear()
        txtRejqty2.Clear()
        txtRejqty2.Clear()
        lstv.Items.Clear()
        lbltotCom.Text = 0.0
        lbltotRej.Text = 0.0
    End Sub

    Private Sub txtOperators_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtOperators.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtStartDate.Select()
        End If
    End Sub

    Private Sub txtStartTime_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtStartTime.KeyPress
        If (Asc(e.KeyChar) > 47 And Asc(e.KeyChar) < 58) Or Asc(e.KeyChar) = 8 Or Asc(e.KeyChar) = 13 Then
            e.Handled = False
            If Asc(e.KeyChar) = 13 Then
                txtEndDate.Select()
            End If
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtStartDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtStartDate.KeyPress
        If (Asc(e.KeyChar) > 46 And Asc(e.KeyChar) < 58) Or Asc(e.KeyChar) = 8 Or Asc(e.KeyChar) = 13 Then
            e.Handled = False
            If Asc(e.KeyChar) = 13 Then
                txtStartTime.Select()
            End If
        Else
            e.Handled = True
        End If
    End Sub

  
    
   
    Private Sub txtStartDate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtStartDate.TextChanged

    End Sub

    Private Sub txtEndDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEndDate.KeyPress
        If (Asc(e.KeyChar) > 46 And Asc(e.KeyChar) < 58) Or Asc(e.KeyChar) = 8 Or Asc(e.KeyChar) = 13 Then
            e.Handled = False
            If Asc(e.KeyChar) = 13 Then
                txtEndTime.Select()
            End If
        Else
            e.Handled = True
        End If
    End Sub

   

    Private Sub txtEndTime_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEndTime.KeyPress
        If (Asc(e.KeyChar) > 47 And Asc(e.KeyChar) < 58) Or Asc(e.KeyChar) = 8 Or Asc(e.KeyChar) = 13 Then
            e.Handled = False
            If Asc(e.KeyChar) = 13 Then
                txtOPQTY.Focus()
            End If
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtCompletedQty_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCompletedQty.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtRejectedQty.Select()
        End If
    End Sub
   
    Private Sub txtRejectedQty_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRejectedQty.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtRejCode1.Select()
        End If
    End Sub
    Private Sub txtRejCode1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRejCode1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "select * from tbFailure where _FailureCode='" & fncstr(txtRejCode1.Text) & "'"
                    cn.Open()
                    dr = .ExecuteReader()
                    If dr.Read Then
                        txtRejDes1.Text = dr.Item("_failureDescription")
                        txtRejqty1.Select()
                    Else
                        lblMsgBox.Text = "Please check rejected cod (" & txtRejCode1.Text & " )!"
                        txtRejCode1.Clear()
                        txtRejDes1.Clear()
                        txtRejqty1.Clear()
                    End If
                End With

            Catch ex As Exception
                MessageBox.Show(ex.Message)
            Finally
                cn.Close()
            End Try
        End If
    End Sub
    Private Sub txtRejqty1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRejqty1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtRejCode2.Select()
        End If
    End Sub
    Private Sub txtRejCode2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRejCode2.KeyPress

        If Asc(e.KeyChar) = 13 Then
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "select * from tbFailure where _FailureCode='" & fncstr(txtRejCode2.Text) & "'"
                    cn.Open()
                    dr = .ExecuteReader()
                    If dr.Read Then
                        txtRejDes2.Text = dr.Item("_failureDescription")
                        txtRejqty2.Select()
                    Else
                        lblMsgBox.Text = "Please check rejected cod (" & txtRejCode2.Text & " )!"
                        txtRejCode2.Clear()
                        txtRejDes2.Clear()
                        txtRejqty2.Clear()
                    End If
                End With

            Catch ex As Exception
                MessageBox.Show(ex.Message)
            Finally
                cn.Close()
            End Try
        End If



       
    End Sub
    Private Sub txtRejqty2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRejqty2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtRejCode3.Select()
        End If
    End Sub

    Private Sub txtRejCode3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRejCode3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "select * from tbFailure where _FailureCode='" & fncstr(txtRejCode3.Text) & "'"
                    cn.Open()
                    dr = .ExecuteReader()
                    If dr.Read Then
                        txtRejDes3.Text = dr.Item("_failureDescription")
                        txtRejqty3.Select()
                    Else
                        lblMsgBox.Text = "Please check rejected cod (" & txtRejCode3.Text & " )!"
                        txtRejCode3.Clear()
                        txtRejDes3.Clear()
                        txtRejqty3.Clear()
                    End If
                End With

            Catch ex As Exception
                MessageBox.Show(ex.Message)
            Finally
                cn.Close()
            End Try
        End If
    End Sub
   
    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If checkUserID() = False Then
            Exit Sub
        End If
        'If Trim(txtWKID.Text) = "" Then
        '    lblMsgBox.Text = "Please scan JOB NO.!"
        '    Exit Sub
        'End If
        'If Trim(txtWKSatationID.Text) = "" Then
        '    lblMsgBox.Text = "Please scan workstation id!"
        '    Exit Sub
        'End If
        'If Trim(txtWKSatationID.Text) = "" Then
        '    lblMsgBox.Text = "Please scan workstation id!"
        '    Exit Sub
        'End If
        'If Trim(txtStartDate.Text) = "" Then
        '    lblMsgBox.Text = "Please enter start date!"
        '    Exit Sub
        'End If
        'If Trim(txtStartTime.Text) = "" Then
        '    lblMsgBox.Text = "Please enter start time!"
        '    Exit Sub
        'End If
        'If Trim(txtEndDate.Text) = "" Then
        '    lblMsgBox.Text = "Please enter end date!"
        '    Exit Sub
        'End If
        'If Trim(txtCompletedQty.Text) = "" Then
        '    lblMsgBox.Text = "Please enter completed quantity!"
        '    Exit Sub
        'End If
        'If Val(txtCompletedQty.Text) > Val(txtReleasedqty.Text) Then
        '    lblMsgBox.Text = "Please check completed quantity!"
        '    Exit Sub
        'End If
        'If Val(txtReleasedqty.Text) < Val(txtRejectedQty.Text) Then
        '    lblMsgBox.Text = "Please check rejected quantity!"
        '    Exit Sub
        'End If
        'If Val(txtReleasedqty.Text) < (Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text)) Then
        '    lblMsgBox.Text = "Please check completed and rejected  quantity!"
        '    Exit Sub
        'End If

        'Dim Totq As Double
        'Totq = Val(lbltotCom.Text) + Val(lbltotRej.Text) + Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text)

        'If Val(txtReleasedqty.Text) < Totq Then
        '    lblMsgBox.Text = "Please check completed and rejected  quantity!"
        '    Exit Sub
        'End If



        'If Val(txtRejectedQty.Text) > 0 Then
        '    If Trim(txtRejDes1.Text) = "" Then
        '        lblMsgBox.Text = "Please scan rejected code!"
        '        Exit Sub
        '    End If
        'End If
        'Dim s() As String
        'Dim s1() As String
        'Dim sd, ED As Date
        'Try
        '    s = Split(txtStartDate.Text, "/")
        '    sd = DateSerial(s(2), s(1), s(0))
        '    s1 = Split(txtStartDate.Text, "/")
        '    ED = DateSerial(s1(2), s1(1), s1(0))
        'Catch ex As Exception
        '    lblMsgBox.Text = "Please check date!"
        '    Exit Sub
        'End Try

        'Try
        '    Dim n As Integer = Val(Mid(txtStartTime.Text, 1, 2))
        '    If n > 23 Then
        '        lblMsgBox.Text = "Please check ime!"
        '        Exit Sub
        '    End If
        '    n = Val(Mid(txtStartTime.Text, 3, 4))
        '    If n > 59 Then
        '        lblMsgBox.Text = "Please check ime!"
        '        Exit Sub
        '    End If

        '    n = Val(Mid(txtEndTime.Text, 1, 2))
        '    If n > 23 Then
        '        lblMsgBox.Text = "Please check ime!"
        '        Exit Sub
        '    End If
        '    n = Val(Mid(txtEndTime.Text, 3, 4))
        '    If n > 59 Then
        '        lblMsgBox.Text = "Please check ime!"
        '        Exit Sub
        '    End If
        'Catch ex As Exception
        'End Try



        'Try

        '    '_job()
        '    '_jobsuffix()
        '    '_item()
        '    '_jobDate()
        '    '_trans_type()
        '    '_trans_date()
        '    '_qty_complete()
        '    '_qty_scrapped()
        '    '_oper_num()
        '    '_emp_num()
        '    '_no_oper()
        '    '_start_time()
        '    '_end_time()
        '    '_CreatedBy()
        '    '_CreateDate()
        '    '_updateby()
        '    '_updateDate()
        '    '_rejectedCode1()
        '    '_rejectedDesc1()
        '    '_rejectedqty1()
        '    '_rejectedCode2()
        '    '_rejectedDesc2()
        '    '_rejectedqty2()
        '    '_rejectedCode3()
        '    '_rejectedDesc3()
        '    '_rejectedqty4()
        '    With com


        '        .Connection = cn
        '        .CommandType = CommandType.StoredProcedure
        '        .CommandText = "sp_Job_Add"
        '        parm = .Parameters.Add("@job", SqlDbType.VarChar, 50)
        '        parm.Value = fncstr(txtWKID.Text)
        '        parm = .Parameters.Add("@jobsuffix", SqlDbType.VarChar, 50)
        '        If cmbsuf.SelectedIndex = 0 Then
        '            parm.Value = "0"
        '        Else
        '            parm.Value = cmbsuf.SelectedIndex
        '        End If
        '        parm = .Parameters.Add("@wc", SqlDbType.VarChar, 50)
        '        parm.Value = fncstr(txtWKSatationID.Text)
        '        parm = .Parameters.Add("@oper_num", SqlDbType.Int)
        '        parm.Value = Val(fncstr(txtOperationNo.Text))
        '        parm = .Parameters.Add("@item", SqlDbType.VarChar, 256)
        '        parm.Value = fncstr(txtItemNo.Text)
        '        parm = .Parameters.Add("@JobDate", SqlDbType.Float)
        '        parm.Value = 0
        '        parm = .Parameters.Add("@trans_type", SqlDbType.Char, 10)
        '        parm.Value = ""
        '        parm = .Parameters.Add("@trans_datefrm", SqlDbType.Float)
        '        parm.Value = sd.ToOADate
        '        parm = .Parameters.Add("@trans_dateto", SqlDbType.Float)
        '        parm.Value = ED.ToOADate
        '        parm = .Parameters.Add("@trans_Timefrm", SqlDbType.Int)
        '        parm.Value = Val(txtStartTime.Text)
        '        parm = .Parameters.Add("@trans_TimeTo", SqlDbType.Int)
        '        parm.Value = Val(txtEndTime.Text)
        '        parm = .Parameters.Add("@qty_op_qty", SqlDbType.Decimal)
        '        parm.Value = Val(txtOPQTY.Text)
        '        parm = .Parameters.Add("@qty_complete", SqlDbType.Decimal)
        '        parm.Value = Val(txtCompletedQty.Text)
        '        parm = .Parameters.Add("@qty_scrapped", SqlDbType.Decimal)
        '        parm.Value = Val(txtRejectedQty.Text)
        '        parm = .Parameters.Add("@no_oper", SqlDbType.Int)
        '        parm.Value = Val(txtOperationNo.Text)
        '        parm = .Parameters.Add("@CreatedBy", SqlDbType.VarChar, 255)
        '        parm.Value = LogUserName

        '        parm = .Parameters.Add("@rejectedCode1", SqlDbType.VarChar, 50)
        '        parm.Value = fncstr(txtRejCode1.Text)
        '        parm = .Parameters.Add("@rejectedDesc1", SqlDbType.VarChar, 256)
        '        parm.Value = fncstr(txtRejDes1.Text)
        '        parm = .Parameters.Add("@rejectedqty1", SqlDbType.Decimal)
        '        parm.Value = Val(fncstr(txtRejqty1.Text))

        '        parm = .Parameters.Add("@rejectedCode2", SqlDbType.VarChar, 50)
        '        parm.Value = fncstr(txtRejCode2.Text)
        '        parm = .Parameters.Add("@rejectedDesc2", SqlDbType.VarChar, 256)
        '        parm.Value = fncstr(txtRejDes2.Text)
        '        parm = .Parameters.Add("@rejectedqty2", SqlDbType.Decimal)
        '        parm.Value = Val(fncstr(txtRejqty2.Text))

        '        parm = .Parameters.Add("@rejectedCode3", SqlDbType.VarChar, 50)
        '        parm.Value = fncstr(txtRejCode3.Text)
        '        parm = .Parameters.Add("@rejectedDesc3", SqlDbType.VarChar, 256)
        '        parm.Value = fncstr(txtRejDes3.Text)
        '        parm = .Parameters.Add("@rejectedqty3", SqlDbType.Decimal)
        '        parm.Value = Val(fncstr(txtRejqty3.Text))
        '        parm = .Parameters.Add("@tansnum", SqlDbType.Int)
        '        parm.Value = Val(txtTansID.Text)
        '        parm = .Parameters.Add("@nextopno", SqlDbType.Int)
        '        parm.Value = Val(txtNextOPNo.Text)

        '        cn.Open()
        '        .ExecuteNonQuery()
        '        cn.Close()
        '        com.Parameters.Clear()
        '        txtWKID.Clear()
        '        ClearAll()
        '        txtWKID.Select()
        '        MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")
        '    End With

        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)
        'Finally
        '    cn.Close()
        '    com.Parameters.Clear()
        'End Try







        If Trim(txtWKID.Text) = "" Then
            lblMsgBox.Text = "Please scan JOB NO.!"
            Exit Sub
        End If
        If Trim(txtWKSatationID.Text) = "" Then
            lblMsgBox.Text = "Please scan workstation id!"
            Exit Sub
        End If
        If Trim(txtStartDate.Text) = "" Then
            lblMsgBox.Text = "Please enter start date!"
            Exit Sub
        End If
        If Trim(txtStartTime.Text) = "" Then
            lblMsgBox.Text = "Please enter start time!"
            Exit Sub
        End If
        If Trim(txtEmpid.Text) = "" Then
            lblMsgBox.Text = "Please enter Operator ID!"
            Exit Sub
        End If
        If txtCompletedQty.Text <> "" And Val(txtCompletedQty.Text) <> 0 Then
            If Trim(txtEndDate.Text) = "" Then
                lblMsgBox.Text = "Please enter start date!"
                Exit Sub
            End If
        End If
        If Trim(txtEndDate.Text) <> "" Then
            'If Val(txtCompletedQty.Text) > Val(txtReleasedqty.Text) Then
            '    lblMsgBox.Text = "Please check completed quantity!"
            '    Exit Sub
            'End If

            'If Trim(txtOPQTY.Text) < Val(txtCompletedQty.Text) + Val(txtRejectedQty.Text) Then
            '    lblMsgBox.Text = "Please check completed quantity and rejected  quantity!"
            '    Exit Sub
            'End If

            'If Val(txtReleasedqty.Text) < Val(txtRejectedQty.Text) Then
            '    lblMsgBox.Text = "Please check rejected quantity!"
            '    Exit Sub
            'End If
            'If Val(txtReleasedqty.Text) < (Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text)) Then
            '    lblMsgBox.Text = "Please check completed and rejected  quantity!"
            '    Exit Sub
            'End If

            'Dim Totq As Double
            'Totq = Val(lbltotCom.Text) + Val(lbltotRej.Text) + Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text)

            'If Val(txtReleasedqty.Text) < Totq Then
            '    lblMsgBox.Text = "Please check completed and rejected  quantity!"
            '    Exit Sub
            'End If
            'If Val(txtRejectedQty.Text) > 0 Then
            '    If Trim(txtRejDes1.Text) = "" Then
            '        lblMsgBox.Text = "Please scan rejected code!"
            '        Exit Sub
            '    End If
            'End If

            If Val(txtCompletedQty.Text) > Val(txtReleasedqty.Text) Then
                lblMsgBox.Text = "Please check completed quantity!"
                Exit Sub
            End If
            If Val(txtReleasedqty.Text) < Val(txtRejectedQty.Text) Then
                lblMsgBox.Text = "Please check rejected quantity!"
                Exit Sub
            End If
            'If Trim(txtOPQTY.Text) > Val(txtCompletedQty.Text) + Val(txtRejectedQty.Text) Then
            '    txtOPQTY.Text = Val(txtCompletedQty.Text) + Val(txtRejectedQty.Text)
            'End If
            If Trim(txtOPQTY.Text) < Val(txtCompletedQty.Text) + Val(txtRejectedQty.Text) Then
                lblMsgBox.Text = "Please check completed and rejected  quantity!"
            End If
            If Val(txtOPQTY.Text) < (Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text)) Then
                lblMsgBox.Text = "Please check completed and rejected  quantity!"
                Exit Sub
            End If

            Dim Totq As Double
            Totq = Val(lbltotCom.Text) + Val(lbltotRej.Text) + Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text) - Val(txtARCQTY.Text)

            If Val(txtReleasedqty.Text) < Totq Then
                lblMsgBox.Text = "Please check completed and rejected  quantity!"
                Exit Sub
            End If
            If Val(txtRejectedQty.Text) > 0 Then
                If Trim(txtRejDes1.Text) = "" Then
                    lblMsgBox.Text = "Please scan rejected code!"
                    Exit Sub
                End If
            End If


        End If
        If Trim(txtMachineid.Text) = "" Then
            lblMsgBox.Text = "Please enter machine ID!"
            Exit Sub
        End If
        If Trim(txtMachineid.Text) <> "" Then
            If CheckMachineID() = False Then
                lblMsgBox.Text = "Please check machine id(" & txtMachineid.Text & ")"
                txtMachineid.Text = ""
            End If
        End If

        If Trim(txtOPQTY.Text) = "" Or Val(txtOPQTY.Text) = 0 Then
            lblMsgBox.Text = "Please enter operation quantity!"
            Exit Sub
        End If



        Dim s() As String
        Dim s1() As String
        Dim sd, ED As Date
        Dim sddbl, EDdbl As Double
        Try
            s = Split(txtStartDate.Text, "/")
            sd = DateSerial(s(2), s(1), s(0))
            sddbl = sd.ToOADate
            If Val(txtEndDate.Text) = 0 Or Trim(txtEndDate.Text) = "" Then
                EDdbl = 0

            Else
                s1 = Split(txtEndDate.Text, "/")
                ED = DateSerial(s1(2), s1(1), s1(0))
                EDdbl = ED.ToOADate
            End If

        Catch ex As Exception
            lblMsgBox.Text = "Please check date!"
            Exit Sub
        End Try

        Try
            Dim n As Integer = Val(Mid(txtStartTime.Text, 1, 2))
            If n > 23 Then
                lblMsgBox.Text = "Please check time!"
                Exit Sub
            End If
            n = Val(Mid(txtStartTime.Text, 3, 4))
            If n > 59 Then
                lblMsgBox.Text = "Please check time!"
                Exit Sub
            End If

            n = Val(Mid(txtEndTime.Text, 1, 2))
            If n > 23 Then
                lblMsgBox.Text = "Please check time!"
                Exit Sub
            End If
            n = Val(Mid(txtEndTime.Text, 3, 4))
            If n > 59 Then
                lblMsgBox.Text = "Please check time!"
                Exit Sub
            End If
        Catch ex As Exception
        End Try


        If Val(txtTansID.Text) = 0 Then
            If (Val(lblTotOPQTY.Text) + Val(txtOPQTY.Text)) > Val(txtReleasedqty.Text) Then
                lblMsgBox.Text = "Please check received quantity!"
                Exit Sub
            End If
        Else
            If ((Val(lblTotOPQTY.Text) - Val(txtARCQTY.Text)) + Val(txtOPQTY.Text)) > Val(txtReleasedqty.Text) Then


                lblMsgBox.Text = "Please check received quantity!"
                Exit Sub
            End If
        End If

        Try
            'If Val(txtTansID.Text) = 0 Then
            '    If (Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text)) < Val(txtReleasedqty.Text) Then
            '        txtsuf.Text = GetMaxSufID()
            '        If txtsuf.Text = "" Then
            '            txtsuf.Text = "-"
            '            MessageBox.Show("Can not Split this job!", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            '            lblMsgBox.Text = "Can not Split this job!"
            '            Exit Sub
            '        End If
            '    Else

            '        txtsuf.Text = "-"
            '    End If
            'End If



            If Val(txtTansID.Text) = 0 Then
                If txtOPQTY.Text = Val(txtReleasedqty.Text) Then
                    txtsuf.Text = "-"
                Else
                    txtsuf.Text = GetMaxSufID()
                    If txtsuf.Text = "" Then
                        txtsuf.Text = "-"
                        MessageBox.Show("Can not Split this job!", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        lblMsgBox.Text = "Can not Split this job!"
                        Exit Sub
                    End If
                End If
            Else

                'If Val(txtRejectedQty.Text) <> 0 Or Val(txtCompletedQty.Text) <> 0 Then
                '    If (Val(txtRejectedQty.Text) + Val(txtCompletedQty.Text)) < Val(txtReleasedqty.Text) Then
                '        txtsuf.Text = GetMaxSufID()
                '        If txtsuf.Text = "" Then
                '            txtsuf.Text = "-"
                '            MessageBox.Show("Can not Split this job!", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                '            lblMsgBox.Text = "Can not Split this job!"
                '            Exit Sub
                '        End If
                '    Else
                '        txtsuf.Text = GetMaxSufID()
                '        If txtsuf.Text = "" Then
                '            txtsuf.Text = "-"
                '            MessageBox.Show("Can not Split this job!", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                '            lblMsgBox.Text = "Can not Split this job!"
                '            Exit Sub
                '        End If
                '        'txtsuf.Text = "-"
                '    End If
                'Else
                txtsuf.Text = "-"
                If Val(txtARCQTY.Text) <> Val(txtOPQTY.Text) Then
                    txtsuf.Text = GetMaxSufID()
                    If txtsuf.Text = "" Then
                        txtsuf.Text = "-"
                        MessageBox.Show("Can not Split this job!", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        lblMsgBox.Text = "Can not Split this job!"
                        Exit Sub
                    End If
                End If





                'End If
            End If


            With com


                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "sp_Job_Add"
                parm = .Parameters.Add("@job", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtWKID.Text)
                parm = .Parameters.Add("@jobsuffix", SqlDbType.VarChar, 50)
                parm.Value = txtsuf.Text
                parm = .Parameters.Add("@jobsuffixParent", SqlDbType.VarChar, 50)
                parm.Value = txtSufParent.Text

                parm = .Parameters.Add("@wc", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtWKSatationID.Text)
                parm = .Parameters.Add("@oper_num", SqlDbType.Int)
                parm.Value = Val(Trim(txtOperationNo.Text))
                parm = .Parameters.Add("@item", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtItemNo.Text)
                parm = .Parameters.Add("@JobDate", SqlDbType.Float)
                parm.Value = 0
                parm = .Parameters.Add("@trans_type", SqlDbType.Char, 10)
                parm.Value = ""
                parm = .Parameters.Add("@trans_datefrm", SqlDbType.Float)
                parm.Value = sddbl
                parm = .Parameters.Add("@trans_dateto", SqlDbType.Float)
                parm.Value = EDdbl
                parm = .Parameters.Add("@trans_Timefrm", SqlDbType.Int)
                parm.Value = Val(txtStartTime.Text)
                parm = .Parameters.Add("@trans_TimeTo", SqlDbType.Int)
                parm.Value = Val(txtEndTime.Text)

                parm = .Parameters.Add("@qty_Rele_qty", SqlDbType.Decimal)
                parm.Value = Val(txtReleasedqty.Text)
                parm = .Parameters.Add("@qty_op_qty", SqlDbType.Decimal)
                parm.Value = Val(txtOPQTY.Text)
                parm = .Parameters.Add("@qty_complete", SqlDbType.Decimal)
                parm.Value = Val(txtCompletedQty.Text)
                parm = .Parameters.Add("@qty_scrapped", SqlDbType.Decimal)
                parm.Value = Val(txtRejectedQty.Text)
                parm = .Parameters.Add("@no_oper", SqlDbType.Int)
                parm.Value = Val(txtOperators.Text)
                parm = .Parameters.Add("@CreatedBy", SqlDbType.VarChar, 255)
                parm.Value = txtEmpName.Text

                parm = .Parameters.Add("@rejectedCode1", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtRejCode1.Text)
                parm = .Parameters.Add("@rejectedDesc1", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtRejDes1.Text)
                parm = .Parameters.Add("@rejectedqty1", SqlDbType.Decimal)
                parm.Value = Val(Trim(txtRejqty1.Text))

                parm = .Parameters.Add("@rejectedCode2", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtRejCode2.Text)
                parm = .Parameters.Add("@rejectedDesc2", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtRejDes2.Text)
                parm = .Parameters.Add("@rejectedqty2", SqlDbType.Decimal)
                parm.Value = Val(Trim(txtRejqty2.Text))

                parm = .Parameters.Add("@rejectedCode3", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtRejCode3.Text)
                parm = .Parameters.Add("@rejectedDesc3", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtRejDes3.Text)
                parm = .Parameters.Add("@rejectedqty3", SqlDbType.Decimal)
                parm.Value = Val(Trim(txtRejqty3.Text))
                parm = .Parameters.Add("@emp_num", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtEmpid.Text)
                parm = .Parameters.Add("@emp_name", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtEmpName.Text)
                parm = .Parameters.Add("@tansnum", SqlDbType.Int)
                parm.Value = Val(txtTansID.Text)
                parm = .Parameters.Add("@nextopno", SqlDbType.Int)
                parm.Value = Val(0)
                parm = .Parameters.Add("@machineid", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtMachineid.Text)
                parm = .Parameters.Add("@Handoverid", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtHandoverid.Text)
                parm = .Parameters.Add("@HandoverName", SqlDbType.VarChar, 255)
                parm.Value = Trim(txtHandoverName.Text)





                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                'txtWKID.Clear()
                txtEmpName.Clear()
                txtEmpid.Text = ""
                clearlts()
                '   txtWKID.Select()
                list_Displaydata()
                lblMsgBox.Text = "Successfully updated!"
            End With

        Catch ex As Exception
            lblMsgBox.Text = (ex.Message)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        CheckTansdata()

    End Sub
    Private Sub dtpStart_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpStart.ValueChanged
        txtStartDate.Text = Format(dtpStart.Value, "dd/MM/yyyy")
        dtpStart.Checked = False
    End Sub

    Private Sub dtpEnd_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpEnd.ValueChanged
        txtEndDate.Text = Format(dtpEnd.Value, "dd/MM/yyyy")
        dtpEnd.Checked = False
    End Sub
    Private Sub txtOperationNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtOperationNo.KeyPress
        If (Asc(e.KeyChar) > 47 And Asc(e.KeyChar) < 58) Or Asc(e.KeyChar) = 8 Or Asc(e.KeyChar) = 13 Then
            e.Handled = False
            If Asc(e.KeyChar) = 13 Then
                lblSplit.Visible = False
                clearlts()
                txtReleasedqty.Text = RelQtyAll
                If txtWKID_KeyPress() = False Then
                    Exit Sub
                End If
                Dim St As Boolean
                St = CheckTansdata()
                If GetAllData() = False Then
                    If St = False Then
                        txtOperationNo.Focus()
                    Else
                        txtSufParent.Focus()
                    End If
                    list_Displaydata()
                    Exit Sub

                End If
                If St = False Then
                    txtMachineid.Focus()
                Else
                    txtSufParent.Focus()
                End If
                list_Displaydata()
            End If





        Else
            e.Handled = True
        End If











    End Sub


    Private Sub txtWKSatationID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtWKSatationID.KeyPress
        If Asc(e.KeyChar) = 13 Then
            'If Trim(txtWKID.Text) <> "" Then
            '    ' wstClear()
            '    Try
            '        With com
            '            .Connection = cn
            '            .CommandType = CommandType.Text
            '            .CommandText = "select * from tbJobRoute where _job='" & fncstr(txtWKID.Text) & "' and _operationNo=" & Val(fncstr(txtOperationNo.Text))
            '            cn.Open()
            '            dr = .ExecuteReader()
            '            If dr.Read Then
            '                txtJobNo.Text = dr.Item("_job")
            '                txtWKSatationID.Text = dr.Item("_wc")
            '                txtItemNo.Text = txtItemNoTemp.Text
            '                'txtOperators.Select()
            '                cn.Close()
            '                list_Displaydata()
            '            End If
            '        End With


            '        Dim strsql As String
            '        Dim booOP As Boolean = False

            '        strsql = "select min(_operationNo) as _operationNo  from tbJobRoute where _job='" & fncstr(txtWKID.Text) & "'"
            '        Dim ds As New DataSet
            '        Dim da As New SqlDataAdapter
            '        da = New SqlDataAdapter(strsql, cn)
            '        da.Fill(ds)
            '        da.Dispose()
            '        If ds.Tables(0).Rows.Count > 0 Then
            '            If ds.Tables(0).Rows(0).Item("_operationNo") = Val(txtOperationNo.Text) Then
            '                booOP = True
            '            Else
            '                txtReleasedqty.Text = ""
            '                getReasedQty()


            '            End If

            '        Else
            '            lblMsgBox.Text = "Please scan SCAN (JOB NO.)!"
            '        End If







            '    Catch ex As Exception
            '        lblMsgBox.Text = (ex.Message)
            '    Finally
            '        cn.Close()
            '    End Try

            'Else
            '    lblMsgBox.Text = "Please scan SCAN (JOB NO.)!"
            'End If
            GetAllData()
        End If
    End Sub


    Sub getReasedQty()
        Dim strsql As String
        Dim booOP As Boolean = False

        strsql = "select sum(_qty_complete) as _qty_complete from tbJobtrans where _job='" & fncstr(txtWKID.Text) & "' and _nextoper_num=" & Val(fncstr(txtOperationNo.Text))
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        da = New SqlDataAdapter(strsql, cn)
        da.Fill(ds)
        da.Dispose()
        If ds.Tables(0).Rows(0).IsNull(0) = False Then
            txtReleasedqty.Text = ds.Tables(0).Rows(0).Item("_qty_complete")
        Else
            txtReleasedqty.Text = ""
        End If
    End Sub

    Private Function txtWKID_KeyPress() As Boolean
        txtWKID_KeyPress = False
        lblMsgBox.Text = ""

        ' ClearAll()
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select * from tbJob where _job='" & fncstr(txtWKID.Text) & "'"
                cn.Open()
                dr = .ExecuteReader()
                If dr.Read Then
                    'txtReleasedqty.Text = dr.Item("_qtyReleased")
                    txtItemNoTemp.Text = dr.Item("_item")
                    txtWKID_KeyPress = True
                    ' txtWKSatationID.Select()
                Else
                    txtReleasedqty.Text = ""
                    txtItemNoTemp.Text = ""
                    lblMsgBox.Text = "Please check your JOB NO.!(" & txtWKID.Text & ")"
                End If
            End With
        Catch ex As Exception
            txtWKID_KeyPress = False
            lblMsgBox.Text = ex.Message
        Finally
            cn.Close()
        End Try
        Return txtWKID_KeyPress

    End Function

    Private Sub txtEmpid_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEmpid.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If checkUserID() = True Then
                CheckTansdata()
                list_Displaydata()
                txtWKID.Focus()
            Else
                txtWKID.Focus()
            End If
        End If
    End Sub

    Private Sub txtOPQTY_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtOPQTY.KeyPress
        If (Asc(e.KeyChar) > 47 And Asc(e.KeyChar) < 58) Or Asc(e.KeyChar) = 8 Or Asc(e.KeyChar) = 13 Or Asc(e.KeyChar) = 46 Then
            e.Handled = False
            If Asc(e.KeyChar) = 13 Then

                txtCompletedQty.Focus()
            End If
        Else
            e.Handled = True
        End If
    End Sub





    Private Sub txtNextOPNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If (Asc(e.KeyChar) > 47 And Asc(e.KeyChar) < 58) Or Asc(e.KeyChar) = 8 Or Asc(e.KeyChar) = 13 Then
            e.Handled = False
            If Asc(e.KeyChar) = 13 Then
                txtEmpid.Focus()
            End If
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub lstv_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.DoubleClick
        Try
            ' Vdd = cmbsuf.SelectedIndex
            ButHand.Visible = False
            txtEndDate.Text = ""
            txtEndTime.Text = ""
            txtStartDate.Text = ""
            txtStartTime.Text = ""
            txtOperators.Text = ""
            txtSufParent.Text = ""
            txtEmpid.Text = ""
            txtOPQTY.Text = ""
            'txtNextOPNo.Text = ""
            txtCompletedQty.Text = ""
            txtRejectedQty.Text = ""
            txtRejCode1.Text = ""
            txtRejqty1.Text = ""
            txtRejDes1.Text = ""
            txtRejCode2.Text = ""
            txtRejqty2.Text = ""
            txtRejDes2.Text = ""
            txtRejCode3.Text = ""
            txtRejqty3.Text = ""
            txtRejDes3.Text = ""
            txtMachineid.Text = ""
            txtTansID.Text = 0
            txtsuf.Text = "-"
            'cmbsuf.SelectedIndex = 0
            txtARCQTY.Text = 0
          
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select * from tbJobtrans where _tansnum=" & Val(lstv.SelectedItems(0).Text)
                cn.Open()
                dr = .ExecuteReader()
                If dr.Read Then
                    txtTansID.Text = dr.Item("_tansnum")
                    If dr.Item("_trans_datefrm") <> 0 Then
                        txtStartDate.Text = Format(DateTime.FromOADate(dr.Item("_trans_datefrm")), "dd/MM/yyyy")
                        txtStartTime.Text = dr.Item("_start_time")
                    End If
                    If dr.Item("_trans_dateto") <> 0 Then
                        txtEndDate.Text = Format(DateTime.FromOADate(dr.Item("_trans_dateto")), "dd/MM/yyyy")
                        txtEndTime.Text = dr.Item("_end_time")
                    End If
                    txtARCQTY.Text = dr.Item("_qty_op_qty")
                    txtOPQTY.Text = dr.Item("_qty_op_qty")
                    txtOperators.Text = dr.Item("_no_oper")
                    txtEmpid.Text = dr.Item("_emp_num")
                    txtEmpName.Text = dr.Item("_emp_name")
                    'txtNextOPNo.Text = dr.Item("_nextoper_num")
                    txtCompletedQty.Text = dr.Item("_qty_complete")
                    txtRejectedQty.Text = dr.Item("_qty_scrapped")
                    txtMachineid.Text = dr.Item("_machineid")
                    txtRejCode1.Text = dr.Item("_rejectedCode1")
                    txtRejDes1.Text = dr.Item("_rejectedDesc1")
                    txtRejqty1.Text = dr.Item("_rejectedqty1")
                    txtRejCode2.Text = dr.Item("_rejectedCode2")
                    txtRejDes2.Text = dr.Item("_rejectedDesc2")
                    txtRejqty2.Text = dr.Item("_rejectedqty2")
                    txtRejCode3.Text = dr.Item("_rejectedCode3")
                    txtRejDes3.Text = dr.Item("_rejectedDesc3")
                    txtRejqty3.Text = dr.Item("_rejectedqty3")
                    txtsuf.Text = dr.Item("_jobsuffix")
                    txtSufParent.Text = dr.Item("_jobsuffixParent")
                    If dr.Item("_trans_dateto") <> 0 Then
                        ButHand.Visible = False
                    Else
                        ButHand.Visible = True
                    End If
                    txtReleasedqty.Text = dr.Item("_qty_Rele_qty")
                    'cmbsuf.Text = dr.Item("_jobsuffix")
                    txtHandoverid.Text = ""
                    txtHandoverName.Text = ""
                    'If Val(txtCompletedQty.Text) > 0 Then
                    '    plHand.Visible = False
                    'Else
                    '    plHand.Visible = True
                    'End If
                Else
                    lblMsgBox.Text = "Please check your JOB NO.!(" & txtWKID.Text & ")"
                End If
            End With
        Catch ex As Exception
            lblMsgBox.Text = ex.Message
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub txtWKID_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWKID.Leave
        If orid <> txtWKID.Text Then
            lblMsgBox.Text = ""
            cn.Close()
            selData()
        End If
        orid = ""
    End Sub
    Private Sub butNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butNext.Click
        clearlts()
        lblMsgBox.Text = ""
        Try
            Dim strsql As String
            Dim booOP As Boolean = False

            strsql = "select min(_operationNo) as _operationNo  from tbJobRoute where _job='" & fncstr(txtWKID.Text) & "' and _operationNo>" & Val(txtOperationNo.Text)
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            da = New SqlDataAdapter(strsql, cn)
            da.Fill(ds)
            da.Dispose()
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0).IsNull(0) = False Then
                    txtOperationNo.Text = ds.Tables(0).Rows(0).Item("_operationNo")
                    txtReleasedqty.Text = RelQtyAll

                    ' txtWKID_KeyPress()
                    ' Dim sender1 As Object
                    ' Dim e1 As System.Windows.Forms.KeyPressEventArgs
                    ' txtWKSatationID_KeyPress(sender1, e1)
                    GetAllData()
                    'getReasedQty()
                Else
                    lblMsgBox.Text = "There is no next operation"
                End If
            Else
                lblMsgBox.Text = "There is no next operation"
            End If



        Catch ex As Exception
            lblMsgBox.Text = (ex.Message)
        Finally
            cn.Close()
        End Try
        'End If
    End Sub

    Private Function GetAllData() As Boolean

        If Trim(txtWKID.Text) <> "" Then
            ' wstClear()
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "select A.*,B._description as _desc from tbJobRoute A,tbWC B where A._wc=B._WC and A._job='" & fncstr(txtWKID.Text) & "' and _operationNo=" & Val(fncstr(txtOperationNo.Text))
                    cn.Open()
                    dr = .ExecuteReader()
                    If dr.Read Then
                        '  txtJobNo.Text = dr.Item("_job")
                        txtWKSatationID.Text = dr.Item("_wc")
                        txtWKSatation.Text = dr.Item("_desc")
                        'txtItemNo.Text = txtItemNoTemp.Text
                        'txtOperators.Select()
                        GetAllData = True
                        cn.Close()
                        list_Displaydata()
                    Else
                        cn.Close()
                        txtWKSatationID.Text = ""
                        txtWKSatation.Text = ""
                        list_Displaydata()
                        GetAllData = False
                    End If


                End With



                




            Catch ex As Exception
                lblMsgBox.Text = (ex.Message)
            Finally
                cn.Close()
            End Try

        Else
            lblMsgBox.Text = "Please scan SCAN (JOB NO.)!"
        End If
        Return GetAllData
    End Function

    Private Sub butPre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPre.Click
        clearlts()
        lblMsgBox.Text = ""
        Try
            Dim strsql As String
            Dim booOP As Boolean = False

            strsql = "select max(_operationNo) as _operationNo  from tbJobRoute where _job='" & fncstr(txtWKID.Text) & "' and _operationNo<" & Val(txtOperationNo.Text)
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            da = New SqlDataAdapter(strsql, cn)
            da.Fill(ds)
            da.Dispose()
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0).IsNull(0) = False Then
                    txtOperationNo.Text = ds.Tables(0).Rows(0).Item("_operationNo")
                    txtReleasedqty.Text = RelQtyAll

                    ' txtWKID_KeyPress()
                    ' Dim sender1 As Object
                    ' Dim e1 As System.Windows.Forms.KeyPressEventArgs
                    ' txtWKSatationID_KeyPress(sender1, e1)
                    GetAllData()
                    'getReasedQty()
                Else
                    lblMsgBox.Text = "There is no next operation"
                End If
            Else
                lblMsgBox.Text = "There is no next operation"
            End If



        Catch ex As Exception
            lblMsgBox.Text = (ex.Message)
        Finally
            cn.Close()
        End Try
    End Sub

    Sub clearlts()
        txtSubval.Text = ""
        txtSufParent.Text = "-"
        txtSpStatus.Text = "True"
        txtTansID.Text = 0
        txtEndDate.Text = ""
        txtEndTime.Text = ""
        txtStartDate.Text = ""
        txtStartTime.Text = ""
        txtOperators.Text = ""

        txtOPQTY.Text = ""
        'txtNextOPNo.Text = ""
        txtCompletedQty.Text = ""
        txtRejectedQty.Text = ""
        txtRejCode1.Text = ""
        txtRejqty1.Text = ""
        txtRejDes1.Text = ""
        txtRejCode2.Text = ""
        txtRejqty2.Text = ""
        txtRejDes2.Text = ""
        txtRejCode3.Text = ""
        txtRejqty3.Text = ""
        txtRejDes3.Text = ""
        txtTansID.Text = 0
        'cmbsuf.SelectedIndex = Vdd
        txtMachineid.Text = ""
        txtHandoverid.Text = ""
        txtHandoverName.Text = ""
        ButHand.Visible = False
        txtARCQTY.Text = 0
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        txtEmpName.Clear()
        txtEmpid.Text = ""
        clearlts()
    End Sub

    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub

    Private Sub txtEmpid_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpid.Leave

        If checkUserID() = True Then
            list_Displaydata()
            txtWKID.Focus()
        Else
            txtWKID.Focus()
        End If

    End Sub

    Private Sub txtEmpid_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEmpid.TextChanged
        'txtEmpid.Text = ""
        'txtEmpName.Text = ""
        'Try
        '    With com
        '        .Connection = cn
        '        .CommandType = CommandType.Text
        '        .CommandText = "SELECT     * FROM  tbOperator where _operatorID='" & fncstr(txtEmpid.Text) & "'"
        '        cn.Open()
        '        dr = .ExecuteReader()
        '        If dr.Read Then
        '            txtEmpid.Text = dr.Item("_operatorID")
        '            txtEmpName.Text = dr.Item("_operatorName")
        '        Else
        '            lblMsgBox.Text = "Please check your operator code(" & txtEmpid.Text & " )!"
        '            txtEmpid.Text = ""
        '            txtEmpName.Text = ""

        '        End If
        '    End With
        'Catch ex As Exception
        '    lblMsgBox.Text = ex.Message
        'Finally
        '    cn.Close()
        'End Try
    End Sub

    Function checkUserID() As Boolean
        lblMsgBox.Text = ""
        txtEmpName.Text = ""
        checkUserID = False
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "SELECT     * FROM  tbOperator where _operatorID='" & fncstr(txtEmpid.Text) & "'"
                cn.Open()
                dr = .ExecuteReader()
                If dr.Read Then
                    txtEmpid.Text = dr.Item("_operatorID")
                    txtEmpName.Text = dr.Item("_operatorName")
                    checkUserID = True
                Else
                    lblMsgBox.Text = "Please check your operator code(" & txtEmpid.Text & " )!"
                    txtEmpid.Text = ""
                    txtEmpName.Text = ""
                End If
            End With
        Catch ex As Exception
            lblMsgBox.Text = ex.Message
            txtEmpid.Text = ""
            txtEmpName.Text = ""
        Finally
            cn.Close()
        End Try
        Return checkUserID
    End Function

    Private Sub butExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExit.Click
        Me.Close()
    End Sub
    Private Function MaxOPNO() As Boolean
        MaxOPNO = False
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select max(_operationNo) as _operationNo from tbJobroute where _job='" & fncstr(txtWKID.Text) & "'"
                cn.Open()
                dr = .ExecuteReader()
                If dr.Read Then
                    If txtOperationNo.Text = dr.Item("_operationNo") Then
                        ' txtNextOPNo.Text = 0
                        MaxOPNO = True
                    End If
                End If
            End With

        Catch ex As Exception
            lblMsgBox.Text = (ex.Message)
        Finally
            cn.Close()
            txtC.Focus()
        End Try

    End Function

    Private Sub txtItemNo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtItemNo.GotFocus
        txtOperationNo.Focus()
    End Sub

   

   
    Private Sub txtOperationNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOperationNo.TextChanged

    End Sub

    Private Sub txtMachineid_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMachineid.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Dim idm As String = txtMachineid.Text
            If CheckMachineID() = True Then
                list_Displaydata()
                txtOperators.Focus()
            Else
                lblMsgBox.Text = "Please check machine id(" & idm & ")"
                txtMachineid.Text = ""
            End If
        End If
    End Sub

    Private Sub txtMachineid_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMachineid.Leave
        Dim idm As String = txtMachineid.Text
        If CheckMachineID() = True Then
            list_Displaydata()
        Else
            lblMsgBox.Text = "Please check machine id(" & idm & ")"
            txtMachineid.Text = ""
        End If
        'MsgBox("Hi")
    End Sub

    Private Sub txtMachineid_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMachineid.TextChanged

    End Sub




    Private Function CheckMachineID() As Boolean
        CheckMachineID = False
        Try
            With com
                Dim Ds As New DataSet
                Dim AD As New SqlDataAdapter
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select * from tbMachineGroup A,tbMachine B where  B._mid=A._mid and _wc='" & fncstr(txtWKSatationID.Text) & "'" ' and _mid=(select _mid from tbMachine where _machineID='" & fncstr(txtMachineid.Text) & "')"
                AD.SelectCommand = com
                AD.Fill(Ds, "tbMachineGroup")
                If Ds.Tables(0).Rows.Count > 0 Then
                    Dim NDR As DataRow() = Ds.Tables(0).Select("_machineID='" & txtMachineid.Text & "'")
                    If NDR.Length > 0 Then
                        CheckMachineID = True
                    Else
                        txtMachineid.Text = ""
                        CheckMachineID = False
                    End If
                Else
                    txtMachineid.Text = "-"
                    CheckMachineID = True
                End If
                ' Dim sql As String
                ' sql = "select * from tbMachineGroup where  _wc='" & fncstr(txtWKSatationID.Text) & "' and _mid=(select _mid from tbMachine where _machineID='" & fncstr(txtMachineid.Text) & "')"
                ' cn.Open()
                ' dr = .ExecuteReader()
                'If dr.Read Then
                '    CheckMachineID = True
                'End If
            End With

        Catch ex As Exception
            lblMsgBox.Text = (ex.Message)
        Finally
            cn.Close()
            txtC.Focus()
        End Try
        Return CheckMachineID
    End Function
    'Sub LaodFix()
    '    cmbsuf.Items.Clear()
    '    Try
    '        With com
    '            .Connection = cn
    '            .CommandType = CommandType.Text
    '            'Dim DI As DataItem
    '            .CommandText = "select * from tbSuffix order by _sidNo"
    '            cn.Open()
    '            dr = .ExecuteReader(CommandBehavior.CloseConnection)
    '            While dr.Read
    '                cmbsuf.Items.Add(New DataItem(dr.Item("_sidNo"), dr.Item("_sno")))
    '                ' lstwrk.Items.Add(dr.Item("_wc") & " - " & dr.Item("_description"))
    '            End While
    '            cn.Close()
    '        End With
    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
    '    Finally
    '        cn.Close()
    '    End Try
    'End Sub

    Private Sub txtWKID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtWKID.TextChanged

    End Sub

    Private Sub txtHandoverid_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtHandoverid.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Dim ACV As String = txtHandoverid.Text
            If txtEmpid.Text <> txtHandoverid.Text And txtEmpid.Text <> "" Then
                If Check_opIDhand(ACV) = True Then
                    ' plHand.Visible = True
                    butSave.Focus()
                Else
                    lblMsgBox.Text = "Please check Handover id(" & ACV & ")"
                    txtC.Focus()
                    Exit Sub

                End If
            Else
                lblMsgBox.Text = "Please check Handover id(" & ACV & ")"
                txtC.Focus()
                Exit Sub
            End If
        End If

    End Sub
    Function Check_opIDhand(ByVal idno As String) As Boolean
        Check_opIDhand = False
        txtHandoverid.Text = ""
        txtHandoverName.Text = ""
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "SELECT     * FROM  tbOperator where _operatorID='" & fncstr(idno) & "'"
                cn.Open()
                dr = .ExecuteReader()
                If dr.Read Then
                    txtHandoverid.Text = dr.Item("_operatorID")
                    txtHandoverName.Text = dr.Item("_operatorName")
                    Check_opIDhand = True
                Else
                    lblMsgBox.Text = "Please check your operator code(" & idno & " )!"
                    txtHandoverid.Text = ""
                    txtHandoverName.Text = ""
                    Check_opIDhand = False
                End If
            End With
        Catch ex As Exception
            lblMsgBox.Text = ex.Message
            Check_opIDhand = False
            txtHandoverid.Text = ""
            txtHandoverName.Text = ""
        Finally
            cn.Close()
        End Try

        'If Check_opID = False Then
        '    lblMsgBox.Text = "Please check your operator code(" & idno & " )!"
        'End If
        Return Check_opIDhand

    End Function

    Private Sub butUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdate.Click

    End Sub

    Private Sub txtOPQTY_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOPQTY.TextChanged

    End Sub
    Sub selreciedQty()

        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                Dim sql As String = "select Max(_oper_num),_qty_complete from tbJobTrans where _job='" & Trim(txtWKID.Text) & "' and _jobsuffix='" & (txtSufParent.Text) & "' and _oper_num<" & Val(txtOperationNo.Text) & " group by _oper_num,_qty_complete"
                .CommandText = "select Max(_oper_num),_qty_complete from tbJobTrans where _job='" & Trim(txtWKID.Text) & "' and _jobsuffix='" & (txtSufParent.Text) & "' and _oper_num<" & Val(txtOperationNo.Text) & " group by _oper_num,_qty_complete"
                cn.Open()
                dr = .ExecuteReader()
                If dr.Read Then
                    txtReleasedqty.Text = dr.Item("_qty_complete")
                    cn.Close()
                    If txtMachineid.Text = "" Then
                        ' txtMachineid.Select()
                        txtMachineid.Focus()
                    Else
                        ' txtOperators.Select()
                        txtOperators.Focus()
                    End If
                  
                Else
                    txtReleasedqty.Text = 0
                    cn.Close()
                    If txtMachineid.Text = "" Then
                        ' txtOperators.Select()
                        txtOperators.Focus()
                    Else
                        ' txtOperators.Select()
                        txtOperators.Focus()
                    End If
                    'Else
                    '    lblMsgBox.Text = "Please check your JOB NO.!(" & txtWKID.Text & ")"
                    '    txtWKID.Clear()
                    '    txtWKID.Select()
                End If
            End With

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            txtWKID.Select()
        Finally
            cn.Close()

        End Try
    End Sub

    Function CheckTansdata() As Boolean
        txtSubval.Text = ""
        txtsuf.Text = "-"
        CheckTansdata = False
        lblMsgBox.Text = ""
        If Trim(txtWKID.Text) <> "" And Trim(txtOperationNo.Text) <> "" Then
            Dim DSRoute As New DataSet
            Dim DSTrans As New DataSet
            Dim DSLevel As New DataSet
            Dim AdLevel As New SqlDataAdapter
            Dim AdRoute As New SqlDataAdapter
            Dim AdTrans As New SqlDataAdapter
            'select * from tbJobRoute where _job='TH00081625'
            'select * from tbJobTrans where _job='TH00081625'
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "select * from tbLevel1 "
                    AdLevel.SelectCommand = com
                    AdLevel.Fill(DSLevel, "tbLevel")


                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "select * from tbJobRoute where _job='" & Trim(txtWKID.Text) & " '"
                    AdRoute.SelectCommand = com
                    AdRoute.Fill(DSRoute, "tbroute")


                    .CommandText = "select * from tbJobTrans where _job='" & Trim(txtWKID.Text) & "'"
                    AdTrans.SelectCommand = com
                    AdTrans.Fill(DSTrans, "AdTrans")
                    If Trim(txtSufParent.Text) = "" Then
                        txtSufParent.Text = "-"
                    End If
                    Dim DRCheckPerant() As DataRow = DSTrans.Tables(0).Select("_jobsuffixParent='" & Trim(txtSufParent.Text) & "' and _oper_num<" & Val(txtOperationNo.Text))
                    Dim DRCheckAfter() As DataRow = DSTrans.Tables(0).Select("_jobsuffixParent='" & Trim(txtSufParent.Text) & "' and _oper_num>" & Val(txtOperationNo.Text))

                    If DRCheckPerant.Length > 0 Then

                        Dim i As Integer
                        Dim Stu As Boolean = False
                        For i = 0 To DRCheckPerant.Length - 1
                            If DRCheckPerant(i).Item("_jobsuffix") <> "-" Then
                                Stu = True
                            End If
                        Next
                        If Stu = True Then
                            txtSufParent.Text = ""
                            txtReleasedqty.Text = 0
                            lblMsgBox.Text = "Please Check split id"
                            'txtSufParent.Select()
                            'txtSufParent.Focus()
                            lblSplit.Visible = False
                            Timer1.Start()
                            CheckTansdata = True
                        Else
                            Timer1.Stop()
                            lblSplit.Visible = False
                            CheckTansdata = False
                        End If





                    Else

                        Dim DRCheckChiled() As DataRow = DSTrans.Tables(0).Select("_jobsuffixParent='" & Trim(txtSufParent.Text) & "' and _oper_num=" & Val(txtOperationNo.Text))
                        If DRCheckChiled.Length > 0 Then
                            Dim i As Integer
                            For i = 0 To DRCheckChiled.Length - 1
                                If txtSubval.Text = "" Then
                                    txtSubval.Text = "'" & Trim(DRCheckChiled(i).Item("_jobsuffix")) & "'"
                                Else
                                    txtSubval.Text = txtSubval.Text & ",'" & Trim(DRCheckChiled(i).Item("_jobsuffix")) & "'"
                                End If
                            Next
                        End If
                        txtsuf.Text = GetMaxSufID()
                    End If




                        'Dim Newroute() As DataRow = DSRoute.Tables(0).Select("_operationNo<" & Val(txtOperationNo.Text), "_operationNo desc")
                        'Dim i As Integer = 0
                        'Dim stroute As Boolean = False

                        'Dim DRJobTansTot As DataRow()
                        'If txtSufParent.Text <> "" And txtSufParent.Text <> "-" Then
                        '    DRJobTansTot = DSTrans.Tables(0).Select("_oper_num<" & Val(txtOperationNo.Text) & " and _jobsuffix='" & txtSufParent.Text & "'")
                        'Else
                        '    DRJobTansTot = DSTrans.Tables(0).Select("_oper_num<" & Val(txtOperationNo.Text) & " and _jobsuffix='-'")
                        'End If

                        'If Newroute.Length > 0 Then
                        '    If DRJobTansTot.Length > 0 Then
                        '        Dim RDSuf As DataRow()


                        '        If txtSufParent.Text <> "" And txtSufParent.Text <> "-" Then
                        '            RDSuf = DSTrans.Tables(0).Select("_oper_num<" & Val(txtOperationNo.Text) & " and _jobsuffixParent='" & txtSufParent.Text & "'")
                        '        Else
                        '            RDSuf = DSTrans.Tables(0).Select("_oper_num<" & Val(txtOperationNo.Text) & " and _jobsuffixParent='-'")
                        '        End If

                        '        Dim TotCom As Double = 0
                        '        Dim opq As Integer = 0
                        '        For i = 0 To RDSuf.Length - 1
                        '            If RDSuf(i).Item("_jobsuffix") <> "-" Then
                        '                txtCompletedQty.Text = 0
                        '                txtSufParent.Text = ""
                        '                txtSufParent.Select()
                        '                txtSufParent.Focus()
                        '                Timer1.Start()
                        '                Exit Function
                        '            End If
                        '            If opq < RDSuf(i).Item("_oper_num") Then
                        '                opq = RDSuf(i).Item("_qty_complete")
                        '                txtReleasedqty.Text = opq
                        '                txtsuf.Text = GetMaxSufID()
                        '            End If
                        '        Next
                        '    Else
                        '        ' txtSufParent.Text = ""
                        '        ' txtSufParent.Select()
                        '        ' txtSufParent.Focus()
                        '        ' Timer1.Start()

                        '        txtSufParent.Text = "-"
                        '        Dim NewTranst() As DataRow = DSTrans.Tables(0).Select("_oper_num=" & Val(txtOperationNo.Text) & " and _jobsuffixParent='-'")
                        '        Dim j As Integer = 0
                        '        If NewTranst.Length > 0 Then
                        '            Dim tempsub As Integer = Asc("-")
                        '            tempsub = tempsub - 1
                        '            For j = 0 To NewTranst.Length - 1
                        '                If txtSubval.Text = "" Then
                        '                    txtSubval.Text = "'" & Trim(NewTranst(j).Item("_jobsuffix")) & "'"
                        '                Else
                        '                    txtSubval.Text = txtSubval.Text & ",'" & Trim(NewTranst(j).Item("_jobsuffix")) & "'"
                        '                End If
                        '            Next
                        '            txtsuf.Text = GetMaxSufID()
                        '        Else
                        '            txtsuf.Text = "-"
                        '        End If






                        '    End If




                        'Else
                        '    txtSufParent.Text = "-"
                        '    Dim NewTranst() As DataRow = DSTrans.Tables(0).Select("_oper_num=" & Val(txtOperationNo.Text) & " and _jobsuffixParent='-'")
                        '    Dim j As Integer = 0
                        '    If NewTranst.Length > 0 Then
                        '        Dim tempsub As Integer = Asc("-")
                        '        tempsub = tempsub - 1
                        '        For j = 0 To NewTranst.Length - 1
                        '            If txtSubval.Text = "" Then
                        '                txtSubval.Text = "'" & Trim(NewTranst(j).Item("_jobsuffix")) & "'"
                        '            Else
                        '                txtSubval.Text = txtSubval.Text & ",'" & Trim(NewTranst(j).Item("_jobsuffix")) & "'"
                        '            End If
                        '        Next
                        '        txtsuf.Text = GetMaxSufID()
                        '    Else
                        '        txtsuf.Text = GetMaxSufID()
                        '    End If
                        'End If
                        'If stroute = False Then
                        '    Dim NewTranst() As DataRow = DSTrans.Tables(0).Select("_oper_num=" & Val(txtOperationNo.Text))
                        '    Dim j As Integer
                        '    If NewTranst.Length > 0 Then
                        '        For j = 0 To NewTranst.Length - 1



                        '        Next
                        '    End If
                        'End If
                End With
            Catch ex As Exception
                lblMsgBox.Text = ex.Message

            Finally
                cn.Close()
                com.Parameters.Clear()

            End Try
        End If
        Return CheckTansdata
    End Function



    Function GetMaxSufID() As String
        GetMaxSufID = "-"
        If txtSubval.Text = "" Then
            txtSubval.Text = "''"
        End If



        Dim DSTrans As New DataSet
  
        Dim AdTrans As New SqlDataAdapter
        'select * from tbJobRoute where _job='TH00081625'
        'select * from tbJobTrans where _job='TH00081625'
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text

                .CommandText = "select * from tbJobTrans where _job='" & Trim(txtWKID.Text) & "'"
                AdTrans.SelectCommand = com
                AdTrans.Fill(DSTrans, "AdTrans")

            End With


            Dim DRCheckChiled() As DataRow = DSTrans.Tables(0).Select("_jobsuffixParent='" & Trim(txtSufParent.Text) & "' and _oper_num=" & Val(txtOperationNo.Text))
            If DRCheckChiled.Length > 0 Then
                Dim i As Integer
                For i = 0 To DRCheckChiled.Length - 1
                    If txtSubval.Text = "" Then
                        txtSubval.Text = "'" & Trim(DRCheckChiled(i).Item("_jobsuffix")) & "'"
                    Else
                        txtSubval.Text = txtSubval.Text & ",'" & Trim(DRCheckChiled(i).Item("_jobsuffix")) & "'"
                    End If
                Next
            End If
            Dim DRCheckAfter() As DataRow = DSTrans.Tables(0).Select("_jobsuffixParent='" & Trim(txtSufParent.Text) & "' and _oper_num>" & Val(txtOperationNo.Text))
            If DRCheckAfter.Length > 0 Then
                GetMaxSufID = ""
                Return GetMaxSufID
                Exit Function
            End If
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                Dim sql As String = "select Min(_lel_1_Name) as _lel_1_Name from tbLevel1 where _lel_1_Parent='" & Trim(txtSufParent.Text) & "' and _lel_1_Name not in (" & txtSubval.Text & ")"
                .CommandText = "select Min(_lel_1_Name) as _lel_1_Name from tbLevel1 where _lel_1_Parent='" & Trim(txtSufParent.Text) & "' and _lel_1_Name not in (" & txtSubval.Text & ")"
                cn.Open()
                dr = .ExecuteReader
                If dr.Read Then
                    GetMaxSufID = dr.Item("_lel_1_Name")
                    cn.Close()
                End If
                cn.Close()
                Return GetMaxSufID
            End With

        Catch ex As Exception
        Finally
            cn.Close()
        End Try

    End Function



    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If txtSufParent.Text = "" Then
            If lblSplit.Visible = False Then
                lblSplit.Visible = True
            Else
                lblSplit.Visible = False
            End If

        End If
    End Sub

    Private Sub txtSufParent_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSufParent.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtOperators.Focus()

            'selreciedQty()
            'If txtSufParent.Text <> "" Then
            '    Timer1.Stop()
            '    lblSplit.Visible = False
            'End If
        End If
    End Sub

    Private Sub txtSufParent_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSufParent.Leave
        'If Asc(e.KeyChar) = 13 Then
        txtReleasedqty.Text = RelQtyAll
        selreciedQty()
        CheckTansdata()
        If txtSufParent.Text <> "" Then
            Timer1.Stop()
            lblSplit.Visible = False
        End If
        list_Displaydata()
        ' End If
    End Sub

    'Private Sub txtSufParent_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSufParent.TextChanged
    '    selreciedQty()
    '    If txtSufParent.Text <> "" Then
    '        Timer1.Stop()
    '        lblSplit.Visible = False
    '    End If
    'End Sub

    Private Sub txtSufParent_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSufParent.TextChanged

    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        If lblMsgBox.Text = "" Then
            Countdown = 0
        ElseIf lblMsgBox.Text <> "" Then
            Countdown += 1
            If lblMsgBox.Visible = True Then
                lblMsgBox.Visible = False
            Else
                lblMsgBox.Visible = True
            End If
        End If
        If Countdown = 10 Then
            lblMsgBox.Visible = True
            lblMsgBox.Text = ""
        End If
    End Sub

    Private Sub ButHand_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButHand.Click
        Dim objhand As New frmHandSup
        objhand.IDNo = Val(txtTansID.Text)
        objhand.lblJob.Text = txtWKID.Text
        objhand.lblMachine.Text = txtMachineid.Text
        objhand.lblSplit.Text = txtSufParent.Text
        objhand.lblOperation.Text = txtOperationNo.Text
        objhand.lblEMPID.Text = txtEmpid.Text
        ChHandOverID = ""
        ChHandOverName = ""
        objhand.ShowDialog()
        If ChHandOverID <> "" Then
            txtEmpid.Text = ChHandOverID
            txtEmpName.Text = ChHandOverName
        End If
    End Sub
End Class