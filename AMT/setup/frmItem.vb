Public Class frmItem

    Private Sub txtDesc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDesc.TextChanged

    End Sub

    Private Sub frmItem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
       

        Panel2.Focus()
        Panel3.Focus()

        With lstv
            .Columns.Add(New ColHeader("WCNo", 0, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("WC Code", 225, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Description", lstv.Width - 295, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Type", 50, HorizontalAlignment.Left, True))
            list_Displaydata()
        End With
      
    End Sub
    Sub list_Displaydata()
        lstv.Items.Clear()
        Try
            Dim clsM As New clsMain
            Dim ds As New DataSet
            ds = clsM.GetDataset("select *   from tbItem order by _itemCode", "tbWC")
            If IsDBNull(ds) = False Then

                Dim i As Integer
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            Dim ls As New ListViewItem(Trim(.Item("_itemid")))    ' you can also use reader.GetSqlValue(0) 
                            ls.SubItems.Add(Trim(.Item("_itemCode")))
                            ls.SubItems.Add(Trim(.Item("_itemdescription")))
                            ls.SubItems.Add(Trim(.Item("_type")))
                            lstv.Items.Add(ls)
                        End With

                    Next
                End If



            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click
        txtItemNo.Text = lstv.SelectedItems(0).Text
        txtItemCode.Text = lstv.SelectedItems(0).SubItems(1).Text
        txtDesc.Text = lstv.SelectedItems(0).SubItems(2).Text
        txtType.Text = lstv.SelectedItems(0).SubItems(3).Text
     
    End Sub

   
    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        txtItemNo.Text = ""
        txtItemCode.Text = ""
        txtDesc.Text = ""
    End Sub
    Private Sub lstv_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lstv.ColumnClick
        Dim clickedCol As ColHeader = CType(Me.lstv.Columns(e.Column), ColHeader)
        clickedCol.ascending = Not clickedCol.ascending
        Dim numItems As Integer = Me.lstv.Items.Count
        Me.lstv.BeginUpdate()
        Dim SortArray As New ArrayList
        Dim i As Integer
        For i = 0 To numItems - 1
            SortArray.Add(New SortWrapper(Me.lstv.Items(i), e.Column))
        Next i
        SortArray.Sort(0, SortArray.Count, New SortWrapper.SortComparer(clickedCol.ascending))
        Me.lstv.Items.Clear()
        Dim z As Integer
        For z = 0 To numItems - 1
            Me.lstv.Items.Add(CType(SortArray(z), SortWrapper).sortItem)
        Next z
        Me.lstv.EndUpdate()
    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub

  
End Class