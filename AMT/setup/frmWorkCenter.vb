Imports System.Data.SqlClient
Public Class frmWorkCenter
    Dim parm As SqlParameter
    Dim strFoCus As Integer
    Dim DSRGroup As New DataSet
    Private Sub frmWorkCenter_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtWCCode.TabIndex = 1


        Panel2.Focus()
        Panel3.Focus()
        txtWCCode.Focus()
        Dim clsM As New clsMain

        DSRGroup = clsM.GetDataset("select A.*,B._Wc from tbResourceGroup A,tbWorkStation B where A._rid=B._rid", "tbRGroup")

        With lstv
            .Columns.Add(New ColHeader("WCNo", 0, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("WC Code", 130, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Description", lstv.Width - 255, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Transfer", 0, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Labour", 50, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Run Rate", 100, HorizontalAlignment.Left, True))
            .Columns.Add(New ColHeader("Resource Group", 100, HorizontalAlignment.Left, True))


            list_Displaydata()
        End With
        txtWCCode.Text = 1
        txtWCCode.Focus()
        txtWCCode.Text = ""
    End Sub

    Sub list_Displaydata()
        lstv.Items.Clear()
        Try
            Dim clsM As New clsMain
            Dim ds As New DataSet
            ds = clsM.GetDataset("select *   from tbWC order by _description", "tbWC")
            If IsDBNull(ds) = False Then

                Dim rcount As Integer = 0
                Dim J As Integer = -1
                Dim i As Integer
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        With ds.Tables(0).Rows(i)
                            J = J + 1
                            Dim ls As New ListViewItem(Trim(.Item("_wcid")))    ' you can also use reader.GetSqlValue(0) 
                            ls.SubItems.Add(Trim(.Item("_wc")))
                            ls.SubItems.Add(Trim(.Item("_description")))
                            ls.SubItems.Add(Trim(.Item("_transfer")))
                            ls.SubItems.Add(Trim(.Item("_labour")))
                            ls.SubItems.Add(Trim(.Item("_Run_Rate")))
                            If DSRGroup.Tables(0).Rows.Count > 0 Then

                                Dim DRG As DataRow() = DSRGroup.Tables(0).Select("_Wc='" & .Item("_wc") & "'")
                                If DRG.Length > 0 Then
                                    ls.SubItems.Add(Trim(DRG(0).Item("_rGroupID")))
                                Else
                                    ls.SubItems.Add(Trim(""))
                                End If
                            Else
                                ls.SubItems.Add(Trim(""))
                            End If
                            lstv.Items.Add(ls)
                            If strFoCus = .Item("_wcid") Then
                                rcount = J
                            End If
                        End With

                    Next
                End If
                lstv.Focus()
                If J <> -1 Then
                    lstv.Items(rcount).Selected = True
                End If
                strFoCus = 0


            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click
        txtWCID.Text = lstv.SelectedItems(0).Text
        txtWCCode.Text = lstv.SelectedItems(0).SubItems(1).Text
        txtDescription.Text = lstv.SelectedItems(0).SubItems(2).Text
        If lstv.SelectedItems(0).SubItems(3).Text = "Y" Then
            ckbTransfer.Checked = True
        Else
            ckbTransfer.Checked = False
        End If
        If lstv.SelectedItems(0).SubItems(4).Text = "L" Then
            ckbLabour.Checked = True
        Else
            ckbLabour.Checked = False
        End If
        txtRunRate.Text = lstv.SelectedItems(0).SubItems(5).Text


        butSave.Enabled = False
        butUpdate.Enabled = True
        butDelete.Enabled = True
    End Sub
    Private Sub butUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdate.Click
        If Trim(txtWCID.Text) = "" Then
            MsgBox("Please select work center!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Try
            With com
                strFoCus = Val(txtWCID.Text)
                .Connection = cn
                .CommandType = CommandType.Text
                ' .CommandText = "Update  tbWC set _transfer=@transfer,_Labour=@Labour where _wcid=@wcid"
                .CommandText = "Update  tbWC set _transfer=@transfer where _wcid=@wcid"
                parm = .Parameters.Add("@transfer", SqlDbType.Char, 1)
                If ckbTransfer.Checked = True Then
                    parm.Value = "Y"
                Else
                    parm.Value = "N"
                End If

                '  parm = .Parameters.Add("@Labour", SqlDbType.Char, 1)
                ' If ckbLabour.Checked = True Then
                'parm.Value = "Y"
                'Else
                'parm.Value = "N"
                'End If

                parm = .Parameters.Add("@wcid", SqlDbType.Int)
                parm.Value = Val(txtWCID.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")
            End With
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub
    Sub textReset()
        txtWCCode.Text = ""
        txtWCID.Text = ""
        txtDescription.Text = ""
        txtRunRate.Text = ""
        ckbTransfer.Checked = False
        ckbLabour.Checked = False
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        textReset()
    End Sub
    Private Sub lstv_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lstv.ColumnClick


        Dim clickedCol As ColHeader = CType(Me.lstv.Columns(e.Column), ColHeader)


        clickedCol.ascending = Not clickedCol.ascending


        Dim numItems As Integer = Me.lstv.Items.Count


        Me.lstv.BeginUpdate()


        Dim SortArray As New ArrayList
        Dim i As Integer
        For i = 0 To numItems - 1
            SortArray.Add(New SortWrapper(Me.lstv.Items(i), e.Column))
        Next i


        SortArray.Sort(0, SortArray.Count, New SortWrapper.SortComparer(clickedCol.ascending))


        Me.lstv.Items.Clear()
        Dim z As Integer
        For z = 0 To numItems - 1
            Me.lstv.Items.Add(CType(SortArray(z), SortWrapper).sortItem)
        Next z

        Me.lstv.EndUpdate()


    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub

   
   
    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click

    End Sub

    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub
End Class