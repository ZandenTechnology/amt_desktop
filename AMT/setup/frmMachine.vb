Imports System.Data.SqlClient
Public Class frmMachine
    Dim dr As SqlDataReader
    Dim parm As SqlParameter
    Dim strFoCus As Integer
    Dim strclsH As String
    Private Sub frmMachine_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With lstv
            .Columns.Add(New ColHeader("MNo", 0, HorizontalAlignment.Left, Left))
            .Columns.Add(New ColHeader("Machine ID", 225, HorizontalAlignment.Left, Left))
            .Columns.Add(New ColHeader("Description", lstv.Width - 245, HorizontalAlignment.Left, Left))
            list_Displaydata()

        End With
    End Sub
    Sub list_Displaydata()
        lstv.Items.Clear()
        Try
            com.Connection = cn
            com.CommandType = CommandType.Text
            If strclsH = "" Then
                com.CommandText = "select _mid,_machineID,_MachineDesc from tbMachine order by _machineID"
            Else
                com.CommandText = "select _mid,_machineID,_MachineDesc from tbMachine " & strclsH
            End If
            'parm = com.Parameters.Add("@Type", SqlDbType.VarChar, 256)
            'parm.Value = refTxt(cmbType.Text)
            cn.Open()
            dr = com.ExecuteReader(CommandBehavior.CloseConnection)
            Dim rcount As Integer = 0
            Dim i As Integer = -1
            While dr.Read
                i = i + 1
                Dim ls As New ListViewItem(Trim(dr.Item("_mid")))    ' you can also use reader.GetSqlValue(0) 
                ls.SubItems.Add(Trim(dr.Item("_machineID")))
                ls.SubItems.Add(Trim(dr.Item("_MachineDesc")))
                lstv.Items.Add(ls)
                If strFoCus = dr.Item("_mid") Then
                    rcount = i
                End If
            End While
            If i <> -1 Then
                lstv.Focus()
                lstv.Items(rcount).Selected = True
            End If
            strFoCus = 0

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click
        txtMIDNo.Text = lstv.SelectedItems(0).Text
        txtMachineid.Text = lstv.SelectedItems(0).SubItems(1).Text
        txtDesc.Text = lstv.SelectedItems(0).SubItems(2).Text
        butSave.Enabled = False
        butUpdate.Enabled = True
        butDelete.Enabled = True
        ' txtUserCode.ReadOnly = True
        'StbUser.Text = "You Can Edit And Delete This Records"
    End Sub

   
    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(txtMachineid.Text) = "" Then
            MsgBox("Enter the machine id!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "insert into tbMachine( _machineID,_MachineDesc)values(@machineID,@MachineDesc)"
                parm = .Parameters.Add("@machineID", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtMachineid.Text)
                parm = .Parameters.Add("@MachineDesc", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtDesc.Text)
                'parm = .Parameters.Add("@Type", SqlDbType.VarChar, 256)
                'parm.Value = refTxt(cmbType.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully saved!", MsgBoxStyle.Information, "eWIP")
            End With
        Catch ex As SqlException
            If ex.Number = 2627 Then
                MsgBox("This machine id already exist", MsgBoxStyle.Critical, "eWIP")
                Exit Sub
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Exit Sub
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub
    Sub textReset()
        txtMIDNo.Clear()
        txtMachineid.Clear()
        txtDesc.Clear()
        butSave.Enabled = True
        butDelete.Enabled = False
        butUpdate.Enabled = False
    End Sub
    Function refTxt(ByVal txt As String) As String
        Return Trim(Replace(txt, "'", "''"))
    End Function

    Private Sub butUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdate.Click
        If Trim(txtMachineid.Text) = "" Then
            MsgBox("Enter the machine id!", MsgBoxStyle.Information, "eWIP")
            Exit Sub
        End If

        Try
            With com
                strFoCus = Val(txtMIDNo.Text)
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "Update  tbMachine set _machineID=@machineID,_MachineDesc=@MachineDesc where _mid =@mid"
                parm = .Parameters.Add("@machineID", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtMachineid.Text)
                parm = .Parameters.Add("@MachineDesc", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtDesc.Text)
                parm = .Parameters.Add("@mid ", SqlDbType.Int)
                parm.Value = Val(txtMIDNo.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")

            End With
        Catch ex As SqlException
            If ex.Number = 2627 Then
                MsgBox("This machine id already exist", MsgBoxStyle.Critical, "eWIP")
                Exit Sub
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Exit Sub
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        Dim result As New DialogResult
        result = MessageBox.Show("Do you Want to delete this Records?", "Caption", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = Windows.Forms.DialogResult.Yes Then
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "delete from   tbMachine  where _mid =@mid "
                    parm = .Parameters.Add("@mid ", SqlDbType.Int)
                    parm.Value = Val(txtMIDNo.Text)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()
                    textReset()
                    MsgBox("Successfully deleted!", MsgBoxStyle.Information, "eWIP")
                End With
            Catch ex As SqlException
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
                Exit Sub
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
                Exit Sub
            Finally
                cn.Close()
                com.Parameters.Clear()
            End Try
            list_Displaydata()
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        textReset()
    End Sub

    Private Sub lstv_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lstv.ColumnClick
        Dim clickedCol As ColHeader = CType(Me.lstv.Columns(e.Column), ColHeader)
        clickedCol.ascending = Not clickedCol.ascending
        Dim strby As String = ""
        If clickedCol.ascending = False Then
            strby = " DESC"
        End If
        '_mid,_machineID,_MachineDesc
        Select Case e.Column
            Case 0
                strclsH = " Order by _mid " & strby
            Case 1
                strclsH = " Order by _machineID " & strby
            Case 2
                strclsH = " Order by _MachineDesc " & strby
        End Select






        Dim numItems As Integer = Me.lstv.Items.Count
        Me.lstv.BeginUpdate()
        Dim SortArray As New ArrayList
        Dim i As Integer
        For i = 0 To numItems - 1
            SortArray.Add(New SortWrapper(Me.lstv.Items(i), e.Column))
        Next i
        SortArray.Sort(0, SortArray.Count, New SortWrapper.SortComparer(clickedCol.ascending))
        Me.lstv.Items.Clear()
        Dim z As Integer
        For z = 0 To numItems - 1
            Me.lstv.Items.Add(CType(SortArray(z), SortWrapper).sortItem)
        Next z
        Me.lstv.EndUpdate()
    End Sub

    Private Sub butEXIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEXIT.Click
        Me.Close()
    End Sub
End Class