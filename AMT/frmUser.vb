Imports System.Data.SqlClient
Public Class frmUser
    Inherits System.Windows.Forms.Form
    Dim parm As SqlParameter
    Dim dr As SqlDataReader
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butDelete As System.Windows.Forms.Button
    Friend WithEvents butUpdate As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents txtIDNo As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents txtUserCode As System.Windows.Forms.TextBox
    Friend WithEvents txtUserName As System.Windows.Forms.TextBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents lstv As System.Windows.Forms.ListView
    Friend WithEvents cmpPrivilege As System.Windows.Forms.ComboBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUser))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label3 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.cmpPrivilege = New System.Windows.Forms.ComboBox
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.txtUserName = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.butCancel = New System.Windows.Forms.Button
        Me.butDelete = New System.Windows.Forms.Button
        Me.butUpdate = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.txtIDNo = New System.Windows.Forms.TextBox
        Me.txtName = New System.Windows.Forms.TextBox
        Me.txtUserCode = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.lstv = New System.Windows.Forms.ListView
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(4, 5)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(736, 656)
        Me.Panel1.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Location = New System.Drawing.Point(8, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(720, 640)
        Me.Panel2.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(10, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 15)
        Me.Label3.TabIndex = 35
        Me.Label3.Text = "USER "
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.cmpPrivilege)
        Me.Panel3.Controls.Add(Me.txtPassword)
        Me.Panel3.Controls.Add(Me.txtUserName)
        Me.Panel3.Controls.Add(Me.Label6)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Controls.Add(Me.txtIDNo)
        Me.Panel3.Controls.Add(Me.txtName)
        Me.Panel3.Controls.Add(Me.txtUserCode)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Location = New System.Drawing.Point(8, 32)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(704, 600)
        Me.Panel3.TabIndex = 0
        '
        'cmpPrivilege
        '
        Me.cmpPrivilege.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmpPrivilege.Items.AddRange(New Object() {"-Select-", "Admin", "Supervisor", "Operator", "Worker"})
        Me.cmpPrivilege.Location = New System.Drawing.Point(116, 142)
        Me.cmpPrivilege.Name = "cmpPrivilege"
        Me.cmpPrivilege.Size = New System.Drawing.Size(121, 21)
        Me.cmpPrivilege.TabIndex = 45
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(116, 112)
        Me.txtPassword.MaxLength = 30
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(176, 20)
        Me.txtPassword.TabIndex = 44
        '
        'txtUserName
        '
        Me.txtUserName.Location = New System.Drawing.Point(116, 82)
        Me.txtUserName.MaxLength = 30
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.Size = New System.Drawing.Size(176, 20)
        Me.txtUserName.TabIndex = 43
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 144)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 15)
        Me.Label6.TabIndex = 42
        Me.Label6.Text = "Privilege"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(8, 114)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(65, 15)
        Me.Label5.TabIndex = 41
        Me.Label5.Text = "Password"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 84)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 15)
        Me.Label4.TabIndex = 40
        Me.Label4.Text = "User Name"
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Panel4.Controls.Add(Me.butCancel)
        Me.Panel4.Controls.Add(Me.butDelete)
        Me.Panel4.Controls.Add(Me.butUpdate)
        Me.Panel4.Controls.Add(Me.butSave)
        Me.Panel4.Location = New System.Drawing.Point(184, 176)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(232, 72)
        Me.Panel4.TabIndex = 39
        '
        'butCancel
        '
        Me.butCancel.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butCancel.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCancel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butCancel.Image = CType(resources.GetObject("butCancel.Image"), System.Drawing.Image)
        Me.butCancel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(176, 5)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(48, 59)
        Me.butCancel.TabIndex = 3
        Me.butCancel.Text = "Cancel"
        Me.butCancel.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butCancel.UseVisualStyleBackColor = False
        '
        'butDelete
        '
        Me.butDelete.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butDelete.Enabled = False
        Me.butDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butDelete.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelete.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butDelete.Image = CType(resources.GetObject("butDelete.Image"), System.Drawing.Image)
        Me.butDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butDelete.Location = New System.Drawing.Point(120, 5)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(48, 59)
        Me.butDelete.TabIndex = 2
        Me.butDelete.Text = "Delete"
        Me.butDelete.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butDelete.UseVisualStyleBackColor = False
        '
        'butUpdate
        '
        Me.butUpdate.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butUpdate.Enabled = False
        Me.butUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butUpdate.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butUpdate.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butUpdate.Image = CType(resources.GetObject("butUpdate.Image"), System.Drawing.Image)
        Me.butUpdate.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butUpdate.Location = New System.Drawing.Point(64, 5)
        Me.butUpdate.Name = "butUpdate"
        Me.butUpdate.Size = New System.Drawing.Size(48, 59)
        Me.butUpdate.TabIndex = 1
        Me.butUpdate.Text = "Update"
        Me.butUpdate.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butUpdate.UseVisualStyleBackColor = False
        '
        'butSave
        '
        Me.butSave.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.butSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.butSave.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSave.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.butSave.Image = CType(resources.GetObject("butSave.Image"), System.Drawing.Image)
        Me.butSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(8, 5)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(48, 59)
        Me.butSave.TabIndex = 0
        Me.butSave.Text = "Save"
        Me.butSave.TextAlign = System.Drawing.ContentAlignment.BottomRight
        Me.butSave.UseVisualStyleBackColor = False
        '
        'txtIDNo
        '
        Me.txtIDNo.Location = New System.Drawing.Point(328, 22)
        Me.txtIDNo.MaxLength = 30
        Me.txtIDNo.Name = "txtIDNo"
        Me.txtIDNo.Size = New System.Drawing.Size(28, 20)
        Me.txtIDNo.TabIndex = 38
        Me.txtIDNo.Visible = False
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(116, 52)
        Me.txtName.MaxLength = 256
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(488, 20)
        Me.txtName.TabIndex = 37
        '
        'txtUserCode
        '
        Me.txtUserCode.Location = New System.Drawing.Point(116, 22)
        Me.txtUserCode.MaxLength = 15
        Me.txtUserCode.Name = "txtUserCode"
        Me.txtUserCode.Size = New System.Drawing.Size(121, 20)
        Me.txtUserCode.TabIndex = 36
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 15)
        Me.Label2.TabIndex = 35
        Me.Label2.Text = "Name"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 15)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "User ID"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.LightGray
        Me.Panel5.Controls.Add(Me.lstv)
        Me.Panel5.Location = New System.Drawing.Point(8, 256)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(688, 336)
        Me.Panel5.TabIndex = 1
        '
        'lstv
        '
        Me.lstv.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(8, 8)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(672, 320)
        Me.lstv.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lstv.TabIndex = 7
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'frmUser
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(744, 666)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmUser"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmUser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With lstv
            .Columns.Add("Uid", 0, HorizontalAlignment.Left)
            .Columns.Add("User ID", 230, HorizontalAlignment.Left)
            .Columns.Add("Name", 230, HorizontalAlignment.Left)
            .Columns.Add("User Name", 230, HorizontalAlignment.Left)
            .Columns.Add("Password", 0, HorizontalAlignment.Left)
            .Columns.Add("Privilege", 230, HorizontalAlignment.Left)
            list_Displaydata()
            cmpPrivilege.Text = "-Select-"
        End With
    End Sub
    Sub list_Displaydata()
        lstv.Items.Clear()
        Try
            com.Connection = cn
            com.CommandType = CommandType.Text
            com.CommandText = "select * from tbUser"
            cn.Open()
            dr = com.ExecuteReader(CommandBehavior.CloseConnection)
            While dr.Read
                Dim ls As New ListViewItem(Trim(dr.Item("_idno")))    ' you can also use reader.GetSqlValue(0) 
                ls.SubItems.Add(Trim(dr.Item("_userID")))
                ls.SubItems.Add(Trim(dr.Item("_name")))
                ls.SubItems.Add(Trim(dr.Item("_userName")))
                ls.SubItems.Add(Trim(dr.Item("_password")))
                ls.SubItems.Add(Trim(dr.Item("_privilege")))
                lstv.Items.Add(ls)
            End While
        Catch exce As Exception
            MsgBox(exce.ToString)
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Trim(txtUserCode.Text) = "" Then
            MsgBox("Enter the user id!", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Trim(txtName.Text) = "" Then
            MsgBox("Enter the name!", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Trim(txtUserName.Text) = "" Then
            MsgBox("Enter the username!", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Trim(txtPassword.Text) = "" Then
            MsgBox("Enter the password!", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Trim(cmpPrivilege.Text) = "" Or Trim(cmpPrivilege.Text) = "-Select-" Then
            MsgBox("Select the privilege!", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "insert into tbUser(_userID,_name,_userName,_password,_privilege)values(@userID,@name,@userName,@password,@privilege)"
                parm = .Parameters.Add("@userID", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtUserCode.Text)
                parm = .Parameters.Add("@name", SqlDbType.VarChar, 256)
                parm.Value = refTxt(txtName.Text)
                parm = .Parameters.Add("@userName", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtUserName.Text)
                parm = .Parameters.Add("@password", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtPassword.Text)
                parm = .Parameters.Add("@privilege", SqlDbType.VarChar, 50)
                parm.Value = refTxt(cmpPrivilege.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully saved!", MsgBoxStyle.Information)
            End With
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub
    Sub textReset()
        txtIDNo.Clear()
        txtUserCode.Clear()
        txtName.Clear()
        txtUserName.Clear()
        txtPassword.Clear()
        cmpPrivilege.Text = "-Select-"
        butSave.Enabled = True
        butDelete.Enabled = False
        butUpdate.Enabled = False
    End Sub
    Function refTxt(ByVal txt As String) As String
        Return Trim(Replace(txt, "'", "''"))
    End Function

    Private Sub butUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdate.Click
        If Trim(txtUserCode.Text) = "" Then
            MsgBox("Enter the user id!", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Trim(txtName.Text) = "" Then
            MsgBox("Enter the name!", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Trim(txtUserName.Text) = "" Then
            MsgBox("Enter the username!", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Trim(txtPassword.Text) = "" Then
            MsgBox("Enter the password!", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Trim(cmpPrivilege.Text) = "" Or Trim(cmpPrivilege.Text) = "-Select-" Then
            MsgBox("Select the privilege!", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "Update tbUser set _userID=@userID,_name=@name,_userName=@userName,_password=@password,_privilege=@privilege where _idno=@idno"
                parm = .Parameters.Add("@userID", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtUserCode.Text)
                parm = .Parameters.Add("@name", SqlDbType.VarChar, 256)
                parm.Value = refTxt(txtName.Text)
                parm = .Parameters.Add("@userName", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtUserName.Text)
                parm = .Parameters.Add("@password", SqlDbType.VarChar, 50)
                parm.Value = refTxt(txtPassword.Text)
                parm = .Parameters.Add("@privilege", SqlDbType.VarChar, 50)
                parm.Value = refTxt(cmpPrivilege.Text)
                parm = .Parameters.Add("@idno", SqlDbType.Int)
                parm.Value = refTxt(txtIDNo.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                textReset()
                MsgBox("Successfully saved!", MsgBoxStyle.Information)
            End With
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        list_Displaydata()
    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        Dim result As New DialogResult
        result = MessageBox.Show("Do you Want to delete this Records?", "Caption", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = Windows.Forms.DialogResult.Yes Then
            Try
                With com
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "delete from  tbUser  where _idno=@idno"
                    parm = .Parameters.Add("@idno", SqlDbType.Int)
                    parm.Value = refTxt(txtIDNo.Text)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()
                    textReset()
                    MsgBox("Successfully saved!", MsgBoxStyle.Information)
                End With
            Catch ex As SqlException
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            Finally
                cn.Close()
                com.Parameters.Clear()
            End Try
            list_Displaydata()
        End If
    End Sub

    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged

    End Sub

    Private Sub lstv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.Click
        txtIDNo.Text = lstv.SelectedItems(0).Text
        txtUserCode.Text = lstv.SelectedItems(0).SubItems(1).Text
        txtName.Text = lstv.SelectedItems(0).SubItems(2).Text
        txtUserName.Text = lstv.SelectedItems(0).SubItems(3).Text
        txtPassword.Text = lstv.SelectedItems(0).SubItems(4).Text
        cmpPrivilege.Text = lstv.SelectedItems(0).SubItems(5).Text
        butSave.Enabled = False
        butUpdate.Enabled = True
        butDelete.Enabled = True
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        textReset()
    End Sub
End Class
