Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings
Imports System.Collections.Specialized.NameValueCollection
Module ModMain
    Dim selectForm As String
    Dim config As System.Collections.Specialized.NameValueCollection = AppSettings()
    Dim SqlString As String = config.Item("SQLConnString")
    Public APPPath As String = config.Item("Path")
    Public cn As New SqlClient.SqlConnection(SqlString)
    Dim Sqlser As String = config.Item("SQLconServer")
    Public cnser As New SqlClient.SqlConnection(Sqlser)
    Public com As New SqlClient.SqlCommand
    Public LogUserID As String
    Public LogUserName As String
    Public LogName As String
    Public LogPrivilege As String
    Public AccessRights As String = config.Item("AccessRights")
    Public SessionJobno As String
    'Public selectForm As String
    Public RealDownload As String = config.Item("DownloadTime")
    Public strMailPath As String = config.Item("AutoMailPath")
    Public strMailServer As String = config.Item("mailServer")
    Public strFromEmail As String = config.Item("fromAdd")
    Public SendMailTime As String = config.Item("SendMailTime")

    Public Function fncstr(ByVal st As String) As String
        Dim stv As String
        Try
            stv = Trim(Replace(st, "'", "''"))
        Catch ex As Exception
            stv = ""
        End Try
        Return stv
    End Function
End Module
