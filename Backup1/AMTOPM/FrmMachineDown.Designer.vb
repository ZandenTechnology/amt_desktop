<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMachineDown
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMachineDown))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.txtHFRemarks = New System.Windows.Forms.Label
        Me.txtHFOption = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtDHTID = New System.Windows.Forms.Label
        Me.txtDHFID = New System.Windows.Forms.Label
        Me.txtHTName = New System.Windows.Forms.Label
        Me.txtHFName = New System.Windows.Forms.Label
        Me.txtHTID = New System.Windows.Forms.Label
        Me.txtHFID = New System.Windows.Forms.Label
        Me.txtHTo = New System.Windows.Forms.Label
        Me.txtHFrom = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.pn = New System.Windows.Forms.Panel
        Me.lblMsgbox = New System.Windows.Forms.Label
        Me.txtMain = New System.Windows.Forms.TextBox
        Me.txtAction = New System.Windows.Forms.Label
        Me.lblAction = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Panel7 = New System.Windows.Forms.Panel
        Me.Label2 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.txtStartTime = New System.Windows.Forms.MaskedTextBox
        Me.txtStartDate = New System.Windows.Forms.MaskedTextBox
        Me.txtRemarks = New System.Windows.Forms.TextBox
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.lblOption = New System.Windows.Forms.Label
        Me.txtDFid = New System.Windows.Forms.Label
        Me.txtFid = New System.Windows.Forms.Label
        Me.lblForm = New System.Windows.Forms.Label
        Me.lblFid = New System.Windows.Forms.Label
        Me.lblFName = New System.Windows.Forms.Label
        Me.picEnt1 = New System.Windows.Forms.PictureBox
        Me.txtFName = New System.Windows.Forms.Label
        Me.picEnt2 = New System.Windows.Forms.PictureBox
        Me.lblStartTime = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.txtFrom = New System.Windows.Forms.Label
        Me.lblSRemarks = New System.Windows.Forms.Label
        Me.cmbOption = New System.Windows.Forms.ComboBox
        Me.lblSOption = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.lblMachineID = New System.Windows.Forms.Label
        Me.txtMachineID = New System.Windows.Forms.Label
        Me.lblMachineDesc = New System.Windows.Forms.Label
        Me.txtMachineDesc = New System.Windows.Forms.Label
        Me.txtID = New System.Windows.Forms.TextBox
        Me.pic1 = New System.Windows.Forms.PictureBox
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.txtEndDate = New System.Windows.Forms.MaskedTextBox
        Me.txtDTid = New System.Windows.Forms.Label
        Me.lblTid = New System.Windows.Forms.Label
        Me.lblTo = New System.Windows.Forms.Label
        Me.lblTName = New System.Windows.Forms.Label
        Me.picEnt4 = New System.Windows.Forms.PictureBox
        Me.txtTid = New System.Windows.Forms.Label
        Me.picEnt3 = New System.Windows.Forms.PictureBox
        Me.txtTName = New System.Windows.Forms.Label
        Me.lblEndTime = New System.Windows.Forms.Label
        Me.txtTo = New System.Windows.Forms.Label
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.lblStatus = New System.Windows.Forms.Label
        Me.TimerMachine = New System.Windows.Forms.Timer(Me.components)
        Me.txtEndTime = New System.Windows.Forms.MaskedTextBox
        Me.Panel1.SuspendLayout()
        Me.pn.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.picEnt1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picEnt2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.picEnt4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picEnt3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.PaleGoldenrod
        Me.Panel1.Controls.Add(Me.txtHFRemarks)
        Me.Panel1.Controls.Add(Me.txtHFOption)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.txtDHTID)
        Me.Panel1.Controls.Add(Me.txtDHFID)
        Me.Panel1.Controls.Add(Me.txtHTName)
        Me.Panel1.Controls.Add(Me.txtHFName)
        Me.Panel1.Controls.Add(Me.txtHTID)
        Me.Panel1.Controls.Add(Me.txtHFID)
        Me.Panel1.Controls.Add(Me.txtHTo)
        Me.Panel1.Controls.Add(Me.txtHFrom)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Location = New System.Drawing.Point(120, 449)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(643, 145)
        Me.Panel1.TabIndex = 4
        '
        'txtHFRemarks
        '
        Me.txtHFRemarks.AutoSize = True
        Me.txtHFRemarks.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtHFRemarks.Location = New System.Drawing.Point(124, 121)
        Me.txtHFRemarks.Name = "txtHFRemarks"
        Me.txtHFRemarks.Size = New System.Drawing.Size(29, 20)
        Me.txtHFRemarks.TabIndex = 25
        Me.txtHFRemarks.Text = "Nil"
        '
        'txtHFOption
        '
        Me.txtHFOption.AutoSize = True
        Me.txtHFOption.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtHFOption.Location = New System.Drawing.Point(124, 94)
        Me.txtHFOption.Name = "txtHFOption"
        Me.txtHFOption.Size = New System.Drawing.Size(29, 20)
        Me.txtHFOption.TabIndex = 139
        Me.txtHFOption.Text = "Nil"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(12, 121)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(90, 20)
        Me.Label10.TabIndex = 138
        Me.Label10.Text = "Remarks :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(12, 94)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 20)
        Me.Label1.TabIndex = 133
        Me.Label1.Text = "Option :"
        '
        'txtDHTID
        '
        Me.txtDHTID.AutoSize = True
        Me.txtDHTID.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtDHTID.Location = New System.Drawing.Point(491, 40)
        Me.txtDHTID.Name = "txtDHTID"
        Me.txtDHTID.Size = New System.Drawing.Size(29, 20)
        Me.txtDHTID.TabIndex = 27
        Me.txtDHTID.Text = "Nil"
        '
        'txtDHFID
        '
        Me.txtDHFID.AutoSize = True
        Me.txtDHFID.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtDHFID.Location = New System.Drawing.Point(124, 40)
        Me.txtDHFID.Name = "txtDHFID"
        Me.txtDHFID.Size = New System.Drawing.Size(29, 20)
        Me.txtDHFID.TabIndex = 26
        Me.txtDHFID.Text = "Nil"
        '
        'txtHTName
        '
        Me.txtHTName.AutoSize = True
        Me.txtHTName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtHTName.Location = New System.Drawing.Point(491, 67)
        Me.txtHTName.Name = "txtHTName"
        Me.txtHTName.Size = New System.Drawing.Size(29, 20)
        Me.txtHTName.TabIndex = 25
        Me.txtHTName.Text = "Nil"
        '
        'txtHFName
        '
        Me.txtHFName.AutoSize = True
        Me.txtHFName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtHFName.Location = New System.Drawing.Point(124, 67)
        Me.txtHFName.Name = "txtHFName"
        Me.txtHFName.Size = New System.Drawing.Size(29, 20)
        Me.txtHFName.TabIndex = 24
        Me.txtHFName.Text = "Nil"
        '
        'txtHTID
        '
        Me.txtHTID.AutoSize = True
        Me.txtHTID.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtHTID.Location = New System.Drawing.Point(582, 40)
        Me.txtHTID.Name = "txtHTID"
        Me.txtHTID.Size = New System.Drawing.Size(29, 20)
        Me.txtHTID.TabIndex = 23
        Me.txtHTID.Text = "Nil"
        Me.txtHTID.Visible = False
        '
        'txtHFID
        '
        Me.txtHFID.AutoSize = True
        Me.txtHFID.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtHFID.Location = New System.Drawing.Point(262, 40)
        Me.txtHFID.Name = "txtHFID"
        Me.txtHFID.Size = New System.Drawing.Size(29, 20)
        Me.txtHFID.TabIndex = 22
        Me.txtHFID.Text = "Nil"
        Me.txtHFID.Visible = False
        '
        'txtHTo
        '
        Me.txtHTo.AutoSize = True
        Me.txtHTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtHTo.Location = New System.Drawing.Point(491, 13)
        Me.txtHTo.Name = "txtHTo"
        Me.txtHTo.Size = New System.Drawing.Size(29, 20)
        Me.txtHTo.TabIndex = 21
        Me.txtHTo.Text = "Nil"
        '
        'txtHFrom
        '
        Me.txtHFrom.AutoSize = True
        Me.txtHFrom.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtHFrom.Location = New System.Drawing.Point(124, 13)
        Me.txtHFrom.Name = "txtHFrom"
        Me.txtHFrom.Size = New System.Drawing.Size(29, 20)
        Me.txtHFrom.TabIndex = 20
        Me.txtHFrom.Text = "Nil"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label8.Location = New System.Drawing.Point(377, 67)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(65, 20)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "Name :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label7.Location = New System.Drawing.Point(377, 40)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(114, 20)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "Operator ID :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label6.Location = New System.Drawing.Point(12, 67)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 20)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Name :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(12, 40)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(114, 20)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Operator ID :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(377, 13)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 20)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "To :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(12, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 20)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "From :"
        '
        'pn
        '
        Me.pn.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pn.BackColor = System.Drawing.SystemColors.Info
        Me.pn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pn.Controls.Add(Me.lblMsgbox)
        Me.pn.Controls.Add(Me.txtMain)
        Me.pn.Controls.Add(Me.PictureBox1)
        Me.pn.Controls.Add(Me.Panel1)
        Me.pn.Controls.Add(Me.Label9)
        Me.pn.Controls.Add(Me.Panel7)
        Me.pn.Controls.Add(Me.Label2)
        Me.pn.Controls.Add(Me.Panel3)
        Me.pn.Controls.Add(Me.Panel6)
        Me.pn.Location = New System.Drawing.Point(5, 6)
        Me.pn.Name = "pn"
        Me.pn.Size = New System.Drawing.Size(800, 600)
        Me.pn.TabIndex = 5
        '
        'lblMsgbox
        '
        Me.lblMsgbox.AutoSize = True
        Me.lblMsgbox.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsgbox.ForeColor = System.Drawing.Color.DarkRed
        Me.lblMsgbox.Location = New System.Drawing.Point(114, 410)
        Me.lblMsgbox.Name = "lblMsgbox"
        Me.lblMsgbox.Size = New System.Drawing.Size(98, 17)
        Me.lblMsgbox.TabIndex = 13
        Me.lblMsgbox.Text = "Machine ID :"
        '
        'txtMain
        '
        Me.txtMain.Location = New System.Drawing.Point(501, 423)
        Me.txtMain.Name = "txtMain"
        Me.txtMain.Size = New System.Drawing.Size(0, 20)
        Me.txtMain.TabIndex = 7
        '
        'txtAction
        '
        Me.txtAction.BackColor = System.Drawing.Color.Olive
        Me.txtAction.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAction.ForeColor = System.Drawing.Color.White
        Me.txtAction.Location = New System.Drawing.Point(423, 341)
        Me.txtAction.Name = "txtAction"
        Me.txtAction.Size = New System.Drawing.Size(223, 29)
        Me.txtAction.TabIndex = 29
        Me.txtAction.Text = "ACTION "
        Me.txtAction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAction
        '
        Me.lblAction.BackColor = System.Drawing.Color.Olive
        Me.lblAction.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAction.ForeColor = System.Drawing.Color.White
        Me.lblAction.Location = New System.Drawing.Point(363, 341)
        Me.lblAction.Name = "lblAction"
        Me.lblAction.Size = New System.Drawing.Size(61, 29)
        Me.lblAction.TabIndex = 28
        Me.lblAction.Text = "ACTION :"
        Me.lblAction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = CType(resources.GetObject("PictureBox1.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(2, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(77, 597)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Olive
        Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(114, 6)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(262, 36)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = " MACHINE DOWNTIME INFORMATION"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Olive
        Me.Panel7.Location = New System.Drawing.Point(114, 449)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(660, 148)
        Me.Panel7.TabIndex = 136
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Olive
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(114, 431)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(262, 22)
        Me.Label2.TabIndex = 137
        Me.Label2.Text = "LAST MACHINE DOWNTIME"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Khaki
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Controls.Add(Me.Panel2)
        Me.Panel3.Controls.Add(Me.txtID)
        Me.Panel3.Controls.Add(Me.pic1)
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Panel3.Location = New System.Drawing.Point(121, 44)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(643, 336)
        Me.Panel3.TabIndex = 5
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.LemonChiffon
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel4.Controls.Add(Me.txtStartTime)
        Me.Panel4.Controls.Add(Me.txtStartDate)
        Me.Panel4.Controls.Add(Me.txtRemarks)
        Me.Panel4.Controls.Add(Me.lblRemarks)
        Me.Panel4.Controls.Add(Me.lblOption)
        Me.Panel4.Controls.Add(Me.txtDFid)
        Me.Panel4.Controls.Add(Me.txtFid)
        Me.Panel4.Controls.Add(Me.lblForm)
        Me.Panel4.Controls.Add(Me.lblFid)
        Me.Panel4.Controls.Add(Me.lblFName)
        Me.Panel4.Controls.Add(Me.picEnt1)
        Me.Panel4.Controls.Add(Me.txtFName)
        Me.Panel4.Controls.Add(Me.picEnt2)
        Me.Panel4.Controls.Add(Me.lblStartTime)
        Me.Panel4.Controls.Add(Me.lblStartDate)
        Me.Panel4.Controls.Add(Me.txtFrom)
        Me.Panel4.Controls.Add(Me.lblSRemarks)
        Me.Panel4.Controls.Add(Me.cmbOption)
        Me.Panel4.Controls.Add(Me.lblSOption)
        Me.Panel4.Location = New System.Drawing.Point(65, 97)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(560, 135)
        Me.Panel4.TabIndex = 135
        '
        'txtStartTime
        '
        Me.txtStartTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStartTime.Location = New System.Drawing.Point(353, 44)
        Me.txtStartTime.Mask = "00:00"
        Me.txtStartTime.Name = "txtStartTime"
        Me.txtStartTime.Size = New System.Drawing.Size(77, 26)
        Me.txtStartTime.TabIndex = 138
        Me.txtStartTime.ValidatingType = GetType(Date)
        '
        'txtStartDate
        '
        Me.txtStartDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStartDate.Location = New System.Drawing.Point(136, 44)
        Me.txtStartDate.Mask = "00/00/0000"
        Me.txtStartDate.Name = "txtStartDate"
        Me.txtStartDate.Size = New System.Drawing.Size(100, 26)
        Me.txtStartDate.TabIndex = 27
        Me.txtStartDate.ValidatingType = GetType(Date)
        '
        'txtRemarks
        '
        Me.txtRemarks.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.Location = New System.Drawing.Point(136, 102)
        Me.txtRemarks.MaxLength = 500
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.Size = New System.Drawing.Size(407, 26)
        Me.txtRemarks.TabIndex = 135
        Me.txtRemarks.Visible = False
        '
        'lblRemarks
        '
        Me.lblRemarks.AutoSize = True
        Me.lblRemarks.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblRemarks.ForeColor = System.Drawing.Color.Black
        Me.lblRemarks.Location = New System.Drawing.Point(2, 105)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(90, 20)
        Me.lblRemarks.TabIndex = 133
        Me.lblRemarks.Text = "Remarks :"
        '
        'lblOption
        '
        Me.lblOption.AutoSize = True
        Me.lblOption.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblOption.ForeColor = System.Drawing.Color.Black
        Me.lblOption.Location = New System.Drawing.Point(2, 76)
        Me.lblOption.Name = "lblOption"
        Me.lblOption.Size = New System.Drawing.Size(57, 20)
        Me.lblOption.TabIndex = 132
        Me.lblOption.Text = "Type :"
        '
        'txtDFid
        '
        Me.txtDFid.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtDFid.ForeColor = System.Drawing.Color.DarkGreen
        Me.txtDFid.Location = New System.Drawing.Point(140, 0)
        Me.txtDFid.Name = "txtDFid"
        Me.txtDFid.Size = New System.Drawing.Size(109, 20)
        Me.txtDFid.TabIndex = 131
        Me.txtDFid.Text = "Nil"
        '
        'txtFid
        '
        Me.txtFid.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtFid.ForeColor = System.Drawing.Color.DarkGreen
        Me.txtFid.Location = New System.Drawing.Point(318, 0)
        Me.txtFid.Name = "txtFid"
        Me.txtFid.Size = New System.Drawing.Size(29, 20)
        Me.txtFid.TabIndex = 20
        Me.txtFid.Text = "Nil"
        Me.txtFid.Visible = False
        '
        'lblForm
        '
        Me.lblForm.AutoSize = True
        Me.lblForm.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblForm.ForeColor = System.Drawing.Color.Black
        Me.lblForm.Location = New System.Drawing.Point(2, 47)
        Me.lblForm.Name = "lblForm"
        Me.lblForm.Size = New System.Drawing.Size(60, 20)
        Me.lblForm.TabIndex = 14
        Me.lblForm.Text = "From :"
        '
        'lblFid
        '
        Me.lblFid.AutoSize = True
        Me.lblFid.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblFid.ForeColor = System.Drawing.Color.Black
        Me.lblFid.Location = New System.Drawing.Point(2, 0)
        Me.lblFid.Name = "lblFid"
        Me.lblFid.Size = New System.Drawing.Size(114, 20)
        Me.lblFid.TabIndex = 17
        Me.lblFid.Text = "Operator ID :"
        '
        'lblFName
        '
        Me.lblFName.AutoSize = True
        Me.lblFName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblFName.ForeColor = System.Drawing.Color.Black
        Me.lblFName.Location = New System.Drawing.Point(2, 22)
        Me.lblFName.Name = "lblFName"
        Me.lblFName.Size = New System.Drawing.Size(65, 20)
        Me.lblFName.TabIndex = 12
        Me.lblFName.Text = "Name :"
        '
        'picEnt1
        '
        Me.picEnt1.Image = CType(resources.GetObject("picEnt1.Image"), System.Drawing.Image)
        Me.picEnt1.Location = New System.Drawing.Point(331, 47)
        Me.picEnt1.Name = "picEnt1"
        Me.picEnt1.Size = New System.Drawing.Size(22, 20)
        Me.picEnt1.TabIndex = 130
        Me.picEnt1.TabStop = False
        Me.picEnt1.Visible = False
        '
        'txtFName
        '
        Me.txtFName.AutoSize = True
        Me.txtFName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtFName.ForeColor = System.Drawing.Color.DarkGreen
        Me.txtFName.Location = New System.Drawing.Point(140, 22)
        Me.txtFName.Name = "txtFName"
        Me.txtFName.Size = New System.Drawing.Size(29, 20)
        Me.txtFName.TabIndex = 21
        Me.txtFName.Text = "Nil"
        '
        'picEnt2
        '
        Me.picEnt2.Image = CType(resources.GetObject("picEnt2.Image"), System.Drawing.Image)
        Me.picEnt2.Location = New System.Drawing.Point(487, 47)
        Me.picEnt2.Name = "picEnt2"
        Me.picEnt2.Size = New System.Drawing.Size(22, 20)
        Me.picEnt2.TabIndex = 129
        Me.picEnt2.TabStop = False
        Me.picEnt2.Visible = False
        '
        'lblStartTime
        '
        Me.lblStartTime.AutoSize = True
        Me.lblStartTime.BackColor = System.Drawing.Color.Red
        Me.lblStartTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartTime.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblStartTime.Location = New System.Drawing.Point(437, 51)
        Me.lblStartTime.Name = "lblStartTime"
        Me.lblStartTime.Size = New System.Drawing.Size(45, 12)
        Me.lblStartTime.TabIndex = 81
        Me.lblStartTime.Text = "HH:MM"
        Me.lblStartTime.Visible = False
        '
        'lblStartDate
        '
        Me.lblStartDate.AutoSize = True
        Me.lblStartDate.BackColor = System.Drawing.Color.Red
        Me.lblStartDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblStartDate.Location = New System.Drawing.Point(255, 51)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(73, 12)
        Me.lblStartDate.TabIndex = 78
        Me.lblStartDate.Text = "DD/MM/YYYY"
        Me.lblStartDate.Visible = False
        '
        'txtFrom
        '
        Me.txtFrom.AutoSize = True
        Me.txtFrom.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtFrom.ForeColor = System.Drawing.Color.DarkGreen
        Me.txtFrom.Location = New System.Drawing.Point(140, 47)
        Me.txtFrom.Name = "txtFrom"
        Me.txtFrom.Size = New System.Drawing.Size(29, 20)
        Me.txtFrom.TabIndex = 19
        Me.txtFrom.Text = "Nil"
        '
        'lblSRemarks
        '
        Me.lblSRemarks.AutoSize = True
        Me.lblSRemarks.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblSRemarks.ForeColor = System.Drawing.Color.DarkGreen
        Me.lblSRemarks.Location = New System.Drawing.Point(140, 105)
        Me.lblSRemarks.Name = "lblSRemarks"
        Me.lblSRemarks.Size = New System.Drawing.Size(29, 20)
        Me.lblSRemarks.TabIndex = 137
        Me.lblSRemarks.Text = "Nil"
        '
        'cmbOption
        '
        Me.cmbOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOption.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbOption.FormattingEnabled = True
        Me.cmbOption.Items.AddRange(New Object() {"Breakdown", "PM"})
        Me.cmbOption.Location = New System.Drawing.Point(136, 72)
        Me.cmbOption.Name = "cmbOption"
        Me.cmbOption.Size = New System.Drawing.Size(109, 28)
        Me.cmbOption.TabIndex = 134
        Me.cmbOption.Visible = False
        '
        'lblSOption
        '
        Me.lblSOption.AutoSize = True
        Me.lblSOption.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblSOption.ForeColor = System.Drawing.Color.DarkGreen
        Me.lblSOption.Location = New System.Drawing.Point(140, 76)
        Me.lblSOption.Name = "lblSOption"
        Me.lblSOption.Size = New System.Drawing.Size(29, 20)
        Me.lblSOption.TabIndex = 136
        Me.lblSOption.Text = "Nil"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.LemonChiffon
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel2.Controls.Add(Me.lblMachineID)
        Me.Panel2.Controls.Add(Me.txtMachineID)
        Me.Panel2.Controls.Add(Me.lblMachineDesc)
        Me.Panel2.Controls.Add(Me.txtMachineDesc)
        Me.Panel2.Location = New System.Drawing.Point(64, 13)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(560, 78)
        Me.Panel2.TabIndex = 134
        '
        'lblMachineID
        '
        Me.lblMachineID.AutoSize = True
        Me.lblMachineID.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblMachineID.ForeColor = System.Drawing.Color.Black
        Me.lblMachineID.Location = New System.Drawing.Point(2, 7)
        Me.lblMachineID.Name = "lblMachineID"
        Me.lblMachineID.Size = New System.Drawing.Size(110, 20)
        Me.lblMachineID.TabIndex = 12
        Me.lblMachineID.Text = "Machine ID :"
        '
        'txtMachineID
        '
        Me.txtMachineID.AutoSize = True
        Me.txtMachineID.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtMachineID.ForeColor = System.Drawing.Color.DarkGreen
        Me.txtMachineID.Location = New System.Drawing.Point(140, 7)
        Me.txtMachineID.Name = "txtMachineID"
        Me.txtMachineID.Size = New System.Drawing.Size(29, 20)
        Me.txtMachineID.TabIndex = 13
        Me.txtMachineID.Text = "Nil"
        '
        'lblMachineDesc
        '
        Me.lblMachineDesc.AutoSize = True
        Me.lblMachineDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblMachineDesc.ForeColor = System.Drawing.Color.Black
        Me.lblMachineDesc.Location = New System.Drawing.Point(2, 46)
        Me.lblMachineDesc.Name = "lblMachineDesc"
        Me.lblMachineDesc.Size = New System.Drawing.Size(132, 20)
        Me.lblMachineDesc.TabIndex = 25
        Me.lblMachineDesc.Text = "Machine Desc :"
        '
        'txtMachineDesc
        '
        Me.txtMachineDesc.AutoSize = True
        Me.txtMachineDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtMachineDesc.ForeColor = System.Drawing.Color.DarkGreen
        Me.txtMachineDesc.Location = New System.Drawing.Point(140, 46)
        Me.txtMachineDesc.Name = "txtMachineDesc"
        Me.txtMachineDesc.Size = New System.Drawing.Size(29, 20)
        Me.txtMachineDesc.TabIndex = 26
        Me.txtMachineDesc.Text = "Nil"
        '
        'txtID
        '
        Me.txtID.Location = New System.Drawing.Point(529, -1)
        Me.txtID.MaxLength = 10
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(96, 20)
        Me.txtID.TabIndex = 133
        Me.txtID.Visible = False
        '
        'pic1
        '
        Me.pic1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.pic1.Image = Global.AMTOPM.My.Resources.Resources.Arrow1
        Me.pic1.Location = New System.Drawing.Point(27, 19)
        Me.pic1.Name = "pic1"
        Me.pic1.Size = New System.Drawing.Size(36, 24)
        Me.pic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic1.TabIndex = 15
        Me.pic1.TabStop = False
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.LemonChiffon
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel5.Controls.Add(Me.txtEndTime)
        Me.Panel5.Controls.Add(Me.txtEndDate)
        Me.Panel5.Controls.Add(Me.txtDTid)
        Me.Panel5.Controls.Add(Me.lblTid)
        Me.Panel5.Controls.Add(Me.lblTo)
        Me.Panel5.Controls.Add(Me.lblTName)
        Me.Panel5.Controls.Add(Me.picEnt4)
        Me.Panel5.Controls.Add(Me.txtTid)
        Me.Panel5.Controls.Add(Me.picEnt3)
        Me.Panel5.Controls.Add(Me.txtTName)
        Me.Panel5.Controls.Add(Me.lblEndTime)
        Me.Panel5.Controls.Add(Me.txtTo)
        Me.Panel5.Controls.Add(Me.lblEndDate)
        Me.Panel5.Location = New System.Drawing.Point(65, 232)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(560, 100)
        Me.Panel5.TabIndex = 136
        '
        'txtEndDate
        '
        Me.txtEndDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEndDate.Location = New System.Drawing.Point(136, 67)
        Me.txtEndDate.Mask = "00/00/0000"
        Me.txtEndDate.Name = "txtEndDate"
        Me.txtEndDate.Size = New System.Drawing.Size(100, 26)
        Me.txtEndDate.TabIndex = 134
        Me.txtEndDate.ValidatingType = GetType(Date)
        '
        'txtDTid
        '
        Me.txtDTid.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtDTid.ForeColor = System.Drawing.Color.DarkGreen
        Me.txtDTid.Location = New System.Drawing.Point(136, 5)
        Me.txtDTid.Name = "txtDTid"
        Me.txtDTid.Size = New System.Drawing.Size(123, 20)
        Me.txtDTid.TabIndex = 133
        Me.txtDTid.Text = "Nil"
        '
        'lblTid
        '
        Me.lblTid.AutoSize = True
        Me.lblTid.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblTid.ForeColor = System.Drawing.Color.Black
        Me.lblTid.Location = New System.Drawing.Point(2, 5)
        Me.lblTid.Name = "lblTid"
        Me.lblTid.Size = New System.Drawing.Size(114, 20)
        Me.lblTid.TabIndex = 16
        Me.lblTid.Text = "Operator ID :"
        '
        'lblTo
        '
        Me.lblTo.AutoSize = True
        Me.lblTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblTo.ForeColor = System.Drawing.Color.Black
        Me.lblTo.Location = New System.Drawing.Point(2, 70)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(39, 20)
        Me.lblTo.TabIndex = 15
        Me.lblTo.Text = "To :"
        '
        'lblTName
        '
        Me.lblTName.AutoSize = True
        Me.lblTName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblTName.ForeColor = System.Drawing.Color.Black
        Me.lblTName.Location = New System.Drawing.Point(2, 36)
        Me.lblTName.Name = "lblTName"
        Me.lblTName.Size = New System.Drawing.Size(65, 20)
        Me.lblTName.TabIndex = 18
        Me.lblTName.Text = "Name :"
        '
        'picEnt4
        '
        Me.picEnt4.Image = CType(resources.GetObject("picEnt4.Image"), System.Drawing.Image)
        Me.picEnt4.Location = New System.Drawing.Point(487, 70)
        Me.picEnt4.Name = "picEnt4"
        Me.picEnt4.Size = New System.Drawing.Size(22, 20)
        Me.picEnt4.TabIndex = 132
        Me.picEnt4.TabStop = False
        Me.picEnt4.Visible = False
        '
        'txtTid
        '
        Me.txtTid.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtTid.ForeColor = System.Drawing.Color.DarkGreen
        Me.txtTid.Location = New System.Drawing.Point(308, 5)
        Me.txtTid.Name = "txtTid"
        Me.txtTid.Size = New System.Drawing.Size(38, 20)
        Me.txtTid.TabIndex = 23
        Me.txtTid.Text = "Nil"
        Me.txtTid.Visible = False
        '
        'picEnt3
        '
        Me.picEnt3.Image = CType(resources.GetObject("picEnt3.Image"), System.Drawing.Image)
        Me.picEnt3.Location = New System.Drawing.Point(316, 70)
        Me.picEnt3.Name = "picEnt3"
        Me.picEnt3.Size = New System.Drawing.Size(22, 20)
        Me.picEnt3.TabIndex = 131
        Me.picEnt3.TabStop = False
        Me.picEnt3.Visible = False
        '
        'txtTName
        '
        Me.txtTName.AutoSize = True
        Me.txtTName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtTName.ForeColor = System.Drawing.Color.DarkGreen
        Me.txtTName.Location = New System.Drawing.Point(136, 36)
        Me.txtTName.Name = "txtTName"
        Me.txtTName.Size = New System.Drawing.Size(29, 20)
        Me.txtTName.TabIndex = 24
        Me.txtTName.Text = "Nil"
        '
        'lblEndTime
        '
        Me.lblEndTime.AutoSize = True
        Me.lblEndTime.BackColor = System.Drawing.Color.Red
        Me.lblEndTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndTime.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblEndTime.Location = New System.Drawing.Point(437, 74)
        Me.lblEndTime.Name = "lblEndTime"
        Me.lblEndTime.Size = New System.Drawing.Size(45, 12)
        Me.lblEndTime.TabIndex = 80
        Me.lblEndTime.Text = "HH:MM"
        Me.lblEndTime.Visible = False
        '
        'txtTo
        '
        Me.txtTo.AutoSize = True
        Me.txtTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.txtTo.ForeColor = System.Drawing.Color.DarkGreen
        Me.txtTo.Location = New System.Drawing.Point(140, 70)
        Me.txtTo.Name = "txtTo"
        Me.txtTo.Size = New System.Drawing.Size(29, 20)
        Me.txtTo.TabIndex = 22
        Me.txtTo.Text = "Nil"
        '
        'lblEndDate
        '
        Me.lblEndDate.AutoSize = True
        Me.lblEndDate.BackColor = System.Drawing.Color.Red
        Me.lblEndDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblEndDate.Location = New System.Drawing.Point(242, 74)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(73, 12)
        Me.lblEndDate.TabIndex = 79
        Me.lblEndDate.Text = "DD/MM/YYYY"
        Me.lblEndDate.Visible = False
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Olive
        Me.Panel6.Controls.Add(Me.lblStatus)
        Me.Panel6.Controls.Add(Me.txtAction)
        Me.Panel6.Controls.Add(Me.lblAction)
        Me.Panel6.Location = New System.Drawing.Point(114, 36)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(660, 371)
        Me.Panel6.TabIndex = 135
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.Red
        Me.lblStatus.Location = New System.Drawing.Point(360, 253)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(0, 13)
        Me.lblStatus.TabIndex = 27
        '
        'TimerMachine
        '
        Me.TimerMachine.Enabled = True
        Me.TimerMachine.Interval = 1000
        '
        'txtEndTime
        '
        Me.txtEndTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEndTime.Location = New System.Drawing.Point(353, 67)
        Me.txtEndTime.Mask = "00:00"
        Me.txtEndTime.Name = "txtEndTime"
        Me.txtEndTime.Size = New System.Drawing.Size(77, 26)
        Me.txtEndTime.TabIndex = 139
        Me.txtEndTime.ValidatingType = GetType(Date)
        '
        'FrmMachineDown
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(812, 618)
        Me.ControlBox = False
        Me.Controls.Add(Me.pn)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "FrmMachineDown"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.pn.ResumeLayout(False)
        Me.pn.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.picEnt1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picEnt2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.picEnt4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picEnt3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents pn As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblForm As System.Windows.Forms.Label
    Friend WithEvents txtMachineID As System.Windows.Forms.Label
    Friend WithEvents lblMachineID As System.Windows.Forms.Label
    Friend WithEvents txtTid As System.Windows.Forms.Label
    Friend WithEvents txtTo As System.Windows.Forms.Label
    Friend WithEvents txtFName As System.Windows.Forms.Label
    Friend WithEvents txtFid As System.Windows.Forms.Label
    Friend WithEvents lblTName As System.Windows.Forms.Label
    Friend WithEvents lblFName As System.Windows.Forms.Label
    Friend WithEvents lblFid As System.Windows.Forms.Label
    Friend WithEvents lblTid As System.Windows.Forms.Label
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents txtTName As System.Windows.Forms.Label
    Friend WithEvents txtMain As System.Windows.Forms.TextBox
    Friend WithEvents lblMsgbox As System.Windows.Forms.Label
    Friend WithEvents TimerMachine As System.Windows.Forms.Timer
    Friend WithEvents txtMachineDesc As System.Windows.Forms.Label
    Friend WithEvents lblMachineDesc As System.Windows.Forms.Label
    Friend WithEvents txtHTName As System.Windows.Forms.Label
    Friend WithEvents txtHFName As System.Windows.Forms.Label
    Friend WithEvents txtHTID As System.Windows.Forms.Label
    Friend WithEvents txtHFID As System.Windows.Forms.Label
    Friend WithEvents txtHTo As System.Windows.Forms.Label
    Friend WithEvents txtHFrom As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents pic1 As System.Windows.Forms.PictureBox
    Friend WithEvents txtAction As System.Windows.Forms.Label
    Friend WithEvents lblAction As System.Windows.Forms.Label
    Friend WithEvents lblStartTime As System.Windows.Forms.Label
    Friend WithEvents lblEndTime As System.Windows.Forms.Label
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents picEnt1 As System.Windows.Forms.PictureBox
    Friend WithEvents picEnt2 As System.Windows.Forms.PictureBox
    Friend WithEvents picEnt4 As System.Windows.Forms.PictureBox
    Friend WithEvents picEnt3 As System.Windows.Forms.PictureBox
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents txtFrom As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDTid As System.Windows.Forms.Label
    Friend WithEvents txtDHTID As System.Windows.Forms.Label
    Friend WithEvents txtDHFID As System.Windows.Forms.Label
    Friend WithEvents txtDFid As System.Windows.Forms.Label
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents lblOption As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As System.Windows.Forms.TextBox
    Friend WithEvents cmbOption As System.Windows.Forms.ComboBox
    Friend WithEvents lblSRemarks As System.Windows.Forms.Label
    Friend WithEvents lblSOption As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtHFRemarks As System.Windows.Forms.Label
    Friend WithEvents txtHFOption As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtStartDate As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtEndDate As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtStartTime As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtEndTime As System.Windows.Forms.MaskedTextBox
End Class
