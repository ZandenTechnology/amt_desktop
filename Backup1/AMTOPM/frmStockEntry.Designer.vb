<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStockEntry
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStockEntry))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label15 = New System.Windows.Forms.Label
        Me.PictureBox5 = New System.Windows.Forms.PictureBox
        Me.txtid = New System.Windows.Forms.Label
        Me.txtParent = New System.Windows.Forms.Label
        Me.lblMsgbox = New System.Windows.Forms.Label
        Me.txtsuf = New System.Windows.Forms.Label
        Me.lbl = New System.Windows.Forms.Label
        Me.pic1 = New System.Windows.Forms.PictureBox
        Me.txtJobno = New System.Windows.Forms.Label
        Me.txtMain = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.pMain = New System.Windows.Forms.Panel
        Me.lblSn = New System.Windows.Forms.Label
        Me.lblOpID = New System.Windows.Forms.Label
        Me.lblOpName = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblCom = New System.Windows.Forms.Label
        Me.lblDesc = New System.Windows.Forms.Label
        Me.lblItemCode = New System.Windows.Forms.Label
        Me.lblJobDate = New System.Windows.Forms.Label
        Me.picJob = New System.Windows.Forms.PictureBox
        Me.lblName = New System.Windows.Forms.Label
        Me.PicSave = New System.Windows.Forms.PictureBox
        Me.lblSave = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.lbl1 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.lbl3 = New System.Windows.Forms.Label
        Me.txtWC = New System.Windows.Forms.Label
        Me.lblJ = New System.Windows.Forms.Label
        Me.lblJobno = New System.Windows.Forms.Label
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.lblStatus = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.pMain.SuspendLayout()
        CType(Me.picJob, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicSave, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.BackColor = System.Drawing.SystemColors.Info
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.PictureBox5)
        Me.Panel1.Controls.Add(Me.txtid)
        Me.Panel1.Controls.Add(Me.txtParent)
        Me.Panel1.Controls.Add(Me.lblMsgbox)
        Me.Panel1.Controls.Add(Me.txtsuf)
        Me.Panel1.Controls.Add(Me.lbl)
        Me.Panel1.Controls.Add(Me.pic1)
        Me.Panel1.Controls.Add(Me.txtJobno)
        Me.Panel1.Controls.Add(Me.txtMain)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.Panel6)
        Me.Panel1.Location = New System.Drawing.Point(32, 51)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(800, 600)
        Me.Panel1.TabIndex = 5
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.DarkRed
        Me.Label15.Location = New System.Drawing.Point(3, 1)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(255, 19)
        Me.Label15.TabIndex = 144
        Me.Label15.Text = "STOCK ENTRY"
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(684, 0)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(111, 60)
        Me.PictureBox5.TabIndex = 142
        Me.PictureBox5.TabStop = False
        '
        'txtid
        '
        Me.txtid.AutoSize = True
        Me.txtid.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtid.ForeColor = System.Drawing.Color.Black
        Me.txtid.Location = New System.Drawing.Point(163, 560)
        Me.txtid.Name = "txtid"
        Me.txtid.Size = New System.Drawing.Size(21, 22)
        Me.txtid.TabIndex = 86
        Me.txtid.Text = "0"
        Me.txtid.Visible = False
        '
        'txtParent
        '
        Me.txtParent.AutoSize = True
        Me.txtParent.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtParent.ForeColor = System.Drawing.Color.Black
        Me.txtParent.Location = New System.Drawing.Point(584, 313)
        Me.txtParent.Name = "txtParent"
        Me.txtParent.Size = New System.Drawing.Size(0, 22)
        Me.txtParent.TabIndex = 84
        Me.txtParent.Visible = False
        '
        'lblMsgbox
        '
        Me.lblMsgbox.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.lblMsgbox.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsgbox.ForeColor = System.Drawing.Color.DarkRed
        Me.lblMsgbox.Location = New System.Drawing.Point(150, 119)
        Me.lblMsgbox.Name = "lblMsgbox"
        Me.lblMsgbox.Size = New System.Drawing.Size(508, 19)
        Me.lblMsgbox.TabIndex = 82
        Me.lblMsgbox.Text = "Please scan job no.!"
        Me.lblMsgbox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtsuf
        '
        Me.txtsuf.BackColor = System.Drawing.Color.Transparent
        Me.txtsuf.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsuf.ForeColor = System.Drawing.Color.Black
        Me.txtsuf.Location = New System.Drawing.Point(533, 171)
        Me.txtsuf.Name = "txtsuf"
        Me.txtsuf.Size = New System.Drawing.Size(47, 22)
        Me.txtsuf.TabIndex = 81
        Me.txtsuf.Text = "A1A"
        Me.txtsuf.Visible = False
        '
        'lbl
        '
        Me.lbl.BackColor = System.Drawing.Color.Transparent
        Me.lbl.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl.ForeColor = System.Drawing.Color.Black
        Me.lbl.Location = New System.Drawing.Point(517, 171)
        Me.lbl.Name = "lbl"
        Me.lbl.Size = New System.Drawing.Size(16, 22)
        Me.lbl.TabIndex = 80
        Me.lbl.Text = "-"
        Me.lbl.Visible = False
        '
        'pic1
        '
        Me.pic1.BackColor = System.Drawing.Color.Transparent
        Me.pic1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.pic1.Image = Global.AMTOPM.My.Resources.Resources.Arrow1
        Me.pic1.Location = New System.Drawing.Point(236, 213)
        Me.pic1.Name = "pic1"
        Me.pic1.Size = New System.Drawing.Size(22, 22)
        Me.pic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic1.TabIndex = 75
        Me.pic1.TabStop = False
        '
        'txtJobno
        '
        Me.txtJobno.BackColor = System.Drawing.Color.Transparent
        Me.txtJobno.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobno.ForeColor = System.Drawing.Color.Black
        Me.txtJobno.Location = New System.Drawing.Point(365, 171)
        Me.txtJobno.Name = "txtJobno"
        Me.txtJobno.Size = New System.Drawing.Size(162, 22)
        Me.txtJobno.TabIndex = 73
        Me.txtJobno.Text = "AAAAAAAAAAA"
        Me.txtJobno.Visible = False
        '
        'txtMain
        '
        Me.txtMain.Location = New System.Drawing.Point(287, 58)
        Me.txtMain.Name = "txtMain"
        Me.txtMain.Size = New System.Drawing.Size(0, 20)
        Me.txtMain.TabIndex = 7
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Olive
        Me.Label14.Location = New System.Drawing.Point(146, 111)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(517, 34)
        Me.Label14.TabIndex = 143
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.Control
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.pMain)
        Me.Panel2.Controls.Add(Me.txtWC)
        Me.Panel2.Controls.Add(Me.lblJ)
        Me.Panel2.Controls.Add(Me.lblJobno)
        Me.Panel2.Location = New System.Drawing.Point(151, 148)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(508, 389)
        Me.Panel2.TabIndex = 87
        '
        'pMain
        '
        Me.pMain.BackColor = System.Drawing.Color.Cornsilk
        Me.pMain.Controls.Add(Me.lblSn)
        Me.pMain.Controls.Add(Me.lblOpID)
        Me.pMain.Controls.Add(Me.lblOpName)
        Me.pMain.Controls.Add(Me.lblDate)
        Me.pMain.Controls.Add(Me.lblCom)
        Me.pMain.Controls.Add(Me.lblDesc)
        Me.pMain.Controls.Add(Me.lblItemCode)
        Me.pMain.Controls.Add(Me.lblJobDate)
        Me.pMain.Controls.Add(Me.picJob)
        Me.pMain.Controls.Add(Me.lblName)
        Me.pMain.Controls.Add(Me.PicSave)
        Me.pMain.Controls.Add(Me.lblSave)
        Me.pMain.Controls.Add(Me.Label2)
        Me.pMain.Controls.Add(Me.Label1)
        Me.pMain.Controls.Add(Me.Label6)
        Me.pMain.Controls.Add(Me.lbl1)
        Me.pMain.Controls.Add(Me.Label5)
        Me.pMain.Controls.Add(Me.lbl3)
        Me.pMain.Location = New System.Drawing.Point(55, 114)
        Me.pMain.Name = "pMain"
        Me.pMain.Size = New System.Drawing.Size(428, 240)
        Me.pMain.TabIndex = 88
        Me.pMain.Visible = False
        '
        'lblSn
        '
        Me.lblSn.AutoSize = True
        Me.lblSn.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSn.Location = New System.Drawing.Point(299, 4)
        Me.lblSn.Name = "lblSn"
        Me.lblSn.Size = New System.Drawing.Size(122, 13)
        Me.lblSn.TabIndex = 87
        Me.lblSn.Text = "OPERATOR NAME :"
        Me.lblSn.Visible = False
        '
        'lblOpID
        '
        Me.lblOpID.AutoSize = True
        Me.lblOpID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOpID.Location = New System.Drawing.Point(299, 120)
        Me.lblOpID.Name = "lblOpID"
        Me.lblOpID.Size = New System.Drawing.Size(122, 13)
        Me.lblOpID.TabIndex = 86
        Me.lblOpID.Text = "OPERATOR NAME :"
        Me.lblOpID.Visible = False
        '
        'lblOpName
        '
        Me.lblOpName.AutoSize = True
        Me.lblOpName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOpName.Location = New System.Drawing.Point(153, 159)
        Me.lblOpName.Name = "lblOpName"
        Me.lblOpName.Size = New System.Drawing.Size(122, 13)
        Me.lblOpName.TabIndex = 85
        Me.lblOpName.Text = "OPERATOR NAME :"
        '
        'lblDate
        '
        Me.lblDate.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.lblDate.AutoSize = True
        Me.lblDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(80, 120)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(48, 13)
        Me.lblDate.TabIndex = 84
        Me.lblDate.Text = "DATE :"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCom
        '
        Me.lblCom.AutoSize = True
        Me.lblCom.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCom.Location = New System.Drawing.Point(166, 85)
        Me.lblCom.Name = "lblCom"
        Me.lblCom.Size = New System.Drawing.Size(75, 13)
        Me.lblCom.TabIndex = 83
        Me.lblCom.Text = "JOB DATE :"
        '
        'lblDesc
        '
        Me.lblDesc.AutoSize = True
        Me.lblDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDesc.Location = New System.Drawing.Point(108, 57)
        Me.lblDesc.Name = "lblDesc"
        Me.lblDesc.Size = New System.Drawing.Size(75, 13)
        Me.lblDesc.TabIndex = 82
        Me.lblDesc.Text = "JOB DATE :"
        '
        'lblItemCode
        '
        Me.lblItemCode.AutoSize = True
        Me.lblItemCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItemCode.Location = New System.Drawing.Point(99, 28)
        Me.lblItemCode.Name = "lblItemCode"
        Me.lblItemCode.Size = New System.Drawing.Size(75, 13)
        Me.lblItemCode.TabIndex = 81
        Me.lblItemCode.Text = "JOB DATE :"
        '
        'lblJobDate
        '
        Me.lblJobDate.AutoSize = True
        Me.lblJobDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobDate.Location = New System.Drawing.Point(86, 4)
        Me.lblJobDate.Name = "lblJobDate"
        Me.lblJobDate.Size = New System.Drawing.Size(75, 13)
        Me.lblJobDate.TabIndex = 80
        Me.lblJobDate.Text = "JOB DATE :"
        '
        'picJob
        '
        Me.picJob.BackColor = System.Drawing.Color.Transparent
        Me.picJob.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.picJob.Image = Global.AMTOPM.My.Resources.Resources.Arrow1
        Me.picJob.Location = New System.Drawing.Point(6, 155)
        Me.picJob.Name = "picJob"
        Me.picJob.Size = New System.Drawing.Size(22, 22)
        Me.picJob.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picJob.TabIndex = 79
        Me.picJob.TabStop = False
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(30, 159)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(122, 13)
        Me.lblName.TabIndex = 78
        Me.lblName.Text = "OPERATOR NAME :"
        '
        'PicSave
        '
        Me.PicSave.BackColor = System.Drawing.Color.Transparent
        Me.PicSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PicSave.Image = Global.AMTOPM.My.Resources.Resources.Arrow1
        Me.PicSave.Location = New System.Drawing.Point(152, 209)
        Me.PicSave.Name = "PicSave"
        Me.PicSave.Size = New System.Drawing.Size(22, 22)
        Me.PicSave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicSave.TabIndex = 77
        Me.PicSave.TabStop = False
        '
        'lblSave
        '
        Me.lblSave.AutoSize = True
        Me.lblSave.BackColor = System.Drawing.Color.Transparent
        Me.lblSave.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSave.ForeColor = System.Drawing.Color.Black
        Me.lblSave.Location = New System.Drawing.Point(180, 209)
        Me.lblSave.Name = "lblSave"
        Me.lblSave.Size = New System.Drawing.Size(67, 22)
        Me.lblSave.TabIndex = 76
        Me.lblSave.Text = "SAVE "
        Me.lblSave.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(32, 120)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 13)
        Me.Label2.TabIndex = 75
        Me.Label2.Text = "DATE :"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Goldenrod
        Me.Label1.Location = New System.Drawing.Point(3, 107)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(423, 1)
        Me.Label1.TabIndex = 74
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(5, 4)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 13)
        Me.Label6.TabIndex = 73
        Me.Label6.Text = "JOB DATE :"
        '
        'lbl1
        '
        Me.lbl1.AutoSize = True
        Me.lbl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl1.Location = New System.Drawing.Point(3, 85)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(157, 13)
        Me.lbl1.TabIndex = 72
        Me.lbl1.Text = "COMPLETED QUANTITY :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 56)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(99, 13)
        Me.Label5.TabIndex = 71
        Me.Label5.Text = "DESCRIPTION :"
        '
        'lbl3
        '
        Me.lbl3.AutoSize = True
        Me.lbl3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl3.Location = New System.Drawing.Point(3, 28)
        Me.lbl3.Name = "lbl3"
        Me.lbl3.Size = New System.Drawing.Size(87, 13)
        Me.lbl3.TabIndex = 70
        Me.lbl3.Text = "ITEM  CODE :"
        '
        'txtWC
        '
        Me.txtWC.AutoSize = True
        Me.txtWC.BackColor = System.Drawing.Color.Khaki
        Me.txtWC.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWC.ForeColor = System.Drawing.Color.Black
        Me.txtWC.Location = New System.Drawing.Point(207, 98)
        Me.txtWC.Name = "txtWC"
        Me.txtWC.Size = New System.Drawing.Size(0, 22)
        Me.txtWC.TabIndex = 87
        '
        'lblJ
        '
        Me.lblJ.BackColor = System.Drawing.Color.Transparent
        Me.lblJ.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJ.ForeColor = System.Drawing.Color.Black
        Me.lblJ.Location = New System.Drawing.Point(207, 66)
        Me.lblJ.Name = "lblJ"
        Me.lblJ.Size = New System.Drawing.Size(281, 22)
        Me.lblJ.TabIndex = 83
        Me.lblJ.Text = "AAAAAAAAAAA"
        '
        'lblJobno
        '
        Me.lblJobno.AutoSize = True
        Me.lblJobno.BackColor = System.Drawing.Color.Transparent
        Me.lblJobno.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobno.ForeColor = System.Drawing.Color.Black
        Me.lblJobno.Location = New System.Drawing.Point(109, 66)
        Me.lblJobno.Name = "lblJobno"
        Me.lblJobno.Size = New System.Drawing.Size(95, 22)
        Me.lblJobno.TabIndex = 71
        Me.lblJobno.Text = "JOB NO :"
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.DarkKhaki
        Me.Panel6.Controls.Add(Me.lblStatus)
        Me.Panel6.Location = New System.Drawing.Point(146, 141)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(517, 403)
        Me.Panel6.TabIndex = 141
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.Red
        Me.lblStatus.Location = New System.Drawing.Point(360, 253)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(0, 13)
        Me.lblStatus.TabIndex = 27
        '
        'frmStockEntry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(864, 702)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmStockEntry"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.pMain.ResumeLayout(False)
        Me.pMain.PerformLayout()
        CType(Me.picJob, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicSave, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents txtid As System.Windows.Forms.Label
    Friend WithEvents txtParent As System.Windows.Forms.Label
    Friend WithEvents lblMsgbox As System.Windows.Forms.Label
    Friend WithEvents txtsuf As System.Windows.Forms.Label
    Friend WithEvents lbl As System.Windows.Forms.Label
    Friend WithEvents pic1 As System.Windows.Forms.PictureBox
    Friend WithEvents txtJobno As System.Windows.Forms.Label
    Friend WithEvents txtMain As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtWC As System.Windows.Forms.Label
    Friend WithEvents lblJ As System.Windows.Forms.Label
    Friend WithEvents lblJobno As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents pMain As System.Windows.Forms.Panel
    Friend WithEvents lbl3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lbl1 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblSave As System.Windows.Forms.Label
    Friend WithEvents PicSave As System.Windows.Forms.PictureBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents picJob As System.Windows.Forms.PictureBox
    Friend WithEvents lblJobDate As System.Windows.Forms.Label
    Friend WithEvents lblItemCode As System.Windows.Forms.Label
    Friend WithEvents lblDesc As System.Windows.Forms.Label
    Friend WithEvents lblCom As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblOpName As System.Windows.Forms.Label
    Friend WithEvents lblOpID As System.Windows.Forms.Label
    Friend WithEvents lblSn As System.Windows.Forms.Label
End Class
