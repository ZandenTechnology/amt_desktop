<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmScanJob
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmScanJob))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.PictureBox5 = New System.Windows.Forms.PictureBox
        Me.txtid = New System.Windows.Forms.Label
        Me.txtParent = New System.Windows.Forms.Label
        Me.lblMsgbox = New System.Windows.Forms.Label
        Me.txtsuf = New System.Windows.Forms.Label
        Me.lbl = New System.Windows.Forms.Label
        Me.pic1 = New System.Windows.Forms.PictureBox
        Me.txtJobno = New System.Windows.Forms.Label
        Me.txtMain = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label24 = New System.Windows.Forms.Label
        Me.txtWC = New System.Windows.Forms.Label
        Me.lblWC = New System.Windows.Forms.Label
        Me.txtRework = New System.Windows.Forms.Label
        Me.lblJ = New System.Windows.Forms.Label
        Me.txtOpID = New System.Windows.Forms.Label
        Me.lblOpID = New System.Windows.Forms.Label
        Me.txtAction = New System.Windows.Forms.Label
        Me.lblJobno = New System.Windows.Forms.Label
        Me.lblAction = New System.Windows.Forms.Label
        Me.PPrint = New System.Windows.Forms.Panel
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.ploptStart = New System.Windows.Forms.Panel
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.ploptnew = New System.Windows.Forms.Panel
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.ploptTrans = New System.Windows.Forms.Panel
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.ploptResume = New System.Windows.Forms.Panel
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.ploptStartrework = New System.Windows.Forms.Panel
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label26 = New System.Windows.Forms.Label
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.lblStatus = New System.Windows.Forms.Label
        Me.TimerMachine = New System.Windows.Forms.Timer(Me.components)
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog
        Me.PrintDocument2 = New System.Drawing.Printing.PrintDocument
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.PPrint.SuspendLayout()
        Me.ploptStart.SuspendLayout()
        Me.ploptnew.SuspendLayout()
        Me.ploptTrans.SuspendLayout()
        Me.ploptResume.SuspendLayout()
        Me.ploptStartrework.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.BackColor = System.Drawing.SystemColors.Info
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.PictureBox5)
        Me.Panel1.Controls.Add(Me.txtid)
        Me.Panel1.Controls.Add(Me.txtParent)
        Me.Panel1.Controls.Add(Me.lblMsgbox)
        Me.Panel1.Controls.Add(Me.txtsuf)
        Me.Panel1.Controls.Add(Me.lbl)
        Me.Panel1.Controls.Add(Me.pic1)
        Me.Panel1.Controls.Add(Me.txtJobno)
        Me.Panel1.Controls.Add(Me.txtMain)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.Panel6)
        Me.Panel1.Location = New System.Drawing.Point(33, 65)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(800, 600)
        Me.Panel1.TabIndex = 4
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(684, 0)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(111, 60)
        Me.PictureBox5.TabIndex = 142
        Me.PictureBox5.TabStop = False
        '
        'txtid
        '
        Me.txtid.AutoSize = True
        Me.txtid.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtid.ForeColor = System.Drawing.Color.Black
        Me.txtid.Location = New System.Drawing.Point(163, 560)
        Me.txtid.Name = "txtid"
        Me.txtid.Size = New System.Drawing.Size(21, 22)
        Me.txtid.TabIndex = 86
        Me.txtid.Text = "0"
        Me.txtid.Visible = False
        '
        'txtParent
        '
        Me.txtParent.AutoSize = True
        Me.txtParent.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtParent.ForeColor = System.Drawing.Color.Black
        Me.txtParent.Location = New System.Drawing.Point(584, 313)
        Me.txtParent.Name = "txtParent"
        Me.txtParent.Size = New System.Drawing.Size(0, 22)
        Me.txtParent.TabIndex = 84
        Me.txtParent.Visible = False
        '
        'lblMsgbox
        '
        Me.lblMsgbox.BackColor = System.Drawing.Color.Gold
        Me.lblMsgbox.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsgbox.ForeColor = System.Drawing.Color.DarkRed
        Me.lblMsgbox.Location = New System.Drawing.Point(150, 119)
        Me.lblMsgbox.Name = "lblMsgbox"
        Me.lblMsgbox.Size = New System.Drawing.Size(508, 19)
        Me.lblMsgbox.TabIndex = 82
        Me.lblMsgbox.Text = "Please scan job no.!"
        Me.lblMsgbox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtsuf
        '
        Me.txtsuf.BackColor = System.Drawing.Color.Khaki
        Me.txtsuf.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsuf.ForeColor = System.Drawing.Color.Black
        Me.txtsuf.Location = New System.Drawing.Point(533, 171)
        Me.txtsuf.Name = "txtsuf"
        Me.txtsuf.Size = New System.Drawing.Size(47, 22)
        Me.txtsuf.TabIndex = 81
        Me.txtsuf.Text = "A1A"
        Me.txtsuf.Visible = False
        '
        'lbl
        '
        Me.lbl.BackColor = System.Drawing.Color.Khaki
        Me.lbl.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl.ForeColor = System.Drawing.Color.Black
        Me.lbl.Location = New System.Drawing.Point(517, 171)
        Me.lbl.Name = "lbl"
        Me.lbl.Size = New System.Drawing.Size(16, 22)
        Me.lbl.TabIndex = 80
        Me.lbl.Text = "-"
        Me.lbl.Visible = False
        '
        'pic1
        '
        Me.pic1.BackColor = System.Drawing.Color.Khaki
        Me.pic1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.pic1.Image = Global.AMTOPM.My.Resources.Resources.Arrow1
        Me.pic1.Location = New System.Drawing.Point(236, 213)
        Me.pic1.Name = "pic1"
        Me.pic1.Size = New System.Drawing.Size(22, 22)
        Me.pic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic1.TabIndex = 75
        Me.pic1.TabStop = False
        '
        'txtJobno
        '
        Me.txtJobno.BackColor = System.Drawing.Color.Khaki
        Me.txtJobno.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobno.ForeColor = System.Drawing.Color.Black
        Me.txtJobno.Location = New System.Drawing.Point(365, 171)
        Me.txtJobno.Name = "txtJobno"
        Me.txtJobno.Size = New System.Drawing.Size(162, 22)
        Me.txtJobno.TabIndex = 73
        Me.txtJobno.Text = "AAAAAAAAAAA"
        Me.txtJobno.Visible = False
        '
        'txtMain
        '
        Me.txtMain.Location = New System.Drawing.Point(287, 58)
        Me.txtMain.Name = "txtMain"
        Me.txtMain.Size = New System.Drawing.Size(0, 20)
        Me.txtMain.TabIndex = 7
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.Color.Gold
        Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Olive
        Me.Label14.Location = New System.Drawing.Point(146, 111)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(517, 34)
        Me.Label14.TabIndex = 143
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Khaki
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Label24)
        Me.Panel2.Controls.Add(Me.txtWC)
        Me.Panel2.Controls.Add(Me.lblWC)
        Me.Panel2.Controls.Add(Me.txtRework)
        Me.Panel2.Controls.Add(Me.lblJ)
        Me.Panel2.Controls.Add(Me.txtOpID)
        Me.Panel2.Controls.Add(Me.lblOpID)
        Me.Panel2.Controls.Add(Me.txtAction)
        Me.Panel2.Controls.Add(Me.lblJobno)
        Me.Panel2.Controls.Add(Me.lblAction)
        Me.Panel2.Controls.Add(Me.PPrint)
        Me.Panel2.Controls.Add(Me.ploptStart)
        Me.Panel2.Controls.Add(Me.ploptnew)
        Me.Panel2.Controls.Add(Me.ploptTrans)
        Me.Panel2.Controls.Add(Me.ploptResume)
        Me.Panel2.Controls.Add(Me.ploptStartrework)
        Me.Panel2.Location = New System.Drawing.Point(151, 148)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(508, 389)
        Me.Panel2.TabIndex = 87
        '
        'Label24
        '
        Me.Label24.BackColor = System.Drawing.Color.Khaki
        Me.Label24.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.Black
        Me.Label24.Location = New System.Drawing.Point(187, 98)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(19, 22)
        Me.Label24.TabIndex = 88
        Me.Label24.Text = ":"
        '
        'txtWC
        '
        Me.txtWC.AutoSize = True
        Me.txtWC.BackColor = System.Drawing.Color.Khaki
        Me.txtWC.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWC.ForeColor = System.Drawing.Color.Black
        Me.txtWC.Location = New System.Drawing.Point(207, 98)
        Me.txtWC.Name = "txtWC"
        Me.txtWC.Size = New System.Drawing.Size(0, 22)
        Me.txtWC.TabIndex = 87
        '
        'lblWC
        '
        Me.lblWC.BackColor = System.Drawing.Color.Khaki
        Me.lblWC.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWC.ForeColor = System.Drawing.Color.Black
        Me.lblWC.Location = New System.Drawing.Point(109, 98)
        Me.lblWC.Name = "lblWC"
        Me.lblWC.Size = New System.Drawing.Size(58, 22)
        Me.lblWC.TabIndex = 86
        Me.lblWC.Text = "WC          :"
        '
        'txtRework
        '
        Me.txtRework.BackColor = System.Drawing.Color.Khaki
        Me.txtRework.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRework.ForeColor = System.Drawing.Color.Black
        Me.txtRework.Location = New System.Drawing.Point(434, 22)
        Me.txtRework.Name = "txtRework"
        Me.txtRework.Size = New System.Drawing.Size(47, 22)
        Me.txtRework.TabIndex = 84
        Me.txtRework.Text = "A1A"
        Me.txtRework.Visible = False
        '
        'lblJ
        '
        Me.lblJ.BackColor = System.Drawing.Color.Khaki
        Me.lblJ.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJ.ForeColor = System.Drawing.Color.Black
        Me.lblJ.Location = New System.Drawing.Point(207, 66)
        Me.lblJ.Name = "lblJ"
        Me.lblJ.Size = New System.Drawing.Size(281, 22)
        Me.lblJ.TabIndex = 83
        Me.lblJ.Text = "AAAAAAAAAAA"
        '
        'txtOpID
        '
        Me.txtOpID.BackColor = System.Drawing.Color.Khaki
        Me.txtOpID.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOpID.ForeColor = System.Drawing.Color.Black
        Me.txtOpID.Location = New System.Drawing.Point(262, 164)
        Me.txtOpID.Name = "txtOpID"
        Me.txtOpID.Size = New System.Drawing.Size(209, 22)
        Me.txtOpID.TabIndex = 81
        Me.txtOpID.Text = "ACTION :"
        Me.txtOpID.Visible = False
        '
        'lblOpID
        '
        Me.lblOpID.AutoSize = True
        Me.lblOpID.BackColor = System.Drawing.Color.Khaki
        Me.lblOpID.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOpID.ForeColor = System.Drawing.Color.Black
        Me.lblOpID.Location = New System.Drawing.Point(109, 162)
        Me.lblOpID.Name = "lblOpID"
        Me.lblOpID.Size = New System.Drawing.Size(154, 22)
        Me.lblOpID.TabIndex = 80
        Me.lblOpID.Text = "OPERATOR ID :"
        Me.lblOpID.Visible = False
        '
        'txtAction
        '
        Me.txtAction.BackColor = System.Drawing.Color.Khaki
        Me.txtAction.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAction.ForeColor = System.Drawing.Color.Black
        Me.txtAction.Location = New System.Drawing.Point(207, 129)
        Me.txtAction.Name = "txtAction"
        Me.txtAction.Size = New System.Drawing.Size(162, 22)
        Me.txtAction.TabIndex = 74
        Me.txtAction.Text = "ACTION :"
        '
        'lblJobno
        '
        Me.lblJobno.AutoSize = True
        Me.lblJobno.BackColor = System.Drawing.Color.Khaki
        Me.lblJobno.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobno.ForeColor = System.Drawing.Color.Black
        Me.lblJobno.Location = New System.Drawing.Point(109, 66)
        Me.lblJobno.Name = "lblJobno"
        Me.lblJobno.Size = New System.Drawing.Size(95, 22)
        Me.lblJobno.TabIndex = 71
        Me.lblJobno.Text = "JOB NO :"
        '
        'lblAction
        '
        Me.lblAction.AutoSize = True
        Me.lblAction.BackColor = System.Drawing.Color.Khaki
        Me.lblAction.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAction.ForeColor = System.Drawing.Color.Black
        Me.lblAction.Location = New System.Drawing.Point(109, 130)
        Me.lblAction.Name = "lblAction"
        Me.lblAction.Size = New System.Drawing.Size(95, 22)
        Me.lblAction.TabIndex = 72
        Me.lblAction.Text = "ACTION :"
        '
        'PPrint
        '
        Me.PPrint.BackColor = System.Drawing.Color.Khaki
        Me.PPrint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PPrint.Controls.Add(Me.Label5)
        Me.PPrint.Controls.Add(Me.Label20)
        Me.PPrint.Location = New System.Drawing.Point(139, 194)
        Me.PPrint.Name = "PPrint"
        Me.PPrint.Size = New System.Drawing.Size(200, 90)
        Me.PPrint.TabIndex = 82
        Me.PPrint.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(59, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 16)
        Me.Label5.TabIndex = 52
        Me.Label5.Text = "- PRINT"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Black
        Me.Label20.Location = New System.Drawing.Point(3, 2)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(58, 16)
        Me.Label20.TabIndex = 50
        Me.Label20.Text = "OPTION"
        '
        'ploptStart
        '
        Me.ploptStart.BackColor = System.Drawing.Color.Khaki
        Me.ploptStart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ploptStart.Controls.Add(Me.Label16)
        Me.ploptStart.Controls.Add(Me.Label13)
        Me.ploptStart.Controls.Add(Me.Label8)
        Me.ploptStart.Controls.Add(Me.Label7)
        Me.ploptStart.Controls.Add(Me.Label4)
        Me.ploptStart.Controls.Add(Me.Label6)
        Me.ploptStart.Location = New System.Drawing.Point(141, 189)
        Me.ploptStart.Name = "ploptStart"
        Me.ploptStart.Size = New System.Drawing.Size(200, 164)
        Me.ploptStart.TabIndex = 77
        Me.ploptStart.Visible = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(80, 91)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(55, 16)
        Me.Label16.TabIndex = 51
        Me.Label16.Text = "- PRINT"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(80, 39)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(60, 16)
        Me.Label13.TabIndex = 53
        Me.Label13.Text = "- PAUSE"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(13, 135)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(48, 16)
        Me.Label8.TabIndex = 52
        Me.Label8.Text = "- VOID"
        Me.Label8.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(80, 65)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 16)
        Me.Label7.TabIndex = 51
        Me.Label7.Text = "- SPLIT"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(3, 2)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 16)
        Me.Label4.TabIndex = 50
        Me.Label4.Text = "OPTION"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(80, 15)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(87, 16)
        Me.Label6.TabIndex = 48
        Me.Label6.Text = "- COMPLETE"
        '
        'ploptnew
        '
        Me.ploptnew.BackColor = System.Drawing.Color.Khaki
        Me.ploptnew.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ploptnew.Controls.Add(Me.Label19)
        Me.ploptnew.Controls.Add(Me.Label3)
        Me.ploptnew.Controls.Add(Me.Label2)
        Me.ploptnew.Controls.Add(Me.Label1)
        Me.ploptnew.Location = New System.Drawing.Point(139, 202)
        Me.ploptnew.Name = "ploptnew"
        Me.ploptnew.Size = New System.Drawing.Size(200, 90)
        Me.ploptnew.TabIndex = 76
        Me.ploptnew.Visible = False
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(74, 64)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(55, 16)
        Me.Label19.TabIndex = 52
        Me.Label19.Text = "- PRINT"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(3, 2)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 16)
        Me.Label3.TabIndex = 50
        Me.Label3.Text = "OPTION"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(74, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 16)
        Me.Label2.TabIndex = 49
        Me.Label2.Text = "- SKIP"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(74, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 16)
        Me.Label1.TabIndex = 48
        Me.Label1.Text = "- START"
        '
        'ploptTrans
        '
        Me.ploptTrans.BackColor = System.Drawing.Color.Khaki
        Me.ploptTrans.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ploptTrans.Controls.Add(Me.Label17)
        Me.ploptTrans.Controls.Add(Me.Label10)
        Me.ploptTrans.Controls.Add(Me.Label12)
        Me.ploptTrans.Location = New System.Drawing.Point(139, 202)
        Me.ploptTrans.Name = "ploptTrans"
        Me.ploptTrans.Size = New System.Drawing.Size(200, 75)
        Me.ploptTrans.TabIndex = 79
        Me.ploptTrans.Visible = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Black
        Me.Label17.Location = New System.Drawing.Point(78, 47)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(55, 16)
        Me.Label17.TabIndex = 52
        Me.Label17.Text = "- PRINT"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(3, 2)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(58, 16)
        Me.Label10.TabIndex = 50
        Me.Label10.Text = "OPTION"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(78, 23)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(85, 16)
        Me.Label12.TabIndex = 48
        Me.Label12.Text = "- TRANSFER"
        '
        'ploptResume
        '
        Me.ploptResume.BackColor = System.Drawing.Color.Khaki
        Me.ploptResume.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ploptResume.Controls.Add(Me.Label18)
        Me.ploptResume.Controls.Add(Me.Label9)
        Me.ploptResume.Controls.Add(Me.Label11)
        Me.ploptResume.Location = New System.Drawing.Point(139, 202)
        Me.ploptResume.Name = "ploptResume"
        Me.ploptResume.Size = New System.Drawing.Size(200, 68)
        Me.ploptResume.TabIndex = 78
        Me.ploptResume.Visible = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Black
        Me.Label18.Location = New System.Drawing.Point(68, 48)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(55, 16)
        Me.Label18.TabIndex = 53
        Me.Label18.Text = "- PRINT"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(3, 2)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(58, 16)
        Me.Label9.TabIndex = 50
        Me.Label9.Text = "OPTION"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(68, 23)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(70, 16)
        Me.Label11.TabIndex = 48
        Me.Label11.Text = "- RESUME"
        '
        'ploptStartrework
        '
        Me.ploptStartrework.BackColor = System.Drawing.Color.Khaki
        Me.ploptStartrework.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ploptStartrework.Controls.Add(Me.Label21)
        Me.ploptStartrework.Controls.Add(Me.Label22)
        Me.ploptStartrework.Controls.Add(Me.Label23)
        Me.ploptStartrework.Controls.Add(Me.Label25)
        Me.ploptStartrework.Controls.Add(Me.Label26)
        Me.ploptStartrework.Location = New System.Drawing.Point(140, 193)
        Me.ploptStartrework.Name = "ploptStartrework"
        Me.ploptStartrework.Size = New System.Drawing.Size(200, 164)
        Me.ploptStartrework.TabIndex = 85
        Me.ploptStartrework.Visible = False
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.Black
        Me.Label21.Location = New System.Drawing.Point(80, 63)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(55, 16)
        Me.Label21.TabIndex = 51
        Me.Label21.Text = "- PRINT"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.Black
        Me.Label22.Location = New System.Drawing.Point(80, 39)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(60, 16)
        Me.Label22.TabIndex = 53
        Me.Label22.Text = "- PAUSE"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.Black
        Me.Label23.Location = New System.Drawing.Point(13, 135)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(48, 16)
        Me.Label23.TabIndex = 52
        Me.Label23.Text = "- VOID"
        Me.Label23.Visible = False
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.Black
        Me.Label25.Location = New System.Drawing.Point(3, 2)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(58, 16)
        Me.Label25.TabIndex = 50
        Me.Label25.Text = "OPTION"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.Black
        Me.Label26.Location = New System.Drawing.Point(80, 15)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(87, 16)
        Me.Label26.TabIndex = 48
        Me.Label26.Text = "- COMPLETE"
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Olive
        Me.Panel6.Controls.Add(Me.lblStatus)
        Me.Panel6.Location = New System.Drawing.Point(146, 141)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(517, 403)
        Me.Panel6.TabIndex = 141
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.Red
        Me.lblStatus.Location = New System.Drawing.Point(360, 253)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(0, 13)
        Me.lblStatus.TabIndex = 27
        '
        'TimerMachine
        '
        Me.TimerMachine.Enabled = True
        Me.TimerMachine.Interval = 1000
        '
        'PrintDialog1
        '
        Me.PrintDialog1.Document = Me.PrintDocument1
        '
        'frmScanJob
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(866, 730)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmScanJob"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.PPrint.ResumeLayout(False)
        Me.PPrint.PerformLayout()
        Me.ploptStart.ResumeLayout(False)
        Me.ploptStart.PerformLayout()
        Me.ploptnew.ResumeLayout(False)
        Me.ploptnew.PerformLayout()
        Me.ploptTrans.ResumeLayout(False)
        Me.ploptTrans.PerformLayout()
        Me.ploptResume.ResumeLayout(False)
        Me.ploptResume.PerformLayout()
        Me.ploptStartrework.ResumeLayout(False)
        Me.ploptStartrework.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtMain As System.Windows.Forms.TextBox
    Friend WithEvents txtAction As System.Windows.Forms.Label
    Friend WithEvents txtJobno As System.Windows.Forms.Label
    Friend WithEvents pic1 As System.Windows.Forms.PictureBox
    Friend WithEvents ploptStart As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ploptnew As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ploptTrans As System.Windows.Forms.Panel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents ploptResume As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtsuf As System.Windows.Forms.Label
    Friend WithEvents lbl As System.Windows.Forms.Label
    Friend WithEvents lblMsgbox As System.Windows.Forms.Label
    Friend WithEvents TimerMachine As System.Windows.Forms.Timer
    Friend WithEvents txtParent As System.Windows.Forms.Label
    Friend WithEvents txtid As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintDialog1 As System.Windows.Forms.PrintDialog
    Friend WithEvents PrintDocument2 As System.Drawing.Printing.PrintDocument
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtOpID As System.Windows.Forms.Label
    Friend WithEvents lblOpID As System.Windows.Forms.Label
    Friend WithEvents lblJobno As System.Windows.Forms.Label
    Friend WithEvents lblAction As System.Windows.Forms.Label
    Friend WithEvents PPrint As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents lblJ As System.Windows.Forms.Label
    Friend WithEvents txtRework As System.Windows.Forms.Label
    Friend WithEvents ploptStartrework As System.Windows.Forms.Panel
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txtWC As System.Windows.Forms.Label
    Friend WithEvents lblWC As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
End Class
