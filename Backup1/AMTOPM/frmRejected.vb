Imports System.Data.SqlClient
Public Class frmRejected
    Dim DS As New DataSet
    Dim clsM As New clsMain
    Dim NAllHASH As New Hashtable

    Private Sub frmRejected_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Click
        txtMain.Focus()
    End Sub
    Private Sub frmRejected_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtMain.Text = ""
        lblMsg.Text = ""

        With lstv
            .Columns.Add("Code", 100, HorizontalAlignment.Left)
            .Columns.Add("QTY", 100, HorizontalAlignment.Left)
            .Columns.Add("Desc.", lstv.Width - 201, HorizontalAlignment.Left)

        End With

        With lstvAll
            .Columns.Add("Row", 70, HorizontalAlignment.Left)
            .Columns.Add("Code", 100, HorizontalAlignment.Left)
            .Columns.Add("Desc.", lstv.Width - 171, HorizontalAlignment.Left)
        End With
        loadView()
        getReject()
        loadViewAll()
        txtMain.Select()
        txtMain.Focus()
        lblMsg.Text = "Scan row no.!"
    End Sub

    Sub getReject()
        Dim cnt As String = " and B._itemcode=''"
        DS = clsM.GetDataset("select A._RejID as _RejID,A._RejectedCode as _RejectedCode ,A._RejectedDesc as _RejectedDesc,B._ItemCode from tbRejected A inner join  tbRejectedGroup B on A._RejID=B._RejID where B._wc='" & fncstr(lblWc.Text) & "' GROUP BY B._ItemCode,A._RejID,A._RejectedCode,A._RejectedDesc", "tbReject")

    End Sub

    Private Sub txtMain_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMain.KeyPress
        If Asc(e.KeyChar) = 13 Then
            lblMsg.Text = ""
            If UCase(txtMain.Text) = "CLER" Then
                Newhash.Clear()
                lstv.Items.Clear()
                txttot.Text = 0.0
                txtMain.Text = ""
                txtMain.Focus()
                lblMsg.Text = "Scan row no.!"
                Exit Sub
            End If
            If UCase(txtMain.Text) = "CNCL" Then
                Newhash.Clear()
                lstv.Items.Clear()
                MsgBox("Total rejected quantity not tally!", MsgBoxStyle.Information, "eWIP")
                Me.Close()
                Exit Sub
            End If
            If UCase(txtMain.Text) = "SAVE" Then
                If Val(txtRejQty.Text) <> Val(txttot.Text) Then
                    MsgBox("Total rejected quantity not tally!", MsgBoxStyle.Information, "eWIP")
                    Me.Close()
                    Exit Sub
                Else
                    Me.Close()
                    Exit Sub
                End If

            End If
            If UCase(txtMain.Text) = "EXIT" Then
                If Val(txtRejQty.Text) <> Val(txttot.Text) Then
                    MsgBox("Total rejected quantity not tally!", MsgBoxStyle.Information, "eWIP")
                    Me.Close()
                    Exit Sub
                Else
                    Me.Close()
                    Exit Sub
                End If
            End If
        
            If Trim(txtMain.Text) <> "" Then

                Dim clsAll As New clsAllRegj
                Dim intI As Integer = Val(txtMain.Text)
                clsAll = NAllHASH(intI)
                Dim strFindCode As String = ""
                If clsAll Is Nothing = False Then
                    strFindCode = clsAll.RCode
                End If

                Dim iItem As clsRej



                Dim DR As DataRow() = DS.Tables(0).Select("_RejectedCode='" & fncstr(strFindCode) & "'")
                txtMain.Text = ""
                If DR.Length > 0 Then

                    ' If DR(0).Item("_itemCode") = Trim(lblItem.Text) Or DR(0).Item("_itemCode") = "" Then
                    Dim str As String = ""
                    Dim strCode As String = ""
                    strCode = DR(0).Item("_RejectedCode")
                    If Not (Newhash.ContainsKey(strCode)) Then
                        iItem = New clsRej
                        lblRejectCode.Text = DR(0).Item("_RejectedCode")
                        lblDesc.Text = DR(0).Item("_RejectedDesc")
                        txtKeyReject.Text = ""
                        txtKeyReject.Visible = True
                        txtKeyReject.Focus()
                        lblMsg.Text = "Enter rejected quantity!"
                    Else
                        Dim irej As clsRej = Newhash(DR(0).Item("_RejectedCode"))
                        lblRejectCode.Text = DR(0).Item("_RejectedCode")
                        lblDesc.Text = DR(0).Item("_RejectedDesc")
                        txtKeyReject.Text = irej.rQty
                        txtKeyReject.Visible = True
                        txtKeyReject.Focus()
                        lblMsg.Text = "Enter rejected quantity!"
                    End If

                    'Else
                    '    MsgBox("Invalid row no.!", MsgBoxStyle.Information, "eWIP")
                    '    If Val(txtRejQty.Text) <> Val(txttot.Text) Then
                    '        lblMsg.Text = "Scan row no.!"
                    '    Else
                    '        lblMsg.Text = "Scan 'SAVE' or 'CANCEL'!"
                    '    End If
                    '    txtMain.Text = ""
                    'End If
                Else

                    MsgBox("Invalid row no.!", MsgBoxStyle.Information, "eWIP")
                    If Val(txtRejQty.Text) <> Val(txttot.Text) Then
                        lblMsg.Text = "Scan row no.!"
                    Else
                        lblMsg.Text = "Scan 'SAVE' or 'CANCEL'!"
                    End If
                    txtMain.Text = ""
                End If
            End If



        End If
    End Sub

    Private Sub txtKeyReject_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtKeyReject.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Val(txtKeyReject.Text) = 0 Then
                Dim strCode As String = ""
                strCode = lblRejectCode.Text
                If (Newhash.ContainsKey(strCode)) Then
                    Newhash.Remove(strCode)
                    lblRejectCode.Text = ""
                    lblDesc.Text = ""
                    txtKeyReject.Text = ""
                    txtKeyReject.Visible = False
                    txtMain.Focus()
                    loadView()
                Else
                    lblRejectCode.Text = ""
                    lblDesc.Text = ""
                    txtKeyReject.Text = ""
                    txtKeyReject.Visible = False
                    txtMain.Focus()
                    loadView()
                End If
            Else

                Dim strCode As String = ""
                strCode = lblRejectCode.Text
                If (Newhash.ContainsKey(strCode)) Then
                    Newhash.Remove(strCode)
                    Dim clsI As New clsRej
                    clsI.Rcode = lblRejectCode.Text
                    clsI.Desc = lblDesc.Text
                    clsI.rQty = Val(txtKeyReject.Text)
                    Newhash.Add(strCode, clsI)
                    lblRejectCode.Text = ""
                    lblDesc.Text = ""
                    txtKeyReject.Text = ""
                    txtKeyReject.Visible = False
                    txtMain.Focus()
                    loadView()
                Else
                    Dim clsI As New clsRej
                    clsI.Rcode = lblRejectCode.Text
                    clsI.Desc = lblDesc.Text
                    clsI.rQty = Val(txtKeyReject.Text)
                    Newhash.Add(strCode, clsI)
                    lblRejectCode.Text = ""
                    lblDesc.Text = ""
                    txtKeyReject.Text = ""
                    txtKeyReject.Visible = False
                    txtMain.Focus()
                    loadView()
                End If

             



            End If
            If Val(txtRejQty.Text) <> Val(txttot.Text) Then
                lblMsg.Text = "Scan row no.!"
            Else
                lblMsg.Text = "Scan 'SAVE' or 'CANCEL'!"
            End If
        End If

        If (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57) Or Asc(e.KeyChar) = 46 Or Asc(e.KeyChar) = 8 Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub
    Sub loadView()
        lstv.Items.Clear()
        txttot.Text = 0.0
        If Newhash.Count > 0 Then
            Dim irej As clsRej
            For Each irej In Newhash.Values
                Dim ls As New ListViewItem(Trim(irej.Rcode))    ' you can also use reader.GetSqlValue(0) 
                ls.SubItems.Add(Trim(irej.rQty))
                ls.SubItems.Add(Trim(irej.Desc))

                lstv.Items.Add(ls)
                txttot.Text = Val(txttot.Text) + Val(irej.rQty)
            Next
        End If
    End Sub
    'Sub loadViewAll()
    '    lstvAll.Items.Clear()
    '    Dim j As Integer
    '    If DS.Tables(0).Rows.Count > 0 Then
    '        For j = 0 To DS.Tables(0).Rows.Count - 1
    '            With DS.Tables(0).Rows(j)
    '                Dim ls As New ListViewItem(CStr(j + 1))    ' you can also use reader.GetSqlValue(0) 
    '                ls.SubItems.Add(Trim(.Item("_RejectedCode")))
    '                ls.SubItems.Add(Trim(.Item("_RejectedDesc")))
    '                lstvAll.Items.Add(ls)
    '                If (NAllHASH.ContainsKey(j + 1)) = False Then
    '                    Dim clsI As New clsAllRegj
    '                    clsI.id = j + 1
    '                    clsI.RCode = .Item("_RejectedCode")
    '                    clsI.RDesc = .Item("_RejectedDesc")
    '                    NAllHASH.Add(j + 1, clsI)
    '                End If



    '            End With
    '        Next
    '    End If

    'End Sub
    Sub loadViewAll()
        lstvAll.Items.Clear()
        Dim j As Integer
        If DS.Tables(0).Rows.Count > 0 Then
            Dim DrC As DataRow() = DS.Tables(0).Select("_ItemCode='" & Trim(lblItem.Text) & "'")
            If DrC.Length > 0 Then
                For j = 0 To DrC.Length - 1
                    With DrC(j)
                        Dim ls As New ListViewItem(CStr(j + 1))    ' you can also use reader.GetSqlValue(0) 
                        ls.SubItems.Add(Trim(.Item("_RejectedCode")))
                        ls.SubItems.Add(Trim(.Item("_RejectedDesc")))
                        lstvAll.Items.Add(ls)
                        If (NAllHASH.ContainsKey(j + 1)) = False Then
                            Dim clsI As New clsAllRegj
                            clsI.id = j + 1
                            clsI.RCode = .Item("_RejectedCode")
                            clsI.RDesc = .Item("_RejectedDesc")
                            NAllHASH.Add(j + 1, clsI)
                        End If



                    End With
                Next

            Else
                Dim DrAll As DataRow() = DS.Tables(0).Select("_ItemCode=''")

                If DrAll.Length > 0 Then
                    For j = 0 To DrAll.Length - 1
                        With DrAll(j)
                            Dim ls As New ListViewItem(CStr(j + 1))    ' you can also use reader.GetSqlValue(0) 
                            ls.SubItems.Add(Trim(.Item("_RejectedCode")))
                            ls.SubItems.Add(Trim(.Item("_RejectedDesc")))
                            lstvAll.Items.Add(ls)
                            If (NAllHASH.ContainsKey(j + 1)) = False Then
                                Dim clsI As New clsAllRegj
                                clsI.id = j + 1
                                clsI.RCode = .Item("_RejectedCode")
                                clsI.RDesc = .Item("_RejectedDesc")
                                NAllHASH.Add(j + 1, clsI)
                            End If



                        End With
                    Next

                End If
            End If

        End If

    End Sub
    Private Sub txtKeyReject_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKeyReject.TextChanged

    End Sub


    Class clsAllRegj
        Public id As Integer
        Public RCode As String
        Public RDesc As String
    End Class

    Private Sub lstvAll_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstvAll.GotFocus
        txtMain.Focus()
    End Sub

    Private Sub lstvAll_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstvAll.LostFocus
        txtMain.Focus()
    End Sub

    Private Sub lstv_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.GotFocus
        txtMain.Focus()
    End Sub

    Private Sub lstv_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.LostFocus
        txtMain.Focus()
    End Sub


    
    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged
        txtMain.Focus()
    End Sub

    Private Sub lstvAll_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstvAll.SelectedIndexChanged
        txtMain.Focus()
    End Sub

    Private Sub TimerMachine_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerMachine.Tick
        If pic2.Visible = False Then
            pic2.Visible = True
        Else
            pic2.Visible = False
        End If
    End Sub
End Class