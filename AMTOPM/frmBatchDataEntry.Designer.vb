﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBatchDataEntry
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBatchDataEntry))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.machineSelectInput = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.selectedJobGridList = New System.Windows.Forms.DataGridView()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.jobnumbersaved = New System.Windows.Forms.TextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.pnlRemarks = New System.Windows.Forms.Panel()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.lbl234 = New System.Windows.Forms.Label()
        Me.txtRemarks = New System.Windows.Forms.TextBox()
        Me.pic2 = New System.Windows.Forms.PictureBox()
        Me.txtOrgRemarks = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblJAC = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.lblTot = New System.Windows.Forms.Label()
        Me.txtTot = New System.Windows.Forms.Label()
        Me.lstv = New System.Windows.Forms.ListView()
        Me.lblOR = New System.Windows.Forms.Label()
        Me.lblCRQ = New System.Windows.Forms.Label()
        Me.txtOPid = New System.Windows.Forms.Label()
        Me.txtID = New System.Windows.Forms.Label()
        Me.fltStartDate = New System.Windows.Forms.Label()
        Me.txtParent = New System.Windows.Forms.Label()
        Me.lblSave = New System.Windows.Forms.Label()
        Me.txtsuf = New System.Windows.Forms.Label()
        Me.lbl = New System.Windows.Forms.Label()
        Me.txtJobno = New System.Windows.Forms.Label()
        Me.txtAction = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtComQty = New System.Windows.Forms.Label()
        Me.lblRlQ = New System.Windows.Forms.Label()
        Me.txtEdate = New System.Windows.Forms.Label()
        Me.txtReQ = New System.Windows.Forms.Label()
        Me.txtRlQ = New System.Windows.Forms.Label()
        Me.lblReQ = New System.Windows.Forms.Label()
        Me.txtStartDate = New System.Windows.Forms.Label()
        Me.txtRejQ = New System.Windows.Forms.Label()
        Me.lblEdate = New System.Windows.Forms.Label()
        Me.lblStartDate = New System.Windows.Forms.Label()
        Me.lblComqty = New System.Windows.Forms.Label()
        Me.lblRejQ = New System.Windows.Forms.Label()
        Me.txtMain = New System.Windows.Forms.TextBox()
        Me.lblMsgbox = New System.Windows.Forms.Label()
        Me.pic1 = New System.Windows.Forms.PictureBox()
        Me.lblAction = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtnoofOP = New System.Windows.Forms.Label()
        Me.lblnoofOP = New System.Windows.Forms.Label()
        Me.txtName = New System.Windows.Forms.Label()
        Me.txtOperatorid = New System.Windows.Forms.Label()
        Me.txtMachineId = New System.Windows.Forms.Label()
        Me.txtWorkcenter = New System.Windows.Forms.Label()
        Me.txtOperationno = New System.Windows.Forms.Label()
        Me.txtItem = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblOperatorid = New System.Windows.Forms.Label()
        Me.lblMachineId = New System.Windows.Forms.Label()
        Me.lblOperationno = New System.Windows.Forms.Label()
        Me.lblWorkcenter = New System.Windows.Forms.Label()
        Me.lblItem = New System.Windows.Forms.Label()
        Me.lblJobno = New System.Windows.Forms.Label()
        Me.pic78 = New System.Windows.Forms.PictureBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.lstvData = New System.Windows.Forms.ListView()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.pnlcom = New System.Windows.Forms.Panel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtKeyComqty = New System.Windows.Forms.TextBox()
        Me.pnlnoofOp = New System.Windows.Forms.Panel()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtKeynoofOp = New System.Windows.Forms.TextBox()
        Me.pnlReject = New System.Windows.Forms.Panel()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtKeyRejqty = New System.Windows.Forms.TextBox()
        Me.TimerMachine = New System.Windows.Forms.Timer(Me.components)
        Me.selected = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.transnum = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.jobnumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.suffix = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.numberofoperators = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CompQty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RejQty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RejectReason = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        Me.Panel7.SuspendLayout()
        CType(Me.selectedJobGridList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlRemarks.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.pic78, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        Me.pnlcom.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlnoofOp.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlReject.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel1.BackColor = System.Drawing.SystemColors.Info
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.Panel7)
        Me.Panel1.Controls.Add(Me.selectedJobGridList)
        Me.Panel1.Controls.Add(Me.Panel5)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.pnlRemarks)
        Me.Panel1.Controls.Add(Me.pic2)
        Me.Panel1.Controls.Add(Me.txtOrgRemarks)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.lblJAC)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Controls.Add(Me.lblOR)
        Me.Panel1.Controls.Add(Me.lblCRQ)
        Me.Panel1.Controls.Add(Me.txtOPid)
        Me.Panel1.Controls.Add(Me.txtID)
        Me.Panel1.Controls.Add(Me.fltStartDate)
        Me.Panel1.Controls.Add(Me.txtParent)
        Me.Panel1.Controls.Add(Me.lblSave)
        Me.Panel1.Controls.Add(Me.txtsuf)
        Me.Panel1.Controls.Add(Me.lbl)
        Me.Panel1.Controls.Add(Me.txtJobno)
        Me.Panel1.Controls.Add(Me.txtAction)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.txtMain)
        Me.Panel1.Controls.Add(Me.lblMsgbox)
        Me.Panel1.Controls.Add(Me.pic1)
        Me.Panel1.Controls.Add(Me.lblAction)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.lblJobno)
        Me.Panel1.Controls.Add(Me.pic78)
        Me.Panel1.Controls.Add(Me.Panel6)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.pnlcom)
        Me.Panel1.Controls.Add(Me.pnlnoofOp)
        Me.Panel1.Controls.Add(Me.pnlReject)
        Me.Panel1.Location = New System.Drawing.Point(12, 8)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(771, 579)
        Me.Panel1.TabIndex = 1
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Beige
        Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel7.Controls.Add(Me.machineSelectInput)
        Me.Panel7.Controls.Add(Me.Label7)
        Me.Panel7.Location = New System.Drawing.Point(100, 48)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(200, 66)
        Me.Panel7.TabIndex = 153
        Me.Panel7.Visible = False
        '
        'machineSelectInput
        '
        Me.machineSelectInput.Location = New System.Drawing.Point(14, 27)
        Me.machineSelectInput.Name = "machineSelectInput"
        Me.machineSelectInput.Size = New System.Drawing.Size(100, 20)
        Me.machineSelectInput.TabIndex = 154
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(11, 6)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(87, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "MACHINE No."
        '
        'selectedJobGridList
        '
        Me.selectedJobGridList.AllowUserToAddRows = False
        Me.selectedJobGridList.AllowUserToDeleteRows = False
        Me.selectedJobGridList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.selectedJobGridList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.selected, Me.transnum, Me.jobnumber, Me.suffix, Me.numberofoperators, Me.CompQty, Me.RejQty, Me.RejectReason})
        Me.selectedJobGridList.Location = New System.Drawing.Point(13, 218)
        Me.selectedJobGridList.Name = "selectedJobGridList"
        Me.selectedJobGridList.Size = New System.Drawing.Size(243, 150)
        Me.selectedJobGridList.TabIndex = 152
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Beige
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.jobnumbersaved)
        Me.Panel5.Controls.Add(Me.PictureBox1)
        Me.Panel5.Controls.Add(Me.Label6)
        Me.Panel5.Location = New System.Drawing.Point(19, 54)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(215, 65)
        Me.Panel5.TabIndex = 151
        '
        'jobnumbersaved
        '
        Me.jobnumbersaved.Location = New System.Drawing.Point(11, 32)
        Me.jobnumbersaved.Name = "jobnumbersaved"
        Me.jobnumbersaved.Size = New System.Drawing.Size(100, 20)
        Me.jobnumbersaved.TabIndex = 153
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(192, 31)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(22, 20)
        Me.PictureBox1.TabIndex = 129
        Me.PictureBox1.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(10, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(51, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Job No:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(493, 383)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(24, 13)
        Me.Label5.TabIndex = 149
        Me.Label5.Text = "test"
        '
        'pnlRemarks
        '
        Me.pnlRemarks.BackColor = System.Drawing.Color.Beige
        Me.pnlRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlRemarks.Controls.Add(Me.PictureBox2)
        Me.pnlRemarks.Controls.Add(Me.lbl234)
        Me.pnlRemarks.Controls.Add(Me.txtRemarks)
        Me.pnlRemarks.Location = New System.Drawing.Point(20, 58)
        Me.pnlRemarks.Name = "pnlRemarks"
        Me.pnlRemarks.Size = New System.Drawing.Size(211, 61)
        Me.pnlRemarks.TabIndex = 145
        Me.pnlRemarks.Visible = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(191, 23)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(22, 20)
        Me.PictureBox2.TabIndex = 128
        Me.PictureBox2.TabStop = False
        '
        'lbl234
        '
        Me.lbl234.AutoSize = True
        Me.lbl234.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl234.Location = New System.Drawing.Point(8, 11)
        Me.lbl234.Name = "lbl234"
        Me.lbl234.Size = New System.Drawing.Size(75, 13)
        Me.lbl234.TabIndex = 57
        Me.lbl234.Text = "REMARKS :"
        '
        'txtRemarks
        '
        Me.txtRemarks.Location = New System.Drawing.Point(4, 27)
        Me.txtRemarks.MaxLength = 50
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.Size = New System.Drawing.Size(181, 20)
        Me.txtRemarks.TabIndex = 56
        '
        'pic2
        '
        Me.pic2.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.pic2.Image = CType(resources.GetObject("pic2.Image"), System.Drawing.Image)
        Me.pic2.Location = New System.Drawing.Point(15, 142)
        Me.pic2.Name = "pic2"
        Me.pic2.Size = New System.Drawing.Size(25, 26)
        Me.pic2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic2.TabIndex = 148
        Me.pic2.TabStop = False
        '
        'txtOrgRemarks
        '
        Me.txtOrgRemarks.BackColor = System.Drawing.SystemColors.Info
        Me.txtOrgRemarks.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtOrgRemarks.Location = New System.Drawing.Point(379, 375)
        Me.txtOrgRemarks.MaxLength = 50
        Me.txtOrgRemarks.Multiline = True
        Me.txtOrgRemarks.Name = "txtOrgRemarks"
        Me.txtOrgRemarks.ReadOnly = True
        Me.txtOrgRemarks.Size = New System.Drawing.Size(380, 28)
        Me.txtOrgRemarks.TabIndex = 147
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(306, 375)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 13)
        Me.Label4.TabIndex = 146
        Me.Label4.Text = "REMARKS :"
        '
        'lblJAC
        '
        Me.lblJAC.AllowDrop = True
        Me.lblJAC.AutoSize = True
        Me.lblJAC.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJAC.ForeColor = System.Drawing.Color.Black
        Me.lblJAC.Location = New System.Drawing.Point(83, 10)
        Me.lblJAC.Name = "lblJAC"
        Me.lblJAC.Size = New System.Drawing.Size(0, 16)
        Me.lblJAC.TabIndex = 144
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Beige
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.lblTot)
        Me.Panel4.Controls.Add(Me.txtTot)
        Me.Panel4.Controls.Add(Me.lstv)
        Me.Panel4.Location = New System.Drawing.Point(610, 5)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(153, 363)
        Me.Panel4.TabIndex = 55
        '
        'lblTot
        '
        Me.lblTot.AutoSize = True
        Me.lblTot.Location = New System.Drawing.Point(3, 342)
        Me.lblTot.Name = "lblTot"
        Me.lblTot.Size = New System.Drawing.Size(37, 13)
        Me.lblTot.TabIndex = 95
        Me.lblTot.Text = "Total :"
        '
        'txtTot
        '
        Me.txtTot.AutoSize = True
        Me.txtTot.Location = New System.Drawing.Point(43, 342)
        Me.txtTot.Name = "txtTot"
        Me.txtTot.Size = New System.Drawing.Size(0, 13)
        Me.txtTot.TabIndex = 94
        '
        'lstv
        '
        Me.lstv.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.lstv.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstv.FullRowSelect = True
        Me.lstv.GridLines = True
        Me.lstv.Location = New System.Drawing.Point(3, 0)
        Me.lstv.Name = "lstv"
        Me.lstv.Size = New System.Drawing.Size(145, 333)
        Me.lstv.TabIndex = 93
        Me.lstv.UseCompatibleStateImageBehavior = False
        Me.lstv.View = System.Windows.Forms.View.Details
        '
        'lblOR
        '
        Me.lblOR.AutoSize = True
        Me.lblOR.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOR.Location = New System.Drawing.Point(163, 375)
        Me.lblOR.Name = "lblOR"
        Me.lblOR.Size = New System.Drawing.Size(26, 13)
        Me.lblOR.TabIndex = 94
        Me.lblOR.Text = "(or)"
        Me.lblOR.Visible = False
        '
        'lblCRQ
        '
        Me.lblCRQ.AutoSize = True
        Me.lblCRQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCRQ.Location = New System.Drawing.Point(183, 375)
        Me.lblCRQ.Name = "lblCRQ"
        Me.lblCRQ.Size = New System.Drawing.Size(73, 13)
        Me.lblCRQ.TabIndex = 93
        Me.lblCRQ.Text = " CQ (or) RQ"
        Me.lblCRQ.Visible = False
        '
        'txtOPid
        '
        Me.txtOPid.AutoSize = True
        Me.txtOPid.Location = New System.Drawing.Point(630, 325)
        Me.txtOPid.Name = "txtOPid"
        Me.txtOPid.Size = New System.Drawing.Size(0, 13)
        Me.txtOPid.TabIndex = 89
        Me.txtOPid.Visible = False
        '
        'txtID
        '
        Me.txtID.AutoSize = True
        Me.txtID.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtID.ForeColor = System.Drawing.Color.Black
        Me.txtID.Location = New System.Drawing.Point(701, 387)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(0, 19)
        Me.txtID.TabIndex = 90
        Me.txtID.Visible = False
        '
        'fltStartDate
        '
        Me.fltStartDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fltStartDate.Location = New System.Drawing.Point(526, 392)
        Me.fltStartDate.Name = "fltStartDate"
        Me.fltStartDate.Size = New System.Drawing.Size(149, 13)
        Me.fltStartDate.TabIndex = 87
        Me.fltStartDate.Text = " "
        Me.fltStartDate.Visible = False
        '
        'txtParent
        '
        Me.txtParent.AutoSize = True
        Me.txtParent.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtParent.ForeColor = System.Drawing.Color.Black
        Me.txtParent.Location = New System.Drawing.Point(613, 391)
        Me.txtParent.Name = "txtParent"
        Me.txtParent.Size = New System.Drawing.Size(0, 19)
        Me.txtParent.TabIndex = 86
        Me.txtParent.Visible = False
        '
        'lblSave
        '
        Me.lblSave.AutoSize = True
        Me.lblSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSave.Location = New System.Drawing.Point(128, 375)
        Me.lblSave.Name = "lblSave"
        Me.lblSave.Size = New System.Drawing.Size(39, 13)
        Me.lblSave.TabIndex = 85
        Me.lblSave.Text = "SAVE"
        Me.lblSave.Visible = False
        '
        'txtsuf
        '
        Me.txtsuf.AutoSize = True
        Me.txtsuf.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsuf.ForeColor = System.Drawing.Color.Black
        Me.txtsuf.Location = New System.Drawing.Point(237, 60)
        Me.txtsuf.Name = "txtsuf"
        Me.txtsuf.Size = New System.Drawing.Size(0, 19)
        Me.txtsuf.TabIndex = 84
        Me.txtsuf.Visible = False
        '
        'lbl
        '
        Me.lbl.AutoSize = True
        Me.lbl.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl.ForeColor = System.Drawing.Color.Black
        Me.lbl.Location = New System.Drawing.Point(222, 58)
        Me.lbl.Name = "lbl"
        Me.lbl.Size = New System.Drawing.Size(0, 22)
        Me.lbl.TabIndex = 83
        Me.lbl.Visible = False
        '
        'txtJobno
        '
        Me.txtJobno.AutoSize = True
        Me.txtJobno.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobno.ForeColor = System.Drawing.Color.Black
        Me.txtJobno.Location = New System.Drawing.Point(254, 57)
        Me.txtJobno.Name = "txtJobno"
        Me.txtJobno.Size = New System.Drawing.Size(0, 19)
        Me.txtJobno.TabIndex = 82
        Me.txtJobno.Visible = False
        '
        'txtAction
        '
        Me.txtAction.AutoSize = True
        Me.txtAction.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAction.ForeColor = System.Drawing.Color.Black
        Me.txtAction.Location = New System.Drawing.Point(88, 35)
        Me.txtAction.Name = "txtAction"
        Me.txtAction.Size = New System.Drawing.Size(12, 16)
        Me.txtAction.TabIndex = 70
        Me.txtAction.Text = " "
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Beige
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.txtComQty)
        Me.Panel3.Controls.Add(Me.lblRlQ)
        Me.Panel3.Controls.Add(Me.txtEdate)
        Me.Panel3.Controls.Add(Me.txtReQ)
        Me.Panel3.Controls.Add(Me.txtRlQ)
        Me.Panel3.Controls.Add(Me.lblReQ)
        Me.Panel3.Controls.Add(Me.txtStartDate)
        Me.Panel3.Controls.Add(Me.txtRejQ)
        Me.Panel3.Controls.Add(Me.lblEdate)
        Me.Panel3.Controls.Add(Me.lblStartDate)
        Me.Panel3.Controls.Add(Me.lblComqty)
        Me.Panel3.Controls.Add(Me.lblRejQ)
        Me.Panel3.Location = New System.Drawing.Point(303, 221)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(301, 147)
        Me.Panel3.TabIndex = 69
        '
        'txtComQty
        '
        Me.txtComQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComQty.Location = New System.Drawing.Point(149, 73)
        Me.txtComQty.Name = "txtComQty"
        Me.txtComQty.Size = New System.Drawing.Size(147, 13)
        Me.txtComQty.TabIndex = 69
        Me.txtComQty.Text = " "
        '
        'lblRlQ
        '
        Me.lblRlQ.AutoSize = True
        Me.lblRlQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRlQ.Location = New System.Drawing.Point(3, 4)
        Me.lblRlQ.Name = "lblRlQ"
        Me.lblRlQ.Size = New System.Drawing.Size(147, 13)
        Me.lblRlQ.TabIndex = 67
        Me.lblRlQ.Text = "RELEASED QUANTITY :"
        '
        'txtEdate
        '
        Me.txtEdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEdate.Location = New System.Drawing.Point(149, 116)
        Me.txtEdate.Name = "txtEdate"
        Me.txtEdate.Size = New System.Drawing.Size(147, 13)
        Me.txtEdate.TabIndex = 68
        '
        'txtReQ
        '
        Me.txtReQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReQ.Location = New System.Drawing.Point(149, 27)
        Me.txtReQ.Name = "txtReQ"
        Me.txtReQ.Size = New System.Drawing.Size(147, 13)
        Me.txtReQ.TabIndex = 65
        Me.txtReQ.Text = " "
        '
        'txtRlQ
        '
        Me.txtRlQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRlQ.Location = New System.Drawing.Point(149, 4)
        Me.txtRlQ.Name = "txtRlQ"
        Me.txtRlQ.Size = New System.Drawing.Size(147, 13)
        Me.txtRlQ.TabIndex = 61
        Me.txtRlQ.Text = " "
        '
        'lblReQ
        '
        Me.lblReQ.AutoSize = True
        Me.lblReQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReQ.Location = New System.Drawing.Point(3, 27)
        Me.lblReQ.Name = "lblReQ"
        Me.lblReQ.Size = New System.Drawing.Size(106, 13)
        Me.lblReQ.TabIndex = 46
        Me.lblReQ.Text = "RECEIVED QTY :"
        '
        'txtStartDate
        '
        Me.txtStartDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStartDate.Location = New System.Drawing.Point(149, 50)
        Me.txtStartDate.Name = "txtStartDate"
        Me.txtStartDate.Size = New System.Drawing.Size(147, 13)
        Me.txtStartDate.TabIndex = 66
        Me.txtStartDate.Text = " "
        '
        'txtRejQ
        '
        Me.txtRejQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRejQ.Location = New System.Drawing.Point(149, 96)
        Me.txtRejQ.Name = "txtRejQ"
        Me.txtRejQ.Size = New System.Drawing.Size(147, 13)
        Me.txtRejQ.TabIndex = 67
        Me.txtRejQ.Text = " "
        '
        'lblEdate
        '
        Me.lblEdate.AutoSize = True
        Me.lblEdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEdate.Location = New System.Drawing.Point(3, 119)
        Me.lblEdate.Name = "lblEdate"
        Me.lblEdate.Size = New System.Drawing.Size(78, 13)
        Me.lblEdate.TabIndex = 56
        Me.lblEdate.Text = "END DATE :"
        '
        'lblStartDate
        '
        Me.lblStartDate.AutoSize = True
        Me.lblStartDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(3, 50)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(93, 13)
        Me.lblStartDate.TabIndex = 54
        Me.lblStartDate.Text = "START DATE :"
        '
        'lblComqty
        '
        Me.lblComqty.AutoSize = True
        Me.lblComqty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComqty.Location = New System.Drawing.Point(3, 73)
        Me.lblComqty.Name = "lblComqty"
        Me.lblComqty.Size = New System.Drawing.Size(119, 13)
        Me.lblComqty.TabIndex = 55
        Me.lblComqty.Text = "COMPLETED QTY :"
        '
        'lblRejQ
        '
        Me.lblRejQ.AutoSize = True
        Me.lblRejQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRejQ.Location = New System.Drawing.Point(3, 96)
        Me.lblRejQ.Name = "lblRejQ"
        Me.lblRejQ.Size = New System.Drawing.Size(108, 13)
        Me.lblRejQ.TabIndex = 57
        Me.lblRejQ.Text = "REJECTED QTY :"
        '
        'txtMain
        '
        Me.txtMain.Location = New System.Drawing.Point(46, 58)
        Me.txtMain.Name = "txtMain"
        Me.txtMain.Size = New System.Drawing.Size(0, 20)
        Me.txtMain.TabIndex = 55
        '
        'lblMsgbox
        '
        Me.lblMsgbox.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(173, Byte), Integer))
        Me.lblMsgbox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsgbox.ForeColor = System.Drawing.Color.DarkRed
        Me.lblMsgbox.Location = New System.Drawing.Point(42, 135)
        Me.lblMsgbox.Name = "lblMsgbox"
        Me.lblMsgbox.Size = New System.Drawing.Size(212, 83)
        Me.lblMsgbox.TabIndex = 52
        Me.lblMsgbox.Text = "Machine ID :"
        Me.lblMsgbox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pic1
        '
        Me.pic1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.pic1.Image = Global.AMTOPM.My.Resources.Resources.Arrow1
        Me.pic1.Location = New System.Drawing.Point(278, 150)
        Me.pic1.Name = "pic1"
        Me.pic1.Size = New System.Drawing.Size(22, 22)
        Me.pic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic1.TabIndex = 51
        Me.pic1.TabStop = False
        '
        'lblAction
        '
        Me.lblAction.AutoSize = True
        Me.lblAction.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAction.ForeColor = System.Drawing.Color.Black
        Me.lblAction.Location = New System.Drawing.Point(17, 35)
        Me.lblAction.Name = "lblAction"
        Me.lblAction.Size = New System.Drawing.Size(65, 16)
        Me.lblAction.TabIndex = 47
        Me.lblAction.Text = "ACTION :"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Beige
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.txtnoofOP)
        Me.Panel2.Controls.Add(Me.lblnoofOP)
        Me.Panel2.Controls.Add(Me.txtName)
        Me.Panel2.Controls.Add(Me.txtOperatorid)
        Me.Panel2.Controls.Add(Me.txtMachineId)
        Me.Panel2.Controls.Add(Me.txtWorkcenter)
        Me.Panel2.Controls.Add(Me.txtOperationno)
        Me.Panel2.Controls.Add(Me.txtItem)
        Me.Panel2.Controls.Add(Me.lblName)
        Me.Panel2.Controls.Add(Me.lblOperatorid)
        Me.Panel2.Controls.Add(Me.lblMachineId)
        Me.Panel2.Controls.Add(Me.lblOperationno)
        Me.Panel2.Controls.Add(Me.lblWorkcenter)
        Me.Panel2.Controls.Add(Me.lblItem)
        Me.Panel2.Location = New System.Drawing.Point(303, 2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(301, 212)
        Me.Panel2.TabIndex = 46
        '
        'txtnoofOP
        '
        Me.txtnoofOP.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnoofOP.Location = New System.Drawing.Point(128, 154)
        Me.txtnoofOP.Name = "txtnoofOP"
        Me.txtnoofOP.Size = New System.Drawing.Size(168, 13)
        Me.txtnoofOP.TabIndex = 66
        Me.txtnoofOP.Text = " "
        '
        'lblnoofOP
        '
        Me.lblnoofOP.AutoSize = True
        Me.lblnoofOP.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblnoofOP.Location = New System.Drawing.Point(1, 154)
        Me.lblnoofOP.Name = "lblnoofOP"
        Me.lblnoofOP.Size = New System.Drawing.Size(129, 13)
        Me.lblnoofOP.TabIndex = 65
        Me.lblnoofOP.Text = "NO. OF OPERATOR :"
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Location = New System.Drawing.Point(128, 123)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(168, 13)
        Me.txtName.TabIndex = 64
        Me.txtName.Text = " "
        '
        'txtOperatorid
        '
        Me.txtOperatorid.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOperatorid.Location = New System.Drawing.Point(10, 202)
        Me.txtOperatorid.Name = "txtOperatorid"
        Me.txtOperatorid.Size = New System.Drawing.Size(168, 13)
        Me.txtOperatorid.TabIndex = 63
        Me.txtOperatorid.Text = " "
        Me.txtOperatorid.Visible = False
        '
        'txtMachineId
        '
        Me.txtMachineId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMachineId.Location = New System.Drawing.Point(128, 93)
        Me.txtMachineId.Name = "txtMachineId"
        Me.txtMachineId.Size = New System.Drawing.Size(168, 13)
        Me.txtMachineId.TabIndex = 62
        Me.txtMachineId.Text = " "
        '
        'txtWorkcenter
        '
        Me.txtWorkcenter.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWorkcenter.Location = New System.Drawing.Point(128, 63)
        Me.txtWorkcenter.Name = "txtWorkcenter"
        Me.txtWorkcenter.Size = New System.Drawing.Size(168, 13)
        Me.txtWorkcenter.TabIndex = 60
        Me.txtWorkcenter.Text = " "
        '
        'txtOperationno
        '
        Me.txtOperationno.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOperationno.Location = New System.Drawing.Point(128, 33)
        Me.txtOperationno.Name = "txtOperationno"
        Me.txtOperationno.Size = New System.Drawing.Size(168, 13)
        Me.txtOperationno.TabIndex = 59
        Me.txtOperationno.Text = " "
        '
        'txtItem
        '
        Me.txtItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItem.Location = New System.Drawing.Point(128, 3)
        Me.txtItem.Name = "txtItem"
        Me.txtItem.Size = New System.Drawing.Size(168, 13)
        Me.txtItem.TabIndex = 58
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(2, 123)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(122, 13)
        Me.lblName.TabIndex = 53
        Me.lblName.Text = "OPERATOR NAME :"
        '
        'lblOperatorid
        '
        Me.lblOperatorid.AutoSize = True
        Me.lblOperatorid.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperatorid.Location = New System.Drawing.Point(184, 202)
        Me.lblOperatorid.Name = "lblOperatorid"
        Me.lblOperatorid.Size = New System.Drawing.Size(100, 13)
        Me.lblOperatorid.TabIndex = 52
        Me.lblOperatorid.Text = "OPERATOR ID :"
        Me.lblOperatorid.Visible = False
        '
        'lblMachineId
        '
        Me.lblMachineId.AutoSize = True
        Me.lblMachineId.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMachineId.Location = New System.Drawing.Point(2, 93)
        Me.lblMachineId.Name = "lblMachineId"
        Me.lblMachineId.Size = New System.Drawing.Size(88, 13)
        Me.lblMachineId.TabIndex = 51
        Me.lblMachineId.Text = "MACHINE ID :"
        '
        'lblOperationno
        '
        Me.lblOperationno.AutoSize = True
        Me.lblOperationno.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperationno.Location = New System.Drawing.Point(3, 33)
        Me.lblOperationno.Name = "lblOperationno"
        Me.lblOperationno.Size = New System.Drawing.Size(113, 13)
        Me.lblOperationno.TabIndex = 44
        Me.lblOperationno.Text = "OPERATION NO. :"
        '
        'lblWorkcenter
        '
        Me.lblWorkcenter.AutoSize = True
        Me.lblWorkcenter.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkcenter.Location = New System.Drawing.Point(3, 63)
        Me.lblWorkcenter.Name = "lblWorkcenter"
        Me.lblWorkcenter.Size = New System.Drawing.Size(107, 13)
        Me.lblWorkcenter.TabIndex = 45
        Me.lblWorkcenter.Text = "WORK CENTER :"
        '
        'lblItem
        '
        Me.lblItem.AutoSize = True
        Me.lblItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItem.Location = New System.Drawing.Point(3, 3)
        Me.lblItem.Name = "lblItem"
        Me.lblItem.Size = New System.Drawing.Size(45, 13)
        Me.lblItem.TabIndex = 42
        Me.lblItem.Text = "ITEM :"
        '
        'lblJobno
        '
        Me.lblJobno.AutoSize = True
        Me.lblJobno.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobno.ForeColor = System.Drawing.Color.Black
        Me.lblJobno.Location = New System.Drawing.Point(17, 10)
        Me.lblJobno.Name = "lblJobno"
        Me.lblJobno.Size = New System.Drawing.Size(65, 16)
        Me.lblJobno.TabIndex = 13
        Me.lblJobno.Text = "JOB NO :"
        '
        'pic78
        '
        Me.pic78.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.pic78.Image = CType(resources.GetObject("pic78.Image"), System.Drawing.Image)
        Me.pic78.Location = New System.Drawing.Point(13, 124)
        Me.pic78.Name = "pic78"
        Me.pic78.Size = New System.Drawing.Size(243, 94)
        Me.pic78.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic78.TabIndex = 97
        Me.pic78.TabStop = False
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Olive
        Me.Panel6.Controls.Add(Me.lblStatus)
        Me.Panel6.Controls.Add(Me.lstvData)
        Me.Panel6.Location = New System.Drawing.Point(-2, 409)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(768, 167)
        Me.Panel6.TabIndex = 142
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.Red
        Me.lblStatus.Location = New System.Drawing.Point(360, 253)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(0, 13)
        Me.lblStatus.TabIndex = 27
        '
        'lstvData
        '
        Me.lstvData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.lstvData.BackColor = System.Drawing.Color.White
        Me.lstvData.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstvData.FullRowSelect = True
        Me.lstvData.GridLines = True
        Me.lstvData.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lstvData.Location = New System.Drawing.Point(3, 7)
        Me.lstvData.Name = "lstvData"
        Me.lstvData.Size = New System.Drawing.Size(762, 151)
        Me.lstvData.TabIndex = 96
        Me.lstvData.TabStop = False
        Me.lstvData.UseCompatibleStateImageBehavior = False
        Me.lstvData.View = System.Windows.Forms.View.Details
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.Olive
        Me.Label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(3, 378)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(107, 36)
        Me.Label15.TabIndex = 143
        Me.Label15.Text = "JOB STATUS"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlcom
        '
        Me.pnlcom.BackColor = System.Drawing.Color.Beige
        Me.pnlcom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlcom.Controls.Add(Me.PictureBox3)
        Me.pnlcom.Controls.Add(Me.Label1)
        Me.pnlcom.Controls.Add(Me.txtKeyComqty)
        Me.pnlcom.Location = New System.Drawing.Point(20, 57)
        Me.pnlcom.Name = "pnlcom"
        Me.pnlcom.Size = New System.Drawing.Size(172, 61)
        Me.pnlcom.TabIndex = 91
        Me.pnlcom.Visible = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(140, 23)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(22, 20)
        Me.PictureBox3.TabIndex = 128
        Me.PictureBox3.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(119, 13)
        Me.Label1.TabIndex = 57
        Me.Label1.Text = "COMPLETED QTY :"
        '
        'txtKeyComqty
        '
        Me.txtKeyComqty.Location = New System.Drawing.Point(11, 27)
        Me.txtKeyComqty.Name = "txtKeyComqty"
        Me.txtKeyComqty.Size = New System.Drawing.Size(126, 20)
        Me.txtKeyComqty.TabIndex = 56
        '
        'pnlnoofOp
        '
        Me.pnlnoofOp.BackColor = System.Drawing.Color.DarkKhaki
        Me.pnlnoofOp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlnoofOp.Controls.Add(Me.PictureBox5)
        Me.pnlnoofOp.Controls.Add(Me.Label3)
        Me.pnlnoofOp.Controls.Add(Me.txtKeynoofOp)
        Me.pnlnoofOp.Location = New System.Drawing.Point(19, 57)
        Me.pnlnoofOp.Name = "pnlnoofOp"
        Me.pnlnoofOp.Size = New System.Drawing.Size(172, 61)
        Me.pnlnoofOp.TabIndex = 95
        Me.pnlnoofOp.Visible = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(140, 23)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(22, 20)
        Me.PictureBox5.TabIndex = 128
        Me.PictureBox5.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(8, 11)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(121, 13)
        Me.Label3.TabIndex = 57
        Me.Label3.Text = "NO. OF OPERATOR"
        '
        'txtKeynoofOp
        '
        Me.txtKeynoofOp.Location = New System.Drawing.Point(11, 27)
        Me.txtKeynoofOp.Name = "txtKeynoofOp"
        Me.txtKeynoofOp.Size = New System.Drawing.Size(126, 20)
        Me.txtKeynoofOp.TabIndex = 56
        '
        'pnlReject
        '
        Me.pnlReject.BackColor = System.Drawing.Color.Goldenrod
        Me.pnlReject.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlReject.Controls.Add(Me.PictureBox4)
        Me.pnlReject.Controls.Add(Me.Label2)
        Me.pnlReject.Controls.Add(Me.txtKeyRejqty)
        Me.pnlReject.Location = New System.Drawing.Point(19, 57)
        Me.pnlReject.Name = "pnlReject"
        Me.pnlReject.Size = New System.Drawing.Size(173, 61)
        Me.pnlReject.TabIndex = 92
        Me.pnlReject.Visible = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(142, 27)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(22, 20)
        Me.PictureBox4.TabIndex = 128
        Me.PictureBox4.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(7, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(108, 13)
        Me.Label2.TabIndex = 58
        Me.Label2.Text = "REJECTED QTY :"
        '
        'txtKeyRejqty
        '
        Me.txtKeyRejqty.Location = New System.Drawing.Point(10, 27)
        Me.txtKeyRejqty.Name = "txtKeyRejqty"
        Me.txtKeyRejqty.Size = New System.Drawing.Size(126, 20)
        Me.txtKeyRejqty.TabIndex = 56
        '
        'TimerMachine
        '
        Me.TimerMachine.Enabled = True
        Me.TimerMachine.Interval = 1000
        '
        'selected
        '
        Me.selected.HeaderText = "selected"
        Me.selected.Name = "selected"
        Me.selected.Visible = False
        '
        'transnum
        '
        Me.transnum.HeaderText = "transnum"
        Me.transnum.Name = "transnum"
        Me.transnum.Visible = False
        '
        'jobnumber
        '
        Me.jobnumber.HeaderText = "job number"
        Me.jobnumber.Name = "jobnumber"
        '
        'suffix
        '
        Me.suffix.HeaderText = "suffix"
        Me.suffix.Name = "suffix"
        '
        'numberofoperators
        '
        Me.numberofoperators.HeaderText = "No. of Operators"
        Me.numberofoperators.Name = "numberofoperators"
        '
        'CompQty
        '
        Me.CompQty.HeaderText = "Completed Qty"
        Me.CompQty.Name = "CompQty"
        '
        'RejQty
        '
        Me.RejQty.HeaderText = "RejectedQty"
        Me.RejQty.Name = "RejQty"
        '
        'RejectReason
        '
        Me.RejectReason.HeaderText = "Rejection Reason"
        Me.RejectReason.Name = "RejectReason"
        Me.RejectReason.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.RejectReason.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'frmBatchDataEntry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(795, 594)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmBatchDataEntry"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.selectedJobGridList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlRemarks.ResumeLayout(False)
        Me.pnlRemarks.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.pic78, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.pnlcom.ResumeLayout(False)
        Me.pnlcom.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlnoofOp.ResumeLayout(False)
        Me.pnlnoofOp.PerformLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlReject.ResumeLayout(False)
        Me.pnlReject.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents pnlRemarks As System.Windows.Forms.Panel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents lbl234 As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As System.Windows.Forms.TextBox
    Friend WithEvents pic2 As System.Windows.Forms.PictureBox
    Friend WithEvents txtOrgRemarks As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblJAC As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents lblTot As System.Windows.Forms.Label
    Friend WithEvents txtTot As System.Windows.Forms.Label
    Friend WithEvents lstv As System.Windows.Forms.ListView
    Friend WithEvents lblOR As System.Windows.Forms.Label
    Friend WithEvents lblCRQ As System.Windows.Forms.Label
    Friend WithEvents txtOPid As System.Windows.Forms.Label
    Friend WithEvents txtID As System.Windows.Forms.Label
    Friend WithEvents fltStartDate As System.Windows.Forms.Label
    Friend WithEvents txtParent As System.Windows.Forms.Label
    Friend WithEvents lblSave As System.Windows.Forms.Label
    Friend WithEvents txtsuf As System.Windows.Forms.Label
    Friend WithEvents lbl As System.Windows.Forms.Label
    Friend WithEvents txtJobno As System.Windows.Forms.Label
    Friend WithEvents txtAction As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtComQty As System.Windows.Forms.Label
    Friend WithEvents lblRlQ As System.Windows.Forms.Label
    Friend WithEvents txtEdate As System.Windows.Forms.Label
    Friend WithEvents txtReQ As System.Windows.Forms.Label
    Friend WithEvents txtRlQ As System.Windows.Forms.Label
    Friend WithEvents lblReQ As System.Windows.Forms.Label
    Friend WithEvents txtStartDate As System.Windows.Forms.Label
    Friend WithEvents txtRejQ As System.Windows.Forms.Label
    Friend WithEvents lblEdate As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents lblComqty As System.Windows.Forms.Label
    Friend WithEvents lblRejQ As System.Windows.Forms.Label
    Friend WithEvents txtMain As System.Windows.Forms.TextBox
    Friend WithEvents lblMsgbox As System.Windows.Forms.Label
    Friend WithEvents pic1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblAction As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtnoofOP As System.Windows.Forms.Label
    Friend WithEvents lblnoofOP As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.Label
    Friend WithEvents txtOperatorid As System.Windows.Forms.Label
    Friend WithEvents txtMachineId As System.Windows.Forms.Label
    Friend WithEvents txtWorkcenter As System.Windows.Forms.Label
    Friend WithEvents txtOperationno As System.Windows.Forms.Label
    Friend WithEvents txtItem As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblOperatorid As System.Windows.Forms.Label
    Friend WithEvents lblMachineId As System.Windows.Forms.Label
    Friend WithEvents lblOperationno As System.Windows.Forms.Label
    Friend WithEvents lblWorkcenter As System.Windows.Forms.Label
    Friend WithEvents lblItem As System.Windows.Forms.Label
    Friend WithEvents lblJobno As System.Windows.Forms.Label
    Friend WithEvents pic78 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lstvData As System.Windows.Forms.ListView
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents pnlcom As System.Windows.Forms.Panel
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtKeyComqty As System.Windows.Forms.TextBox
    Friend WithEvents pnlnoofOp As System.Windows.Forms.Panel
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtKeynoofOp As System.Windows.Forms.TextBox
    Friend WithEvents pnlReject As System.Windows.Forms.Panel
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtKeyRejqty As System.Windows.Forms.TextBox
    Friend WithEvents TimerMachine As System.Windows.Forms.Timer
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents selectedJobGridList As System.Windows.Forms.DataGridView
    Friend WithEvents jobnumbersaved As System.Windows.Forms.TextBox
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents machineSelectInput As System.Windows.Forms.TextBox
    Friend WithEvents selected As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents transnum As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents jobnumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents suffix As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents numberofoperators As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CompQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RejQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RejectReason As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
