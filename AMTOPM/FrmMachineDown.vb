Imports System.Data.SqlClient
Public Class FrmMachineDown
    Dim clsM As New clsMain
    Dim intCnt As Integer
    Dim parm As SqlParameter
    Dim dblStartdate As Double
    Private Sub FrmMachineDown_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        check_refresh()
        intCnt = 0
        cmbOption.SelectedIndex = 0
        lblMsgbox.Text = "Scan machine id!"
        txtMain.Focus()
    End Sub

    Private Sub TimerMachine_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerMachine.Tick
        If lblMsgbox.Visible = False Then
            lblMsgbox.Visible = True
        Else
            lblMsgbox.Visible = False
        End If
    End Sub

    Private Sub txtMain_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMain.KeyPress
        If Asc(e.KeyChar) = 13 Then
            ' Dim idv As String = UCase(Mid(txtMain.Text, 1, 2))
            Dim ACV As String = txtMain.Text
            ACV = UCase(ACV)
            txtMain.Text = ""
            If ACV = "CLER" Then
                intCnt = 0
                txtMachineID.Text = ""
                check_refresh()
                txtMain.Text = ""
                Exit Sub
            End If
            If ACV = "EXIT" Or UCase(txtMain.Text) = "CNCL" Then
                intCnt = 0
                txtMachineID.Text = ""
                check_refresh()
                txtMain.Text = ""
                Me.Close()
                Exit Sub
            End If
         
            If intCnt = 0 Then
                If CheckMachineID(ACV) = False Then
                    intCnt = 0
                    lblMsgbox.Text = "Scan machine id!"
                    ACV = txtMain.Text
                Else
                    intCnt = 1
                    lblMsgbox.Text = "Scan your id!"
                    txtMachineID.Text = ""
                End If
                txtMachineID.Text = ACV
                If CheckMachineID(ACV) = False Then
                    txtMachineID.Text = ""
                    lblMsgbox.Text = "Scan machine id!"
                    pic1.Top = 20
                    pic1.Left = 17
                    intCnt = 0
                Else
                    lblMsgbox.Text = "Scan your id!"
                    If lblStatus.Text = "Start" Then
                        pic1.Top = 236
                        pic1.Left = 26
                    Else
                        pic1.Top = 96
                        pic1.Left = 26
                    End If
                    intCnt = 2
                End If
                txtMain.Text = ""
                txtMain.Focus()
                Exit Sub
            End If

            If intCnt = 2 Then
                If Check_opID(ACV) = False Then
                    lblMsgbox.Text = "Please check and rescan you id!"

                    If lblStatus.Text = "Start" Then
                        intCnt = 2
                        pic1.Top = 115
                        pic1.Left = 26
                    Else
                        pic1.Top = 216
                        pic1.Left = 3
                        intCnt = 2
                    End If

                Else
                    If lblStatus.Text = "Start" Then
                        pic1.Top = 297
                        pic1.Left = 26
                        intCnt = 3
                        txtMain.Text = ""
                        lblMsgbox.Text = "Please enter the date!"
                        txtEndDate.BackColor = Color.Yellow
                        txtEndDate.Focus()
                        Exit Sub
                    Else
                        pic1.Top = 146
                        pic1.Left = 26
                        intCnt = 3
                        txtMain.Text = ""
                        txtStartDate.BackColor = Color.Yellow
                        lblMsgbox.Text = "Please enter the date!"
                        txtStartDate.Focus()
                        Exit Sub
                    End If
                End If



                txtMain.Text = ""
                txtMain.Focus()
                Exit Sub
            End If





            If intCnt = 3 Then
                If ACV = "OPTION" Then
                    cmbOption.Focus()
                    Exit Sub
                End If
                If ACV = "REMK" Then
                    txtRemarks.Focus()
                    Exit Sub
                End If
            End If

            If intCnt = 3 Or intCnt = 4 Then
                If ACV = "SAVE" Then
                    If SaveData() = True Then
                        Me.Close()
                    End If
                End If
            End If



        End If
    End Sub


    Private Function CheckMachineID(ByVal mcid As String) As Boolean
        CheckMachineID = False
        Dim sql As String
        Dim i As Integer
        txtID.Text = ""
        Try

            Dim Ds As New DataSet
            sql = "select * from tbMachine where _machineID='" & fncstr(mcid) & "'"
            Ds = clsM.GetDataset(sql, "tbMachineDown")
            If Ds.Tables(0).Rows.Count > 0 Then
                txtMachineID.Text = mcid
                txtMachineDesc.Text = Ds.Tables(0).Rows(0).Item("_MachineDesc")
                txtMachineDesc.ForeColor = Color.Green
                txtMachineID.ForeColor = Color.Green
                lblMachineDesc.ForeColor = Color.Green
                lblMachineID.ForeColor = Color.Green
                CheckMachineID = True
            Else
                txtMachineDesc.Text = ""
                txtMachineDesc.ForeColor = Color.Black
                txtMachineID.ForeColor = Color.Black
                lblMachineDesc.ForeColor = Color.Black
                lblMachineID.ForeColor = Color.Black
            End If

            Dim DsCheck As New DataSet
            Dim ADCheck As New SqlDataAdapter
            sql = "select * from  tbMachineDown where _mcdowntimeid =(select max(_mcdowntimeid) from tbMachineDown where  _status='End' and _machineid='" & mcid & "') Union all (select * from  tbMachineDown where  _status='Start' and _machineid='" & mcid & "')"
            DsCheck = clsM.GetDataset(sql, "tbMachineDown")
            If DsCheck.Tables(0).Rows.Count > 0 Then
                For i = 0 To DsCheck.Tables(0).Rows.Count - 1
                    With DsCheck.Tables(0).Rows(i)
                        If .Item("_status") = "Start" Then
                            txtID.Text = .Item("_mcdowntimeid")
                            txtFrom.Text = Format(DateTime.FromOADate(.Item("_startDate")), "dd/MM/yyyy HH:mm")
                            dblStartdate = .Item("_startDate")
                            txtFid.Text = .Item("_stOperatorid")
                            If CStr(.Item("_stOperatorid")).Length > 4 Then
                                txtDFid.Text = Mid(.Item("_stOperatorid"), 1, 4)
                            Else
                                txtDFid.Text = .Item("_stOperatorid")
                            End If
                            lblSRemarks.Text = .Item("_Remarks")
                            lblSOption.Text = .Item("_Type")
                            txtFName.Text = .Item("_stOperatorName")
                            lblRemarks.ForeColor = Color.Green
                            lblOption.ForeColor = Color.Green
                            txtFName.ForeColor = Color.Green
                            txtFrom.ForeColor = Color.Green
                            txtFid.ForeColor = Color.Green
                            txtDFid.ForeColor = Color.Green
                            lblForm.ForeColor = Color.Green
                            lblFName.ForeColor = Color.Green
                            lblFid.ForeColor = Color.Green

                            lblMsgbox.Text = "Scan opeartor id!"
                            lblStatus.Text = "Start"
                            txtAction.Text = "MACHINE DOWNTIME END"
                            'txtTo.Text = Format(Now, "dd/MM/yyyy HH:mm")
                        Else


                            txtHFrom.Text = Format(DateTime.FromOADate(.Item("_startDate")), "dd/MM/yyyy HH:mm")
                            txtHFOption.Text = .Item("_Type")
                            txtHFRemarks.Text = .Item("_Remarks")
                            txtHFID.Text = .Item("_stOperatorid")
                            If CStr(.Item("_stOperatorid")).Length > 4 Then
                                txtDHFID.Text = Mid(.Item("_stOperatorid"), 1, 4)
                            Else
                                txtDHFID.Text = .Item("_stOperatorid")
                            End If
                            txtHFName.Text = .Item("_stOperatorName")
                            txtHTo.Text = Format(DateTime.FromOADate(.Item("_endDate")), "dd/MM/yyyy HH:mm")
                            txtHTID.Text = .Item("_etOperatorid")

                            If CStr(.Item("_etOperatorid")).Length > 4 Then
                                txtDHTID.Text = Mid(.Item("_etOperatorid"), 1, 4)
                            Else
                                txtDHTID.Text = .Item("_etOperatorid")
                            End If
                            txtHTName.Text = .Item("_etOperatorName")
                            lblMsgbox.Text = "Scan opeartor id!"
                            txtAction.Text = "MACHINE DOWNTIME START"
                            'txtFrom.Text = Format(Now, "dd/MM/yyyy HH:mm")
                            txtTo.Text = ""
                            txtAction.Text = "MACHINE DOWNTIME START"
                        End If


                    End With
                Next


            End If
            'CheckMachineID = True

        Catch ex As Exception
            lblMsgbox.Text = (ex.Message)
        Finally
            cn.Close()
            txtMain.Focus()
        End Try
        Return CheckMachineID
    End Function

    Function Check_opID(ByVal idno As String) As Boolean
        Check_opID = False

        Try
            Dim sql As String = "SELECT * FROM  tbOperator where _operatorID='" & fncstr(idno) & "'"
            Dim DsOp As New DataSet
            DsOp = clsM.GetDataset(Sql, "tbMachineDown")

            If DsOp.Tables(0).Rows.Count > 0 Then
                Check_opID = True
                With DsOp.Tables(0).Rows(0)
                    If lblStatus.Text = "Start" Then
                        txtTid.Text = .Item("_operatorID")
                        ' txtDTid.Text = .Item("_operatorID")
                        If CStr(.Item("_operatorID")).Length > 4 Then
                            txtDTid.Text = Mid(.Item("_operatorID"), 1, 4)
                        Else
                            txtDTid.Text = .Item("_operatorID")
                        End If

                        txtTName.Text = .Item("_operatorName")
                        txtTid.ForeColor = Color.Green
                        txtDTid.ForeColor = Color.Green
                        txtTName.ForeColor = Color.Green
                        lblTid.ForeColor = Color.Green
                        lblTName.ForeColor = Color.Green
                        lblRemarks.ForeColor = Color.Green
                        lblOption.ForeColor = Color.Green
                        lblEndDate.Visible = True
                        txtEndDate.Visible = True
                        lblEndTime.Visible = True
                        txtEndTime.Visible = True


                        txtEndDate.Focus()

                    Else
                        txtFid.Text = .Item("_operatorID")
                        txtDFid.ForeColor = Color.Green
                        If CStr(.Item("_operatorID")).Length > 4 Then
                            txtDFid.Text = Mid(.Item("_operatorID"), 1, 4)
                        Else
                            txtDFid.Text = .Item("_operatorID")
                        End If
                        txtFName.Text = .Item("_operatorName")
                        txtTid.Text = ""
                        txtDTid.Text = ""
                        txtTName.Text = ""
                        txtFid.ForeColor = Color.Green
                        txtDFid.ForeColor = Color.Green
                        txtFName.ForeColor = Color.Green
                        txtTid.ForeColor = Color.Black
                        txtDTid.ForeColor = Color.Black
                        txtTName.ForeColor = Color.Black
                        lblTid.ForeColor = Color.Black
                        lblTName.ForeColor = Color.Black
                        lblStartDate.Visible = True
                        txtStartDate.Visible = True
                        lblStartTime.Visible = True
                        txtStartTime.Visible = True

                        txtRemarks.Visible = True
                        cmbOption.Visible = True
                        lblRemarks.ForeColor = Color.Black
                        lblOption.ForeColor = Color.Black

                        txtEndDate.Focus()
                    End If
                End With
            Else
                Check_opID = False

            End If
        Catch ex As Exception
            lblMsgbox.Text = ex.Message
            Check_opID = False

        Finally
            cn.Close()
        End Try


        Return Check_opID

    End Function


    Sub check_refresh()
        dblStartdate = 0
        'If Trim(txtMachineID.Text) = "" Then
        txtID.Text = ""
        lblMsgbox.Text = "Scan machine id!"
        txtMachineID.Text = ""
        txtMachineDesc.Text = ""

        txtFrom.Text = ""
        txtFid.Text = ""
        txtDFid.Text = ""
        txtFName.Text = ""
        txtTo.Text = ""
        txtTid.Text = ""
        txtDTid.Text = ""
        txtTName.Text = ""
        cmbOption.SelectedIndex = 0
        cmbOption.Visible = False
        txtRemarks.Visible = False
        txtHFOption.Text = ""
        txtRemarks.Text = ""
        txtHFOption.Text = ""
        txtHFRemarks.Text = ""
        lblSOption.Text = ""
        lblSRemarks.Text = ""
        ' txtStartDate.Text = ""
        'txtStartTime.Text = ""
        'txtEndDate.Text = ""
        ' txtEndTime.Text = ""

        txtHFrom.Text = ""
        txtHTo.Text = ""
        txtHFID.Text = ""
        txtDHFID.Text = ""
        txtHFName.Text = ""
        txtHTID.Text = ""
        txtDHTID.Text = ""
        txtHTName.Text = ""



        txtMachineID.ForeColor = Color.Black
        txtMachineDesc.ForeColor = Color.Black
        lblMachineID.ForeColor = Color.Black
        lblMachineDesc.ForeColor = Color.Black
        lblOption.ForeColor = Color.Black
        lblRemarks.ForeColor = Color.Black
        lblFid.ForeColor = Color.Black
        txtFid.ForeColor = Color.Black
        txtDFid.ForeColor = Color.Black
        lblFName.ForeColor = Color.Black
        txtFName.ForeColor = Color.Black

        txtFrom.ForeColor = Color.Black
        lblForm.ForeColor = Color.Black



        txtTo.ForeColor = Color.Black
        txtTid.ForeColor = Color.Black
        txtDTid.ForeColor = Color.Black
        txtTName.ForeColor = Color.Black



        'pn.ForeColor = Color.Red


        lblTo.ForeColor = Color.Black
        lblTid.ForeColor = Color.Black
        lblTName.ForeColor = Color.Black


        lblStatus.Text = ""
        txtAction.Text = ""
        pic1.Top = 19
        pic1.Left = 27



        lblStartDate.Visible = False
        txtStartDate.Visible = False
        lblStartTime.Visible = False
        txtStartTime.Visible = False

        lblEndDate.Visible = False
        txtEndDate.Visible = False
        lblEndTime.Visible = False
        txtEndTime.Visible = False
        picEnt1.Visible = False
        picEnt2.Visible = False
        picEnt3.Visible = False
        picEnt4.Visible = False
        txtStartDate.Text = ""
        txtStartTime.Text = ""
        txtEndDate.Text = ""
        txtEndTime.Text = ""

        txtStartDate.Text = ""
        ' End If
    End Sub



    Private Sub lblMsgbox_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblMsgbox.Resize
        'PictureBox2.Left = lblMsgbox.Left + lblMsgbox.Width - 10
        'lblTest.Left = lblMsgbox.Left
        'lblTest.Width = lblMsgbox.Width
    End Sub

    Private Sub txtStartDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtStartDate.KeyPress
        If Asc(e.KeyChar) = 13 Then


            txtStartDate.BackColor = Color.White
            txtStartTime.BackColor = Color.Yellow
            lblMsgbox.Text = "Please enter the time!"
            txtStartTime.Focus()
        End If

    End Sub


    Private Sub txtStartTime_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtStartTime.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtStartTime.BackColor = Color.White
            intCnt = 3
            pic1.Top = 173
            pic1.Left = 26
            lblMsgbox.Text = "Please select option!"

            cmbOption.Focus()
        End If
    End Sub


    Private Sub txtendDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEndDate.KeyPress
        If Asc(e.KeyChar) = 13 Then
            intCnt = 3
            txtEndDate.BackColor = Color.White
            txtEndTime.BackColor = Color.Yellow
            pic1.Top = 197
            pic1.Left = 26
            lblMsgbox.Text = "Please enter the time!"
            txtEndTime.Focus()
        End If

    End Sub


    Private Sub txtendTime_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEndTime.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtEndTime.BackColor = Color.White
            intCnt = 3
            lblMsgbox.Text = "Please scan SAVE!"
            txtMain.Focus()
        End If
    End Sub
    Function SaveData() As Boolean
        SaveData = False
        Try
            If Trim(txtMachineID.Text) = "" Then
                lblMsgbox.Text = "Please scan your machine id!"
                txtMain.Text = ""
                txtMain.Focus()
                Exit Function
            End If
            If lblStatus.Text = "Start" Then
                If Trim(txtEndDate.Text) = "" Then
                    lblMsgbox.Text = "Please enter the date!"
                    txtMain.Text = ""
                    txtEndDate.BackColor = Color.Yellow
                    txtEndDate.Focus()
                    Exit Function
                End If
            End If

            If lblStatus.Text = "Start" Then
                If Trim(txtEndTime.Text) = "" Then
                    lblMsgbox.Text = "Please enter the time!"
                    txtMain.Text = ""
                    txtEndTime.BackColor = Color.Yellow
                    txtEndTime.Focus()
                    Exit Function
                End If
            End If

            If lblStatus.Text <> "Start" Then
                If Trim(txtStartDate.Text) = "" Then
                    lblMsgbox.Text = "Please enter the time!"
                    txtMain.Text = ""
                    txtStartDate.BackColor = Color.Yellow
                    txtStartDate.Focus()
                    Exit Function
                End If
            End If
            If lblStatus.Text <> "Start" Then
                If Trim(txtStartTime.Text) = "" Then
                    lblMsgbox.Text = "Please enter the time!"
                    txtMain.Text = ""
                    txtStartTime.BackColor = Color.Yellow
                    txtStartTime.Focus()
                    Exit Function
                End If
            End If
            Dim s() As String

            Dim sd, Ed As Double
            Try
                If lblStatus.Text <> "Start" Then
                    s = Split(txtStartDate.Text, "/")
                    'Dim s1 As String = Mid(txtStartTime.Text, 1, 2)
                    ' Dim s1 As String = Mid(txtStartTime.Text, 1, 2)
                    'Dim s2 As String = Mid(txtStartTime.Text, 3, 4)
                    sd = CDate(DateSerial(s(2), s(1), s(0)) & " " & Trim(txtStartTime.Text)).ToOADate
                End If
            Catch ex As Exception
                lblMsgbox.Text = "Please check your date and  time!"
                txtMain.Text = ""
                txtStartDate.BackColor = Color.Yellow
                txtStartDate.Focus()
                Exit Function
            End Try
           
            Try
                If lblStatus.Text = "Start" Then
                    s = Split(txtEndDate.Text, "/")
                    ' Dim s1 As String = Mid(txtEndTime.Text, 1, 2)
                    'Dim s2 As String = Mid(txtEndTime.Text, 3, 4)
                    Ed = CDate(DateSerial(s(2), s(1), s(0)) & " " & Trim(txtEndTime.Text)).ToOADate
                End If
            Catch ex As Exception
                lblMsgbox.Text = "Please check your date and  time!"
                txtMain.Text = ""
                txtEndDate.BackColor = Color.Yellow
                txtEndDate.Focus()
                Exit Function
            End Try
            If lblStatus.Text = "Start" Then
                If dblStartdate > Ed Then
                    lblMsgbox.Text = "Verify the date/time of To is not earlier than From!"
                    txtMain.Text = ""
                    txtEndDate.BackColor = Color.Yellow
                    txtEndDate.Focus()
                    Exit Function
                End If
            End If



            With com
                If lblStatus.Text <> "Start" Then
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "Insert into tbMachineDown(_machineid,_startDate,_stOperatorid,_stOperatorName,_status,_Type,_Remarks)values(@machineid,@startDate,@stOperatorid,@stOperatorName,'Start',@Type,@Remarks)"
                    parm = .Parameters.Add("@machineid", SqlDbType.VarChar, 50)
                    parm.Value = Trim(txtMachineID.Text)
                    parm = .Parameters.Add("@startDate", SqlDbType.Float)
                    parm.Value = sd
                    parm = .Parameters.Add("@stOperatorid", SqlDbType.VarChar, 50)
                    parm.Value = Trim(txtFid.Text)
                    parm = .Parameters.Add("@stOperatorName", SqlDbType.VarChar, 256)
                    parm.Value = Trim(txtFName.Text)
                    parm = .Parameters.Add("@Type", SqlDbType.VarChar, 50)
                    parm.Value = Trim(cmbOption.Text)
                    parm = .Parameters.Add("@Remarks", SqlDbType.VarChar, 500)
                    parm.Value = Trim(txtRemarks.Text)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()
                    SaveData = True
                    MsgBox("Successfully insert!", MsgBoxStyle.Information, "eWIP")
                    check_refresh()
                    Exit Function
                Else
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "Update tbMachineDown set _endDate=@endDate,_etOperatorid=@etOperatorid,_etOperatorName=@etOperatorName,_status='End' where _mcdowntimeid=@mcdowntimeid"
                    parm = .Parameters.Add("@endDate", SqlDbType.Float)
                    parm.Value = Ed
                    parm = .Parameters.Add("@etOperatorid", SqlDbType.VarChar, 50)
                    parm.Value = Trim(txtTid.Text)
                    parm = .Parameters.Add("@etOperatorName", SqlDbType.VarChar, 256)
                    parm.Value = Trim(txtTName.Text)
                    parm = .Parameters.Add("@mcdowntimeid", SqlDbType.Int)
                    parm.Value = Val(txtID.Text)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()
                    SaveData = True
                    MsgBox("Action updated!", MsgBoxStyle.Information, "eWIP")
                    check_refresh()
                    Exit Function
                End If
                Return SaveData
            End With









        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try


    End Function




    Private Sub txtStartDate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub lblMsgbox_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblMsgbox.Click

    End Sub

    Private Sub txtHFName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtHFName.Click

    End Sub

    Private Sub txtStartTime_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cmbOption_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbOption.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtStartTime.BackColor = Color.White
            intCnt = 3
            lblMsgbox.Text = "Please enter the remark!"
            txtRemarks.Focus()
            txtRemarks.BackColor = Color.Yellow
        End If
    End Sub

    Private Sub cmbOption_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbOption.SelectedIndexChanged

    End Sub

    Private Sub txtRemarks_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRemarks.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtRemarks.BackColor = Color.White
            intCnt = 4
            lblMsgbox.Text = "Please scan SAVE!"
            txtMain.Text = "SAVE"
            txtMain.Focus()
        End If
    End Sub

  
   
    
    Private Sub txtRemarks_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemarks.TextChanged

    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblOption.Click

    End Sub

    Private Sub txtEndDate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub txtEndTime_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class