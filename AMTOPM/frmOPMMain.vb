
Public Class frmOPMMain
    Dim count As Integer

    Private Sub txtMain_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMain.KeyPress
        If Asc(e.KeyChar) = 13 Then
            lblMSG.Text = ""
            Dim strText As String = UCase(txtMain.Text)
            txtMain.Text = ""
            If UCase(strText) = "QUIT" Then
                Application.Exit()
            ElseIf UCase(strText) = "MCDN" Then
                Dim obj As New FrmMachineDown
                TimerMachine.Stop()
                obj.ShowDialog()
                TimerMachine.Start()
                txtMain.Text = ""
                txtMain.Focus()
            ElseIf UCase(strText) = "DATA" Then
                Dim obj As New frmScanJob
                TimerMachine.Stop()
                obj.ShowDialog()
                TimerMachine.Start()
                txtMain.Text = ""
                txtMain.Focus()
            ElseIf UCase(strText) = "STOCK" Then
                Dim obj As New frmStockEntry
                TimerMachine.Stop()
                obj.ShowDialog()
                TimerMachine.Start()
                txtMain.Text = ""
                txtMain.Focus()
            ElseIf UCase(strText) = "BATCH" Then
                Dim obj As New frmBatchScan
                TimerMachine.Stop()
                obj.ShowDialog()
                TimerMachine.Start()
                txtMain.Text = ""
                txtMain.Focus()


            Else
                lblMSG.Text = "Invalid input!"
            End If
        End If
    End Sub

    Private Sub frmOPMMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        count = 0
        LogPrivilege = ""
    End Sub

    Private Sub TimerMachine_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerMachine.Tick
        If lblMSG.Text <> "" Then
            If count > 10 Then
                lblMSG.Text = ""
                count = 0
            Else
                count = count + 1
            End If
        Else
            count = 0
        End If
        If lblMSG.Visible = False Then
            lblMSG.Visible = True
        Else
            lblMSG.Visible = False
        End If
    End Sub

  
End Class