﻿Imports System.Data.SqlClient

Public Class frmBatchDataEntry
    Dim parm As SqlParameter
    Dim i As Integer
    Dim clsM As New clsMain
    Public CountI As Integer
    Dim strstage As String
    Dim strOPid As String
    Dim strOPName As String
    Dim strOPsplit As String
    Dim strOPskip As String
    Public curNO As String
    Public curName As String
    Dim ChkSkip As Integer
    Dim Chksplit As Integer
    Public OPSPLIT As Integer
    Public OPSKIP As Integer
    Dim TempMsg As String
    Dim WCDesc As String

    'variable bikinan eka !=
    Dim Editcolumn As Boolean
    Dim CurrentColumnSelected As Integer
    Dim ListSelectMode As Boolean
    Dim RejCode As String


    Public rejtransfer As String

    Public Property RejectedCode() As String
        Get
            Return RejCode
        End Get
        Set(value As String)
            RejCode = value
        End Set
    End Property



    '=!


    Private Property jobnoinsert As Object

    Sub SKIP_Status()
        Dim DSP As New DataSet
        DSP = clsM.GetDataset("select * from tbOperationStatus", "tbOperationStatus")
        If DSP.Tables(0).Rows.Count > 0 Then
            Chksplit = DSP.Tables(0).Rows(0).Item("_Split")
            ChkSkip = DSP.Tables(0).Rows(0).Item("_skip")
        End If
    End Sub
    Private Sub frmDataEntery_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Chksplit = 0
        ChkSkip = 0
        SKIP_Status()
        Newhash.Clear()
        txtMain.Text = ""
        txtMain.Focus()
        ' CountI = 0
        strOPid = ""
        strOPName = ""
        strOPsplit = ""
        strOPskip = ""

        If UCase(txtAction.Text) = "START" Then
            'lblSave.Visible = True
        End If

        With lstv
            .Columns.Add("RJ-Code", 75, HorizontalAlignment.Left)
            .Columns.Add("QTY", lstv.Width - 75, HorizontalAlignment.Left)
        End With


        With lstvData
            .Columns.Add("IDNo", 0, HorizontalAlignment.Left)
            .Columns.Add("Job No", 0, HorizontalAlignment.Left)
            .Columns.Add("OP ID", 0, HorizontalAlignment.Left)
            .Columns.Add("OP Name", lstvData.Width - 600, HorizontalAlignment.Left)
            '_endemp_name
            .Columns.Add("Start Time", 100, HorizontalAlignment.Left)
            .Columns.Add("End Time", 100, HorizontalAlignment.Left)
            .Columns.Add("Released ", 75, HorizontalAlignment.Left)
            .Columns.Add("Received ", 75, HorizontalAlignment.Left)
            .Columns.Add("Completed ", 75, HorizontalAlignment.Left)
            .Columns.Add("Rejected", 75, HorizontalAlignment.Left)

            .Columns.Add("SP", 50, HorizontalAlignment.Left)
            .Columns.Add("SC", 50, HorizontalAlignment.Left)

            .Columns.Add("WC", 0, HorizontalAlignment.Left)
            .Columns.Add("OP No.", 0, HorizontalAlignment.Left)
            .Columns.Add("Item No.", 0, HorizontalAlignment.Left)


        End With
        Dim DSWCDESC As New DataSet
        WCDesc = ""
        DSWCDESC = clsM.GetDataset("select * from tbWC where _wc='" & fncstr(txtWorkcenter.Text) & "' and _description like('INJECTION%')", "TbWC")
        If DSWCDESC.Tables(0).Rows.Count > 0 Then
            WCDesc = DSWCDESC.Tables(0).Rows(0).Item("_description")
        End If
        LoadListItem()
        LoadListWorkOrder()
        LoadForm()
    End Sub
    Sub LoadListItem()
        Dim DSLoadItem As New DataSet
        'DSLoadItem = clsM.GetDataset("select * from tbJobTrans where _job='" & fncstr(txtJobno.Text) & "' and  _jobsuffix ='" & txtsuf.Text & "' and _oper_num=" & Val(txtOperationno.Text), "TbJob")
        DSLoadItem = clsM.GetDataset("select * from tbJobTrans where _job='" & fncstr(txtJobno.Text) & "' and  _jobsuffixParent ='" & txtParent.Text & "' and _oper_num=" & Val(txtOperationno.Text), "TbJob")
        If DSLoadItem.Tables(0).Rows.Count > 0 Then
            For i = 0 To DSLoadItem.Tables(0).Rows.Count - 1
                With DSLoadItem.Tables(0).Rows(i)
                    Dim ls As New ListViewItem(Trim(.Item("_tansnum")))    ' you can also use reader.GetSqlValue(0) 
                    ls.SubItems.Add(Trim(.Item("_job")))
                    ls.SubItems.Add(Trim(.Item("_emp_num")))
                    ls.SubItems.Add(Trim(.Item("_emp_name")))
                    If .Item("_start_Date") <> 0 Then
                        ls.SubItems.Add(Format(DateTime.FromOADate(.Item("_start_Date")), "dd/MM/yyyy HH:mm"))
                    Else
                        ls.SubItems.Add("-")
                    End If

                    If .Item("_end_Date") <> 0 Then
                        ls.SubItems.Add(Format(DateTime.FromOADate(.Item("_end_Date")), "dd/MM/yyyy HH:mm"))
                    Else
                        ls.SubItems.Add("-")
                    End If
                    ls.SubItems.Add(Format(.Item("_qty_Rele_qty"), "00"))
                    ls.SubItems.Add(Format(.Item("_qty_op_qty"), "00"))
                    ls.SubItems.Add(Format(.Item("_qty_complete"), "00"))
                    ls.SubItems.Add(Format(.Item("_qty_scrapped"), "00"))
                    ls.SubItems.Add(Trim(.Item("_jobsuffixParent")))
                    If Trim(.Item("_jobsuffix")) = "M" Then
                        ls.SubItems.Add("-")
                    Else
                        ls.SubItems.Add(Trim(.Item("_jobsuffix")))
                    End If

                    ls.SubItems.Add(Trim(.Item("_wc")))
                    ls.SubItems.Add(Trim(.Item("_oper_num")))
                    ls.SubItems.Add(Trim(.Item("_item")))



                    ls.SubItems.Add("-")
                    lstvData.Items.Add(ls)
                End With
            Next


        End If
    End Sub

    Sub LoadListWorkOrder()
        Dim DSLoadItem As New DataSet
        'DSLoadItem = clsM.GetDataset("select * from tbJobTrans where _job='" & fncstr(txtJobno.Text) & "' and  _jobsuffix ='" & txtsuf.Text & "' and _oper_num=" & Val(txtOperationno.Text), "TbJob")
        'DSLoadItem = clsM.GetDataset("select * from tbJobRoute where _job='" & fncstr(txtJobno.Text) & "'", "tbJobRoute")
        'If DSLoadItem.Tables(0).Rows.Count > 0 Then
        '    For i = 0 To DSLoadItem.Tables(0).Rows.Count - 1
        '        With DSLoadItem.Tables(0).Rows(i)
        '            ' you can also use reader.GetSqlValue(0) 


        '            'ls = New ListViewItem(Trim(.Item("_operationNo")))


        '            selectedJobList.Items.Add(.Item("_operationNo"))
        '        End With
        '    Next


        'End If
    End Sub



    Sub LoadForm()
        Dim strRelV As String
        strRelV = UCase(txtMain.Text)

        lblMsgbox.Text = "Type Job No"
        jobnumbersaved.Focus()
        InsertToList(txtJobno.Text, txtsuf.Text)


        If UCase(txtAction.Text) = "COMPLETED" Or UCase(txtAction.Text) = "SPLIT" Then
            If CountI = 0 Then


                txtOperatorid.Text = strOPid
                txtName.Text = strOPName
                txtOperatorid.ForeColor = Color.Green
                lblOperatorid.ForeColor = Color.Green
                txtName.ForeColor = Color.Green
                lblName.ForeColor = Color.Green


                pnlReject.Visible = False
                pnlcom.Visible = False
                'pnlnoofOp.Visible = True
                'pic1.Left = 278
                'pic1.Top = 150
                'TempMsg = lblMsgbox.Text
                'lblMsgbox.Text = "Enter the job no!"
                'If Trim(txtnoofOP.Text) = "" Then
                '    txtKeynoofOp.Text = 1
                'Else
                '    txtKeynoofOp.Text = txtnoofOP.Text
                'End If
                'txtKeynoofOp.Focus()




                CountI = 1
                Exit Sub

            End If

            If CountI = 1 Then
                If strRelV = "NOOP" Then
                    pnlReject.Visible = False
                    pnlcom.Visible = False
                    pnlnoofOp.Visible = True
                    ' pic1.Left = 278
                    '  pic1.Top = 149
                    TempMsg = lblMsgbox.Text
                    lblMsgbox.Text = "Enter the no. of operator!"

                    txtKeynoofOp.Text = txtnoofOP.Text
                    txtKeynoofOp.Focus()
                    Exit Sub
                End If
                If strRelV = "CQ" Then
                    pnlReject.Visible = False
                    pnlcom.Visible = True
                    txtKeyComqty.Text = txtComQty.Text
                    lblMsgbox.Text = "Enter the completed quantity!"
                    txtKeyComqty.Focus()
                    pic1.Left = 278
                    ' txtnoofOP.Text = 1
                    pic1.Top = 289
                ElseIf strRelV = "RQ" Then
                    pnlReject.Visible = True
                    pnlcom.Visible = False
                    lblMsgbox.Text = "Enter the rejected quantity, if any."
                    txtKeyRejqty.Text = txtRejQ.Text
                    txtKeyRejqty.Focus()
                ElseIf strRelV = "SAVE" Then

                    Dim savestatus As Boolean = True
                    Dim i As Integer

                    For i = 0 To selectedJobGridList.Rows.Count

                        Dim DS As New DataSet
                        Dim DSroute As New DataSet
                        Dim DSActual As New DataSet

                        DS = clsM.GetDataset("select * from tbjob where _job='" & selectedJobGridList.Rows(i).Cells(2).Value & "' AND _jobsuffix='" & selectedJobGridList.Rows(i).Cells(3).Value & "' order by _oper_num", "tbJob")

                        DSActual = clsM.GetDataset("select top 1 * from tbjobTrans where _job='" & selectedJobGridList.Rows(i).Cells(2).Value & "' AND _jobsuffix='" & selectedJobGridList.Rows(i).Cells(3).Value & "' order by _oper_num", "tbjobTrans")
                        DSroute = clsM.GetDataset("select top 1 * from tbjobroute where _job='" & selectedJobGridList.Rows(i).Cells(2).Value & "' AND _jobSuffix='" & DSActual.Tables(0).Rows(0).Item("_oper_num") & "' order by _operationNo", "tbjobroute")
                        If DSActual.Tables(0).Rows.Count > 0 Then

                            With DSActual.Tables(0).Rows(0)
                                txtJobno.Text = .Item("_job")
                                txtJobno.ForeColor = Color.Green
                                lblJobno.ForeColor = Color.Green
                                txtItem.Text = .Item("_item")
                                txtItem.ForeColor = Color.Green
                                lblItem.ForeColor = Color.Green
                                txtRlQ.Text = Format(.Item("_qty_Rele_qty"), "0.00")
                                txtRlQ.ForeColor = Color.Green
                                lblRlQ.ForeColor = Color.Green
                                txtReQ.Text = Format(.Item("_qty_op_qty"), "0.00")
                                txtReQ.ForeColor = Color.Green
                                lblReQ.ForeColor = Color.Green
                                txtStartDate.Text = Format(Now, "dd/MM/yyyy HH:mm")
                                txtStartDate.ForeColor = Color.Green
                                lblStartDate.ForeColor = Color.Green
                                ' ploptnew.Visible = True
                                strstage = "New"
                                If DSroute.Tables(0).Rows.Count > 0 Then
                                    txtWorkcenter.Text = DSroute.Tables(0).Rows(0).Item("_wc")
                                    txtWorkcenter.ForeColor = Color.Green
                                    lblWorkcenter.ForeColor = Color.Green
                                    txtOperationno.Text = DSroute.Tables(0).Rows(0).Item("_oper_enum")
                                    txtOperationno.ForeColor = Color.Green
                                    lblOperationno.ForeColor = Color.Green
                                    txtMachineId.Text = clsM.CheckMachineID(DSroute.Tables(0).Rows(0).Item("_wc"))
                                End If
                            End With
                        End If


                        If COMPLETED_SAVE() = True Then

                        Else
                            savestatus = False
                        End If
                    Next

                    If savestatus = True Then
                        Me.Close()
                        Exit Sub
                    Else
                        txtMain.Focus()
                    End If


                ElseIf strRelV = "LIST" Then

                Else
                    MsgBox("Invalid input!", MsgBoxStyle.Information, "eWIP")
                    txtMain.Focus()
                    Exit Sub
                End If

            End If
        End If


    End Sub


    Private Sub InsertToList(ByVal JobNumber As String, ByVal suffix As String)
        Dim DSLoadItem As New DataSet
        'DSLoadItem = clsM.GetDataset("select * from tbJobTrans where _job='" & fncstr(txtJobno.Text) & "' and  _jobsuffix ='" & txtsuf.Text & "' and _oper_num=" & Val(txtOperationno.Text), "TbJob")

        Dim dsparentitems As New DataSet
        dsparentitems = clsM.GetDataset("select * from tbJobTrans where _job='" & fncstr(txtJobno.Text) & "' and  _jobsuffix ='" & txtsuf.Text & "'  order by _oper_num desc ", "tbJobTrans")

        'Dim getstat = dsparentitems.Tables(0).Rows(0).Item("_status")
        Dim getstat = dsparentitems.Tables(0).Rows(0).Item("_oper_num")
        Dim getitem = dsparentitems.Tables(0).Rows(0).Item("_item")

        'DSLoadItem = clsM.GetDataset("select * from tbJobTrans where _job='" & fncstr(JobNumber) & "' AND _jobsuffix='" & fncstr(suffix) & "' AND _status='" & getstat & "'  order by _oper_num desc", "tbJobTrans")
        'DSLoadItem = clsM.GetDataset("select * from tbJobTrans where _job='" & fncstr(JobNumber) & "' AND _jobsuffix='" & fncstr(suffix) & "' AND _oper_num='" & getstat & "' AND _item='" & getitem & "' ", "tbJobTrans")
        DSLoadItem = clsM.GetDataset("select * from tbJobTrans where _job='" & fncstr(JobNumber) & "' AND _jobsuffix='" & fncstr(suffix) & "' AND _item='" & getitem & "'  order by _oper_num desc ", "tbJobTrans")




        Dim selectedjobcount = selectedJobGridList.RowCount

        If DSLoadItem.Tables(0).Rows(0).Item("_oper_num") = getstat Then
            If selectedjobcount < 1 Then
                selectedJobGridList.Rows.Add(New String() {False, DSLoadItem.Tables(0).Rows(0).Item("_tansnum"), DSLoadItem.Tables(0).Rows(0).Item("_job"), DSLoadItem.Tables(0).Rows(0).Item("_jobsuffix")})
            Else
                For i = 0 To selectedjobcount - 1
                    If selectedJobGridList.Rows(0).Cells(1).Value = DSLoadItem.Tables(0).Rows(0).Item("_tansnum") Then
                        MsgBox("Job Transfer Number is already inputted")
                    Else

                        'selectedJobGridList.Rows.Add(DSLoadItem.Tables(0).Rows(0).Item("_operationNo"))

                        'Dim dtable As New DataTable
                        'dtable.Columns.Add(New DataColumn("transnum"))
                        'dtable.Columns.Add(New DataColumn("jobnumber"))
                        'dtable.Columns.Add(New DataColumn("suffix"))



                        selectedJobGridList.Rows.Add(New String() {False, DSLoadItem.Tables(0).Rows(0).Item("_tansnum"), DSLoadItem.Tables(0).Rows(0).Item("_job"), DSLoadItem.Tables(0).Rows(0).Item("_jobsuffix")})

                        Exit For
                        'Dim cbo = CType(dgv2.Columns("FooCol"), DataGridViewComboBoxColumn)
                        'cbo.DataSource = dt
                        'cbo.ValueMember = "Id"
                        'cbo.DisplayMember = "Name"
                    End If
                Next
            End If

        Else
            MsgBox("No match operation")
        End If


    End Sub


    Private Sub txtMain_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMain.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Dim idv As String = ""
            Dim ACV As String = ""
            Dim strRelV As String
            strRelV = UCase(txtMain.Text)

            If (ListSelectMode) Then
                Try
                    Dim selectRow = Integer.Parse(txtMain.Text)
                    JobSelected(selectRow)
                Catch ex As Exception

                End Try
                JobSelected(txtMain.Text)
            End If

            If txtMain.TextLength >= 2 Then
                idv = UCase(Mid(txtMain.Text, 1, 2))
                ACV = UCase(Mid(txtMain.Text, 3, txtMain.TextLength))
            End If

            txtMain.Text = ""

            If UCase(strRelV) = "CLER" Or UCase(strRelV) = "CNCL" Then
                Me.Close()
                Exit Sub
            End If

            If UCase(strRelV) = "LIST" Then
                Panel5.Visible = True
                jobnumbersaved.Focus()

            End If

            If UCase(strRelV) = "REMK" Then
                pnlRemarks.Visible = True
                txtRemarks.Focus()
                TempMsg = lblMsgbox.Text
                lblMsgbox.Text = "Enter the Remarks!"
                Exit Sub
            End If
            'If strRelV = "NOOP" Then
            '    pnlReject.Visible = False
            '    pnlcom.Visible = False
            '    pnlnoofOp.Visible = True
            '    ' pic1.Left = 278
            '    '  pic1.Top = 149
            '    TempMsg = lblMsgbox.Text
            '    lblMsgbox.Text = "Enter the no. of operator!"
            '    txtKeynoofOp.Text = txtnoofOP.Text
            '    txtKeynoofOp.Focus()
            '    Exit Sub
            'End If


            If UCase(txtAction.Text) = "START" Or UCase(txtAction.Text) = "SKIP" Then
                selectedJobGridList.Columns(3).Visible = False
                selectedJobGridList.Columns(4).Visible = False
                selectedJobGridList.Columns(5).Visible = False


                If CountI = 0 Then
                    If UCase(txtAction.Text) <> "SKIP" Then
                        Dim MidName As String = clsM.CheckMachineIDName(txtWorkcenter.Text, strRelV)
                        If MidName = "" Then
                            lblMsgbox.Text = "Scan correct machine id!"
                            MsgBox("Scan correct machine id!", MsgBoxStyle.Information, "eWIP")
                            CountI = 0
                            Exit Sub
                        Else
                            txtMachineId.Text = MidName
                            txtMachineId.ForeColor = Color.Green
                            lblMachineId.ForeColor = Color.Green
                            lblMsgbox.Text = ""
                            pic1.Left = 278
                            pic1.Top = 121

                            CountI = 1
                            lblMsgbox.Text = "Scan your id!"
                            Exit Sub
                        End If
                    End If
                ElseIf CountI = 1 Then
                    If CheckOperator(strRelV) = True Then

                        ' If strOPid = curNO Or curNO = "" Then
                        txtOperatorid.Text = strOPid
                        txtName.Text = strOPName

                        If UCase(txtAction.Text) = "SKIP" Then
                            If ChkSkip = 0 Then
                                MsgBox("Now can not skip this job. Please contact your supervisor.", MsgBoxStyle.Information, "eWIP")
                                Me.Close()
                            Else
                                If strOPskip = "N" Then
                                    MsgBox("You don’t have rights to skip this job. Please contact your supervisor.", MsgBoxStyle.Information, "eWIP")
                                    Me.Close()
                                End If
                            End If
                        End If




                        txtOperatorid.ForeColor = Color.Green
                        lblOperatorid.ForeColor = Color.Green
                        txtName.ForeColor = Color.Green
                        lblName.ForeColor = Color.Green
                        Dim dsdate As New DataSet
                        dsdate = clsM.GetDataset("select getDate() as ToDate", "tbDate")
                        If dsdate.Tables(0).Rows.Count > 0 Then
                            txtStartDate.Text = Format(dsdate.Tables(0).Rows(0).Item("ToDate"), "dd/MM/yyy HH:mm")
                            fltStartDate.Text = CDate(dsdate.Tables(0).Rows(0).Item("ToDate")).ToOADate
                        Else
                            txtStartDate.Text = Format(Now, "dd/MM/yyy HH:mm")
                            fltStartDate.Text = CDate(Now).ToOADate
                        End If
                        txtStartDate.ForeColor = Color.Green
                        lblStartDate.ForeColor = Color.Green

                        '  lblSave.Visible = True

                        If WCDesc <> "" Then
                            pnlRemarks.Visible = True
                            txtRemarks.Focus()
                            TempMsg = lblMsgbox.Text
                            pic1.Left = 278
                            pic1.Top = 366
                            lblMsgbox.Text = "Enter the Remarks!"
                            WCDesc = "HI"
                            Exit Sub

                        Else

                            CountI = 2
                            pic1.Left = 2780
                            pic1.Top = 369
                            lblMsgbox.Text = "Scan 'SAVE' or 'CANCEL'!"
                        End If



                        ' Else

                        '   MsgBox("This operation handle by " & curName & "(" & curNO & "), Please check your supervisor!.")
                        ' CountI = 1
                        ' End If
                    Else
                        CountI = 1
                        lblMsgbox.Text = "Scan your id!"
                        MsgBox("Invalid operator id!", MsgBoxStyle.Information, "eWIP")
                    End If
                ElseIf CountI = 2 Then
                    If strRelV = "SAVE" Then

                        Dim datarows = selectedJobGridList.Rows.Count

                        Dim statusaction As Boolean = True

                        For i = 0 To datarows - 1

                            txtJobno.Text = selectedJobGridList.Rows(i).Cells(1).Value


                            'txtItem.Text = ""
                            'txtOperationno.Text = ""
                            'txtWorkcenter.Text = ""
                            'txtMachineId.Text = ""
                            'txtOperatorid.Text = ""
                            'txtName.Text = ""

                            'txtRlQ.Text = ""
                            'txtReQ.Text = ""
                            'txtStartDate.Text = ""


                            Dim DS As New DataSet
                            Dim DSroute As New DataSet
                            Dim DSActual As New DataSet

                            DS = clsM.GetDataset("select * from tbjob where _job='" & selectedJobGridList.Rows(i).Cells(2).Value & "' AND _jobsuffix='" & selectedJobGridList.Rows(i).Cells(3).Value & "' order by _oper_num", "tbJob")

                            DSActual = clsM.GetDataset("select top 1 * from tbjobTrans where _job='" & selectedJobGridList.Rows(i).Cells(2).Value & "' AND _jobsuffix='" & selectedJobGridList.Rows(i).Cells(3).Value & "' order by _oper_num", "tbjobTrans")
                            DSroute = clsM.GetDataset("select top 1 * from tbjobroute where _job='" & selectedJobGridList.Rows(i).Cells(2).Value & "' AND _jobSuffix='" & DSActual.Tables(0).Rows(0).Item("_oper_num") & "' order by _operationNo", "tbjobroute")
                            If DSActual.Tables(0).Rows.Count > 0 Then

                                With DSActual.Tables(0).Rows(0)
                                    txtJobno.Text = .Item("_job")
                                    txtJobno.ForeColor = Color.Green
                                    lblJobno.ForeColor = Color.Green
                                    txtItem.Text = .Item("_item")
                                    txtItem.ForeColor = Color.Green
                                    lblItem.ForeColor = Color.Green
                                    txtRlQ.Text = Format(.Item("_qty_Rele_qty"), "0.00")
                                    txtRlQ.ForeColor = Color.Green
                                    lblRlQ.ForeColor = Color.Green
                                    txtReQ.Text = Format(.Item("_qty_op_qty"), "0.00")
                                    txtReQ.ForeColor = Color.Green
                                    lblReQ.ForeColor = Color.Green
                                    txtStartDate.Text = Format(Now, "dd/MM/yyyy HH:mm")
                                    txtStartDate.ForeColor = Color.Green
                                    lblStartDate.ForeColor = Color.Green
                                    ' ploptnew.Visible = True
                                    strstage = "New"
                                    If DSroute.Tables(0).Rows.Count > 0 Then
                                        txtWorkcenter.Text = DSroute.Tables(0).Rows(0).Item("_wc")
                                        txtWorkcenter.ForeColor = Color.Green
                                        lblWorkcenter.ForeColor = Color.Green
                                        txtOperationno.Text = DSroute.Tables(0).Rows(0).Item("_oper_enum")
                                        txtOperationno.ForeColor = Color.Green
                                        lblOperationno.ForeColor = Color.Green
                                        txtMachineId.Text = clsM.CheckMachineID(DSroute.Tables(0).Rows(0).Item("_wc"))
                                    End If
                                End With
                            End If


                            If START_SAVE() = True Then

                            Else
                                statusaction = False
                            End If
                        Next

                        If statusaction = True Then
                            MsgBox("Action updated!", MsgBoxStyle.Information, "eWIP")
                            Me.Close()
                            Exit Sub
                        Else
                            lblMsgbox.Text = "Scan your ID!"
                        End If
                    Else
                        MsgBox("Invalid input!", MsgBoxStyle.Information, "eWIP")
                        ' mas("Invalid input!")
                        txtMain.Focus()
                    End If
                End If


            ElseIf UCase(txtAction.Text) = "PAUSE" Or UCase(txtAction.Text) = "RESUME" Then
                If CountI = 0 Then
                    If CheckOperator(strRelV) = True Then
                        ' If strOPid = txtOPid.Text Then
                        txtOperatorid.Text = strOPid
                        txtName.Text = strOPName
                        txtOperatorid.ForeColor = Color.Green
                        lblOperatorid.ForeColor = Color.Green
                        txtName.ForeColor = Color.Green
                        lblName.ForeColor = Color.Green
                        pic1.Left = 2780
                        pic1.Top = 369
                        CountI = 1
                        lblMsgbox.Text = "Scan 'SAVE' or 'CANCEL'!"
                        ' lblSave.Visible = True
                        txtMain.Focus()
                        Exit Sub
                        'Else

                        ' MsgBox("This operation handle by " & strOPName & "(" & strOPid & "), Please check your supervisor!.")
                        ' CountI = 0
                        ' End If
                    Else
                        MsgBox("Check your id!.", MsgBoxStyle.Information, "eWIP")
                        CountI = 0
                    End If
                End If
                If CountI = 1 Then
                    If strRelV = "SAVE" Then
                        If PAUSE_SAVE() = True Then
                            MsgBox("Action updated!", MsgBoxStyle.Information, "eWIP")
                            Me.Close()
                            Exit Sub
                        Else
                            lblMsgbox.Text = "Invalid input!"
                            txtMain.Focus()
                        End If
                    Else
                        lblMsgbox.Text = "Invalid input!"
                        txtMain.Focus()
                    End If
                End If


            ElseIf UCase(txtAction.Text) = "HANDOVER" Then
                If CountI = 0 Then
                    If CheckOperator(strRelV) = True Then
                        If strOPid = txtOPid.Text Then
                            txtOperatorid.Text = strOPid
                            txtName.Text = strOPName
                            txtOperatorid.ForeColor = Color.Green
                            lblOperatorid.ForeColor = Color.Green
                            txtName.ForeColor = Color.Green
                            lblName.ForeColor = Color.Green
                            pic1.Left = 278
                            pic1.Top = 369
                            CountI = 1
                            pic1.Visible = False

                            Show_Handover()
                            If UCase(strHandover) = "YES" Then
                                MsgBox("Action updated!", MsgBoxStyle.Information, "eWIP")
                                Me.Close()
                                Exit Sub
                            End If
                            MsgBox("Action faild!", MsgBoxStyle.Information, "eWIP")
                            Me.Close()
                            Exit Sub
                            Exit Sub
                        Else
                            MsgBox("This operation handle by " & curName & "(" & curNO & "), Please contact your supervisor.", MsgBoxStyle.Information, "eWIP")
                            CountI = 0
                        End If
                    Else
                        MsgBox("Check your id!.", MsgBoxStyle.Information, "eWIP")
                        CountI = 0
                    End If
                End If
            ElseIf UCase(txtAction.Text) = "COMPLETED" Or UCase(txtAction.Text) = "SPLIT" Then


                If CountI = 0 Then


                    'fungsi gua {
                    If txtOperatorid.Text = "" Then

                        'yg orinya{
                        If CheckOperator(strRelV) = True Then
                            'If strOPid = txtOPid.Text Then

                            If UCase(txtAction.Text) = "SPLIT" Then
                                If Chksplit = 0 Then
                                    MsgBox("Now can not split this job!. Please contact your supervisor.", MsgBoxStyle.Information, "eWIP")
                                    Me.Close()
                                Else
                                    If strOPsplit = "N" Then
                                        MsgBox("Authorization failed !. Please contact your supervisor.", MsgBoxStyle.Information, "eWIP")
                                        Me.Close()
                                    End If
                                End If
                            End If


                            txtOperatorid.Text = strOPid
                            txtName.Text = strOPName
                            txtOperatorid.ForeColor = Color.Green
                            lblOperatorid.ForeColor = Color.Green
                            txtName.ForeColor = Color.Green
                            lblName.ForeColor = Color.Green
                            ''pnlcom.Visible = True
                            ''txtKeyComqty.Text = ""
                            ''txtKeyRejqty.Text = ""
                            ''pic1.Left = 278
                            ''txtnoofOP.Text = 1
                            ''pic1.Top = 289
                            ''lblMsgbox.Text = "Enter the completed quantity!"
                            ''txtKeyComqty.Text = txtComQty.Text
                            ''txtKeyComqty.Focus()

                            pnlReject.Visible = False
                            pnlcom.Visible = False
                            pnlnoofOp.Visible = True
                            pic1.Left = 278
                            pic1.Top = 150
                            TempMsg = lblMsgbox.Text
                            lblMsgbox.Text = "Enter the no. of operator!"
                            If Trim(txtnoofOP.Text) = "" Then
                                txtKeynoofOp.Text = 1
                            Else
                                txtKeynoofOp.Text = txtnoofOP.Text
                            End If
                            ' txtKeynoofOp.Text = txtnoofOP.Text
                            txtKeynoofOp.Focus()


                            CountI = 1
                            Exit Sub

                        Else
                            MsgBox("Check your id!", MsgBoxStyle.Information, "eWIP")
                            CountI = 0
                        End If
                        '} sampe sini

                    Else
                        pnlReject.Visible = False
                        pnlcom.Visible = False
                        pnlnoofOp.Visible = True
                        pic1.Left = 278
                        pic1.Top = 150
                        TempMsg = lblMsgbox.Text
                        lblMsgbox.Text = "Enter the no. of operator!"
                        If Trim(txtnoofOP.Text) = "" Then
                            txtKeynoofOp.Text = 1
                        Else
                            txtKeynoofOp.Text = txtnoofOP.Text
                        End If
                        ' txtKeynoofOp.Text = txtnoofOP.Text
                        txtKeynoofOp.Focus()


                        CountI = 1
                        Exit Sub

                    End If
                    '} sampesini




                End If

                If CountI = 1 Then
                    If strRelV = "NOOP" Then
                        pnlReject.Visible = False
                        pnlcom.Visible = False
                        pnlnoofOp.Visible = True
                        ' pic1.Left = 278
                        '  pic1.Top = 149
                        TempMsg = lblMsgbox.Text
                        lblMsgbox.Text = "Enter the no. of operator!"

                        txtKeynoofOp.Text = txtnoofOP.Text
                        txtKeynoofOp.Focus()
                        Exit Sub
                    End If
                    If strRelV = "CQ" Then
                        pnlReject.Visible = False
                        pnlcom.Visible = True
                        txtKeyComqty.Text = txtComQty.Text
                        lblMsgbox.Text = "Enter the completed quantity!"
                        txtKeyComqty.Focus()
                        pic1.Left = 278
                        ' txtnoofOP.Text = 1
                        pic1.Top = 289
                    ElseIf strRelV = "RQ" Then
                        pnlReject.Visible = True
                        pnlcom.Visible = False
                        lblMsgbox.Text = "Enter the rejected quantity, if any."
                        txtKeyRejqty.Text = txtRejQ.Text
                        txtKeyRejqty.Focus()
                    ElseIf strRelV = "SAVE" Then

                        'codingan gua {
                        Dim savestatus As Boolean = True
                        Dim i As Integer

                        For i = 0 To selectedJobGridList.Rows.Count - 1

                            Dim DS As New DataSet
                            Dim DSroute As New DataSet
                            Dim DSActual As New DataSet

                            DS = clsM.GetDataset("select * from tbjob where _job='" & selectedJobGridList.Rows(i).Cells(2).Value & "' AND _jobsuffix='" & selectedJobGridList.Rows(i).Cells(3).Value & "' order by _oper_num", "tbJob")

                            DSActual = clsM.GetDataset("select top 1 * from tbjobTrans where _job='" & selectedJobGridList.Rows(i).Cells(2).Value & "' AND _jobsuffix='" & selectedJobGridList.Rows(i).Cells(3).Value & "' order by _oper_num", "tbjobTrans")
                            DSroute = clsM.GetDataset("select top 1 * from tbjobroute where _job='" & selectedJobGridList.Rows(i).Cells(2).Value & "' AND _jobSuffix='" & DSActual.Tables(0).Rows(0).Item("_oper_num") & "' order by _operationNo", "tbjobroute")
                            If DSActual.Tables(0).Rows.Count > 0 Then

                                With DSActual.Tables(0).Rows(0)
                                    txtJobno.Text = .Item("_job")
                                    txtJobno.ForeColor = Color.Green
                                    lblJobno.ForeColor = Color.Green
                                    txtItem.Text = .Item("_item")
                                    txtItem.ForeColor = Color.Green
                                    lblItem.ForeColor = Color.Green
                                    txtRlQ.Text = Format(.Item("_qty_Rele_qty"), "0.00")
                                    txtRlQ.ForeColor = Color.Green
                                    lblRlQ.ForeColor = Color.Green
                                    txtReQ.Text = Format(.Item("_qty_op_qty"), "0.00")
                                    txtReQ.ForeColor = Color.Green
                                    lblReQ.ForeColor = Color.Green
                                    txtStartDate.Text = Format(Now, "dd/MM/yyyy HH:mm")
                                    txtStartDate.ForeColor = Color.Green
                                    lblStartDate.ForeColor = Color.Green
                                    ' ploptnew.Visible = True
                                    strstage = "New"
                                    If DSroute.Tables(0).Rows.Count > 0 Then
                                        txtWorkcenter.Text = DSroute.Tables(0).Rows(0).Item("_wc")
                                        txtWorkcenter.ForeColor = Color.Green
                                        lblWorkcenter.ForeColor = Color.Green
                                        txtOperationno.Text = DSroute.Tables(0).Rows(0).Item("_oper_enum")
                                        txtOperationno.ForeColor = Color.Green
                                        lblOperationno.ForeColor = Color.Green
                                        txtMachineId.Text = clsM.CheckMachineID(DSroute.Tables(0).Rows(0).Item("_wc"))
                                    End If
                                End With
                            End If


                            If COMPLETED_SAVE() = True Then

                            Else
                                savestatus = False
                            End If
                        Next

                        If savestatus = True Then
                            Me.Close()
                            Exit Sub
                        Else
                            txtMain.Focus()
                        End If

                        '} akhir codingan gua




                        'If COMPLETED_SAVE() = True Then
                        '    Me.Close()
                        '    Exit Sub
                        'Else
                        '    txtMain.Focus()
                        'End If
                    Else
                        MsgBox("Invalid input!", MsgBoxStyle.Information, "eWIP")
                        txtMain.Focus()
                        Exit Sub
                    End If

                End If


            ElseIf UCase(txtAction.Text) = "SKIP" Then
                If CountI = 0 Then


                    If CheckOperator(strRelV) = True Then
                        ' If strOPid = txtOPid.Text Then
                        txtOperatorid.Text = strOPid
                        txtName.Text = strOPName
                        txtOperatorid.ForeColor = Color.Green
                        lblOperatorid.ForeColor = Color.Green
                        txtName.ForeColor = Color.Green
                        lblName.ForeColor = Color.Green
                        pnlcom.Visible = True
                        txtKeyComqty.Text = ""
                        txtKeyRejqty.Text = ""
                        pic1.Left = 278
                        txtnoofOP.Text = 1
                        pic1.Top = 237
                        txtKeyComqty.Text = txtComQty.Text
                        txtKeyComqty.Focus()
                        CountI = 1
                        lblMsgbox.Text = "Scan 'SAVE' or 'CANCEL'!"
                        'Else
                        ' MsgBox("This operation handle by " & curName & "(" & curNO & "), Please check your supervisor!.")
                        ' CountI = 0
                        'End If
                    Else
                        MsgBox("Check your id!.", MsgBoxStyle.Information, "eWIP")
                        CountI = 0
                    End If


                End If

                If CountI = 1 Then
                    If strRelV = "NOOP" Then
                        pnlReject.Visible = False
                        pnlcom.Visible = False
                        pnlnoofOp.Visible = True
                        '   pic1.Left = 278
                        '  pic1.Top = 149
                        lblMsgbox.Text = "Enter the no. of operator!"
                        txtKeynoofOp.Text = txtnoofOP.Text
                        txtKeynoofOp.Focus()
                        Exit Sub
                    End If
                    If strRelV = "CQ" Then
                        pnlReject.Visible = False
                        pnlcom.Visible = True
                        lblMsgbox.Text = "Enter the completed quantity!"
                        txtKeyComqty.Text = txtComQty.Text
                        txtKeyComqty.Focus()
                    ElseIf strRelV = "RQ" Then
                        pnlReject.Visible = True
                        lblMsgbox.Text = "Enter the rejected quantity, if any."
                        pnlcom.Visible = False
                        pic1.Left = 278
                        pic1.Top = 314
                        txtKeyRejqty.Text = txtRejQ.Text
                        txtKeyRejqty.Focus()
                    ElseIf strRelV = "SAVE" Then


                        'codingan gua {
                        Dim savestatus As Boolean = True
                        Dim i As Integer

                        For i = 0 To selectedJobGridList.Rows.Count - 1

                            Dim DS As New DataSet
                            Dim DSroute As New DataSet
                            Dim DSActual As New DataSet

                            DS = clsM.GetDataset("select * from tbjob where _job='" & selectedJobGridList.Rows(i).Cells(2).Value & "' AND _jobsuffix='" & selectedJobGridList.Rows(i).Cells(3).Value & "' order by _oper_num", "tbJob")

                            DSActual = clsM.GetDataset("select top 1 * from tbjobTrans where _job='" & selectedJobGridList.Rows(i).Cells(2).Value & "' AND _jobsuffix='" & selectedJobGridList.Rows(i).Cells(3).Value & "' order by _oper_num", "tbjobTrans")
                            DSroute = clsM.GetDataset("select top 1 * from tbjobroute where _job='" & selectedJobGridList.Rows(i).Cells(2).Value & "' AND _jobSuffix='" & DSActual.Tables(0).Rows(0).Item("_oper_num") & "' order by _operationNo", "tbjobroute")
                            If DSActual.Tables(0).Rows.Count > 0 Then

                                With DSActual.Tables(0).Rows(0)
                                    txtJobno.Text = .Item("_job")
                                    txtJobno.ForeColor = Color.Green
                                    lblJobno.ForeColor = Color.Green
                                    txtItem.Text = .Item("_item")
                                    txtItem.ForeColor = Color.Green
                                    lblItem.ForeColor = Color.Green
                                    txtRlQ.Text = Format(.Item("_qty_Rele_qty"), "0.00")
                                    txtRlQ.ForeColor = Color.Green
                                    lblRlQ.ForeColor = Color.Green
                                    txtReQ.Text = Format(.Item("_qty_op_qty"), "0.00")
                                    txtReQ.ForeColor = Color.Green
                                    lblReQ.ForeColor = Color.Green
                                    txtStartDate.Text = Format(Now, "dd/MM/yyyy HH:mm")
                                    txtStartDate.ForeColor = Color.Green
                                    lblStartDate.ForeColor = Color.Green
                                    ' ploptnew.Visible = True
                                    strstage = "New"
                                    If DSroute.Tables(0).Rows.Count > 0 Then
                                        txtWorkcenter.Text = DSroute.Tables(0).Rows(0).Item("_wc")
                                        txtWorkcenter.ForeColor = Color.Green
                                        lblWorkcenter.ForeColor = Color.Green
                                        txtOperationno.Text = DSroute.Tables(0).Rows(0).Item("_oper_enum")
                                        txtOperationno.ForeColor = Color.Green
                                        lblOperationno.ForeColor = Color.Green
                                        txtMachineId.Text = clsM.CheckMachineID(DSroute.Tables(0).Rows(0).Item("_wc"))
                                    End If
                                End With
                            End If


                            If COMPLETED_SAVE() = True Then

                            Else
                                savestatus = False
                            End If
                        Next

                        If savestatus = True Then
                            Me.Close()
                            Exit Sub
                        Else
                            txtMain.Focus()
                        End If
                        ' } akhir codingan gua


                        'If COMPLETED_SAVE() = True Then
                        '    Me.Close()
                        '    Exit Sub
                        'Else
                        '    txtMain.Focus()
                        'End If
                    End If

                End If




            End If
        End If

    End Sub
    Function CheckJobNo(ByVal strjobAC As String, ByVal strjobpre As String) As Boolean
        CheckJobNo = False
        Dim DS As New DataSet
        Dim DSroute As New DataSet
        Dim DSActual As New DataSet
        Dim strJob, strSuf As String
        strJob = strjobAC
        strSuf = strjobpre
        DS = clsM.GetDataset("select * from tbjob where _job='" & strJob & "'", "tbJob")

        If DS.Tables(0).Rows.Count > 0 Then
            CheckJobNo = True
            DSActual = clsM.GetDataset("select * from tbjobTrans where _job='" & strJob & "' order by _oper_num", "tbJob")
            DSroute = clsM.GetDataset("select * from tbjobroute where _job='" & strJob & "' order by _operationNo", "tbJob")
            If DSActual.Tables(0).Rows.Count > 0 Then
            Else
                With DS.Tables(0).Rows(0)
                    txtJobno.Text = .Item("_job")
                    txtJobno.ForeColor = Color.Green
                    lblJobno.ForeColor = Color.Green
                    txtItem.Text = .Item("_item")
                    txtItem.ForeColor = Color.Green
                    lblItem.ForeColor = Color.Green
                    txtRlQ.Text = Format(.Item("_qtyReleased"), "0.00")
                    txtRlQ.ForeColor = Color.Green
                    lblRlQ.ForeColor = Color.Green
                    txtReQ.Text = Format(.Item("_qtyReleased"), "0.00")
                    txtReQ.ForeColor = Color.Green
                    lblReQ.ForeColor = Color.Green
                    txtStartDate.Text = Format(Now, "dd/MM/yyyy HH:mm")
                    txtStartDate.ForeColor = Color.Green
                    lblStartDate.ForeColor = Color.Green
                    ' ploptnew.Visible = True
                    strstage = "New"
                    If DSroute.Tables(0).Rows.Count > 0 Then
                        txtWorkcenter.Text = DSroute.Tables(0).Rows(0).Item("_wc")
                        txtWorkcenter.ForeColor = Color.Green
                        lblWorkcenter.ForeColor = Color.Green
                        txtOperationno.Text = DSroute.Tables(0).Rows(0).Item("_operationNo")
                        txtOperationno.ForeColor = Color.Green
                        lblOperationno.ForeColor = Color.Green
                        txtMachineId.Text = clsM.CheckMachineID(DSroute.Tables(0).Rows(0).Item("_wc"))
                    End If
                End With

            End If
        Else
            lblMsgbox.Text = "Scan job order no.!"
            txtMain.Text = ""
            txtMain.Focus()
        End If

    End Function



    Function CheckOperator(ByVal opid As String) As Boolean
        strOPid = ""
        strOPName = ""
        strOPsplit = ""
        strOPskip = ""
        CheckOperator = False
        Dim DSOperator As New DataSet
        DSOperator = clsM.GetDataset("select * from tbOperator where _operatorID ='" & fncstr(opid) & "'", "tbOperator")
        If DSOperator.Tables(0).Rows.Count > 0 Then
            strOPid = DSOperator.Tables(0).Rows(0).Item("_operatorID")
            strOPName = DSOperator.Tables(0).Rows(0).Item("_operatorName")
            strOPsplit = DSOperator.Tables(0).Rows(0).Item("_split")
            strOPskip = DSOperator.Tables(0).Rows(0).Item("_skip")
            'txtOperatorid.Text = DSOperator.Tables(0).Rows(0).Item("_operatorID")
            'txtName.Text = DSOperator.Tables(0).Rows(0).Item("_operatorName")
            strOPsplit = DSOperator.Tables(0).Rows(0).Item("_split")
            strOPskip = DSOperator.Tables(0).Rows(0).Item("_skip")
            CheckOperator = True
        End If

    End Function








    Sub resetAll()
        txtMain.Text = ""
        pic1.Left = 84
        pic1.Top = 3
        ' ploptnew.Visible = False

        'txtJobno
        txtJobno.Text = ""
        txtJobno.ForeColor = Color.Black
        lblJobno.ForeColor = Color.Black
        'Action
        txtAction.Text = ""
        txtAction.ForeColor = Color.Black
        lblAction.ForeColor = Color.Black
        'lblItem
        txtItem.Text = ""
        txtItem.ForeColor = Color.Black
        lblItem.ForeColor = Color.Black
        'lblOperationno
        txtOperationno.Text = ""
        txtOperationno.ForeColor = Color.Black
        lblOperationno.ForeColor = Color.Black
        'txtWorkcenter
        txtWorkcenter.Text = ""
        txtWorkcenter.ForeColor = Color.Black
        lblWorkcenter.ForeColor = Color.Black
        'txtMachineId
        txtMachineId.Text = ""
        txtMachineId.ForeColor = Color.Black
        lblMachineId.ForeColor = Color.Black
        'lblOperatorid
        txtOperatorid.Text = ""
        txtOperatorid.ForeColor = Color.Black
        lblOperatorid.ForeColor = Color.Black
        'txtName
        txtName.Text = ""
        txtName.ForeColor = Color.Black
        lblName.ForeColor = Color.Black
        'Released Quantity
        txtRlQ.Text = ""
        txtRlQ.ForeColor = Color.Black
        lblRlQ.ForeColor = Color.Black
        'Received Quantity
        txtReQ.Text = ""
        txtReQ.ForeColor = Color.Black
        lblReQ.ForeColor = Color.Black
        'StartDate
        txtStartDate.Text = ""
        txtStartDate.ForeColor = Color.Black
        lblStartDate.ForeColor = Color.Black
        'Completed Quantity
        txtComQty.Text = ""
        txtComQty.ForeColor = Color.Black
        lblComqty.ForeColor = Color.Black
        'Rejected Quantity
        txtRejQ.Text = ""
        txtRejQ.ForeColor = Color.Black
        lblRejQ.ForeColor = Color.Black
        'Edate
        txtEdate.Text = ""
        txtEdate.ForeColor = Color.Black
        lblEdate.ForeColor = Color.Black
        txtOrgRemarks.Text = ""
        txtRemarks.Text = ""
    End Sub


    Private Sub TimerMachine_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerMachine.Tick
        If Trim(lblMsgbox.Text) = "" Then
            ' pic2.Visible = False
            pic78.Visible = False
            lblMsgbox.Visible = False
        Else
            lblMsgbox.Visible = True
            'pic2.Visible = True
            pic78.Visible = True
        End If
        If Trim(lblMsgbox.Text) = "" Then
            pic2.Visible = False
        Else
            If pic2.Visible = False Then
                pic2.Visible = True
            Else
                pic2.Visible = False
            End If
        End If

    End Sub
    Function START_SAVE() As Boolean
        START_SAVE = False

        If Trim(txtJobno.Text) = "" Then
            Exit Function
        End If
        If Trim(txtAction.Text) = "" Then
            Exit Function
        End If

        If Trim(txtItem.Text) = "" Then
            Exit Function
        End If
        If Trim(txtOperationno.Text) = "" Then
            Exit Function
        End If
        If Trim(txtWorkcenter.Text) = "" Then
            Exit Function
        End If
        If Trim(txtMachineId.Text) = "" Then
            Exit Function
        End If
        If Trim(txtOperatorid.Text) = "" Then
            Exit Function
        End If
        If Trim(txtName.Text) = "" Then
            Exit Function
        End If

        If Trim(txtRlQ.Text) = "" Then
            Exit Function
        End If

        If Trim(txtReQ.Text) = "" Then
            Exit Function
        End If
        If Trim(txtStartDate.Text) = "" Then
            Exit Function
        End If


        Try

            With com

                '(_job,_jobsuffix,_jobsuffixParent,_item,_jobDate,_wc,_trans_type,_trans_datefrm,_qty_Rele_qty,_qty_op_qty,_oper_num,_emp_num,_emp_name,_start_time,_CreatedBy,_status,_resourceid,_resourceDesc,_machineid


                .Connection = cn

                .CommandType = CommandType.StoredProcedure
                If Trim(txtAction.Text) = "START" Then
                    .CommandText = "sp_JOB_START_ADD"
                Else
                    .CommandText = "sp_JOB_SKIP_ADD"
                End If

                parm = .Parameters.Add("@tansnum", SqlDbType.Int)
                parm.Value = Val(txtID.Text)

                parm = .Parameters.Add("@job", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtJobno.Text)
                parm = .Parameters.Add("@jobsuffix", SqlDbType.VarChar, 50)
                parm.Value = txtsuf.Text

                parm = .Parameters.Add("@jobsuffixParent", SqlDbType.VarChar, 50)
                parm.Value = txtParent.Text
                parm = .Parameters.Add("@item", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtItem.Text)


                parm = .Parameters.Add("@oper_num", SqlDbType.Int)
                parm.Value = Val(Trim(txtOperationno.Text))
                parm = .Parameters.Add("@wc", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtWorkcenter.Text)








                parm = .Parameters.Add("@machineid", SqlDbType.VarChar, 50)


                parm.Value = Trim(txtMachineId.Text)

                parm = .Parameters.Add("@emp_num", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtOperatorid.Text)

                parm = .Parameters.Add("@emp_name", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtName.Text)
                Dim strdbl As Double = get_ServerDate()
                ' txtEdate.Text = Format(DateTime.FromOADate(strdbl), "dd/MM/yyyy HH:mm")
                fltStartDate.Text = strdbl
                parm = .Parameters.Add("@Start_date", SqlDbType.Float)
                parm.Value = CDbl(strdbl)

                parm = .Parameters.Add("@start_time", SqlDbType.Int)
                parm.Value = CInt(Format(DateTime.FromOADate(CDbl(strdbl)), "HHmm"))


                parm = .Parameters.Add("@qty_Rele_qty", SqlDbType.Float)
                parm.Value = Val(txtRlQ.Text)

                parm = .Parameters.Add("@qty_op_qty", SqlDbType.Float)
                parm.Value = Val(txtReQ.Text)
                parm = .Parameters.Add("@status", SqlDbType.VarChar, 10)
                parm.Value = Trim(txtAction.Text)
                parm = .Parameters.Add("@Createdid", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtOperatorid.Text)
                parm = .Parameters.Add("@CreatedBy", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtName.Text)
                parm = .Parameters.Add("@Remarks", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtRemarks.Text)

                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                START_SAVE = True
                lblMsgbox.Text = "Action updated!"
                'lblMsgbox.Text = "Successfully updated!"
            End With



        Catch ex As Exception
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try

        Return START_SAVE
    End Function


    Function COMPLETED_SAVE() As Boolean
        COMPLETED_SAVE = False

        If Trim(txtJobno.Text) = "" Then
            MsgBox("Invalid input!", MsgBoxStyle.Information, "eWIP")
            Me.Close()
            Exit Function
        End If
        If Trim(txtAction.Text) = "" Then
            MsgBox("Invalid input!", MsgBoxStyle.Information, "eWIP")
            Me.Close()
            Exit Function
        End If
        If Trim(txtID.Text) = "" Or Val(txtID.Text) = 0 Then
            MsgBox("Invalid input!")
            Me.Close()
            Exit Function
        End If

        If Trim(txtMachineId.Text) = "" Then
            MsgBox("Invalid input!", MsgBoxStyle.Information, "eWIP")
            Me.Close()
            Exit Function
        End If
        If Trim(txtOperatorid.Text) = "" Then
            MsgBox("Scan your id!", MsgBoxStyle.Information, "eWIP")
            txtMain.Focus()
            CountI = 0
            Exit Function
        End If



        If Val(txtRejQ.Text) <> Val(txtTot.Text) Then
            MsgBox("Total Rejected Quantity not tally!", MsgBoxStyle.Information, "eWIP")
            txtMain.Focus()
            CountI = 1
            Exit Function
        End If

        If UCase(txtAction.Text) <> "SPLIT" Then
            If Val(txtReQ.Text) <> Val(txtComQty.Text) + Val(txtRejQ.Text) Then
                MsgBox("Total completed and rejected Quantity not tally!", MsgBoxStyle.Information, "eWIP")
                txtMain.Focus()
                CountI = 1
                Exit Function
            End If
        Else
            If Val(txtComQty.Text) = 0 And Val(txtRejQ.Text) = 0 Then
                MsgBox("Total completed and rejected Quantity not tally!", MsgBoxStyle.Information, "eWIP")
                txtMain.Focus()
                CountI = 1
                Exit Function
            End If
            If Val(txtReQ.Text) = Val(txtComQty.Text) + Val(txtRejQ.Text) Then
                MsgBox("Total completed and rejected Quantity not tally!", MsgBoxStyle.Information, "eWIP")
                txtMain.Focus()
                CountI = 1
                Exit Function
            End If
        End If

        Dim strspt As String = GetMaxSufID()
        If UCase(txtAction.Text) = "SPLIT" Then
            If strspt = "" Then
                MsgBox("Split only three level only,Please contact your supervisor.", MsgBoxStyle.Information, "eWIP")
                Exit Function
            Else
                txtParent.Text = txtsuf.Text
            End If
        End If
        Dim splitst As Boolean = False
        If UCase(txtAction.Text) <> "SPLIT" Then
            If CheckSplitCon() = True Then
                If strspt = "" Then
                    MsgBox("Split only three level only,Please contact your supervisor.", MsgBoxStyle.Information, "eWIP")
                    Exit Function
                Else
                    splitst = True
                    txtParent.Text = txtsuf.Text
                    txtsuf.Text = strspt
                End If

            End If
        End If




        Try
            Dim strSupNew As String
            With com

                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                If UCase(txtAction.Text) <> "SPLIT" Then
                    .CommandText = "sp_JOB_COMPLETED_UPDATED_new"
                Else

                    .CommandText = "sp_JOB_SPLIT_UPDATED_new"
                End If



                parm = .Parameters.Add("@tansnum", SqlDbType.Int)
                parm.Value = Val(txtID.Text)
                parm = .Parameters.Add("@jobsuffixParent", SqlDbType.VarChar, 10)
                parm.Value = Trim(txtParent.Text)
                parm = .Parameters.Add("@jobsuffix", SqlDbType.VarChar, 10)
                If UCase(txtAction.Text) <> "SPLIT" Then
                    strSupNew = Trim(txtsuf.Text)
                    parm.Value = Trim(txtsuf.Text)
                Else
                    strSupNew = strspt
                    parm.Value = strspt
                End If

                parm = .Parameters.Add("@emp_num", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtOperatorid.Text)
                parm = .Parameters.Add("@emp_name", SqlDbType.VarChar, 256)
                parm.Value = Trim(txtName.Text)


                Dim strdbl As Double = get_ServerDate()


                parm = .Parameters.Add("@end_Date", SqlDbType.Float)
                parm.Value = CDbl(strdbl)
                parm = .Parameters.Add("@end_time", SqlDbType.Int)
                parm.Value = CInt(Format(DateTime.FromOADate(CDbl(strdbl)), "HHmm"))

                parm = .Parameters.Add("@no_oper", SqlDbType.Int)
                If Val(txtnoofOP.Text) = 0 Or Trim(txtnoofOP.Text) = "" Then
                    parm.Value = 1
                Else
                    parm.Value = Val(txtnoofOP.Text)
                End If


                parm = .Parameters.Add("@qty_complete", SqlDbType.Float)
                parm.Value = Val(txtComQty.Text)
                parm = .Parameters.Add("@qty_scrapped", SqlDbType.Float)
                parm.Value = Val(txtRejQ.Text)
                parm = .Parameters.Add("@status", SqlDbType.VarChar, 10)
                parm.Value = Trim(txtAction.Text)
                parm = .Parameters.Add("@Remarks", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtRemarks.Text)
                parm = .Parameters.Add("@SplitAction", SqlDbType.Int)
                If splitst = False Then
                    parm.Value = 0
                Else
                    parm.Value = 1
                End If
                parm = .Parameters.Add("@ARelQty", SqlDbType.Float)
                If splitst = False Then
                    parm.Value = Val(txtRlQ.Text)
                Else
                    parm.Value = Val(Val(txtComQty.Text) + Val(txtRejQ.Text))
                End If


                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()

                Delete_rejected(Val(txtID.Text))
                COMPLETED_SAVE = True
                Dim clsR As clsRej
                For Each clsR In Newhash.Values
                    .Connection = cn
                    .CommandType = CommandType.StoredProcedure
                    .CommandText = "sp_JOB_REJECTED_UPDATED"
                    parm = .Parameters.Add("@refID", SqlDbType.Int)
                    parm.Value = Val(txtID.Text)
                    parm = .Parameters.Add("@RejectedCode", SqlDbType.VarChar, 50)
                    parm.Value = Trim(clsR.Rcode)
                    parm = .Parameters.Add("@RejectedDesc", SqlDbType.VarChar, 256)
                    parm.Value = Trim(clsR.Desc)
                    parm = .Parameters.Add("@RejectedQty", SqlDbType.Float)
                    parm.Value = CDbl(clsR.rQty)
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()

                Next
                If Val(txtComQty.Text) = 0 Then
                    'CloseAllCase(Trim(Split(txtJobno.Text, "-")(0)), strSupNew, Trim(txtParent.Text), Val(txtOperationno.Text))
                End If



                If UCase(txtAction.Text) = "SPLIT" Then
                    PrintReport(txtJobno.Text, strspt)
                    MsgBox(PrintMSG, MsgBoxStyle.Information, "eWIP")
                End If
                If UCase(txtAction.Text) = "COMPLETED" Then
                    If splitst = True Then
                        PrintReport(txtJobno.Text, strspt)
                        MsgBox(PrintMSG, MsgBoxStyle.Information, "eWIP")
                    Else
                        lblMsgbox.Text = "Action updated!"
                        MsgBox("Action updated!", MsgBoxStyle.Information, "eWIP")
                    End If
                End If

                COMPLETED_SAVE = True

            End With
            ' lblMsgbox.Text = "Successfully updated!"
            ' MsgBox("Successfully updated!", MsgBoxStyle.Information, "eWIP")

        Catch ex As Exception
            lblMsgbox.Text = "ERROR"
            COMPLETED_SAVE = False
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try

        Return COMPLETED_SAVE
    End Function
    Sub PrintReport(ByVal strjob As String, ByVal strsuf As String)
        Dim prn As New ClsAMTprint.clsmain

        prn.printAMT(Trim(strjob) & "-" & Trim(strsuf), SqlString, PrinterName, False)
    End Sub


    Private Sub txtKeyComqty_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtKeyComqty.KeyPress
        If Asc(e.KeyChar) = 13 Then


            If Editcolumn Then
                'tambahan gua
                If UCase(txtAction.Text) = "COMPLETED" Then
                    If Val(txtKeyComqty.Text) > Val(txtReQ.Text) Then
                        MsgBox("Total completed quantity not tally", MsgBoxStyle.Information, "eWIP")
                        Exit Sub
                    ElseIf Val(txtKeyComqty.Text) < Val(txtReQ.Text) Then

                        selectedJobGridList.Rows(CurrentColumnSelected).Cells(5).Value = txtKeyComqty.Text

                        txtComQty.Text = txtKeyComqty.Text
                        txtKeyRejqty.Text = txtRejQ.Text
                        txtComQty.ForeColor = Color.Green
                        lblComqty.ForeColor = Color.Green
                        pnlcom.Visible = False
                        pnlReject.Visible = True
                        pic1.Left = 278
                        pic1.Top = 314
                        txtKeyComqty.Text = txtKeyRejqty.Text
                        lblMsgbox.Text = "Enter the rejected quantity, if any."
                        txtKeyRejqty.Focus()
                    Else

                        selectedJobGridList.Rows(CurrentColumnSelected).Cells(5).Value = txtKeyComqty.Text

                        txtComQty.Text = txtKeyComqty.Text
                        txtComQty.ForeColor = Color.Green
                        lblComqty.ForeColor = Color.Green
                        txtRejQ.Text = 0.0
                        txtComQty.ForeColor = Color.Green
                        lblComqty.ForeColor = Color.Green
                        lblRejQ.ForeColor = Color.Green
                        txtRejQ.ForeColor = Color.Green
                        pnlcom.Visible = False
                        txtKeyComqty.Text = ""
                        Dim strdbl As Double = get_ServerDate()
                        txtEdate.Text = Format(DateTime.FromOADate(strdbl), "dd/MM/yyyy HH:mm")
                        fltStartDate.Text = strdbl
                        '   If WCDesc <> "" Then

                        'End If
                        CountI = 1
                        txtEdate.ForeColor = Color.Green
                        lblEdate.ForeColor = Color.Green
                        pic1.Left = 2780
                        pic1.Top = 369
                        ' lblSave.Visible = True
                        ' lblCRQ.Visible = True
                        ' lblCRQ.Visible = True
                        txtMain.Focus()
                        lblMsgbox.Text = "Scan 'SAVE' or 'CANCEL'!"
                    End If

                End If
            Else

                'original code

                If UCase(txtAction.Text) = "COMPLETED" Then
                    If Val(txtKeyComqty.Text) > Val(txtReQ.Text) Then
                        MsgBox("Total completed quantity not tally", MsgBoxStyle.Information, "eWIP")
                        Exit Sub
                    ElseIf Val(txtKeyComqty.Text) < Val(txtReQ.Text) Then
                        txtComQty.Text = txtKeyComqty.Text
                        txtKeyRejqty.Text = txtRejQ.Text
                        txtComQty.ForeColor = Color.Green
                        lblComqty.ForeColor = Color.Green
                        pnlcom.Visible = False
                        pnlReject.Visible = True
                        pic1.Left = 278
                        pic1.Top = 314
                        txtKeyComqty.Text = txtKeyRejqty.Text
                        lblMsgbox.Text = "Enter the rejected quantity, if any."
                        txtKeyRejqty.Focus()
                    Else
                        txtComQty.Text = txtKeyComqty.Text
                        txtComQty.ForeColor = Color.Green
                        lblComqty.ForeColor = Color.Green
                        txtRejQ.Text = 0.0
                        txtComQty.ForeColor = Color.Green
                        lblComqty.ForeColor = Color.Green
                        lblRejQ.ForeColor = Color.Green
                        txtRejQ.ForeColor = Color.Green
                        pnlcom.Visible = False
                        txtKeyComqty.Text = ""
                        Dim strdbl As Double = get_ServerDate()
                        txtEdate.Text = Format(DateTime.FromOADate(strdbl), "dd/MM/yyyy HH:mm")
                        fltStartDate.Text = strdbl
                        '   If WCDesc <> "" Then

                        'End If
                        CountI = 1
                        txtEdate.ForeColor = Color.Green
                        lblEdate.ForeColor = Color.Green
                        pic1.Left = 2780
                        pic1.Top = 369
                        ' lblSave.Visible = True
                        ' lblCRQ.Visible = True
                        ' lblCRQ.Visible = True
                        txtMain.Focus()
                        lblMsgbox.Text = "Scan 'SAVE' or 'CANCEL'!"
                    End If
                ElseIf UCase(txtAction.Text) = "SPLIT" Then
                    If Val(txtKeyComqty.Text) >= Val(txtReQ.Text) Then
                        MsgBox("Total completed quantity not tally", MsgBoxStyle.Information, "eWIP")
                        Exit Sub
                    ElseIf Val(txtKeyComqty.Text) < Val(txtReQ.Text) Then
                        txtComQty.Text = txtKeyComqty.Text
                        txtComQty.ForeColor = Color.Green
                        lblComqty.ForeColor = Color.Green
                        pnlcom.Visible = False
                        pnlReject.Visible = True
                        pic1.Left = 278
                        pic1.Top = 312
                        txtKeyComqty.Text = txtRejQ.Text
                        txtKeyComqty.Text = txtKeyRejqty.Text
                        CountI = 1
                        txtKeyRejqty.Focus()
                    End If
                End If

            End If






        End If
        If (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57) Or Asc(e.KeyChar) = 46 Or Asc(e.KeyChar) = 8 Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtKeyRejqty_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtKeyRejqty.KeyPress
        If Asc(e.KeyChar) = 13 Then

            If UCase(txtAction.Text) = "COMPLETED" Then

                If Editcolumn Then
                    'fungsi gua


                    If Val(txtKeyRejqty.Text) > Val(txtReQ.Text) Then
                        MsgBox("Check your rejected quantity", MsgBoxStyle.Information, "eWIP")
                        Exit Sub
                    ElseIf Val(txtKeyRejqty.Text) <> 0 Then

                        RejectedCode = ""
                        rejtransfer = ""
                        selectedJobGridList.Rows(CurrentColumnSelected).Cells(6).Value = txtKeyRejqty.Text

                        txtRejQ.Text = txtKeyRejqty.Text
                        txtRejQ.ForeColor = Color.Green
                        lblRejQ.ForeColor = Color.Green
                        Dim obj As New frmRejected
                        obj.lblWc.Text = txtWorkcenter.Text
                        obj.lblItem.Text = txtItem.Text
                        obj.txtRlQty.Text = txtRlQ.Text
                        obj.txtReQty.Text = txtReQ.Text
                        obj.txtRejQty.Text = txtRejQ.Text
                        pnlReject.Visible = False
                        txtKeyRejqty.Text = ""
                        obj.ShowDialog()
                        'txtMain.Focus()


                        selectedJobGridList.Rows(CurrentColumnSelected).Cells(7).Value = RejStuffs.RejDataString

                        Panel5.Visible = True
                        'jobnumbersaved.Focus()
                        selectedJobGridList.Focus()

                        loadView()
                        'lblMsgbox.Text = "Scan 'SAVE' or 'CANCEL'!"
                        lblMsgbox.Text = "Press ESC to back, use ARROW keys to select WO"
                        If Val(txtTot.Text) <> Val(txtRejQ.Text) Then
                            '  lblSave.Visible = False
                            '  lblCRQ.Visible = True

                            'txtMain.Focus()

                            selectedJobGridList.Focus()

                            pic1.Left = 2780
                            pic1.Top = 367
                            Exit Sub
                        End If
                        If Val(txtReQ.Text) <> Val(txtRejQ.Text) + Val(txtComQty.Text) Then
                            ' lblSave.Visible = True
                            'lblCRQ.Visible = True
                            'lblCRQ.Visible = True

                            selectedJobGridList.Focus()
                            'txtMain.Focus()
                            pic1.Left = 2780
                            pic1.Top = 367
                            Exit Sub
                        End If
                        If Val(txtReQ.Text) = Val(txtComQty.Text) + Val(txtRejQ.Text) Then
                            Dim strdbl As Double = get_ServerDate()
                            txtEdate.Text = Format(DateTime.FromOADate(strdbl), "dd/MM/yyyy HH:mm")
                            fltStartDate.Text = strdbl
                            txtEdate.ForeColor = Color.Green
                            lblEdate.ForeColor = Color.Green
                            ' lblSave.Visible = True
                            '  lblCRQ.Visible = True
                            pic1.Left = 2780
                            pic1.Top = 367
                        End If

                        Editcolumn = False
                    Else
                        pnlReject.Visible = False
                        txtRejQ.Text = 0.0
                        txtRejQ.ForeColor = Color.Green
                        lblRejQ.ForeColor = Color.Green
                        ' lblSave.Visible = True
                        '  lblCRQ.Visible = True
                        lblMsgbox.Text = ""
                        pic1.Left = 2780
                        pic1.Top = 367
                        txtMain.Focus()
                        lblMsgbox.Text = "Scan 'SAVE' or 'CANCEL'!"
                    End If

                Else
                    'original code

                    If Val(txtKeyRejqty.Text) > Val(txtReQ.Text) Then
                        MsgBox("Check your rejected quantity", MsgBoxStyle.Information, "eWIP")
                        Exit Sub
                    ElseIf Val(txtKeyRejqty.Text) <> 0 Then
                        txtRejQ.Text = txtKeyRejqty.Text
                        txtRejQ.ForeColor = Color.Green
                        lblRejQ.ForeColor = Color.Green
                        Dim obj As New frmRejected
                        obj.lblWc.Text = txtWorkcenter.Text
                        obj.lblItem.Text = txtItem.Text
                        obj.txtRlQty.Text = txtRlQ.Text
                        obj.txtReQty.Text = txtReQ.Text
                        obj.txtRejQty.Text = txtRejQ.Text
                        pnlReject.Visible = False
                        txtKeyRejqty.Text = ""
                        obj.ShowDialog()
                        txtMain.Focus()
                        loadView()
                        lblMsgbox.Text = "Scan 'SAVE' or 'CANCEL'!"
                        If Val(txtTot.Text) <> Val(txtRejQ.Text) Then
                            '  lblSave.Visible = False
                            '  lblCRQ.Visible = True
                            txtMain.Focus()
                            pic1.Left = 2780
                            pic1.Top = 367
                            Exit Sub
                        End If
                        If Val(txtReQ.Text) <> Val(txtRejQ.Text) + Val(txtComQty.Text) Then
                            ' lblSave.Visible = True
                            'lblCRQ.Visible = True
                            'lblCRQ.Visible = True
                            txtMain.Focus()
                            pic1.Left = 2780
                            pic1.Top = 367
                            Exit Sub
                        End If
                        If Val(txtReQ.Text) = Val(txtComQty.Text) + Val(txtRejQ.Text) Then
                            Dim strdbl As Double = get_ServerDate()
                            txtEdate.Text = Format(DateTime.FromOADate(strdbl), "dd/MM/yyyy HH:mm")
                            fltStartDate.Text = strdbl
                            txtEdate.ForeColor = Color.Green
                            lblEdate.ForeColor = Color.Green
                            ' lblSave.Visible = True
                            '  lblCRQ.Visible = True
                            pic1.Left = 2780
                            pic1.Top = 367
                        End If
                    Else
                        pnlReject.Visible = False
                        txtRejQ.Text = 0.0
                        txtRejQ.ForeColor = Color.Green
                        lblRejQ.ForeColor = Color.Green
                        ' lblSave.Visible = True
                        '  lblCRQ.Visible = True
                        lblMsgbox.Text = ""
                        pic1.Left = 2780
                        pic1.Top = 367
                        txtMain.Focus()
                        lblMsgbox.Text = "Scan 'SAVE' or 'CANCEL'!"
                    End If
                End If




            ElseIf UCase(txtAction.Text) = "SPLIT" Then

                If Val(txtKeyRejqty.Text) + Val(txtComQty.Text) > Val(txtReQ.Text) Then
                    MsgBox("Check your rejected quantity", MsgBoxStyle.Information, "eWIP")
                    Exit Sub

                ElseIf Val(txtKeyRejqty.Text) + Val(txtComQty.Text) = Val(txtReQ.Text) Then
                    MsgBox("Check your completed and rejected quantity", MsgBoxStyle.Information, "eWIP")
                    Exit Sub

                ElseIf Val(txtKeyRejqty.Text) <> 0 Then
                    lblMsgbox.Text = ""
                    txtRejQ.Text = txtKeyRejqty.Text
                    txtRejQ.ForeColor = Color.Green
                    lblRejQ.ForeColor = Color.Green
                    Dim obj As New frmRejected
                    obj.lblWc.Text = txtWorkcenter.Text
                    obj.lblItem.Text = txtItem.Text
                    obj.txtRlQty.Text = txtRlQ.Text
                    obj.txtReQty.Text = txtReQ.Text
                    obj.txtRejQty.Text = txtRejQ.Text
                    pnlReject.Visible = False
                    txtKeyRejqty.Text = ""
                    obj.ShowDialog()
                    txtMain.Focus()
                    loadView()
                    ' lblSave.Visible = True
                    ' lblCRQ.Visible = True
                    pic1.Left = 2780
                    pic1.Top = 367
                    If Val(txtTot.Text) <> Val(txtRejQ.Text) Then
                        'lblSave.Visible = False
                        ' lblCRQ.Visible = True
                        txtMain.Focus()
                        pic1.Left = 2780
                        pic1.Top = 367
                        Exit Sub
                    End If
                    Dim strdbl As Double = get_ServerDate()
                    txtEdate.Text = Format(DateTime.FromOADate(strdbl), "dd/MM/yyyy HH:mm")
                    fltStartDate.Text = strdbl
                    lblMsgbox.Text = "Scan 'SAVE' or 'CANCEL'!"
                Else
                    lblMsgbox.Text = ""
                    pnlReject.Visible = False
                    txtRejQ.Text = 0.0
                    txtRejQ.ForeColor = Color.Green
                    lblRejQ.ForeColor = Color.Green
                    'lblSave.Visible = False
                    'lblCRQ.Visible = True
                    Dim strdbl As Double = get_ServerDate()
                    txtEdate.Text = Format(DateTime.FromOADate(strdbl), "dd/MM/yyyy HH:mm")
                    fltStartDate.Text = strdbl
                    '  lblSave.Visible = True
                    ' lblCRQ.Visible = True
                    pic1.Left = 2780
                    pic1.Top = 367
                    lblMsgbox.Text = "Scan 'SAVE' or 'CANCEL'!"
                    If Val(txtTot.Text) <> Val(txtRejQ.Text) Then
                        ' lblSave.Visible = False
                        ' lblCRQ.Visible = True
                        txtMain.Focus()
                        pic1.Left = 2780
                        pic1.Top = 367
                        Exit Sub
                    End If
                    txtMain.Focus()
                End If
            End If
        End If
        If (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57) Or Asc(e.KeyChar) = 46 Or Asc(e.KeyChar) = 8 Then
            e.Handled = False
        Else
            e.Handled = True
        End If


    End Sub
    Sub loadView()
        lstv.Items.Clear()
        txtTot.Text = 0
        If Newhash.Count > 0 Then
            Dim irej As clsRej
            For Each irej In Newhash.Values
                Dim ls As New ListViewItem(Trim(irej.Rcode))    ' you can also use reader.GetSqlValue(0) 
                ls.SubItems.Add(Trim(irej.rQty))
                lstv.Items.Add(ls)
                txtTot.Text = Val(txtTot.Text) + Trim(irej.rQty)
            Next
        End If
    End Sub


    Sub loadlstvData()
        lstv.Items.Clear()
        If selectedJobGridList.Rows(CurrentColumnSelected).Cells(7).Value <> "" Then
            Dim rejrow() As String = selectedJobGridList.Rows(CurrentColumnSelected).Cells(7).Value.ToString().Split(";")
            Dim rejcell() As String
            Dim rejrowcount As Integer
            For rejrowcount = 0 To rejrow.Length - 1

                rejcell = rejrow(rejrowcount).Split(",")

                Dim ls As New ListViewItem(Trim(rejcell(0)))    ' you can also use reader.GetSqlValue(0) 
                ls.SubItems.Add(Trim(rejcell(1)))
                lstv.Items.Add(ls)
                txtTot.Text = Val(txtTot.Text) + Trim(rejcell(1))

            Next

            RejStuffs.RejDataString = selectedJobGridList.Rows(CurrentColumnSelected).Cells(7).Value.ToString()
        Else
            RejStuffs.RejDataString = ""
        End If
    End Sub

    Function Delete_rejected(ByVal idno As Integer) As Boolean
        With com
            Delete_rejected = False
            Try
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "delete from tbRejectedTrans where _rejid=" & idno
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                Delete_rejected = True
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
            Finally
                cn.Close()
            End Try
            Return Delete_rejected
        End With


    End Function

    Function get_ServerDate() As Double
        Dim dsdate As DataSet
        dsdate = clsM.GetDataset("select getDate() as ToDate", "tbDate")
        get_ServerDate = CDate(dsdate.Tables(0).Rows(0).Item("ToDate")).ToOADate
        Return get_ServerDate
    End Function

    Private Sub txtKeynoofOp_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtKeynoofOp.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If UCase(txtAction.Text) = "COMPLETED" Or UCase(txtAction.Text) = "SPLIT" Then

                'editan eka
                If Editcolumn Then
                    'editan gua




                    If Val(txtKeynoofOp.Text) = 0 Or Trim(txtKeynoofOp.Text) = "" Then
                        txtnoofOP.Text = 1
                        selectedJobGridList.Rows(CurrentColumnSelected).Cells(4).Value = 1
                    Else
                        txtnoofOP.Text = Val(txtKeynoofOp.Text)
                        selectedJobGridList.Rows(CurrentColumnSelected).Cells(4).Value = txtKeynoofOp.Text
                    End If
                    txtnoofOP.ForeColor = Color.Green
                    lblnoofOP.ForeColor = Color.Green

                    '  pic1.Left = 278
                    ' pic1.Top = 300
                    pnlnoofOp.Visible = False
                    '  txtKeynoofOp.Text = ""
                    txtMain.Focus()
                    lblMsgbox.Text = TempMsg
                    TempMsg = ""

                    pnlcom.Visible = True
                    txtKeyComqty.Text = ""
                    txtKeyRejqty.Text = ""
                    pic1.Left = 278
                    ' txtnoofOP.Text = 1
                    pic1.Top = 289
                    lblMsgbox.Text = "Enter the completed quantity!"
                    txtKeyComqty.Text = txtComQty.Text
                    txtKeyComqty.Focus()
                    CountI = 1



                Else
                    'original

                    If Val(txtKeynoofOp.Text) = 0 Or Trim(txtKeynoofOp.Text) = "" Then
                        txtnoofOP.Text = 1
                    Else
                        txtnoofOP.Text = Val(txtKeynoofOp.Text)
                    End If
                    txtnoofOP.ForeColor = Color.Green
                    lblnoofOP.ForeColor = Color.Green

                    '  pic1.Left = 278
                    ' pic1.Top = 300
                    pnlnoofOp.Visible = False
                    '  txtKeynoofOp.Text = ""
                    txtMain.Focus()
                    lblMsgbox.Text = TempMsg
                    TempMsg = ""

                    pnlcom.Visible = True
                    txtKeyComqty.Text = ""
                    txtKeyRejqty.Text = ""
                    pic1.Left = 278
                    ' txtnoofOP.Text = 1
                    pic1.Top = 289
                    lblMsgbox.Text = "Enter the completed quantity!"
                    txtKeyComqty.Text = txtComQty.Text
                    txtKeyComqty.Focus()
                    CountI = 1
                End If

            End If
        End If

        If (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57) Or Asc(e.KeyChar) = 46 Or Asc(e.KeyChar) = 8 Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub
    Function PAUSE_SAVE() As Boolean
        PAUSE_SAVE = False
        If Trim(txtJobno.Text) = "" Then
            MsgBox("Invalid input!", MsgBoxStyle.Information, "eWIP")
            Me.Close()
            Exit Function
        End If
        If Trim(txtAction.Text) = "" Then
            MsgBox("Invalid input!")
            Me.Close()
            Exit Function
        End If
        If Trim(txtID.Text) = "" Or Val(txtID.Text) = 0 Then
            MsgBox("Invalid input!", MsgBoxStyle.Information, "eWIP")
            Me.Close()
            Exit Function
        End If

        If Trim(txtMachineId.Text) = "" Then
            MsgBox("Invalid input!", MsgBoxStyle.Information, "eWIP")
            Me.Close()
            Exit Function
        End If
        If Trim(txtOperatorid.Text) = "" Then
            MsgBox("Scan your id!", MsgBoxStyle.Information, "eWIP")
            txtMain.Focus()
            CountI = 0
            Exit Function
        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "sp_JOB_PAUSE_UPDATED"
                parm = .Parameters.Add("@tansnum", SqlDbType.Int)
                parm.Value = Val(txtID.Text)
                parm = .Parameters.Add("@parm", SqlDbType.Int)
                If txtAction.Text = "PAUSE" Then
                    parm.Value = 0
                Else
                    parm.Value = 1
                End If
                Dim strdbl As Double = get_ServerDate()
                parm = .Parameters.Add("@pstart", SqlDbType.Float)
                parm.Value = strdbl
                parm = .Parameters.Add("@pend", SqlDbType.Float)
                parm.Value = strdbl
                parm = .Parameters.Add("@Remarks", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtRemarks.Text)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                com.Parameters.Clear()
                PAUSE_SAVE = True
            End With
        Catch ex As Exception
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try


        Return PAUSE_SAVE



    End Function
    Function GetMaxSufID() As String
        GetMaxSufID = ""
        Dim DSTrans As New DataSet
        Dim txtSubval As String = ""
        Dim AdTrans As New SqlDataAdapter
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.Text

                .CommandText = "select * from tbJobTrans where _job='" & Trim(txtJobno.Text) & "'"
                AdTrans.SelectCommand = com
                AdTrans.Fill(DSTrans, "AdTrans")

            End With


            Dim DRCheckChiled() As DataRow = DSTrans.Tables(0).Select("_jobsuffixParent='" & Trim(txtsuf.Text) & "' and _oper_num=" & Val(txtOperationno.Text))
            If DRCheckChiled.Length > 0 Then
                Dim i As Integer
                For i = 0 To DRCheckChiled.Length - 1
                    If txtSubval = "" Then
                        txtSubval = "'" & Trim(DRCheckChiled(i).Item("_jobsuffix")) & "'"
                    Else
                        txtSubval = txtSubval & ",'" & Trim(DRCheckChiled(i).Item("_jobsuffix")) & "'"
                    End If
                Next
            End If
            '  Dim DRCheckAfter() As DataRow = DSTrans.Tables(0).Select("_jobsuffixParent='" & Trim(txtParent.Text) & "' and _oper_num>" & Val(txtOperationno.Text))
            ' If DRCheckAfter.Length > 0 Then
            'GetMaxSufID = ""
            'Return GetMaxSufID
            'Exit Function
            'End If
            Dim dr As SqlDataReader
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                If txtSubval = "" Then
                    txtSubval = "''"
                End If
                Dim sql As String = "select Min(_lel_1_Name) as _lel_1_Name from tbLevel1 where _lel_1_Parent='" & Trim(txtsuf.Text) & "' and _lel_1_Name not in (" & txtSubval & ")"
                .CommandText = "select Min(_lel_1_Name) as _lel_1_Name from tbLevel1 where _lel_1_Parent='" & Trim(txtsuf.Text) & "' and _lel_1_Name not in (" & txtSubval & ")"
                cn.Open()
                dr = .ExecuteReader
                If dr.Read Then
                    GetMaxSufID = dr.Item("_lel_1_Name")
                    cn.Close()
                End If
                cn.Close()
                Return GetMaxSufID
            End With

        Catch ex As Exception
        Finally
            cn.Close()
        End Try
    End Function
    Function Show_Handover() As Boolean
        Dim obj As New frmHandover
        obj.txtJobno.Text = txtJobno.Text
        obj.lbl.Text = lbl.Text
        obj.txtsuf.Text = txtsuf.Text
        obj.lblEMP.Text = txtOperatorid.Text
        obj.lblName.Text = txtName.Text
        obj.txtid.Text = txtID.Text
        obj.ShowDialog()

    End Function

    Function CheckSplitCon() As Boolean
        Dim ds As New DataSet
        CheckSplitCon = False
        ds = clsM.GetDataset("select * from  tbJobTrans where _tansnum<>" & Val(txtID.Text) & " and  _job='" & Trim(txtJobno.Text) & "' and _oper_num=" & Val(txtOperationno.Text) & " and _jobsuffixParent='" & Trim(txtsuf.Text) & "'", "tbDate")
        If ds.Tables(0).Rows.Count > 0 Then
            CheckSplitCon = True
        End If
        Return CheckSplitCon
    End Function




    'Private Sub lblMsgbox_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblMsgbox.Resize
    '    PictureBox2.Left = lblMsgbox.Left + lblMsgbox.Width - 10
    '    lblTest.Left = lblMsgbox.Left
    '    lblTest.Width = lblMsgbox.Width
    'End Sub




    Private Sub lstvData_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstvData.GotFocus
        txtMain.Text = ""
        txtMain.Focus()
    End Sub

    Private Sub lstvData_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstvData.SelectedIndexChanged
        txtMain.Text = ""
        txtMain.Focus()
    End Sub



    Private Sub lstv_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstv.GotFocus
        txtMain.Text = ""
        txtMain.Focus()
    End Sub

    Private Sub lstv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstv.SelectedIndexChanged
        txtMain.Text = ""
        txtMain.Focus()
    End Sub


    Private Sub txtKeyComqty_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKeyComqty.TextChanged

    End Sub

    Private Sub txtRemarks_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRemarks.KeyPress
        If Asc(e.KeyChar) = 13 Then

            If WCDesc = "HI" Then
                CountI = 2
                pic1.Left = 2780
                pic1.Top = 369
                lblMsgbox.Text = "Scan 'SAVE' or 'CANCEL'!"
                WCDesc = ""
                ' lblMsgbox.Text = TempMsg
                TempMsg = ""
                pnlRemarks.Visible = False
                txtOrgRemarks.Text = txtRemarks.Text
                txtMain.Text = ""
                txtMain.Focus()
            Else
                lblMsgbox.Text = TempMsg
                TempMsg = ""
                pnlRemarks.Visible = False
                txtOrgRemarks.Text = txtRemarks.Text
                txtMain.Text = ""
                txtMain.Focus()
            End If

            'WCDesc = ""
        End If
    End Sub



    Private Sub txtOrgRemarks_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOrgRemarks.GotFocus

        txtMain.Text = ""
        txtMain.Focus()
    End Sub


    Private Sub txtKeynoofOp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKeynoofOp.TextChanged

    End Sub

    Function CloseAllCase(ByVal strJobNo As String, ByVal strSuf As String, ByVal strSufP As String, ByVal strOPNum As String) As Boolean
        CloseAllCase = False
        Dim DSOP As New DataSet
        DSOP = clsM.GetDataset("select _operationNo,_WC from tbJobRoute  where _job='" & strJobNo & "' and _operationNo>" & strOPNum & " order by _operationNo", "tbOP")
        If DSOP.Tables("tbOP").Rows.Count > 0 Then
            For i = 0 To DSOP.Tables("tbOP").Rows.Count - 1
                With com


                    .Connection = cn
                    .CommandType = CommandType.StoredProcedure

                    .CommandText = "sp_JOB_Close_Com_is_Zero"

                    parm = .Parameters.Add("@Job", SqlDbType.VarChar, 50)
                    parm.Value = Trim(strJobNo)
                    parm = .Parameters.Add("@jobsuffix", SqlDbType.VarChar, 10)
                    parm.Value = Trim(strSuf)
                    parm = .Parameters.Add("@jobsuffixParent", SqlDbType.VarChar, 10)
                    parm.Value = Trim(strSufP)

                    parm = .Parameters.Add("@OldOPNo", SqlDbType.Int)
                    parm.Value = strOPNum

                    parm = .Parameters.Add("@opNo", SqlDbType.Int)
                    parm.Value = DSOP.Tables("tbOP").Rows(i).Item("_operationNo")

                    parm = .Parameters.Add("WC", SqlDbType.VarChar, 256)
                    parm.Value = DSOP.Tables("tbOP").Rows(i).Item("_WC")

                    parm = .Parameters.Add("@NextOPNo", SqlDbType.Int)
                    If DSOP.Tables("tbOP").Rows.Count > i + 1 Then
                        parm.Value = DSOP.Tables("tbOP").Rows(i + 1).Item("_operationNo")
                    Else
                        parm.Value = 0
                    End If
                    cn.Open()
                    .ExecuteNonQuery()
                    cn.Close()
                    com.Parameters.Clear()
                    strOPNum = DSOP.Tables("tbOP").Rows(i).Item("_operationNo")
                End With

            Next
        End If
    End Function

    Sub endlist()



    End Sub


    Sub selectlist()
        'jobnumbersaved.Visible = False
        'machineSelectInput.Visible = True
        txtMain.Focus()
        ListSelectMode = True
    End Sub


    Private Sub jobnumbersaved_KeyPress(sender As Object, e As KeyPressEventArgs) Handles jobnumbersaved.KeyPress
        Dim strRelV = UCase(jobnumbersaved.Text)
        If Asc(e.KeyChar) = 13 Then
            'Dim s() As String
            's = Split(strRelV, "-")
            'Dim strJob As String = ""
            'Dim strSuf As String = ""
            'Dim strRework As String = ""
            'If s.Length = 2 Then
            '    strJob = s(0)
            '    strSuf = s(1)
            '    strRework = ""
            'ElseIf s.Length > 2 Then
            '    strJob = s(0)
            'strSuf = s(1)
            '    strRework = s(2)
            'Else
            '    strJob = s(0)
            '    strSuf = "M"
            '    strRework = ""
            'End If

            'If CheckJobNo(strJob, strSuf, strRework) = True Then
            '    CountI = 1
            '    pic1.Left = 236
            '    pic1.Top = 278
            '    lblMsgbox.Text = "Scan action!"
            'Else
            '    lblMsgbox.Text = "Scan job order no.!"
            '    txtMain.Text = ""
            '    txtMain.Focus()
            '    CountI = 0
            '    pic1.Left = 236
            '    pic1.Top = 213
            '    Exit Sub
            'End If

            If UCase(jobnumbersaved.Text) = "SELECT" Then
                'selectlist()
                lblMsgbox.Text = "Use ARROW keys to select row, Press ENTER to Edit it"
                selectedJobGridList.Focus()

            ElseIf UCase(jobnumbersaved.Text) = "BACK" Then
                jobnumbersaved.Focus()
                ListSelectMode = False
            ElseIf UCase(jobnumbersaved.Text) = "ENDLIST" Then
                txtMain.Focus()
                Panel5.Visible = False
                ListSelectMode = False

                If CountI = 0 Then
                    lblMsgbox.Text = "Scan machine ID"
                Else
                    lblMsgbox.Text = "Scan your id!"
                End If


            Else
                Dim jobarray() As String

                jobarray = jobnumbersaved.Text.Split("-")
                If (jobarray.Length > 1) Then
                    Try
                        InsertToList(jobarray(0), jobarray(1))

                        jobnumbersaved.Text = ""
                    Catch ex As Exception

                    End Try

                End If

            End If
        End If
    End Sub


    Sub JobSelected(ByVal Row As Integer)
        Dim selectedList = selectedJobGridList.Rows(Row)


        'Dim DS As New DataSet
        'Dim DSroute As New DataSet
        'Dim DSActual As New DataSet

        'DSActual = clsM.GetDataset("select * from tbjobTrans where _job='" & selectedList.Cells(2) & "' AND _suffix='" & selectedList.Cells(3) & "' order by _oper_num", "tbJob")
        'DSroute = clsM.GetDataset("select * from tbjobroute where _job='" & selectedList.Cells(2) & "' order by _operationNo", "tbJob")
        'If DSActual.Tables(0).Rows.Count > 0 Then
        'Else
        '    With DS.Tables(0).Rows(0)
        '        txtJobno.Text = .Item("_job")
        '        txtJobno.ForeColor = Color.Green
        '        lblJobno.ForeColor = Color.Green
        '        txtItem.Text = .Item("_item")
        '        txtItem.ForeColor = Color.Green
        '        lblItem.ForeColor = Color.Green
        '        txtRlQ.Text = Format(.Item("_qtyReleased"), "0.00")
        '        txtRlQ.ForeColor = Color.Green
        '        lblRlQ.ForeColor = Color.Green
        '        txtReQ.Text = Format(.Item("_qtyReleased"), "0.00")
        '        txtReQ.ForeColor = Color.Green
        '        lblReQ.ForeColor = Color.Green
        '        txtStartDate.Text = Format(Now, "dd/MM/yyyy HH:mm")
        '        txtStartDate.ForeColor = Color.Green
        '        lblStartDate.ForeColor = Color.Green
        '        ' ploptnew.Visible = True
        '        strstage = "New"
        '        If DSroute.Tables(0).Rows.Count > 0 Then
        '            txtWorkcenter.Text = DSroute.Tables(0).Rows(0).Item("_wc")
        '            txtWorkcenter.ForeColor = Color.Green
        '            lblWorkcenter.ForeColor = Color.Green
        '            txtOperationno.Text = DSroute.Tables(0).Rows(0).Item("_operationNo")
        '            txtOperationno.ForeColor = Color.Green
        '            lblOperationno.ForeColor = Color.Green
        '            txtMachineId.Text = clsM.CheckMachineID(DSroute.Tables(0).Rows(0).Item("_wc"))
        '        End If
        '    End With

        'End If


    End Sub


    Private Sub jobnumbersaved_TextChanged(sender As Object, e As EventArgs) Handles jobnumbersaved.TextChanged

    End Sub

    Private Sub selectedJobGridList_KeyDown(sender As Object, e As KeyEventArgs) Handles selectedJobGridList.KeyDown
        Dim getIndex = selectedJobGridList.CurrentCell.OwningRow.Index
        If e.KeyCode = Keys.Enter Then
            ' Your code here
            Editcolumn = True
            e.SuppressKeyPress = True
            Panel5.Visible = False
            'MsgBox(getIndex)

            If selectedJobGridList.Rows(CurrentColumnSelected).Cells(7).Value <> "" Then
                RejStuffs.RejDataString = selectedJobGridList.Rows(CurrentColumnSelected).Cells(7).Value.ToString()
            Else
                RejStuffs.RejDataString = ""
            End If

            txtMain.Focus()
            If txtOperatorid.Text <> "" Then
                lblMsgbox.Text = "tap enter to continue"
            Else
                lblMsgbox.Text = "Scan your id!"
            End If



            LoadDataToFields(getIndex)
            loadlstvData()

        ElseIf e.KeyCode = Keys.Escape Then
            jobnumbersaved.Text = ""
            jobnumbersaved.Focus()
            ListSelectMode = False

            Panel5.Visible = True
        End If
    End Sub


    Sub LoadDataToFields(ByVal Index As Integer)
        Dim DS As New DataSet
        Dim DSroute As New DataSet
        Dim DSActual As New DataSet

        DS = clsM.GetDataset("select * from tbjob where _job='" & selectedJobGridList.Rows(Index).Cells(2).Value & "' AND _jobsuffix='" & selectedJobGridList.Rows(Index).Cells(3).Value & "' order by _oper_num", "tbJob")

        DSActual = clsM.GetDataset("select top 1 * from tbjobTrans where _job='" & selectedJobGridList.Rows(Index).Cells(2).Value & "' AND _jobsuffix='" & selectedJobGridList.Rows(Index).Cells(3).Value & "' order by _oper_num", "tbjobTrans")
        DSroute = clsM.GetDataset("select top 1 * from tbjobroute where _job='" & selectedJobGridList.Rows(Index).Cells(2).Value & "' AND _jobSuffix='" & DSActual.Tables(0).Rows(0).Item("_oper_num") & "' order by _operationNo", "tbjobroute")
        If DSActual.Tables(0).Rows.Count > 0 Then

            With DSActual.Tables(0).Rows(0)
                txtJobno.Text = .Item("_job")
                txtJobno.ForeColor = Color.Green
                lblJobno.ForeColor = Color.Green
                txtItem.Text = .Item("_item")
                txtItem.ForeColor = Color.Green
                lblItem.ForeColor = Color.Green
                txtRlQ.Text = Format(.Item("_qty_Rele_qty"), "0.00")
                txtRlQ.ForeColor = Color.Green
                lblRlQ.ForeColor = Color.Green
                txtReQ.Text = Format(.Item("_qty_op_qty"), "0.00")
                txtReQ.ForeColor = Color.Green
                lblReQ.ForeColor = Color.Green
                txtStartDate.Text = Format(Now, "dd/MM/yyyy HH:mm")
                txtStartDate.ForeColor = Color.Green
                lblStartDate.ForeColor = Color.Green
                ' ploptnew.Visible = True
                strstage = "New"
                If DSroute.Tables(0).Rows.Count > 0 Then
                    txtWorkcenter.Text = DSroute.Tables(0).Rows(0).Item("_wc")
                    txtWorkcenter.ForeColor = Color.Green
                    lblWorkcenter.ForeColor = Color.Green
                    txtOperationno.Text = DSroute.Tables(0).Rows(0).Item("_oper_enum")
                    txtOperationno.ForeColor = Color.Green
                    lblOperationno.ForeColor = Color.Green
                    txtMachineId.Text = clsM.CheckMachineID(DSroute.Tables(0).Rows(0).Item("_wc"))
                End If
            End With
        End If
        CountI = 0
        CurrentColumnSelected = Index


    End Sub


    Private Sub EditDataGrid()


        txtKeynoofOp.Visible = True
        txtKeynoofOp.Focus()




    End Sub


    Private Sub selectedJobGridList_KeyPress(sender As Object, e As KeyPressEventArgs) Handles selectedJobGridList.KeyPress



    End Sub
End Class