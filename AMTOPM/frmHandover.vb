Imports System.Data.SqlClient
Public Class frmHandover
    Dim parm As SqlParameter
    Private Sub txtMain_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMain.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Dim strRelV As String = txtMain.Text
            txtMain.Text = ""
            If UCase(strRelV) = "CL" Then
                txtHandoverid.Text = ""
                txtHandoverName.Text = ""
                txtMain.Focus()
                Exit Sub
            End If

            If UCase(strRelV) = "SAVE" Then
                SaveData()
                Me.Close()
                txtMain.Focus()
                Exit Sub
            End If
            If UCase(strRelV) = "EX" Then
                Me.Close()
                txtMain.Focus()
                Exit Sub
            End If
            If strRelV <> "" Then
                If strRelV = lblEMP.Text Then
                    lblMSG.Text = "Please check handover id!"
                    txtMain.Focus()
                    Exit Sub
                End If
                If CheckOperator(strRelV) = True Then
                Else
                    lblMSG.Text = "Please check handover id!"
                End If


            End If

        End If
    End Sub

    Function CheckOperator(ByVal opid As String) As Boolean
        CheckOperator = False
        Dim DSOperator As New DataSet
        Dim clsM As New clsMain
        DSOperator = clsM.GetDataset("select * from tbOperator where _operatorID ='" & fncstr(opid) & "'", "tbOperator")
        If DSOperator.Tables(0).Rows.Count > 0 Then
            txtHandoverid.Text = DSOperator.Tables(0).Rows(0).Item("_operatorID")
            txtHandoverName.Text = DSOperator.Tables(0).Rows(0).Item("_operatorName")
            CheckOperator = True
            lblMSG.Text = "Please scan 'SAVE'"
        End If
    End Function

    Private Sub frmHandover_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        strHandover = "NO"
        lblMSG.Text = "Please scan handover id!"
    End Sub

    Function SaveData() As Boolean
        SaveData = False
        If Val(txtid.Text) = 0 Then
            lblMSG.Text = "Please Check your data!"
            txtMain.Focus()
            Return SaveData
            Exit Function
        End If
        If Trim(txtHandoverid.Text) = "" Then
            lblMSG.Text = "Please scan handover id!"
            txtMain.Focus()
            Return SaveData
            Exit Function

        End If
        Try
            With com
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .CommandText = "sp_JobHandover_Add"
                parm = .Parameters.Add("@tansnum", SqlDbType.Int)
                parm.Value = Val(txtid.Text)
                parm = .Parameters.Add("@hemp_num", SqlDbType.VarChar, 50)
                parm.Value = fncstr(txtHandoverid.Text)
                parm = .Parameters.Add("@hemp_name", SqlDbType.VarChar, 255)
                parm.Value = fncstr(txtHandoverName.Text)

                parm = .Parameters.Add("@emp_num", SqlDbType.VarChar, 50)
                parm.Value = fncstr(txtHandoverid.Text)
                parm = .Parameters.Add("@emp_name", SqlDbType.VarChar, 255)
                parm.Value = fncstr(txtHandoverName.Text)


                cn.Open()
                .ExecuteNonQuery()
                strHandover = "YES"
                SaveData = True
                cn.Close()
                com.Parameters.Clear()
                '   txtWKID.Select()
                lblMSG.Text = "Action updated!"
            End With


        Catch ex As Exception
            lblMSG.Text = ex.Message
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        txtMain.Focus()
        Return SaveData
    End Function
End Class