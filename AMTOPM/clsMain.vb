Imports System.Data.SqlClient
Public Class clsMain
    Public Function GetDataset(ByVal SQLs As String, ByVal TableName As String) As DataSet
        Dim da As New SqlDataAdapter
        Dim clsM As New clsMain
        Try
            'If cn.State = ConnectionState.Closed Then
            '    GetDataset = Nothing
            '    Exit Function
            'ElseIf Not cn.State = ConnectionState.Open Then
            '    GetDataset = Nothing
            '    Exit Function
            'End If
            Dim ds As New DataSet
            da = New SqlDataAdapter(SQLs, cn)
            da.Fill(ds, TableName)
            da.Dispose()

            GetDataset = ds

            ds.Dispose()
        Catch ex As Exception
            GetDataset = Nothing
        End Try
    End Function
    Function CheckMachineID(ByVal wrkid As String) As String
        CheckMachineID = "-"
        Dim DSMachine As New DataSet
        DSMachine = GetDataset("select * from tbMachine where _Mid in(select _Mid from tbMachineGroup where _wc='" & fncstr(wrkid) & "')", "tbMachine")
        If DSMachine.Tables(0).Rows.Count = 1 Then
            CheckMachineID = DSMachine.Tables(0).Rows(0).Item("_machineID")
        ElseIf DSMachine.Tables(0).Rows.Count > 1 Then
            CheckMachineID = ""
        Else
            CheckMachineID = "-"
        End If
    End Function

    Function CheckMachineIDName(ByVal wrkid As String, ByVal Mname As String) As String
        CheckMachineIDName = "-"
        Dim DSMachine As New DataSet
        DSMachine = GetDataset("select * from tbMachine where _machineID='" & fncstr(Mname) & "' and  _Mid in(select _Mid from tbMachineGroup where _wc='" & fncstr(wrkid) & "')", "tbMachine")
        If DSMachine.Tables(0).Rows.Count > 0 Then
            CheckMachineIDName = DSMachine.Tables(0).Rows(0).Item("_machineID")
        Else
            CheckMachineIDName = ""
      
        End If
    End Function

End Class
