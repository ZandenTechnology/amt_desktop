Imports System.Data.SqlClient
Imports ClsAMTprint.clsmain
Public Class frmTransfer
    Dim CountI As Integer
    Dim strOPid As String = ""
    Dim strOPName As String = ""
    Dim strOPsplit As String = ""
    Dim strOPskip As String = ""
    Dim clsM As New clsMain
    Public trasopid As String
    Public trasopName As String
    Public intidno As Integer

    Private Sub frmTransfer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtTRID.Text = ""
        txtTRName.Text = ""
        txtOPid.Text = ""
        txtOPName.Text = ""
        CountI = 0
        lblMsgbox.Text = ""
        lblMsgbox.Text = "Scan ID for sending party"
    End Sub
    Private Sub txtMain_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMain.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Dim strRelV As String

            strRelV = UCase(txtMain.Text)
            txtMain.Text = ""

            If UCase(strRelV) = "CLER" Or UCase(strRelV) = "CNCL" Then
                ClearAll()
                CountI = 0
                Exit Sub
            End If

            If UCase(strRelV) = "EXIT" Then
                Me.Close()
                Exit Sub
            End If

            If CountI = 0 Then
                If CheckOperator(strRelV) = True Then
                    'If strOPid = trasopid Then
                    txtOPid.Text = strOPid
                    txtOPName.Text = strOPName
                    lblOPName.ForeColor = Color.Green
                    lblOPid.ForeColor = Color.Green
                    txtOPid.ForeColor = Color.Green
                    txtOPid.ForeColor = Color.Green
                    pic1.Visible = False
                    pic2.Visible = True
                    pic3.Visible = False
                    CountI = 1
                    lblMsgbox.Text = "Scan ID for receiving party"
                    txtMain.Focus()

                    Exit Sub

                    'Else
                    ' MsgBox("This operation handle by " & trasopName & "(" & trasopid & "), Please check your supervisor!.", MsgBoxStyle.Information, "eWIP")
                    '  CountI = 0
                    ' End If
                Else
                    MsgBox("Invalid operator id!", MsgBoxStyle.Information, "eWIP")
                    CountI = 0
                End If
            End If

            If CountI = 1 Then
                If CheckOperator(strRelV) = True Then
                   
                    txtTRID.Text = strOPid
                    txtTRName.Text = strOPName
                    lblTRName.ForeColor = Color.Green
                    lblTRID.ForeColor = Color.Green
                    txtTRID.ForeColor = Color.Green
                    txtTRName.ForeColor = Color.Green
                    pic1.Visible = False
                    pic2.Visible = False
                    ' pic3.Visible = True
                    ' lblAction.Visible = True

                    If Trim(strRelV) = Trim(txtOPid.Text) Then
                        lblMsgbox.Text = "Sending and receiving party must be different !"
                        MsgBox("ID Error!", MsgBoxStyle.Information, "eWIP")
                        CountI = 0
                        txtOPid.Text = ""
                        txtOPName.Text = ""
                        txtTRID.Text = ""
                        txtTRName.Text = ""
                        lblOPName.ForeColor = Color.Black
                        lblOPid.ForeColor = Color.Black
                        txtOPid.ForeColor = Color.Black
                        txtOPid.ForeColor = Color.Black
                        lblTRName.ForeColor = Color.Black
                        lblTRID.ForeColor = Color.Black
                        txtTRID.ForeColor = Color.Black
                        txtTRID.ForeColor = Color.Black


                        lblMsgbox.Text = "Scan ID for sending party"
                        Exit Sub
                    End If

                    CountI = 2
                    txtMain.Focus()
                    lblMsgbox.Text = "Scan 'SAVE' or 'CANCEL'!"
                    Exit Sub
                Else
                    MsgBox("Invalid operator id!", MsgBoxStyle.Information, "eWIP")
                    CountI = 1
                End If
            End If

            If CountI = 2 Then
                If strRelV = "SAVE" Then

                    If SAVE_Transfer() = True Then

                        '  PrintReport()

                        Me.Close()
                    End If
                    txtMain.Focus()
                    Exit Sub
                Else
                    CountI = 2
                End If
            End If


        End If


    End Sub
    Sub ClearAll()
        txtOPid.Text = ""
        txtOPName.Text = ""
        txtTRID.Text = ""
        txtTRName.Text = ""
        strOPid = ""
        strOPName = ""
        strOPsplit = ""
        strOPskip = ""
        pic1.Visible = True
        pic2.Visible = False
        pic3.Visible = False
         lblAction.Visible = False 
        lblOPName.ForeColor = Color.Black
        lblOPid.ForeColor = Color.Black
        txtOPid.ForeColor = Color.Black
        txtOPid.ForeColor = Color.Black
        lblMsgbox.Text = "Scan ID for sending party"
    End Sub

    Function CheckOperator(ByVal opid As String) As Boolean
        strOPid = ""
        strOPName = ""
        strOPsplit = ""
        strOPskip = ""
        CheckOperator = False
        Dim DSOperator As New DataSet
        DSOperator = clsM.GetDataset("select * from tbOperator where _operatorID ='" & fncstr(opid) & "'", "tbOperator")
        If DSOperator.Tables(0).Rows.Count > 0 Then
            strOPid = DSOperator.Tables(0).Rows(0).Item("_operatorID")
            strOPName = DSOperator.Tables(0).Rows(0).Item("_operatorName")
            strOPsplit = DSOperator.Tables(0).Rows(0).Item("_skip")
            strOPskip = DSOperator.Tables(0).Rows(0).Item("_split")
            'txtOperatorid.Text = DSOperator.Tables(0).Rows(0).Item("_operatorID")
            'txtName.Text = DSOperator.Tables(0).Rows(0).Item("_operatorName")
            strOPsplit = DSOperator.Tables(0).Rows(0).Item("_skip")
            strOPskip = DSOperator.Tables(0).Rows(0).Item("_split")
            CheckOperator = True
        End If

    End Function


    Function SAVE_Transfer() As Boolean
        SAVE_Transfer = False
        If txtOPid.Text = "" Then
            MsgBox("Please Scan properly", MsgBoxStyle.Information, "eWIP")
            CountI = 0
            Exit Function
        End If

        If txtTRID.Text = "" Then
            MsgBox("Please Scan properly", MsgBoxStyle.Information, "eWIP")
            CountI = 1
            Exit Function
        End If


        Try
            Dim parm As SqlParameter
            With com
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "Update  tbJObTrans set _CreateDate=@CreateDate, _Transempid=@emp_num,_TransempName=@emp_name,_Transempid_to=@Transempid_to,_TransempName_to=@TransempName_to,_trans='No'  where _tansnum=" & intidno
                parm = .Parameters.Add("@emp_num", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtTRID.Text)
                parm = .Parameters.Add("@emp_name", SqlDbType.VarChar, 255)
                parm.Value = (txtTRName.Text)

                parm = .Parameters.Add("@Transempid_to", SqlDbType.VarChar, 50)
                parm.Value = Trim(txtTRID.Text)
                parm = .Parameters.Add("@TransempName_to", SqlDbType.VarChar, 255)
                parm.Value = (txtTRName.Text)
                parm = .Parameters.Add("@CreateDate", SqlDbType.Float)
                parm.Value = CDbl(Now.ToOADate)
                cn.Open()
                .ExecuteNonQuery()
                cn.Close()
                MsgBox("Action updated!", MsgBoxStyle.Information, "eWIP")
                SAVE_Transfer = True
            End With
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "eWIP")
        Finally
            cn.Close()
            com.Parameters.Clear()
        End Try
        Return SAVE_Transfer
    End Function


    Sub PrintReport()
        Dim prn As New ClsAMTprint.clsmain
        prn.printAMT(txtJobno.Text, SqlString, PrinterName, False)
    End Sub

    Private Sub TimerMachine_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerMachine.Tick
        If PictureBox1.Visible = False Then
            PictureBox1.Visible = True
        Else
            PictureBox1.Visible = False
        End If
    End Sub
End Class