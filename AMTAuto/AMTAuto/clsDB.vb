Imports System.Data.SqlClient
Public Class clsMain
    Public Function GetDataset(ByVal SQLs As String, ByVal TableName As String) As DataSet
        Dim da As New SqlDataAdapter
        Try
            'If cn.State = ConnectionState.Closed Then
            '    GetDataset = Nothing
            '    Exit Function
            'ElseIf Not cn.State = ConnectionState.Open Then
            '    GetDataset = Nothing
            '    Exit Function
            'End If
            Dim ds As New DataSet
            da = New SqlDataAdapter(SQLs, cn)
            da.Fill(ds, TableName)
            da.Dispose()

            GetDataset = ds

            ds.Dispose()
        Catch ex As Exception
            GetDataset = Nothing
        End Try
    End Function
    Public Function CheckDate(ByVal st As String) As Double
        CheckDate = 0
        If Trim(st) <> "" Then
            Dim s() As String = Split(st, "/")
            Dim setdate As Date = CDate(DateSerial(s(2), s(1), s(0)))
            If CInt(Format(setdate, "dd")) <> CInt(s(0)) Then
                Return 0
                Exit Function
            End If
            If CInt(Format(setdate, "MM")) <> CInt(s(1)) Then
                Return 0
                Exit Function
            End If
            If CInt(Format(setdate, "yyyy")) <> CInt(s(2)) Then
                Return 0
                Exit Function
            End If
            Return setdate.ToOADate
        End If
    End Function
End Class
Public Class clsRej
    Public Rcode As String
    Public Desc As String
    Public rQty As Double
End Class
